<%@ page language="java" import="java.util.*" pageEncoding="utf-8"
	contentType="text/html; charset=utf-8"%>
<%@taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" " http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="keywords" content="">
<link rel="icon" href="${baseurl}/res/common/images/favicon.ico">
<title>管理端</title>

<link href="${baseurl}/res/Content/styles/bootstrap.min.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/styles/font-awesome.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/scripts/plugins/BeAlert-master/BeAlert.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/scripts/plugins/loading/css/loading.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/scripts/plugins/colorbox/colorbox.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/scripts/plugins/datetime/pikaday.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/scripts/plugins/tree/tree.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/scripts/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/scripts/plugins/jqgrid/jqgrid.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/CloudSale/css/Desktop.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/styles/cloudSale-ui.css" rel="stylesheet" />
<link href="${baseurl}/res/Content/styles/style.css" rel="stylesheet" />

<script src="${baseurl}/res/common/js/vue.js"></script>
<script src="${baseurl}/res/Content/scripts/jquery/jquery-2.1.1.js"></script>
<script src="${baseurl}/res/Content/CloudSale/init/Public_Js.js"></script>
<script src="${baseurl}/res/Content/scripts/utils/util.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/dialog/dialog.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="${baseurl}/res/Content/scripts/utils/cloudSale-clientdata.js"></script>
<script src="${baseurl}/res/Content/scripts/bootstrap/bootstrap.min.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/cookie/jquery.cookie.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/datepicker/WdatePicker.js"></script>

<script src="${baseurl}/res/Content/scripts/plugins/jqgrid/grid.locale-en.js"></script>
<script src="${baseurl}/res/Content/scripts/utils/cloudSale-ui.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/BeAlert-master/BeAlert.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/loading/js/loading.js"></script>


<script src="${baseurl}/res/Content/scripts/bootstrap/BootstrapMenu.min.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/printTable/jquery.printTable.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/echarts/echarts.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/echarts/CloudSale_zt.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/echarts/MyEcharts.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/echarts/worldcloud.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/tree/treeCloudsale.js"></script>
<script src="${baseurl}/res/Content/scripts/plugins/jqgrid/jqgrid.js"></script>
<script src="${baseurl}/res/Content/CloudSale/init/Public_Js.js?v=1.0.0.5"></script>

<script>
	var baseurl = '${baseurl}';
	</script>
<script src="${baseurl}/res/Content/CloudSale/index.js"></script>

<style>
    html {
        /*height: 100%;*/
        width: 100%;
        /*overflow: hidden;*/
        margin: 0;
        padding: 0;
    }

    body {
        height: 100%;
        width: 100%;
        overflow: auto;
        margin: 0;
        padding-left: 5px;
        background: rgb(241, 241, 241);
    }

    .block {
        margin: 0;
        padding: 0;
    }
</style>
    
</head>
<body style="overflow: hidden;">
	<!-- Main section-->
	<section style="width:100%;height:100%;"> <!-- Page content--> <decorator:body /> </section>
</body>