<#ftl strip_whitespace=true>
<#macro applicationName
>${Application["webapp.kb.name"]?default("hhmanagement")}</#macro>
<#macro splitStr strName strLen>
<#if (strName?length > strLen)>${strName?substring(0, strLen)}...<#else>${strName!}</#if>
</#macro>
<#macro labelNameSplitStr labelName strName strLen>
<div><#if (strName?length > 0)>${labelName!}：</#if><#if (strName?length > strLen)>${strName?substring(0, strLen)}...<#else>${strName!}</#if></div>
</#macro>