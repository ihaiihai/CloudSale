<%@ page language="java" import="java.util.*" pageEncoding="utf-8"
	contentType="text/html; charset=utf-8"%>
<%@taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <meta charset="utf-8" />
    <title>${title}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="${baseurl}/assets/images/favicon.ico">

    <!-- third party css -->
    <link href="${baseurl}/assets/css/vendor/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="${baseurl}/res/Content/scripts/plugins/tree/tree.css" rel="stylesheet" />
	<link href="${baseurl}/res/Content/scripts/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="${baseurl}/res/Content/scripts/plugins/jqgrid/jqgrid.css" rel="stylesheet" />
	<link href="${baseurl}/assets/vendor/vue-treeselect/vue-treeselect.min.css" rel="stylesheet" type="text/css" />
    <link href="${baseurl}/assets/vendor/vue-tree/vue-tree.css" rel="stylesheet" type="text/css" />
    <link href="${baseurl}/assets/css/vendor/glyphicon.css" rel="stylesheet" type="text/css" />
    <link href="${baseurl}/assets/vendor/bootstrap/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="${baseurl}/assets/vendor/bootstrap/daterangepicker.css" rel="stylesheet" type="text/css" />
    <!-- third party css end -->

    <!-- App css -->
    <link href="${baseurl}/assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <link href="${baseurl}/assets/css/app.min.css" rel="stylesheet" type="text/css" />
    <link href="${baseurl}/assets/css/style.css" rel="stylesheet" type="text/css" />
    
    <!-- third party js -->
    <script src="${baseurl}/assets/js/vendor/vue.js"></script>
    <script src="${baseurl}/assets/js/vendor/jquery-2.1.1.js"></script>
    <script src="${baseurl}/assets/vendor/vue-treeselect/vue-treeselect.min.js"></script>
    <script src="${baseurl}/assets/vendor/vue-tree/vue-tree.js"></script>
    <!-- third party js ends -->
    
    <script>
    var baseurl = '${baseurl}';
    var timeout = '${timeout}';
    if(timeout == '1'){
    	top.logout();
    }
    </script>

    <!-- bundle -->
    <script src="${baseurl}/assets/js/app.min.js"></script>

    <!-- third party js -->
    <script src="${baseurl}/assets/vendor/echart/echarts.js"></script>
    <script src="${baseurl}/assets/vendor/echart/theme.js"></script>
    
    <script src="${baseurl}/assets/js/vendor/jquery.cookie.js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/echarts/CloudSale_zt.js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/echarts/MyEcharts.js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/echarts/echarts-wordcloud.js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/jqgrid/jqgrid.js"></script>
    
	<script src="${baseurl}/res/Content/scripts/plugins/dialog/dialog.js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/colorbox/jquery.colorbox-min.js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/cookie/jquery.cookie.js"></script>
	
	<script src="${baseurl}/res/Content/scripts/plugins/jqgrid/grid.locale-en.js"></script>
	<script src="${baseurl}/res/Content/scripts/utils/cloudSale-ui.js"></script>
	<script src="${baseurl}/res/Content/CloudSale/init/Public_Js.js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/loading/js/loading.js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/printTable/jquery.printTable.js"></script>
	
	<script src="${baseurl}/res/Content/scripts/plugins/tree/treeCloudsale.js"></script>
	<script src="${baseurl}/assets/vendor/bootstrap/bootstrap-datetimepicker.min.js"></script>
    <script src="${baseurl}/assets/vendor/bootstrap/bootstrap-datetimepicker.zh-CN.js"></script>
    <script src="${baseurl}/assets/vendor/bootstrap/daterangepicker.js"></script>
    <!-- third party js ends -->
    
    <script src="${baseurl}/assets/js/common/daterangepicker.js"></script>
    <script src="${baseurl}/assets/js/utils/common.js"></script>
    <script src="${baseurl}/assets/js/utils/long.js"></script>
    
    <style>
    body{
    	padding-bottom:0px;
    }
	[v-cloak] {
		display: none !important;
	}
	</style>
	
</head>
<body>
	<decorator:body />
	<div class="modal fade" id="loadingModal">
	  <div class="modal-dialog modal-sm" style="height:100%;">
		  <div class="loader">
			  <div class="dot"></div>
			  <div class="dot"></div>
			  <div class="dot"></div>
			  <div class="dot"></div>
			  <div class="dot"></div>
		  </div>
	  </div><!-- /.modal-dialog -->
	</div><!-- /.modal -->
</body>