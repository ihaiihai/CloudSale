<%@ page language="java" import="java.util.*" pageEncoding="utf-8"
	contentType="text/html; charset=utf-8"%>
<%@taglib uri="http://www.opensymphony.com/sitemesh/decorator"
	prefix="decorator"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" " http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<link rel="icon" href="${baseurl}/res/common/images/favicon.ico">
	<title>管理端</title>

	<!--框架必需start-->
    <script src="${baseurl}/res/Content/scripts/jquery/jquery-2.1.1.js"></script>
    <link href="${baseurl}/res/Content/styles/font-awesome.min.css" rel="stylesheet" />
    <link href="${baseurl}/res/Content/scripts/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
    <script src="${baseurl}/res/Content/scripts/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!--框架必需end-->
    <!--bootstrap组件start-->
    <link href="${baseurl}/res/Content/scripts/bootstrap/bootstrap.min.css" rel="stylesheet" />
    <link href="${baseurl}/res/Content/scripts/bootstrap/bootstrap.extension.css" rel="stylesheet" />
    <script src="${baseurl}/res/Content/scripts/bootstrap/bootstrap.min.js"></script>
    <!--bootstrap组件end-->
    
    <script src="${baseurl}/res/Content/scripts/plugins/datepicker/WdatePicker.js"></script>

    <link href="${baseurl}/res/Content/scripts/plugins/tree/css" rel="stylesheet"/>
	<link href="${baseurl}/res/Content/scripts/plugins/datetime/css" rel="stylesheet"/>
	<link href="${baseurl}/res/Content/scripts/plugins/wizard/css" rel="stylesheet"/>
	<link href="${baseurl}/res/Content/styles/cloudSale-ui.css" rel="stylesheet"/>

    <script src="${baseurl}/res/Content/scripts/plugins/tree/js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/validator/js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/wizard/js"></script>
	<script src="${baseurl}/res/Content/scripts/plugins/datepicker/js"></script>
	<script src="${baseurl}/res/Content/scripts/utils/js"></script>
    
    <!--工作流设计器依赖start-->

    <link href="${baseurl}/res/Content/scripts/plugins/simditor/css/simditor.css" rel="stylesheet" />
    <link href="${baseurl}/res/Content/scripts/plugins/cxColor/css/jquery.cxcolor.css" rel="stylesheet" />
    <link href="${baseurl}/res/Content/scripts/plugins/uploadify/uploadify.css" rel="stylesheet" />
    <link href="${baseurl}/res/Content/scripts/plugins/uploadify/uploadify.extension.css" rel="stylesheet" />
    <link href="${baseurl}/res/Content/scripts/plugins/flow-ui/flow.css" rel="stylesheet" />

    <script src="${baseurl}/res/Content/scripts/plugins/simditor/js/module.min.js"></script>
    <script src="${baseurl}/res/Content/scripts/plugins/simditor/js/uploader.min.js"></script>
    <script src="${baseurl}/res/Content/scripts/plugins/simditor/js/hotkeys.min.js"></script>
    <script src="${baseurl}/res/Content/scripts/plugins/simditor/js/simditor.min.js"></script>
    <script src="${baseurl}/res/Content/scripts/plugins/cxColor/js/jquery.cxcolor.js"></script>
    <script src="${baseurl}/res/Content/scripts/plugins/uploadify/jquery.uploadify.min.js"></script>
    <link href="${baseurl}/res/Content/styles/cloudSale-flowall.css" rel="stylesheet"/>

    <script src="${baseurl}/res/Content/scripts/flow/js"></script>

    <!--工作流设计器依赖end-->
    
</head>
<body style="overflow: hidden;">
	<!-- Main section-->
	<section style="width:100%;height:100%;"> <!-- Page content--> <decorator:body /> </section>
</body>