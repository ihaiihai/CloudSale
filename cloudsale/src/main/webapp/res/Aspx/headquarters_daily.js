var pageVM = new Vue({
	el : '#page',
	data : {
		data1: [{"门店":"003001","实销":8,"同期实销":69},{"门店":"002801","实销":60,"同期实销":1305},{"门店":"001401","实销":98,"同期实销":3260},{"门店":"002301","实销":99,"同期实销":519},{"门店":"003201","实销":173,"同期实销":null},{"门店":"003301","实销":265,"同期实销":326},{"门店":"001501","实销":715,"同期实销":4359},{"门店":"001901","实销":722,"同期实销":4104},{"门店":"002901","实销":1007,"同期实销":663},{"门店":"001701","实销":1044,"同期实销":7743},{"门店":"001601","实销":1333,"同期实销":5322},{"门店":"000701","实销":1619,"同期实销":2086},{"门店":"001301","实销":1711,"同期实销":1709},{"门店":"002601","实销":2291,"同期实销":4327},{"门店":"003401","实销":2811,"同期实销":2831},{"门店":"000801","实销":2885,"同期实销":9962},{"门店":"003101","实销":3118,"同期实销":3935},{"门店":"000301","实销":3630,"同期实销":10366},{"门店":"001101","实销":4459,"同期实销":6196},{"门店":"000401","实销":5092,"同期实销":6090},{"门店":"000501","实销":6236,"同期实销":12412},{"门店":"003501","实销":7725,"同期实销":5215},{"门店":"000201","实销":10281,"同期实销":4165},{"门店":"000601","实销":10312,"同期实销":26257},{"门店":"000901","实销":28027,"同期实销":32856}],
		data2: [{"昨日销售":95721,"月累计销售":4111026,"完成率":68.18}],
		data3: [{"时间":"10","昨日时段销售额":7537,"同期时段销售额":12134},{"时间":"11","昨日时段销售额":7768,"同期时段销售额":18099},{"时间":"12","昨日时段销售额":6867,"同期时段销售额":10203},{"时间":"13","昨日时段销售额":3469,"同期时段销售额":9708},{"时间":"14","昨日时段销售额":4840,"同期时段销售额":11983},{"时间":"15","昨日时段销售额":10455,"同期时段销售额":16009},{"时间":"16","昨日时段销售额":11323,"同期时段销售额":17854},{"时间":"17","昨日时段销售额":10731,"同期时段销售额":13927},{"时间":"18","昨日时段销售额":6728,"同期时段销售额":13250},{"时间":"19","昨日时段销售额":6412,"同期时段销售额":17596},{"时间":"20","昨日时段销售额":14098,"同期时段销售额":12535},{"时间":"21","昨日时段销售额":2,"同期时段销售额":-34},{"时间":"22","昨日时段销售额":0,"同期时段销售额":-978},{"时间":"7","昨日时段销售额":0,"同期时段销售额":234},{"时间":"8","昨日时段销售额":1321,"同期时段销售额":1304},{"时间":"9","昨日时段销售额":4170,"同期时段销售额":12403}],
		data4: [{"平均退货率":10.204300,"平均折扣率":66.888500,"客单价":146.586584,"连带率":4.842266}],
		data5: [{"name":"食品","value":57976},{"name":null,"value":14706},{"name":"纸尿裤","value":7844},{"name":"洗护用品","value":5422},{"name":"床品内衣宝宝套","value":2427},{"name":"婴童装外出服","value":2068},{"name":"喂养用品","value":2025},{"name":"鞋帽袜散货件","value":1315},{"name":"游泳馆","value":1293},{"name":"孕产妇类","value":564},{"name":"玩具","value":142}],
		data6: [{"营业员编号":"203","姓名":"张强","销售金额":8933.4700,"客单价":595.564666,"连带率":5.066666},{"营业员编号":"1101","姓名":"叶芳","销售金额":4459.3000,"客单价":139.353125,"连带率":2.437500},{"营业员编号":"407","姓名":"陈声波","销售金额":4082.9500,"客单价":131.708064,"连带率":3.967741},{"营业员编号":"904","姓名":"刘志忠","销售金额":3897.3000,"客单价":299.792307,"连带率":8.307692},{"营业员编号":"909","姓名":"马春秀","销售金额":3850.7000,"客单价":154.028000,"连带率":10.800000},{"营业员编号":"619","姓名":"雷湘军","销售金额":3424.0000,"客单价":190.222222,"连带率":3.611111},{"营业员编号":"918","姓名":"于鸿伟","销售金额":3346.4500,"客单价":278.870833,"连带率":9.333333},{"营业员编号":"914","姓名":"肖文飚","销售金额":3284.7000,"客单价":234.621428,"连带率":4.928571},{"营业员编号":"3601","姓名":"杨靖","销售金额":3063.3000,"客单价":133.186956,"连带率":1.913043},{"营业员编号":"505","姓名":"刘倍菡","销售金额":3052.8000,"客单价":254.400000,"连带率":4.500000},{"营业员编号":"503","姓名":"史银红","销售金额":2963.6000,"客单价":123.483333,"连带率":2.666666},{"营业员编号":"3901","姓名":"廖晨光","销售金额":2810.5800,"客单价":255.507272,"连带率":4.090909},{"营业员编号":"931","姓名":"侯慧娟","销售金额":2712.3700,"客单价":193.740714,"连带率":15.285714},{"营业员编号":"933","姓名":"姚艳霞","销售金额":2359.3000,"客单价":131.072222,"连带率":9.166666},{"营业员编号":"2101","姓名":"黄晋","销售金额":2291.3000,"客单价":99.621739,"连带率":2.739130},{"营业员编号":"937","姓名":"王长旭","销售金额":2175.5000,"客单价":241.722222,"连带率":9.222222},{"营业员编号":"943","姓名":"马瑞","销售金额":2025.8000,"客单价":112.544444,"连带率":7.888888},{"营业员编号":"602","姓名":"刘洁筱","销售金额":1842.0100,"客单价":460.502500,"连带率":4.000000},{"营业员编号":"1301","姓名":"幸大鹏","销售金额":1711.0000,"客单价":142.583333,"连带率":2.833333},{"营业员编号":"805","姓名":"石卫平","销售金额":1656.8500,"客单价":87.202631,"连带率":2.736842},{"营业员编号":"630","姓名":"文燕","销售金额":1614.5000,"客单价":161.450000,"连带率":3.500000},{"营业员编号":"806","姓名":"王思彤","销售金额":1584.0700,"客单价":88.003888,"连带率":2.555555},{"营业员编号":"302","姓名":"李群仙","销售金额":1451.1000,"客单价":103.650000,"连带率":2.714285},{"营业员编号":"921","姓名":"周国强","销售金额":1364.8000,"客单价":151.644444,"连带率":11.333333},{"营业员编号":"604","姓名":"陶翔华","销售金额":1314.8000,"客单价":109.566666,"连带率":5.166666},{"营业员编号":"920","姓名":"徐敏","销售金额":1212.9000,"客单价":151.612500,"连带率":12.250000},{"营业员编号":"202","姓名":"李向东","销售金额":1170.0000,"客单价":585.000000,"连带率":103.500000},{"营业员编号":"616","姓名":"柳青","销售金额":1013.0500,"客单价":126.631250,"连带率":2.000000},{"营业员编号":"2701","姓名":"张长杰","销售金额":1007.0000,"客单价":167.833333,"连带率":3.000000},{"营业员编号":"951","姓名":"赖莉","销售金额":781.2000,"客单价":78.120000,"连带率":11.900000},{"营业员编号":"1605","姓名":"左嘉","销售金额":780.8000,"客单价":55.771428,"连带率":1.285714},{"营业员编号":"1901","姓名":"潘俊武","销售金额":722.0000,"客单价":60.166666,"连带率":1.416666},{"营业员编号":"406","姓名":"谢纯德","销售金额":629.0000,"客单价":314.500000,"连带率":4.000000},{"营业员编号":"1502","姓名":"朱庆中","销售金额":536.5000,"客单价":107.300000,"连带率":4.400000},{"营业员编号":"632","姓名":"刘新中","销售金额":475.3000,"客单价":118.825000,"连带率":1.750000},{"营业员编号":"405","姓名":"乐开诚","销售金额":400.0000,"客单价":400.000000,"连带率":6.000000},{"营业员编号":"701","姓名":"高舒","销售金额":350.8700,"客单价":35.087000,"连带率":1.900000},{"营业员编号":"606","姓名":"庄葛巍","销售金额":328.0000,"客单价":109.333333,"连带率":1.666666},{"营业员编号":"3802","姓名":"赵静静","销售金额":265.0000,"客单价":132.500000,"连带率":5.000000},{"营业员编号":"501","姓名":"徐友筠","销售金额":220.0000,"客单价":31.428571,"连带率":1.428571},{"营业员编号":"1609","姓名":"欧晓东","销售金额":206.0000,"客单价":25.750000,"连带率":1.000000},{"营业员编号":"1501","姓名":"党明惠","销售金额":178.8000,"客单价":59.600000,"连带率":1.000000},{"营业员编号":"206","姓名":"田祥","销售金额":177.3000,"客单价":59.100000,"连带率":3.000000},{"营业员编号":"3201","姓名":"郝玉杰","销售金额":173.0000,"客单价":86.500000,"连带率":2.500000},{"营业员编号":"1601","姓名":"李铮","销售金额":150.0000,"客单价":75.000000,"连带率":2.000000},{"营业员编号":"304","姓名":"宣峰","销售金额":131.1000,"客单价":43.700000,"连带率":3.000000},{"营业员编号":"2011","姓名":"杜春","销售金额":99.0000,"客单价":99.000000,"连带率":8.000000},{"营业员编号":"3501","姓名":"郑旭东","销售金额":61.0000,"客单价":30.500000,"连带率":2.500000},{"营业员编号":"2601","姓名":"邓树清","销售金额":60.0000,"客单价":60.000000,"连带率":3.000000},{"营业员编号":"3602","姓名":"赵良超","销售金额":55.0000,"客单价":55.000000,"连带率":1.000000},{"营业员编号":"2501","姓名":"杨克军","销售金额":8.0000,"客单价":8.000000,"连带率":1.000000},{"营业员编号":"504","姓名":"陶永华","销售金额":0.0000,"客单价":0.000000,"连带率":1.000000},{"营业员编号":"801","姓名":"舒开旗","销售金额":0.0000,"客单价":0.000000,"连带率":1.000000},{"营业员编号":"910","姓名":"梁晓惠","销售金额":0.0000,"客单价":0.000000,"连带率":0.000000},{"营业员编号":"401","姓名":"陈澍","销售金额":-20.0000,"客单价":-20.000000,"连带率":-1.000000},{"营业员编号":"935","姓名":"朱贤申","销售金额":-89.0000,"客单价":-89.000000,"连带率":-1.000000},{"营业员编号":"803","姓名":"郑振洲","销售金额":-355.8000,"客单价":-118.600000,"连带率":-1.000000}],
		data7: [{"标题1":"【2018-11-26】销售共计：￥95721"}],
		data8: [{"标题2":"昨日整体客流【653人】,同比去年下降41.01%,客单价【146.6元/人】,同比去年下降2.4%"}],
		data9: [{"name":"本月任务：￥6030000,目前已完成￥4111026,完成率：68.18%,预测完成率：75.76%"}]
	},
	mounted: function(){
        this.init();
    },
	methods : {
		init: function(){
        	var me = this;
        	setTimeout(function(){
        		me.create_C_01();
        		me.create_C_02();
        		me.create_C_03();
        		me.create_C_04();
        		me.create_C_05();
        		me.create_C_06();
        		me.create_C_07();
        		me.create_C_08();
        		me.create_grid();
        	},1000);
        },
        create_C_01: function(){
        	var sql_str = 'company_daily 1';
            var col_name = "门店~实销~同期实销";
            var data = this.data1;
            option = {
                title: {
                    text: '●各门店销售完成状况(点击可看详情)',
                    x: 'left'
                },
                tooltip: {
                    show: true,
                    formatter: "{b}<br />{a}: {c}元",
                },
                toolbox: {
                    feature: {
                        restore: {
                            show: true
                        }
                    },
                    top: '4%'
                },
                legend: {
                    data: ['昨日实销', '同期实销'],
                    top: '4%'
                },
                grid: {
                    left: '2%',
                    right: '2%',
                    bottom: '0%',
                    containLabel: true
                },
                xAxis: {
                    show: false,
                    type: 'value',
                    boundaryGap: [0, 0.2]
                },
                yAxis: {
                    type: 'category',
                    data: public_data.getEchartsData(data, '门店', 'branch'),
                    position: 'right',
                    splitArea: {
                        show: true
                    },
                },
                series: [
                {
                    name: '昨日实销',
                    type: 'bar',
                    label: {
                        normal: {
                            show: true,
                            position: 'right',
                            formatter: '￥{c}'
                        }

                    },
                    data: public_data.getEchartsData(data, '实销', ''),
                    markLine: {
                        label: {
                            normal: {
                                show: false,
                                position: 'end',
                                formatter: '{b}{c}%'
                            }
                        },
                        lineStyle: {
                            normal: {
                                color: 'blue'
                            }
                        },
                        symbol: 'circle',
                        data: [
                            { name: '平均值', type: 'average' }
                        ]
                    }
                },
                {
                    name: '同期实销',
                    type: 'bar',
                    label: {
                        normal: {
                            show: true,
                            position: 'right',
                            formatter: '￥{c}'
                        }
                    },
                    data: public_data.getEchartsData(data, '同期实销', '')
                }
                ]
            };
          
            var myChart = echarts.init(document.getElementById("C_01"), 'CloudSale');
            myChart.setOption(option, true);
            myChart.on('click', function (params) {
                if (params.name.indexOf("均值") == -1) {
                    myColorbox({ overlayClose: true, href: '/CloudSale/Report_PC/web_page/Aspx_XZ/headquarters_daily_trip.aspx?idid=' + public_data.Name(params.name, 'branch') + "&cmd=" + ym_name + id , width: '70%', height: '60%', iframe: true })
                }
            });
        },
        create_C_02:function(){
        	var sql_str = 'company_daily 2';
            var col_name = "昨日销售~月累计销售~完成率";
            var data = this.data2;
            var option = {
                title: {
                    text: (top.authorityFlag == 1?"全公司":top.branchName) + public_data.getEchartsData(this.data7, '标题1', '') + '',
                    subtext: '' + public_data.getEchartsData(this.data9, 'name', '') + '',
                    x: 'center',
                    top: '10%',
                    textStyle: {
                        fontSize: 22
                    }
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "月累计销售：" + data[0].月累计销售 + "元，完成率：" + data[0].完成率 + "%"
                },
                grid: {
                    top: '55%',
                    left: '0%',
                    right: '0%',
                    bottom: '5%',
                    containLabel: true
                },
                xAxis: {
                    max: 100,
                    axisLabel: {
                        show: true,
                        formatter: "{value}%",
                    },
                    axisLine: {
                        show: false
                    }

                },
                yAxis: [{
                    show: false,
                    data: ['xx'],
                    axisLine: {
                        show: false
                    }
                }, {
                    // 辅助 x 轴
                    show: false,
                    data: [100]
                }],

                series: [{
                    // 辅助系列
                    type: 'bar',
                    silent: true,
                    yAxisIndex: 1,
                    itemStyle: {
                        normal: {
                            barBorderRadius: 120,
                            color: '#ddd'
                        }
                    },
                    barWidth: 15,
                    data: [100]
                }, {
                    type: 'bar',
                    data: [fixNum(data[0].完成率,2)],
                    markPoint: {
                        symbol: 'roundRect',
                        symbolSize: 40,
                        data: [{
                            type: 'max'
                        }],
                        label: {
                            normal: {
                                formatter: '{c}%',
                                show: true,
                            }
                        }
                    },
                    barWidth: 15,
                    itemStyle: {
                        normal: {
                            barBorderRadius: 180,
                            //color: '#59c4e6',#7b93e0
                            color: '#7b93e0'
                        }
                    }
                }]
            };
            //var isDrill = true; //是否下钻
           // var isDrill_url = "myColorbox({overlayClose:true, href:'/CloudSale/Report_PC/web_page/Aspx_XZ/headquarters_daily_trip.aspx?idid='+public_data.Name(params.name,'branch')+'&cmd=" + ym_name + id + "', width:'70%', height:'60%', iframe: true})"
            //alert(isDrill_url);
            ECharts.Draw(option, "C_02");
            // return $.extend({}, option);
        },
        create_C_03:function(){
        	var sql_str = 'company_daily 3';
            var col_name = "时间~昨日时段销售额~同期时段销售额";
            var data = this.data3;
            //data:数据格式：{name：xxx,group:xxx,value:xxx}...
            //var bars_dates = ECharts.ChartDataFormate.FormateGroupData(data, 'bar', is_stack)
            option = {
                title: {
                    text: '' + public_data.getEchartsData(this.data8, '标题2', '') + '',
                    x: 'center',
                    backgroundColor: '#f5f5f5',
                    padding: [5, 10000],
                    textStyle: {
                        fontSize: 14
                    }
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                    data: ['昨日时段销售额', '同期时段销售额'],
                    top: '7%'
                },
                xAxis: [
                {
                    type: 'category',
                    data: public_data.getEchartsData(data, '时间', ''),
                    axisPointer: {
                        type: 'shadow'
                    }
                }
                ],
                yAxis: [
                 {
                     type: 'value',
                     name: '成交额',
                     axisLabel: {
                         formatter: '{value}元'
                     }
                 }
                ],
                series: [{
                    name: '昨日时段销售额',
                    type: 'line',
                    symbolSize: 2,
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                color: "rgba(123, 147, 224, 0.2)",
                                type: "default"
                            },
                            lineStyle: {
                                width: 1
                            }
                        }
                    },
                    smooth: true,
                    data: public_data.getEchartsData(data, '昨日时段销售额', ''),
                },
                    {
                        name: '同期时段销售额',
                        type: 'line',
                        smooth: true,
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(89, 196, 230, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        data: public_data.getEchartsData(data, '同期时段销售额', ''),
                    }
                ]
            };
            var isDrill = true; //是否下钻
            ECharts.Draw(option, 'C_03');
        },
        create_C_04: function(){
        	var hl = this.data4;
        	option = null;
            // 初始 option
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: '平均退货率',
                    type: 'pie',
                    radius: [
                    '70%',
                    '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: hl[0].平均退货率,
                        name: '平均退货率',

                        label: {
                            normal: {
                                formatter: '{d} %',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: 100 - (hl[0].平均退货率),
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n平均退货率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
            ECharts.Draw(option, 'C_04');
        },
        create_C_05: function(){
        	var hl = this.data4;
        	option = null;
            // 初始 option
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: '平均折扣率',
                    type: 'pie',
                    radius: [
                    '70%',
                    '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: hl[0].平均折扣率,
                        name: '平均折扣率',

                        label: {
                            normal: {
                                formatter: '{d}%',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: 100 - hl[0].平均折扣率,
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n平均折扣率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
            ECharts.Draw(option, 'C_05');
        },
        create_C_06: function(){
        	var hl = this.data4;
        	option = null;
            // 初始 option
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: '客单价',
                    type: 'pie',
                    radius: [
                    '70%',
                    '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: fixNum(hl[0].客单价,2),
                        name: '客单价',

                        label: {
                            normal: {
                                formatter: '{c}',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: (hl[0].客单价 < 500 ? 500 : hl[0].客单价 * 1.2) - (hl[0].客单价),
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n客单价',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
            ECharts.Draw(option, 'C_06');
        },
        create_C_07: function(){
        	var hl = this.data4;
        	option = null;
            // 初始 option
            option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: '连带率',
                    type: 'pie',
                    radius: [
                    '70%',
                    '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: fixNum(hl[0].连带率,2),
                        name: '连带率',

                        label: {
                            normal: {
                                formatter: '{c}',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: (hl[0].连带率 < 10 ? 10 : hl[0].连带率 * 1.1) - (hl[0].连带率),
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n连带率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
            ECharts.Draw(option, 'C_07');
        },
        create_C_08: function(){
        	data = this.data5;            
            //data:数据格式：{name：xxx,group:xxx,value:xxx}...
            //var bars_dates = ECharts.ChartDataFormate.FormateGroupData(data, 'bar', is_stack);
            var option = {
                title: {
                    top: '2%',
                    text: "●各类别销售分布",
                    x: 'left'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                grid: {
                    top: '20%'
                },
                series: [
                    {
                        name: '销售分布',
                        type: 'pie',
                        radius: ['55%', '80%'],
                        center: ['50%', '55%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '25',
                                    fontWeight: 'normal'
                                },
                                formatter: "{b}\n{c}({d}%)"
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: data
                    }
                ]
            };

            var dom = document.getElementById("C_08");
            var myChart = echarts.init(dom, 'CloudSale');
            var data = this.data5;
            var app = {};
            var option = {
                title: {
                    top: '2%',
                    text: "●各类别销售分布",
                    x: 'left'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                grid: {
                    top: '20%'
                },
                series: [
                    {
                        name: '销售分布',
                        type: 'pie',
                        radius: ['55%', '80%'],
                        center: ['50%', '55%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '25',
                                    fontWeight: 'normal'
                                },
                                formatter: "{b}\n{c}"
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: data
                    }
                ]
            };
            var app = {};
            app.currentIndex = -1;
            showChart = setInterval(function () {
                var dataLen = option.series[0].data.length;
                // 取消之前高亮的图形
                myChart.dispatchAction({
                    type: 'downplay',
                    seriesIndex: 0,
                    dataIndex: app.currentIndex
                });
                app.currentIndex = (app.currentIndex + 1) % dataLen;
                // 高亮当前图形

                myChart.dispatchAction({
                    type: 'highlight',
                    seriesIndex: 0,
                    dataIndex: app.currentIndex
                });
                pieIndex = app.currentIndex
            }, 1000)
            myChart.setOption(option, true);
        },
        create_grid: function(){
            var $gridTable = $("#gridPanel_1");
            $gridTable.jqGrid({
                datatype: "local",
                height: document.body.scrollHeight * 0.5-10,
                colModel: [
                    { name: "营业员编号", label: "营业员编号", align: "center", sortable: true, sorttype: 'float' },
                    { name: "姓名", label: "姓名", align: "center", sortable: true },
                    $.extend({ name: "销售金额", label: "销售金额", align: "center", sortable: true, sorttype: 'float' }, jqFormatter.RMB0),
                    $.extend({ name: "客单价", label: "客单价", align: "center", sortable: true, sorttype: 'float' }, jqFormatter.RMB0),
                    $.extend({ name: "连带率", label: "连带率", align: "center", sortable: true, sorttype: 'float' }, jqFormatter.Num2)
                ],
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                width: document.body.scrollWidth * 0.25 - 5,
                rowNum: 1000,
                loadonce: true,
                gridComplete: function () {
                    condition.fontColor_ABC($(this).attr("id"), "客单价", 300, 200, 100);
                    condition.backColor_ABC($(this).attr("id"), "连带率", 7,5, 3);
                }
            });
            rows = this.data6;
            for(var i=0;i<rows.length;i++){
        		$("#gridPanel_1").jqGrid('addRowData',i+1,rows[i]);
        	}
        }
	}
});