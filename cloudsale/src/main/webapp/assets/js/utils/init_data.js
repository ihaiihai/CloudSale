$.ajax({
	type : "GET",
	url : baseurl + "/queryAllBranch",
	dataType : "json",
	success : function(data) {
		window.branchData = data;
	}
});
$.ajax({
	type : "GET",
	url : baseurl + "/queryAllBrand",
	dataType : "json",
	success : function(data) {
		window.brandData = data;
	}
});
$.ajax({
	type : "GET",
	url : baseurl + "/queryAllCls",
	dataType : "json",
	success : function(data) {
		window.clsData = data;
	}
});
$.ajax({
	type : "GET",
	url : baseurl + "/queryAllSaler",
	dataType : "json",
	success : function(data) {
		window.salerData = data;
	}
});
$.ajax({
	type : "GET",
	url : baseurl + "/queryBranchOptions",
	dataType : "json",
	success : function(data) {
		window.branchOptionData = data;
	}
});
$.ajax({
	type : "GET",
	url : baseurl + "/queryClsOptions",
	dataType : "json",
	success : function(data) {
		window.clsOptionData = data;
	}
});
$.ajax({
	type : "GET",
	url : baseurl + "/queryBrandOptions",
	dataType : "json",
	success : function(data) {
		window.brandOptionData = data;
	}
});
$.ajax({
	type : "GET",
	url : baseurl + "/querySalerOptions",
	dataType : "json",
	success : function(data) {
		window.salerOptionData = data;
	}
});
window.InitData = {
	getBranchName: function(code){
		if(!code)return '';
		return window.branchData[code] ? window.branchData[code]['branch_name'] : code;
	},
	getBrandName: function(code){
		if(!code)return '';
		return window.brandData[code] ? window.brandData[code]['item_brandname'] : code;
	},
	getClsName: function(code){
		if(!code)return '';
		return window.clsData[code] ? window.clsData[code]['item_clsname'] : code;
	},
	getSalerName: function(code){
		if(!code)return '';
		return window.salerData[code] ? window.salerData[code]['sale_name'] : code;
	},
	getChartData: function(arr, field, type){
		var result = [];
		if(type == 'branch'){
			for(var obj in arr){
				result.push(window.branchData[obj[field]]['branch_name']);
			}
		}else if(type == 'brand'){
			for(var obj in arr){
				result.push(window.brandData[obj[field]]['item_brandname']);
			}
		}else{
			for(var obj in arr){
				result.push(obj[field]);
			}
		}
		return result;
	}
}