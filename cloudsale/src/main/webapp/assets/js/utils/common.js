window.showLoading = function (){
	setTimeout(function(){
		$('#loadingModal').modal({backdrop:'static',keyboard:false});
	});
}
window.hideLoading = function (){
	setTimeout(function(){
		$('#loadingModal').modal('hide');
	},500);
}
