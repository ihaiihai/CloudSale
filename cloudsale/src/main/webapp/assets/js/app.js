var menu = new Vue({
	el : '#menu',
	data : {
		menuData: {},
		menuExpandState: {}
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.initModal();
				me.loadMenuExpandState();
				me.loadMenuData();
			});
		},
		clickMenuItem: function(){
			var event = arguments[0];
			var pages = Array.prototype.slice.call(arguments,1);
			var page = pages[pages.length - 1];
			if(page.url){
				$('#external-frame').attr('src',baseurl + page.url);
				pageTitleVm.pages = pages;
			}else{
				if($('.left-side-menu').width() == 250){
					$(event.currentTarget).next().toggle();
					$(event.currentTarget).parent().toggleClass('active');
					if(this.menuExpandState[page.moduleId]){
						this.menuExpandState[page.moduleId] = false;
					}else{
						this.menuExpandState[page.moduleId] = true;
					}
					$.cookie('menuExpandState', JSON.stringify(this.menuExpandState));
				}
			}
		},
		initEvent: function(){
			$('#button-menu').click(function(){
				$('.side-nav li').removeClass('active');
				$('.side-nav-second-level').hide();
				$('.side-nav-third-level').hide();
				setTimeout(function(){
					$.cookie('menuWidth',$('.left-side-menu').width());
				},200);
			});
			$(window).resize(function(){
				if($('.left-side-menu').width() == 70){
					$('.side-nav-second-level').hide();
					$('.side-nav-third-level').hide();
				}
			});
		},
		initModal: function(){
			window.addModal = function(modal){
				if(modal && $('#modalCon').find('#'+modal.id).length > 0){
					$('#modalCon').find('#'+modal.id).remove();
				}
				$('#modalCon').append(modal);
				return $('#modalCon').find('#'+modal.id);
			}
			window.showModal = function(id){
				$('#'+id).modal('show');
			}
			window.hideModal = function(id){
				$('#'+id).modal('hide');
			}
			$(document).ready(function () {
		    // 通过该方法来为每次弹出的模态框设置最新的zIndex值，从而使最新的modal显示在最前面
		        $(document).on('show.bs.modal', '.modal', function (event) {
		            var zIndex = 1040 + (10 * $('.modal:visible').length);
		            $(this).css('z-index', zIndex);
		             setTimeout(function() {
		                 $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
		             }, 0);
		        });
		    });
		},
		loadMenuExpandState: function(){
			var menuWidth = $.cookie('menuWidth');
			if(menuWidth == 70){
				$('body').addClass('enlarged');
				$('.side-nav-second-level').hide();
				$('.side-nav-third-level').hide();
			}else{
				if($('.left-side-menu').width() == 250){
					var menuExpandState = $.cookie('menuExpandState');
					if(menuExpandState){
						this.menuExpandState = JSON.parse(menuExpandState);
					}
				}
			}
		},
		loadMenuData : function() {
			var me = this;
			$.ajax({
				type : "GET",
				url : baseurl + "/sys/module/queryAll",
				dataType : "json",
				success : function(data) {
					me.menuData = me.formatMenuData(data);
				}
			});
		},
		formatMenuData : function(data) {
			var menuData = {};
			for (var i = 0, len = data.length; i < len; i++) {
				if (!menuData[data[i].parentId]) {
					menuData[data[i].parentId] = [];
				}
				menuData[data[i].parentId].push(data[i]);
			}
			return menuData;
		}
	}
});
var pageTitleVm = new Vue({
	el : '#pageTitle',
	data: {
		pages: [{moduleName:'首页'}]
	}
});
var helpBox = new Vue({
	el : '#helpBox',
	methods : {
		closeBox: function(){
			$('.help-box').hide();
		}
	}
});
var navUser = new Vue({
	el : '#navUser',
	data : {
		welcome : '',
		date: ''
	},
	mounted : function() {
		var me = this;
		var now,hour;
		this.timer = setInterval(function(){
			now = moment();
			hour = now.hour();
			if(hour >= 0 && hour < 12){
				me.welcome = '上午好';
			}else if(hour >= 12 && hour < 18){
				me.welcome = '下午好';
			}else if(hour >= 18 && hour < 24){
				me.welcome = '晚上好';
			}
			me.date = now.format('YYYY-MM-DD HH:mm:ss');
		},1000);
	},
	methods : {
		logOut: function(){
			window.location = baseurl + "/login";
		}
	},
	beforeDestroy: function(){
		clearInterval(this.timer);
	}
});
window.chartModal = new Vue({
    el: '#chartModal',
    data: {
        modalTitle: '',
        showLoading: false,
        noData:false
    },
    mounted: function(){
        this.init();
    },
    methods: {
        init: function(){

        },
        show: function(title){
        		if(title){
        			this.modalTitle = title;
        		}
        		this.showLoading = true;
        		this.noData = true;
        		window.showModal('chartModal');
        }
    }
});
window.gridModal = new Vue({
    el: '#gridModal',
    data: {
        modalTitle: '',
        showLoading: false,
        padding: 200,
        maxWidth:$(window).width() - 200,
        minHeight:$(window).height() - 200,
        gridType: 0
    },
    mounted: function(){
    	var me = this;
        this.init();
        $(window).resize(function (e) {
            window.setTimeout(function () {
                me.maxWidth = $(window).width() - me.padding;
                me.minHeight = $(window).height() - me.padding;
            }, 200);
            e.stopPropagation();
        });
    },
    methods: {
        init: function(){

        },
        show: function(){
        	this.maxWidth = $(window).width() - this.padding;
        	this.minHeight = $(window).height() - this.padding;
        	window.showModal('gridModal');
        }
    }
});