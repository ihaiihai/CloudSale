var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		areaCode: null,
		areaOptions:top.branchOptionData,
		year: moment().format('YYYY'),
		month: moment().format('MM'),
		maxCnt: 0,
		maxLen: 0
	},
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.getNewData(function(data){
					me.GetGrid(data);
				});
			});
		},
		initEvent: function(){
			var me = this;
			$('#date').datetimepicker({  
		        format: 'yyyy年mm月',  
		         autoclose: true,  
		         forceParse: false,
		         startView: 4,  
		         minView: 3,  
		         language: 'zh-CN'
		    }).val(moment().format('YYYY年MM月')).change(function(){
		    		me.year = moment($(this).val(), 'YYYY年MM月').format('YYYY');
		    		me.month = moment($(this).val(), 'YYYY年MM月').format('MM');
		    });
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 230);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		getNewData: function(callback) {
			var me = this;
			window.showLoading();
			$.ajax({
	            url: baseurl + "/task/saler/queryFollowUp?branchNos="+(me.areaCode?me.areaCode:"")+"&year="+me.year+"&month="+me.month,
	            type: "get",
	            dataType: "json",
	            success: function (data) {
	            		window.hideLoading();
	            		if(callback){
	            			callback(data);
	            		}
	            },
	            error: function(){
            			window.hideLoading();
	            }
        		});
        },
        GetGrid: function(jsonArr) {
        		var me = this;
        		var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var myArr = "门店~营业员~年度目标~今年完成~年进度~年任务预测~年完成率预测~本月任务~本月完成~月进度~剩余任务~剩余天数~本月任务预测~预测完成率".split('~');
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if (json.split("_").length > 1) {
                    colModel.push({ name: json, label: json.split("_")[2], align: "center", sortable: true, sorttype: 'float', width: 90 })
                } else {
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float', width: 90 })
                }
            if (json.indexOf("姓名") >= 0) {
                $.extend(colModel[i], { formatter: top.InitData.getSalerName, frozen: true })
                }else if (json.indexOf("门店")>= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getBranchName, frozen: true })
                } else if (json.indexOf("营业员") >= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getSalerName, frozen: true })
                } else if (json.indexOf("率") >= 0 || json.indexOf("进度") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                }
            }
            if(colModel.length == 0){
            	colModel.push({ name: "门店", label: "门店", width: 200 })
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 230,
                colModel: colModel,
                viewrecords: true,//定义是否要显示总记录数
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 5000,
                shrinkToFit: false,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                sumColArr: sumCol,
                sumName: "姓名_合计",
                avgColArr: avgCol,                
                grouping: true,
                groupingView: {
                    groupField: ['门店'],
                    groupSummary: [true],
                    groupColumnShow: [true],
                    groupText: ['<b>{0}各品类任务概况</b>'],
                    groupCollapse: false,
                },
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
	        	var _this = $("#" + gridID);
	            if ($(".btn.btn-sm.btn-white.active").attr("data-value") == 0) {
	                for (var obj in this.gridData[0]) {
	                    if (obj.indexOf("年进度") >= 0) {
	                        eval("sumColArr." + obj + "= fixNum((sumColArr.今年完成_" + obj.split("_")[1] + " / sumColArr.年度目标_" + obj.split("_")[1] + "),4)");
	                    }
	                    if (obj.indexOf("年完成率预测") >= 0) {
	                        eval("sumColArr." + obj + "= fixNum((sumColArr.年任务预测_" + obj.split("_")[1] + " / sumColArr.年度目标_" + obj.split("_")[1] + "),4)");
	                    }
	                    if (obj.indexOf("月进度") >= 0) {
	                        eval("sumColArr." + obj + "= fixNum((sumColArr.本月完成_" + obj.split("_")[1] + " / sumColArr.本月任务_" + obj.split("_")[1] + "),4)");
	                    }
	                    if (obj.indexOf("预测完成率") >= 0) {
	                        eval("sumColArr." + obj + "= fixNum((sumColArr.本月任务预测_" + obj.split("_")[1] + " / sumColArr.本月任务_" + obj.split("_")[1] + "),4)");
	                    }
	                }
	            } else {
	                sumColArr["年进度"] = sumColArr["今年完成"] / sumColArr["年度目标"]
	                sumColArr["年完成率预测"] = sumColArr["年任务预测"] / sumColArr["年度目标"]
	                sumColArr["月进度"] = sumColArr["本月完成"] / sumColArr["本月任务"]
	                sumColArr["预测完成率"] = sumColArr["本月任务预测"] / sumColArr["本月任务"]
	            }
	        _this.footerData("set", sumColArr);
	        condition.backColor_ABC("gridTable", "预测完成率", 95, 85,75)
	        condition.backColor_ABC("gridTable", "月进度", 95, 85, 75)
	        _this.find("[aria-describedby*='年度目标']").css("border-left", "1px dashed red")
	        $(".ui-jqgrid-hdiv").find("th[id*='年度目标']").css("border-left", "1px dashed red")
	        _this.find("[aria-describedby*='年完成率预测']").css("border-right", "1px dashed #1c84c6")
	        _this.find("[aria-describedby*='今年完成']").css("background-color", "#FCF2CA").css("font-weight", "700")
	        _this.find("[aria-describedby*='本月完成']").css("background-color", "#FCF2CA").css("font-weight", "700")
	        this.afterInit(this.gridData);
        },
        afterInit: function(jsonArr) {
        		var groupHeaders = [];

            if ($(".btn.btn-sm.btn-white.active").attr("data-value") == 0) {
                for (var obj in jsonArr[0]) {
                    if (obj.indexOf("年度目标") >= 0) {
                        groupHeaders.push({ startColumnName: obj, numberOfColumns: 12, titleText: "【" + jsonArr[0]['品牌'] + "】任务概况" })
                    }
                }
            $("#gridTable").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: groupHeaders
            });
            $("#gridTable").jqGrid('setFrozenColumns');
            $(".ui-th-column-header").addClass("column-header");
            $(".ui-th-column-header").css("border-left", "1px dashed red")
            }
        },
		search: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.getNewData(function(data){
				me.GetGrid(data);
			});
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
            var me = this;
            window.location.href = baseurl + "/Report_PC/dataExport/excel?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+me.year+'&end='+me.month+"&busi=salerTaskFollowUp";
        },
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.getNewData(function(data){
				me.GetGrid(data);
			});
		}
	}
});