var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		areaCode: null,
		areaOptions:top.branchOptionData,
		year: moment().format('YYYY'),
		setUpModal: null
	},
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.getNewData(function(data){
					me.loadGrid(data);
				});
				me.setUpModal = top.addModal($('#setUpModal')[0]);
			});
		},
		initEvent: function(){
			var me = this;
			$('#date').datetimepicker({  
		        format: 'yyyy年',  
		         autoclose: true,  
		         forceParse: false,
		         startView: 4,  
		         minView: 4,  
		         language: 'zh-CN'
		    }).val(moment().format('YYYY年')).change(function(){
		    		me.year = moment($(this).val(), 'YYYY年').format('YYYY');
		    });
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 230);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		getNewData: function(callback) {
			var me = this;
            window.showLoading();
        	$.ajax({
                url: baseurl + "/task/overall/querySetUp?branchNos="+(me.areaCode?me.areaCode:"")+"&year="+me.year,
                type: "get",
                dataType: "json",
                success: function (data) {
                	window.hideLoading();
	                if(callback){
	                	callback(data);
	                }
                },
                error: function(){
                	window.hideLoading();
                }
			});
        },
		loadGrid: function(jsonArr){
			var me = this;
			var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var myArr = "门店编号~门店名称~前一年销售~前一年占比~销售任务~业绩提升~月度目标_01~月度目标_02~月度目标_03~月度目标_04~月度目标_05~月度目标_06~月度目标_07~月度目标_08~月度目标_09~月度目标_10~月度目标_11~月度目标_12".split('~');
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if (json.indexOf("月") >= 0) {
                    colModel.push({ name: json, label:json.split("_")[1] + "月", sortable: true, sorttype: 'float', align: "center", width:90 })
                } else {
                    colModel.push({ name: json, label: json, sortable: true, sorttype: 'float', align: "center", width:90 })
                }
                if (json.indexOf("占比") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                    sumCol.push(json);
                } else if (json.indexOf("业绩提升") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else if(json.indexOf("门店名称") >= 0){
                		$.extend(colModel[i], { formatter: top.InitData.getBranchName })
                }
                else if (json.indexOf("门店") == -1) {
                    $.extend(colModel[i], jqFormatter.Num0)
                    sumCol.push(json);
                };
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 230,
                colModel: colModel,
                viewrecords: true,//定义是否要显示总记录数
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: jsonArr.length,
                shrinkToFit: false,
                footerrow: true,
                sumColArr: sumCol,
                sumColArr1: sumCol,
                sumName: "门店_合计",
                avgColArr: avgCol,
                Complete: function(gridID, sumColArr) {
                		var _this = $("#" + gridID);
                    var rowID = _this.jqGrid('getDataIDs');
                    for (var i = 0; i < rowID.length; i++) {
                        var lastYear = _this.getCell(rowID[i], "前一年销售")
                        console.log(lastYear)
                        console.log(sumColArr.前一年销售)
                        _this.setCell(rowID[i], "前一年占比", lastYear / sumColArr.前一年销售)
                    }
                    sumColArr.前一年占比 = _this.getCol("前一年占比", false, "sum")/100
                    sumColArr.业绩提升 = (sumColArr.销售任务 - sumColArr.前一年销售) / sumColArr.前一年销售;
                    _this.footerData("set", sumColArr);
                    condition.backColor_ABC(gridID, "业绩提升", 30,15,5)
                    _this.find("[aria-describedby*='销售任务']").css("color", "#222222").css("border-left", "1px dashed red").css("border-right", "1px dashed red").css("background-color", "#FCF2CA")
                    _this.find("[aria-describedby*='月度目标']").css("border-left", "1px solid #e7eaec ")
                    $("th[id*='月度目标']").css("border-left", "1px solid #e7eaec ")
                    _this.find("[aria-describedby*='年完成率预测']").css("border-right", "1px dashed #1c84c6")
                    $("#taskAll").val(sumColArr.销售任务);
                    
                    _this.jqGrid('setFrozenColumns');
                    $(".ui-th-column-header").addClass("column-header");
                    $(".ui-th-column-header").css("border-bottom", "1px solid #e7eaec")
                }
            });   
		},
		search: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.getNewData(function(data){
				me.loadGrid(data);
			});
		},
		setup: function(){
			top.showModal('setUpModal');
			top.setUpModalVm.clear();
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
            var me = this;
            window.location.href = baseurl + "/Report_PC/dataExport/excel?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+me.year+"&busi=overallTaskSetUp";
        },
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.getNewData(function(data){
				me.loadGrid(data);
			});
		}
	}
});
top.setUpModalVm = new Vue({
	el: '#setUpModal',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		formData : {
			areaCode:null,
			oper_date: moment().format('YYYY'),
			key_value: 0,
		},
		areaOptions:top.branchOptionData,
		yearValue: 0,
		monthValue:[
			[{month:1,value:0},{month:2,value:0},{month:3,value:0},{month:4,value:0},{month:5,value:0},{month:6,value:0}],
			[{month:7,value:0},{month:8,value:0},{month:9,value:0},{month:10,value:0},{month:11,value:0},{month:12,value:0}]
		]
	},
	mounted: function(){
		this.init();
	},
	methods: {
		changeYearValue: function(){
			var value = (this.yearValue / 12).toFixed(2);
			for(var i=0,len=this.monthValue.length; i<len; i++){
				this.monthValue[i][0].value = value;
				this.monthValue[i][1].value = value;
				this.monthValue[i][2].value = value;
				this.monthValue[i][3].value = value;
				this.monthValue[i][4].value = value;
				this.monthValue[i][5].value = value;
			}
		},
		changeMonthValue: function(){
			var value = 0;
			for(var i=0,len=this.monthValue.length; i<len; i++){
				value = value + this.monthValue[i][0].value * 1;
				value = value + this.monthValue[i][1].value * 1;
				value = value + this.monthValue[i][2].value * 1;
				value = value + this.monthValue[i][3].value * 1;
				value = value + this.monthValue[i][4].value * 1;
				value = value + this.monthValue[i][5].value * 1;
			}
			this.yearValue = value;
		},
		init: function(){
			var me = this;
			setTimeout(function(){
				me.initEvent();
			});
		},
		initEvent: function(){
			var me = this;
			pageVm.setUpModal.on('shown.bs.modal ', function (e) {
				pageVm.setUpModal.find('#operDate').datetimepicker({  
			        format: 'yyyy年',  
			         autoclose: true,  
			         forceParse: false,
			         startView: 4,  
			         minView: 4,  
			         language: 'zh-CN'
			    }).val(moment().format('YYYY年')).change(function(){
			    		me.formData.oper_date = moment($(this).val(), 'YYYY年').format('YYYY');
			    });
				me.formData.oper_date = moment().format('YYYY');
			});
		},
		save : function(){
			var me = this;
			if(!this.valid()){
				return;
			}
            me.formData.flag = "ZT";
            top.hideModal('setUpModal');
            window.showLoading();
			$.ajax({
			   type: "POST",
			   contentType:"application/json",
			   url: baseurl + '/Report_PC/Branch_Aim/submit_aim',
			   data: JSON.stringify(me.formData),
			   dataType: 'json',
			   success: function(res){
				   window.hideLoading();
                   pageVm.reload();
			   },
			   error: function(){
				   window.hideLoading();
			   }
			});
		},
		valid: function(){
			this.formData.key_value = this.yearValue;
			this.formData.month = this.monthValue[0].concat(this.monthValue[1]);
			if(!this.formData.areaCode){
				dialogMsg("请选择门店", 0);
				return false;
			}
			return true;
		},
		clear: function(){
			this.formData = {
				areaCode:null,
				oper_date: moment().format('YYYY'),
				key_value: 0
			};
			this.yearValue = 0;
			this.changeYearValue();
		}
	}
});