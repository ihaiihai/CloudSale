var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		areaCode: null,
		areaOptions:top.branchOptionData,
		year: moment().format('YYYY'),
		maxCnt: 0,
        maxLen: 0,
        setUpModal: null
	},
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.getNewData(function(data){
					me.GetGrid(data);
				});
				me.setUpModal = top.addModal($('#setUpModal')[0]);
			});
		},
		initEvent: function(){
			var me = this;
			$('#date').datetimepicker({  
		        format: 'yyyy年',  
		         autoclose: true,  
		         forceParse: false,
		         startView: 4,  
		         minView: 4,  
		         language: 'zh-CN'
		    }).val(moment().format('YYYY年')).change(function(){
		    		me.year = moment($(this).val(), 'YYYY年').format('YYYY');
		    });
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 230);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		getNewData: function(callback) {
			var me = this;
			window.showLoading();
        		$.ajax({
                url: baseurl + "/task/cls/querySetUp?branchNos="+(me.areaCode?me.areaCode:"")+"&year="+me.year,
                type: "get",
                dataType: "json",
                success: function (data) {
                		window.hideLoading();
                		if(callback){
    	                		callback(data);
    	                }
                },
                error: function(){
                		window.hideLoading();
                }
			});
        },
        GetGrid: function (jsonArr) {
        		var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var myArr = "门店编号~门店名称~类别编号~类别~前一年销售~前一年占比~销售任务~业绩提升~月度目标_01~月度目标_02~月度目标_03~月度目标_04~月度目标_05~月度目标_06~月度目标_07~月度目标_08~月度目标_09~月度目标_10~月度目标_11~月度目标_12".split('~');
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if (json.indexOf("月") >= 0) {
                    colModel.push({ name: json, label: Number(json.split("_")[1]) + "月", sortable: true, sorttype: 'float', align: "center", width:90 })
                } else {
                    colModel.push({ name: json, label: json, sortable: true, sorttype: 'float', align: "center", width:90 })
                }
                if (json.indexOf("占比") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else if (json.indexOf("业绩提升") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else if (json.indexOf("门店名称") >= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getBranchName })
                } else if (json.indexOf("类别编号") >= 0) {
                    $.extend(colModel[i], {hidden:true })
                } else if (json.indexOf("类别") >= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getClsName, width: 200 })
                } else if (json.indexOf("门店编号") == -1) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                };
                if (i > 7 || i == 6) {
                    $.extend(colModel[i], { editable: true })
                }
            }
            var oldVal = 0;
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 230,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 1000,
                shrinkToFit: false,
                shrinkToFit: true,
                footerrow: true,
                sumColArr: sumCol,
                sortname: "前一年销售",
                sortorder:"desc",
                sumName: "门店名称_合计",
                avgColArr: avgCol,
                grouping: true,
                groupingView: {
                    groupField: ['门店名称'],
                    groupSummary: [true],
                    groupColumnShow: [true],
                    groupText: ['<b>{0}各品类任务概况</b>'],
                    groupCollapse: false 
                },
                Complete: this.Complete
            });
        },
        Complete:function (gridID, sumColArr) {
        		sumColArr.业绩提升 = (sumColArr.销售任务 - sumColArr.前一年销售) / sumColArr.前一年销售;
            var _this = $("#" + gridID);
            _this.footerData("set", sumColArr);
            condition.backColor_ABC("gridTable", "业绩提升", 30, 15, 5)
            _this.find("[aria-describedby*='销售任务']").css("border-left", "1px dashed red").css("border-right", "1px dashed red").css("background-color", "#FCF2CA")
            _this.find("[aria-describedby*='月度目标']").css("border-left", "1px solid #e7eaec ")
              $("th[id*='月度目标']").css("border-left", "1px solid #e7eaec ")
            _this.find("[aria-describedby*='年完成率预测']").css("border-right", "1px dashed #1c84c6")
            
            this.afterInit(this.gridData);
        },
        afterInit:function (jsonArr) {
        		var me = this;
        		$("#gridTable").jqGrid('setFrozenColumns');
//            var footGroupNew = $("#gridTable").find(".ui-widget-content.jqfoot.ui-row-ltr")
//            for (var j = 0; j < footGroupNew.length; j++) {
//                var branchNo = footGroupNew.eq(j).prev("tr").find("[aria-describedby*='门店编号']").text();
//                for (var json in jsonArr[me.maxCnt]) {
//                    if (json.indexOf("月") >= 0) {
//                        var taskNow = footGroupNew.eq(j).find("[aria-describedby*='" + json + "']").text();
//                        monthTask["M" + branchNo + "_Y" + json.split("_")[1]] = parseInt(taskNow)
//                    }
//                }
//            };
        },
		search: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.getNewData(function(data){
				me.GetGrid(data);
			});
		},
		setup: function(){
			top.showModal('setUpModal');
			top.setUpModalVm.clear();
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
            var me = this;
            window.location.href = baseurl + "/Report_PC/dataExport/excel?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+me.year+"&busi=clsTaskSetUp";
        },
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.getNewData(function(data){
				me.GetGrid(data);
			});
		}
	}
});
top.setUpModalVm = new Vue({
	el: '#setUpModal',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		formData : {
			areaCode:null,
			clsCode: null,
			oper_date: moment().format('YYYY'),
			key_value: 0,
		},
		areaOptions:top.branchOptionData,
		clsOptions:top.clsOptionData,
		yearValue: 0,
		monthValue:[
			[{month:1,value:0},{month:2,value:0},{month:3,value:0},{month:4,value:0},{month:5,value:0},{month:6,value:0}],
			[{month:7,value:0},{month:8,value:0},{month:9,value:0},{month:10,value:0},{month:11,value:0},{month:12,value:0}]
		]
	},
	mounted: function(){
		this.init();
	},
	methods: {
		changeYearValue: function(){
			var value = (this.yearValue / 12).toFixed(2);
			for(var i=0,len=this.monthValue.length; i<len; i++){
				this.monthValue[i][0].value = value;
				this.monthValue[i][1].value = value;
				this.monthValue[i][2].value = value;
				this.monthValue[i][3].value = value;
				this.monthValue[i][4].value = value;
				this.monthValue[i][5].value = value;
			}
		},
		changeMonthValue: function(){
			var value = 0;
			for(var i=0,len=this.monthValue.length; i<len; i++){
				value = value + this.monthValue[i][0].value * 1;
				value = value + this.monthValue[i][1].value * 1;
				value = value + this.monthValue[i][2].value * 1;
				value = value + this.monthValue[i][3].value * 1;
				value = value + this.monthValue[i][4].value * 1;
				value = value + this.monthValue[i][5].value * 1;
			}
			this.yearValue = value;
		},
		init: function(){
			var me = this;
			setTimeout(function(){
				me.initEvent();
			});
		},
		initEvent: function(){
			var me = this;
			pageVm.setUpModal.on('shown.bs.modal ', function (e) {
				pageVm.setUpModal.find('#operDate').datetimepicker({  
			        format: 'yyyy年',  
			         autoclose: true,  
			         forceParse: false,
			         startView: 4,  
			         minView: 4,  
			         language: 'zh-CN'
			    }).val(moment().format('YYYY年')).change(function(){
			    		me.formData.oper_date = moment($(this).val(), 'YYYY年').format('YYYY');
			    });
				me.formData.oper_date = moment().format('YYYY');
			});
		},
		save : function(){
			var me = this;
			if(!this.valid()){
				return;
			}
            me.formData.flag = "PL";
            top.hideModal('setUpModal');
            window.showLoading();
			$.ajax({
			   type: "POST",
			   contentType:"application/json",
			   url: baseurl + '/Report_PC/Branch_Aim/submit_aim',
			   data: JSON.stringify(me.formData),
			   dataType: 'json',
			   success: function(res){
				   window.hideLoading();
				   pageVm.reload();
			   },
			   error: function(){
				   window.hideLoading();
			   }
			});
		},
		valid: function(){
			this.formData.key_value = this.yearValue;
			this.formData.month = this.monthValue[0].concat(this.monthValue[1]);
			if(!this.formData.areaCode){
				dialogMsg("请选择门店", 0);
				return false;
			}
			if(!this.formData.clsCode){
				dialogMsg("请选择分类", 0);
				return false;
			}
			return true;
		},
		clear: function(){
			this.formData = {
				areaCode:null,
				clsCode:null,
				oper_date: moment().format('YYYY'),
				key_value: 0
			};
			this.yearValue = 0;
			this.changeYearValue();
		}
	}
});