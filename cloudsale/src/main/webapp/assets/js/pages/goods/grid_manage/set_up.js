var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		areaCode: null,
		areaOptions:[],
		groupType:0
	},
	watch:{  
    		'groupType': 'loadGrid'
    },
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
				me.loadAreaOptions();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 260);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			this.GetGrid();
		},
        GetGrid: function() {
        		var me = this;
        		var keyWord = {
                "col_name": "门店名称~item_no~条码~货号~商品名称~成本价~零售价~最小陈列~最低库存~最大库存~滞销标准~日均销量~会员数~最后日期~未动天数~库存数量~现有库存~库存金额"
            }
            var colNames = keyWord.col_name.split("~");
            var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var i = 0;
            for (var i = 0; i < colNames.length; i++) {
                var json = colNames[i]
                colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                if (json.indexOf("商品名称") >= 0) {
                    $.extend(colModel[i], { width:380})
                } else if (json.indexOf("item_no") >= 0) {
                    $.extend(colModel[i], { hidden: true })
                } else if (json.indexOf("门店名称") >= 0) {
                    $.extend(colModel[i], { formatter: public_data.Dic_branch ,hidden:(me.groupType=="0"?true:false)})
                } else if (json.indexOf("条码") >= 0 || json.indexOf("货号") >= 0) {
                    $.extend(colModel[i],{ width: 250 })
                } else if (json.indexOf("现有库存") >= 0) {
                    $.extend(colModel[i], { formatter: me.stockFormat })
                    sumCol.push(json);
                } else if (json.indexOf("库存数量") >= 0) {
                    $.extend(colModel[i], { hidden: true })
                    sumCol.push(json);
                } else if (json.indexOf("库存金额") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                    sumCol.push(json);
                } else if (json.indexOf("价") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB2)
                    avgCol.push(json);
                } else if (json.indexOf("最") >= 0 && json.indexOf("最后") ==-1) {
                        $.extend(colModel[i], jqFormatter.Num0,{editable: true })
                    sumCol.push(json);
                } else if (json.indexOf("滞销标准") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { editable: true })
                    avgCol.push(json);
                }
            }
            var data = [{"门店名称":"全体门店","item_no":"6907992633534","条码":"6907992633534","货号":"633534","商品名称":"金领冠菁护3段800g","成本价":143.9065038461,"零售价":238.000000,"最小陈列":6,"最低库存":9,"最大库存":12,"滞销标准":30,"日均销量":20.18,"会员数":240.00,"最后日期":"2019-01-16","未动天数":0,"库存数量":2974.0000000000,"现有库存":2974.0000000000,"库存金额":502996.6902000000},{"门店名称":"全体门店","item_no":"6907925770121","条码":"6907925770121","货号":"770121","商品名称":"飞帆五星优护(厅1)","成本价":254.3893961538,"零售价":378.000000,"最小陈列":null,"最低库存":null,"最大库存":null,"滞销标准":null,"日均销量":8.11,"会员数":129.00,"最后日期":"2019-01-16","未动天数":0,"库存数量":2000.0000000000,"现有库存":2000.0000000000,"库存金额":488132.8370000000},{"门店名称":"全体门店","item_no":"6907992632520","条码":"6907992632520","货号":"632520","商品名称":"金领冠珍护3段800g","成本价":192.4952653846,"零售价":318.000000,"最小陈列":7,"最低库存":11,"最大库存":15,"滞销标准":17,"日均销量":15.75,"会员数":173.00,"最后日期":"2019-01-15","未动天数":1,"库存数量":1920.0000000000,"现有库存":1920.0000000000,"库存金额":439421.4510000000}];
            this.gridData = data;
            $("#gridTable").jqGridEx({
                datatype: "local",
                data:data,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                sortable: true,
                footerrow: true,
                //sortname: '商品名称',
                sumColArr: sumCol,
                sumName: "商品名称_合计（价格为均值）",
                avgColArr: avgCol,
                height: $(window).height() - 260,
                width: $('.gridCard').width() - 10,
                pager: "#pager_list",
                pagerpos: "right",
                loadonce: true,  //缓存排序后才可翻页
                rowList: [100, 200, 500, 1000],
            });
        },
        Complete: function(gridID, sumColArr) {
        		var me = this;
        		var _this = $("#" + gridID);
            sumColArr.现有库存 = sumColArr.库存数量
            _this.footerData("set", sumColArr);
            _this.find("[aria-describedby*='滞销标准']").css("border-right", "1px dashed red").css("background-color", "#FCF2CA")
            _this.find("[aria-describedby*='最低库存']").css("background-color", "#FCF2CA")
            _this.find("[aria-describedby*='最大库存']").css("background-color", "#FCF2CA")
            _this.find("[aria-describedby*='最小陈列']").css("border-left", "1px dashed red").css("background-color", "#FCF2CA")
            $(".ui-jqgrid-hdiv").find("th[id*='滞销标准']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='最小陈列']").css("border-left", "1px dashed red")
           
        },
        stockFormat:function (cellvalue, options, rowObject) {
            if (cellvalue < rowObject.最低库存) {
                return ('<span class="badge badge-danger">' + cellvalue + '</span>')
            } else if (cellvalue > rowObject.最大库存) {
                return ('<span class="badge badge-warning">' + cellvalue + '</span>')
            } else {
                return ('<span class="badge badge-info">' + cellvalue + '</span>')
            }
        },
		loadAreaOptions: function(parentAreaCode, callback){
			var me = this;
			$.getJSON(baseurl + "/sys/area/queryOptions", {
				parentAreaCode: parentAreaCode
			}, function(res) {
				if(callback){
					callback(res);
				}else{
					me.areaOptions = res;
				}
			});
		},
		search: function(){
			var me = this;
			$('#gridTable').GridUnload();
			this.GetGrid(me.getNewData());
		},
		edit: function(){
			
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
			
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			me.key = '';
			me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
	    				key: me.key
	            },
	            page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
		}
	}
});