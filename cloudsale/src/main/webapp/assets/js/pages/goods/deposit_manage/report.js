var pageVM = new Vue({
	el : '#page',
	data : {
		gridTab: 0,
		areaCode: null,
		areaOptions:[],
		data:[{"name":"全公司截止2019-01-17客户寄存未取走商品总金额【1485325元】,总数量【13066件】"}],
		data1:[{"name":"0757","value":90.0000},{"name":"0717","value":113.0000},{"name":"0743","value":116.0000},{"name":"0820","value":128.0000},{"name":"0701","value":129.0000},{"name":"0704","value":175.0000},{"name":"0702","value":186.0000},{"name":"0758","value":203.0000},{"name":"0718","value":237.0000},{"name":"0913","value":261.0000},{"name":"PP","value":309.0000},{"name":"0668","value":376.0000},{"name":"0811","value":422.0000},{"name":"0606","value":505.0000},{"name":"0807","value":528.0000},{"name":"0724","value":789.0000},{"name":"0712","value":859.0000},{"name":"0709","value":1737.0000},{"name":"0669","value":1940.0000},{"name":"0818","value":3287.0000}],
		data2:[{"branch_no":"000901","寄存金额":300404.0216,"寄存占比":24.910000},{"branch_no":"000601","寄存金额":218109.8636,"寄存占比":47.090000},{"branch_no":"000801","寄存金额":167122.3468,"寄存占比":65.340000},{"branch_no":"000301","寄存金额":164024.5048,"寄存占比":80.790000},{"branch_no":"000401","寄存金额":131473.4850,"寄存占比":53.360000},{"branch_no":"000501","寄存金额":123113.1880,"寄存占比":43.010000},{"branch_no":"001701","寄存金额":94314.2720,"寄存占比":53.170000},{"branch_no":"000201","寄存金额":91293.2567,"寄存占比":56.830000},{"branch_no":"003501","寄存金额":81001.8385,"寄存占比":42.820000},{"branch_no":"003601","寄存金额":59256.6782,"寄存占比":null},{"branch_no":"000701","寄存金额":49166.0718,"寄存占比":50.520000},{"branch_no":"003701","寄存金额":5535.4800,"寄存占比":null},{"branch_no":"001601","寄存金额":272.8000,"寄存占比":0.370000},{"branch_no":"001901","寄存金额":237.6000,"寄存占比":0.270000}],
		data3:[{"name":"180-360天","value":6922.0000},{"name":"360天以上","value":6144.0000}],
		data4:[{"name":"食品","value":1105235},{"name":"纸尿裤","value":317010},{"name":"洗护用品","value":55772},{"name":"喂养用品","value":3250},{"name":"其它类别","value":1837},{"name":"鞋帽袜散货件","value":826},{"name":"童车","value":605},{"name":"床品内衣宝宝套","value":253},{"name":"婴童装外出服","value":241},{"name":"孕产妇类","value":239},{"name":"玩具","value":29},{"name":"赠品","value":23},{"name":"购物券","value":0}],
		data5:[{"name":"食品","value":5041.0000},{"name":"纸尿裤","value":4610.0000},{"name":"洗护用品","value":3129.0000},{"name":"赠品","value":129.0000},{"name":"喂养用品","value":47.0000},{"name":"其它类别","value":42.0000},{"name":"鞋帽袜散货件","value":23.0000},{"name":"物料","value":20.0000},{"name":"童车","value":14.0000},{"name":"婴童装外出服","value":4.0000},{"name":"玩具","value":3.0000},{"name":"购物券","value":2.0000},{"name":"床品内衣宝宝套","value":1.0000},{"name":"孕产妇类","value":1.0000}]
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.loadData();
			});
		},
		initEvent: function(){
			var me = this;
		},
		loadData: function(){
			this.getChartData();
		},
		getChartData: function(){
			var me = this;
			this.createChart1(this.data1);
			this.createChart2(this.data2);
			this.createChart3(this.data3);
			this.createChart4(this.data4);
			this.createChart5(this.data5);
		},
		createChart1 : function(data) {
			var me = this;
			option = {
                title: {
                    text: '●寄存数量按品牌 top 20',
                    x: 'left'
                },
                tooltip: {
                    show: true,
                    formatter: "{a}: {c}",
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '0%',
                    top: '5%',
                    containLabel: true
                },
                xAxis: {
                    show: false,
                    type: 'value',
                    boundaryGap: [0, 0.2]
                },
                yAxis: {
                    type: 'category',
                    data: public_data.getEchartsData(data, 'name', 'brand'),
                    position: 'right',
                    splitArea: {
                        show: true
                    },
                },
                series: [
                {
                    name: '寄存数量',
                    type: 'bar',
                    label: {
                        normal: {
                            show: true,
                            position: 'right',
                            formatter: '{c}'
                        }
                    },
                    data: public_data.getEchartsData(data, 'value', ''),
                }
                ]
            };
			ECharts.Draw(option, "chart1");
		},
		createChart2 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '●各门店寄存金额、及寄存金额销售占比（寄存金额÷30日内门店销售额）',
                    left: 'left'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '1%',
                    top: '15%',
                    containLabel: true
                },
                legend: {
                    data: ['寄存金额', '寄存占比'],
                    top: '2%',
                    x:'right'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: public_data.getEchartsData(data, 'branch_no', 'branch'),
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}'
                        }
                    },
                    {
                        type: 'value',
                        splitLine: {
                            show: false
                        },
                        axisLabel: {
                            formatter: '{value}/%'
                        }
                    }
                ],
                series: [
                    {
                        name: '寄存金额',
                        type: 'bar',
                        data: public_data.getEchartsData(data, '寄存金额', ''),
                    },
                    {
                        name: '寄存占比',
                        type: 'line',
                        yAxisIndex: 1,
                        data: public_data.getEchartsData(data, '寄存占比', ''),
                    }
                ]
            };
			ECharts.Draw(option, "chart2");
		},
		createChart3 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '●寄存未取数量按时间分布',
                    left: 'left'
                },
                legend: {
                    data: public_data.getEchartsData(data, 'name', ''),
                    top: '15%',
                    orient: 'vertical',
                    x: 'right',
                },
                grid: {
                    top:'15%',
                    left: '1%',
                    right: '30%',
                    bottom: '3%',
                    containLabel: true
                },
                yAxis: {
                    type: 'value'
                },
                xAxis: {
                    type: 'category',
                    data: ['1 ']
                },
                series: []
            };
            for (var i = 0; i < data.length; i++) {
               var json={
                    name: data[i]["name"],
                    type: 'bar',
                    stack: '总量',
                    label: {
                        normal: {
                            show: true,
                            position: 'inside'
                        }
                    },
                    data: [data[i]["value"]]
               }
                option.series.push(json)
            }
			ECharts.Draw(option, "chart3");
		},
		createChart4 : function(data) {
			var me = this;
			data = public_data.Dic_pie(data, 5, 10, 2);
            var option = {
                title: {
                    text: '●寄存商品金额类别分布',
                    left: 'left'
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '1%',
                    containLabel: true
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [
                       {
                           name: '寄存',
                           type: 'pie',
                           radius: ['35%', '55%'],
                           center: ['50%', '55%'],
                           data: data,
                           itemStyle: {
                               normal: {
                                   label: {
                                       show: true,
                                       formatter: '{b}\n ￥{c}',
                                       textStyle: {
                                           color: '#333'
                                       }
                                   },
                                   labelLine: { show: true }
                               }
                           }
                       }
                ]
            };
			ECharts.Draw(option, "chart4");
		},
		createChart5 : function(data) {
			var me = this;
			data=public_data.Dic_pie(data,15)
            var option = {
                title: {
                    text: '●寄存数量按类别分布',
                    left: 'left'
                },
                grid: {
                    left: '3%',
                    right: '3%',
                    bottom: '3%',
                    containLabel: true
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [
                       {
                           name: '寄存',
                           type: 'pie',
                           radius: ['35%', '55%'],
                           center: ['50%', '55%'],
                           data: data,
                           itemStyle: {
                               normal: {
                                   label: {
                                       show: true,
                                       formatter: '{b} : {c}',
                                       textStyle: {
                                           color: '#333'
                                       }
                                   },
                                   labelLine: { show: true }
                               }
                           }
                       }
                ]
            };
			ECharts.Draw(option, "chart5");
		}
	}
});