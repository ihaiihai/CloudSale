var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		searchType:1,
		areaCode: null,
		areaOptions:top.branchOptionData,
		clsCode: null,
		clsOptions:top.clsOptionData,
		brandCode: null,
		brandOptions:top.brandOptionData,
		gridType:0,
		parmCls: [
            { "id": "cls", "name": "整体" },
            { "id": "item_brand", "name": "品牌" },
            { "id": "main_supcust", "name": "供应商" }
        ],
        groupName:'',
        groupNameCls: "门店名称"
	},
	watch:{  
    		'gridType': 'loadGrid'
    },
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 290);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			var me = this;
			var branchNos = me.areaCode ? me.areaCode : '';
			var clses = me.clsCode?me.clsCode:"";
			var brands = me.brandCode?me.brandCode:"";
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
			var end = date ? date.split(' - ')[1] : '';
			var type = ['cls','item_brand'][me.gridType];
			$('#gridTable').GridUnload();
			window.showLoading();
			$.get(baseurl + "/Report_PC/Jinxiaocun/jinxiaocun_query?branchNos="+branchNos+"&itemClses="+clses+"&brands="+brands+"&begin="+begin+'&end='+end+'&type='+type,function(data){
				window.hideLoading();
				me.GetGrid(data);
				me.afterInit(data);
			});
		},
		getNewData: function() {
			var me = this;
			me.groupName = "";
            switch (me.gridType) {
                case "1": me.groupName = "品牌"; break;
                case "2": me.groupName = "供应商"; break;
                default: me.groupName = "类别"; break;
            }
            var keyWord = {
                "col_name": "编号~分类名称~采购_成本金额~采购_售价金额~采购_数量~整体销售_金额~整体销售_成本~整体销售_数量~整体销售_单数~整体销售_客单价~整体销售_件均价~整体销售_退货额~整体销售_退货率~库存_库存数量~库存_售价金额~库存_售价均价~库存_成本金额~库存_成本均价"
            }
            colNames = keyWord.col_name.split("~");
            var colArr = keyWord.col_name.split("~");
            var newArr = [{"编号":null,"分类名称":null,"采购_成本金额":378023.6220,"采购_售价金额":1203219.65000000,"采购_数量":27508.0000,"整体销售_金额":0.0000,"整体销售_成本":0.00000000,"整体销售_数量":0.0000,"整体销售_单数":0,"整体销售_客单价":0.000000,"整体销售_件均价":0.000000,"整体销售_退货额":0.0000,"整体销售_退货率":0.000000,"库存_库存数量":0.0000000000,"库存_售价金额":0.00000000000,"库存_售价均价":null,"库存_成本金额":0.000000000,"库存_成本均价":null},{"编号":"01","分类名称":"01","采购_成本金额":3065529.3980,"采购_售价金额":4039784.60000000,"采购_数量":30028.0000,"整体销售_金额":2558236.3000,"整体销售_成本":2367334.36610000,"整体销售_数量":20004.0000,"整体销售_单数":7758,"整体销售_客单价":329.754614,"整体销售_件均价":127.886237,"整体销售_退货额":380874.5600,"整体销售_退货率":0.148881,"库存_库存数量":76709.0040000000,"库存_售价金额":12130996.75200000000,"库存_售价均价":158.143061,"库存_成本金额":8256597.598044800,"库存_成本均价":107.635312},{"编号":"02","分类名称":"02","采购_成本金额":904648.4800,"采购_售价金额":1382180.00000000,"采购_数量":13162.0000,"整体销售_金额":588036.1900,"整体销售_成本":630381.22990000,"整体销售_数量":10229.0000,"整体销售_单数":3875,"整体销售_客单价":151.751274,"整体销售_件均价":57.487162,"整体销售_退货额":95843.4500,"整体销售_退货率":0.162989,"库存_库存数量":44609.3000000000,"库存_售价金额":2962778.70000000000,"库存_售价均价":66.416166,"库存_成本金额":1878086.892040000,"库存_成本均价":42.100792},{"编号":"03","分类名称":"03","采购_成本金额":111362.2000,"采购_售价金额":414195.60000000,"采购_数量":11120.0000,"整体销售_金额":198251.0900,"整体销售_成本":143272.06620000,"整体销售_数量":10078.0000,"整体销售_单数":3683,"整体销售_客单价":53.828696,"整体销售_件均价":19.671669,"整体销售_退货额":4632.9500,"整体销售_退货率":0.023369,"库存_库存数量":52849.0000000000,"库存_售价金额":1903616.50000000000,"库存_售价均价":36.019915,"库存_成本金额":625039.331400000,"库存_成本均价":11.826890}];
            this.gridData = newArr;
            return newArr
        },
        GetGrid: function(jsonArr) {
    		var me = this;
    		var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var myArr = "编号~分类名称~采购_成本金额~采购_售价金额~采购_数量~整体销售_金额~整体销售_成本~整体销售_数量~整体销售_单数~整体销售_客单价~整体销售_件均价~整体销售_退货额~整体销售_退货率~库存_库存数量~库存_售价金额~库存_售价均价~库存_成本金额~库存_成本均价".split("~");
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if (i == 0) {
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                } else {
                    colModel.push({ name: json, label: json.split("_")[1], align: "center", sortable: true, sorttype: 'float' })
                };
                if (json.indexOf("分类名称") >= 0) {
                    switch (me.gridType) {
                        case 0: $.extend(colModel[i], { width:300,label: "类别名称", formatter: top.InitData.getClsName }); break
                        case 1: $.extend(colModel[i], { width: 300, label: "品牌名称", formatter: top.InitData.getBrandName }); break
                    }
                } else if (json.indexOf("额") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("价") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                } else if (json.indexOf("数量") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("售罄_销售") >= 0 || json.indexOf("售罄_入库") >= 0 || json.indexOf("售罄_库存") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json); 
                } else if (json.indexOf("整体销售_单数") >= 0 ) {
                    $.extend(colModel[i], { summaryType: 'sum' ,hidden:true})
                    sumCol.push(json);
                } else if (json.indexOf("整体销售_成本") >= 0) {
                        $.extend(colModel[i], jqFormatter.RMB0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("率") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else if (json.indexOf("售罄") >= 0 && json.indexOf("售罄率")==-1) {
                    $.extend(colModel[i], jqFormatter.Num0)
                    sumCol.push(json);
                }
                if (top.CanSeeProfit == 0 && (json.indexOf("利") >= 0 || json.indexOf("成本") >= 0)) {
                    $.extend(colModel[i], { hidden: true })
                }
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 290,
                width: $('.gridCard').width() - 10,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                shrinkToFit: true,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                pager: "#pager_list",
                pagerpos: "right",
                rowList: [100, 200, 500, 1000],
                sortname: "分类",
                sumColArr: sumCol,
                sumName: "分类_合计",
                avgColArr: avgCol,
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
    		var me = this;
    		var _this = $("#" + gridID);
            sumColArr.售罄_售罄率 = sumColArr.售罄_销售 / sumColArr.售罄_入库
            sumColArr.库存_售价均价 = sumColArr.库存_售价金额 / sumColArr.库存_库存数量
            sumColArr.库存_成本均价 = sumColArr.库存_成本金额 / sumColArr.库存_库存数量
            sumColArr.整体销售_客单价 = sumColArr.整体销售_金额 / sumColArr.整体销售_单数
            sumColArr.整体销售_件均价 = sumColArr.整体销售_金额 / sumColArr.整体销售_数量
            sumColArr.整体销售_退货率 = sumColArr.整体销售_退货额 / sumColArr.整体销售_金额
            _this.footerData("set", sumColArr);
            condition.backColor_ABC(gridID, "售罄率", 85,65,45)
            condition.backColor_CBA(gridID, "退货率", 3, 5,8)
            _this.find("[aria-describedby*='分类名称']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='分类名称']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='采购_数量']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='采购_数量']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='成本均价']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='成本均价']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='退货率']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='退货率']").css("border-right", "1px dashed red")
            
        },
        afterInit: function(jsonArr) {
			var me = this;
			var groupHeaders = [];
            for (var json in jsonArr[0]) {
                if (json.indexOf("采购_成本金额") >= 0) {
                    groupHeaders.push({ startColumnName: json, numberOfColumns: 3, titleText: "【采购概况】" })
                } else if (json.indexOf("整体销售_金额") >= 0) {
                    groupHeaders.push({ startColumnName: json, numberOfColumns: 8, titleText: "【本期销售概况】" })
                } else if (json.indexOf("库存_库存数量") >= 0) {
                    groupHeaders.push({ startColumnName: json, numberOfColumns: 5, titleText: "【实时库存概况】" })
                } 
            }
            $("#gridTable").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: groupHeaders
            });
        },
		search: function(){
			var me = this;
			$('#gridTable').GridUnload();
			me.loadGrid();
		},
		setSku: function(){
			
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
            var me = this;
            var branchNos = me.areaCode ? me.areaCode : '';
            var clses = me.clsCode?me.clsCode:"";
            var brands = me.brandCode?me.brandCode:"";
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
            var end = date ? date.split(' - ')[1] : '';
            var type = ['cls','item_brand'][me.gridType];
            window.location.href =baseurl + "/Report_PC/dataExport/excel?branchNos="+branchNos+"&clsNos="+clses+"&brandNos="+brands+"&begin="+begin+'&end='+end+'&type='+type+"&busi=jinxiaocun";
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			me.key = '';
			me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
	    				key: me.key
	            },
	            page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
		}
	}
});