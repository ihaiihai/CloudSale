var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		areaCode: null,
		areaOptions:[],
		gridType:0,
		gridGroup:0
	},
	watch:{  
    		'gridType': 'loadGrid',
    		'gridGroup': 'changeGroup'
    },
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
				me.loadAreaOptions();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 260);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			this.GetGrid(this.getNewData());
		},
		changeGroup: function(){
			if (this.gridGroup == 0) {
                $("#gridTable").jqGrid('groupingGroupBy',"门店名称");
            } else {
                $("#gridTable").jqGrid('groupingGroupBy', this.groupName);
            }
		},
		getNewData: function() {
			var me = this;
			groupName="";
            switch (this.gridType) {
                case "brand": groupName = "品牌"; break;
                case "supcust": groupName = "供应商"; break;
                default : groupName = "类别"; break;
            }
            this.groupName = groupName;
            var keyWord = {
                "col_name": "门店编号~门店名称~" + groupName + "~库存金额~库存数量~有效SKU~有库存SKU~有指标~指标完整率~库存合理~合理率~陈列不足~不足率~缺货SKU~缺货率~积压SKU~积压率~滞销SKU~滞销率~异常SKU~异常率"
            }
            var colArr = keyWord.col_name.split("~");
            var newArr = [{"门店编号":"000001","门店名称":"000001","类别":"01","库存金额":5851947.3587000000,"库存数量":49645.0000000000,"有效SKU":476,"有库存SKU":286,"有指标":2,"指标完整率":0.004201680672,"库存合理":0,"合理率":0.000000000000,"陈列不足":0,"不足率":0.000000000000,"缺货SKU":0,"缺货率":0.000000000000,"积压SKU":2,"积压率":0.000000000000,"滞销SKU":0,"滞销率":0.000000000000,"异常SKU":1,"异常率":0.003496503496},{"门店编号":"000001","门店名称":"000001","类别":"02","库存金额":1159138.2162000000,"库存数量":31296.0000000000,"有效SKU":140,"有库存SKU":80,"有指标":0,"指标完整率":0.000000000000,"库存合理":0,"合理率":0.000000000000,"陈列不足":0,"不足率":0.000000000000,"缺货SKU":0,"缺货率":0.000000000000,"积压SKU":0,"积压率":0.000000000000,"滞销SKU":0,"滞销率":0.000000000000,"异常SKU":0,"异常率":0.000000000000},{"门店编号":"000001","门店名称":"000001","类别":"03","库存金额":315815.5485000000,"库存数量":33717.0000000000,"有效SKU":193,"有库存SKU":123,"有指标":0,"指标完整率":0.000000000000,"库存合理":0,"合理率":0.000000000000,"陈列不足":0,"不足率":0.000000000000,"缺货SKU":0,"缺货率":0.000000000000,"积压SKU":0,"积压率":0.000000000000,"滞销SKU":0,"滞销率":0.000000000000,"异常SKU":0,"异常率":0.000000000000}];
            this.gridData = newArr;
            return newArr;
        },
        GetGrid: function(jsonArr) {
        		var me = this;
        		var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var i = 0;
            for (var json in jsonArr[0]) {
                colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                if (json.indexOf("门店名称") >= 0) {
                    $.extend(colModel[i], { formatter: public_data.Dic_branch})
                } else if (json.indexOf("门店编号") >= 0) {
                    $.extend(colModel[i], { hidden:true})
                } else if (json.indexOf("类别") >= 0) {
                    $.extend(colModel[i], { formatter: public_data.Dic_cls_all})
                } else if (json.indexOf("品牌") >= 0) {
                    $.extend(colModel[i], { formatter: public_data.Dic_brand })
                } else if (json.indexOf("供应商") >= 0) {
                    $.extend(colModel[i], { width:300,formatter: public_data.Dic_supcust})
                } else if (json.indexOf("率") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else if (json.indexOf("金额") >= 0) {
                    $.extend(colModel[i],  jqFormatter.RMB0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("门店编号")==-1) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                }
                i++
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 260,
                width: $('.gridCard').width() - 10,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                shrinkToFit: true,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                pager: "#pager_list",
                pagerpos: "right",
                rowList: [100, 200, 500, 1000],
                sortname: groupName,
                sumColArr: sumCol,
                sumName: "门店名称_合计",
                avgColArr: avgCol,
                grouping: true,
                groupingView: {
                    groupField: ['门店名称'],
                    groupSummary: [true],
                    groupColumnShow: [true],
                    groupText: ['<b>{0}各品类库存指标概况</b>'],
                    groupCollapse: false,
                },
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
        		var me = this;
        		var _this = $("#" + gridID);
            sumColArr.指标完整率 = sumColArr.有指标 / sumColArr.有效SKU
            sumColArr.合理率 = sumColArr.库存合理 / sumColArr.有指标
            sumColArr.不足率 = sumColArr.陈列不足 / sumColArr.有指标
            sumColArr.缺货率 = sumColArr.缺货SKU / sumColArr.有指标
            sumColArr.积压率 = sumColArr.积压SKU / sumColArr.有指标
            sumColArr.滞销率 = sumColArr.滞销SKU / sumColArr.有指标
            _this.footerData("set", sumColArr);
            condition.backColor_ABC(gridID, "完整率", 95, 85, 75);
            condition.backColor_ABC(gridID, "合理率", 95, 85, 75);
            condition.backColor_CBA(gridID, "不足率", 10, 20, 30);
            condition.backColor_CBA(gridID, "缺货率", 10, 20, 30);
            condition.backColor_CBA(gridID, "积压率", 10, 20, 30);
            condition.backColor_CBA(gridID, "滞销率", 10, 20, 30);
            condition.backColor_CBA(gridID, "异常率", 5,10,15);
            _this.find("[aria-describedby*='率']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='有效SKU']").css("border-left", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='率']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='有效SKU']").css("border-left", "1px dashed red")
            _this.find("[aria-describedby*='年完成率预测']").css("border-right", "1px dashed #1c84c6")
            
            this.afterInit(this.gridData);
        },
        afterInit: function(jsonArr) {
    			var me = this;
			var groupHeaders = [];
			for ( var obj in jsonArr[0]) {
				if (obj.indexOf("年度目标") >= 0) {
					groupHeaders.push({
						startColumnName : obj,
						numberOfColumns : 12,
						titleText : "【" + public_data.Dic_GetName(obj.split("_")[1], "cls_all") + "类】任务概况"
					})
				}
			}
			if ($(".btn.btn-sm.btn-white.active").attr("data-value") == 0) {
				$("#gridTable").jqGrid('setGroupHeaders', {
					useColSpanStyle : true,
					groupHeaders : groupHeaders
				});
				$("#gridTable").jqGrid('setFrozenColumns');
				$(".ui-th-column-header").addClass("column-header");
				$(".ui-th-column-header").css("border-left", "1px dashed red")
			}
        },
		loadAreaOptions: function(parentAreaCode, callback){
			var me = this;
			$.getJSON(baseurl + "/sys/area/queryOptions", {
				parentAreaCode: parentAreaCode
			}, function(res) {
				if(callback){
					callback(res);
				}else{
					me.areaOptions = res;
				}
			});
		},
		search: function(){
			var me = this;
			$('#gridTable').GridUnload();
			this.GetGrid(me.getNewData());
		},
		setSku: function(){
			
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
			
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			me.key = '';
			me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
	    				key: me.key
	            },
	            page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
		}
	}
});