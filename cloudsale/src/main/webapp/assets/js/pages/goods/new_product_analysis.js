var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		searchType:1,
		areaCode: null,
		areaOptions:top.branchOptionData,
		clsCode: null,
		clsOptions:top.clsOptionData,
		brandCode: null,
		brandOptions:top.brandOptionData,
		gridType:0
	},
	watch:{  
    		'gridType': 'loadGrid'
    },
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 290);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			var me = this;
			var branchNos = me.areaCode ? me.areaCode : '';
			var clses = me.clsCode?me.clsCode:"";
			var brands = me.brandCode?me.brandCode:"";
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
			var end = date ? date.split(' - ')[1] : '';
			$('#gridTable').GridUnload();
			window.showLoading();
			$.get(baseurl + "/Report_PC/Xinpin_dongxiao/dongxiao_query?branchNos="+branchNos+"&itemClses="+clses+"&brands="+brands+"&begin="+begin+'&end='+end,function(data){
				window.hideLoading();
				me.GetGrid(data);
				me.afterInit(data);
			});
		},
		getNewData: function() {
			var me = this;
			var keyWord = {
                "col_name": "item_no~条码~商品名称~门店编号~门店名称~上货数量~成本均价~上货成本~销售金额~销售数量~成交单数~客单价~退货金额~退货率~毛利贡献~毛利率~实时库存~未动天数"
            }
            colNames = keyWord.col_name.split("~");
            var colArr = keyWord.col_name.split("~");
            var newArr = [{"item_no":"6927153029075       ","条码":"6927153029075","商品名称":"美意6D卡通毛绒帽子","门店编号":"000601","门店名称":"000601","上货数量":-3.0000,"成本均价":10.930000,"上货成本":-32.79000000,"销售金额":0.0000,"销售数量":0.0000,"成交单数":0,"客单价":0.000000,"退货金额":0.0000,"退货率":0.000000,"毛利贡献":null,"毛利率":null,"实时库存":-3.0000000000,"未动天数":null},{"item_no":"6927153029075       ","条码":"6927153029075","商品名称":"美意6D卡通毛绒帽子","门店编号":"002601","门店名称":"002601","上货数量":-1.0000,"成本均价":10.930000,"上货成本":-10.93000000,"销售金额":0.0000,"销售数量":0.0000,"成交单数":0,"客单价":0.000000,"退货金额":0.0000,"退货率":0.000000,"毛利贡献":null,"毛利率":null,"实时库存":-1.0000000000,"未动天数":null},{"item_no":"6927153029075       ","条码":"6927153029075","商品名称":"美意6D卡通毛绒帽子","门店编号":"002801","门店名称":"002801","上货数量":1.0000,"成本均价":10.930000,"上货成本":10.93000000,"销售金额":0.0000,"销售数量":0.0000,"成交单数":0,"客单价":0.000000,"退货金额":0.0000,"退货率":0.000000,"毛利贡献":null,"毛利率":null,"实时库存":1.0000000000,"未动天数":null},{"item_no":"6927153029075       ","条码":"6927153029075","商品名称":"美意6D卡通毛绒帽子","门店编号":"003001","门店名称":"003001","上货数量":1.0000,"成本均价":10.930000,"上货成本":10.93000000,"销售金额":0.0000,"销售数量":0.0000,"成交单数":0,"客单价":0.000000,"退货金额":0.0000,"退货率":0.000000,"毛利贡献":null,"毛利率":null,"实时库存":1.0000000000,"未动天数":null}];
            this.gridData = newArr;
            console.log(newArr)
            return newArr
        },
        GetGrid: function(jsonArr) {
    		var me = this;
    		var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var myArr = "item_no~条码~商品名称~门店编号~门店名称~上货数量~成本均价~上货成本~销售金额~销售数量~成交单数~客单价~退货金额~退货率~毛利贡献~毛利率~实时库存~未动天数".split("~");
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if (i <= 2) {
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                } else {
                    colModel.push({ name: json, label: json.split("_")[1], align: "center", sortable: true, sorttype: 'float' })
                };
                if (json.indexOf("item_no") >= 0 || json.indexOf("门店编号") >= 0) {
                    $.extend(colModel[i], { hidden: true })
                }else if (json.indexOf("商品名称") >= 0) {
                    $.extend(colModel[i], {
                        width: 300
                    })
                } else if (json.indexOf("条码") >= 0) {
                    $.extend(colModel[i], {
                        width: 300
                    })
                } else if (json.indexOf("额") >= 0 || json.indexOf("成本") >= 0 || json.indexOf("退货金额") >= 0 || json.indexOf("毛利贡献") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                    sumCol.push(json);
                } else if (json.indexOf("价") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                } else if (json.indexOf("数") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0)
                    sumCol.push(json);
                }  else if (json.indexOf("门店名称") >= 0) {
                    $.extend(colModel[i], { width:300, formatter: top.InitData.getBranchName })
                    sumCol.push(json);
                } else if (json.indexOf("率") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } 
                if (top.CanSeeProfit == 0 && (json.indexOf("利") >= 0 || json.indexOf("成本") >= 0)) {
                    $.extend(colModel[i], { hidden: true })
                }
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 290,
                width: $('.gridCard').width() - 10,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                shrinkToFit: true,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                pager: "#pager_list",
                pagerpos: "right",
                rowList: [100, 200, 500, 1000],
                sortname: "item_no",
                sumColArr: sumCol,
                sumName: "商品名称_合计",
                avgColArr: avgCol,
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
    		var me = this;
    		var _this = $("#" + gridID);
            sumColArr.库存_售价均价 = sumColArr.库存_售价金额 / sumColArr.库存_库存数量
            sumColArr.库存_成本均价 = sumColArr.库存_成本金额 / sumColArr.库存_库存数量
            _this.footerData("set", sumColArr);
            condition.backColor_ABC(gridID, "毛利率",30,20,10)
            condition.backColor_CBA(gridID, "退货率", 3, 5, 8)
            condition.backColor_CBA(gridID, "未动天数", 15,30,60)
            _this.find("[aria-describedby*='商品名称']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='商品名称']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='采购_数量']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='采购_数量']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='上货成本']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='上货成本']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='毛利率']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='毛利率']").css("border-right", "1px dashed red")
            
        },
        afterInit: function(jsonArr) {
			var me = this;
			var groupHeaders = [
                { startColumnName: "条码", numberOfColumns: 2, titleText: "【商品信息（点击可查看采购明细）】" },
                { startColumnName: "门店编号", numberOfColumns: 5, titleText: "【货品上货分布概况，上货数量即库存数量】" },
                { startColumnName: "销售金额", numberOfColumns: 8, titleText: "【新品至今日动销概况，为动天数为最后销售日期至今日】" }
            ];
            $("#gridTable").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: groupHeaders
            });
        },
		search: function(){
			var me = this;
			$('#gridTable').GridUnload();
			me.loadGrid();
		},
		setSku: function(){
			
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
            var me = this;
            var branchNos = me.areaCode ? me.areaCode : '';
            var clses = me.clsCode?me.clsCode:"";
            var brands = me.brandCode?me.brandCode:"";
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
            var end = date ? date.split(' - ')[1] : '';
            window.location.href = baseurl + "/Report_PC/dataExport/excel?branchNos="+branchNos+"&clsNos="+clses+"&brandNos="+brands+"&begin="+begin+'&end='+end+"&busi=xinpinDongxiao";
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			me.key = '';
			me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
	    				key: me.key
	            },
	            page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
		}
	}
});