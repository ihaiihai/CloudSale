var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		areaCode: null,
		areaOptions:[],
		gridType:0,
		groupType:0,
		groupClsName:'门店名称',
		parmCls: [
            { "id": "a.branch_no", "name": "门店" },
            { "id": "cls", "name": "类别" },
            { "id": "item_brand", "name": "品牌" },
            { "id": "main_supcust", "name": "供应商" }
        ]
	},
	watch:{  
    		'gridType': 'loadGrid',
    		'groupType': "changeGroupType"
    },
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
				me.loadAreaOptions();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 290);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			this.GetGrid(this.getNewData());
		},
		changeGroupType: function(){
			if (this.groupType == 0) {
				this.groupClsName = '门店名称';
            } else {
            	this.groupClsName = '分类';
            }
			$('#gridTable').GridUnload();
			this.GetGrid(this.gridData);
		},
		getNewData: function() {
			var me = this;
			groupName = me.parmCls[me.gridType].name;
            var keyWord = {
                "col_name": "门店编号~门店名称~分类~库存数量~售价金额~成本金额~七日_销售金额~七日_销售数量~七日_销售成本~七日_周转率~三十日_销售金额~三十日_销售数量~三十日_销售成本~三十日_周转率~九十日_销售金额~九十日_销售数量~九十日_销售成本~九十日_周转率"
            }
            var newArr = [{"门店编号":"全公司","门店名称":"全公司","分类":"01","库存数量":76687.0040000000,"售价金额":12113630.95,"成本金额":8243155.72,"七日_销售金额":391637.6700,"七日_销售数量":3206.0000,"七日_销售成本":354333.76,"七日_周转率":23.26,"三十日_销售金额":2546057.2800,"三十日_销售数量":19912.0000,"三十日_销售成本":2357170.36,"三十日_周转率":3.50,"九十日_销售金额":7557788.8400,"九十日_销售数量":59505.0000,"九十日_销售成本":6807298.50,"九十日_周转率":1.21},{"门店编号":"全公司","门店名称":"全公司","分类":"02","库存数量":44704.3000000000,"售价金额":2967078.10,"成本金额":1880461.76,"七日_销售金额":64142.4000,"七日_销售数量":1112.0000,"七日_销售成本":58176.64,"七日_周转率":32.32,"三十日_销售金额":585763.6900,"三十日_销售数量":10195.0000,"三十日_销售成本":628207.43,"三十日_周转率":2.99,"九十日_销售金额":1362067.1600,"九十日_销售数量":24273.7000,"九十日_销售成本":1407309.40,"九十日_周转率":1.34},{"门店编号":"全公司","门店名称":"全公司","分类":"03","库存数量":52890.0000000000,"售价金额":1905294.70,"成本金额":625492.56,"七日_销售金额":33785.1700,"七日_销售数量":1772.0000,"七日_销售成本":23047.89,"七日_周转率":27.14,"三十日_销售金额":197504.4400,"三十日_销售数量":10027.0000,"三十日_销售成本":142763.48,"三十日_周转率":4.38,"九十日_销售金额":550857.6800,"九十日_销售数量":28985.0000,"九十日_销售成本":390765.59,"九十日_周转率":1.60},{"门店编号":"全公司","门店名称":"全公司","分类":"04","库存数量":22553.0000000000,"售价金额":1035064.80,"成本金额":466756.32,"七日_销售金额":18815.2000,"七日_销售数量":610.0000,"七日_销售成本":12603.20,"七日_周转率":37.03,"三十日_销售金额":110917.2200,"三十日_销售数量":3272.0000,"三十日_销售成本":84816.55,"三十日_周转率":5.50,"九十日_销售金额":341474.3200,"九十日_销售数量":8920.0000,"九十日_销售成本":248175.99,"九十日_周转率":1.88}];
            this.gridData = newArr;
            return newArr;
        },
        GetGrid: function(jsonArr) {
    		var me = this;
    		var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var i = 0;
            for (var json in jsonArr[0]) {
                if (i <= 5) {
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                } else {
                    colModel.push({ name: json, label: json.split("_")[1], align: "center", sortable: true, sorttype: 'float' })
                };
               if (json.indexOf("门店编号") >= 0) {
                   $.extend(colModel[i], { hidden: true })
               } else if (json.indexOf("门店名称") >= 0) {
                    if (me.gridType == 0) {
                        $.extend(colModel[i], { hidden: true })
                    } else {
                        $.extend(colModel[i], { formatter: public_data.Dic_branch })
                    }
                } else if (json.indexOf("分类") >= 0) {
                    switch (me.gridType) {
                        case 0: $.extend(colModel[i], { label: "门店名称", formatter: public_data.Dic_branch }); break
                        case 1: $.extend(colModel[i], { label: "类别", formatter: public_data.Dic_cls_all }); break
                        case 2: $.extend(colModel[i], { label: "品牌", formatter: public_data.Dic_brand }); break
                        case 3: $.extend(colModel[i], { label: "供应商", formatter: public_data.Dic_supcust }); break
                    }
                } else if (json.indexOf("金额") >= 0 || json.indexOf("成本") >= 0) {
                        $.extend(colModel[i], jqFormatter.RMB0, { summaryType: 'sum' })
                        sumCol.push(json);
                } else if (json.indexOf("数量") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("周转率") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num2)
                }
                if (top.CanSeeProfit == 0 && (json.indexOf("利") >= 0 || json.indexOf("成本") >= 0)) {
                    $.extend(colModel[i], { hidden: true })
                }
                i++
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 290,
                width: $('.gridCard').width() - 10,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                shrinkToFit: true,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                pager: "#pager_list",
                pagerpos: "right",
                rowList: [100, 200, 500, 1000],
                sortname: "分类",
                sumColArr: sumCol,
                sumName: "门店名称_合计",
                avgColArr: avgCol,
                grouping: true,
                groupingView: {
                    groupField: [me.groupClsName],
                    groupText: ['<b>{0}各品类库存周转概况</b>'],
                    groupSummary: [true],
                    groupColumnShow: [true],
                    groupCollapse: false
                },
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
    		var me = this;
    		var _this = $("#" + gridID);
            sumColArr.七日_周转率 = fixNum(sumColArr.成本金额 / sumColArr.七日_销售成本,2)
            sumColArr.三十日_周转率 = fixNum(sumColArr.成本金额 / sumColArr.三十日_销售成本,2)
            sumColArr.九十日_周转率 = fixNum(sumColArr.成本金额 / sumColArr.九十日_销售成本,2)
            _this.footerData("set", sumColArr);
            condition.backColor_CBA(gridID, "七日_周转率", 8, 16, 28)
            condition.backColor_CBA(gridID, "三十日_周转率", 2,4, 6)
            condition.backColor_CBA(gridID, "九十日_周转率", 1, 2, 3)
            if (top.CanSeeProfit == 0) {
                _this.find("[aria-describedby*='售价金额']").css("border-right", "1px dashed red")
                $(".ui-jqgrid-hdiv").find("th[id*='售价金额']").css("border-right", "1px dashed red")
            }
            _this.find("[aria-describedby*='率']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='成本金额']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='率']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='成本金额']").css("border-right", "1px dashed red")
            
            this.afterInit(this.gridData);
        },
        afterInit: function(jsonArr) {
			var me = this;
			var groupHeaders = [];
            for (var obj in jsonArr[0]) {
                if (obj.indexOf("销售金额") >= 0) {
                    groupHeaders.push({ startColumnName: obj, numberOfColumns: 4, titleText: "【" + obj.split("_")[0] + "】库存销售概况" })
                }
            }
            $("#gridTable").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: groupHeaders
            });
            var footGroup = $("#gridTable").find(".ui-widget-content.jqfoot.ui-row-ltr")
            for (var j = 0; j < footGroup.length; j++) {
                var cost = parseFloat(footGroup.eq(j).find("[aria-describedby*='成本金额']").text().replace("¥", "").replace(/,/g, ''))
                var sale_7 = parseFloat(footGroup.eq(j).find("[aria-describedby*='七日_销售成本']").text().replace("¥", "").replace(/,/g, ''))
                var sale_30 = parseFloat(footGroup.eq(j).find("[aria-describedby*='三十日_销售成本']").text().replace("¥", "").replace(/,/g, ''))
                var sale_90 = parseFloat(footGroup.eq(j).find("[aria-describedby*='九十日_销售成本']").text().replace("¥", "").replace(/,/g, ''))
                footGroup.eq(j).find("[aria-describedby*='七日_周转率']").html(sale_7 == 0 ? "" : fixNum((cost / sale_7),2));
                footGroup.eq(j).find("[aria-describedby*='三十日_周转率']").html(sale_30 == 0 ? "" : fixNum((cost / sale_30),2));
                footGroup.eq(j).find("[aria-describedby*='九十日_周转率']").html(sale_90 == 0 ? "" : fixNum((cost / sale_90),2));
            };
        },
		loadAreaOptions: function(parentAreaCode, callback){
			var me = this;
			$.getJSON(baseurl + "/sys/area/queryOptions", {
				parentAreaCode: parentAreaCode
			}, function(res) {
				if(callback){
					callback(res);
				}else{
					me.areaOptions = res;
				}
			});
		},
		search: function(){
			var me = this;
			$('#gridTable').GridUnload();
			this.GetGrid(me.getNewData());
		},
		setSku: function(){
			
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
			
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			me.key = '';
			me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
	    				key: me.key
	            },
	            page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
		}
	}
});