var pageVm = new Vue({
    el: '#page',
    components: {
        Treeselect: VueTreeselect.Treeselect
    },
    data: {
        key: "",
        grid: null,
        gridData: [],
        searchType:1,
        areaCode: null,
        areaOptions:top.branchOptionData,
        clsCode: null,
        clsOptions:top.clsOptionData,
        brandCode: null,
        brandOptions:top.brandOptionData,
        gridType:0,
        initJson: [
            { "id": "a.branch_no", "name": "门店名称~分类~有销售品牌数_10~有销售SKU数_10~销售金额_10~销售数量_10~毛利贡献_10~会员数_10~平均折扣_10~连带率_10~退货率_10~有库存品牌数_10~有库存SKU数_10~库存金额_10~库存数量_10~可用天数_10", "dic": public_data.Dic_branch, "tableData": [], "isNew": 0 },
            { "id": "cls", "name": "门店名称~分类~有销售品牌数_10~有销售SKU数_10~销售金额_10~销售数量_10~毛利贡献_10~会员数_10~平均折扣_10~连带率_10~退货率_10~有库存品牌数_10~有库存SKU数_10~库存金额_10~库存数量_10~可用天数_10", "dic": public_data.Dic_cls_all, "tableData": [], "isNew": 0 },
            { "id": "item_brand", "name": "门店名称~分类~有销售品牌数_10~有销售SKU数_10~销售金额_10~销售数量_10~毛利贡献_10~会员数_10~平均折扣_10~连带率_10~退货率_10~有库存品牌数_10~有库存SKU数_10~库存金额_10~库存数量_10~可用天数_10", "dic": public_data.Dic_brand, "tableData": [], "isNew": 0 },
            { "id": "main_supcust", "name": "门店名称~分类~有销售品牌数_10~有销售SKU数_10~销售金额_10~销售数量_10~毛利贡献_10~会员数_10~平均折扣_10~连带率_10~退货率_10~有库存品牌数_10~有库存SKU数_10~库存金额_10~库存数量_10~可用天数_10", "dic": public_data.Dic_supcust, "tableData": [], "isNew": 0 }
        ]
    },
    watch:{
        'gridType': 'loadGrid'
    },
    mounted: function(){
        this.init();
    },
    methods: {
        init: function(){
            var me = this;
            setTimeout(function(){
                me.grid = $('#gridTable');
                me.initEvent();
                me.loadGrid();
            });
        },
        initEvent: function(){
            var me = this;
            $('#page').find('.daterange').dateRange();
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    $("#gridTable").setGridHeight($(window).height() - 290);
                    $("#gridTable").setGridWidth($('.gridCard').width() - 10);
                }, 200);
                e.stopPropagation();
            });
        },
        loadGrid: function(){
            var me = this;
            var branchNos = me.areaCode ? me.areaCode : '';
            var clses = me.clsCode?me.clsCode:"";
            var brands = me.brandCode?me.brandCode:"";
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
            var end = date ? date.split(' - ')[1] : '';
            var type = ['branch','cls','item_brand'][me.gridType];
            $('#gridTable').GridUnload();
            window.showLoading();
            $.ajax({
                url: baseurl + "/Report_PC/Jiegou_Baogao/jiegou_query?branchNos="+branchNos+"&itemClses="+clses+"&brands="+brands+"&type="+type+"&begin="+begin+'&end='+end,
                type: "post",
                contentType: "application/json",
                data:JSON.stringify([{low:'0',high:'100'}]),
                dataType: "json",
                success: function (data) {
                    window.hideLoading();
                    me.GetGrid(data);
                    me.afterInit(data);
                },
                error: function(){
                    window.hideLoading();
                }
            });
        },
        GetGrid: function(jsonArr) {
            var me = this;
            var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var myArr = this.initJson[this.gridType]["name"].split("~");
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if(json.indexOf('门店名称') >= 0 || json.indexOf("分类") >= 0){
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float', frozen: true  })
                }else{
                    colModel.push($.extend({ name: json, label: json.split("_")[0], align: "center", sortable: true, sorttype: 'float' }, { width: 100 }))
                }
                if (json.indexOf("门店名称") >= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getBrandName, hidden: me.gridType ==0?true:false})
                } else if (json.indexOf("分类") >= 0) {
                    switch (me.gridType) {
                        case 0: $.extend(colModel[i], { width: 200, label: "门店名称", formatter: top.InitData.getBranchName }); break
                        case 1: $.extend(colModel[i], { width: 200, label: "类别名称", formatter: top.InitData.getClsName }); break
                        case 2: $.extend(colModel[i], { width: 200, label: "品牌名称", formatter: top.InitData.getBrandName }); break
                    }
                } else if (json.indexOf("原价金额") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0, {hidden:true, summaryType: 'sum' })
                    sumCol.push(json);
                }  else if (json.indexOf("额") >= 0 || json.indexOf("毛利") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0, { summaryType: 'sum' })
                    sumCol.push(json);
                }  else if (json.indexOf("数") >= 0 || json.indexOf("会员数") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("折扣") >= 0 || json.indexOf("退货率") > -1) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else if (json.indexOf("连带率") >= 0) {
                    $.extend(colModel[i],jqFormatter.Num2)
                } else if (json.indexOf("天数") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0)
                }

                if (top.CanSeeProfit == 0 && (json.indexOf("利") >= 0 || json.indexOf("成本") >= 0)) {
                    $.extend(colModel[i], { hidden: true })
                }
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 290,
                width: $('.gridCard').width() - 10,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                shrinkToFit: true,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                pager: "#pager_list",
                pagerpos: "right",
                rowList: [100, 200, 500, 1000],
                sortname: "分类",
                sumColArr: sumCol,
                sumName: "分类_合计",
                avgColArr: avgCol,
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
            var me = this;
            var dateNum = 1
            var _this = $("#" + gridID);
            for (var json in this.gridData[0]) {
                if (json.indexOf("折扣") >= 0) {
                    sumColArr[json] = sumColArr["销售金额_" + json.split("_")[1]] / sumColArr["原价金额_" + json.split("_")[1]]
                }
                if (json.indexOf("可用天数") >= 0) {
                    sumColArr[json] = sumColArr["库存金额_" + json.split("_")[1]] / (sumColArr["销售金额_" + json.split("_")[1]] - sumColArr["毛利贡献_" + json.split("_")[1]]) * dateNum
                }
            }

            _this.footerData("set", sumColArr);
            condition.fontColor_ABC(gridID, "连带率", 4, 3, 2)
            condition.backColor_ABC(gridID, "折扣", 80, 70, 60)
            condition.backColor_CBA(gridID, "退货率", 3, 5, 8)
            condition.backColor_CBA(gridID, "可用天数", 30,60,90)
            _this.find("[aria-describedby*='分类']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='分类']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='可用天数']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='可用天数']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='退货率']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='退货率']").css("border-right", "1px dashed red")

        },
        afterInit: function(jsonArr) {
            var me = this;
            var groupHeaders = [];
            for (var json in jsonArr[0]) {
                if (json.indexOf("有销售品牌数") >= 0) {
                    groupHeaders.push({ startColumnName: json, numberOfColumns: 15, titleText: "【价格区间】商品贡献及库存概况" })
                }
            }
            $("#gridTable").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: groupHeaders
            });
//            if (priceNow.split("'',''").length > 1) {
//                $("#gridTable").jqGrid('setFrozenColumns');
//            }
            var footGroup = $("#gridTable").find(".ui-widget-content.jqfoot.ui-row-ltr")
            for (var j = 0; j < footGroup.length; j++) {
                var saleQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='售罄_销售']").text().replace("¥", "").replace(/,/g, ''))
                var inQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='售罄_入库']").text().replace("¥", "").replace(/,/g, ''))
                var stockQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='售罄_入库']").text().replace("¥", "").replace(/,/g, ''))
                var salemoney = parseFloat(footGroup.eq(j).find("[aria-describedby*='整体销售_金额']").text().replace("¥", "").replace(/,/g, ''))
                var salemoneyB = parseFloat(footGroup.eq(j).find("[aria-describedby*='整体销售_退货额']").text().replace("¥", "").replace(/,/g, ''))
                var saleqnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='整体销售_数量']").text().replace("¥", "").replace(/,/g, ''))
                var flowqnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='整体销售_单数']").text().replace("¥", "").replace(/,/g, ''))
                var salePriceQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='库存_售价金额']").text().replace("¥", "").replace(/,/g, ''))
                var costPriceQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='库存_成本金额']").text().replace("¥", "").replace(/,/g, ''))
                footGroup.eq(j).find("[aria-describedby*='售罄_售罄率']").html(inQnty == 0 ? "" : fixNum(((saleQnty / inQnty)*100),2)+"%");
                footGroup.eq(j).find("[aria-describedby*='库存_售价均价']").html(stockQnty == 0 ? "" : fixNum((salePriceQnty / stockQnty),2));
                footGroup.eq(j).find("[aria-describedby*='库存_成本均价']").html(stockQnty == 0 ? "" : fixNum((costPriceQnty / stockQnty),2));
                footGroup.eq(j).find("[aria-describedby*='整体销售_客单价']").html(flowqnty == 0 ? "" : fixNum((salemoney / flowqnty),2));
                footGroup.eq(j).find("[aria-describedby*='整体销售_件均价']").html(saleqnty == 0 ? "" : fixNum((salemoney / saleqnty),2));
                footGroup.eq(j).find("[aria-describedby*='整体销售_退货率']").html(salemoney == 0 ? "" : fixNum(((salemoneyB / salemoney)*100),2)+"%");
            };
        },
        search: function(){
            var me = this;
            $('#gridTable').GridUnload();
            me.loadGrid();
        },
        setSku: function(){

        },
        refresh: function(){
            window.location.reload();
        },
        download: function(){
            var me = this;
            var branchNos = me.areaCode ? me.areaCode : '';
            var clses = me.clsCode?me.clsCode:"";
            var brands = me.brandCode?me.brandCode:"";
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
            var end = date ? date.split(' - ')[1] : '';
            var type = ['branch','cls','item_brand'][me.gridType];
            window.location.href =  baseurl + "/Report_PC/dataExport/excel?branchNos="+branchNos+"&clsNos="+clses+"&brandNos="+brands+"&type="+type+"&begin="+begin+'&end='+end+"&priceZones="+JSON.stringify([{low:'0',high:'100'}]+"&busi=goodStruct");
        },
        print: function(){
            $("#printTable").printTable();
        },
        reload: function(){
            var me = this;
            me.key = '';
            me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
                    key: me.key
                },
                page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
        }
    }
});