var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		searchType:1,
		areaCode: null,
		areaOptions:top.branchOptionData,
		clsCode: null,
		clsOptions:top.clsOptionData,
		brandCode: null,
		brandOptions:top.brandOptionData,
		gridType:0
	},
	watch:{  
    		'gridType': 'loadGrid'
    },
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 260);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			var me = this;
			var branchNos = me.areaCode ? me.areaCode : '';
			var clses = me.clsCode?me.clsCode:"";
			var brands = me.brandCode?me.brandCode:"";
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
			var end = date ? date.split(' - ')[1] : '';
			var type = ['cls','item_brand'][me.gridType];
			$('#gridTable').GridUnload();
			window.showLoading();
			$.get(baseurl + "/Report_PC/Shouqing/shouqing_query?branchNos="+branchNos+"&itemClses="+clses+"&brands="+brands+"&begin="+begin+'&end='+end+'&type='+type+'&pageNo=1&pageSize=1000',function(data){
				window.hideLoading();
				me.GetGrid(data);
				me.afterInit(data);
			});
		},
        GetGrid: function(jsonArr) {
    		var me = this;
    		var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var i = 0;
            var myArr = "门店名称~分类~售罄_入库~售罄_销售~售罄_库存~售罄_售罄率~整体销售_金额~整体销售_成本~整体销售_数量~整体销售_单数~整体销售_客单价~整体销售_件均价~整体销售_退货额~整体销售_退货率~库存_库存数量~库存_售价金额~库存_售价均价~库存_成本金额~库存_成本均价".split("~");
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if (i <= 1) {
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                } else {
                    colModel.push({ name: json, label: json.split("_")[1], align: "center", sortable: true, sorttype: 'float' })
                };
                if (json.indexOf("门店名称") >= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getBranchName })
                } else if (json.indexOf("分类") >= 0) {
                    switch (me.gridType) {
	                    case 0: $.extend(colModel[i], { width:300,label: "类别名称", formatter: top.InitData.getClsName }); break
	                    case 1: $.extend(colModel[i], { width: 300, label: "品牌名称", formatter: top.InitData.getBrandName }); break
                    }
                } else if (json.indexOf("额") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("价") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                } else if (json.indexOf("数量") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("售罄_销售") >= 0 || json.indexOf("售罄_入库") >= 0 || json.indexOf("售罄_库存") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json); 
                } else if (json.indexOf("整体销售_单数") >= 0 ) {
                    $.extend(colModel[i], { summaryType: 'sum' ,hidden:true})
                    sumCol.push(json);
                } else if (json.indexOf("整体销售_成本") >= 0) {
                        $.extend(colModel[i], jqFormatter.RMB0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("率") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else if (json.indexOf("售罄") >= 0 && json.indexOf("售罄率")==-1) {
                    $.extend(colModel[i], jqFormatter.Num0)
                    sumCol.push(json);
                }
                if (top.CanSeeProfit == 0 && (json.indexOf("利") >= 0 || json.indexOf("成本") >= 0)) {
                    $.extend(colModel[i], { hidden: true })
                }
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 270,
                width: $('.gridCard').width() - 10,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                shrinkToFit: true,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                pager: "#pager_list",
                pagerpos: "right",
                rowList: [100, 200, 500, 1000],
                sortname: "分类",
                sumColArr: sumCol,
                sumName: "分类_合计",
                avgColArr: avgCol,
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
    		var me = this;
    		var _this = $("#" + gridID);
            sumColArr.售罄_售罄率 = sumColArr.售罄_销售 / sumColArr.售罄_入库
            sumColArr.库存_售价均价 = sumColArr.库存_售价金额 / sumColArr.库存_库存数量
            sumColArr.库存_成本均价 = sumColArr.库存_成本金额 / sumColArr.库存_库存数量
            sumColArr.整体销售_客单价 = sumColArr.整体销售_金额 / sumColArr.整体销售_单数
            sumColArr.整体销售_件均价 = sumColArr.整体销售_金额 / sumColArr.整体销售_数量
            sumColArr.整体销售_退货率 = sumColArr.整体销售_退货额 / sumColArr.整体销售_金额
            _this.footerData("set", sumColArr);
            condition.backColor_ABC(gridID, "售罄率", 85,65,45)
            condition.backColor_CBA(gridID, "退货率", 3, 5,8)
            _this.find("[aria-describedby*='分类']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='分类']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='售罄率']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='售罄率']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='成本均价']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='成本均价']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='退货率']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='退货率']").css("border-right", "1px dashed red")
            
        },
        afterInit: function(jsonArr) {
			var me = this;
			var groupHeaders = [];
            for (var json in jsonArr[0]) {
                if (json.indexOf("售罄_入库") >= 0) {
                    groupHeaders.push({ startColumnName: json, numberOfColumns: 4, titleText: "【商品售罄概况（按数量计算）】" })
                } else if (json.indexOf("库存_库存数量") >= 0) {
                    groupHeaders.push({ startColumnName: json, numberOfColumns: 7, titleText: "【商品现有库存概况】" })
                } else if (json.indexOf("整体销售_金额") >= 0) {
                    groupHeaders.push({ startColumnName: json, numberOfColumns: 8, titleText: "【该期间整体销售概况】" })
                } 
            }
            $("#gridTable").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: groupHeaders
            });
            var footGroup = $("#gridTable").find(".ui-widget-content.jqfoot.ui-row-ltr")
            for (var j = 0; j < footGroup.length; j++) {
                var saleQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='售罄_销售']").text().replace("¥", "").replace(/,/g, ''))
                var inQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='售罄_入库']").text().replace("¥", "").replace(/,/g, ''))
                var stockQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='售罄_入库']").text().replace("¥", "").replace(/,/g, ''))
                var salemoney = parseFloat(footGroup.eq(j).find("[aria-describedby*='整体销售_金额']").text().replace("¥", "").replace(/,/g, ''))
                var salemoneyB = parseFloat(footGroup.eq(j).find("[aria-describedby*='整体销售_退货额']").text().replace("¥", "").replace(/,/g, ''))
                var saleqnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='整体销售_数量']").text().replace("¥", "").replace(/,/g, ''))
                var flowqnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='整体销售_单数']").text().replace("¥", "").replace(/,/g, ''))
                var salePriceQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='库存_售价金额']").text().replace("¥", "").replace(/,/g, ''))
                var costPriceQnty = parseFloat(footGroup.eq(j).find("[aria-describedby*='库存_成本金额']").text().replace("¥", "").replace(/,/g, ''))
                footGroup.eq(j).find("[aria-describedby*='售罄_售罄率']").html(inQnty == 0 ? "" : fixNum(((saleQnty / inQnty)*100),2)+"%");
                footGroup.eq(j).find("[aria-describedby*='库存_售价均价']").html(stockQnty == 0 ? "" : fixNum((salePriceQnty / stockQnty),2));
                footGroup.eq(j).find("[aria-describedby*='库存_成本均价']").html(stockQnty == 0 ? "" : fixNum((costPriceQnty / stockQnty),2));
                footGroup.eq(j).find("[aria-describedby*='整体销售_客单价']").html(flowqnty == 0 ? "" : fixNum((salemoney / flowqnty),2));
                footGroup.eq(j).find("[aria-describedby*='整体销售_件均价']").html(saleqnty == 0 ? "" : fixNum((salemoney / saleqnty),2));
                footGroup.eq(j).find("[aria-describedby*='整体销售_退货率']").html(salemoney == 0 ? "" : fixNum(((salemoneyB / salemoney)*100),2)+"%");
            };
        },
		search: function(){
			var me = this;
			$('#gridTable').GridUnload();
			me.loadGrid();
		},
		setSku: function(){
			
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
            var me = this;
            var branchNos = me.areaCode ? me.areaCode : '';
            var clses = me.clsCode?me.clsCode:"";
            var brands = me.brandCode?me.brandCode:"";
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
            var end = date ? date.split(' - ')[1] : '';
            var type = ['cls','item_brand'][me.gridType];
			window.location.href = baseurl + "/Report_PC/dataExport/excel?branchNos="+branchNos+"&clsNos="+clses+"&brandNos="+brands+"&begin="+begin+'&end='+end+'&type='+type+'&pageNo=1&pageSize=99999&busi=soldOutManage';
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			me.key = '';
			me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
	    				key: me.key
	            },
	            page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
		}
	}
});