var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		areaCode: null,
		areaOptions:[]
	},
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
				me.loadAreaOptions();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 260);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			this.GetGrid(this.getNewData());
		},
		getNewData: function() {
			var me = this;
			var keyWord = {
                "col_name": "营业员编号~姓名~门店~销售额~有效销售~有效数量~类型~编号~提成"
            }
            var newArr = [];
            this.colArr = {
                "ALL": ["营业员编号", "姓名", "门店", "提成合计"],
                "LB": [],
                "PP": [],
                "DP": []
            }
            var colKey ={ 
                "LB": [],
                "PP": [],
                "DP": []
            };
            var tempJsonArr = [{"营业员编号":"1101  ","姓名":"1101  ","门店":"001101","销售额":774.0000,"有效销售":774.0000,"有效数量":3.0000,"类型":"LB","编号":"0102","提成":15.479999999999999},{"营业员编号":"1101  ","姓名":"1101  ","门店":"001101","销售额":31910.3000,"有效销售":30918.3000,"有效数量":103.0000,"类型":"LB","编号":"0103","提成":618.366},{"营业员编号":"1101  ","姓名":"1101  ","门店":"001101","销售额":264.0000,"有效销售":264.0000,"有效数量":3.0000,"类型":"PP","编号":"0663                ","提成":15},{"营业员编号":"1101  ","姓名":"1101  ","门店":"001101","销售额":1363.0000,"有效销售":1363.0000,"有效数量":9.0000,"类型":"DP","编号":"单品汇总","提成":120},{"营业员编号":"1301  ","姓名":"1301  ","门店":"001301","销售额":33804.6200,"有效销售":33804.6200,"有效数量":148.0000,"类型":"LB","编号":"0103","提成":676.0924},{"营业员编号":"1301  ","姓名":"1301  ","门店":"001301","销售额":668.2500,"有效销售":668.2500,"有效数量":7.0000,"类型":"DP","编号":"单品汇总","提成":56}];
            for (var i = 0; i < tempJsonArr.length; i++) {
                var newJson = {};
                newJson["营业员编号"] = tempJsonArr[i]["营业员编号"]
                newJson["姓名"] = tempJsonArr[i]["姓名"]
                newJson["门店"] = tempJsonArr[i]["门店"]
                newJson["提成合计"] = tempJsonArr[i]["提成"]
                newJson[tempJsonArr[i]["类型"] + "_销售额_" + tempJsonArr[i]["编号"]] = tempJsonArr[i]["销售额"]
                newJson[tempJsonArr[i]["类型"] + "_有效销售_" + tempJsonArr[i]["编号"]] = tempJsonArr[i]["有效销售"]
                newJson[tempJsonArr[i]["类型"] + "_有效数量_" + tempJsonArr[i]["编号"]] = tempJsonArr[i]["有效数量"]
                newJson[tempJsonArr[i]["类型"] + "_提成_" + tempJsonArr[i]["编号"]] = tempJsonArr[i]["提成"]
                for (var j = 0; j <= newArr.length; j++) {
                    if (j == newArr.length) {
                        newArr.push(newJson)
                        break
                    }
                    if (newArr[j]["营业员编号"] == newJson["营业员编号"] && newArr[j]["门店"] == newJson["门店"]) {
                        newJson["提成合计"] = newArr[j]["提成合计"] + tempJsonArr[i]["提成"]
                        $.extend(newArr[j], newJson)
                        break;
                    }
                }
                var myArr =colKey[tempJsonArr[i]["类型"]];
                for (var k = 0; k <= myArr.length; k++) {
                    if (k == myArr.length) {
                        var temarr = [tempJsonArr[i]["类型"] + "_销售额_" + tempJsonArr[i]["编号"], tempJsonArr[i]["类型"] + "_有效销售_" + tempJsonArr[i]["编号"], tempJsonArr[i]["类型"] + "_有效数量_" + tempJsonArr[i]["编号"], tempJsonArr[i]["类型"] + "_提成_" + tempJsonArr[i]["编号"]]
                        me.colArr[tempJsonArr[i]["类型"]] = me.colArr[tempJsonArr[i]["类型"]].concat(temarr)
                        colKey[tempJsonArr[i]["类型"]].push(tempJsonArr[i]["编号"])
                        break
                    }
                    if (colKey[tempJsonArr[i]["类型"]][k]==tempJsonArr[i]["编号"]) {
                        break;
                    }
                }
            }
            me.colArr = me.colArr["ALL"].concat(me.colArr["LB"]).concat(me.colArr["PP"]).concat(me.colArr["DP"])
            return newArr
        },
        GetGrid: function(jsonArr) {
        	var me = this;
        	var colModel = [];
            var sumCol = [];
            var avgCol = [];
            for (var i=0 ;i<me.colArr.length;i++) {
                json=me.colArr[i]
                if (json.split("_").length > 1) {
                    colModel.push({ name: json, label: json.split("_")[1], align: "center", sortable: true, sorttype: 'float',width:100 })
                } else {
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float', width: 100 })
                }
                if (i<=3) {
                    $.extend(colModel[i], { frozen: true })
                }
                if (json.indexOf("姓名") >= 0 ) {
                    $.extend(colModel[i], { formatter: public_data.Dic_sale_man })
                } else if (json.indexOf("门店") >= 0) {
                    $.extend(colModel[i], { formatter: public_data.Dic_branch })
                } else if (json.indexOf("销售") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("数量") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("提成") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB2, { summaryType: 'sum' })
                    sumCol.push(json);
                };
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - 260,
                width: $('.gridCard').width() - 10,
                colModel: colModel,
                viewrecords: true,//定义是否要显示总记录数
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 1000,
                shrinkToFit: false,
                loadonce: true,  //缓存排序后才可翻页
                sortname: '门店',
                viewrecords: true,
                sortorder: "asc",
                footerrow: true,
                sumColArr: sumCol,
                sumName: "门店_合计",
                avgColArr: avgCol,                
                grouping: true,
                groupingView: {
                    groupField: ['门店'],
                    groupSummary: [true],
                    groupColumnShow: [true],
                    groupText: ['<b>{0}员工提成概况</b>'],
                    groupCollapse: false,
                },
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
        	var me = this;
        	var _this = $("#" + gridID);
            for (var i = 0 ; i < me.colArr.length; i++) {
                var obj = me.colArr[i]
                    if (obj.indexOf("活跃度") >= 0) {
                        sumColArr.整体_整体_活跃度 = fixNum((sumColArr.整体_整体_本期活跃 / sumColArr.在册会员),4)
                    }
                    if (obj.indexOf("渗透率") >= 0 ) {
                        eval("sumColArr." + obj + "= fixNum((sumColArr." + obj.replace(obj.split("_")[2], "本期活跃") + " / sumColArr.整体_整体_本期活跃),4)");
                    }
                    if (obj.indexOf("人均贡献") >= 0) {
                        eval("sumColArr." + obj + "= fixNum((sumColArr." + obj.replace(obj.split("_")[2], "销售贡献") + " / sumColArr." + obj.replace(obj.split("_")[2], "本期活跃") + "),4)");
                    }
                }
            _this.footerData("set", sumColArr);
            condition.backColor_ABC(gridID, "提成", 100,60, 20);
            _this.find("[aria-describedby*='提成']").css("border-left", "1px dashed red").css("border-right", "1px dashed red").css("background-color", "#FCF2CA")
            $(".ui-jqgrid-hdiv").find("th[id*='提成']").css("border-right", "1px dashed red").css("border-right", "1px dashed red");
            
            this.afterInit(this.gridData);
        },
        afterInit: function(jsonArr) {
        	var me = this;
        	var groupHeaders = [];
            for (var i = 0 ; i < me.colArr.length; i++) {
                var obj = me.colArr[i]
                if (obj.indexOf("销售额") > -1) {
                    if (obj.indexOf("LB") >= 0) {
                        groupHeaders.push({ startColumnName: obj, numberOfColumns: 4, titleText: "【" + obj.split("_")[2] + "（类别）提成汇总】" })
                    }
                    if (obj.indexOf("PP") >= 0) {
                        groupHeaders.push({ startColumnName: obj, numberOfColumns: 4, titleText: "【" + obj.split("_")[2] + "（品牌）提成汇总】" })
                    }
                    if (obj.indexOf("单品") >= 0) {
                        groupHeaders.push({ startColumnName: obj, numberOfColumns: 4, titleText: "【单品销售提成汇总】" })
                    }
                }
            }
            //groupHeaders.push({ startColumnName: groupCls[0], numberOfColumns: groupCls.length, titleText: "【类别提成汇总(不含有提成的单品和品牌)】" })
            //groupHeaders.push({ startColumnName: groupBrand[0], numberOfColumns: groupBrand.length, titleText: "【品牌提成汇总(不含有提成的单品)】" })
            $("#gridTable").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: groupHeaders
            });
            $("#gridTable").jqGrid('setFrozenColumns');
        },
		loadAreaOptions: function(parentAreaCode, callback){
			var me = this;
			$.getJSON(baseurl + "/sys/area/queryOptions", {
				parentAreaCode: parentAreaCode
			}, function(res) {
				if(callback){
					callback(res);
				}else{
					me.areaOptions = res;
				}
			});
		},
		search: function(){
			var me = this;
			$('#gridTable').GridUnload();
			this.GetGrid(me.getNewData());
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
			
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			me.key = '';
			me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
	    				key: me.key
	            },
	            page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
		}
	}
});