var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		areaCode: null,
		areaOptions:[],
		year: moment().format('YYYY'),
		month: moment().format('MM')
	},
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
				me.loadAreaOptions();
			});
		},
		initEvent: function(){
			var me = this;
			$('#date').datetimepicker({  
		        format: 'yyyy年mm月',  
		         autoclose: true,  
		         forceParse: false,
		         startView: 4,  
		         minView: 3,  
		         language: 'zh-CN'
		    }).val(moment().format('YYYY年MM月')).change(function(){
		    		me.year = moment($(this).val(), 'YYYY年MM月').format('YYYY');
		    		me.month = moment($(this).val(), 'YYYY年MM月').format('MM');
		    });
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 260);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			this.GetGrid();
		},
        GetGrid: function() {
        		var me = this;
        		var col = ["userName","depName","dutyName", "sumAll"]
            for (var i = 1; i <= 60; i++) {
                col.push("col" + i)
            }
            var keyWord = {
                "col_name": col.join("~")
            }
            var jsonArr = []
            var colModel = [];
            var sumCol = ["sumAll"];
            var avgCol = [];
            var i = 0;
            for (var json in jsonArr[0]) {
                if (json.indexOf("sumAll") == -1) {
                    colModel.push({ name: json, label: jsonArr[0][json], align: "center", sortable: true, sorttype: 'float', width: 100 })
                    if (i <= 2) {
                        $.extend(colModel[i], { frozen: true })
                    } else {
                        $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                        sumCol.push(json)
                    }
                    i++
                }
            }
            colModel.push($.extend({ name: "sumAll", label: "合计", summaryType: 'sum' ,width: 150, sortable: true, sorttype: 'float', align: "center", width: 80 }, jqFormatter.RMB2))
            jsonArr.splice(0, 1)
            if (jsonArr.length > 0) {
                $("#gridTable").jqGridEx({
                    datatype: "local",
                    data: jsonArr,//数据数组
                    colModel: colModel,
                    height: $(window).height() - 260,
                    width: $('.gridCard').width() - 10,
                    shrinkToFit: false,
                    rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                    rowNum: 50,
                    sortable: true,
                    loadonce: true,  //缓存排序后才可翻页
                    footerrow: true,
                    gridComplete: "",
                    sumColArr: sumCol,//合计求和列列名数组，字符串时split处理
                    sumName: "dutyName_合计",//合计文字所在列名及对应文字，用下划线分隔如XXX_小计
                    avgColArr: avgCol,//平均值求和列列名数组，字符串时split处理
                    grouping: true,
                    groupingView: {
                        groupField: ['depName'],
                        groupSummary: [true],
                        groupColumnShow: [true],
                        groupText: ['<b>{0}员工工资明细</b>'],
                        groupCollapse: false,
                    },
                    Complete: this.Complete
                })
                $("#gridTable").jqGrid('setFrozenColumns');
            } else {
                dialogMsg("无对应工资详情，请先导入工资明细查看！", 0);
            }
        },
        Complete: function(gridID, sumColArr) {
        		var me = this;
        		var _this = $("#" + gridID);
            _this.footerData("set", sumColArr);
            for (var obj in sumColArr) {
                if (sumColArr[obj] == 0) {
                    _this.setGridParam().hideCol(obj)
                }
            }
            _this.find("td").each(function () {
                if (parseFloat($(this).html().replace("¥","")) == 0) {
                    $(this).html("-")
                }
            })
            _this.find("[aria-describedby*='sumAll']").css("background-color", "#FCF2CA").css("font-weight", "700").css("border-right", "1px dashed red").css("border-left", "1px dashed red")
        },
		loadAreaOptions: function(parentAreaCode, callback){
			var me = this;
			$.getJSON(baseurl + "/sys/area/queryOptions", {
				parentAreaCode: parentAreaCode
			}, function(res) {
				if(callback){
					callback(res);
				}else{
					me.areaOptions = res;
				}
			});
		},
		search: function(){
			var me = this;
			$('#gridTable').GridUnload();
			this.GetGrid(me.getNewData());
		},
		importData: function(){
			
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
			
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			me.key = '';
			me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
	    				key: me.key
	            },
	            page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
		}
	}
});