var pageVm = new Vue({
    el: '#page',
    components: {
        Treeselect: VueTreeselect.Treeselect
    },
    data: {
        key: "",
        grid: null,
        gridData: [],
        areaCode: null,
        areaOptions:top.branchOptionData,
        ruleName:'',
        gridType:-1
    },
    watch:{
        'gridType': function(){
            $('#gridTable').GridUnload();
            this.loadGrid();
        }
    },
    mounted: function(){
        this.init();
    },
    methods: {
        init: function(){
            var me = this;
            setTimeout(function(){
                me.grid = $('#gridTable');
                me.initEvent();
                me.loadGrid();
                me.setClsModal = top.addModal($('#setClsModal')[0]);
                me.addItemModal = top.addModal($('#addItemModal')[0]);
            });
        },
        initEvent: function(){
            var me = this;
            $('#page').find('.daterange').dateRange();
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    $("#gridTable").setGridHeight($(window).height() - $('.form-inline').height() - 180);
                    $("#gridTable").setGridWidth($('.gridCard').width() - 10);
                }, 200);
                e.stopPropagation();
            });
        },
        setClsCommision: function(){
            top.showModal('setClsModal');
            top.setClsModalVm.clear();
            top.addItemModalVm.clear();
        },
        loadGrid: function(){
			var me = this;
			me.getNewData(function(data){
				me.GetGrid(data);
			});
		},
        getNewData: function(callback) {
            var me = this;
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
            var end = date ? date.split(' - ')[1] : '';
            window.showLoading();
            $.ajax({
                url: baseurl + "/Report_PC/Deduct_Rule/queryRules?branchNos="+(me.areaCode?me.areaCode:"")+"&ruleName="+encodeURI(encodeURI(me.ruleName))+"&isEnable="+me.gridType+"&begin="+begin+"&end="+end,
                type: "get",
                dataType: "json",
                success: function (data) {
                	var myData = [];
                    for (var i = 0; i < data.length; i++) {
                        var newJson = {};
                        newJson.编号 = data[i].deduct_no;
                        newJson.名称 = data[i].deduct_name;
                        newJson.摘要 = data[i].deduct_desc;
                        newJson.开始时间 = moment(data[i].begin_date).format('YYYY-MM-DD');
                        newJson.结束时间 = moment(data[i].end_date).format('YYYY-MM-DD');
                        newJson.操作员 = data[i].user_id;
                        newJson.操作日期 = moment(data[i].gmt_modified).format('YYYY-MM-DD');
                        newJson.生效 = data[i].is_enable;
                        newJson.编辑 = data[i].deduct_no;
                        newJson.删除 = data[i].deduct_no;
                        myData.push(newJson)
                    }
                    window.hideLoading();
                    if(callback){
                		callback(myData);
                    }
                },
                error: function(){
            		window.hideLoading();
                }
            });
        },
        getRuleDetail: function(deductNo) {
            var me = this;
            var ruleData = null;
            $.ajax({
                url: baseurl + "/Report_PC/Deduct_Rule/rule_detail?deductNo="+deductNo,
                type: "get",
                cache: false,
                async: false,
                dataType: "json",
                success: function (data) {
                    ruleData = data;
                }
            });
            return ruleData;
        },
        enableRule:function (deductNo,isEnable) {
            var me = this;
            $.ajax({
                url: baseurl + "/Report_PC/Deduct_Rule/setEnable?deductNo="+deductNo+"&isEnable="+isEnable,
                type: "get",
                cache: false,
                async: false,
                dataType: "json",
                success: function (data) {
                    me.search();
                }
            });
        },
        deleteRule: function(deductNo){
        	var me = this;
            $.ajax({
                url: baseurl + "/Report_PC/Deduct_Rule/deleteRule?deductNo="+deductNo,
                type: "get",
                dataType: "json",
                success: function (data) {
                    me.search();
                }
            });
        },
        GetGrid: function (jsonArr) {
            var me = this;
            var colNames = ("编号~名称~摘要~开始时间~结束时间~操作员~操作日期~生效~编辑~删除").split("~");
            var colModel = [];
            var sumCol = [];
            var avgCol = [];
            for (var i = 0; i < colNames.length; i++) {
                var json = colNames[i]
                colModel.push({ name: colNames[i], label: colNames[i], align: "center", sortable: true, sorttype: 'float' })
                if (i <= 0) {
                    // $.extend(colModel[i], { hidden: true })
                }else if(json == "生效"){
                    $.extend(colModel[i], { width:50, formatter: function (cellvalue, options, rowObject) {
                        return cellvalue == 1 ? '<i class="fa fa-toggle-on" style="cursor:pointer;"></i>' : '<i class="fa fa-toggle-off" style="cursor:pointer;"></i>';
                    }})
                }else if(json == "编辑"){
                    $.extend(colModel[i], { width:50, formatter: function (cellvalue, options, rowObject) {
                        return '<i class="mdi dripicons-document-edit" style="cursor:pointer;"></i>';
                    }})
                }else if(json == "删除"){
                    $.extend(colModel[i], { width:50, formatter: function (cellvalue, options, rowObject) {
                        return '<i class="mdi dripicons-trash" style="cursor:pointer;"></i>';
                    }})
                }
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                sortable: true,
                footerrow: false,
                shrinkToFit: true,
                sumColArr: sumCol,
                sumName: "clsName_合计",
                avgColArr: avgCol,
                height: $(window).height() - $('.form-inline').height() - 180,
                width: $('.gridCard').width() - 10,
                pager: "#gridPager",
                pagerpos: "right",
                loadonce: true,  //缓存排序后才可翻页
                rowList: [100, 200, 300, 500],
                onCellSelect: function (rowid, iCol, contents, event) {
                    var deductNo = $(this).getCell(rowid, 1);
                    if(iCol===8){//操作：停止：启动
                        var isEnable = $(this).getCell(rowid, 8);
                        if(isEnable.indexOf("fa-toggle-on") != -1){
                            me.enableRule(deductNo,0);
                        }else{
                            me.enableRule(deductNo,1);
                        }
                    }else if(iCol===9){
                    	top.setClsModalVm.tabIndex = 0;
                    	top.setClsModalVm.formData = me.getRuleDetail(deductNo);
                        top.showModal('setClsModal');
                    }else if(iCol===10){
                    	me.deleteRule(deductNo);
                    }
                }
            })
        },
        search: function(){
            var me = this;
            $("#gridTable").GridUnload();
            this.loadGrid();
        },
        refresh: function(){
            window.location.reload();
        },
        download: function(){
            var me = this;
            var myData = [];
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
            var end = date ? date.split(' - ')[1] : '';
            window.location.href = baseurl + "/Report_PC/dataExport/excel?branchNos="+(me.areaCode?me.areaCode:"")+"&likeName="+encodeURI(encodeURI(me.ruleName))+"&type="+me.gridType+"&begin="+begin+"&end="+end+"&busi=commisionSet";
        },
        print: function(){
            $("#printTable").printTable();
        },
        reload: function(){
            var me = this;
            $("#gridTable").GridUnload();
            this.loadGrid();
        }
    }
});
top.setClsModalVm = new Vue({
    el: '#setClsModal',
    components: {
        Treeselect: VueTreeselect.Treeselect
    },
    data: {
        formData : {
            deductRuleMasterDto:{
                deduct_no:"",
                deduct_name:"",
                deduct_desc:"",
                begin_date: '',
                end_date: '',
//                begin_end_date:moment().format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
                long_valid:0,

                mission_cycle:0,
                use_range:0,
                branch_nos:null,
                saler_ids:null,

                cal_type:0,
                cal_mode:0,
                deduct_rate1:100,
                deduct_rate2:0,
                deduct_rate3:0,
                deduct_rate4:0,

                deduct_mode:0,
                full_mode:0,
                min_money:0,
                reward_money:0,
                reward_cal:0
            },
            deductRuleTargetDtoList:[],
            deductRuleDetailDtoList:[],
            deductRuleExceptionDtoList:[],
            scopeType: 1,
            programType: 1
        },
        ruleData:[],
        itemData:[],
        exceptItemData:[],
        areaOptions:top.branchOptionData,
        salerOptions:top.salerOptionData,
        tabIndex: 0,
        deduct_name_error_msg: false,
        valid_date_error_msg: false,
        deduct_rate_error_msg: false
    },
    computed: {
        deduct_mode: function() {
        		return this.formData.deductRuleMasterDto.deduct_mode;
        },
        cal_mode: function(){
        		return this.formData.deductRuleMasterDto.cal_mode;
        },
        cal_type: function(){
        		return this.formData.deductRuleMasterDto.cal_type;
        }
    },
    watch: {
    		deduct_mode: {
            handler: function(value,oldValue) {
               console.log(value)
               if(value == 4 || value == 5){
            	   		if(this.formData.deductRuleMasterDto.cal_mode != 0){
            	   			this.formData.deductRuleMasterDto.deduct_mode = oldValue;
            	   			dialogMsg('如果选择PK计算方式必须是单独计算', 0);
            	   		}
               }
            }
        },
        cal_mode: {
        		handler: function(value,oldValue) {
                if(value == 1){
         	   		if(this.formData.deductRuleMasterDto.cal_type == 0){
         	   			this.formData.deductRuleMasterDto.cal_mode = 0;
         	   			dialogMsg('如果是按单计算，计算方式必须单独', 0);
         	   		}
                }
             }
        },
        cal_type: {
        		handler: function(value,oldValue) {
                if(value == 0){
         	   		if(this.formData.deductRuleMasterDto.cal_mode == 1){
         	   			this.formData.deductRuleMasterDto.cal_type = 1;
         	   			dialogMsg('如果选择合计计算，就不能选择按单计算', 0);
         	   		}
                }
             }
        }
    },
    mounted: function(){
        this.init();
    },
    methods: {
        init: function(){
            var me = this;
            setTimeout(function(){
                me.initEvent();
            });
        },
        initEvent: function(){
            var me = this;
            pageVm.setClsModal.on('shown.bs.modal ', function (e) {
                pageVm.setClsModal.find('.date').datetimepicker({
                    format:'yyyy-mm-dd',
                    autoclose: true,
                    forceParse: false,
                    startView: 2,
                    minView: 2,
                    language: 'zh-CN'
                }).val('');
                pageVm.setClsModal.find('.begin_date').val(me.formData.deductRuleMasterDto.begin_date).change(function(){
                    me.formData.deductRuleMasterDto.begin_date = $(this).val();
                });
                pageVm.setClsModal.find('.end_date').val(me.formData.deductRuleMasterDto.end_date).change(function(){
                    me.formData.deductRuleMasterDto.end_date = $(this).val();
                });
                if(me.formData.deductRuleMasterDto.long_valid == 'true'){
                	me.formData.deductRuleMasterDto.long_valid = true;
                }else{
                	me.formData.deductRuleMasterDto.long_valid = 0;
                }
            });
        },
        changeRewardValue: function(obj,index){
        	if(obj.cal_type == 1 || obj.cal_type == 2){
        		if(obj.reward_value * 1 > 100){
        			this.formData.deductRuleTargetDtoList[index].reward_value = 100;
        		}
        	}
        },
        addRule: function(type){
        		var cal_type = 1;
        		if(this.formData.deductRuleTargetDtoList.length > 0){
        			cal_type = this.formData.deductRuleTargetDtoList[this.formData.deductRuleTargetDtoList.length - 1].cal_type;
        		}
            this.formData.deductRuleTargetDtoList.push({
                target_money: 0,
                cal_type: cal_type,
                reward_value: 0,
                punish_money: 0
            });
        },
        deleteRule: function(index){
            this.formData.deductRuleTargetDtoList.splice(index, 1);
        },
        addItem: function(){
            top.showModal('addItemModal');
            if(this.formData.deductRuleMasterDto.deduct_mode==0){
                top.addItemModalVm.modalTitle = '添加商品';
                top.addItemModalVm.type=0;
                top.addItemModalVm.loadData();
            }else if(this.formData.deductRuleMasterDto.deduct_mode==1){
                top.addItemModalVm.modalTitle = '添加分类';
                top.addItemModalVm.type=1;
                top.addItemModalVm.loadData();
            }else if(this.formData.deductRuleMasterDto.deduct_mode==2){
                top.addItemModalVm.modalTitle = '添加品牌';
                top.addItemModalVm.type=2;
                top.addItemModalVm.loadData();
            }else if(this.formData.deductRuleMasterDto.deduct_mode==3){
                top.addItemModalVm.modalTitle = '添加商品';
                top.addItemModalVm.type=3;
                top.addItemModalVm.loadData();
            }
        },
        deleteItem: function(index){
            this.formData.deductRuleDetailDtoList.splice(index, 1);
        },
        addExceptItem: function(){
            top.showModal('addItemModal');
            top.addItemModalVm.clear();
            top.addItemModalVm.modalTitle = '添加商品';
            top.addItemModalVm.type=-1;
            top.addItemModalVm.loadData();
        },
        deleteExceptItem: function(index){
            this.formData.deductRuleExceptionDtoList.splice(index, 1);
        },
        save : function(){
            var me = this;
            if(!this.valid()){
                return;
            }
            $.ajax({
                type: "POST",
                contentType:"application/json",
                url: baseurl + '/Report_PC/Deduct_Rule/submit_rule',
                data: JSON.stringify(me.formData),
                dataType: 'json',
                success: function(res){
                    top.hideModal('setClsModal');
                    pageVm.reload();
                }
            });
        },
        valid: function(){
            console.log(this.formData)
            var validSate = true;
            if(this.tabIndex == 0){
            	if(!this.formData.deductRuleMasterDto.deduct_name){
            		this.deduct_name_error_msg = true;
            		validSate = false;
            	}else{
            		this.deduct_name_error_msg = false;
            	}
            	if(this.formData.deductRuleMasterDto.long_valid == 0 && (!this.formData.deductRuleMasterDto.begin_date || !this.formData.deductRuleMasterDto.end_date)){
            		this.valid_date_error_msg = true;
            		validSate = false;
            	}else{
            		this.valid_date_error_msg = false;
            	}
            	var totalRate = (this.formData.deductRuleMasterDto.deduct_rate1*1) + (this.formData.deductRuleMasterDto.deduct_rate2*1) + (this.formData.deductRuleMasterDto.deduct_rate3*1) + (this.formData.deductRuleMasterDto.deduct_rate4*1);
            	if(totalRate != 0 && totalRate != 100){
            		this.deduct_rate_error_msg = true;
            		validSate = false;
            	}else{
            		this.deduct_rate_error_msg = false;
            	}
            }
            return validSate;
        },
        clear: function(){
            this.formData = {
                deductRuleMasterDto:{
                    decuct_no:"",
                    deduct_name:"",
                    deduct_desc:"",
                    begin_date: '',
                    end_date: '',
//                    begin_end_date:moment().format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
                    long_valid:0,

                    mission_cycle:0,
                    use_range:0,
                    branch_nos:null,
                    saler_ids:null,

                    cal_type:0,
                    cal_mode:0,
                    deduct_rate1:100,
                    deduct_rate2:0,
                    deduct_rate3:0,
                    deduct_rate4:0,

                    deduct_mode:0,
                    full_mode:0,
                    min_money:0,
                    reward_money:0,
                    reward_cal:0
                },
                deductRuleTargetDtoList:[],
                deductRuleDetailDtoList:[],
                deductRuleExceptionDtoList:[],
                scopeType: 1,
                programType: 1
            };
            this.ruleData = [];
            this.tabIndex = 0;
            this.$forceUpdate();
        },
        preTab: function(){
            if(this.tabIndex > 0){
                this.tabIndex--;
            }
        },
        nextTab: function(){
            if(this.tabIndex == 0){
                var height = pageVm.setClsModal.find('.tab-item').eq(0).height();
                pageVm.setClsModal.find('.grid1').height(height - 178);
                pageVm.setClsModal.find('.grid1-add').css('margin-top',(height - 165) / 2 - 30);
                pageVm.setClsModal.find('.grid2').height(height - 53);
                pageVm.setClsModal.find('.grid3').height(height - 53);
            }
            if(this.formData.deductRuleMasterDto.deduct_mode < 4 && this.tabIndex == 3){
                if(this.valid()){
                    this.save();
                }
            }else if(this.formData.deductRuleMasterDto.deduct_mode >= 4 && this.tabIndex == 2){
                if(this.valid()){
                    this.save();
                }
            }else{
            	if(this.valid()){
                    this.tabIndex++;
                }
            }
        }
    }
});
top.addItemModalVm = new Vue({
    el: '#addItemModal',
    components: {
        Treeselect: VueTreeselect.Treeselect
    },
    data: {
        modalTitle:'',
        type:0,//0:商品1:分类2:品牌,3商品组合,-1:例外商品
        errorMsg:"",
        clsNo:top.clsOptionData[0].id,
        key:'',
        itemData:[],
        clsData:[],
        brandData:[],
        clsOptions: top.clsOptionData,
        allChecked: false
    },
    mounted: function(){
        this.init();
    },
    methods: {
        init: function(){
            var me = this;
            setTimeout(function(){
                me.initEvent();
            });
        },
        initEvent: function(){
            var me = this;
        },
        checkAll: function(){
        	if(this.allChecked){
        		this.allChecked = false;
        		if(this.type == 0 || this.type == 3 || this.type == -1){
        			$.each(this.itemData, function(index,item){
            			item.checked = 0;
            		});
        		}else if(this.type == 1){
        			$.each(this.clsData, function(index,item){
            			item.checked = 0;
            		});
        		}else if(this.type == 2){
        			$.each(this.brandData, function(index,item){
            			item.checked = 0;
            		});
        		}
        	}else{
        		this.allChecked = true;
        		if(this.type == 0 || this.type == 3 || this.type == -1){
        			$.each(this.itemData, function(index,item){
            			item.checked = 1;
            		});
        		}else if(this.type == 1){
        			$.each(this.clsData, function(index,item){
            			item.checked = 1;
            		});
        		}else if(this.type == 2){
        			$.each(this.brandData, function(index,item){
            			item.checked = 1;
            		});
        		}
        	}
        },
        loadData: function(){
        	var me = this;
        	if(this.type == 0 || this.type == 3 || this.type == -1){
        		$.ajax({
                    url: baseurl + "/salary/set/queryByCondition",
                    data:JSON.stringify({clsNo:(me.clsNo?me.clsNo:''), key:me.key}),
                    type: "POST",
     			    contentType:"application/json",
                    dataType: "json",
                    success: function (data) {
                        me.itemData = data;
                    }
    			});
        	}else if(this.type == 1){
        		if(this.key){
        			var arr = [];
        			$.each(top.clsData, function(i,item){
        				if(item.item_clsname.indexOf(me.key) != -1 || item.item_clsno.indexOf(me.key) != -1){
        					arr.push(item);
        				}
        			});
        			this.clsData = arr;
        		}else{
        			this.clsData = top.clsData;
        		}
        	}else if(this.type == 2){
        		if(this.key){
        			var arr = [];
        			$.each(top.brandData, function(i,item){
        				if(item.item_brandname.indexOf(me.key) != -1 || item.item_brandno.indexOf(me.key) != -1){
        					arr.push(item);
        				}
        			});
        			this.brandData = arr;
        		}else{
        			this.brandData = top.brandData;
        		}
        	}
        },
        clear:function () {
            var me = this;
            this.key = '';
        },
        save: function(){
            var me = this;
            me.errorMsg="";
            if(this.type == 0){
            	$.each(this.itemData, function(index,item){
        			if(item.checked == 1){
        				top.setClsModalVm.formData.deductRuleDetailDtoList.push({
        					info_no : item.item_no,
        					info_name : item.item_name
        				});
        			}
        		});
            }else if(this.type == 1){
            	$.each(this.clsData, function(index,item){
        			if(item.checked == 1){
        				top.setClsModalVm.formData.deductRuleDetailDtoList.push({
        					info_no : item.item_clsno,
        					info_name : item.item_clsname
        				});
        			}
        		});
            }else if(this.type == 2){
            	$.each(this.brandData, function(index,item){
        			if(item.checked == 1){
        				top.setClsModalVm.formData.deductRuleDetailDtoList.push({
        					info_no : item.item_brandno,
        					info_name : item.item_brandname
        				});
        			}
        		});
            }else if(this.type == 3){
            	$.each(this.itemData, function(index,item){
        			if(item.checked == 1){
        				top.setClsModalVm.formData.deductRuleDetailDtoList.push({
        					group_no : '1',
        					info_no : item.item_no,
        					info_name : item.item_name,
        					deduct_value : 1
        				});
        			}
        		});
            }else if(this.type == -1){
            	$.each(this.itemData, function(index,item){
        			if(item.checked == 1){
        				top.setClsModalVm.formData.deductRuleExceptionDtoList.push({
        					item_no : item.item_no,
        					item_name : item.item_name
        				});
        			}
        		});
            }
            top.hideModal('addItemModal');
        }
    }
});
