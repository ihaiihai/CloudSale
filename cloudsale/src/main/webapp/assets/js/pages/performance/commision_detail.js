var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		grid: null,
		gridData: [],
		areaCode: null,
		areaOptions:top.branchOptionData,
		date: moment().startOf('month').format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
		queryName: '选择门店',
		salerCode: null,
		salerOptions: top.salerOptionData
	},
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 250);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			var me = this;
			me.getNewData(function(data){
				me.GetGrid(data);
			});
		},
		getNewData: function(callback) {
			var me = this;
			var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
			var end = date ? date.split(' - ')[1] : '';
			window.showLoading();
        	$.ajax({
                url: baseurl + "/salary/detail/queryByCondition?begin="+begin+'&end='+end+'&branchNo='+(me.areaCode?me.areaCode:''+'&salerIds='+(me.salerCode?me.salerCode:'')),
                type: "get",
                dataType: "json",
                success: function (data) {
                	window.hideLoading();
                    if(callback){
                		callback(data);
                    }
                },
                error: function(){
            		window.hideLoading();
                }
			});
        },
        GetGrid: function(data) {
        		var me = this;
        		var keyWord = {
                "col_name": "日期~门店~营业员~方案编码~方案说明~流水号~商品编码~商品名称~规格~单位~分类~品牌~销售数量~销售金额~毛利~提成金额~分成~扣款~审核状态~审核时间~审核人",
                "key_name": "oper_date~branch_no~saler_id~deduct_no~deduct_desc~voucher_no~item_no~item_name~item_size~unit_no~item_clsno~item_brandno~sale_qty~sale_amt~profit~deduct_amt~share_amt1~off_amt~confirm_flag~confirm_date~confirm_man"
            }
            var colNames = keyWord.col_name.split("~");
        	var keyNames = keyWord.key_name.split("~");
            var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var i = 0;
            for (var i = 0; i < colNames.length; i++) {
                var json = colNames[i];
                var name = keyNames[i];
                colModel.push({ name: name, label: json, align: "center", sortable: true, sorttype: 'float' })
                if (json.indexOf("商品名称") >= 0) {
                    $.extend(colModel[i], { width: 350 })
                } else if (json.indexOf("门店") >= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getBranchName })
                } else if (json.indexOf("营业员") >= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getSalerName })
                } else if (json.indexOf("item_no") >= 0 || json.indexOf("利润") >= 0) {
                    $.extend(colModel[i], { hidden: true })
                    sumCol.push(name);
                } else if (json.indexOf("条码") >= 0) {
                    $.extend(colModel[i], { width: 200 })
                } else if (json.indexOf("价") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB2)
                    avgCol.push(name);
                } else if (json.indexOf("日期") >= 0 || json.indexOf("时间") >= 0) {
                    $.extend(colModel[i], { sorttype: 'date', formatter: DateUtil.format})
                } else if (json.indexOf("量") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0)
                    sumCol.push(name);
                } else if ( json.indexOf("额") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB2)
                    sumCol.push(name);
                } else if (json.indexOf("提成汇总") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB2)
                    sumCol.push(name);
                } else if (json.indexOf("比例") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else if (json.indexOf("备注") >= 0) {
                    $.extend(colModel[i], { width: 250 })
                } else if (json.indexOf("审核状态") >= 0) {
                    $.extend(colModel[i], { formatter: function(value){
                    		if(value == 1){
                    			return '已审核';
                    		}else{
	                			return '';
	                		}
                    } })
                }
                if (top.CanSeeProfit == 0 && (json.indexOf("利") >= 0 || json.indexOf("成本") >= 0)) {
                    $.extend(colModel[i], { hidden: true })
                }
            }
            $("#gridTable").jqGridEx({
            	datatype: "local",
                data: data,//数据数组
                height: $(window).height() - 250,
                width: $('.gridCard').width() - 10,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                sortable: true,
                footerrow: true,
                sumColArr: sumCol,
                sumName: "商品名称_合计(价格为均价)",
                avgColArr: avgCol,
                pager: "#gridPager",
                pagerpos: "right",
                loadonce: true,  //缓存排序后才可翻页
                rowList: [100, 200, 500, 1000],
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
        		var me = this;
        		var _this = $("#" + gridID);
            _this.footerData("set", sumColArr);
            condition.backColor_ABC(gridID, "deduct_amt", 0, 0,0);
            //_this.find("[aria-describedby*='折']").css("background-color", "#FCF2CA")
            _this.find("[aria-describedby*='deduct_amt']").css("border-left", "1px dashed red").css("border-right", "1px dashed red").css("background-color", "#FCF2CA")
            $(".ui-jqgrid-hdiv").find("th[id*='deduct_amt']").css("border-left", "1px dashed red").css("border-right", "1px dashed red")

            _this.find("[aria-describedby*='sale_qty']").css("background-color", "#FCF2CA")
            _this.find("[aria-describedby*='sale_amt']").css("background-color", "#FCF2CA")
            _this.find("[aria-describedby*='sale_amt']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='sale_amt']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='item_brandno']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='item_brandno']").css("border-right", "1px dashed red")
        },
		search: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.loadGrid();
		},
		confirm: function(){
			var me = this;
			window.showLoading();
			$.ajax({
                url: baseurl + "/salary/detail/updateConfirm?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),
                type: "get",
                dataType: "json",
                success: function (data) {
                		window.hideLoading();
                    me.reload();
                },
                error: function(){
            			window.hideLoading();
                }
			});
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
            var me = this;
            window.location.href = baseurl + "/Report_PC/dataExport/excel?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+"&busi=commisionDetail";
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.loadGrid();
		}
	}
});