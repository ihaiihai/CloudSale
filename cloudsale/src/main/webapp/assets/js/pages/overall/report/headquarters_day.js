var pageVM = new Vue({
    el : '#page',
    data : {
        data1: null,
        data2: null,
        data3: null,
        data4: null,
        data8: null,
        data9: null,
        chart1Data:null,
        chart2Title:[{"标题1":"【2018-12-30】销售共计：￥164312"}],
        chart2SubTitle:[{"name":"本月任务：￥7200000,目前已完成￥5400038,完成率：75%,预测完成率：75%"}],
        chart2Data:null,
        chart3Data:[{"时间":"10","昨日时段销售额":11742,"同期时段销售额":30625},{"时间":"11","昨日时段销售额":16821,"同期时段销售额":23115},{"时间":"12","昨日时段销售额":12281,"同期时段销售额":14749},{"时间":"13","昨日时段销售额":11070,"同期时段销售额":7419},{"时间":"14","昨日时段销售额":10083,"同期时段销售额":18059},{"时间":"15","昨日时段销售额":27177,"同期时段销售额":15366},{"时间":"16","昨日时段销售额":14910,"同期时段销售额":33278},{"时间":"17","昨日时段销售额":17469,"同期时段销售额":20594},{"时间":"18","昨日时段销售额":9559,"同期时段销售额":20392},{"时间":"19","昨日时段销售额":13004,"同期时段销售额":12009},{"时间":"20","昨日时段销售额":8688,"同期时段销售额":6688},{"时间":"21","昨日时段销售额":47,"同期时段销售额":-10898},{"时间":"22","昨日时段销售额":0,"同期时段销售额":150},{"时间":"7","昨日时段销售额":0,"同期时段销售额":58},{"时间":"8","昨日时段销售额":3437,"同期时段销售额":792},{"时间":"9","昨日时段销售额":8022,"同期时段销售额":6934}],
        chart3Title:[{"标题2":"昨日整体客流【1151人】,同比去年下降10.84%,\n客单价【142.8元/人】,同比去年下降7.51%"}],
        chart4Data:null,
        chart8Data:null,
        chart9Data:null,
        chart10Data:[{"分类":"000601_8","门店":"000601","时间":8,"昨日客流":0,"昨日客单价":0.0,"昨日时段销售额":0.0000,"同期客流":1,"同期客单价":10.0,"同期时段销售额":10.0000},{"分类":"000601_9","门店":"000601","时间":9,"昨日客流":0,"昨日客单价":0.0,"昨日时段销售额":0.0000,"同期客流":3,"同期客单价":52.0,"同期时段销售额":156.0000},{"分类":"000601_10","门店":"000601","时间":10,"昨日客流":2,"昨日客单价":9.5,"昨日时段销售额":19.0000,"同期客流":14,"同期客单价":274.6,"同期时段销售额":3844.5500},{"分类":"000601_11","门店":"000601","时间":11,"昨日客流":11,"昨日客单价":140.4,"昨日时段销售额":1544.9200,"同期客流":9,"同期客单价":878.2,"同期时段销售额":7904.0000},{"分类":"000601_12","门店":"000601","时间":12,"昨日客流":6,"昨日客单价":98.3,"昨日时段销售额":589.8000,"同期客流":6,"同期客单价":102.1,"同期时段销售额":612.5000},{"分类":"000601_13","门店":"000601","时间":13,"昨日客流":0,"昨日客单价":0.0,"昨日时段销售额":0.0000,"同期客流":12,"同期客单价":86.9,"同期时段销售额":1043.2200},{"分类":"000601_14","门店":"000601","时间":14,"昨日客流":14,"昨日客单价":98.5,"昨日时段销售额":1379.6100,"同期客流":9,"同期客单价":290.4,"同期时段销售额":2613.5000},{"分类":"000601_15","门店":"000601","时间":15,"昨日客流":11,"昨日客单价":88.3,"昨日时段销售额":971.0000,"同期客流":15,"同期客单价":297.7,"同期时段销售额":4465.0300},{"分类":"000601_16","门店":"000601","时间":16,"昨日客流":12,"昨日客单价":228.1,"昨日时段销售额":2737.7500,"同期客流":22,"同期客单价":243.5,"同期时段销售额":5357.8300},{"分类":"000601_17","门店":"000601","时间":17,"昨日客流":21,"昨日客单价":165.3,"昨日时段销售额":3471.1400,"同期客流":15,"同期客单价":359.8,"同期时段销售额":5396.2600},{"分类":"000601_18","门店":"000601","时间":18,"昨日客流":12,"昨日客单价":113.0,"昨日时段销售额":1356.2000,"同期客流":9,"同期客单价":429.6,"同期时段销售额":3866.2900},{"分类":"000601_19","门店":"000601","时间":19,"昨日客流":6,"昨日客单价":62.8,"昨日时段销售额":376.8200,"同期客流":12,"同期客单价":249.3,"同期时段销售额":2991.3500},{"分类":"000601_20","门店":"000601","时间":20,"昨日客流":4,"昨日客单价":601.5,"昨日时段销售额":2405.9100,"同期客流":12,"同期客单价":126.8,"同期时段销售额":1521.2000},{"分类":"000601_21","门店":"000601","时间":21,"昨日客流":0,"昨日客单价":0.0,"昨日时段销售额":0.0000,"同期客流":1,"同期客单价":-3558.0,"同期时段销售额":-3558.0000}],
        chart11Data:[{"name":"000901","销售":56832.8400,"退货":7426.4900,"退货率":13.07},{"name":"000601","销售":23981.3400,"退货":4337.0000,"退货率":18.08},{"name":"001101","销售":15404.8100,"退货":3678.5000,"退货率":23.88},{"name":"000301","销售":14364.0500,"退货":2969.0000,"退货率":20.67},{"name":"000401","销售":10419.0500,"退货":3412.0000,"退货率":32.75},{"name":"000801","销售":9710.7000,"退货":2275.9000,"退货率":23.44},{"name":"001701","销售":9519.6300,"退货":939.8000,"退货率":9.87},{"name":"002901","销售":8491.9000,"退货":393.0000,"退货率":4.63},{"name":"003501","销售":8466.7600,"退货":4366.0000,"退货率":51.57},{"name":"001501","销售":8114.5400,"退货":3531.7000,"退货率":43.52},{"name":"000501","销售":7395.7600,"退货":737.9000,"退货率":9.98},{"name":"000201","销售":6424.3100,"退货":2212.0000,"退货率":34.43},{"name":"002601","销售":5004.0000,"退货":98.0000,"退货率":1.96},{"name":"001901","销售":4270.5000,"退货":1751.0000,"退货率":41.00},{"name":"003001","销售":3799.1000,"退货":2258.4000,"退货率":59.45},{"name":"001401","销售":3511.0800,"退货":90.0000,"退货率":2.56},{"name":"002801","销售":3062.8000,"退货":0.0000,"退货率":0.00},{"name":"000701","销售":2409.8300,"退货":555.0000,"退货率":23.03},{"name":"003301","销售":2093.1000,"退货":535.6000,"退货率":25.59},{"name":"003101","销售":1820.5100,"退货":138.5000,"退货率":7.61},{"name":"001301","销售":1759.5000,"退货":0.0000,"退货率":0.00},{"name":"003201","销售":1416.6100,"退货":312.0000,"退货率":22.02},{"name":"002301","销售":887.8000,"退货":0.0000,"退货率":0.00},{"name":"003401","销售":489.3000,"退货":368.2000,"退货率":75.25}],
        chart12Data:[{"name":"000201","value":48.23},{"name":"000301","value":55.14},{"name":"000401","value":60.92},{"name":"000501","value":57.91},{"name":"000601","value":61.38},{"name":"000701","value":50.26},{"name":"000801","value":58.39},{"name":"000901","value":59.65},{"name":"001101","value":53.05},{"name":"001301","value":43.17},{"name":"001401","value":57.02},{"name":"001501","value":55.71},{"name":"001701","value":60.29},{"name":"001901","value":47.46},{"name":"002301","value":79.13},{"name":"002601","value":57.55},{"name":"002801","value":70.62},{"name":"002901","value":65.38},{"name":"003001","value":57.39},{"name":"003101","value":47.92},{"name":"003201","value":57.26},{"name":"003301","value":43.57},{"name":"003401","value":60.64},{"name":"003501","value":61.15}],
        chart13Data:[{"name":"000201","value":50.19},{"name":"000301","value":105.62},{"name":"000401","value":105.24},{"name":"000501","value":90.19},{"name":"000601","value":134.73},{"name":"000701","value":46.34},{"name":"000801","value":70.37},{"name":"000901","value":100.77},{"name":"001101","value":149.56},{"name":"001301","value":109.97},{"name":"001401","value":152.66},{"name":"001501","value":150.27},{"name":"001601","value":0.00},{"name":"001701","value":144.24},{"name":"001901","value":44.48},{"name":"002301","value":295.93},{"name":"002601","value":200.16},{"name":"002801","value":510.47},{"name":"002901","value":283.06},{"name":"003001","value":237.44},{"name":"003101","value":72.82},{"name":"003201","value":94.44},{"name":"003301","value":104.66},{"name":"003401","value":48.93},{"name":"003501","value":112.89}],
        chart14Data:[{"name":"000201","value":0.97},{"name":"000301","value":1.22},{"name":"000401","value":1.58},{"name":"000501","value":1.48},{"name":"000601","value":2.25},{"name":"000701","value":1.31},{"name":"000801","value":1.17},{"name":"000901","value":1.79},{"name":"001101","value":2.02},{"name":"001301","value":2.44},{"name":"001401","value":1.96},{"name":"001501","value":1.43},{"name":"001701","value":2.43},{"name":"001901","value":1.04},{"name":"002301","value":2.67},{"name":"002601","value":2.75},{"name":"002801","value":3.50},{"name":"002901","value":2.70},{"name":"003001","value":2.31},{"name":"003101","value":1.54},{"name":"003201","value":1.27},{"name":"003301","value":3.20},{"name":"003401","value":1.90},{"name":"003501","value":1.59}],
        chart15Data:[{"name":"伊利","value":23110.9000},{"name":"飞鹤","value":15854.4000},{"name":"海普诺凯1897","value":9997.4000},{"name":"美赞臣","value":9526.9000},{"name":"澳优","value":6888.0000},{"name":"惠氏","value":6549.0000},{"name":"美素","value":5286.9000},{"name":"合生元","value":4561.3300},{"name":"婴尚","value":4312.4400},{"name":"其他","value":4057.5100},{"name":"牛栏","value":3210.0000},{"name":"英氏","value":2484.9200},{"name":"佳贝艾特.","value":2040.0000},{"name":"爱他美","value":1770.0000},{"name":"亲益","value":1629.0000},{"name":"优莱美","value":1101.0000}]
    },
    mounted : function() {
        this.init();
    },
    methods : {
        init : function() {
            var me = this;
            setTimeout(function() {
                me.initEvent();
                me.loadData();
            });
        },
        refresh: function(){
    		this.getChart9Data();
    	},
    	print: function(){
            $("#printTable").printTable();
    	},
        initEvent: function(){
            var me = this;
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    $("#gridPanel_1").setGridWidth($("#gridPanel_1").parents('.card-body').width());
                }, 200);
            });
        },
        loadData: function(){
            this.getChart1Data();
            this.getChart2Data();
            this.getChart3Data();
            this.getChart4Data();
            this.getChart8Data();
            this.getChart9Data();
        },
        getChart1Data: function(){
            var me = this;
            me.chart1Data = null;
            $.get(baseurl + "/Report_PC/Company_DayMonth/branch_sale_tongqi",function(data){
                me.chart1Data = data;
                setTimeout(function(){
                    me.createChart1(data);
                });
            });
        },
        getChart2Data: function(){
            var me = this;
            me.chart2Data = null;
            $.get(baseurl + "/Report_PC/Company_DayMonth/title",function(data){
                me.chart2Title = data;
                $.get(baseurl + "/Report_PC/Company_DayMonth/month_aim_wancheng",function(data){
                    if(data && data.length > 0){
                        me.chart2SubTitle = data;
                    }
                    $.get(baseurl + "/Report_PC/Company_DayMonth/yes_month_wancheng",function(data){
                        me.chart2Data = data;
                        setTimeout(function(){
                            me.createChart2(data);
                        });
                    });
                });
            });
        },
        getChart3Data: function(){
            var me = this;
            me.chart3Data = null;
            $.get(baseurl + "/Report_PC/Company_DayMonth/small_title",function(data){
                setTimeout(function(){
                    me.chart3Title = data;
                    $.get(baseurl + "/Report_PC/Company_DayMonth/shijianduan",function(data){
                        me.chart3Data = data;
                        setTimeout(function(){
                        	me.createChart3(me.chart3Data);
                        },200);
                    });
                });
            });
        },
        getChart4Data: function(){
            var me = this;
            me.chart4Data = null;
            $.get(baseurl + "/Report_PC/Company_DayMonth/ret_dis_danjia_liandai",function(data){
                me.chart4Data = data;
                console.log(me.chart4Data)
                setTimeout(function(){
                    me.createChart4(data);
                    me.createChart5(data);
                    me.createChart6(data);
                    me.createChart7(data);
                });
            });
        },
        getChart8Data: function(){
            var me = this;
            me.chart8Data = null;
            $.get(baseurl + "/Report_PC/Company_DayMonth/cls_fenbu",function(data){
                me.chart8Data = data;
                setTimeout(function(){
                    me.createChart8(data);
                });
            });
        },
        getChart9Data: function(){
            var me = this;
            me.chart9Data = null;
            $.get(baseurl + "/Report_PC/Company_DayMonth/saler_sale",function(data){
                me.chart9Data = data;
                setTimeout(function(){
                    me.createChart9(data);
                });
            });
        },
        createChart1 : function(data) {
            if(!data || data.length == 0){
                return;
            }
            var me = this;
            var data1 = [];
            var data2 = [];
            var data3 = [];
            for(var i=0,len=data.length; i<len; i++){
                data1.push(top.InitData.getBranchName(data[i]['门店']));
                data2.push(data[i]['实销']);
                data3.push(data[i]['同期实销']);
            }
            var option = {
                title: {
                    text: '●各门店销售完成状况(点击可看详情)',
                    x: 'left'
                },
                tooltip: {
                    show: true,
                    formatter: "{b}<br />{a}: {c}元",
                },
                toolbox: {
                    feature: {
                        restore: {
                            show: true
                        }
                    },
                    top: '4%'
                },
                legend: {
                    data: ['昨日实销', '同期实销'],
                    top: '6%'
                },
                grid: {
                    left: '2%',
                    right: '2%',
                    bottom: '0%',
                    containLabel: true
                },
                xAxis: {
                    show: false,
                    type: 'value',
                    boundaryGap: [0, 0.2]
                },
                yAxis: {
                    type: 'category',
                    data: data1,
                    position: 'right',
                    splitArea: {
                        show: true
                    },
                },
                series: [
                    {
                        name: '昨日实销',
                        type: 'bar',
                        label: {
                            normal: {
                                show: true,
                                position: 'right',
                                formatter: '￥{c}'
                            }

                        },
                        data: data2,
                        markLine: {
                            label: {
                                normal: {
                                    show: false,
                                    position: 'end',
                                    formatter: '{b}{c}%'
                                }
                            },
                            lineStyle: {
                                normal: {
                                    color: 'blue'
                                }
                            },
                            symbol: 'circle',
                            data: [
                                { name: '平均值', type: 'average' }
                            ]
                        }
                    },
                    {
                        name: '同期实销',
                        type: 'bar',
                        label: {
                            normal: {
                                show: true,
                                position: 'right',
                                formatter: '￥{c}'
                            }
                        },
                        data: data3
                    }
                ]
            };
            ECharts.Draw(option, "chart1");
        },
        createChart2 : function(data) {
            if(!data || data.length == 0){
                return;
            }
            var option = {
                title: {
                    text: '全公司' + this.chart2Title[0]['标题1'],
                    subtext: this.chart2SubTitle[0]['name'],
                    x: 'center',
                    top: '10%',
                    textStyle: {
                        fontSize: 22
                    }
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "月累计销售：" + data[0]['月累计销售'].toFixed(2) + "元，完成率：" + data[0].完成率 + "%"
                },
                grid: {
                    top: '55%',
                    left: '0%',
                    right: '0%',
                    bottom: '5%',
                    containLabel: true
                },
                xAxis: {
                    max: 100,
                    axisLabel: {
                        show: true,
                        formatter: "{value}%",
                    },
                    axisLine: {
                        show: false
                    }

                },
                yAxis: [{
                    show: false,
                    data: ['xx'],
                    axisLine: {
                        show: false
                    }
                }, {
                    // 辅助 x 轴
                    show: false,
                    data: [100]
                }],

                series: [{
                    // 辅助系列
                    type: 'bar',
                    silent: true,
                    yAxisIndex: 1,
                    itemStyle: {
                        normal: {
                            barBorderRadius: 120,
                            color: '#ddd'
                        }
                    },
                    barWidth: 15,
                    data: [100]
                }, {
                    type: 'bar',
                    data: data[0].完成率,
                    markPoint: {
                        symbol: 'roundRect',
                        symbolSize: 40,
                        data: [{
                            type: 'max'
                        }],
                        label: {
                            normal: {
                                formatter: '{c}%',
                                show: true,
                            }
                        }
                    },
                    barWidth: 15,
                    itemStyle: {
                        normal: {
                            barBorderRadius: 180,
                            //color: '#59c4e6',#7b93e0
                            color: '#7b93e0'
                        }
                    }
                }]
            };
            ECharts.Draw(option, "chart2");
        },
        createChart3 : function(data) {
            if(!data || data.length == 0){
                return;
            }
            var option = {
                title: {
                    text: this.chart3Title[0]['标题2'],
                    x: 'center',
                    textStyle: {
                        fontSize: 14
                    }
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                grid: {
                    top: '40%',
                    left: '0%',
                    right: '0%',
                    bottom: '5%',
                    containLabel: true
                },
                legend: {
                    data: ['昨日时段销售额', '同期时段销售额'],
                    top: '20%'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: public_data.getEchartsData(data, '时间', ''),
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '成交额',
                        axisLabel: {
                            formatter: '{value}元'
                        }
                    }
                ],
                series: [{
                    name: '昨日时段销售额',
                    type: 'line',
                    symbolSize: 2,
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                color: "rgba(123, 147, 224, 0.2)",
                                type: "default"
                            },
                            lineStyle: {
                                width: 1
                            }
                        }
                    },
                    smooth: true,
                    data: public_data.getEchartsData(data, '昨日时段销售额', ''),
                },
                    {
                        name: '同期时段销售额',
                        type: 'line',
                        smooth: true,
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(89, 196, 230, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        data: public_data.getEchartsData(data, '同期时段销售额', ''),
                    }
                ]
            };
            ECharts.Draw(option, "chart3");
        },
        createChart4 : function(data) {
            if(!data || data.length == 0){
                return;
            }
            var me = this;
            var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: '平均退货率',
                    type: 'pie',
                    radius: [
                        '70%',
                        '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: data[0].平均退货率,
                        name: '平均退货率',

                        label: {
                            normal: {
                                formatter: '{d} %',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: 100 - (data[0].平均退货率),
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n平均退货率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
            ECharts.Draw(option, "chart4", function(params){
                top.showModal('chartModal');
                top.chartModal.modalTitle = '全公司各门店销售及退货情况';
                top.chartModal.showLoading = true;
                $.get(baseurl + "/Report_PC/Company_DayMonth/branch_sale_ret",function(data){
                    top.chartModal.showLoading = false;
                    me.chart11Data = data;
                    me.createChart11(data);
                });
            });
        },
        createChart5 : function(data) {
            if(!data || data.length == 0){
                return;
            }
            var me = this;
            var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: '平均折扣率',
                    type: 'pie',
                    radius: [
                        '70%',
                        '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: data[0].平均折扣率,
                        name: '平均折扣率',

                        label: {
                            normal: {
                                formatter: '{d}%',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: 100 - data[0].平均折扣率,
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n平均折扣率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
            ECharts.Draw(option, "chart5", function(params){
                top.showModal('chartModal');
                top.chartModal.modalTitle = '各门店折扣分布';
                top.chartModal.showLoading = true;
                $.get(baseurl + "/Report_PC/Company_DayMonth/branch_sale_dis",function(data){
                    top.chartModal.showLoading = false;
                    me.createChart12(data);
                });
            });
        },
        createChart6 : function(data) {
            if(!data || data.length == 0){
                return;
            }
            var me = this;
            var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: '客单价',
                    type: 'pie',
                    radius: [
                        '70%',
                        '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: data[0].客单价,
                        name: '客单价',

                        label: {
                            normal: {
                                formatter: '{c}',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: (data[0].客单价 < 500 ? 500 : data[0].客单价 * 1.2) - (data[0].客单价),
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n客单价',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
            ECharts.Draw(option, "chart6", function(params){
                top.showModal('chartModal');
                top.chartModal.modalTitle = '各门店客单价分布';
                top.chartModal.showLoading = true;
                $.get(baseurl + "/Report_PC/Company_DayMonth/branch_sale_client",function(data){
                    top.chartModal.showLoading = false;
                    me.createChart13(data);
                });
            });
        },
        createChart7 : function(data) {
            if(!data || data.length == 0){
                return;
            }
            var me = this;
            var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: '连带率',
                    type: 'pie',
                    radius: [
                        '70%',
                        '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: data[0].连带率,
                        name: '连带率',

                        label: {
                            normal: {
                                formatter: '{c}',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: (data[0].连带率 < 10 ? 10 : data[0].连带率 * 1.1) - (data[0].连带率),
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n连带率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
            ECharts.Draw(option, "chart7", function(params){
                top.showModal('chartModal');
                top.chartModal.modalTitle = '各门店连带率分布';
                top.chartModal.showLoading = true;
                $.get(baseurl + "/Report_PC/Company_DayMonth/branch_sale_liandailv",function(data){
                    top.chartModal.showLoading = false;
                    me.createChart14(data);
                });
            });
        },
        createChart8 : function(data) {
            if(!data || data.length == 0){
                return;
            }
            var me = this;
            var option = {
                title: {
                    top: '2%',
                    text: "●各类别销售分布",
                    x: 'left'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                grid: {
                    top: '20%'
                },
                series: [
                    {
                        name: '销售分布',
                        type: 'pie',
                        radius: ['55%', '80%'],
                        center: ['50%', '55%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '25',
                                    fontWeight: 'normal'
                                },
                                formatter: "{b}\n{c}"
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: data
                    }
                ]
            };
            var dom = document.getElementById("chart8");
            var myChart = echarts.init(dom, 'cloudsale-echart-theme');
            this.show(myChart, option)
            myChart.setOption(option, true);
            myChart.on('click', function (params) {
                var clsId = public_data.Name(params.name, 'cls_all')
                top.showModal('chartModal');
                top.chartModal.modalTitle = '【'+params.name+'】各品牌详情';
                $.get(baseurl + "/Report_PC/Company_DayMonth/cls_fenbu_detail?itemClsName="+encodeURI(encodeURI(params.name)),function(data){
                	top.chartModal.showLoading = false;
            		if(data && data.length > 0){
            			top.chartModal.noData = false;
        				setTimeout(function(){
        					me.createChart15(data);
        				},200);
            		}else{
            			top.chartModal.noData = true;
            		}
                });
            });
            $(window).resize(function () {
                myChart.resize();
            });
        },
        createChart9 : function(data) {
            if(!data || data.length == 0){
                return;
            }
            var $gridTable = $("#gridPanel_1");
            $gridTable.jqGrid({
                datatype: "local",
                data:data,
                width: $('.gridCard').width() - 10,
                height: 230,
                colModel: [
                    { name: "营业员编号", label: "营业员编号", align: "center", sortable: true, sorttype: 'float' },
                    { name: "姓名", label: "姓名", align: "center", sortable: true },
                    $.extend({ name: "销售金额", label: "销售金额", align: "center", sortable: true, sorttype: 'float' }, jqFormatter.RMB0),
                    $.extend({ name: "客单价", label: "客单价", align: "center", sortable: true, sorttype: 'float' }, jqFormatter.RMB0),
                    $.extend({ name: "连带率", label: "连带率", align: "center", sortable: true, sorttype: 'float' }, jqFormatter.Num2)
                ],
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 1000,
                loadonce: true
            });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    $gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        },
        createChart10: function(data){
            var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                    data: ['昨日客流', '同期客流', '昨日客单价', '同期客单价'],
                    top: '1%'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: public_data.getEchartsData(data, '时间', ''),
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '客流量',
                        axisLabel: {
                            formatter: '{value} /人'
                        }
                    },
                    {
                        type: 'value',
                        name: '累计销售',
                        axisLabel: {
                            formatter: '{value}/元'
                        }
                    }
                ],
                series: [
                    {
                        name: '昨日客流',
                        type: 'bar',
                        data: public_data.getEchartsData(data, '昨日客流', ''),
                    },
                    {
                        name: '同期客流',
                        type: 'bar',
                        data: public_data.getEchartsData(data, '同期客流', ''),
                    },
                    {
                        name: '昨日客单价',
                        type: 'line',
                        yAxisIndex: 1,
                        data: public_data.getEchartsData(data, '昨日客单价', ''),
                    },
                    {
                        name: '同期客单价',
                        type: 'line',
                        yAxisIndex: 1,
                        data: public_data.getEchartsData(data, '同期客单价', ''),
                    }
                ]
            };
            ECharts.Draw(option, top.document.getElementById('chartModalCon'));
        },
        createChart11: function(data){
            if(!data || data.length == 0){
                return;
            }
            var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                    data: ['销售', '退货', '退货率'],
                    top: '2%'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: public_data.getEchartsData(data, 'name', 'branch'),
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '金额',
                        axisLabel: {
                            formatter: '{value} /元'
                        }
                    },
                    {
                        type: 'value',
                        name: '百分比',
                        axisLabel: {
                            formatter: '{value}/%'
                        }
                    }
                ],
                series: [
                    {
                        name: '销售',
                        type: 'bar',
                        data: public_data.getEchartsData(data, '销售', ''),
                    },
                    {
                        name: '退货',
                        type: 'bar',
                        data: public_data.getEchartsData(data, '退货', ''),
                    },
                    {
                        name: '退货率',
                        type: 'line',
                        yAxisIndex: 1,
                        data: public_data.getEchartsData(data, '退货率', ''),
                    },
                ]
            };
            ECharts.Draw(option, top.document.getElementById('chartModalCon'));
        },
        createChart12: function(data){
            if(!data || data.length == 0){
                return;
            }
            var data1 = [];
            var data2 = [];
            $.each(data,function(i,item){
                data1.push(item.name);
                data2.push(item.value.replace('%','') * 1);
            });
            var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '百分比',
                        axisLabel: {
                            formatter: '{value}/%'
                        }
                    }
                ],
                series: [
                    {
                        name: '缺货率',
                        type: 'line',
                        label: {
                            normal: {
                                show: true,
                                position: 'top'
                            }
                        },
                        data: data2,
                    },
                ]
            };
            ECharts.Draw(option, top.document.getElementById('chartModalCon'));
        },
        createChart13: function(data){
            if(!data || data.length == 0){
                return;
            }
            var data1 = [];
            var data2 = [];
            for(var i=0,len=data.length; i<len; i++){
                data1.push(top.InitData.getBranchName(data[i].name));
                data2.push(data[i].value);
            }
            var option = {
                title: {
                    text: '各门店客单价分布',
                    left: 'center'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '百分比',
                        axisLabel: {
                            formatter: '{value}/%'
                        }
                    }
                ],
                series: [
                    {
                        name: '客单价',
                        type: 'line',
                        label: {
                            normal: {
                                show: true,
                                position: 'top'
                            }
                        },
                        data: data2,
                    },
                ]
            };
            ECharts.Draw(option, top.document.getElementById('chartModalCon'));
        },
        createChart14: function(data){
            if(!data || data.length == 0){
                return;
            }
            var data1 = [];
            var data2 = [];
            for(var i=0,len=data.length; i<len; i++){
                data1.push(top.InitData.getBranchName(data[i].name));
                data2.push(data[i].value);
            }
            var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        //name: '百分比',
                        axisLabel: {
                            formatter: '{value}'
                        }
                    }
                ],
                series: [
                    {
                        name: '连带率',
                        type: 'line',
                        label: {
                            normal: {
                                show: true,
                                position: 'top'
                            }
                        },
                        data: data2,
                    },
                ]
            };
            ECharts.Draw(option, top.document.getElementById('chartModalCon'));
        },
        createChart15: function(data){
            if(!data || data.length == 0){
                return;
            }
            var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [
                    {
                        name: '品牌',
                        type: 'pie',
                        radius: '55%',
                        center: ['50%', '50%'],
                        data: data,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    formatter: '{b} : ￥{c}',
                                    textStyle: {
                                        color: '#333'
                                    }
                                },
                                labelLine: { show: true }
                            }
                        }
                    }
                ]
            };
            ECharts.Draw(option, top.document.getElementById('chartModalCon'));
        },
        show: function(myChart, option) {
            var me = this;
            var app = {};
            app.currentIndex = -1;
            showChart = setInterval(function () {
                var dataLen = option.series[0].data.length;
                // 取消之前高亮的图形
                myChart.dispatchAction({
                    type: 'downplay',
                    seriesIndex: 0,
                    dataIndex: app.currentIndex
                });
                app.currentIndex = (app.currentIndex + 1) % dataLen;
                // 高亮当前图形

                myChart.dispatchAction({
                    type: 'highlight',
                    seriesIndex: 0,
                    dataIndex: app.currentIndex
                });
                pieIndex = app.currentIndex
            }, 1000)
            myChart.on('mousemove', function (params) {
                window.clearInterval(showChart)
                if (app.currentIndex != null) {
                    myChart.dispatchAction({
                        type: 'downplay',
                        seriesIndex: 0,
                        dataIndex: app.currentIndex
                    });
                    app.currentIndex = null;
                }
            });
            myChart.on('globalout', function (params) {
                if (app.currentIndex == null) {
                    me.show(myChart, option)
                }
            });
        }
    }
});