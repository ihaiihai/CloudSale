var pageVM = new Vue({
	el : '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data : {
		gridTab: 0,
		areaCode: null,
		areaOptions:top.branchOptionData,
		date: moment().startOf('month').format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
		data1:null,
		data2:null,
		data3:null,
		data4:[{"name":"食品","value":74998.00},{"name":"纸尿裤","value":198.00},{"name":"孕产妇类","value":0.00},{"name":"鞋帽袜散货件","value":0.00},{"name":"洗护用品","value":0.00},{"name":"婴童装外出服","value":0.00},{"name":"物料","value":0.00},{"name":"喂养用品","value":0.00},{"name":"床品内衣宝宝套","value":0.00}],
		data5:[{"名称":"舒比奇高护柔薄舒爽纸尿裤M码","退货金额":0.0000},{"名称":"乐得宝7Q企鹅舒绒棉无缝肩开套12-24M","退货金额":0.0000},{"名称":"加菲猫优护婴儿洗发沐浴露860g","退货金额":0.0000},{"名称":"三妹竹纤维（童巾）1条装25*50","退货金额":0.0000},{"名称":"芊婴小屋婴儿纱布小方巾","退货金额":0.0000},{"名称":"舒比奇柔薄舒爽纸尿片L码大包","退货金额":198.0000},{"名称":"启赋(厅2)","退货金额":776.0000},{"名称":"澳优有机能力多(厅1)","退货金额":856.0000},{"名称":"澳优金装幼优(厅3)","退货金额":952.0000},{"名称":"超级飞帆900g(厅3)","退货金额":72414.0000}]
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.loadData();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
		},
		loadData: function(){
			this.getData1();
			this.getData2();
			this.getData3();
			this.getData4();
			this.getData5();
		},
		getData1 : function() {
			var me = this;
			me.data1 = null;
			$('#gridPanel').GridUnload();
			$.get(baseurl + "/Report_PC/Branch_RejectReport/branch_item_reject_detail?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.data1 = data;
				setTimeout(function(){
					me.GetGrid(data);
				});
			});
		},
		getData2 : function() {
			var me = this;
			me.data2 = null;
			$.get(baseurl + "/Report_PC/Branch_RejectReport/branch_ret_retlv?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.data2 = data;
				setTimeout(function(){
					me.createChart1(data);
					me.createChart2(data);
				});
			});
		},
		getData3 : function() {
			var me = this;
			me.data3 = null;
			$.get(baseurl + "/Report_PC/Branch_RejectReport/branch_sale_reject_retlv?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.data3 = data;
				setTimeout(function(){
					me.createChart3(data);
				});
			});
		},
		getData4 : function() {
			var me = this;
			me.data4 = null;
			$.get(baseurl + "/Report_PC/Branch_RejectReport/branch_reject_top10?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.data4 = data;
				setTimeout(function(){
					me.createChart5(data);
				});
			});
		},
		getData5 : function() {
			var me = this;
			me.data5 = null;
			$.get(baseurl + "/Report_PC/Branch_RejectReport/branch_cls_reject_fenbu?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.data5 = data;
				setTimeout(function(){
					me.createChart4(data);
				});
			});
		},
		createChart1 : function(data) {
			var me = this;
			var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: data[0].name,
                    type: 'pie',
                    radius: [
                    '70%',
                    '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: data[0].value.toFixed(2),
                        name: data[0].name,

                        label: {
                            normal: {
                                formatter: '￥{c}',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: data[0].max - data[0].value,
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n退货金额',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
			ECharts.Draw(option, "chart1");
		},
		createChart2 : function(data) {
			var me = this;
			var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: data[1].name,
                    type: 'pie',
                    radius: [
                    '70%',
                    '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: data[1].value,
                        name: data[1].name,

                        label: {
                            normal: {
                                formatter: '{d} %',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: 100 - (data[1].value),
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n退货率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
			ECharts.Draw(option, "chart2");
		},
		createChart3 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '●各品类实销、退货及退货率分布',
                    x: 'left'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                    data: ['实销金额', '退货金额', '退货率'],
                    top: '2%',
                    x:'right'
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '0%',
                    top: '15%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        data: public_data.getEchartsData(data, '类别', ''),
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        axisLabel: {
                            formatter: '{value}元'
                        }
                    },
                    {
                        type: 'value',
                        scale: true,
                        splitLine: {
                            show: false
                        },
                        axisLabel: {
                            formatter: '{value}%'
                        }
                    }
                ],
                series: [
                    {
                        name: '实销金额',
                        stack: 1,
                        type: 'bar',
                        data: public_data.getEchartsData(data, '实销金额', ''),
                    },
                    {
                        name: '退货金额',
                        stack: 1,
                        type: 'bar',
                        data: public_data.getEchartsData(data, '退货金额', ''),
                    },
                    {
                        name: '退货率',
                        type: 'line',
                        yAxisIndex: 1,
                        data: public_data.getEchartsData(data, '退货率', ''),
                    }
                ]
            };
			ECharts.Draw(option, "chart3");
		},
		createChart4 : function(data) {
			var me = this;
			if(data && data.length > 0){
				$('#chart4').find('.data_none').hide();
			}else{
				$('#chart4').find('.data_none').show();
				return;
			}
			var option = {
                title: {
                    text: '各品类退货分布',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{b} : {c} ({d}%)"
                },
                series: [
                {
                    type: 'pie',
                    radius: '55%',
                    data: data,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                formatter: '{b} : ￥{c}',
                                textStyle: {
                                    color: '#333'
                                }
                            },
                            labelLine: { show: true }
                        }
                    }
                }
                ]
            };
			ECharts.Draw(option, "chart4");
		},
		createChart5 : function(data) {
			var me = this;
			if(data && data.length > 0){
				$('#chart5').find('.data_none').hide();
			}else{
				$('#chart5').find('.data_none').show();
				return;
			}
			var option = {
                title: {
                    text: '单品退货TOP10(按退货金额排行)',
                    x: 'center'
                },
                tooltip: {
                    show: true,
                    formatter: "{b}<br />{c}元",
                },
                grid: {
                    left: '0%',
                    right: '20%',
                    bottom: '0%',
                    top:"10%"
                },
                xAxis: {
                    show: false,
                    type: 'value',
                },
                yAxis: {
                    type: 'category',
                    data: public_data.getEchartsData(data, '名称', ''),
                    position: 'right',
                    splitArea: {
                        show: true
                    },
                },
                series: [
                {
                    type: 'bar',
                    data: public_data.getEchartsData(data, '退货金额', '')
                }
                ]
            };
			ECharts.Draw(option, "chart5");
		},
		GetGrid: function (data) {
			if(data == null || data.length == 0)return;
			var $gridTable = $("#gridPanel");
            $gridTable.jqGridEx({
            	datatype: "local",
                data:data,
                colModel: [
                    { name: "条码", label: "条码", align: "center", sortable: false },
                    { name: "名称", label: "名称", width:250, align: "center", sortable: false },
                    { name: "数量", label: "数量", align: "center", sortable: true, sorttype: 'int' },
                    { name: "金额", label: "退货金额", align: "center", sortable: true, sorttype: 'float' },
                    { name: "卡号", label: "卡号", align: "center", sortable: false, sorttype: 'float' }
                ],
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                loadonce:true,
                height: 350,
                width: $('.gridCard').width() - 10
            });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        }
	}
});