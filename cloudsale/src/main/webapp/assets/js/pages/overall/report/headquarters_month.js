var pageVM = new Vue({
	el : '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data : {
		gridTab: 0,
		areaCode: null,
		areaOptions:top.branchOptionData,
		year: moment().format('YYYY'),
		month: moment().format('MM'),
		classArr: ["label-success", "label-info", "label-primary", "label-warning", "label-danger", "label-info"],
        unitArr1: ["(元)", "(元)", "(单)", "(元/单)", "(件/单)"],
        unitArr2: ["(人)", "(人)", "(人)", "(元)", "(元/人)", "(件/单)"],
        chart1Data:null,
        chart2Data:null,
        chart3Data:null,
        chart4Data:null,
        grid1Data:null,
        grid2Data:null,
        grid3Data:null,
		data1:[{"name":"2019-01-01至2019-01-17实时指标"}],
		data2:[{"name":"销售完成","value1":1707705.00,"value2":"完成率47.46%"},{"name":"毛利","value1":278933.00,"value2":"毛利率16.33%"},{"name":"成交单数","value1":10394.00,"value2":"同比下降15963"},{"name":"客单价","value1":164.30,"value2":"同比上涨8.49"},{"name":"连带率","value1":3.20,"value2":"同比下降0.24"}],
		data3:[{"name":"在册人数","value1":38417,"value2":"365天内办卡或消费"},{"name":"新增会员","value1":645,"value2":"新增率1.68%"},{"name":"活跃客户","value1":5206,"value2":"活跃度13.55%"},{"name":"会员贡献","value1":1702757,"value2":"贡献占比99.71%"},{"name":"人均贡献","value1":327.08,"value2":"会员人均消费金额"},{"name":"人均数量","value1":6.38,"value2":"会员人均购买数量"}],
		data4:[{"value":47.46}],
		data5:[{"日期":1,"本期":393493,"环比":92893,"同比":526769},{"日期":2,"本期":96036,"环比":94681,"同比":128805},{"日期":3,"本期":63088,"环比":83455,"同比":85598},{"日期":4,"本期":59960,"环比":91771,"同比":89579},{"日期":5,"本期":80868,"环比":103762,"同比":96223},{"日期":6,"本期":96707,"环比":87133,"同比":92176},{"日期":7,"本期":77279,"环比":59199,"同比":112002},{"日期":8,"本期":72909,"环比":345586,"同比":103672},{"日期":9,"本期":73042,"环比":279180,"同比":115820},{"日期":10,"本期":67545,"环比":219421,"同比":101374},{"日期":11,"本期":96236,"环比":211402,"同比":112442},{"日期":12,"本期":103289,"环比":340794,"同比":111675},{"日期":13,"本期":118455,"环比":116616,"同比":123092},{"日期":14,"本期":108566,"环比":148713,"同比":114932},{"日期":15,"本期":94280,"环比":163624,"同比":75230},{"日期":16,"本期":80980,"环比":191447,"同比":103875},{"日期":17,"本期":24972,"环比":133353,"同比":135934}],
		data6:[{"name":"测试09店","value":503877.6600},{"name":"测试06店","value":154807.4800},{"name":"测试05店","value":116280.9300},{"name":"测试08店","value":92930.6800},{"name":"测试04店","value":88608.1000},{"name":"测试03店","value":69114.1700},{"name":"测试11店","value":66611.9000},{"name":"测试15店","value":64594.4800},{"name":"测试35店","value":60137.3300},{"name":"测试17店","value":59478.7600},{"name":"测试13店","value":58005.2200},{"name":"测试02店","value":48372.4000},{"name":"测试31店","value":48032.2700},{"name":"测试07店","value":45890.1500},{"name":"测试16店","value":38652.5000},{"name":"测试19店","value":37557.9800},{"name":"测试29店","value":36787.2500},{"name":"测试26店","value":30120.8400},{"name":"测试32店","value":18412.9000},{"name":"测试33店","value":15567.6800},{"name":"测试28店","value":13825.9200},{"name":"测试14店","value":13438.1700},{"name":"测试34店","value":11536.7200},{"name":"测试30店","value":10929.0900},{"name":"测试23店","value":4134.7400}],
		data7:[{"name":"食品","value":989186},{"name":null,"value":266554},{"name":"纸尿裤","value":190339},{"name":"洗护用品","value":84217},{"name":"喂养用品","value":51282},{"name":"床品内衣宝宝套","value":37586},{"name":"婴童装外出服","value":22453},{"name":"游泳馆","value":21579},{"name":"鞋帽袜散货件","value":15638},{"name":"孕产妇类","value":15420},{"name":"产后护理","value":10528},{"name":"童车","value":1523},{"name":"玩具","value":1252},{"name":"其它类别","value":777},{"name":"赠品","value":24},{"name":"物料","value":4}],
		data8:[{"门店":"000201","任务":98299.6599319864,"完成率":0.49209122425722424,"本期销售":48372.4000,"本期销量":1335.0000,"本期客流":275,"本期客单价":175.899636,"本期连带率":4.854545,"环比销售":201146.2500,"环比销量":4936.0000,"环比客流":1083,"环比客单价":185.730609,"环比连带率":4.557710,"同比销售":100455.1200,"同比销量":2367.0000,"同比客流":650,"同比客单价":154.546338,"同比连带率":3.641538},{"门店":"000301","任务":162392.47849569915,"完成率":0.42559957604089671,"本期销售":69114.1700,"本期销量":1696.0000,"本期客流":480,"本期客单价":143.987854,"本期连带率":3.533333,"环比销售":241379.2200,"环比销量":5909.0000,"环比客流":1526,"环比客单价":158.177732,"环比连带率":3.872214,"同比销售":181656.6500,"同比销量":4137.0000,"同比客流":1250,"同比客单价":145.325320,"同比连带率":3.309600}],
		data9:[{"门店编号":"000001","门店名称":"000001","在册人数":5937,"新增人数_本期":0,"新增率_本期":0.0000000000000,"活跃人数_本期":null,"活跃度_本期":null,"人均贡献_本期":null,"新增人数_环比":14,"新增率_环比":0.0023580933131,"活跃人数_环比":null,"活跃度_环比":null,"人均贡献_环比":null,"新增人数_同比":1,"新增率_同比":0.0001684352366,"活跃人数_同比":null,"活跃度_同比":null,"人均贡献_同比":null,"消费总额_本期":null,"消费总额_环比":null,"消费总额_同比":null},{"门店编号":"000201","门店名称":"000201","在册人数":3991,"新增人数_本期":207,"新增率_本期":0.0518667000751,"活跃人数_本期":144,"活跃度_本期":0.0360811826609,"人均贡献_本期":335.919444,"新增人数_环比":313,"新增率_环比":0.0784264595339,"活跃人数_环比":414,"活跃度_环比":0.1037334001503,"人均贡献_环比":485.860507,"新增人数_同比":260,"新增率_同比":0.0651465798045,"活跃人数_同比":235,"活跃度_同比":0.0588824855925,"人均贡献_同比":425.837957,"消费总额_本期":48372.4000,"消费总额_环比":201146.2500,"消费总额_同比":100071.9200},{"门店编号":"000301","门店名称":"000301","在册人数":1114,"新增人数_本期":22,"新增率_本期":0.0197486535008,"活跃人数_本期":306,"活跃度_本期":0.2746858168761,"人均贡献_本期":225.605130,"新增人数_环比":24,"新增率_环比":0.0215439856373,"活跃人数_环比":641,"活跃度_环比":0.5754039497307,"人均贡献_环比":376.566645,"新增人数_同比":38,"新增率_同比":0.0341113105924,"活跃人数_同比":601,"活跃度_同比":0.5394973070017,"人均贡献_同比":300.817054,"消费总额_本期":69035.1700,"消费总额_环比":241379.2200,"消费总额_同比":180791.0500}],
		data10:[{"员工编号":"1301","姓名":"幸大鹏","门店名称":"000011","入职时间":"","工龄":1428,"销售_本期":58005.2200,"有效天数_本期":14,"成交单数_本期":196,"销量_本期":1020.0000,"客单价_本期":295.945000,"连带率_本期":5.204081,"销售_环比":88622.9400,"有效天数_环比":31,"成交单数_环比":529,"销量_环比":2043.0000,"客单价_环比":167.529187,"连带率_环比":3.862003,"销售_同比":116269.9500,"有效天数_同比":30,"成交单数_同比":640,"销量_同比":2219.0000,"客单价_同比":181.671796,"连带率_同比":3.467187},{"员工编号":"203","姓名":"张强","门店名称":"000201","入职时间":null,"工龄":null,"销售_本期":24987.6400,"有效天数_本期":11,"成交单数_本期":98,"销量_本期":893.0000,"客单价_本期":254.975918,"连带率_本期":9.112244,"销售_环比":126827.3200,"有效天数_环比":27,"成交单数_环比":548,"销量_环比":3547.0000,"客单价_环比":231.436715,"连带率_环比":6.472627,"销售_同比":45688.7800,"有效天数_同比":26,"成交单数_同比":304,"销量_同比":1191.0000,"客单价_同比":150.292039,"连带率_同比":3.917763},{"员工编号":"205","姓名":"魏剑","门店名称":"000201","入职时间":null,"工龄":null,"销售_本期":5316.9000,"有效天数_本期":11,"成交单数_本期":80,"销量_本期":155.0000,"客单价_本期":66.461250,"连带率_本期":1.937500,"销售_环比":2043.4300,"有效天数_环比":4,"成交单数_环比":25,"销量_环比":40.0000,"客单价_环比":81.737200,"连带率_环比":1.600000,"销售_同比":21673.0100,"有效天数_同比":11,"成交单数_同比":100,"销量_同比":395.0000,"客单价_同比":216.730100,"连带率_同比":3.950000}]
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.loadData();
			});
		},
		initEvent: function(){
			var me = this;
			$('#date').datetimepicker({  
		        format: 'yyyy年mm月',  
		         autoclose: true,  
		         forceParse: false,
		         startView: 4,  
		         minView: 3,  
		         language: 'zh-CN'
		    }).val(moment().format('YYYY年MM月')).change(function(){
		    		me.year = moment($(this).val(), 'YYYY年MM月').format('YYYY');
		    		me.month = moment($(this).val(), 'YYYY年MM月').format('MM');
		    });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    //$("#gridPanel_1").setGridWidth($("#gridPanel_1").parents('.card-body').width());
                }, 200);
            });
		},
		loadData: function(){
			this.getData1();
			this.getData2();
			this.getData3();
			this.getData4();
			this.getData5();
			this.getData6();
			this.getData7();
			this.getData8();
			this.getData9();
			this.getData10();
		},
		getData1 : function() {
			var me = this;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_title?operMonth="+me.year+'-'+me.month,function(data){
				me.data1 = data;
			});
		},
		getData2 : function() {
			var me = this;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_yunying?operMonth="+me.year+'-'+me.month+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.data2 = data;
			});
		},
		getData3 : function() {
			var me = this;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_vip?operMonth="+me.year+'-'+me.month+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.data3 = data;
			});
		},
		getData4 : function() {
			var me = this;
			me.chart1Data = null;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_wanchenglv?operMonth="+me.year+'-'+me.month+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.chart1Data = data;
				setTimeout(function(){
					me.createChart1(data);
				});
			});
		},
		getData5 : function() {
			var me = this;
			me.chart2Data = null;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_sale_fenbu_duibi?operMonth="+me.year+'-'+me.month+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.chart2Data = data;
				setTimeout(function(){
					me.createChart2(data);
				});
			});
		},
		getData6 : function() {
			var me = this;
			me.chart3Data = null;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_branch_sale_fenbu?operMonth="+me.year+'-'+me.month+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.chart3Data = data;
				setTimeout(function(){
					me.createChart3(data);
				});
			});
		},
		getData7 : function() {
			var me = this;
			me.chart4Data = null;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_cls_sale_fenbu?operMonth="+me.year+'-'+me.month+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.chart4Data = data;
				setTimeout(function(){
					me.createChart4(data);
				});
			});
		},
		getData8 : function() {
			var me = this;
			me.grid1Data = null;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_aim_wancheng_sale_client?operMonth="+me.year+'-'+me.month+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.grid1Data = data;
				setTimeout(function(){
					me.GetGrid1(data);
				});
			});
		},
		getData9 : function() {
			var me = this;
			me.grid2Data = null;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_client?operMonth="+me.year+'-'+me.month+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.grid2Data = data;
				setTimeout(function(){
					me.GetGrid2(data);
				});
			});
		},
		getData10 : function() {
			var me = this;
			me.grid3Data = null;
			$.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_saler?operMonth="+me.year+'-'+me.month+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.grid3Data = data;
				setTimeout(function(){
					me.GetGrid3(data);
				});
			});
		},
		createChart1 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '本月销售完成率',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                grid: {
                    left: '0%',
                    right: '0%',
                    bottom: '0%',
                    containLabel: true
                },
                series: [{
                    name: '完成率',
                    type: 'pie',
                    radius: [
                    '60%',
                    '75%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: (data[0].value),
                        name: '任务完成率',

                        label: {
                            normal: {
                                formatter: '{d} %',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: 100 - (data[0].value),
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n任务完成率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
			ECharts.Draw(option, "chart1", function(params){
				top.showModal('chartModal');
                top.chartModal.modalTitle = '各门店任务完成情况';
                top.chartModal.showLoading = true;
                $.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_branch_aim_wancheng?operMonth="+me.year+'-'+me.month,function(data){
                	top.chartModal.showLoading = false;
            		if(data && data.length > 0){
            			top.chartModal.noData = false;
        				setTimeout(function(){
        					me.createChart5(data);
        				},200);
            		}else{
            			top.chartModal.noData = true;
            		}
    			});
			});
		},
		createChart2 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '销售波动对比分析(本期为本月，环比为上月，同比为去年本月)',
                    x: 'left',
                    padding: [5, 0]
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        label: {
                            backgroundColor: '#6a7985'
                        }
                    }
                },
                legend: {
                    data: ['本期', '环比', '同比'],
                    top: '2%',
                    right: '7%'
                },
                toolbox: {
                    feature: {
                        saveAsImage: {},
                        magicType: {
                            show: true,
                            type: ['line', 'bar']
                        }
                    }
                },
                grid: {
                    left: '0%',
                    right: '1%',
                    top: '15%',
                    bottom: '1%',
                    containLabel: true
                },
                xAxis: [
                    {
                        type: 'category',
                        boundaryGap: false,
                        data: public_data.getEchartsData(data, '日期', ''),
                    }
                ],
                yAxis: [
                    {
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '本期',
                        type: 'line',
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(123, 147, 224, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        smooth: true,
                        data: public_data.getEchartsData(data, '本期', ''),
                    },
                    {
                        name: '环比',
                        type: 'line',
                        smooth: true,
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(89, 196, 230, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        data: public_data.getEchartsData(data, '环比', ''),
                    },
                    {
                        name: '同比',
                        type: 'line',
                        smooth: true,
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(237, 175, 218, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        data: public_data.getEchartsData(data, '同比', ''),
                    }
                ]
            };
			ECharts.Draw(option, "chart2");
		},
		createChart3 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: "各门店销售分布",
                    x: 'center',
                   // y: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },
                series: [
                    {
                        name: "各门店销售分布",
                        type: 'pie',
                        radius: ['55%', '75%'],
                        center: ['50%', '50%'],
                        avoidLabelOverlap: false,
                        label: {
                            normal: {
                                show: false,
                                position: 'center'
                            },
                            emphasis: {
                                show: true,
                                textStyle: {
                                    fontSize: '25',
                                    fontWeight: 'normal'
                                },
                                formatter: "{b}\n{c}"
                            }
                        },
                        labelLine: {
                            normal: {
                                show: false
                            }
                        },
                        data: data
                    }
                ]
            };
			ECharts.Draw(option, "chart3", function(params){
				top.showModal('chartModal');
				console.log(params)
                top.chartModal.modalTitle = '【' + params.name + '】各类别销售分布';
                top.chartModal.showLoading = true;
                $.get(baseurl + "/Report_PC/Company_DayMonth/branch_month_branch_cls?operMonth="+me.year+'-'+me.month+'&branchNo='+params.data.code,function(data){
                	top.chartModal.showLoading = false;
            		if(data && data.length > 0){
            			top.chartModal.noData = false;
        				setTimeout(function(){
        					me.createChart6(data);
        				},200);
            		}else{
            			top.chartModal.noData = true;
            		}
    			});
			});
		},
		createChart4 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '各类别销售分布',
                    x: 'center',
                    padding: [5, 0]
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{b} : {c} ({d}%)"
                },
                series: [{
                    type: 'pie',
                    radius: ['55%', '75%'],
                    center: ['50%', '50%'],
                    avoidLabelOverlap: false,
                    label: {
                        normal: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            show: true,
                            textStyle: {
                                fontSize: '25',
                                fontWeight: 'normal'
                            },
                            formatter: "{b}\n{c}"
                        }
                    },
                    labelLine: {
                        normal: {
                            show: false
                        }
                    },
                    data: data
                }]
            };
			ECharts.Draw(option, "chart4");
		},
		createChart5: function(data){
			var data1 = [];
        	var data2 = [];
        	var data3 = [];
        	var data4 = [];
        	for(var i=0,len=data.length; i<len; i++){
        		data1.push(top.InitData.getBranchName(data[i]['门店']));
        		data2.push(data[i]['计划']);
        		data3.push(data[i]['完成']);
        		data4.push(data[i]['完成率']);
        	}
			var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '1%',
                    top:'40',
                    containLabel: true
                },
                legend: {
                    data: ['计划','完成','完成率'],
                    top: '10',
                    x:'right'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                       // name: '金额',
                        axisLabel: {
                            formatter: '{value}元'
                        }
                    },
                    {
                        type: 'value',
                       // name: '完成率',
                        axisLabel: {
                            formatter: '{value}%'
                        },
                        splitLine: {
                            show: false
                        },
                    }
                ],
                series: [
                    {
                        name: '计划',
                        type: 'bar',
                        data: data2,
                    },
                    {
                        name: '完成',
                        type: 'bar',
                        data: data3,
                    },
                    {
                        name: '完成率',
                        type: 'line',
                        yAxisIndex: 1,
                        data: data4,
                    }
                ]
            };
			ECharts.Draw(option, top.document.getElementById('chartModalCon'));
		},
		createChart6: function(data){
			var option = {
                grid: {
                    left: '3%',
                    right: '3%',
                    bottom: '3%',
                    containLabel: true
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{b} : {c} ({d}%)"
                },
                series: [
                       {
                           type: 'pie',
                           radius: '55%',
                           center: ['50%', '55%'],
                           data: data,
                           itemStyle: {
                               normal: {
                                   label: {
                                       show: true,
                                       formatter: '{b} : ￥{c}',
                                       textStyle: {
                                           color: '#333'
                                       }
                                   },
                                   labelLine: { show: true }
                               }
                           }
                       }
                ]
            };
			ECharts.Draw(option, top.document.getElementById('chartModalCon'));
		},
		GetGrid1: function (data) {
			if(data == null || data.length == 0){
				return;
			}
			var keyWord = {
                "col_name": "门店"+"~任务~完成率~本期销售~本期销量~本期客流~本期客单价~本期连带率~环比销售~环比销量~环比客流~环比客单价~环比连带率~同比销售~同比销量~同比客流~同比客单价~同比连带率"
            }
            var colNames = keyWord.col_name.split("~");
            var colModel = [];
            var sumCol = [];
            var i = 0;
            for (var i = 0; i < colNames.length; i++) {
                var json = colNames[i]
                colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                if (json.indexOf("门店") >= 0) {
                    $.extend(colModel[i], { formatter: public_data.Dic_branch })
                } else if (json.indexOf("类别") >= 0) {
                    $.extend(colModel[i], { formatter: public_data.Dic_cls_all })
                } else if (json.indexOf("任务") >= 0 || json.indexOf("销售") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                    sumCol.push(json);
                } else if (json.indexOf("销量") >= 0 || json.indexOf("客流") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0)
                    sumCol.push(json);
                } else if (json.indexOf("客单价") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB2)
                } else if (json.indexOf("完成率") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                } else if (json.indexOf("连带率") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num2 )
                }
            }
            var $gridTable = $("#gridTable1");
            $gridTable.GridUnload();
            $gridTable.jqGrid({
                datatype: "local",
                data:data,
                height: 'auto' ,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rownumWidth: 50,
                rowNum: 1000,
                width: $('.gridCard').width() - 10,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                gridComplete: function () {
                    var sumColArr = {};
                    for (var i = 0; i < sumCol.length; i++) {
                        eval("sumColArr." + sumCol[i] + "=" + $(this).getCol(sumCol[i], false, "sum"))
                    }
                    sumColArr.门店 = "合计"
                    sumColArr.完成率 = sumColArr.本期销售 / sumColArr.任务
                    sumColArr.本期客单价 = sumColArr.本期销售 / sumColArr.本期客流
                    sumColArr.环比客单价 = sumColArr.环比销售 / sumColArr.环比客流
                    sumColArr.同比客单价 = sumColArr.同比销售 / sumColArr.同比客流
                    sumColArr.本期连带率 = sumColArr.本期销量 / sumColArr.本期客流
                    sumColArr.环比连带率 = sumColArr.环比销量 / sumColArr.环比客流
                    sumColArr.同比连带率 = sumColArr.同比销量 / sumColArr.同比客流
                    $(this).footerData("set", sumColArr);
                    condition.backColor_ABC($(this).attr("id"), "完成率", 95, 90, 85);
                    condition.backColor_ABC($(this).attr("id"), "客单价", 200, 150, 100);
                    condition.fontColor_ABC($(this).attr("id"), "连带率", 4, 3,2);
                    $(this).find("[aria-describedby*='完成率']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='完成率']").css("border-right", "1px dashed red")
                    $(this).find("[aria-describedby*='连带率']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='连带率']").css("border-right", "1px dashed red")
                }
            })
            $gridTable.jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [
                { startColumnName: '本期销售', numberOfColumns: 5, titleText: '本期销售概况' },
                { startColumnName: '环比销售', numberOfColumns: 5, titleText: '环比销售概况' },
                { startColumnName: '同比销售', numberOfColumns: 5, titleText: '同比销售概况' }
                ]
            });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        },
        GetGrid2: function (data) {
        	if(data == null || data.length == 0){
				return;
			}
            var $gridTable = $("#gridTable2");
            var keyWord = {
                "col_name": "门店编号~门店名称~在册人数~新增人数_本期~新增率_本期~活跃人数_本期~活跃度_本期~人均贡献_本期~新增人数_环比~新增率_环比~活跃人数_环比~活跃度_环比~人均贡献_环比~新增人数_同比~新增率_同比~活跃人数_同比~活跃度_同比~人均贡献_同比~消费总额_本期~消费总额_环比~消费总额_同比"
            }
            var colNames = keyWord.col_name.split("~");
            var colModel = [];
            var sumCol = [];
            for (var i = 0; i < colNames.length; i++) {
                var json = colNames[i]
                if (i <= 2) {
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                } else {
                    colModel.push({ name: json, label: json.split("_")[0], align: "center", sortable: true, sorttype: 'float' })
                }
                if (json.indexOf("门店名称") >= 0) {
                    
                } else if (json.indexOf("贡献") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                } else if (json.indexOf("消费总额") >= 0) {
                    $.extend(colModel[i], { hidden: true })
                    sumCol.push(json);
                } else if (json.indexOf("人数") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0)
                    sumCol.push(json);
                } else if (json.indexOf("率") >= 0 || json.indexOf("度") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                }
            }
            $gridTable.GridUnload();
            $gridTable.jqGrid({
                datatype: "local",
                data: data,
                height: 'auto',
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rownumWidth: 50,
                rowNum: 1000,
                width: $('.gridCard').width() - 10,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                gridComplete: function () {
                    var sumColArr = {};
                    for (var i = 0; i < sumCol.length; i++) {
                        eval("sumColArr." + sumCol[i] + "=" + $(this).getCol(sumCol[i], false, "sum"))
                    }
                    sumColArr.门店名称 = "合计"
                    sumColArr.新增率_本期 = sumColArr.新增人数_本期 / sumColArr.在册人数
                    sumColArr.新增率_环比 = sumColArr.新增人数_环比 / sumColArr.在册人数
                    sumColArr.新增率_同比 = sumColArr.新增人数_同比 / sumColArr.在册人数
                    sumColArr.活跃度_本期 = sumColArr.活跃人数_本期 / sumColArr.在册人数
                    sumColArr.活跃度_环比 = sumColArr.活跃人数_环比 / sumColArr.在册人数
                    sumColArr.活跃度_同比 = sumColArr.活跃人数_同比 / sumColArr.在册人数
                    sumColArr.人均贡献_本期 = sumColArr.消费总额_本期 / sumColArr.活跃人数_本期
                    sumColArr.人均贡献_环比 = sumColArr.消费总额_环比 / sumColArr.活跃人数_环比
                    sumColArr.人均贡献_同比 = sumColArr.消费总额_同比 / sumColArr.活跃人数_同比
                    $(this).footerData("set", sumColArr);
                    condition.backColor_ABC($(this).attr("id"), "新增率", 30, 20, 10);
                    condition.backColor_ABC($(this).attr("id"), "活跃度", 30, 20, 10);
                    $(this).find("[aria-describedby*='在册人数']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='在册人数']").css("border-right", "1px dashed red")
                    $(this).find("[aria-describedby*='人均贡献']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='人均贡献']").css("border-right", "1px dashed red")
                }
            })
            jQuery("#gridTable2").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [
                { startColumnName: '新增人数_本期', numberOfColumns: 5, titleText: '本期会员概况' },
                { startColumnName: '新增人数_环比', numberOfColumns: 5, titleText: '环比会员概况' },
                { startColumnName: '新增人数_同比', numberOfColumns: 5, titleText: '同比会员概况' }
                ]
            });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        },
        GetGrid3: function (data) {
        	if(data == null || data.length == 0){
				return;
			}
            var $gridTable = $("#gridTable3");
            var keyWord = {
                "col_name": "员工编号~姓名~门店名称~入职时间~工龄~销售_本期~有效天数_本期~成交单数_本期~销量_本期~客单价_本期~连带率_本期~销售_环比~有效天数_环比~成交单数_环比~销量_环比~客单价_环比~连带率_环比~销售_同比~有效天数_同比~成交单数_同比~销量_同比~客单价_同比~连带率_同比"
            }
            var jsonArr=data;
            var colNames = keyWord.col_name.split("~");
            var colModel = [];
            var avgCol = [];
            var sumCol = [];
            var i = 0;
            for (var i = 0; i < colNames.length; i++) {
                var json = colNames[i]
                if (i <= 3) {
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                } else {
                    colModel.push({ name: json, label: json.split("_")[0], align: "center", sortable: true, sorttype: 'float' })
                }
                if (json.indexOf("门店名称") >= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getBranchName })
                } else if (json.indexOf("工龄") >= 0) {
                    $.extend(colModel[i],jqFormatter.Num0,{ label: "工龄（月）" }, { summaryType: 'avg' })
                } else if (json.indexOf("销售") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("销量") >= 0) {
                        $.extend(colModel[i], jqFormatter.Num0, { hidden: true,summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("成交单数") >= 0 || json.indexOf("有效天数") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("价") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                } else if (json.indexOf("率") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num2)
                }
            }
            $gridTable.GridUnload();
            $gridTable.jqGrid({
                datatype: "local",
                data: jsonArr,//数据数组
                height: 'auto',
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rownumWidth: 50,
                rowNum: 1000,
                width: $('.gridCard').width() - 10,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                grouping: true,
                groupingView: {
                    groupField: ['门店名称'],
                    groupSummary: [true],
                    groupColumnShow: [true],
                    groupText: ['<b>{0}各人员销售概况</b>'],
                    groupCollapse: false,
                },
                gridComplete: function () {
                    var sumColArr = {};
                    for (var i = 0; i < sumCol.length; i++) {
                        eval("sumColArr." + sumCol[i] + "=" + $(this).getCol(sumCol[i], false, "sum"))
                    }
                    sumColArr.入职时间 = "合计";
                    sumColArr.完成率 = sumColArr.本期销售;
                    sumColArr.工龄 = $(this).getCol("工龄", false, "avg")
                    sumColArr.客单价_本期 = sumColArr.销售_本期 / sumColArr.成交单数_本期;
                    sumColArr.客单价_环比 = sumColArr.销售_环比 / sumColArr.成交单数_环比;
                    sumColArr.客单价_同比 = sumColArr.销售_同比 / sumColArr.成交单数_同比;
                    sumColArr.连带率_本期 = sumColArr.销量_本期 / sumColArr.成交单数_本期;
                    sumColArr.连带率_环比 = sumColArr.销量_环比 / sumColArr.成交单数_环比;
                    sumColArr.连带率_同比 = sumColArr.销量_同比 / sumColArr.成交单数_同比;
                    $(this).footerData("set", sumColArr);
                    condition.backColor_ABC($(this).attr("id"), "完成率", 95, 90, 85);
                    condition.backColor_ABC($(this).attr("id"), "客单价", 200, 150, 100);
                    condition.fontColor_ABC($(this).attr("id"), "连带率", 4, 3, 2); 
                    $(this).find("[aria-describedby*='工龄']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='工龄']").css("border-right", "1px dashed red")
                    $(this).find("[aria-describedby*='完成率']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='完成率']").css("border-right", "1px dashed red")
                    $(this).find("[aria-describedby*='连带率']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='连带率']").css("border-right", "1px dashed red")
                }
            })
            jQuery("#gridTable3").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [
                { startColumnName: '销售_本期', numberOfColumns: 6, titleText: '员工本期销售概况' },
                { startColumnName: '销售_环比', numberOfColumns:6, titleText: '员工环比销售概况' },
                { startColumnName: '销售_同比', numberOfColumns:6, titleText: '员工同比销售概况' }
                ]
            });
            var footGroup = $("#gridTable3").find(".ui-widget-content.jqfoot.ui-row-ltr")
            for (var j = 0; j < footGroup.length; j++) {
                var sale_B = parseFloat(footGroup.eq(j).find("[aria-describedby*='销售_本期']").text().replace("¥", "").replace(/,/g, ''))
                var sale_H = parseFloat(footGroup.eq(j).find("[aria-describedby*='销售_环比']").text().replace("¥", "").replace(/,/g, ''))
                var sale_T = parseFloat(footGroup.eq(j).find("[aria-describedby*='销售_同比']").text().replace("¥", "").replace(/,/g, ''))
                var qnty_B = parseFloat(footGroup.eq(j).find("[aria-describedby*='销量_本期']").text().replace("¥", "").replace(/,/g, ''))
                var qnty_H = parseFloat(footGroup.eq(j).find("[aria-describedby*='销量_环比']").text().replace("¥", "").replace(/,/g, ''))
                var qnty_T = parseFloat(footGroup.eq(j).find("[aria-describedby*='销量_同比']").text().replace("¥", "").replace(/,/g, ''))
                var flow_B = parseFloat(footGroup.eq(j).find("[aria-describedby*='成交单数_本期']").text().replace("¥", "").replace(/,/g, ''))
                var flow_H = parseFloat(footGroup.eq(j).find("[aria-describedby*='成交单数_环比']").text().replace("¥", "").replace(/,/g, ''))
                var flow_T = parseFloat(footGroup.eq(j).find("[aria-describedby*='成交单数_同比']").text().replace("¥", "").replace(/,/g, ''))
                footGroup.eq(j).find("[aria-describedby*='客单价_本期']").html(flow_B == 0 ? "" : fixNum((sale_B / flow_B),2));
                footGroup.eq(j).find("[aria-describedby*='客单价_环比']").html(flow_H == 0 ? "" : fixNum((sale_H / flow_H),2));
                footGroup.eq(j).find("[aria-describedby*='客单价_同比']").html(flow_T == 0 ? "" : fixNum((sale_T / flow_T),2));
                footGroup.eq(j).find("[aria-describedby*='连带率_本期']").html(flow_B == 0 ? "" : fixNum((qnty_B / flow_B),2));
                footGroup.eq(j).find("[aria-describedby*='连带率_环比']").html(flow_H == 0 ? "" : fixNum((qnty_H / flow_H),2));
                footGroup.eq(j).find("[aria-describedby*='连带率_同比']").html(flow_T == 0 ? "" : fixNum((qnty_T / flow_T),2));
            };
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        }
	}
});