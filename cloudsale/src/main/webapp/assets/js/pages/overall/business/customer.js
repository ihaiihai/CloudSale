var pageVm = new Vue({
	el: '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		key: "",
		searchType:1,
		grid: null,
		gridData: [],
		areaCode: null,
		areaOptions:top.branchOptionData,
		clsCode: null,
		clsOptions:top.clsOptionData,
		brandCode: null,
		brandOptions:top.brandOptionData,
		gridType: 1,
		initJson : [
            { "id": "money", "name": "门店编号~门店名称~订单总数~五十以下_数量~五十以下_占比~五十到一百_数量~五十到一百_占比~一百到两百_数量~一百到两百_占比~二百到三百_数量~二百到三百_占比~三百到四百_数量~三百到四百_占比~四百到五百_数量~四百到五百_占比~五百到一千_数量~五百到一千_占比~一千以上_数量~一千以上_占比", "tableData": [], "isNew": 0 },
            { "id": "qnty", "name": "门店编号~门店名称~订单总数~数量1个_数量~数量1个_占比~数量2个_数量~数量2个_占比~数量3个_数量~数量3个_占比~数量4个_数量~数量4个_占比~数量5个_数量~数量5个_占比~数量5个以上_数量~数量5个以上_占比", "tableData": [], "isNew": 0 },
            { "id": "cross", "name": "门店编号~门店名称~订单总数~交叉1_数量~交叉1_占比~交叉2_数量~交叉2_占比~交叉3_数量~交叉3_占比~交叉4_数量~交叉4_占比~交叉5_数量~交叉5_占比~交叉5个以上_数量~交叉5个以上_占比", "tableData": [], "isNew": 0 }
        ]
	},
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadGrid();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 270);
	                $("#gridTable").setGridWidth($('.gridCard').width() - 10);
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadGrid: function(){
			var me = this;
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
			var end = date ? date.split(' - ')[1] : '';
			window.showLoading();
			$.get(baseurl + "/Report_PC/Client_Fenbu/query?branchNos="+(me.areaCode?me.areaCode:"")+"&clses="+(me.clsCode?me.clsCode:"")+"&brands="+(me.brandCode?me.brandCode:"")+"&type="+me.gridType+"&begin="+begin+'&end='+end,function(data){
				window.hideLoading();
				me.gridData = data;
				me.GetGrid(data);
				me.afterInit(data);
			});
		},
        GetGrid: function(jsonArr) {
        		var colModel = [];
            var sumCol = [];
            var avgCol = [];
            var myArr = this.initJson[this.gridType - 1]["name"].split("~");
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if(json.indexOf('门店') > 0 && json.indexOf('订单') > 0){
                		colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' });
                }else{
                		colModel.push({ name: json, label: json.split('_')[1], align: "center", sortable: true, sorttype: 'float' });
                }
                if(json.indexOf('总数') > 0){
	        			sumCol.push(json);
	        		}else if(json.indexOf('_数量') > 0){
	        			$.extend(colModel[i], jqFormatter.Num0)
	                sumCol.push(json);
	        		}else if (json.indexOf("_占比") >= 0) {
	                $.extend(colModel[i], { formatter: jqFormatter.Percent })
	            }
            }
            $("#gridTable").jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                shrinkToFit: true,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                pager: "#gridPager",
                pagerpos: "right",
                rowList: [100, 200, 500, 1000],
                sortname: "门店编号",
                sumColArr: sumCol,
                sumName: "门店名称_合计",
                avgColArr: avgCol,
                height: $(window).height() - 270,
                width: $('.gridCard').width() - 10,
                Complete: this.Complete
            });
        },
        Complete: function(gridID, sumColArr) {
        		var _this = $("#" + gridID);
            var myArr = this.initJson[0]["name"].split("~");
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if (json.indexOf("占比") > -1) {
                    sumColArr[json] = sumColArr[myArr[i - 1]] / sumColArr["订单总数"]
                }
            }
            _this.footerData("set", sumColArr);
            condition.backColor_ABC(gridID, "占比", 20, 10, 5);
            _this.find("[aria-describedby*='订单总数']").css("background-color", "#FCF2CA").css("border-left", "1px dashed red").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='订单总数']").css("border-left", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='订单总数']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='_占比']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='_占比']").css("border-right", "1px dashed red")
        },
        afterInit: function(jsonArr) {
        		var groupHeaders = [];
            for (var obj in jsonArr[0]) {
                if (obj.indexOf("_数量") >= 0) {
                    groupHeaders.push({ startColumnName: obj, numberOfColumns: 2, titleText:"【"+obj.split("_")[0]+ "】" })
                }
            }
            $("#gridTable").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: groupHeaders
            });
        },
		search: function(){
			var me = this;
			$('#gridTable').GridUnload();
			this.loadGrid();
		},
		refresh: function(){
			window.location.reload();
		},
		download: function(){
            var me = this;
            var date = $('#page').find('.daterange').val();
            var begin = date ? date.split(' - ')[0] : '';
            var end = date ? date.split(' - ')[1] : '';
            window.location.href = baseurl + "/Report_PC/dataExport/excel?branchNos="+(me.areaCode?me.areaCode:"")+"&clsNos="+(me.clsCode?me.clsCode:"")+"&brandNos="+(me.brandCode?me.brandCode:"")+"&begin="+begin+'&end='+end+"&busi=clientFenbu";
		},
		print: function(){
            $("#printTable").printTable();
		},
		reload: function(){
			var me = this;
			me.key = '';
			me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
	    				key: me.key
	            },
	            page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
		}
	}
});