var pageVM = new Vue({
	el : '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data : {
        date: moment().startOf('month').format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
		areaCode: null,
		areaOptions:[],
		chart1Data: null,
		chart2Data: null,
		chart3Data: null,
		chart4Data: null,
		chart5Data: null,
		chart6Data: null,
		data1:[{"销售":479726,"利润":67575}],
		data11:[{"name":"完成销售共计【479726元】,毛利率【14.1%】"}],
		data2:[{"新增客户":184,"新增会员":313}],
		data3:[{"时间":7,"本期客流":0,"本期客单价":74,"环比客流":1,"环比客单价":266,"同比客流":0,"同比客单价":0},{"时间":8,"本期客流":13,"本期客单价":125,"环比客流":18,"环比客单价":205,"同比客流":20,"同比客单价":118},{"时间":9,"本期客流":36,"本期客单价":87,"环比客流":58,"环比客单价":157,"同比客流":49,"同比客单价":121},{"时间":10,"本期客流":49,"本期客单价":121,"环比客流":80,"环比客单价":169,"同比客流":71,"同比客单价":114},{"时间":11,"本期客流":47,"本期客单价":155,"环比客流":77,"环比客单价":180,"同比客流":70,"同比客单价":136},{"时间":12,"本期客流":31,"本期客单价":137,"环比客流":54,"环比客单价":214,"同比客流":52,"同比客单价":134},{"时间":13,"本期客流":34,"本期客单价":127,"环比客流":57,"环比客单价":199,"同比客流":57,"同比客单价":131},{"时间":14,"本期客流":50,"本期客单价":148,"环比客流":70,"环比客单价":179,"同比客流":79,"同比客单价":140},{"时间":15,"本期客流":55,"本期客单价":160,"环比客流":80,"环比客单价":170,"同比客流":86,"同比客单价":141},{"时间":16,"本期客流":62,"本期客单价":118,"环比客流":84,"环比客单价":145,"同比客流":94,"同比客单价":129},{"时间":17,"本期客流":58,"本期客单价":109,"环比客流":91,"环比客单价":172,"同比客流":91,"同比客单价":129},{"时间":18,"本期客流":35,"本期客单价":132,"环比客流":65,"环比客单价":159,"同比客流":62,"同比客单价":156},{"时间":19,"本期客流":29,"本期客单价":96,"环比客流":52,"环比客单价":188,"同比客流":50,"同比客单价":158},{"时间":20,"本期客流":25,"本期客单价":181,"环比客流":53,"环比客单价":150,"同比客流":47,"同比客单价":47},{"时间":21,"本期客流":2,"本期客单价":157,"环比客流":11,"环比客单价":77,"同比客流":6,"同比客单价":2},{"时间":22,"本期客流":0,"本期客单价":78,"环比客流":2,"环比客单价":328,"同比客流":0,"同比客单价":-25},{"时间":23,"本期客流":0,"本期客单价":0,"环比客流":1,"环比客单价":42,"同比客流":0,"同比客单价":0},{"时间":23,"本期客流":0,"本期客单价":0,"环比客流":0,"环比客单价":0,"同比客流":0,"同比客单价":-610}],
		data31:[{"name":"本期客流【526人】,客单价【130.3元/人】,客流环比【下降329人】,同比【下降309人】"}],
		data4:[{"日期":"2018-12-30","成交金额":139943,"利润":6725,"黄氏金额":131619,"黄氏利润":12147,"标记":"促销前"},{"日期":"2018-12-31","成交金额":178049,"利润":14959,"黄氏金额":131619,"黄氏利润":12147,"标记":"促销前"},{"日期":"2019-01-01","成交金额":357455,"利润":27755,"黄氏金额":131619,"黄氏利润":12147,"标记":"促销前"},{"日期":"2019-01-02","成交金额":81756,"利润":14402,"黄氏金额":131619,"黄氏利润":12147,"标记":"促销前"},{"日期":"2019-01-03","成交金额":52065,"利润":5479,"黄氏金额":131619,"黄氏利润":12147,"标记":"促销前"},{"日期":"2019-01-04","成交金额":47306,"利润":5588,"黄氏金额":131619,"黄氏利润":12147,"标记":"促销前"},{"日期":"2019-01-05","成交金额":64756,"利润":10118,"黄氏金额":131619,"黄氏利润":12147,"标记":"促销前"},{"日期":"2019-01-06","成交金额":79367,"利润":13874,"黄氏金额":68532,"黄氏利润":9654,"标记":"促销中"},{"日期":"2019-01-07","成交金额":63837,"利润":10856,"黄氏金额":68532,"黄氏利润":9654,"标记":"促销中"},{"日期":"2019-01-08","成交金额":58126,"利润":8643,"黄氏金额":68532,"黄氏利润":9654,"标记":"促销中"},{"日期":"2019-01-09","成交金额":58474,"利润":7746,"黄氏金额":68532,"黄氏利润":9654,"标记":"促销中"},{"日期":"2019-01-10","成交金额":52279,"利润":6183,"黄氏金额":68532,"黄氏利润":9654,"标记":"促销中"},{"日期":"2019-01-11","成交金额":81217,"利润":12443,"黄氏金额":68532,"黄氏利润":9654,"标记":"促销中"},{"日期":"2019-01-12","成交金额":86426,"利润":7831,"黄氏金额":68532,"黄氏利润":9654,"标记":"促销中"},{"日期":"2019-01-13","成交金额":97990,"利润":14461,"黄氏金额":71892,"黄氏利润":10895,"标记":"促销后"},{"日期":"2019-01-14","成交金额":92309,"利润":14812,"黄氏金额":71892,"黄氏利润":10895,"标记":"促销后"},{"日期":"2019-01-15","成交金额":25378,"利润":3412,"黄氏金额":71892,"黄氏利润":10895,"标记":"促销后"}],
		data41:[{"name":"本次促销爆发度为【-47.9%】,衰减度为【-4.7%】"}],
		data5:[{"折扣":66.08}],
		data6:[{"条码":"6907925887645       ","名称":"飞帆900g(厅3)","销售额":8118.0000},{"条码":"8716200719247       ","名称":"美素佳儿金装 3#","销售额":8859.2000},{"条码":"8715845001830       ","名称":"海普诺凯1897幼儿奶粉900g","销售额":10076.0000},{"条码":"6907992632520       ","名称":"金领冠珍护3段800g","销售额":11384.0000},{"条码":"6907992633534       ","名称":"金领冠菁护3段800g","销售额":12376.0000},{"条码":"6907925770121       ","名称":"飞帆五星优护(厅1)","销售额":13810.0000},{"条码":"6951025042657       ","名称":"哈丁婴幼儿专用洗衣液2L","销售额":14190.0000},{"条码":"6907925770800       ","名称":"飞鹤星阶优护(厅3)","销售额":15846.0000},{"条码":"6907925770664       ","名称":"飞帆五星优护(厅3)","销售额":16476.0000},{"条码":"8715845001823       ","名称":"海普诺凯1897较大婴儿奶粉900g","销售额":23816.0000}]
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.loadData();
				me.loadAreaOptions();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
		},
		loadAreaOptions: function(parentAreaCode, callback){
			var me = this;
			$.getJSON(baseurl + "/sys/area/queryOptions", {
				parentAreaCode: parentAreaCode
			}, function(res) {
				if(callback){
					callback(res);
				}else{
					console.log(res)
					me.areaOptions = res;
				}
			});
		},
		loadData: function(){
			this.getChartData();
		},
        search:function () {
            this.getChartData();
        },
		getChartData: function(){
			var me = this;
			me.chart1Data = null;
			this.getData1(function (data) {
				me.chart1Data = data;
				setTimeout(function(){
                    me.createChart1(data[0],data[1]);
                });
            });
			me.chart2Data = null;
            this.getData2(function (data) {
            		me.chart2Data = data;
            		setTimeout(function(){
            			me.createChart2(data);
                });
            });
            me.chart3Data = null;
            this.getData3(function (data) {
            		me.chart3Data = data;
            		setTimeout(function(){
            			me.createChart3(data[0],data[1]);
                });
            });
            me.chart4Data = null;
            this.getData4(function (data) {
            		me.chart4Data = data;
            		setTimeout(function(){
            			me.createChart4(data[0],data[1]);
                });
            });
            me.chart5Data = null;
			this.getData5(function (data) {
				me.chart5Data = data;
				setTimeout(function(){
					me.createChart5(data);
	            });
            });
			me.chart6Data = null;
            this.getData6(function (data) {
            		me.chart6Data = data;
            		setTimeout(function(){
    					me.createChart6(data);
    	            });
            });
		},
        getData1:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Cuxiao_Report/xiaoshou_lirun_maoli?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    callback(data)
                }
            });
        },
        getData2:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Cuxiao_Report/kehu_huiyuan?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    callback(data)
                }
            });
        },
        getData3:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Cuxiao_Report/tongqi_huanbi?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    callback(data)
                }
            });
        },
        getData4:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Cuxiao_Report/huangshi?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    callback(data)
                }
            });
        },
        getData5:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Cuxiao_Report/zhekou?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    callback(data)
                }
            });
        },
        getData6:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Cuxiao_Report/item_rank?branchNos="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    callback(data)
                }
            });
        },
		createChart1 : function(data0,data1) {
			var me = this;
            var option = {
                title: {
                    text: data1['name'],
                    x: 'center',
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                legend: {
                    data: ['利润', '销售']
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: {
                    show: false,
                    type: 'value',
                    boundaryGap: [0, 0.01]
                },
                yAxis: {
                    type: 'category',
                    data: ['利润', '销售']
                },
                series: [
                {
                    name: '金额',
                    type: 'bar',
                    stack: '1',
                    data: [data0.利润, data0.销售],
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight',
                        }
                    }
                }
                ]
            };
			ECharts.Draw(option, "chart1");
		},
		createChart2 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '新增会员及新客户情况',
                    x: 'center',
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                xAxis: {
                    show: false,
                    type: 'value',
                    boundaryGap: [0, 0.01]
                },
                yAxis: {
                    type: 'category',
                    data: ['新增客户', '新增会员']
                },
                series: [
                {
                    name: '人数',
                    type: 'bar',
                    stack: '1',
                    data: [data[0].新增客户, data[0].新增会员],
                    label: {
                        normal: {
                            show: true,
                            position: 'insideRight',
                        }
                    }
                }
                ]
            };
			ECharts.Draw(option, "chart2");
		},
		createChart3 : function(data0,data1) {
			var me = this;
			var option = {
                title: {
                    text: data1[0]['name'],
                    x: 'center',
                    x: 'center',
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                    data: ['本期客均', '环比客均', '同比客均', '本期客单', '环比客单', '同比客单'],
                    top: '7%'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: public_data.getEchartsData(data0, '时间', ''),
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '客流量',
                        axisLabel: {
                            formatter: '{value}/人'
                        }
                    },
                    {
                        type: 'value',
                        name: '客单价',
                        axisLabel: {
                            formatter: '￥{value}'
                        }
                    }
                ],
                series: [
                    {
                        name: '本期客均',
                        type: 'bar',
                        data: public_data.getEchartsData(data0, '本期客均', ''),
                    },
                    {
                        name: '环比客均',
                        type: 'bar',
                        data: public_data.getEchartsData(data0, '环比客均', ''),
                    },
                    {
                        name: '同比客均',
                        type: 'bar',
                        data: public_data.getEchartsData(data0, '同比客均', ''),
                    }
                    ,
                     {
                         name: '本期客单',
                         type: 'line',
                         yAxisIndex: 1,
                         symbolSize: 2,
                         itemStyle: {
                             normal: {
                                 areaStyle: {
                                     color: "rgba(123, 147, 224, 0.2)",
                                     type: "default"
                                 },
                                 lineStyle: {
                                     width: 1
                                 }
                             }
                         },
                         smooth: true,
                         data: public_data.getEchartsData(data0, '本期客单', ''),
                     },
                    {
                        name: '环比客单',
                        type: 'line',
                        smooth: true,
                        yAxisIndex: 1,
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(89, 196, 230, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        data: public_data.getEchartsData(data0, '环比客单', ''),
                    },
                    {
                        name: '同比客单',
                        type: 'line',
                        smooth: true,
                        yAxisIndex: 1,
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(237, 175, 218, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        data: public_data.getEchartsData(data0, '同比客单', ''),
                    }
                   
                ]
            };
			ECharts.Draw(option, "chart3");
		},
		createChart4 : function(data0,data1) {
			var me = this;
			var option = {
                title: {
                    text: data1['name'],
                    x: 'center'
                },
                tooltip: {
                    trigger: 'axis'
                },
                legend: {
                    data: ['成交金额', '利润', '黄氏金额', '黄氏利润'],
                    top: '7%'
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                toolbox: {
                    feature: {
                        saveAsImage: {}
                    }
                },
                xAxis: {
                    type: 'category',
                    boundaryGap: false,
                    data: public_data.getEchartsData(data0, '日期', ''),
                },
                yAxis: {
                    type: 'value'
                },
                series: [
                    {
                        name: '成交金额',
                        type: 'line',
                        data: public_data.getEchartsData(data0, '成交金额', ''),
                    },
                    {
                        name: '利润',
                        type: 'line',
                        data: public_data.getEchartsData(data0, '利润', ''),
                    },
                    {
                        name: '黄氏金额',
                        type: 'line',
                        data: public_data.getEchartsData(data0, '黄氏金额', ''),
                    },
                    {
                        name: '黄氏利润',
                        type: 'line',
                        data: public_data.getEchartsData(data0, '黄氏利润', ''),
                    }
                ]
            };
			ECharts.Draw(option, "chart4");
		},
		createChart5 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '平均折扣',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c}元 ({d}%)"
                },
                series: [{
                    name: '平均折扣',
                    type: 'pie',
                    center: ['50%', '55%'],
                    radius: [
                    '55%',
                    '70%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: data[0].折扣,
                        name: '平均折扣',

                        label: {
                            normal: {
                                formatter: '{c}%',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: 100 - data[0].折扣,
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n平均折扣',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
			ECharts.Draw(option, "chart5");
		},
		createChart6 : function(data) {
            data = data||[];
            data.reverse();
			var me = this;
			var option = {
                title: {
                    text: '单品销售TOP10',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                grid: {
                    left: '3%',
                    right: '4%',
                    bottom: '3%',
                    containLabel: true
                },
                yAxis: [
                    {
                        show: false,
                        type: 'category',
                    }
                    ,
                    {
                        type: 'category',
                        data: public_data.getEchartsData(data, '名称', ''),
                        axisLabel: {
                            alignWithLabel: true
                        }
                    }
                ],
                xAxis: [
                    {
                        show: false,
                        type: 'value'
                    }
                ],
                series: [
                    {
                        name: '销售额',
                        type: 'bar',
                        barWidth: '70%',
                        label: {
                            normal: {
                                show: true,
                                position: 'insideRight',
                                formatter: '￥:{c}'
                            }
                        },
                        yAxisIndex: 1,
                        data: public_data.getEchartsData(data, '销售额', ''),
                    }
                ]
            };
			ECharts.Draw(option, "chart6");
		}
	}
});