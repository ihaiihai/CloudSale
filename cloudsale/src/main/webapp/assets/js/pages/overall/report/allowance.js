var pageVM = new Vue({
	el : '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data : {
		gridTab: 0,
		areaCode: null,
		areaOptions:[],
		date: moment().startOf('month').format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
		chart1Data:null,
		chart2Data:null,
		chart3Data:null,
		gridData:null,
		data1:[{"name":"全店折扣","value":59.74}],
		data2:[{"类别":"01","销售金额":17347.4500,"折让金额":7612.7500,"平均折让":69.50},{"类别":"02","销售金额":4324.5000,"折让金额":2470.4000,"平均折让":63.64},{"类别":"03","销售金额":1676.8400,"折让金额":2219.8600,"平均折让":43.03},{"类别":"04","销售金额":831.5000,"折让金额":450.7000,"平均折让":64.85},{"类别":"06","销售金额":428.5200,"折让金额":701.2800,"平均折让":37.93},{"类别":"07","销售金额":110.9000,"折让金额":110.9000,"平均折让":50.00},{"类别":"11","销售金额":30.0000,"折让金额":8.0000,"平均折让":78.95},{"类别":"05","销售金额":10.2000,"折让金额":1659.8000,"平均折让":0.61},{"类别":"22","销售金额":0.0000,"折让金额":182.1000,"平均折让":0.00}],
		data3:[{"品牌":"0709                ","同单销售额":5083.7000,"同单折让额":1799.7000,"同单折让率":64.598700},{"品牌":"0712                ","同单销售额":4462.0000,"同单折让额":3027.6000,"同单折让率":32.147100},{"品牌":"0724                ","同单销售额":2351.6000,"同单折让额":1383.0000,"同单折让率":41.189000},{"品牌":"0701                ","同单销售额":1791.0000,"同单折让额":199.0000,"同单折让率":88.888900},{"品牌":"0717                ","同单销售额":1250.0000,"同单折让额":240.0000,"同单折让率":80.800000},{"品牌":"0704                ","同单销售额":975.8000,"同单折让额":418.2000,"同单折让率":57.142900},{"品牌":"0702                ","同单销售额":776.0000,"同单折让额":388.0000,"同单折让率":50.000000},{"品牌":"0752                ","同单销售额":378.0000,"同单折让额":108.0000,"同单折让率":71.428600},{"品牌":"0734                ","同单销售额":207.0500,"同单折让额":238.9500,"同单折让率":-15.406900},{"品牌":"0757                ","同单销售额":198.1800,"同单折让额":130.0200,"同单折让率":34.393000},{"品牌":"0758                ","同单销售额":196.0000,"同单折让额":176.0000,"同单折让率":10.204100},{"品牌":"0706                ","同单销售额":145.0000,"同单折让额":0.3000,"同单折让率":99.793200},{"品牌":"0760                ","同单销售额":83.6200,"同单折让额":16.3800,"同单折让率":80.411400},{"品牌":"0723                ","同单销售额":42.0000,"同单折让额":12.8000,"同单折让率":69.523900},{"品牌":"0763                ","同单销售额":0.0000,"同单折让额":238.0000,"同单折让率":null},{"品牌":"0759                ","同单销售额":0.0000,"同单折让额":240.0000,"同单折让率":null}],
		data4:[{"条码":"6907992633534       ","商品名称":"金领冠菁护3段800g","销售金额":45315.2000,"折让金额":17516.8000,"折扣率":0.72},{"条码":"6907992632520       ","商品名称":"金领冠珍护3段800g","销售金额":43437.6000,"折让金额":17936.4000,"折扣率":0.71},{"条码":"6907925770664       ","商品名称":"飞帆五星优护(厅3)","销售金额":29872.0000,"折让金额":6632.0000,"折扣率":0.82},{"条码":"8715845001823       ","商品名称":"海普诺凯1897较大婴儿奶粉900g","销售金额":26295.7000,"折让金额":1642.3000,"折扣率":0.94},{"条码":"6951025042657       ","商品名称":"哈丁婴幼儿专用洗衣液2L","销售金额":24041.0000,"折让金额":45751.0000,"折扣率":0.34},{"条码":"9333737000427       ","商品名称":"澳优金装幼优(厅3)","销售金额":23086.0000,"折让金额":7854.0000,"折扣率":0.75},{"条码":"6907925770800       ","商品名称":"飞鹤星阶优护(厅3)","销售金额":21684.0000,"折让金额":5560.0000,"折扣率":0.80},{"条码":"6907992632513       ","商品名称":"金领冠珍护2段800g","销售金额":19874.4000,"折让金额":6827.6000,"折扣率":0.74},{"条码":"6907925770145       ","商品名称":"飞帆五星优护(厅2)","销售金额":18152.6700,"折让金额":2465.3300,"折扣率":0.88},{"条码":"6907925770633       ","商品名称":"超级飞帆900g(厅2)","销售金额":17440.0000,"折让金额":3552.0000,"折扣率":0.83},{"条码":"7513035260160       ","商品名称":"启赋(厅3)","销售金额":15676.8000,"折让金额":7875.2000,"折扣率":0.67},{"条码":"6907992632506       ","商品名称":"金领冠珍护1段800g","销售金额":14608.4000,"折让金额":7229.6000,"折扣率":0.67},{"条码":"8715845001830       ","商品名称":"海普诺凯1897幼儿奶粉900g","销售金额":13282.0000,"折让金额":0.0000,"折扣率":1.00},{"条码":"6907925770121       ","商品名称":"飞帆五星优护(厅1)","销售金额":13182.0000,"折让金额":2694.0000,"折扣率":0.83},{"条码":"8712045031773       ","商品名称":"美赞臣蓝臻3段","销售金额":11820.6000,"折让金额":1313.4000,"折扣率":0.90},{"条码":"3760170400020       ","商品名称":"合生元贝嗒星(厅3)金装","销售金额":10734.0000,"折让金额":2080.0000,"折扣率":0.84},{"条码":"6907992633510       ","商品名称":"金领冠菁护1段800g","销售金额":10619.6000,"折让金额":3558.4000,"折扣率":0.75},{"条码":"6921123302434       ","商品名称":"舒比奇高护柔薄舒爽纸尿裤L码","销售金额":10432.4500,"折让金额":5659.5500,"折扣率":0.65},{"条码":"8718117605125       ","商品名称":"牛栏诺优能奶粉(厅3)","销售金额":10432.0000,"折让金额":2528.0000,"折扣率":0.80},{"条码":"6921123302441       ","商品名称":"舒比奇高护柔薄舒爽纸尿裤XL码","销售金额":10324.9500,"折让金额":4277.0500,"折扣率":0.71},{"条码":"8716200719247       ","商品名称":"美素佳儿金装 3#","销售金额":10124.8000,"折让金额":4339.2000,"折扣率":0.70},{"条码":"8712045031759       ","商品名称":"美赞臣蓝臻2段","销售金额":9949.5000,"折让金额":1105.5000,"折扣率":0.90},{"条码":"8712045031261       ","商品名称":"美赞臣荷兰版(厅3)","销售金额":9924.6700,"折让金额":3687.8300,"折扣率":0.73},{"条码":"6907992633527       ","商品名称":"金领冠菁护2段800g","销售金额":9895.2000,"折让金额":5798.8000,"折扣率":0.63},{"条码":"8715845002875       ","商品名称":"澳优氨基酸奶粉(厅1)","销售金额":9552.0000,"折让金额":2388.0000,"折扣率":0.80},{"条码":"8715845002899       ","商品名称":"澳优氨基酸配方(厅3)","销售金额":8358.0000,"折让金额":1592.0000,"折扣率":0.84},{"条码":"6907925887645       ","商品名称":"飞帆900g(厅3)","销售金额":8118.0000,"折让金额":1782.0000,"折扣率":0.82},{"条码":"4008976527589       ","商品名称":"爱他美幼儿配方奶粉(厅3)","销售金额":7919.0000,"折让金额":2331.0000,"折扣率":0.77},{"条码":"3760170400013       ","商品名称":"合生元贝嗒星(厅2)金装","销售金额":7598.0000,"折让金额":1640.0000,"折扣率":0.82},{"条码":"6907925770787       ","商品名称":"飞鹤星阶优护(厅1)","销售金额":7392.0000,"折让金额":1848.0000,"折扣率":0.80},{"条码":"6921123301482       ","商品名称":"舒比奇柔薄舒爽纸尿片XL码大包","销售金额":7029.0000,"折让金额":3663.0000,"折扣率":0.66},{"条码":"6930868201285       ","商品名称":"倍康瞬吸零感尿裤(L码)大包","销售金额":6901.0000,"折让金额":2719.0000,"折扣率":0.72},{"条码":"8716677004983       ","商品名称":"佳贝艾特优装(厅3)大","销售金额":6888.0000,"折让金额":0.0000,"折扣率":1.00},{"条码":"8716200719155       ","商品名称":"美素佳儿金装 1#","销售金额":6745.2000,"折让金额":2890.8000,"折扣率":0.70},{"条码":"00008               ","商品名称":"产后护理其他","销售金额":6008.0000,"折让金额":0.0000,"折扣率":1.00},{"条码":"8716200713184       ","商品名称":"皇家美素佳儿3段800G","销售金额":5826.1000,"折让金额":975.9000,"折扣率":0.86},{"条码":"6903244541176       ","商品名称":"安儿乐极薄小轻芯尿裤(L码)","销售金额":5671.8000,"折让金额":2194.2000,"折扣率":0.72},{"条码":"8718117605910       ","商品名称":"爱他美白金版(厅3)","销售金额":5539.5000,"折让金额":1015.5000,"折扣率":0.85},{"条码":"6921123301918       ","商品名称":"舒比奇高护金薄运动成长裤XL码","销售金额":5341.0000,"折让金额":3087.0000,"折扣率":0.63},{"条码":"8716677004662       ","商品名称":"佳贝艾特悦白  1#","销售金额":5136.0000,"折让金额":0.0000,"折扣率":1.00},{"条码":"9345850007184       ","商品名称":"澳优珀淳婴儿配方奶粉(厅1)","销售金额":4920.0000,"折让金额":1640.0000,"折扣率":0.75},{"条码":"6921123301475       ","商品名称":"舒比奇柔薄舒爽纸尿片L码大包","销售金额":4628.2500,"折让金额":3390.7500,"折扣率":0.58},{"条码":"6941013802493       ","商品名称":"贝肯熊(大M码)尿片","销售金额":4621.5000,"折让金额":1306.5000,"折扣率":0.78},{"条码":"6907925887638       ","商品名称":"飞帆900g(厅2)","销售金额":4560.0000,"折让金额":912.0000,"折扣率":0.83},{"条码":"8712045030615       ","商品名称":"美赞臣荷兰版(厅2)","销售金额":4447.2000,"折让金额":1536.8000,"折扣率":0.74},{"条码":"3760170400037       ","商品名称":"合生元派星(厅1)超金","销售金额":4332.0000,"折让金额":960.0000,"折扣率":0.82},{"条码":"6938952990091       ","商品名称":"好妻子干衣机","销售金额":4302.0000,"折让金额":2862.0000,"折扣率":0.60},{"条码":"9333737000380       ","商品名称":"澳优金装健优(厅2)","销售金额":4216.0000,"折让金额":992.0000,"折扣率":0.81},{"条码":"8712045030608       ","商品名称":"美赞臣荷兰版(厅1)","销售金额":3947.8500,"折让金额":1353.1500,"折扣率":0.74},{"条码":"8716200712972       ","商品名称":"皇家美素佳儿2段(厅)800G","销售金额":3936.0000,"折让金额":600.0000,"折扣率":0.87}]
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.loadData();
				me.loadAreaOptions();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
		},
		loadAreaOptions: function(parentAreaCode, callback){
			var me = this;
			$.getJSON(baseurl + "/sys/area/queryOptions", {
				parentAreaCode: parentAreaCode
			}, function(res) {
				if(callback){
					callback(res);
				}else{
					me.areaOptions = res;
				}
			});
		},
		loadData: function(){
			this.getData1();
			this.getData2();
			this.getData3();
			this.getData4();
		},
		getData1 : function() {
			var me = this;
			me.chart1Data = null;
			$.get(baseurl + "/Report_PC/Branch_DisError/branch_dislv?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.chart1Data = data;
				setTimeout(function(){
					me.createChart1(data);
				});
			});
		},
		getData2 : function() {
			var me = this;
			me.chart2Data = null;
			$.get(baseurl + "/Report_PC/Branch_DisError/branch_cls_dis_fenbu?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.chart2Data = data;
				setTimeout(function(){
					me.createChart2(data);
				});
			});
		},
		getData3 : function() {
			var me = this;
			me.chart3Data = null;
			$.get(baseurl + "/Report_PC/Branch_DisError/branch_cls_branch_dis_fenbu?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.chart3Data = data;
				setTimeout(function(){
					me.createChart3(data);
				});
			});
		},
		getData4 : function() {
			var me = this;
			me.gridData = null;
			$.get(baseurl + "/Report_PC/Branch_DisError/branch_best_sale_item_dis?begin="+me.date.split(' - ')[0]+'&end='+me.date.split(' - ')[1]+'&branchNo='+(me.areaCode?me.areaCode:''),function(data){
				me.gridData = data;
				setTimeout(function(){
					me.GetGrid(data);
				});
			});
		},
		createChart1 : function(data) {
			var me = this;
			var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: data[0].name,
                    type: 'pie',
                    radius: [
                    '70%',
                    '85%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: data[0].value,
                        name: data[0].name,

                        label: {
                            normal: {
                                formatter: '{c}%',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: 100 - data[0].value,
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '平均折扣率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
			ECharts.Draw(option, "chart1");
		},
		createChart2 : function(data) {
			var me = this;
			var data1 = [];
        	var data2 = [];
        	var data3 = [];
        	var data4 = [];
        	for(var i=0,len=data.length; i<len; i++){
        		data1.push(top.InitData.getClsName(data[i]['类别']));
        		data2.push(data[i]['销售金额']);
        		data3.push(data[i]['折让金额']);
        		data4.push(data[i]['平均折让']);
        	}
			var option = {
                title: {
                    text: '各品类折扣情况',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                legend: {
                    top: '9%',
                    data: ['销售金额', '折让金额', '平均折让']
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '金额',
                        axisLabel: {
                            formatter: '{value}/元'
                        }
                    },
                    {
                        type: 'value',
                        name: '折让',
                        min: 0,
                        max: 100,
                        splitLine: {
                            show: false
                        },
                        axisLabel: {
                            formatter: '{value}/%'
                        }
                    }
                ],
                series: [
                    {
                        name: '销售金额',
                        type: 'bar',
                        stack: 'one',
                        data: data2,
                    },
                    {
                        name: '折让金额',
                        type: 'bar',
                        stack: 'one',
                        data: data3,
                    },
                    {
                        name: '平均折让',
                        type: 'line',
                        yAxisIndex: 1,
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(237, 175, 218, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        data: data4,
                    }
                ]
            };
			ECharts.Draw(option, "chart2");
		},
		createChart3 : function(data) {
			var me = this;
			var data1 = [];
        	var data2 = [];
        	var data3 = [];
        	var data4 = [];
        	for(var i=0,len=data.length; i<len; i++){
        		data1.push(top.InitData.getBrandName(data[i]['品牌']));
        		data2.push(data[i]['同单销售额']);
        		data3.push(data[i]['同单折让额']);
        		data4.push(data[i]['同单折让率']);
        	}
			var option = {
                title: {
                    text: '各品牌同单折让情况（同单为包含该品牌的单的整体折让）',
                    x: 'left'
                },
                tooltip: {
                    trigger: 'axis',
                    
                    axisPointer: {            // 坐标轴指示器，坐标轴触发有效 按类别各品牌同单销售金额、折让金额、同单平均折扣分析
                        type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                    }
                },
                legend: {
                    x: 'right',
                    data: ['同单销售额', '同单折让额', '同单折让率']
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '金额',
                        axisLabel: {
                            formatter: '{value}元'
                        }
                    },
                    {
                        type: 'value',
                        name: '折让',
                        splitLine: {
                            show: false
                        },
                        axisLabel: {
                            formatter: '{value}%'
                        }
                    }
                ],
                series: [
                    {
                        name: '同单销售额',
                        type: 'bar',
                        stack: 'one',
                        data: data2,
                    },
                    {
                        name: '同单折让额',
                        type: 'bar',
                        stack: 'one',
                        data: data3,
                    },
                    {
                        name: '同单折让率',
                        type: 'line',
                        yAxisIndex: 1,
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(237, 175, 218, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        data: data4,
                    }
                ]
            };
			ECharts.Draw(option, "chart3");
		},
		GetGrid: function (data) {
			if(data == null || data.length == 0)return;
			var $gridTable = $("#gridPanel");
            $gridTable.jqGrid({
            	datatype: "local",
                data:data,
                colModel: [
                    { name: "条码", label: "条码", hidden:true, align: "left", sortable: true },
                    { name: "商品名称", label: "商品名称", width:400, align: "left", sortable: true },
                   $.extend({ name: "销售金额", label: "销售金额", align: "center", sortable: true }, jqFormatter.RMB0),
                    { name: "折扣率", label: "折扣率", align: "center", sortable: true, formatter: jqFormatter.Percent }
                ],
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                height: 700,
                width: $('.gridCard').width() - 10,
                footerrow: true,
                rowNum: 100,
                loadonce: true,                 
                onCellSelect: function (rowid, iCol, contents, event) {
	                var item_noN = $(this).getCell(rowid,1);
	                myColorbox({ href: "/CloudSale/Report_PC/web_page/Aspx/sales_details_inquiry.aspx?branch=" + branch + "&sDate=" + sDate + "&eDate=" + eDate + "&item_no=" + item_noN, closeButton: false, width: '95%', height: '95%', iframe: true })
                }, 
                gridComplete: function () {
                    var gridName = $gridTable;
                    var a = $(this).getCol("销售金额", false, "sum");
                    //合计
                    $(this).footerData("set", {
                        "商品名称": "合计：",
                        "销售金额": fixNum(a,0)
                    });
                    condition.backColor_ABC($(this).attr("id"), "折扣率", 80, 70, 60);
                }
            });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        }
	}
});