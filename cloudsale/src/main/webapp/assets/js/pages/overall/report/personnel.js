var pageVM = new Vue({
    el : '#page',
    components: {
        Treeselect: VueTreeselect.Treeselect
    },
    data : {
        gridTab: 0,
        areaCode: null,
        areaOptions:[],
        date: moment().startOf('month').format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
        salerInfo:{},
        data1:[{"value":"828,60812,2.51,叶芳"},{"value":"872,52486,3.84,幸大鹏"},{"value":"591,44076,2.14,姚艳霞"},{"value":"708,43347,2.53,肖文飚"},{"value":"594,42555,2.07,杨靖"},{"value":"497,40595,2.74,党明惠"},{"value":"586,40417,2.56,王长旭"},{"value":"599,37410,2.53,于鸿伟"},{"value":"694,36984,2.01,陶永华"},{"value":"450,36862,2.21,马春秀"},{"value":"589,36396,2.26,空"},{"value":"717,35734,1.92,史银红"},{"value":"601,35314,3.07,张长杰"},{"value":"521,34010,2.49,徐敏"},{"value":"642,33850,1.92,石卫平"},{"value":"611,31184,2.23,刘志忠"},{"value":"724,30047,2.52,陈声波"},{"value":"686,29705,1.85,刘倍菡"},{"value":"411,29626,2.76,陶翔华"},{"value":"613,28816,2.49,李群仙"},{"value":"547,28666,2.01,潘俊武"},{"value":"371,28503,1.96,周国强"},{"value":"464,27164,2.53,乐开诚"},{"value":"649,26737,2.31,郑振洲"},{"value":"508,26144,1.87,黄晋"},{"value":"401,25856,2.58,段爱红"},{"value":"300,25201,2.20,赖莉"},{"value":"516,24755,2.25,空"},{"value":"381,24251,2.67,谢纯德"},{"value":"876,22495,2.44,张强"},{"value":"341,22162,2.32,朱贤申"},{"value":"401,21539,2.44,空"},{"value":"288,20940,2.09,空"},{"value":"264,20474,2.00,侯慧娟"},{"value":"283,20121,2.20,空"},{"value":"253,18549,2.14,刘洁筱"},{"value":"296,18188,1.98,空"},{"value":"463,18117,1.12,左嘉"},{"value":"235,18051,2.09,庄葛巍"},{"value":"543,17942,2.21,王思彤"},{"value":"317,17735,2.54,朱庆中"},{"value":"324,17206,2.41,马瑞"},{"value":"277,17021,2.38,文燕"},{"value":"435,16864,1.85,空"},{"value":"249,16831,2.21,朱振坤"},{"value":"195,16333,1.87,郝玉杰"},{"value":"346,16303,1.93,空"},{"value":"228,14855,2.34,空"},{"value":"402,14007,2.03,空"},{"value":"233,13709,2.00,李向东"},{"value":"179,13080,2.01,赵静静"},{"value":"284,11724,2.80,空"},{"value":"216,11708,1.91,空"},{"value":"373,11533,3.06,廖晨光"},{"value":"198,11434,2.46,朱安全"},{"value":"180,11101,2.11,邓树清"},{"value":"228,10381,2.53,杨克军"},{"value":"189,9506,1.82,刘新中"},{"value":"165,7910,2.10,柳青"},{"value":"107,5404,2.13,高舒"},{"value":"134,4823,1.62,魏剑"},{"value":"3,4647,1.00,何万桢"},{"value":"147,3935,1.87,杜春"},{"value":"5,3482,1.25,王洋"},{"value":"123,3230,0.84,空"},{"value":"2,2549,1.00,王四知"},{"value":"62,2205,3.93,李铮"},{"value":"32,1191,0.69,欧晓东"},{"value":"1341,748,1.19,郑旭东"},{"value":"42,469,0.97,徐友筠"},{"value":"135,376,0.91,陈澍"},{"value":"2,135,2.00,刘恩惠"},{"value":"24,132,0.83,奇秀波"},{"value":"9,67,1.75,舒开旗"},{"value":"3,28,1.00,陈新定"},{"value":"1,15,1.00,刘禾"},{"value":"1,6,1.00,韩永峰"},{"value":"9,-1,0.82,岳涛"},{"value":"0,-8,0.00,梁晓惠"},{"value":"0,-296,0.00,雷湘军"},{"value":"13,-1092,0.87,空"}],
        data2:[{"客单价":167.154539,"连带率":3.118984,"退货率":-0.170823,"人均销售":17129.541363,"坪效":1177.6559687499998,"时间差":14}],
        data3:[{"name":"叶芳","value":60812.78},{"name":"幸大鹏","value":52486.58},{"name":"姚艳霞","value":44076.21},{"name":"肖文飚","value":43347.37},{"name":"杨靖","value":42555.13},{"name":"党明惠","value":40595.11},{"name":"王长旭","value":40417.56},{"name":"于鸿伟","value":37410.00},{"name":"陶永华","value":36984.68},{"name":"马春秀","value":36862.16},{"name":"958   ","value":36396.25},{"name":"史银红","value":35734.26},{"name":"张长杰","value":35314.60},{"name":"徐敏","value":34010.69},{"name":"石卫平","value":33850.23},{"name":"刘志忠","value":31184.35},{"name":"陈声波","value":30047.54},{"name":"刘倍菡","value":29705.56},{"name":"陶翔华","value":29626.34},{"name":"李群仙","value":28816.02},{"name":"潘俊武","value":28666.84},{"name":"周国强","value":28503.42},{"name":"乐开诚","value":27164.21},{"name":"郑振洲","value":26737.04},{"name":"黄晋","value":26144.68},{"name":"段爱红","value":25856.68},{"name":"赖莉","value":25201.32},{"name":"1707  ","value":24755.79},{"name":"谢纯德","value":24251.69},{"name":"张强","value":22495.49},{"name":"朱贤申","value":22162.41},{"name":"707   ","value":21539.09},{"name":"3505  ","value":20940.84},{"name":"侯慧娟","value":20474.87},{"name":"3504  ","value":20121.92},{"name":"刘洁筱","value":18549.37},{"name":"635   ","value":18188.75},{"name":"左嘉","value":18117.60},{"name":"庄葛巍","value":18051.74},{"name":"王思彤","value":17942.13},{"name":"朱庆中","value":17735.38},{"name":"马瑞","value":17206.89},{"name":"文燕","value":17021.03},{"name":"306   ","value":16864.76},{"name":"朱振坤","value":16831.45},{"name":"郝玉杰","value":16333.03},{"name":"1706  ","value":16303.94},{"name":"706   ","value":14855.43},{"name":"307   ","value":14007.28},{"name":"李向东","value":13709.36},{"name":"赵静静","value":13080.80},{"name":"1403  ","value":11724.35},{"name":"3506  ","value":11708.03},{"name":"廖晨光","value":11533.72},{"name":"朱安全","value":11434.03},{"name":"邓树清","value":11101.12},{"name":"杨克军","value":10381.09},{"name":"刘新中","value":9506.34},{"name":"柳青","value":7910.99},{"name":"高舒","value":5404.59},{"name":"魏剑","value":4823.88},{"name":"何万桢","value":4647.00},{"name":"杜春","value":3935.54},{"name":"王洋","value":3482.00},{"name":"1612  ","value":3230.00},{"name":"王四知","value":2549.80},{"name":"李铮","value":2205.00},{"name":"欧晓东","value":1191.90},{"name":"郑旭东","value":748.72},{"name":"徐友筠","value":469.50},{"name":"陈澍","value":376.40},{"name":"刘恩惠","value":135.80},{"name":"奇秀波","value":132.70},{"name":"舒开旗","value":67.00},{"name":"陈新定","value":28.10},{"name":"刘禾","value":15.00},{"name":"韩永峰","value":6.00},{"name":"岳涛","value":-1.10},{"name":"梁晓惠","value":-8.00},{"name":"雷湘军","value":-296.00},{"name":"9999  ","value":-1092.51}],
        data4:[{"员工编号":"1301","姓名":"幸大鹏","门店名称":"000011","入职时间":"","工龄":1428,"销售_本期":52486.5800,"有效天数_本期":11,"成交单数_本期":158,"销量_本期":872.0000,"客单价_本期":332.193544,"连带率_本期":5.518987,"销售_环比":42759.3200,"有效天数_环比":15,"成交单数_环比":241,"销量_环比":783.0000,"客单价_环比":177.424564,"连带率_环比":3.248962,"销售_同比":46843.0500,"有效天数_同比":14,"成交单数_同比":248,"销量_同比":780.0000,"客单价_同比":188.883266,"连带率_同比":3.145161},{"员工编号":"203","姓名":"张强","门店名称":"000201","入职时间":null,"工龄":null,"销售_本期":22495.4900,"有效天数_本期":10,"成交单数_本期":90,"销量_本期":876.0000,"客单价_本期":249.949888,"连带率_本期":9.733333,"销售_环比":43871.0900,"有效天数_环比":14,"成交单数_环比":183,"销量_环比":2262.0000,"客单价_环比":239.732732,"连带率_环比":12.360655,"销售_同比":17939.4500,"有效天数_同比":11,"成交单数_同比":89,"销量_同比":449.0000,"客单价_同比":201.566853,"连带率_同比":5.044943},{"员工编号":"205","姓名":"魏剑","门店名称":"000201","入职时间":null,"工龄":null,"销售_本期":4823.8800,"有效天数_本期":9,"成交单数_本期":71,"销量_本期":134.0000,"客单价_本期":67.941971,"连带率_本期":1.887323,"销售_环比":0.0000,"有效天数_环比":0,"成交单数_环比":0,"销量_环比":0.0000,"客单价_环比":null,"连带率_环比":null,"销售_同比":21673.0100,"有效天数_同比":11,"成交单数_同比":100,"销量_同比":395.0000,"客单价_同比":216.730100,"连带率_同比":3.950000}]
    },
    mounted : function() {
        this.init();
    },
    methods : {
        init : function() {
            var me = this;
            setTimeout(function() {
                me.initEvent();
                me.loadData();
                me.loadAreaOptions();
            });
        },
        search:function () {
            $("#gridTable").GridUnload();
            this.loadData();
        },
        initEvent: function(){
            var me = this;
            $('#page').find('.daterange').dateRange();
        },
        loadAreaOptions: function(parentAreaCode, callback){
            var me = this;
            $.getJSON(baseurl + "/sys/area/queryOptions", {
                parentAreaCode: parentAreaCode
            }, function(res) {
                if(callback){
                    callback(res);
                }else{
                    console.log(res)
                    me.areaOptions = res;
                }
            });
        },
        loadData: function(){
            this.getData0();
            this.getChartData();
        },
        getChartData: function(){
            var me = this;
            me.getData1(function (data) {
                me.createChart1(data);
            })
            me.getData2(function (data) {
                me.createChart2(data);
            })
            me.getData3(function (data) {
                me.createChart3(data);
            })
            me.getData4(function (data) {
                me.GetGrid(data);
            })
        },
        getData0:function () {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Saler_Report/salerInfo?branchNo="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: false,
                dataType: "json",
                success: function (data) {
                    myData = data||{};
                    me.salerInfo.total = myData.目前在职;
                    me.salerInfo.newJoin = myData.本期入职人数;
                    me.salerInfo.left = myData.本期离职人数;
                    me.salerInfo.hasGainCnt = myData.有销售人数;
                    me.salerInfo.renjun = myData.有销售人均贡献;
                }
            });
            return myData
        },
        getData1:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Saler_Report/salerDetail?branchNo="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    myData = data||[];
                    callback(myData);
                }
            });
        },
        getData2:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Saler_Report/salerRadio?branchNo="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    myData = data||[];
                    callback(myData)
                }
            });
        },
        getData3:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Saler_Report/salerRank?branchNo="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    myData = data||[];
                    callback(myData)
                }
            });
        },
        getData4:function (callback) {
            var me = this;
            var myData = [];
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            $.ajax({
                url: baseurl + "/Report_PC/Saler_Report/salerOther?branchNo="+(me.areaCode?me.areaCode:"")+"&begin="+begin+"&end="+end,
                type: "get",
                cache: false,
                async: true,
                dataType: "json",
                success: function (data) {
                    myData = data||[];
                    callback(myData)
                }
            });
        },
        createChart1 : function(data) {
            var me = this;
            var xx = [];
            for (var i = 0; i < data.length; i++) {
                xx.push(data[i].value.split(","))
            }
            var schema = [
                { name: '销售数量', index: 0, text: '销售数量' },
                { name: '销售金额', index: 1, text: '销售金额' },
                { name: '连带率', index: 2, text: '连带率' },
                { name: '姓名', index: 3, text: '姓名' },
            ];
            var option = {
                title: {
                    text: '●公司全体门店人员最近三十天销售贡献分布',
                    subtext: '注：气泡大小为连带率、红色虚线为平均值'
                },
                grid: {
                    top: '80px',
                    left: '1%',
                    right: '5%',
                    bottom: '1%',
                    containLabel: true
                },
                toolbox: {
                    show: true,
                    feature: {
                        dataZoom: {
                            yAxisIndex: 'none'
                        },
                        restore: {}
                    }
                },
                tooltip: {
                    backgroundColor: '#222',
                    borderColor: '#777',
                    borderWidth: 1,
                    formatter: function (obj) {
                        var value = obj.value;
                        if (value.length > 1) {
                            return '<div style="border-bottom: 1px solid rgba(255,255,255,.3); font-size: 18px;padding-bottom: 7px;margin-bottom: 7px">'
                                + obj.seriesName + '</div>'
                                + schema[3].text + '：' + value[3] + '<br>'
                                + schema[0].text + '：' + value[0] + '<br>'
                                + schema[1].text + '：' + value[1] + '<br>'
                                + schema[2].text + '：' + value[2] + '<br>';
                        } else {
                            return obj.name + ' : '
                                + obj.value + '';
                        }
                    }
                },
                xAxis: {
                    //name: '销售数量',
                    splitLine: {
                        lineStyle: {
                            type: 'dashed'
                        }
                    }
                },
                yAxis: {
                    // name: '销售金额',
                    splitLine: {
                        lineStyle: {
                            type: 'dashed'
                        }
                    },
                    scale: true
                },
                series: [{
                    name: '营业员销售详情',
                    data: xx,
                    type: 'scatter',
                    symbolSize: function (data) {
                        return Math.sqrt(data[1] / 100);
                    },
                    markLine: {
                        data: [
                            { type: 'average', name: '平均销售数量', valueIndex: '0' },
                            { type: 'average', name: '平均销售金额', valueIndex: '1' },
                            //{ xAxis: 170 }
                        ]
                    }

                }]
            };
            ECharts.Draw(option, "chart1");
        },
        createChart2 : function(data) {
            var me = this;
            var option = {
                title: {
                    text: '●人员各评估指标',
                    x: 'left',
                    top:'0'
                },
                grid: {
                    left: '3%',
                    right: '3%',
                    bottom: '3%',
                    containLabel: true
                },
                tooltip: { confine: true },
                radar: {
                    // shape: 'circle',
                    indicator: [
                        { name: '客单价', max: 500, min:0 },
                        { name: '连带率', max: 10, min:0},
                        { name: '退货率', max: 0, min: -0.2 },
                        { name: '人均销售', max:2000 * data[0]["时间差"], min: 0 },
                        { name: '坪效', max: 100 * data[0]["时间差"], min: 0 }
                    ],
                    name: {
                        textStyle: {
                            color: '#000'
                        }
                    }, splitLine: {
                        show: true,
                        lineStyle: {
                            width: 1,
                            type: "dotted",
                            color: "#d3d3d3"
                        }
                    },
                    axisLine: {
                        lineStyle: {
                            type: "dotted",
                            color: "#d3d3d3"
                        }
                    },
                    center: ["53%", "60%"],
                    radius: "60%"
                },
                series: [{
                    name: '门店人员评估指标',
                    type: 'radar',
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                color: "rgba(123, 147, 224, 0.2)",
                                type: "default"
                            },
                            lineStyle: {
                                width: 1
                            }
                        }
                    },
                    data: [
                        {
                            value: [data[0]["客单价"], data[0]["连带率"], data[0]["退货率"], data[0]["人均销售"], (data[0]["坪效"] + 0)],
                            name: '评估得分'
                        }
                    ]
                }]
            };
            ECharts.Draw(option, "chart2");
        },
        createChart3 : function(data) {
            var me = this;
            var option = {
                title: {
                    text: '●店员销售排名分布',
                    subtext: '注：文字大小代表销售金额多少',
                    top: '0'
                },
                tooltip: {},
                grid: {
                    left: '0%',
                    right: '0%',
                    bottom: '2%',
                    containLabel: true
                },
                series: [{
                    type: 'wordCloud',
                    gridSize: 10,
                    sizeRange: [10, 60],
                    rotationRange: [-45, 45],
                    top:'20%',
                    width: '100%',
                    height: '100%',
                    shape: 'circle',
                    textStyle: {
                        emphasis: {
                            shadowBlur: 1,
                            // color: 'red'
                        }
                    },
                    data: data
                }]
            }
            ECharts.Draw(option, "chart3");
        },
        createChart4 : function(data) {
            var me = this;
            var option = {
                title: {
                    text: '各品类退货分布',
                    x: 'center'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [
                    {
                        type: 'pie',
                        radius: '55%',
                        data: data,
                        itemStyle: {
                            normal: {
                                label: {
                                    show: true,
                                    formatter: '{b} : ￥{c}',
                                    textStyle: {
                                        color: '#333'
                                    }
                                },
                                labelLine: { show: true }
                            }
                        }
                    }
                ]
            };
            ECharts.Draw(option, "chart4");
        },
        createChart5 : function(data) {
            var me = this;
            var option = {
                title: {
                    text: '单品退货TOP10(按退货金额排行)',
                    x: 'center'
                },
                tooltip: {
                    show: true,
                    formatter: "{b}<br />{a}: {c}元",
                },
                grid: {
                    left: '0%',
                    right: '0%',
                    bottom: '0%',
                    top:"10%",
                    containLabel: true
                },
                xAxis: {
                    show: false,
                    type: 'value',
                },
                yAxis: {
                    type: 'category',
                    data: public_data.getEchartsData(data, '名称', ''),
                    position: 'right',
                    splitArea: {
                        show: true
                    },
                },
                series: [
                    {
                        type: 'bar',
                        label: {
                            normal: {
                                show: true,
                                position: 'right',
                                formatter: '￥{c}'
                            }
                        },
                        data: public_data.getEchartsData(data, '退货金额', '')
                    }
                ]
            };
            ECharts.Draw(option, "chart5");
        },
        GetGrid: function (data) {
            var keyWord = {
                "col_name": "员工编号~姓名~门店名称~入职时间~工龄~销售_本期~有效天数_本期~成交单数_本期~销量_本期~客单价_本期~连带率_本期~销售_环比~有效天数_环比~成交单数_环比~销量_环比~客单价_环比~连带率_环比~销售_同比~有效天数_同比~成交单数_同比~销量_同比~客单价_同比~连带率_同比"
            }
            var jsonArr = data;
            var $gridTable = $("#gridTable");
            var colNames = keyWord.col_name.split("~");
            var colModel = [];
            var avgCol = [];
            var sumCol = [];
            var i = 0;
            for (var i = 0; i < colNames.length; i++) {
                var json = colNames[i]
                if (i <= 3) {
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float' })
                } else {
                    colModel.push({ name: json, label: json.split("_")[0], align: "center", sortable: true, sorttype: 'float' })
                }
                if (json.indexOf("门店名称") >= 0) {
                    $.extend(colModel[i], { formatter: top.InitData.getBranchName })

                }
                // else if (json.indexOf("工龄") >= 0) {
                //     $.extend(colModel[i], jqFormatter.Num0, { label: "工龄（月）" }, { summaryType: 'avg' })
                // }
                else if (json.indexOf("销售") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("销量") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { hidden: true, summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("成交单数") >= 0 || json.indexOf("有效天数") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0, { summaryType: 'sum' })
                    sumCol.push(json);
                } else if (json.indexOf("价") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                } else if (json.indexOf("率") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num2)
                }
            }
            $gridTable.jqGrid({
                datatype: "local",
                data: jsonArr,//数据数组
                height: 'auto',
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rownumWidth: 50,
                rowNum: 1000,
                width: $('.gridCard').width() - 10,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                grouping: true,
                groupingView: {
                    groupField: ['门店名称'],
                    groupSummary: [true],
                    groupColumnShow: [true],
                    groupText: ['<b>{0}各人员销售概况</b>'],
                    groupCollapse: false,
                },
                gridComplete: function () {
                    var sumColArr = {};
                    for (var i = 0; i < sumCol.length; i++) {
                        eval("sumColArr." + sumCol[i] + "=" + $(this).getCol(sumCol[i], false, "sum"))
                    }
                    sumColArr.入职时间 = "合计";
                    sumColArr.完成率 = sumColArr.本期销售;
                    sumColArr.工龄 = $(this).getCol("工龄", false, "avg")
                    sumColArr.客单价_本期 = sumColArr.销售_本期 / sumColArr.成交单数_本期;
                    sumColArr.客单价_环比 = sumColArr.销售_环比 / sumColArr.成交单数_环比;
                    sumColArr.客单价_同比 = sumColArr.销售_同比 / sumColArr.成交单数_同比;
                    sumColArr.连带率_本期 = sumColArr.销量_本期 / sumColArr.成交单数_本期;
                    sumColArr.连带率_环比 = sumColArr.销量_环比 / sumColArr.成交单数_环比;
                    sumColArr.连带率_同比 = sumColArr.销量_同比 / sumColArr.成交单数_同比;
                    $(this).footerData("set", sumColArr);
                    condition.backColor_ABC($(this).attr("id"), "完成率", 95, 90, 85);
                    condition.backColor_ABC($(this).attr("id"), "客单价", 200, 150, 100);
                    condition.fontColor_ABC($(this).attr("id"), "连带率", 4, 3, 2);
                    $(this).find("[aria-describedby*='工龄']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='工龄']").css("border-right", "1px dashed red")
                    $(this).find("[aria-describedby*='完成率']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='完成率']").css("border-right", "1px dashed red")
                    $(this).find("[aria-describedby*='连带率']").css("border-right", "1px dashed red")
                    $(".ui-jqgrid-hdiv").find("th[id*='连带率']").css("border-right", "1px dashed red")
                }
            })
            jQuery("#gridTable").jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: [
                    { startColumnName: '销售_本期', numberOfColumns: 6, titleText: '员工本期销售概况' },
                    { startColumnName: '销售_环比', numberOfColumns: 6, titleText: '员工环比销售概况' },
                    { startColumnName: '销售_同比', numberOfColumns: 6, titleText: '员工同比销售概况' }
                ]
            });
            var footGroup = $("#gridPanel_3").find(".ui-widget-content.jqfoot.ui-row-ltr")
            for (var j = 0; j < footGroup.length; j++) {
                var sale_B = parseFloat(footGroup.eq(j).find("[aria-describedby*='销售_本期']").text().replace("¥", "").replace(/,/g, ''))
                var sale_H = parseFloat(footGroup.eq(j).find("[aria-describedby*='销售_环比']").text().replace("¥", "").replace(/,/g, ''))
                var sale_T = parseFloat(footGroup.eq(j).find("[aria-describedby*='销售_同比']").text().replace("¥", "").replace(/,/g, ''))
                var qnty_B = parseFloat(footGroup.eq(j).find("[aria-describedby*='销量_本期']").text().replace("¥", "").replace(/,/g, ''))
                var qnty_H = parseFloat(footGroup.eq(j).find("[aria-describedby*='销量_环比']").text().replace("¥", "").replace(/,/g, ''))
                var qnty_T = parseFloat(footGroup.eq(j).find("[aria-describedby*='销量_同比']").text().replace("¥", "").replace(/,/g, ''))
                var flow_B = parseFloat(footGroup.eq(j).find("[aria-describedby*='成交单数_本期']").text().replace("¥", "").replace(/,/g, ''))
                var flow_H = parseFloat(footGroup.eq(j).find("[aria-describedby*='成交单数_环比']").text().replace("¥", "").replace(/,/g, ''))
                var flow_T = parseFloat(footGroup.eq(j).find("[aria-describedby*='成交单数_同比']").text().replace("¥", "").replace(/,/g, ''))
                footGroup.eq(j).find("[aria-describedby*='客单价_本期']").html(flow_B == 0 ? "" : fixNum((sale_B / flow_B),2));
                footGroup.eq(j).find("[aria-describedby*='客单价_环比']").html(flow_H == 0 ? "" : fixNum((sale_H / flow_H),2));
                footGroup.eq(j).find("[aria-describedby*='客单价_同比']").html(flow_T == 0 ? "" : fixNum((sale_T / flow_T),2));
                footGroup.eq(j).find("[aria-describedby*='连带率_本期']").html(flow_B == 0 ? "" : fixNum((qnty_B / flow_B),2));
                footGroup.eq(j).find("[aria-describedby*='连带率_环比']").html(flow_H == 0 ? "" : fixNum((qnty_H / flow_H),2));
                footGroup.eq(j).find("[aria-describedby*='连带率_同比']").html(flow_T == 0 ? "" : fixNum((qnty_T / flow_T),2));
            };
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    $gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        }
    }
});