var pageVM = new Vue({
	el : '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data : {
		gridTab: 0,
		areaCode: null,
		areaOptions:top.branchOptionData,
		chart1Data:null,
		chart2Data:null,
		chart3Data:null,
		chart4Data:null,
		chart5Data:null,
		data1:[{"value1":"测试02店2019-01-10日销售报告","value2":"----本月还剩：21天,今日最低任务：3324元----","value3":"本月任务：98299 | 累计完成：28498.5 | 完成率：29% | 预测完成率82%"}],
		data2:[{"成交金额":1133,"成交单数":19,"客单价":59.61,"购物连带率":1.47,"营业面积":50,"坪效":22.65,"有效工号":3,"人均销售":378,"新增会员":15,"新增客户":1}],
		data3:[{"item_no":"9345850007207       ","条码":"9345850007207","商品名称":"澳优珀淳幼儿配方奶粉(厅3）","库存数量":0.0000000000,"销售数量":52.0000,"销售金额":11622.0000},{"item_no":"6907925770633       ","条码":"6907925770633","商品名称":"超级飞帆900g(厅2)","库存数量":8.0000000000,"销售数量":41.0000,"销售金额":11208.0000}],
		data4:[{"昨日销售":1132.6200,"今日最低任务":3323.8623777136381}],
		data5:[{"本月累计":28498.5500,"本月任务":98299.6599319864}],
		data6:[{"时间":9,"客流":7,"客单价":48.157142,"时段销售额":337.1000},{"时间":10,"客流":1,"客单价":149.000000,"时段销售额":149.0000},{"时间":11,"客流":4,"客单价":2980.000000,"时段销售额":11920.0000},{"时间":13,"客流":2,"客单价":50.000000,"时段销售额":100.0000},{"时间":16,"客流":2,"客单价":57.260000,"时段销售额":114.5200},{"时间":17,"客流":1,"客单价":236.000000,"时段销售额":236.0000},{"时间":18,"客流":1,"客单价":196.000000,"时段销售额":196.0000},{"时间":19,"客流":1,"客单价":-11920.000000,"时段销售额":-11920.0000}],
		data7:[{"name":"食品","value":209.9000},{"name":"纸尿裤","value":100.0000},{"name":"其他","value":49.1000}],
		data8:[{"name":"100元以下","value":9},{"name":"100至200元","value":4},{"name":"200至500元","value":2},{"name":"500至1000元","value":1},{"name":"1000元以上","value":3}],
		data9:[],
		data10:[],
		data11:[{"item_no":"9345850007207","条码":"9345850007207","商品名称":"澳优珀淳幼儿配方奶粉(厅3）","月销数量":52.0000,"日均销售":1.730000,"月销金额":11622.0000,"门店库存":0.0000000000,"缺货数量":4.000000,"总部库存":-3.0000000000},{"item_no":"8712045031261","条码":"8712045031261","商品名称":"美赞臣荷兰版(厅3)","月销数量":20.0000,"日均销售":0.670000,"月销金额":3465.0000,"门店库存":0.0000000000,"缺货数量":2.000000,"总部库存":438.0000000000}]
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.loadData();
			});
		},
		initEvent: function(){
			var me = this;
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    //$("#gridPanel_1").setGridWidth($("#gridPanel_1").parents('.card-body').width());
                }, 200);
            });
		},
		changeGridTab: function(index){
			this.gridTab = index;
			switch(index){
			case 0:
				this.GetGrid0(this.data3);
				break;
			case 1:
				if(this.data9.length > 0){
					this.GetGrid1(this.data9);
				}else{
					this.getData9();
				}
				break;
			case 2:
				if(this.data10.length > 0){
					this.GetGrid1(this.data10);
				}else{
					this.getData10();
				}
				break;
			}
		},
		loadData: function(){
			this.getData1();
			this.getData2();
			this.getData3();
			this.getData4();
			this.getData5();
			this.getData6();
			this.getData7();
			this.getData8();
			this.getData9();
			this.getData10();
		},
		getData1: function(){
			var me = this;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/day_month_branch_gain_abstract?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.data1 = data;
			});
		},
		getData2: function(){
			var me = this;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/yesterday_branch_gain?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.data2 = data;
			});
		},
		getData3: function(){
			var me = this;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/yesterday_branch_best_item?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.data3 = data;
				me.GetGrid0(data);
			});
		},
		getData4: function(){
			var me = this;
			me.chart1Data = null;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/today_lowest_target?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.chart1Data = data;
				setTimeout(function(){
					me.createChart1(data);
				});
			});
		},
		getData5: function(){
			var me = this;
			me.chart2Data = null;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/month_gain?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.chart2Data = data;
				setTimeout(function(){
					me.createChart2(data);
				});
			});
		},
		getData6: function(){
			var me = this;
			me.chart3Data = null;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/yesterday_period?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.chart3Data = data;
				setTimeout(function(){
					me.createChart3(data);
				});
			});
		},
		getData7: function(){
			var me = this;
			me.chart4Data = null;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/yesterday_cls?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.chart4Data = data;
				setTimeout(function(){
					me.createChart4(data);
				});
			});
		},
		getData8: function(){
			var me = this;
			me.chart5Data = null;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/yesterday_price_zone?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.chart5Data = data;
				setTimeout(function(){
					me.createChart5(data);
				});
			});
		},
		getData9: function(){
			var me = this;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/yesterday_branch_bad_item?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.data9 = data;
				me.GetGrid1(data);
			});
		},
		getData10: function(){
			var me = this;
			$.get(baseurl + "/Report_PC/Branch_DayMonth/yesterday_branch_exception_stock_item?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.data10 = data;
				me.GetGrid2(data);
			});
		},
		createChart1 : function(data) {
			var me = this;
			var data1 = [];
        	var data2 = [];
        	for(var i=0,len=data.length; i<len; i++){
        		data1.push(data[i]['今日最低任务']);
        		data2.push(data[i]['昨日销售']);
        	}
			var option = {
                title: {
                    show: true,
                    x: "center",
                    y: "50%",
                    text: '本月累计：元', //幸运值取代码置于值于此处
                    textStyle: {
                        fontSize: 12,
                        fontWeight: 'bolder',
                        fontStyle: 'normal',
                    }
                },
                tooltip: {
                    formatter: "{a} <br/>{c} {b}"
                },
                series: [
                    {
                        name: '昨日销售',
                        type: 'gauge',
                        z: 3,
                        min: 0,
                        max: data1,
                        radius: '95%',
                        axisLine: {            // 坐标轴线
                            lineStyle: {       // 属性lineStyle控制线条样式
                                width: 15,
                                color: [
                                [0, '#7B93E0'],
                                [0.333333, '#7B93E0'],
                                [0.666666, '#59c4e6'],
                                [1, '#edafda']
                                ]
                            }
                        },
                        axisTick: {
                            show: false
                        },
                        axisLabel: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        },
                        pointer: {
                            width: "3%",
                            length: '80%'
                        },
                        itemStyle: {
                            normal: {
                                color: "rgba(255, 255, 255, 0.8)",
                                shadowBlur: 1
                            }
                        },
                        detail: {
                            show: true,
                            textStyle: {
                                fontSize: 16
                            }
                        },
                        data: [{ value: data2 }]
                    }
                ]
            };
			ECharts.Draw(option, "chart1");
		},
		createChart2 : function(data) {
			var me = this;
			var data1 = [];
        	var data2 = [];
        	for(var i=0,len=data.length; i<len; i++){
        		data1.push(data[i]['本月任务']);
        		data2.push(data[i]['本月累计'].toFixed(2));
        	}
			var option = {
                title: {
                    show: true,
                    x: "center",
                    y: "50%",
                    text: '本月累计：元', //幸运值取代码置于值于此处
                    //subtext: '幸运指数',
                    textStyle: {
                        fontSize: 12,
                        fontWeight: 'bolder',
                        fontStyle: 'normal'
                    }
                },
                tooltip: {
                    formatter: "{a} <br/>{c} {b}"
                },
                series: [
                    {
                        name: '昨日销售',
                        type: 'gauge',
                        z: 3,
                        min: 0,
                        max: data1,
                        radius: '95%',
                        axisLine: {            // 坐标轴线
                            lineStyle: {       // 属性lineStyle控制线条样式
                                width: 15,
                                color: [
                                [0, '#7B93E0'],
                                [0.333333, '#7B93E0'],
                                [0.666666, '#59c4e6'],
                                [1, '#edafda']
                                ]
                            }
                        },
                        axisTick: {
                            show: false
                        },
                        axisLabel: {
                            show: false
                        },
                        splitLine: {
                            show: false
                        },
                        pointer: {
                            width: "3%",
                            length: '80%'
                        },
                        itemStyle: {
                            normal: {
                                color: "rgba(255, 255, 255, 0.8)",
                                shadowBlur: 1
                            }
                        },
                        detail: {
                            show: true,
                            textStyle: {
                                fontSize: 16
                            }
                        },
                        data: [{ value: data2 }]
                    }
                ]
            };
			ECharts.Draw(option, "chart2");
		},
		createChart3 : function(data) {
			var me = this;
			var data1 = [];
        	var data2 = [];
        	var data3 = [];
        	for(var i=0,len=data.length; i<len; i++){
        		data1.push(data[i]['时间']);
        		data2.push(data[i]['客流']);
        		data3.push(data[i]['客单价']);
        	}
			var option = {
                title: {
                    text: '●客流及成交时段分析',
                    x: 'left'
                },
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    top: '40',
                    bottom: '1%',
                    containLabel: true
                },
                legend: {
                    data: ['客流', '客单价'],
                    x:'right',
                    top: '10'
                },
                xAxis: [
                {
                    type: 'category',
                    data: data1,
                    axisPointer: {
                        type: 'shadow'
                    }
                }
                ],
                yAxis: [
                {
                    type: 'value',
                    //name: '金额',
                    axisLabel: {
                        formatter: '{value}元'
                    }
                },
                {
                    type: 'value',
                   // name: '客流',
                    axisLabel: {
                        formatter: '{value}人'
                    },
                    splitLine: {
                        show: false
                    }
                }
                ],
                series: [
                    {
                        name: '客流',
                        type: 'line',
                        symbolSize: 2,
                        yAxisIndex: 1,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(123, 147, 224, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        smooth: true,
                        data: data2,
                    },
                    {
                        name: '客单价',
                        type: 'line',
                        smooth: true,
                        symbolSize: 2,
                        itemStyle: {
                            normal: {
                                areaStyle: {
                                    color: "rgba(89, 196, 230, 0.2)",
                                    type: "default"
                                },
                                lineStyle: {
                                    width: 1
                                }
                            }
                        },
                        data: data3,
                    }
                ]
            };
			ECharts.Draw(option, "chart3");
		},
		createChart4 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '●销售类别分布',
                    x: 'left',
                    top: '10',
                    left: '10'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [
                {
                    name: '销售金额',
                    type: 'pie',
                    radius: '50%',
                    center: ['50%', '60%'],
                    avoidLabelOverlap: true,
                    data: data,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                formatter: '{b} :\n ￥{c}',
                                textStyle: {
                                    color: '#333'
                                }
                            },
                            labelLine: { show: true }
                        }
                    }
                }
                ]
            };
			ECharts.Draw(option, "chart4");
		},
		createChart5 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '●销售客单分布',
                    x: 'left',
                    top: '10',
                    left:'10'
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [
                {
                    name: '销售金额',
                    type: 'pie',
                    radius: '50%',
                    center: ['50%', '60%'],
                    avoidLabelOverlap: true,
                    data: data,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                formatter: '{b}:\n{c}',
                                textStyle: {
                                    color: '#333'
                                }
                            },
                            labelLine: { show: true }
                        }
                    }
                }
                ]
            };
			ECharts.Draw(option, "chart5");
		},
		GetGrid0: function (data) {
            var $gridTable = $("#gridPanel0");
            $gridTable.jqGridEx({
                datatype: "local",
                data:data,
                rowNum: 100,
                loadonce: true,
                colModel: [
                    { name: "item_no", label: "item_no", hidden: true },
                    { name: "条码", label: "条码", align: "center", sortable: false },
                    //{ name: "货号", label: "货号", align: "center", sortable: false },
                    { name: "商品名称", label: "商品名称", width: '300', align: "center", sortable: false, sorttype: 'float' },
                    { name: "库存数量", label: "库存数量", align: "center", sortable: true, sorttype: 'float' },
                    { name: "销售数量", label: "销售数量", align: "center", sortable: true, sorttype: 'float' },
                    { name: "销售金额", label: "销售金额", align: "center", sortable: true, sorttype: 'float' }
                ],
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                height: 225,
                width: $('.gridCard').width() - 10
            });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        },
		GetGrid1: function (data) {
            var $gridTable = $("#gridPanel1");
            $gridTable.jqGridEx({
            	datatype: "local",
                data:data,
                rowNum: 100,
                loadonce: true,
                colModel: [
                     { name: "item_no", label: "item_no", hidden: true },
                    { name: "条码", label: "条码", align: "center", sortable: false },
                    //{ name: "货号", label: "货号", width: '200', align: "center", sortable: false },
                    { name: "商品名称", label: "商品名称", width: '500', align: "center", sortable: false, sorttype: 'float' },
                    { name: "库存数量", label: "库存数量", align: "center", sortable: true, sorttype: 'float' },
                    { name: "库存金额", label: "库存金额", align: "center", sortable: true, sorttype: 'float' },
                    { name: "最后销售日期", label: "最后销售日期", align: "center", sortable: true, sorttype: 'date' },
                    { name: "未动销天数", label: "未动销天数", align: "center", sortable: true, sorttype: 'float' }
                ],
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                height: 225,
                width: $('.gridCard').width() - 10
            });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        },
        GetGrid2: function (data) {
            var $gridTable = $("#gridPanel2");
            $gridTable.jqGridEx({
            	datatype: "local",
                data:data,
                rowNum: 100,
                loadonce: true,
                height: document.body.scrollHeight * 0.5 * 0.8,
                colModel: [
                     { name: "item_no", label: "item_no", hidden: true },
                    { name: "条码", label: "条码", align: "center", sortable: false },
                    //{ name: "货号", label: "货号", align: "center", sortable: false },
                    { name: "商品名称", label: "商品名称", width: '300', align: "center", sortable: false, sorttype: 'float' },
                    { name: "库存数量", label: "库存数量", align: "center", sortable: true, sorttype: 'float' },
                    { name: "库存金额", label: "库存金额", align: "center", sortable: true, sorttype: 'float' },
                    { name: "零售单价", label: "零售单价", align: "center", sortable: true, sorttype: 'float' }
                ],
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                height: 225,
                width: $('.gridCard').width() - 10
            });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        },
        GetGrid3: function (data) {
            var $gridTable = $("#gridPanel3");
            $gridTable.jqGridEx({
            	datatype: "local",
                data:data,
                rowNum: 100,
                loadonce: true,
                height: document.body.scrollHeight * 0.5 * 0.8,
                colModel: [
                     { name: "item_no", label: "item_no", hidden: true },
                    { name: "条码", label: "条码", align: "center", sortable: false },
                    //{ name: "货号", label: "货号", align: "center", sortable: false },
                    { name: "商品名称", label: "商品名称", width: '300', align: "center", sortable: false, sorttype: 'float' },
                    { name: "月销数量", label: "月销数量", align: "center", sortable: true, sorttype: 'float' },
                    { name: "日均销售", label: "日均销售", align: "center", sortable: true, sorttype: 'float' },
                    { name: "月销金额", label: "月销金额", align: "center", sortable: true, sorttype: 'float' },
                    { name: "门店库存", label: "门店库存", align: "center", sortable: true, sorttype: 'float' },
                    { name: "缺货数量", label: "缺货数量", align: "center", sortable: true, sorttype: 'float' },
                    { name: "总部库存", label: "总部库存", align: "center", sortable: true, sorttype: 'float' }
                ],
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                height: 225,
                width: $('.gridCard').width() - 10
            });
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        }
	}
});