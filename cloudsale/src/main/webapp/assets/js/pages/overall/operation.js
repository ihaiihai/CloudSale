var pageVm = new Vue({
	el : '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data : {
		areaCode: null,
		areaOptions:top.branchOptionData,
		date: moment().startOf('month').format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
		chartData:[
			{
				"门店编号":"000201",
				"门店名称":"测试02店",
				"标题":"销售总额: ￥165676 | 面积: 50㎡ | 员工数量: 3人",
				"人均日销":1781.461935,
				"平米日均":106.88771612000001,
				"日均销售":5344.385806,
				"日均客流":29,
				"会员占比":1.000000,
				"会员日均贡献":258.464836,
				"平均折扣":0.625856,
				"退货率":-0.534322,
				"客单价":183.269867,
				"连带率":3.580752,
				"人均日销_目标":1800,
				"平米日均_目标":400,
				"日均销售_目标":8000,
				"日均客流_目标":50,
				"会员占比_目标":1,
				"会员日均贡献_目标":600,
				"平均折扣_目标":0.8,
				"退货率_目标":-0.03,
				"客单价_目标":202,
				"连带率_目标":6,
				"评估得分":62.782778065253211,
				"人均日销_得分":9.89701075,
				"平米日均_得分":2.672192903,
				"日均销售_得分":6.6804822575,
				"日均客流_得分":5.8,
				"会员占比_得分":10,
				"会员日均贡献_得分":4.3077472666666665,
				"平均折扣_得分":7.823199999999999,
				"退货率_得分":0.56145919501723673,
				"客单价_得分":9.0727656930693072,
				"连带率_得分":5.9679199999999994
			}
		]
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.loadData();
				me.setUpModal = top.addModal($('#setUpModal')[0]);
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
		},
        search:function () {
            this.loadData();
        },
		loadData: function(){
			var me = this;
			var branchNos = me.branchNos ? me.branchNos : '';
			var begin = me.date ? me.date.split(' - ')[0] : '';
			var end = me.date ? me.date.split(' - ')[1] : '';
			window.showLoading();
			$.get(baseurl + "/overall/operation/queryFollowUp?branchNos="+branchNos+"&begin="+begin+'&end='+end,function(data){
				window.hideLoading();
				//data = me.chartData;
				setTimeout(function(){
					for(var i=0,len=data.length; i<len; i++){
						me.createChart(data[i], i);
					}
				});
			});
		},
		createChart : function(data, index) {
			var me = this;
			var dataValue = [];
            var maxJsonArr = [];
            var colJson = [];
            for (var obj in data) {
            	if (obj.indexOf("门店名称") >= 0) {
            		
            	} else if (obj.indexOf("标题") >= 0) {
            		
            	} else if (obj.indexOf("目标") >= 0) {
                    colJson.push({ "指标名称": obj.split("_")[0], "目标值": data[obj], "实际值": data[obj.split("_")[0]], "得分":fixNum(data[obj.split("_")[0]+"_得分"],2)})
                    if (obj.indexOf("退货率") >= 0) {
                        maxJsonArr.push({ name: obj.split("_")[0], max: 0,min:-0.2 })
                    } else {
                        maxJsonArr.push({ name: obj.split("_")[0], max: fixNum(data[obj],0), min: 0 })
                    }
                } else if (obj.indexOf("门店编号") == -1 && obj.indexOf("评估得分") == -1 && obj.indexOf("_得分") == -1) {
                		dataValue.push(data[obj])
                }
            }
            var option = {
                title: {
                    text: data['门店名称'],
                    subtext: data['标题'],
                    x: 'left'
                },
                tooltip: { confine: true },
                radar: {
                	indicator : maxJsonArr,
                    splitLine: {
                        show: true,
                        lineStyle: {
                            width: 1,
                            type: "dotted",
                            color: "#d3d3d3"
                        }
                    },
                    axisLine: {
                        lineStyle: {
                            type: "dotted",
                            color: "#d3d3d3"
                        }
                    },
                    name: {
                        textStyle: {
                            color: '#000'
                        }
                    }, center: ["50%", "60%"],
                    radius: "60%"
                },
                series: [{
                    type: 'radar',
                    itemStyle: {
                        normal: {
                            areaStyle: {
                                color: "rgba(123, 147, 224, 0.2)",
                                type: "default"
                            },
                            lineStyle: {
                                width: 1
                            }
                        }
                    },
                    data: [
                         {
                             value: dataValue,
                             name: '各项指标值'
                         }
                    ]
                }]
            };
			ECharts.Draw(option, $('.chartjs-chart').eq(index)[0]);
			var html = this.value_ABC(data.评估得分, 85, 75, 60);
			$('.chartjs-chart').eq(index).append(html);
		},
		value_ABC:function (keyValue, A, B, C) {
            var value = fixNum(keyValue,2)
            var resultHtml = ''
            if (value > A) {
                resultHtml = ('<span class="pull-right badge badge-info" style="position:absolute;top:10px;right:20px;">评估得分:' + value + '</span>')
            } else if (value > B) {
                resultHtml = ('<span class="pull-right badge badge-success" style="position:absolute;top:10px;right:20px;">评估得分:' + value + '</span>')
            } else if (value > C) {
                resultHtml = ('<span class="pull-right badge badge-warning" style="position:absolute;top:10px;right:20px;">评估得分:' + value + '</span>')
            } else {
                resultHtml = ('<span class="pull-right badge badge-danger" style="position:absolute;top:10px;right:20px;">评估得分:' + value + '</span>')
            }
            return resultHtml
        },
        setup: function(){
			top.showModal('setUpModal');
			top.setUpModalVm.clear();
		}
	}
});
top.setUpModalVm = new Vue({
	el: '#setUpModal',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data: {
		formData : {
			branch_no:null
		},
		areaOptions:top.branchOptionData
	},
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.initEvent();
			});
		},
		initEvent: function(){
			var me = this;
		},
		save : function(){
			var me = this;
			if(!this.valid()){
				return;
			}
            top.hideModal('setUpModal');
            window.showLoading();
			$.ajax({
			   type: "POST",
			   contentType:"application/json",
			   url: baseurl + '/overall/operation/createOperation',
			   data: JSON.stringify(me.formData),
			   dataType: 'json',
			   success: function(res){
				   window.hideLoading();
				   console.log(pageVm)
				   pageVm.loadData();
			   },
			   error: function(){
				   window.hideLoading();
			   }
			});
		},
		valid: function(){
			if(!this.formData.branch_no){
				dialogMsg("请选择门店", 0);
				return false;
			}
			return true;
		},
		clear: function(){
			this.formData = {
				branch_no:null
			};
		}
	}
});