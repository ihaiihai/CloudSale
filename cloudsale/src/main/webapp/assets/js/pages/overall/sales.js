var pageVm = new Vue({
    el: '#page',
    components: {
        Treeselect: VueTreeselect.Treeselect
    },
    data: {
        key: "",
        date: moment().startOf('month').format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
        grid: null,
        gridData: [],
        areaCode: null,
        areaOptions:top.branchOptionData,
        gridType: 0,
        initJson: [
            { "id": "branch_no", "name": "门店编号~门店名称~本期_销售~本期_占比~本期_销量~本期_利润~本期_毛利率~环比_销售~环比_占比~环比_销量~环比_利润~环比_毛利率~环比_增长率~同比_销售~同比_占比~同比_销量~同比_利润~同比_毛利率~同比_增长率", "dic": public_data.Dic_branch ,"tableData":[],"isNew":0},
            { "id": "datepart(mm,oper_date)", "name": "月份~本期_销售~本期_占比~本期_销量~本期_利润~本期_毛利率~环比_销售~环比_占比~环比_销量~环比_利润~环比_毛利率~环比_增长率~同比_销售~同比_占比~同比_销量~同比_利润~同比_毛利率~同比_增长率", "dic": 0, "tableData": [], "isNew": 0 },
            { "id": "convert(varchar(10),oper_date,120)", "name": "日期~本期_销售~本期_占比~本期_销量~本期_利润~本期_毛利率~环比_销售~环比_占比~环比_销量~环比_利润~环比_毛利率~环比_增长率~同比_销售~同比_占比~同比_销量~同比_利润~同比_毛利率~同比_增长率", "dic": 0, "tableData": [], "isNew": 0 },
            { "id": "cls", "name": "类别编号~类别名称~本期_销售~本期_占比~本期_销量~本期_利润~本期_毛利率~环比_销售~环比_占比~环比_销量~环比_利润~环比_毛利率~环比_增长率~同比_销售~同比_占比~同比_销量~同比_利润~同比_毛利率~同比_增长率", "dic": public_data.Dic_cls_all, "tableData": [], "isNew": 0 },
            { "id": "item_brand", "name": "品牌编号~品牌名称~本期_销售~本期_占比~本期_销量~本期_利润~本期_毛利率~环比_销售~环比_占比~环比_销量~环比_利润~环比_毛利率~环比_增长率~同比_销售~同比_占比~同比_销量~同比_利润~同比_毛利率~同比_增长率", "dic": public_data.Dic_brand, "tableData": [], "isNew": 0 },
            { "id": "sale_man", "name": "营业员编号~营业员名称~本期_销售~本期_占比~本期_销量~本期_利润~本期_毛利率~环比_销售~环比_占比~环比_销量~环比_利润~环比_毛利率~环比_增长率~同比_销售~同比_占比~同比_销量~同比_利润~同比_毛利率~同比_增长率", "dic": public_data.Dic_sale_man, "tableData": [], "isNew": 0 }
        ]
    },
    watch:{
        'gridType': function(){
            $('#gridTable').GridUnload();
            this.loadGrid('#gridTable', {date:this.date, branch_no:this.areaCode, gridType:this.gridType});
        }
    },
    mounted: function(){
        this.init();
    },
    methods: {
        init: function(){
            var me = this;
            setTimeout(function(){
                me.grid = $('#gridTable');
                me.initEvent();
                me.loadGrid('#gridTable', {date:me.date, branch_no:me.areaCode, gridType:me.gridType});
            });
        },
        initEvent: function(){
            var me = this;
            $('#page').find('.daterange').dateRange();
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    $("#gridTable").setGridHeight($(window).height() - $('.form-inline').height() - ($('.gridCard').width() > 1300 ? 225 : 245));
                    $("#gridTable").setGridWidth($('.gridCard').width() - 10);
                }, 200);
                e.stopPropagation();
            });
        },
        loadGrid: function(tableId, params, callback){
            var me = this;
            var begin = params.date ? params.date.split(' - ')[0] : '';
            var end = params.date ? params.date.split(' - ')[1] : '';
            var branchNos = params.branch_no ? params.branch_no : '';
            var method = ['byBranch','byMonth','byDay','byCls','byBrand','bySaler'];
            $(tableId).GridUnload();
            window.showLoading();
            $.get(baseurl + "/Report_PC/Xiaoshou_Fenxi/"+method[params.gridType]+"?branchNos="+branchNos+"&begin="+begin+'&end='+end+'&pageNo=1'+'&pageSize=1000',function(data){
            		window.hideLoading();
            		me.GetGrid(tableId, data);
                me.afterInit(tableId, data);
                if(callback){
                    callback($(tableId));
                }
            });
        },
        GetGrid: function(tableId, jsonArr) {
            var me = this;
            var colModel = [];
            var sortname = '';
            var sumCol = [];
            var sumName = '';
            var avgCol = [];
            var myArr = this.initJson[this.gridType]["name"].split("~");
            for (var i = 0; i < myArr.length; i++) {
                var json = myArr[i];
                if(json.indexOf('编号') > 0 || json.indexOf('名称') > 0 || json == '月份' || json == '日期'){
                    colModel.push({ name: json, label: json, align: "center", sortable: true, sorttype: 'float', width: 120, frozen: true })
                    sortname = json;
                    sumName = json + '_合计';
                } else {
                    colModel.push({ name: json, label: json.split("_")[1], align: "center", sortable: true, sorttype: 'float', width: 80 })
                }
                if(json == '门店名称'){
                    $.extend(colModel[i], { formatter: top.InitData.getBranchName })
                }else if(json == '类别名称'){
                    $.extend(colModel[i], { formatter: top.InitData.getClsName })
                }else if(json == '品牌名称'){
                    $.extend(colModel[i], { formatter: top.InitData.getBrandName })
                }else if(json == '营业员名称'){
                    $.extend(colModel[i], { formatter: top.InitData.getSalerName })
                }else if (json.indexOf("增长率") >= 0) {
                    $.extend(colModel[i], { width: 180, formatter: jqFormatter.Percent })
                } else if (json.indexOf("销售") >= 0 || json.indexOf("利润") >= 0) {
                    $.extend(colModel[i], jqFormatter.RMB0)
                    sumCol.push(json);
                } else if (json.indexOf("销量") >= 0) {
                    $.extend(colModel[i], jqFormatter.Num0)
                    sumCol.push(json);
                } else if (json.indexOf("率") >= 0 || json.indexOf("占比") >= 0) {
                    $.extend(colModel[i], { formatter: jqFormatter.Percent })
                }
                if (top.CanSeeProfit == 0 && (json.indexOf("利") >= 0 || json.indexOf("成本") >= 0)) {
                    $.extend(colModel[i], { hidden: true })
                }
            }
            $(tableId).jqGridEx({
                datatype: "local",
                data: jsonArr,//数据数组
                height: $(window).height() - $('.form-inline').height() - ($('.gridCard').width() > 1300 ? 225 : 245),
                width: $('.gridCard').width() - 10,
                colModel: colModel,
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rowNum: 100,
                shrinkToFit: false,
                loadonce: true,  //缓存排序后才可翻页
                footerrow: true,
                pager: "#pager_list",
                pagerpos: "right",
                rowList: [100, 200, 500, 1000],
                sortname: sortname,
                sumColArr: sumCol,
                sumName: sumName,
                avgColArr: avgCol,
                Complete: this.Complete,
                onCellSelect: function (rowid, iCol, contents, event) {
                    var value = $(this).getCell(rowid, 1);
                    var branch_no = me.gridType == 0 ? value : (me.areaCode?me.areaCode:'');
                    var clsN = me.gridType ==3 ? value : '';
                    var brandN = me.gridType ==4 ? value : '';
                    var saleManN = me.gridType == 5 ? value : '';
                    var date = me.gridType == 2 ? (value + ' - ' + value) : me.date;
                    var params = {branch_no:branch_no, clsN:clsN, brandN:brandN, saleManN:saleManN, date:date, gridType:me.gridType};
                    if (iCol <= 2) {
                        //	me.showGrid1(params);
                    } else{
                        //me.showGrid2(params);
                    }
                }
            });
        },
        Complete: function(gridID, sumColArr) {
            var _this = $("#" + gridID);
            sumColArr.本期_毛利率 = sumColArr.本期_利润 / sumColArr.本期_销售
            sumColArr.环比_毛利率 = sumColArr.环比_利润 / sumColArr.环比_销售
            sumColArr.环比_增长率 = (sumColArr.本期_销售 - sumColArr.环比_销售) / sumColArr.环比_销售
            sumColArr.同比_毛利率 = sumColArr.同比_利润 / sumColArr.同比_销售
            sumColArr.同比_增长率 = (sumColArr.本期_销售 - sumColArr.同比_销售) / sumColArr.同比_销售
            sumColArr.本期_占比 = 1
            sumColArr.环比_占比 =1
            sumColArr.同比_占比 =1
            _this.footerData("set", sumColArr);
            condition.backColor_ABC(gridID, "毛利率", 30, 20, 10);
            condition.arrow_init(gridID, "增长率")
            _this.find("[aria-describedby*='名称']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='名称']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='月份']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='月份']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='日期']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='日期']").css("border-right", "1px dashed red")
            _this.find("[aria-describedby*='增长率']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='增长率']").css("border-right", "1px dashed red")
            if (top.CanSeeProfit == 0) {
                _this.find("[aria-describedby*='本期_销量']").css("border-right", "1px dashed red")
                $(".ui-jqgrid-hdiv").find("th[id*='本期_销量']").css("border-right", "1px dashed red")
            }
            _this.find("[aria-describedby*='本期_毛利率']").css("border-right", "1px dashed red")
            $(".ui-jqgrid-hdiv").find("th[id*='本期_毛利率']").css("border-right", "1px dashed red")
        },
        afterInit: function(tableId, jsonArr) {
            var groupHeaders = [];
            for (var obj in jsonArr[0]) {
                if (obj.indexOf("本期_销售") >= 0) {
                    groupHeaders.push({ startColumnName: obj, numberOfColumns: 5, titleText: "【本期时间段】销售概况" })
                } else if (obj.indexOf("环比_销售") >= 0) {
                    groupHeaders.push({ startColumnName: obj, numberOfColumns: 6, titleText: "【环比时间段（所选开始及结束时间各往前推一个月）】销售概况" })
                } else if (obj.indexOf("同比_销售") >= 0) {
                    groupHeaders.push({ startColumnName: obj, numberOfColumns: 6, titleText: "【同比时间段（所选开始及结束时间各往前推一年）】销售概况" })
                }
            }
            $(tableId).jqGrid('setGroupHeaders', {
                useColSpanStyle: true,
                groupHeaders: groupHeaders
            });
            $(tableId).jqGrid('setFrozenColumns');
        },
        search: function(){
            var me = this;
            me.loadGrid('#gridTable', {date:me.date, branch_no:me.areaCode, gridType:me.gridType});
        },
        refresh: function(){
            window.location.reload();
        },
        download: function(){
            var me = this;
            var begin = me.date ? me.date.split(' - ')[0] : '';
            var end = me.date ? me.date.split(' - ')[1] : '';
            var branchNos = me.areaCode ? me.areaCode : '';
            var method = ['byBranch','byMonth','byDay','byCls','byBrand','bySaler'];
            window.location.href = baseurl + "/Report_PC/dataExport/excel"+"?branchNos="+branchNos+"&begin="+begin+'&end='+end+'&pageNo=1'+'&pageSize=9999'+"&busi=xiaoshouFenxi"+"&type="+method[me.gridType];
        },
        print: function(){
            $("#printTable").printTable();
        },
        reload: function(){
            var me = this;
            me.key = '';
            me.grid.jqGrid('setGridParam', {
                datatype: 'json',
                postData: {
                    key: me.key
                },
                page: 1
            }).trigger('reloadGrid').jqGrid('resetSelection');
        },
        showGrid1: function(params){
            top.gridModal.modalTitle = '销售分析报告';
            top.gridModal.show();
            this.loadGrid(top.document.getElementById('gridModalTable'), {date:params.date, branch_no:params.branch_no, gridType:params.gridType},function($grid){
                $grid.setGridHeight($grid.parents('.table-responsive').height()-120);
                $grid.setGridWidth($grid.parents('.table-responsive').width());
            });
        },
        showGrid2: function(params){

        }
    }
});