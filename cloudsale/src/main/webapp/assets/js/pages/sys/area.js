var pageVm = new Vue({
	el: '#page',
	data: {
		key: "",
		grid: null
	},
	mounted: function(){
		this.init();
	},
	methods: {
		init: function(){
			var me = this;
			setTimeout(function(){
				me.grid = $('#gridTable');
				me.initEvent();
				me.loadData();
			});
		},
		initEvent: function(){
			$(window).resize(function (e) {
	            window.setTimeout(function () {
	                $("#gridTable").setGridHeight($(window).height() - 135);
	                $("#gridTable").setGridWidth($(window).width());
	            }, 200);
	            e.stopPropagation();
	        });
		},
		loadData: function(){
			var me = this;
			me.getNewData(function(data){
				me.GetGrid(data);
			});
		},
		getNewData: function(callback) {
			var me = this;
			window.showLoading();
        	$.ajax({
                url: baseurl + "/sys/area/queryByCondition?key="+(me.key?me.key:""),
                type: "get",
                dataType: "json",
                success: function (data) {
                	window.hideLoading();
                    if(callback){
                		callback(data);
                    }
                },
                error: function(){
            		window.hideLoading();
                }
			});
        },
        GetGrid: function(data){
			var me = this;
			console.log(data)
			$("#gridTable").jqGrid({
				datatype : 'jsonstring',
				datastr : data,
				treedatatype : "local",
	            height: $(window).height() - 135,
	            autowidth: true,
	            colModel: [
	            		{ label: '节点层级', name: 'level', hidden: true },
	            		{ label: '区域名称', name: 'areaName', index: 'areaName', sortable: false,sorttype: 'float', width: 300, align:"left" },
	            		{ label: '区域编号', name: 'areaCode', index: 'areaCode', sortable: false,sorttype: 'float', width: 200, align:"center" },
	                { label: '门店编号', name: 'branchCode', index: 'branchCode', sortable: false, sorttype: 'float', width: 150, align: 'center' }
	            ],
	            treeGrid: true,
	            treeGridModel: "adjacency",
	            ExpandColumn: "areaName",
	            treeReader:{
					parent_id_field: "parentAreaCode"
				},
				rowNum: "all",
				hoverrows:false,
				viewrecords:false,
				gridview:true
	        });
		},
		search: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.loadData();
		},
		refresh: function(){
			window.location.reload();
		},
		reload: function(){
			var me = this;
			$("#gridTable").GridUnload();
			me.loadData();
		}
	}
});