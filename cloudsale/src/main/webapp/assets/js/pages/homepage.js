var pageVM = new Vue({
	el : '#page',
	data : {
		data1 : null,
		data2 : null,
		data3: null,
		data4: null,
		chart1Data: null,
		chart2Data: null,
		chart3Data: null
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.loadData();
			});
		},
		initEvent: function(){
			var me = this;
			
		},
		getPercent: function(yesValue, beforeYesValue){
			if(beforeYesValue != 0){
				return ((yesValue - beforeYesValue) / beforeYesValue).toFixed(2) * 100 + '%';
			}else{
				return yesValue;
			}
		},
		loadData: function(){
			this.getData4();
			this.getChart1Data();
			this.getYesterdayGain();
			this.getChart2Data();
			this.getChart3Data();
			this.getTargetData();
			this.getRealtimeData();
		},
		getYesterdayGain : function() {
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/yesterday_gain",function(data){
				me.data1 = data;
			});
		},
		getChart1Data: function(){
			var me = this;
			me.chart1Data = null;
			$.get(baseurl + "/Report_PC/Home_page/month_gain",function(data){
				me.chart1Data = data;
				setTimeout(function(){
					me.createChart1(data);
				});
			});
		},
		getData4: function(){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/month_gain_percent",function(data){
				me.data4 = data;
			});
		},
		getChart2Data: function(){
			var me = this;
			me.chart2Data = null;
			$.get(baseurl + "/Report_PC/Home_page/yesterday_t_z_d_l",function(data){
				me.chart2Data = data;
				setTimeout(function(){
					me.createChart2(data);
				});
			});
		},
		getChart3Data: function(){
			var me = this;
			me.chart3Data = null;
			$.get(baseurl + "/Report_PC/Home_page/today_branch_sale",function(data){
				me.chart3Data = data;
				setTimeout(function(){
					me.createChart3(data);
				});
			});
		},
		getRealtimeData: function(){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/today_realtime_index",function(data){
				me.data3 = data;
			});
		},
		getTargetData: function(){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/today_lowest_target",function(data){
				me.data2 = data;
			});
		},
		getDayBranchSaleData: function(params, callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/someday_branch_sale?operDate=" + params.operDate,function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getDayBranchClsSaleData: function(params, callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/someday_branch_cls_sale?branchNo=" + params.branchNo,function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getYestdayBranchSale: function(callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/yestday_branch_sale",function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getLast30_branch_bill_cnt: function(callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/last30_branch_bill_cnt",function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getLast30_branch_new_client: function(callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/last30_branch_new_client",function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getLast30_branch_new_vip: function(callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/last30_branch_new_vip",function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getYesterdayRetDetail: function(callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/yesterday_ret_detail",function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getYesterday_dis_detail: function(callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/yesterday_dis_detail",function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getYesterday_client_amt_detail: function(callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/yesterday_client_amt_detail",function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getYesterday_liandailv_detail: function(callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/yesterday_liandailv_detail",function(data){
				if(callback){
					callback(data);
				}
			});
		},
		getToday_realtime_aim: function(callback){
			var me = this;
			$.get(baseurl + "/Report_PC/Home_page/today_realtime_aim",function(data){
				if(callback){
					callback(data);
				}
			});
		},
		createChart1 : function(data) {
			var me = this;
			var xData = [],y1Data = [], y2Data = [], y3Data = [];
			$.each(data,function(i,item){
				xData.push(item['日期']);
				y1Data.push(item['本期销售']);
				y2Data.push(item['同期销售']);
				y3Data.push(item['累计']);
			});
			var option = {
				tooltip : {
					trigger : 'axis',
					axisPointer : {
						type : 'cross',
						crossStyle : {
							color : '#999'
						}
					}
				},
				grid : {
					left : '1%',
					right : '1%',
					bottom : '1%',
					top : '50',
					containLabel : true
				},
				legend : {
					data : [ '本期销售', '同期销售', '累计完成' ],
					top : '2',
					x : "right"
				},
				xAxis : [ {
					type : 'category',
					data : xData,
					axisPointer : {
						type : 'shadow'
					}
				} ],
				yAxis : [ {
					type : 'value',
					//name: '每日完成',
					axisLabel : {
						formatter : '{value}'
					}
				}, {
					type : 'value',
					//name: '累计完成',
					axisLabel : {
						formatter : '{value}'
					},
					splitLine : {
						show : false
					}
				} ],
				series : [ {
					name : '本期销售',
					type : 'bar',
					smooth : true,
					data : y1Data
				}, {
					name : '同期销售',
					type : 'bar',
					smooth : true,
					data : y2Data
				}, {
					name : '累计完成',
					type : 'line',
					smooth : 'true',
					symbolSize : 2,
					itemStyle : {
						normal : {
							areaStyle : {
								color : "rgba(237, 175, 218, 0.2)",
								type : "default"
							},
							lineStyle : {
								width : 1
							}
						}
					},
					yAxisIndex : 1,
					data : y3Data
				} ]
			};
			ECharts.Draw(option, "chart1", function(params){
                top.chartModal.show('【'+params.name+'】各门店销售情况');
                me.getDayBranchSaleData({operDate:params.name},function(data){
                	top.chartModal.showLoading = false;
                		if(data && data.length > 0){
                			top.chartModal.noData = false;
            				setTimeout(function(){
            					me.createChart4(data);
            				},200);
                		}else{
                			top.chartModal.noData = true;
                		}
                });
			});
		},
		createChart2 : function(data) {
			var me = this;
			var option = {
				tooltip : {
					trigger : 'item',
					formatter : "{a} <br/>{b} : {c} ({d}%)"
				},
				series : [
						{
							name : "昨日退货率",
							type : 'pie',
							radius : [ '65%', '85%' ],
							center : [ '12.5%', '50%' ],
							label : {
								normal : {
									position : 'center'
								}
							},
							data : [ {
								value : data[0].昨日退货率,
								name : "昨日退货率",
								label : {
									normal : {
										formatter : '{d} %',
										textStyle : {
											fontSize : 26
										}
									}
								}
							}, {
								value : 100 - (data[0].昨日退货率),
								name : '占比',
								label : {
									normal : {
										formatter : '\n昨日退货率',
										textStyle : {
											color : '#555',
											fontSize : 16
										}
									}
								},
								tooltip : {
									show : false
								},
								itemStyle : {
									normal : {
										color : '#dedede'
									},
									emphasis : {
										color : '#dedede'
									}
								},
								hoverAnimation : false
							} ]
						},
						{
							name : "昨日折扣率",
							type : 'pie',
							radius : [ '65%', '85%' ],
							center : [ '37.5%', '50%' ],
							label : {
								normal : {
									position : 'center'
								}
							},
							data : [ {
								value : data[0].昨日折扣率,
								name : "昨日折扣率",
								label : {
									normal : {
										formatter : '{d} %',
										textStyle : {
											fontSize : 26
										}
									}
								}
							}, {
								value : 100 - (data[0].昨日折扣率),
								name : '占比',
								label : {
									normal : {
										formatter : '\n昨日折扣率',
										textStyle : {
											color : '#555',
											fontSize : 16
										}
									}
								},
								tooltip : {
									show : false
								},
								itemStyle : {
									normal : {
										color : '#dedede'
									},
									emphasis : {
										color : '#dedede'
									}
								},
								hoverAnimation : false
							} ]
						},
						{
							name : "昨日客单价",
							type : 'pie',
							radius : [ '65%', '85%' ],
							center : [ '62.5%', '50%' ],
							label : {
								normal : {
									position : 'center'
								}
							},
							data : [
									{
										value : data[0].昨日客单价,
										name : "昨日客单价",
										label : {
											normal : {
												formatter : '{c}',
												textStyle : {
													fontSize : 26
												}
											}
										}
									},
									{
										value : (data[0].昨日客单价 < 500 ? 500
												: data[0].昨日客单价 * 1.2)
												- (data[0].昨日客单价),
										name : '占比',
										label : {
											normal : {
												formatter : '\n昨日客单价',
												textStyle : {
													color : '#555',
													fontSize : 16
												}
											}
										},
										tooltip : {
											show : false
										},
										itemStyle : {
											normal : {
												color : '#dedede'
											},
											emphasis : {
												color : '#dedede'
											}
										},
										hoverAnimation : false
									} ]
						},
						{
							name : "昨日连带率",
							type : 'pie',
							radius : [ '65%', '85%' ],
							center : [ '87.5%', '50%' ],
							label : {
								normal : {
									position : 'center'
								}
							},
							data : [
									{
										value : data[0].昨日连带率,
										name : "昨日连带率",
										label : {
											normal : {
												formatter : '{c}',
												textStyle : {
													fontSize : 26
												}
											}
										}
									},
									{
										value : (data[0].昨日连带率 < 10 ? 10
												: data[0].昨日连带率 * 1.1)
												- (data[0].昨日连带率),
										name : '占比',
										label : {
											normal : {
												formatter : '\n昨日连带率',
												textStyle : {
													color : '#555',
													fontSize : 16
												}
											}
										},
										tooltip : {
											show : false
										},
										itemStyle : {
											normal : {
												color : '#dedede'
											},
											emphasis : {
												color : '#dedede'
											}
										},
										hoverAnimation : false
									} ]
						} ]
			};
			ECharts.Draw(option, "chart2", function(params){
				if(params.seriesIndex == 0){
					top.chartModal.show('昨日退货率详情');
	                me.getYesterdayRetDetail(function(data){
		                	top.chartModal.showLoading = false;
		                	if(data && data.length > 0){
		                		top.chartModal.noData = false;
	            				setTimeout(function(){
	            					me.createChart9(data);
	            				},200);
		                	}else{
		                		top.chartModal.noData = true;
		                	}
	                });
				}else if(params.seriesIndex == 1){
					top.chartModal.show('昨日折扣率详情');
	                me.getYesterday_dis_detail(function(data){
	                		top.chartModal.showLoading = false;
	                		if(data && data.length > 0){
	                			top.chartModal.noData = false;
	            				setTimeout(function(){
	            					me.createChart10(data);
	            				},200);
		                	}else{
		                		top.chartModal.noData = true;
		                	}
	                });
				}else if(params.seriesIndex == 2){
					top.chartModal.show('各门店客单价分布');
	                me.getYesterday_client_amt_detail(function(data){
	                		top.chartModal.showLoading = false;
	                		if(data && data.length > 0){
	                			top.chartModal.noData = false;
	            				setTimeout(function(){
	            					me.createChart11(data);
	            				},200);
		                	}else{
		                		top.chartModal.noData = true;
		                	}
	                });
				}else if(params.seriesIndex == 3){
					top.chartModal.show('各门店连带率分布');
	                me.getYesterday_liandailv_detail(function(data){
	                		top.chartModal.showLoading = false;
	                		if(data && data.length > 0){
	                			top.chartModal.noData = false;
	            				setTimeout(function(){
	            					me.createChart12(data);
	            				},200);
		                	}else{
		                		top.chartModal.noData = true;
		                	}
	                });
				}
			});
		},
		createChart3 : function(chartData) {
			var me = this;
			var xData = [];
			var dataAxis = [];
			var data = [];
			var key = "";
			var yMax = 0;
			for(var i=0,len=chartData.length; i<len; i++){
				xData.push(chartData[i].branchNo);
				dataAxis.push(chartData[i].branchName);
				data.push(chartData[i].sale);
				if(chartData[i].sale > yMax){
					yMax = chartData[i].sale;
				}
			}
			var dataShadow = [];
			for (var i = 0; i < data.length; i++) {
			    dataShadow.push(yMax);
			}
			var option = {
				tooltip : {
					trigger : 'axis',
					axisPointer : {
						type : 'cross',
						crossStyle : {
							color : '#999'
						}
					}
				},
				grid: {
			        left: '3%',
			        right: '4%',
			        bottom: '3%',
			        top: '3%',
			        containLabel: true
			    },
			    xAxis: {
			    	type : 'category',
			        data: dataAxis,
			        axisTick: {
		                alignWithLabel: true
		            }
			    },
			    yAxis: {
			        axisLine: {
			            show: false
			        },
			        axisTick: {
			            show: false
			        },
			        axisLabel: {
			            textStyle: {
			                color: '#999'
			            }
			        }
			    },
			    series: [
			        { // For shadow
			            type: 'bar',
			            itemStyle: {
			                normal: {color: 'rgba(0,0,0,0.05)'}
			            },
			            barGap:'-100%',
			            barCategoryGap:'40%',
			            data: dataShadow,
			            animation: false
			        },
			        {
			            type: 'bar',
			            itemStyle: {
			                normal: {
			                    color: new echarts.graphic.LinearGradient(
			                        0, 0, 0, 1,
			                        [
			                            {offset: 0, color: '#727CF5'},
			                            {offset: 0.5, color: '#787AEF'},
			                            {offset: 1, color: '#A76FC5'}
			                        ]
			                    )
			                }
			            },
			            data: data
			        }
			    ]
			};
			ECharts.Draw(option, "chart3", function(params){
                top.chartModal.show('【'+params.name+'】今日销售情况');
                me.getDayBranchClsSaleData({branchNo:xData[params.dataIndex]},function(data){
                		top.chartModal.showLoading = false;
                		if(data && data.length > 0){
	                		top.chartModal.noData = false;
            				setTimeout(function(){
            					me.createChart4(data);
            				},200);
	                	}else{
	                		top.chartModal.noData = true;
	                	}
                });
			});
		},
		createChart4: function(data){
			var data1 = [];
			var key = "";
			$.each(data,function(i,item){
				key = Object.keys(item)[0];
				data1.push({
					name: top.InitData.getClsName(key),
					value: item[key]
				});
			});
			var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{b} : {c} ({d}%)"
                },
                series: [
                       {
                           type: 'pie',
                           radius: '55%',
                           center: ['50%', '50%'],
                           data: data1,
                           itemStyle: {
                               normal: {
                                   label: {
                                       show: true,
                                       formatter: '{b} : ￥{c}',
                                       textStyle: {
                                           color: '#333'
                                       }
                                   },
                                   labelLine: { show: true }
                               }
                           }
                       }
                ]
            };
			ECharts.Draw(option, top.document.getElementById('chartModalCon'));
		},
		showChart5: function(){
            top.chartModal.show('昨日各门店销售完成情况');
            this.getYestdayBranchSale(function(data){
            		top.chartModal.showLoading = false;
            		if(data && data.length > 0){
            			var data1 = [];
	    	            	var data2 = [];
	    	            	var data3 = [];
	    	            	var data4 = [];
	    	            	for(var i=0,len=data.length; i<len; i++){
	    	            		data1.push(top.InitData.getBranchName(data[i]['门店']));
	    	            		data2.push(data[i]['任务']);
	    	            		data3.push(data[i]['完成']);
	    	            		data4.push(data[i]['完成率']);
	    	            	}
	    	            	var option = {
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {
                                type: 'cross',
                                crossStyle: {
                                    color: '#999'
                                }
                            }
                        },
                        grid: {
                            left: '1%',
                            right: '5%',
                            bottom: '1%',
                            containLabel: true
                        },
                        legend: {
                            data: ['任务', '完成', '完成率'],
                            top: '2%'
                        },
                        xAxis: [
                            {
                                type: 'category',
                                data: data1,
                                axisPointer: {
                                    type: 'shadow'
                                }
                            }
                        ],
                        yAxis: [
                            {
                                type: 'value',
                                name: '金额',
                                axisLabel: {
                                    formatter: '{value}元'
                                }
                            },
                            {
                                type: 'value',
                                name: '完成率',
                                axisLabel: {
                                    formatter: '{value}%'
                                }
                            }
                        ],
                        series: [
                            {
                                name: '任务',
                                type: 'bar',
                                data: data2,
                            },
                            {
                                name: '完成',
                                type: 'bar',
                                data: data3,
                            },
                            {
                                name: '完成率',
                                type: 'line',
                                yAxisIndex: 1,
                                data: data4,
                            }
                        ]
                    };
	    	            	top.chartModal.noData = false;
            				setTimeout(function(){
            					ECharts.Draw(option, top.document.getElementById('chartModalCon'));
            				},200);
            		}else{
            			top.chartModal.noData = true;
            		}
            });
		},
		showChart6: function(){
            top.chartModal.show('最近30日内成交单数波动详情');
            this.getLast30_branch_bill_cnt(function(data){
            		top.chartModal.showLoading = false;
            		if(data && data.length > 0){
            			var data1 = [];
	    	            	var data2 = [];
	    	            	var data3 = [];
	    	            	for(var i=0,len=data.length; i<len; i++){
	    	            		data1.push(data[i]['name']);
	    	            		data2.push(data[i]['本期']);
	    	            		data3.push(data[i]['同比']);
	    	            	}
	    	            	var option = {
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {
                                type: 'cross',
                                crossStyle: {
                                    color: '#999'
                                }
                            }
                        },
                        grid: {
                            left: '1%',
                            right: '1%',
                            bottom: '1%',
                            containLabel: true
                        },
                        legend: {
                            data: ['本期', '同比'],
                            top: '2%'
                        },
                        xAxis: [
                            {
                                type: 'category',
                                data: data1,
                                axisPointer: {
                                    type: 'shadow'
                                }
                            }
                        ],
                        yAxis: [
                            {
                                type: 'value',
                                name: '成交单数',
                                axisLabel: {
                                    formatter: '{value}单'
                                }
                            }
                        ],
                        series: [
                            {
                                name: '本期',
                                type: 'line',
                                data: data2,
                                markLine: {
                                    silent:true,
                                    data: [
                                        {
                                            label: {
                                                normal: {
                                                    position: 'middle',
                                                    formatter: '均值：{c}'
                                                }
                                            },
                                            type: 'average', name: '本期平均销售金额', valueIndex: '1',
                                        },
                                    ]
                                }
                            },
                            {
                                name: '同比',
                                type: 'line',
                                data: data3,
                                markLine: {
                                    silent: true,
                                    label: {
                                        normal: {
                                            position: 'middle',
                                            formatter: '均值：{c}'
                                        }
                                    },
                                    data: [
                                        { type: 'average', name: '同比平均销售金额', valueIndex: '1' },
                                    ]
                                }
                            }
                        ]
                    };
	    	            	top.chartModal.noData = false;
            				setTimeout(function(){
            					ECharts.Draw(option, top.document.getElementById('chartModalCon'));
            				},200);
            		}else{
            			top.chartModal.noData = true;
            		}
            });
		},
		showChart7: function(){
            top.chartModal.show('最近30日内昨日新增客户波动详情（新增客户为首次消费的会员）');
            this.getLast30_branch_new_client(function(data){
	            	top.chartModal.showLoading = false;
	            	if(data && data.length > 0 ){
	            		var data1 = [];
		            	var data2 = [];
		            	var data3 = [];
		            	for(var i=0,len=data.length; i<len; i++){
		            		data1.push(data[i]['name']);
		            		data2.push(data[i]['本期']);
		            		data3.push(data[i]['同比']);
		            	}
		            	var option = {
	                    tooltip: {
	                        trigger: 'axis',
	                        axisPointer: {
	                            type: 'cross',
	                            crossStyle: {
	                                color: '#999'
	                            }
	                        }
	                    },
	                    grid: {
	                        left: '1%',
	                        right: '1%',
	                        bottom: '1%',
	                        containLabel: true
	                    },
	                    legend: {
	                        data: ['本期', '同比'],
	                        top: '2%'
	                    },
	                    xAxis: [
	                        {
	                            type: 'category',
	                            data: data1,
	                            axisPointer: {
	                                type: 'shadow'
	                            }
	                        }
	                    ],
	                    yAxis: [
	                        {
	                            type: 'value',
	                            name: '金额',
	                            axisLabel: {
	                                formatter: '{value}人'
	                            }
	                        }
	                    ],
	                    series: [
	                        {
	                            name: '本期',
	                            type: 'line',
	                            data: data2,

	                            markLine: {
	                                silent: true,
	                                label: {
	                                    normal: {
	                                        position: 'middle',
	                                        formatter: '均值：{c}'
	                                    }
	                                },
	                                data: [
	                                    { type: 'average', name: '本期平均销售金额', valueIndex: '1' },
	                                ]
	                            }

	                        },
	                        {
	                            name: '同比',
	                            type: 'line',
	                            data: data3,
	                            markLine: {
	                                silent: true,
	                                label: {
	                                    normal: {
	                                        position: 'middle',
	                                        formatter: '均值：{c}'
	                                    }
	                                },
	                                data: [
	                                    { type: 'average', name: '同比平均销售金额', valueIndex: '1' },
	                                ]
	                            }
	                        }
	                    ]
	                };
		            	top.chartModal.noData = false;
        				setTimeout(function(){
        					ECharts.Draw(option, top.document.getElementById('chartModalCon'));
        				},200);
	            	}else{
	            		top.chartModal.noData = true;
	            	}
            });
		},
		showChart8: function(){
            top.chartModal.show('最近30日内新增会员波动详情(新增会员为新办理会员卡的会员)');
            this.getLast30_branch_new_vip(function(data){
            		top.chartModal.showLoading = false;
            		if(data && data.length > 0 ){
            			var data1 = [];
	    	            	var data2 = [];
	    	            	var data3 = [];
	    	            	for(var i=0,len=data.length; i<len; i++){
	    	            		data1.push(data[i]['name']);
	    	            		data2.push(data[i]['本期']);
	    	            		data3.push(data[i]['同比']);
	    	            	}
	    	            	var option = {
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {
                                type: 'cross',
                                crossStyle: {
                                    color: '#999'
                                }
                            }
                        },
                        grid: {
                            left: '1%',
                            right: '1%',
                            bottom: '1%',
                            containLabel: true
                        },
                        legend: {
                            data: ['本期', '同比'],
                            top: '2%'
                        },
                        xAxis: [
                            {
                                type: 'category',
                                data: data1,
                                axisPointer: {
                                    type: 'shadow'
                                }
                            }
                        ],
                        yAxis: [
                            {
                                type: 'value',
                                name: '人数',
                                axisLabel: {
                                    formatter: '{value}人'
                                }
                            }
                        ],
                        series: [
                            {
                                name: '本期',
                                type: 'line',
                                data: data2,
                                markLine: {
                                    silent: true,
                                    label: {
                                        normal: {
                                            position: 'middle',
                                            formatter: '均值：{c}'
                                        }
                                    },
                                    data: [
                                        { type: 'average', name: '本期平均销售金额', valueIndex: '1' },
                                    ]
                                }
                            },
                            {
                                name: '同比',
                                type: 'line',
                                data: data3,
                                markLine: {
                                    silent: true,
                                    label: {
                                        normal: {
                                            position: 'middle',
                                            formatter: '均值：{c}'
                                        }
                                    },
                                    data: [
                                        { type: 'average', name: '同比平均销售金额', valueIndex: '1' },
                                    ]
                                }
                            }
                        ]
                    };
	    	            	top.chartModal.noData = false;
            				setTimeout(function(){
            					ECharts.Draw(option, top.document.getElementById('chartModalCon'));
            				},200);
            		}else{
            			top.chartModal.noData = true;
            		}
            });
		},
		createChart9: function(data){
			var data1 = [];
	        	var data2 = [];
	        	var data3 = [];
	        	var data4 = [];
	        	for(var i=0,len=data.length; i<len; i++){
	        		data1.push(data[i].name);
	        		data2.push(data[i]['销售']);
	        		data3.push(data[i]['退货']);
	        		data4.push(data[i]['退货率']);
	        	}
			var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '1%',
                    containLabel: true
                },
                legend: {
                    data: ['销售', '退货', '退货率'],
                    top: '2%'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '金额',
                        axisLabel: {
                            formatter: '{value} 元'
                        }
                    },
                    {
                        type: 'value',
                        name: '完成率',
                        axisLabel: {
                            formatter: '{value}%'
                        }
                    }
                ],
                series: [
                    {
                        name: '销售',
                        type: 'bar',
                        data: data2,
                    },
                    {
                        name: '退货',
                        type: 'bar',
                        data: data3,
                    },
                    {
                        name: '退货率',
                        type: 'line',
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                formatter: '{c}%'
                            }
                        },
                        yAxisIndex: 1,
                        data: data4,
                    }
                ]
            };
			ECharts.Draw(option, top.document.getElementById('chartModalCon'));
		},
		createChart10: function(data){
			var data1 = [];
	        	var data2 = [];
	        	for(var i=0,len=data.length; i<len; i++){
	        		data1.push(data[i].name);
	        		data2.push(data[i].value.replace('%','') * 1);
	        	}
			var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                grid: {
                    left: '1%',
                    right: '1%',
                    bottom: '1%',
                    containLabel: true
                },
                legend: {
                	data: ['昨日折扣率'],
                    top: '2%'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        boundaryGap: false,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        name: '占比',
                        axisLabel: {
                            formatter: '{value}%'
                        }
                    }
                ],
                series: [
                    {
                        name: '昨日折扣率',
                        type: 'line',
                        label: {
                            normal: {
                                show: true,
                                position: 'top',
                                formatter: '{c}%'
                            }
                        },
                       // areaStyle: { normal: {} },
                        data: data2
                    }
                ]
            };
			ECharts.Draw(option, top.document.getElementById('chartModalCon'));
		},
		createChart11: function(data){
			var data1 = [];
	        	var data2 = [];
	        	for(var i=0,len=data.length; i<len; i++){
	        		data1.push(top.InitData.getBranchName(data[i].name));
	        		data2.push(data[i].value * 1);
	        	}
	        	var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                	data: ['客单价'],
                    top: '2%'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        //name: '',
                        axisLabel: {
                            formatter: '{value}元/单'
                        }
                    }
                ],
                series: [
                    {
                        name: '客单价',
                        type: 'line',
                        label: {
                            normal: {
                                show: true,
                                position: 'top'
                            }
                        },
                        data: data2
                    },
                ]
            };
			ECharts.Draw(option, top.document.getElementById('chartModalCon'));
		},
		createChart12: function(data){
			var data1 = [];
	        	var data2 = [];
	        	for(var i=0,len=data.length; i<len; i++){
	        		data1.push(top.InitData.getBranchName(data[i].name));
	        		data2.push(fixNum(data[i].value * 1, 2));
	        	}
	        	var option = {
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#999'
                        }
                    }
                },
                legend: {
                	data: ['连带率'],
                    top: '2%'
                },
                xAxis: [
                    {
                        type: 'category',
                        data: data1,
                        axisPointer: {
                            type: 'shadow'
                        }
                    }
                ],
                yAxis: [
                    {
                        type: 'value',
                        //name: '百分比',
                        axisLabel: {
                            formatter: '{value}'
                        }
                    }
                ],
                series: [
                    {
                        name: '连带率',
                        type: 'line',
                        label: {
                            normal: {
                                show: true,
                                position: 'top'
                            }
                        },
                        data: data2,
                    },
                ]
            };
			ECharts.Draw(option, top.document.getElementById('chartModalCon'));
		},
		showBranchTask: function(){
			top.chartModal.show('今日各店实时任务进度');
            this.getToday_realtime_aim(function(data){
            	top.chartModal.showLoading = false;
            	if(data && data.length > 0){
            		var data1 = [];
                	var data2 = [];
                	var data3 = [];
                	var data4 = [];
                	for(var i=0,len=data.length; i<len; i++){
                		data1.push(top.InitData.getBranchName(data[i]['门店']));
                		data2.push(data[i]['任务']);
                		data3.push(data[i]['今日完成']);
                		data4.push(data[i]['完成率']);
                	}
                	var option = {
                        tooltip: {
                            trigger: 'axis',
                            axisPointer: {
                                type: 'cross',
                                crossStyle: {
                                    color: '#999'
                                }
                            }
                        },
                        grid: {
                            left: '1%',
                            right: '1%',
                            bottom: '1%',
                            containLabel: true
                        },
                        legend: {
                            data: ['任务', '今日完成', '完成率'],
                            top: '2%'
                        },
                        xAxis: [
                            {
                                type: 'category',
                                data: data1,
                                axisPointer: {
                                    type: 'shadow'
                                }
                            }
                        ],
                        yAxis: [
                            {
                                type: 'value',
                                name: '金额',
                                axisLabel: {
                                    formatter: '{value} 元'
                                }
                            },
                            {
                                type: 'value',
                                name: '完成率',
                                axisLabel: {
                                    formatter: '{value}%'
                                }
                            }
                        ],
                        series: [
                            {
                                name: '任务',
                                type: 'bar',
                                data: data2,
                            },
                            {
                                name: '今日完成',
                                type: 'bar',
                                data: data3,
                            },
                            {
                                name: '完成率',
                                type: 'line',
                                yAxisIndex: 1,
                                data: data4
                            }
                        ]
                    };
                	top.chartModal.noData = false;
    				setTimeout(function(){
    					ECharts.Draw(option, top.document.getElementById('chartModalCon'));
    				},200);
            	}else{
            		top.chartModal.noData = true;
            	}
            });
		}
	}
});