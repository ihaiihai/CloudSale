var pageVM = new Vue({
	el : '#page',
	components: {
		Treeselect: VueTreeselect.Treeselect
	},
	data : {
		gridTab: 0,
		areaCode: null,
		areaOptions:[],
		date: moment().startOf('month').format('YYYY-MM-DD') + ' - ' + moment().format('YYYY-MM-DD'),
		chart1Data:null,
		chart2Data:null,
		gridData:null,
		data1:[{"异常率":3.763567258300}],
		data2:[{"name":"鞋帽袜散货件","value":342},{"name":"婴童装外出服","value":261},{"name":null,"value":220},{"name":"床品内衣宝宝套","value":207},{"name":"食品","value":73},{"name":"玩具","value":73},{"name":"赠品","value":32},{"name":"洗护用品","value":29},{"name":"纸尿裤","value":26},{"name":"喂养用品","value":25},{"name":"游泳馆","value":25},{"name":"孕产妇类","value":22},{"name":"童车","value":18},{"name":"购物券","value":17},{"name":"物料","value":16},{"name":"产后护理","value":1}],
		data3:[{"门店":"000001","item_no":"YJ020","条码":"YJ020","货号":"YJ020","名称":"帽架（带夹子）","品牌":"PP                  ","零售价":5.0000,"库存数量":-608.0000000000,"库存零售金额":-3040.00000000000},{"门店":"000001","item_no":"6943863702305","条码":"6943863702305","货号":"DG3745","名称":"迪高压力爬行毛毛虫","品牌":"15                  ","零售价":58.0000,"库存数量":-37.0000000000,"库存零售金额":-2146.00000000000},{"门店":"000001","item_no":"DBD002","条码":"DBD002","货号":"DBD002","名称":"服装打包袋（中）","品牌":"0651                ","零售价":0.4500,"库存数量":-3097.0000000000,"库存零售金额":-1393.65000000000},{"门店":"000001","item_no":"LC800-H","条码":"LC800-H","货号":"LC800-H-L197","名称":"5004童车(LC800HL197)","品牌":"1001                ","零售价":1148.0000,"库存数量":-1.0000000000,"库存零售金额":-1148.00000000000},{"门店":"000001","item_no":"DBD001","条码":"DBD001","货号":"DBD001","名称":"服装打包袋（小）","品牌":"0651                ","零售价":0.2500,"库存数量":-3646.0000000000,"库存零售金额":-911.50000000000},{"门店":"000001","item_no":"6943121488487","条码":"6943121488487","货号":"8377","名称":"婴花坊7D钻石绒加长睡袋","品牌":"110346              ","零售价":252.0000,"库存数量":-3.0000000000,"库存零售金额":-756.00000000000}]
	},
	mounted : function() {
		this.init();
	},
	methods : {
		init : function() {
			var me = this;
			setTimeout(function() {
				me.initEvent();
				me.loadData();
				me.loadAreaOptions();
			});
		},
		initEvent: function(){
			var me = this;
			$('#page').find('.daterange').dateRange();
            $(window).resize(function (e) {
                window.setTimeout(function () {
                    //$("#gridPanel_1").setGridWidth($("#gridPanel_1").parents('.card-body').width());
                }, 200);
            });
		},
		loadAreaOptions: function(parentAreaCode, callback){
			var me = this;
			$.getJSON(baseurl + "/sys/area/queryOptions", {
				parentAreaCode: parentAreaCode
			}, function(res) {
				if(callback){
					callback(res);
				}else{
					me.areaOptions = res;
				}
			});
		},
		loadData: function(){
			this.getData1();
			this.getData2();
			this.getData3();
		},
		getData1 : function() {
			var me = this;
			me.chart1Data = null;
			$.get(baseurl + "/Report_PC/Branch_DisError/branch_errorlv?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.chart1Data = data;
				setTimeout(function(){
					me.createChart1(data);
				});
			});
		},
		getData2 : function() {
			var me = this;
			me.chart2Data = null;
			$.get(baseurl + "/Report_PC/Branch_DisError/branch_error_fenbu?branchNo="+(me.areaCode?me.areaCode:''),function(data){
				me.chart2Data = data;
				setTimeout(function(){
					me.createChart2(data);
				});
			});
		},
		getData3 : function() {
			var me = this;
			me.gridData = null;
			$.get(baseurl + "/Report_PC/Branch_DisError/branch_error_detail?branchNo="+(me.areaCode?me.areaCode:'')+"&pageNo=0&pageSize=50",function(data){
				me.gridData = data;
				setTimeout(function(){
					me.GetGrid(data);
				});
			});
		},
		createChart1 : function(data) {
			var me = this;
			var option = {
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [{
                    name: '异常率',
                    type: 'pie',
                    radius: [
                    '60%',
                    '75%'
                    ],
                    label: {
                        normal: {
                            position: 'center'
                        }
                    },
                    data: [{
                        value: fixNum(data[0].异常率,2),
                        name: '异常率',
                        label: {
                            normal: {
                                formatter: '{c} %',
                                textStyle: {
                                    fontSize: 26
                                }
                            }
                        }
                    }, {
                        value: (data[0].异常率 >= 100) ? (0) : (100 - ((data[0].异常率))),
                        name: '占比',
                        label: {
                            normal: {
                                formatter: '\n异常率',
                                textStyle: {
                                    color: '#555',
                                    fontSize: 16
                                }
                            }
                        },
                        tooltip: {
                            show: false
                        },
                        itemStyle: {
                            normal: {
                                color: '#dedede'
                            },
                            emphasis: {
                                color: '#dedede'
                            }
                        },
                        hoverAnimation: false
                    }]
                }
                ]
            };
			ECharts.Draw(option, "chart1");
		},
		createChart2 : function(data) {
			var me = this;
			var option = {
                title: {
                    text: '异常商品分布（点击联动右边明细表）',
                    x: 'center',
                    backgroundColor: '#dedede',
                    padding: [5, 1000]
                },
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b} : {c} ({d}%)"
                },
                series: [
                {
                    name: '品牌销售数量',
                    type: 'pie',
                    radius: '55%',
                    center: ['50%', '60%'],
                    data: data,
                    avoidLabelOverlap: true,
                    itemStyle: {
                        normal: {
                            label: {
                                show: true,
                                formatter: '{b} :\n {c}',
                                textStyle: {
                                    color: '#333'
                                }
                            },
                            labelLine: { show: true }
                        }
                    }
                }
                ]
            };
			ECharts.Draw(option, "chart2");
		},
        GetGrid: function (data) {
        	if(data == null || data.length == 0)return;
            var $gridTable = $("#gridTable");
            var keyWord_branch = {
                "col_name": "门店~item_no~条码~货号~名称~品牌~零售价~库存数量~库存零售金额"
            }
            $gridTable.jqGridEx({
                datatype: "local",
                data:data,
                height: $(window).height() - 200,
                width: $('.gridCard').width() - 10,
                colModel: [
                    { name: "门店", label: "类别", align: 'center', sortable: false, formatter: public_data.Dic_branch },
                    { name: "item_no", label: "item_no", align: "center", sortable: false,hidden:true},
                    { name: "条码", label: "条码", align: "center", sortable: false },
                    { name: "货号", label: "自编码", align: "center", sortable: false },
                    { name: "名称", label: "名称", align: "center",width:300, sortable: false },
                    { name: "品牌", label: "品牌", align: "center", sortable: true, formatter: public_data.Dic_brand },
                    { name: "零售价", label: "零售价", align: "center", sortable: true, sorttype: 'float' },
                    { name: "库存数量", label: "库存数量", align: "center", sortable: true, sorttype: 'float' },
                    { name: "库存零售金额", label: "库存零售金额", align: "center", sortable: true, sorttype: 'float' }
                ],
                rownumbers: true, //如果为ture则会在表格左边新增一列，显示行顺序号，从1开始递增。此列名为'rn'.
                rownumWidth: 50,
                rowNum: 100,
                pager: "#pager_list",
                pagerpos: "right",
                rowList: [100, 200, 500, 1000],
                pagerpos: "right",
                loadonce: true,  //缓存排序后才可翻页
            })
            $(window).resize(function (e) {
                window.setTimeout(function () {
                	$gridTable.setGridHeight($(window).height() - 200);
                	$gridTable.setGridWidth($('.gridCard').width() - 10);
                }, 200);
            });
        }
	}
});