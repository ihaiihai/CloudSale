package com.bi.cloudsale.condition;

public class ItemQueryCondition extends BaseQueryCondition {

	private String clsNo;

	public String getClsNo() {
		return clsNo;
	}

	public void setClsNo(String clsNo) {
		this.clsNo = clsNo;
	}
}
