package com.bi.cloudsale.condition;

public class TaskQueryCondition extends BaseQueryCondition {

	private String branchNos;
	private Integer year;
	private Integer month;
	private Integer aimType;
	private Integer aimCycle;
	public String getBranchNos() {
		return branchNos;
	}
	public void setBranchNos(String branchNos) {
		this.branchNos = branchNos;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getAimType() {
		return aimType;
	}
	public void setAimType(Integer aimType) {
		this.aimType = aimType;
	}
	public Integer getAimCycle() {
		return aimCycle;
	}
	public void setAimCycle(Integer aimCycle) {
		this.aimCycle = aimCycle;
	}
}
