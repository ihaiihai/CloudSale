package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.AimEntity;
import com.bi.cloudsale.persistent.entity.ItemClsEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface ItemClsDao {

	Long save(ItemClsEntity itemClsEntity) throws HibernateException;

	void updateEntity(ItemClsEntity itemClsEntity) throws HibernateException;

	List<ItemClsEntity> list(String hql);
}
