package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_im_branch_stock", catalog = "cloudsale")
public class StockEntity implements Serializable{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String branch_no;
  private String item_no;
  private Double stock_qty;
  private Double cost_price;
  private Double cost_amt;
  private Double max_qty;
  private Double min_qty;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getBranch_no() {
    return branch_no;
  }

  public void setBranch_no(String branch_no) {
    this.branch_no = branch_no;
  }

  public String getItem_no() {
    return item_no;
  }

  public void setItem_no(String item_no) {
    this.item_no = item_no;
  }

  public Double getStock_qty() {
    return stock_qty;
  }

  public void setStock_qty(Double stock_qty) {
    this.stock_qty = stock_qty;
  }

  public Double getCost_price() {
    return cost_price;
  }

  public void setCost_price(Double cost_price) {
    this.cost_price = cost_price;
  }

  public Double getCost_amt() {
    return cost_amt;
  }

  public void setCost_amt(Double cost_amt) {
    this.cost_amt = cost_amt;
  }

  public Double getMax_qty() {
    return max_qty;
  }

  public void setMax_qty(Double max_qty) {
    this.max_qty = max_qty;
  }

  public Double getMin_qty() {
    return min_qty;
  }

  public void setMin_qty(Double min_qty) {
    this.min_qty = min_qty;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
