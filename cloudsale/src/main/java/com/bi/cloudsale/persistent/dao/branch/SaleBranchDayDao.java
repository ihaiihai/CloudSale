package com.bi.cloudsale.persistent.dao.branch;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.bi.cloudsale.persistent.entity.branch.SaleBranchDayEntity;

public interface SaleBranchDayDao {

	double querySum(String userId, String orgId, Set<String> branchNo, Date startDate, Date endDate);
	
	List<SaleBranchDayEntity> querySumByBranch(String userId, String orgId, Set<String> branchNo, Date startDate, Date endDate);
	
	List<SaleBranchDayEntity> queryAllSumByBranch(String userId, String orgId, Set<String> branchNo, Date startDate, Date endDate);
}
