package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.AimDao;
import com.bi.cloudsale.persistent.dao.BranchTimeDao;
import com.bi.cloudsale.persistent.entity.AimEntity;
import com.bi.cloudsale.persistent.entity.BranchTimeEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class BranchTimeDaoImpl extends BaseDao<BranchTimeEntity> implements BranchTimeDao {

	public BranchTimeDaoImpl() {
		super();
		setClazz(BranchTimeEntity.class);
	}

	@Override
	public Long save(BranchTimeEntity branchTimeEntity)  throws HibernateException {
		branchTimeEntity = createEntity(branchTimeEntity);
		return branchTimeEntity.getId();
	}

	@Override
	public void updateEntity(BranchTimeEntity branchTimeEntity) throws HibernateException {
		update(branchTimeEntity);
	}

	@Override
	public List<BranchTimeEntity> list(String hql) {
		return	findByHql(hql);
	}
}
