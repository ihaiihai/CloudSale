package com.bi.cloudsale.persistent.dao.saler.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.saler.SaleSalerDayDao;
import com.bi.cloudsale.persistent.entity.saler.SaleSalerDayEntity;

@Repository
@Transactional
public class SaleSalerDayDaoImpl extends BaseDao<SaleSalerDayEntity> implements SaleSalerDayDao {

	@Override
	public List<SaleSalerDayEntity> querySum(String userId, String orgId, Set<String> salerNo, Set<String> branchNo, Date startDate,
			Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select new SaleSalerDayEntity(branch_no,sale_id,sum(sale_amt) as sale_amt) from SaleSalerDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(salerNo != null && salerNo.size() > 0) {
			hqlSb.append(" and sale_id in (:salerNo)");
			mParam.put("salerNo", salerNo);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		hqlSb.append(" GROUP BY branch_no,sale_id");
		return find(hqlSb.toString(), mParam);
	}

	@Override
	public List<SaleSalerDayEntity> querySalerCount(String userId, String orgId, Set<String> branchNo, Date startDate,
			Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select new SaleSalerDayEntity(branch_no,count(DISTINCT sale_id) as salerCount) from SaleSalerDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		hqlSb.append(" GROUP BY branch_no");
		return find(hqlSb.toString(), mParam);
	}

}
