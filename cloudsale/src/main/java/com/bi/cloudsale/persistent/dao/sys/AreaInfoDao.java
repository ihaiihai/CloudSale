package com.bi.cloudsale.persistent.dao.sys;

import java.util.List;

import org.hibernate.HibernateException;

import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.persistent.entity.sys.AreaInfoEntity;

public interface AreaInfoDao {
	
	AreaInfoEntity queryById(Long id);

	AreaInfoEntity queryByCode(String areaCode);

	void createAreaInfo(AreaInfoEntity areaInfoEntity) throws HibernateException;
	
	void deleteAreaInfo(String areaCode) throws HibernateException;
	
	void deleteAreaByParentCode(String parentAreaCode) throws HibernateException;
	
	void updateAreaInfo(AreaInfoEntity areaInfoEntity) throws HibernateException;
	
	List<AreaInfoEntity> queryByCondition(BaseQueryCondition baseQueryCondition);
	
	AreaInfoEntity queryLastInParent(String userId, Long parentId);
	
	void updateSortNo(String userId, Integer sortNo);
	
	List<AreaInfoEntity> queryChild(String areaCode);
	
	long queryChildCount(String areaCode);
}
