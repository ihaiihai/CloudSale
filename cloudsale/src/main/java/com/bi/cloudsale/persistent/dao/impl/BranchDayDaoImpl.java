package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.BranchDayDao;
import com.bi.cloudsale.persistent.entity.BranchDayEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
@Transactional
public class BranchDayDaoImpl extends BaseDao<BranchDayEntity> implements BranchDayDao {

	public BranchDayDaoImpl() {
		super();
		setClazz(BranchDayEntity.class);
	}

	@Override
	public Long save(BranchDayEntity branchDayEntity)  throws HibernateException {
		branchDayEntity = createEntity(branchDayEntity);
		return branchDayEntity.getId();
	}

	@Override
	public void updateEntity(BranchDayEntity branchDayEntity) throws HibernateException {
		update(branchDayEntity);
	}

	@Override
	public List<BranchDayEntity> list(String hql) {
		return	findByHql(hql);
	}

	@Override
	public long querySaleAmt(String orgId, String startDate, String endDate) {
		String hql = "select sum(saleAmt) from BranchDayEntity where org_id = ? and oper_date >= ? and oper_date <= ?";
		List<Object> param = new ArrayList<Object>();
		param.add(orgId);
		param.add(startDate);
		param.add(endDate);
		return findCount(hql, param);
	}
}
