package com.bi.cloudsale.persistent.dao.cls.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.cls.SaleClsDayDao;
import com.bi.cloudsale.persistent.entity.cls.SaleClsDayEntity;

@Repository
@Transactional
public class SaleClsDayDaoImpl extends BaseDao<SaleClsDayEntity> implements SaleClsDayDao {

	@Override
	public List<SaleClsDayEntity> querySum(String userId, String orgId, Set<String> clsNo, Set<String> branchNo,
			Date startDate, Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select new SaleClsDayEntity(branch_no,item_clsno,sum(sale_amt) as sale_amt) from SaleClsDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(clsNo != null && clsNo.size() > 0) {
			hqlSb.append(" and item_clsno in (:clsNo)");
			mParam.put("clsNo", clsNo);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		hqlSb.append(" GROUP BY branch_no,item_clsno");
		return find(hqlSb.toString(), mParam);
	}

	@Override
	public List<SaleClsDayEntity> querySumByBranch(String userId, String orgId, Set<String> branchNo, Date startDate,
			Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select new SaleClsDayEntity(branch_no,sum(sale_amt) as sale_amt) from SaleClsDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		hqlSb.append(" GROUP BY branch_no");
		return find(hqlSb.toString(), mParam);
	}

	@Override
	public List<SaleClsDayEntity> querySumByBranch(String userId, String orgId, String branchNo, Date startDate,
			Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select new SaleClsDayEntity(branch_no,item_clsno,sum(sale_amt) as sale_amt) from SaleClsDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(branchNo != null) {
			hqlSb.append(" and branch_no = :branch_no");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		hqlSb.append(" GROUP BY item_clsno");
		return find(hqlSb.toString(), mParam);
	}

}
