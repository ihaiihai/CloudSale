package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.SalerEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface SalerDao {

	Long save(SalerEntity salerEntity) throws HibernateException;

	void updateEntity(SalerEntity salerEntity) throws HibernateException;

	List<SalerEntity> list(String hql);
}
