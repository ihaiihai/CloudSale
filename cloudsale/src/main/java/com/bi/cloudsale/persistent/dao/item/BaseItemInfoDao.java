package com.bi.cloudsale.persistent.dao.item;

import java.util.List;

import com.bi.cloudsale.persistent.entity.item.BaseItemInfoEntity;

public interface BaseItemInfoDao {

	List<BaseItemInfoEntity> queryByCondition(String orgId, String clsNo, String key, Integer pageIndex, Integer pageSize);
}
