package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.persistent.dao.DeductRuleDetailDao;
import com.bi.cloudsale.persistent.entity.DeductRuleDetailEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class DeductRuleDetailDaoImpl extends BaseDao<DeductRuleDetailEntity> implements DeductRuleDetailDao {

	public DeductRuleDetailDaoImpl() {
		super();
		setClazz(DeductRuleDetailEntity.class);
	}

	@Override
	public List<DeductRuleDetailEntity> findByDeductNo(String deductNo) throws HibernateException {
		List<DeductRuleDetailEntity> deductRuleDetailEntities = findByHql(" where deduct_no = '"+deductNo+"'");
		return deductRuleDetailEntities;
	}

	@Override
	public void deleteByDeductNo(String deductNo) throws HibernateException {
		excuteBySql("delete from cs_deduct_rule_detail where deduct_no = '"+deductNo+"'");
	}

	@Override
	public Long save(DeductRuleDetailEntity deductRuleDetailEntity)  throws HibernateException {
		deductRuleDetailEntity.setGmt_create(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleDetailEntity.setGmt_modified(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleDetailEntity = createEntity(deductRuleDetailEntity);
		return deductRuleDetailEntity.getId();
	}
}
