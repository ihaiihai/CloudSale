package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_sale_aim", catalog = "cloudsale")
public class AimEntity implements Serializable{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private Long aim_type;
  private Long aim_cycle;
  private Long cycle_year;
  private Long cycle_month;
  private Long cycle_week;
  private String branch_clsno;
  private String branch_no;
  private String item_clsno;
  private String item_brandno;
  private String sale_id;
  private Double aim_value;
  private String other1;
  private String other2;
  private String other3;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public Long getAim_type() {
    return aim_type;
  }

  public void setAim_type(Long aim_type) {
    this.aim_type = aim_type;
  }

  public Long getAim_cycle() {
    return aim_cycle;
  }

  public void setAim_cycle(Long aim_cycle) {
    this.aim_cycle = aim_cycle;
  }

  public Long getCycle_year() {
    return cycle_year;
  }

  public void setCycle_year(Long cycle_year) {
    this.cycle_year = cycle_year;
  }

  public Long getCycle_month() {
    return cycle_month;
  }

  public void setCycle_month(Long cycle_month) {
    this.cycle_month = cycle_month;
  }

  public Long getCycle_week() {
    return cycle_week;
  }

  public void setCycle_week(Long cycle_week) {
    this.cycle_week = cycle_week;
  }

  public String getBranch_clsno() {
    return branch_clsno;
  }

  public void setBranch_clsno(String branch_clsno) {
    this.branch_clsno = branch_clsno;
  }

  public String getBranch_no() {
    return branch_no;
  }

  public void setBranch_no(String branch_no) {
    this.branch_no = branch_no;
  }

  public String getItem_clsno() {
    return item_clsno;
  }

  public void setItem_clsno(String item_clsno) {
    this.item_clsno = item_clsno;
  }

  public String getItem_brandno() {
    return item_brandno;
  }

  public void setItem_brandno(String item_brandno) {
    this.item_brandno = item_brandno;
  }

  public String getSale_id() {
    return sale_id;
  }

  public void setSale_id(String sale_id) {
    this.sale_id = sale_id;
  }

  public Double getAim_value() {
    return aim_value;
  }

  public void setAim_value(Double aim_value) {
    this.aim_value = aim_value;
  }

  public String getOther1() {
    return other1;
  }

  public void setOther1(String other1) {
    this.other1 = other1;
  }

  public String getOther2() {
    return other2;
  }

  public void setOther2(String other2) {
    this.other2 = other2;
  }

  public String getOther3() {
    return other3;
  }

  public void setOther3(String other3) {
    this.other3 = other3;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
