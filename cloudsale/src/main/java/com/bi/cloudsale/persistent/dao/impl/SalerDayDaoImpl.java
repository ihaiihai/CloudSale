package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.SalerDayDao;
import com.bi.cloudsale.persistent.entity.BranchDayEntity;
import com.bi.cloudsale.persistent.entity.SalerDayEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class SalerDayDaoImpl extends BaseDao<SalerDayEntity> implements SalerDayDao {

	public SalerDayDaoImpl() {
		super();
		setClazz(SalerDayEntity.class);
	}

	@Override
	public Long save(SalerDayEntity salerDayEntity)  throws HibernateException {
		salerDayEntity = createEntity(salerDayEntity);
		return salerDayEntity.getId();
	}

	@Override
	public void updateEntity(SalerDayEntity salerDayEntity) throws HibernateException {
		update(salerDayEntity);
	}

	@Override
	public List<SalerDayEntity> list(String hql) {
		return	findByHql(hql);
	}
}
