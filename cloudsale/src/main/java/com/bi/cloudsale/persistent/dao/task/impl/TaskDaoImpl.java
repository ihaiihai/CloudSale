package com.bi.cloudsale.persistent.dao.task.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.persistent.dao.task.TaskDao;
import com.bi.cloudsale.persistent.entity.task.SaleAimEntity;

@Repository
@Transactional
public class TaskDaoImpl extends BaseDao<SaleAimEntity> implements TaskDao {

	@Override
	public List<SaleAimEntity> queryByCondition(TaskQueryCondition condition) {
		StringBuilder hqlSb = new StringBuilder("from SaleAimEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", condition.getOrgId());
		if(condition.getUserId() != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", condition.getUserId());
		}
		if(condition.getAimType() != null) {
			hqlSb.append(" and aim_type = :aim_type");
			mParam.put("aim_type", condition.getAimType());
		}
		if(condition.getAimCycle() != null) {
			hqlSb.append(" and aim_cycle = :aim_cycle");
			mParam.put("aim_cycle", condition.getAimCycle());
		}
		if(condition.getYear() != null) {
			hqlSb.append(" and cycle_year = :cycle_year");
			mParam.put("cycle_year", condition.getYear());
		}
		if(StringUtils.isNotEmpty(condition.getBranchNos())) {
			hqlSb.append(" and branch_no in (:branch_no)");
			Set<String> branch_no = new HashSet<>(Arrays.asList(condition.getBranchNos().split(",")));
			mParam.put("branch_no", branch_no);
		}
		return find(hqlSb.toString(), mParam);
	}
	
}
