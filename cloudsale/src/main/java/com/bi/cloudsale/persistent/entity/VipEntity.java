package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_base_vip_info", catalog = "cloudsale")
public class VipEntity implements Serializable{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String card_id;
  private String card_no;
  private String card_type;
  private String card_name;
  private String branch_no;
  private java.sql.Timestamp create_date;
  private String sale_id;
  private java.sql.Timestamp birthday;
  private java.sql.Timestamp birhtday2;
  private String sex;
  private java.sql.Timestamp fst_date;
  private Double fst_amt;
  private java.sql.Timestamp snd_date;
  private Double snd_amt;
  private java.sql.Timestamp thd_date;
  private Double thd_amt;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getCard_id() {
    return card_id;
  }

  public void setCard_id(String card_id) {
    this.card_id = card_id;
  }

  public String getCard_no() {
    return card_no;
  }

  public void setCard_no(String card_no) {
    this.card_no = card_no;
  }

  public String getCard_type() {
    return card_type;
  }

  public void setCard_type(String card_type) {
    this.card_type = card_type;
  }

  public String getCard_name() {
    return card_name;
  }

  public void setCard_name(String card_name) {
    this.card_name = card_name;
  }

  public String getBranch_no() {
    return branch_no;
  }

  public void setBranch_no(String branch_no) {
    this.branch_no = branch_no;
  }

  public java.sql.Timestamp getCreate_date() {
    return create_date;
  }

  public void setCreate_date(java.sql.Timestamp create_date) {
    this.create_date = create_date;
  }

  public String getSale_id() {
    return sale_id;
  }

  public void setSale_id(String sale_id) {
    this.sale_id = sale_id;
  }

  public java.sql.Timestamp getBirthday() {
    return birthday;
  }

  public void setBirthday(java.sql.Timestamp birthday) {
    this.birthday = birthday;
  }

  public java.sql.Timestamp getBirhtday2() {
    return birhtday2;
  }

  public void setBirhtday2(java.sql.Timestamp birhtday2) {
    this.birhtday2 = birhtday2;
  }

  public String getSex() {
    return sex;
  }

  public void setSex(String sex) {
    this.sex = sex;
  }

  public java.sql.Timestamp getFst_date() {
    return fst_date;
  }

  public void setFst_date(java.sql.Timestamp fst_date) {
    this.fst_date = fst_date;
  }

  public Double getFst_amt() {
    return fst_amt;
  }

  public void setFst_amt(Double fst_amt) {
    this.fst_amt = fst_amt;
  }

  public java.sql.Timestamp getSnd_date() {
    return snd_date;
  }

  public void setSnd_date(java.sql.Timestamp snd_date) {
    this.snd_date = snd_date;
  }

  public Double getSnd_amt() {
    return snd_amt;
  }

  public void setSnd_amt(Double snd_amt) {
    this.snd_amt = snd_amt;
  }

  public java.sql.Timestamp getThd_date() {
    return thd_date;
  }

  public void setThd_date(java.sql.Timestamp thd_date) {
    this.thd_date = thd_date;
  }

  public Double getThd_amt() {
    return thd_amt;
  }

  public void setThd_amt(Double thd_amt) {
    this.thd_amt = thd_amt;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
