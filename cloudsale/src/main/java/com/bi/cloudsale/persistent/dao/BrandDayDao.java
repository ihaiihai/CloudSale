package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.BranchDayEntity;
import com.bi.cloudsale.persistent.entity.BrandDayEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface BrandDayDao {

	Long save(BrandDayEntity brandDayEntity) throws HibernateException;

	void updateEntity(BrandDayEntity brandDayEntity) throws HibernateException;

	List<BrandDayEntity> list(String hql);
}
