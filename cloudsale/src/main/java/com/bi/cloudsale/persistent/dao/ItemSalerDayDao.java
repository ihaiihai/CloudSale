package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.BranchDayEntity;
import com.bi.cloudsale.persistent.entity.ItemSalerDayEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface ItemSalerDayDao {

	Long save(ItemSalerDayEntity itemSalerDayEntity) throws HibernateException;

	void updateEntity(ItemSalerDayEntity itemSalerDayEntity) throws HibernateException;

	List<ItemSalerDayEntity> list(String hql);
}
