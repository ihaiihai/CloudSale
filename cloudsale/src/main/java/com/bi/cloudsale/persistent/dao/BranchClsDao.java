package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.BranchClsEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface BranchClsDao {

	Long save(BranchClsEntity branchClsEntity) throws HibernateException;

	void updateEntity(BranchClsEntity branchClsEntity) throws HibernateException;

	List<BranchClsEntity> list(String hql);
}
