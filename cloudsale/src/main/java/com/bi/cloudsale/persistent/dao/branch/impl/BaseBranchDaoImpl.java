package com.bi.cloudsale.persistent.dao.branch.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.branch.BaseBranchDao;
import com.bi.cloudsale.persistent.entity.branch.BaseBranchEntity;

@Repository
@Transactional
public class BaseBranchDaoImpl extends BaseDao<BaseBranchEntity> implements BaseBranchDao {

	@Override
	public List<BaseBranchEntity> queryByBranch(String userId, String orgId, Set<String> branchNo) {
		StringBuilder hqlSb = new StringBuilder("from BranchEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		return find(hqlSb.toString(), mParam);
	}

}
