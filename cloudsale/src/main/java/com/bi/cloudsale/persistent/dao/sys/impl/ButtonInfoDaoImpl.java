package com.bi.cloudsale.persistent.dao.sys.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.sys.ButtonInfoDao;
import com.bi.cloudsale.persistent.entity.sys.ButtonInfoEntity;

@Repository
@Transactional
public class ButtonInfoDaoImpl extends BaseDao<ButtonInfoEntity> implements ButtonInfoDao {

	@Override
	public List<ButtonInfoEntity> queryAll() {
		StringBuilder hqlSb = new StringBuilder("select new ButtonInfoEntity(b.buttonCode,b.buttonName,b.moduleId,m.moduleName)  from ButtonInfoEntity as b,ModuleInfoEntity m where b.moduleId = m.moduleId");
		List<Object> params = new ArrayList<Object>();
		return find(hqlSb.toString(), params);
	}

}
