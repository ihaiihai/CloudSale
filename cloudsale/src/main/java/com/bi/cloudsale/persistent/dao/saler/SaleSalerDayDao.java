package com.bi.cloudsale.persistent.dao.saler;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.bi.cloudsale.persistent.entity.saler.SaleSalerDayEntity;

public interface SaleSalerDayDao {
 
	List<SaleSalerDayEntity> querySum(String userId, String orgId, Set<String> salerNo, Set<String> branchNo, Date startDate, Date endDate);
	
	List<SaleSalerDayEntity> querySalerCount(String userId, String orgId, Set<String> branchNo, Date startDate, Date endDate);
}
