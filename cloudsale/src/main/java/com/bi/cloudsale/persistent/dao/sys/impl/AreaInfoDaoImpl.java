package com.bi.cloudsale.persistent.dao.sys.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.persistent.dao.sys.AreaInfoDao;
import com.bi.cloudsale.persistent.entity.sys.AreaInfoEntity;

@Repository
@Transactional
public class AreaInfoDaoImpl extends BaseDao<AreaInfoEntity> implements AreaInfoDao {

	@Override
	public AreaInfoEntity queryByCode(String areaCode) {
		StringBuilder hqlSb = new StringBuilder("from AreaInfoEntity where areaCode = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(areaCode);
		return findOne(hqlSb.toString(), params);
	}

	@Override
	public void createAreaInfo(AreaInfoEntity areaInfoEntity) throws HibernateException {
		create(areaInfoEntity);
	}

	@Override
	public void deleteAreaInfo(String areaCode) throws HibernateException {
		AreaInfoEntity areaInfoEntity = this.queryByCode(areaCode);
		this.delete(areaInfoEntity);
	}

	@Override
	public void updateAreaInfo(AreaInfoEntity areaInfoEntity) throws HibernateException {
		this.update(areaInfoEntity);
	}

	@Override
	public List<AreaInfoEntity> queryByCondition(BaseQueryCondition baseQueryCondition) {
		StringBuilder hqlSb = new StringBuilder("from AreaInfoEntity");
		StringBuilder whereSb = new StringBuilder();
		Map<String,Object> mParams = new HashMap<String,Object>();
		if(!StringUtils.isEmpty(baseQueryCondition.getUserId())) {
			whereSb.append(" and userId = :userId ");
			mParams.put("userId", baseQueryCondition.getUserId());
		}
		if(!StringUtils.isEmpty(baseQueryCondition.getKey())) {
			whereSb.append(" and areaName like :areaName ");
			mParams.put("areaName", "%" + baseQueryCondition.getKey() + "%");
		}
		if(whereSb.length() > 0) {
			String whereStr = whereSb.toString();
			whereStr = whereStr.replaceFirst(" and", " where");
			hqlSb.append(whereStr);
		}
		hqlSb.append(" order by level");
		return find(hqlSb.toString(), mParams);
	}

	@Override
	public AreaInfoEntity queryLastInParent(String userId, Long parentId) {
		StringBuilder hqlSb = new StringBuilder("from AreaInfoEntity where userId = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(userId);
		if(parentId != null) {
			hqlSb.append(" and id = ?");
			params.add(parentId);
		}
		hqlSb.append("order by sortNo desc limit 1");
		return findOne(hqlSb.toString(), params);
	}

	@Override
	public void updateSortNo(String userId, Integer sortNo) {
		StringBuilder hqlSb = new StringBuilder("update AreaInfoEntity set sortNo = sortNo + 1 where userId = ? and sortNo >= ?");
		List<Object> params = new ArrayList<Object>();
		params.add(userId);
		params.add(sortNo);
		excuteByHql(hqlSb.toString(), params);
	}

	@Override
	public AreaInfoEntity queryById(Long id) {
		StringBuilder hqlSb = new StringBuilder("from AreaInfoEntity where id = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(id);
		return findOne(hqlSb.toString(), params);
	}

	@Override
	public void deleteAreaByParentCode(String parentAreaCode) throws HibernateException {
		StringBuilder hqlSb = new StringBuilder("delete from AreaInfoEntity where parentAreaCode = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(parentAreaCode);
		excuteByHql(hqlSb.toString(), params);
	}

	@Override
	public List<AreaInfoEntity> queryChild(String areaCode) {
		StringBuilder hqlSb = new StringBuilder("from AreaInfoEntity where parentAreaCode = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(areaCode);
		return find(hqlSb.toString(), params);
	}

	@Override
	public long queryChildCount(String areaCode) {
		StringBuilder hqlSb = new StringBuilder("select count(*) from AreaInfoEntity where parentAreaCode = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(areaCode);
		return findCount(hqlSb.toString(), params);
	}

}
