package com.bi.cloudsale.persistent.enums.sys;

public enum RoleAuthTypeEnum {
	
	MENU("菜单项", 1),
	BUTTON("按钮项", 2);

	private final String name;  
    private final Integer value;  
  
    public String getName() {  
        return name;  
    }  
  
    public Integer getValue() {  
        return value;  
    }  
  
    RoleAuthTypeEnum(String name, Integer value) {  
        this.name = name;  
        this.value = value;  
    }
}
