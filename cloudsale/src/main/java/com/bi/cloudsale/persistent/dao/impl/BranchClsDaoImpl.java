package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.AimDao;
import com.bi.cloudsale.persistent.dao.BranchClsDao;
import com.bi.cloudsale.persistent.entity.AimEntity;
import com.bi.cloudsale.persistent.entity.BranchClsEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class BranchClsDaoImpl extends BaseDao<BranchClsEntity> implements BranchClsDao {

	public BranchClsDaoImpl() {
		super();
		setClazz(BranchClsEntity.class);
	}

	@Override
	public Long save(BranchClsEntity branchClsEntity)  throws HibernateException {
		branchClsEntity = createEntity(branchClsEntity);
		return branchClsEntity.getId();
	}

	@Override
	public void updateEntity(BranchClsEntity branchClsEntity) throws HibernateException {
		update(branchClsEntity);
	}

	@Override
	public List<BranchClsEntity> list(String hql) {
		return	findByHql(hql);
	}
}
