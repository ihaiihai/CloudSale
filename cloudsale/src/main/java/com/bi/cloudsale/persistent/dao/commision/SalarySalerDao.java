package com.bi.cloudsale.persistent.dao.commision;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.bi.cloudsale.persistent.entity.commision.SalarySalerEntity;

public interface SalarySalerDao {

	Long save(SalarySalerEntity SalarySalerEntity) throws HibernateException;

	void updateEntity(SalarySalerEntity SalarySalerEntity) throws HibernateException;

	List<SalarySalerEntity> list(String hql, List<Object> params);
	
	List<SalarySalerEntity> queryByCondition(String orgId, Date startDate, Date endDate, String branchNos, String salerIds);
	
	void updateConfirm(String orgId, String account, Date startDate, Date endDate, String branchNos, String salerIds) throws HibernateException;
}
