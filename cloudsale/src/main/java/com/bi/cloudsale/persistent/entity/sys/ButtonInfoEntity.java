package com.bi.cloudsale.persistent.entity.sys;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cs_sys_button_info", catalog = "cloudsale")
public class ButtonInfoEntity implements Serializable {

	private static final long serialVersionUID = 2555153695382304719L;
	
	private Long id;
	private String buttonCode;
	private String buttonName;
	private String moduleId;
	private String description;
	private Date gmtCreate;
	private Date gmtModified;
	
	private String moduleName;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Basic(optional = false)
	@Column(name = "button_code")
	public String getButtonCode() {
		return buttonCode;
	}
	public void setButtonCode(String buttonCode) {
		this.buttonCode = buttonCode;
	}
	
	@Basic(optional = false)
	@Column(name = "button_name")
	public String getButtonName() {
		return buttonName;
	}
	public void setButtonName(String buttonName) {
		this.buttonName = buttonName;
	}
	
	@Basic(optional = false)
	@Column(name = "module_id")
	public String getModuleId() {
		return moduleId;
	}
	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}
	
	@Basic(optional = true)
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Basic(optional = false)
	@Column(name = "gmt_create")
	public Date getGmtCreate() {
		return gmtCreate;
	}
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	
	@Basic(optional = false)
	@Column(name = "gmt_modified")
	public Date getGmtModified() {
		return gmtModified;
	}
	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	public ButtonInfoEntity() {
		
	}
	
	public ButtonInfoEntity(String buttonCode, String buttonName, String moduleId, String moduleName) {
		this.buttonCode = buttonCode;
		this.buttonName = buttonName;
		this.moduleId = moduleId;
		this.moduleName = moduleName;
	}

}
