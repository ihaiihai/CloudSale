package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.DeductRuleExceptionEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface DeductRuleExceptionDao {

	List<DeductRuleExceptionEntity> findByDeductNo(String deductNo) throws HibernateException;

	void deleteByDeductNo(String deductNo) throws HibernateException;

	Long save(DeductRuleExceptionEntity deductRuleExceptionEntity)  throws HibernateException;
}
