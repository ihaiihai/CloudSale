package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_base_branch", catalog = "cloudsale")
public class BranchEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String branch_no;
  private String branch_name;
  private String branch_clsno;
  private Double area;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;
  @Transient
  private Integer checked = 0;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getBranch_no() {
    return branch_no;
  }

  public void setBranch_no(String branch_no) {
    this.branch_no = branch_no;
  }

  public String getBranch_name() {
    return branch_name;
  }

  public void setBranch_name(String branch_name) {
    this.branch_name = branch_name;
  }

  public String getBranch_clsno() {
    return branch_clsno;
  }

  public void setBranch_clsno(String branch_clsno) {
    this.branch_clsno = branch_clsno;
  }

  public Double getArea() {
    return area;
  }

  public void setArea(Double area) {
    this.area = area;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }

	public Integer getChecked() {
		return checked;
	}
	
	public void setChecked(Integer checked) {
		this.checked = checked;
	}
}
