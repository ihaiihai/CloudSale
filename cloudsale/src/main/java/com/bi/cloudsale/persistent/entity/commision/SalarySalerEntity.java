package com.bi.cloudsale.persistent.entity.commision;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cs_deduct_salary_saler", catalog = "cloudsale")
public class SalarySalerEntity implements Serializable {

	private static final long serialVersionUID = -8940091830601051721L;
	
	private Long id;
	private String user_id;
	private String org_id;
	private Date oper_date;
	private String deduct_no;
	private String deduct_desc;
	private String branch_no;
	private String saler_id;
	private Double aim_amt;
	private Double fin_amt;
	private Double deduct_amt;
	private Double share_amt1;
	private Double share_amt2;
	private Double share_amt3;
	private Double share_amt4;
	private Double off_amt;
	private Integer confirm_flag;
	private String confirm_man;
	private Date confirm_date;
	private Date gmt_create;
	private Date gmt_modified;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getOrg_id() {
		return org_id;
	}
	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}
	public Date getOper_date() {
		return oper_date;
	}
	public void setOper_date(Date oper_date) {
		this.oper_date = oper_date;
	}
	public String getDeduct_no() {
		return deduct_no;
	}
	public void setDeduct_no(String deduct_no) {
		this.deduct_no = deduct_no;
	}
	public String getDeduct_desc() {
		return deduct_desc;
	}
	public void setDeduct_desc(String deduct_desc) {
		this.deduct_desc = deduct_desc;
	}
	public String getBranch_no() {
		return branch_no;
	}
	public void setBranch_no(String branch_no) {
		this.branch_no = branch_no;
	}
	public String getSaler_id() {
		return saler_id;
	}
	public void setSaler_id(String saler_id) {
		this.saler_id = saler_id;
	}
	public Double getAim_amt() {
		return aim_amt;
	}
	public void setAim_amt(Double aim_amt) {
		this.aim_amt = aim_amt;
	}
	public Double getFin_amt() {
		return fin_amt;
	}
	public void setFin_amt(Double fin_amt) {
		this.fin_amt = fin_amt;
	}
	public Double getDeduct_amt() {
		return deduct_amt;
	}
	public void setDeduct_amt(Double deduct_amt) {
		this.deduct_amt = deduct_amt;
	}
	public Double getShare_amt1() {
		return share_amt1;
	}
	public void setShare_amt1(Double share_amt1) {
		this.share_amt1 = share_amt1;
	}
	public Double getShare_amt2() {
		return share_amt2;
	}
	public void setShare_amt2(Double share_amt2) {
		this.share_amt2 = share_amt2;
	}
	public Double getShare_amt3() {
		return share_amt3;
	}
	public void setShare_amt3(Double share_amt3) {
		this.share_amt3 = share_amt3;
	}
	public Double getShare_amt4() {
		return share_amt4;
	}
	public void setShare_amt4(Double share_amt4) {
		this.share_amt4 = share_amt4;
	}
	public Double getOff_amt() {
		return off_amt;
	}
	public void setOff_amt(Double off_amt) {
		this.off_amt = off_amt;
	}
	public Integer getConfirm_flag() {
		return confirm_flag;
	}
	public void setConfirm_flag(Integer confirm_flag) {
		this.confirm_flag = confirm_flag;
	}
	public String getConfirm_man() {
		return confirm_man;
	}
	public void setConfirm_man(String confirm_man) {
		this.confirm_man = confirm_man;
	}
	public Date getConfirm_date() {
		return confirm_date;
	}
	public void setConfirm_date(Date confirm_date) {
		this.confirm_date = confirm_date;
	}
	public Date getGmt_create() {
		return gmt_create;
	}
	public void setGmt_create(Date gmt_create) {
		this.gmt_create = gmt_create;
	}
	public Date getGmt_modified() {
		return gmt_modified;
	}
	public void setGmt_modified(Date gmt_modified) {
		this.gmt_modified = gmt_modified;
	}
}
