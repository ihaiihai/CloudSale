package com.bi.cloudsale.persistent.dao.cls;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.bi.cloudsale.persistent.entity.cls.SaleClsDayEntity;

public interface SaleClsDayDao {

	List<SaleClsDayEntity> querySum(String userId, String orgId, Set<String> clsNo, Set<String> branchNo, Date startDate, Date endDate);
	
	List<SaleClsDayEntity> querySumByBranch(String userId, String orgId, Set<String> branchNo, Date startDate, Date endDate);
	
	List<SaleClsDayEntity> querySumByBranch(String userId, String orgId, String branchNo, Date startDate, Date endDate);
}
