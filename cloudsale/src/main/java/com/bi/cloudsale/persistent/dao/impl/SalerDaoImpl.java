package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.BranchDao;
import com.bi.cloudsale.persistent.dao.SalerDao;
import com.bi.cloudsale.persistent.entity.BranchEntity;
import com.bi.cloudsale.persistent.entity.SalerEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class SalerDaoImpl extends BaseDao<SalerEntity> implements SalerDao {

	public SalerDaoImpl() {
		super();
		setClazz(SalerEntity.class);
	}

	@Override
	public Long save(SalerEntity salerEntity)  throws HibernateException {
		salerEntity = createEntity(salerEntity);
		return salerEntity.getId();
	}

	@Override
	public void updateEntity(SalerEntity salerEntity) throws HibernateException {
		update(salerEntity);
	}

	@Override
	public List<SalerEntity> list(String hql) {
		return	findByHql(hql);
	}
}
