package com.bi.cloudsale.persistent.dao.commision.impl;

import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.commision.SalarySalerDao;
import com.bi.cloudsale.persistent.entity.commision.SalarySalerEntity;

@Repository
@Transactional
public class SalarySalerDaoImpl extends BaseDao<SalarySalerEntity> implements SalarySalerDao {
	
	public SalarySalerDaoImpl() {
		super();
		setClazz(SalarySalerEntity.class);
	}

	@Override
	public Long save(SalarySalerEntity SalarySalerEntity) throws HibernateException {
		SalarySalerEntity = createEntity(SalarySalerEntity);
		return SalarySalerEntity.getId();
	}

	@Override
	public void updateEntity(SalarySalerEntity SalarySalerEntity) throws HibernateException {
		update(SalarySalerEntity);
	}

	@Override
	public List<SalarySalerEntity> list(String hql, List<Object> params) {
		return	find("from SalarySalerEntity " + hql, params);
	}

	@Override
	public List<SalarySalerEntity> queryByCondition(String orgId, Date startDate, Date endDate, String branchNos,
			String salerIds) {
		StringBuilder hqlSb = new StringBuilder("from SalarySalerEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		if(StringUtils.isNotEmpty(branchNos)) {
			hqlSb.append(" and branch_no in (:branch_no)");
			Set<String> branch_no = new HashSet<>(Arrays.asList(branchNos.split(",")));
			mParam.put("branch_no", branch_no);
		}
		if(StringUtils.isNotEmpty(salerIds)) {
			hqlSb.append(" and saler_id in (:saler_id)");
			Set<String> saler_id = new HashSet<>(Arrays.asList(salerIds.split(",")));
			mParam.put("saler_id", saler_id);
		}
		return find(hqlSb.toString(), mParam);
	}

	@Override
	public void updateConfirm(String orgId, String account, Date startDate, Date endDate, String branchNos,
			String salerIds) throws HibernateException {
		StringBuilder hqlSb = new StringBuilder("update SalarySalerEntity set confirm_flag = 1, confirm_man = :confirm_man, confirm_date = :confirm_date where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		mParam.put("confirm_man", account);
		mParam.put("confirm_date", new Date());
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		if(StringUtils.isNotEmpty(branchNos)) {
			hqlSb.append(" and branch_no in (:branch_no)");
			Set<String> branch_no = new HashSet<>(Arrays.asList(branchNos.split(",")));
			mParam.put("branch_no", branch_no);
		}
		if(StringUtils.isNotEmpty(salerIds)) {
			hqlSb.append(" and saler_id in (:saler_id)");
			Set<String> saler_id = new HashSet<>(Arrays.asList(salerIds.split(",")));
			mParam.put("saler_id", saler_id);
		}
		excuteByHql(hqlSb.toString(), mParam);
	}

}
