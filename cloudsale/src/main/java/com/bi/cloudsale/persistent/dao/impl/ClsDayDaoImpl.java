package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.BrandDayDao;
import com.bi.cloudsale.persistent.dao.ClsDayDao;
import com.bi.cloudsale.persistent.entity.BrandDayEntity;
import com.bi.cloudsale.persistent.entity.ClsDayEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class ClsDayDaoImpl extends BaseDao<ClsDayEntity> implements ClsDayDao {

	public ClsDayDaoImpl() {
		super();
		setClazz(ClsDayEntity.class);
	}

	@Override
	public Long save(ClsDayEntity clsDayEntity)  throws HibernateException {
		clsDayEntity = createEntity(clsDayEntity);
		return clsDayEntity.getId();
	}

	@Override
	public void updateEntity(ClsDayEntity clsDayEntity) throws HibernateException {
		update(clsDayEntity);
	}

	@Override
	public List<ClsDayEntity> list(String hql) {
		return	findByHql(hql);
	}
}
