package com.bi.cloudsale.persistent.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cs_base_branch_cls", catalog = "cloudsale")
public class BranchClsEntity implements Serializable{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String branch_clsno;
  private String branch_clsname;
  private String parent_clsno;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getBranch_clsno() {
    return branch_clsno;
  }

  public void setBranch_clsno(String branch_clsno) {
    this.branch_clsno = branch_clsno;
  }

  public String getBranch_clsname() {
    return branch_clsname;
  }

  public void setBranch_clsname(String branch_clsname) {
    this.branch_clsname = branch_clsname;
  }

  public String getParent_clsno() {
    return parent_clsno;
  }

  public void setParent_clsno(String parent_clsno) {
    this.parent_clsno = parent_clsno;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
