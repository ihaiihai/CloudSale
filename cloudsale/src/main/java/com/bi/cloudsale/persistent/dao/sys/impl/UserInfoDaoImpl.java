package com.bi.cloudsale.persistent.dao.sys.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.RemoteInfoDto;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;

@Repository
@Transactional
public class UserInfoDaoImpl extends BaseDao<UserInfoEntity> implements UserInfoDao {

	public UserInfoDaoImpl() {
		super();
		setClazz(UserInfoEntity.class);
	}
	@Override
	public UserInfoEntity queryByUserId(String userId) {
		StringBuilder hqlSb = new StringBuilder("from UserInfoEntity where userId = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(userId);
		return findOne(hqlSb.toString(), params);
	}
	@Override
	public List<UserInfoEntity> queryByMobile(String mobile) {
		StringBuilder hqlSb = new StringBuilder("from UserInfoEntity where mobile = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(mobile);
		return find(hqlSb.toString(), params);
	}

	@Override
	public UserInfoEntity queryByAccount(String account) {
		StringBuilder hqlSb = new StringBuilder("from UserInfoEntity where account = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(account);
		return findOne(hqlSb.toString(), params);
	}

	@Override
	public void createUser(UserInfoEntity userInfoEntity) throws HibernateException{
		create(userInfoEntity);
	}

	@Override
	public void deleteUser(String userId) throws HibernateException {
		UserInfoEntity userInfoEntity = this.queryByUserId(userId);
		this.delete(userInfoEntity);
	}

	@Override
	public void updateUser(UserInfoEntity userInfoEntity) throws HibernateException {
		this.update(userInfoEntity);
	}

	@Override
	public List<UserInfoEntity> queryByCondition(BaseQueryCondition baseQueryCondition) {
		StringBuilder hqlSb = new StringBuilder("select new UserInfoEntity(u.userId, u.account, u.realName, u.mobile, u.gender, u.organizeName, u.description, u.areaName, r.roleName) from UserInfoEntity u, RoleInfoEntity r where u.roleCode = r.roleCode");
		Map<String,Object> mParams = new HashMap<String,Object>();
		if(!StringUtils.isEmpty(baseQueryCondition.getUserId())) {
			hqlSb.append(" and u.parentUserId = :parentUserId ");
			mParams.put("parentUserId", baseQueryCondition.getUserId());
		}
		if(!StringUtils.isEmpty(baseQueryCondition.getOrgId())) {
			hqlSb.append(" and u.organizeId = :orgId ");
			mParams.put("orgId", baseQueryCondition.getOrgId());
		}
		if(!StringUtils.isEmpty(baseQueryCondition.getKey())) {
			hqlSb.append(" and (u.realName like :realName or u.mobile = :mobile)");
			mParams.put("realName", "%" + baseQueryCondition.getKey() + "%");
			mParams.put("mobile", baseQueryCondition.getKey());
		}
		return find(hqlSb.toString(), mParams);
	}

	@Override
	public List<UserInfoEntity> queryByOrgId(String orgId) {
		return findByHql("where organize_id='"+orgId+"'");
	}
	@Override
	public void updateRemoteInfo(RemoteInfoDto dto) throws HibernateException {
		String hql = "update UserInfoEntity set remoteUrl = ?, remotePort = ? where organizeId = ?";
		List<Object> params = new ArrayList<Object>();
		params.add(dto.getRemoteUrl());
		params.add(dto.getRemotePort());
		params.add(dto.getOrgId());
		excuteByHql(hql, params);
	}
}
