package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.ClientSaveDto;
import com.bi.cloudsale.persistent.dao.ClientSaveDao;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Transactional
public class ClientSaveDaoImpl extends BaseDao<Class> implements ClientSaveDao {

	@Override
	public Class getTableClassByTableName(String tableName){
		Map<String,Class> tableClasses = getTables();
		Class tableClass = tableClasses.get(tableName);
		return tableClass;
	}
}
