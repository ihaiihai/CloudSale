
package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;
@Entity
@Table(name = "cs_deduct_rule_range", catalog = "cloudsale")
public class DeductRuleRangeEntity implements Serializable{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String deduct_no;
  private String branch_no;
  private String sale_id;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
    return org_id;
  }

  public void setOrg_id(String org_id) {
    this.org_id = org_id;
  }

  public String getDeduct_no() {
    return deduct_no;
  }

  public void setDeduct_no(String deduct_no) {
    this.deduct_no = deduct_no;
  }

  public String getBranch_no() {
    return branch_no;
  }

  public void setBranch_no(String branch_no) {
    this.branch_no = branch_no;
  }

  public String getSale_id() {
    return sale_id;
  }

  public void setSale_id(String sale_id) {
    this.sale_id = sale_id;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
