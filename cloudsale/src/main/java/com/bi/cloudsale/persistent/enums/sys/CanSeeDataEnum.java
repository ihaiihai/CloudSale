package com.bi.cloudsale.persistent.enums.sys;

public enum CanSeeDataEnum {

	NONE("", 0),
	PERSON("仅看个人", 1),
	BRANCH("仅看本门店", 2),
	AREA("仅看本区域", 3),
	COMPANY("仅看本公司", 4);

	private final String name;  
    private final Integer value;  
  
    public String getName() {  
        return name;  
    }  
  
    public Integer getValue() {  
        return value;  
    }  
  
    CanSeeDataEnum(String name, Integer value) {  
        this.name = name;  
        this.value = value;  
    }
    
	public static CanSeeDataEnum valueOf(Integer value) {
		for (CanSeeDataEnum item : CanSeeDataEnum.values()) {
			if (item.getValue() == value) {
				return item;
			}
		}
		return NONE;
	}
}
