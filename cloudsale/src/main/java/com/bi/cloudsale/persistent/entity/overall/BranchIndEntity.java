package com.bi.cloudsale.persistent.entity.overall;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cs_busi_branch_ind", catalog = "cloudsale")
public class BranchIndEntity implements Serializable {

	private static final long serialVersionUID = -776248916399062827L;

	private Long id;
	private String user_id;
	private String org_id;
	private String branch_no;
	private Double consume_saler;
	private Double consume_per;
	private Double consume_area;
	private Double consume_day;
	private Double bill_day;
	private Double vip_rate;
	private Double consume_vip;
	private Double dis_rate;
	private Double ret_rate;
	private Double trans_value;
	private Date gmt_create;
	private Date gmt_modified;
	
	private Double area;
	
	public BranchIndEntity() {
		
	}
	
	public BranchIndEntity(String branch_no, Double consume_saler, Double consume_per, Double consume_area,
			Double consume_day, Double bill_day, Double vip_rate, Double consume_vip, Double dis_rate, Double ret_rate,
			Double trans_value, Double area) {
		super();
		this.branch_no = branch_no;
		this.consume_saler = consume_saler;
		this.consume_per = consume_per;
		this.consume_area = consume_area;
		this.consume_day = consume_day;
		this.bill_day = bill_day;
		this.vip_rate = vip_rate;
		this.consume_vip = consume_vip;
		this.dis_rate = dis_rate;
		this.ret_rate = ret_rate;
		this.trans_value = trans_value;
		this.area = area;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getOrg_id() {
		return org_id;
	}
	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}
	public String getBranch_no() {
		return branch_no;
	}
	public void setBranch_no(String branch_no) {
		this.branch_no = branch_no;
	}
	public Double getConsume_saler() {
		return consume_saler;
	}
	public void setConsume_saler(Double consume_saler) {
		this.consume_saler = consume_saler;
	}
	public Double getConsume_per() {
		return consume_per;
	}
	public void setConsume_per(Double consume_per) {
		this.consume_per = consume_per;
	}
	public Double getConsume_area() {
		return consume_area;
	}
	public void setConsume_area(Double consume_area) {
		this.consume_area = consume_area;
	}
	public Double getConsume_day() {
		return consume_day;
	}
	public void setConsume_day(Double consume_day) {
		this.consume_day = consume_day;
	}
	public Double getBill_day() {
		return bill_day;
	}
	public void setBill_day(Double bill_day) {
		this.bill_day = bill_day;
	}
	public Double getVip_rate() {
		return vip_rate;
	}
	public void setVip_rate(Double vip_rate) {
		this.vip_rate = vip_rate;
	}
	public Double getConsume_vip() {
		return consume_vip;
	}
	public void setConsume_vip(Double consume_vip) {
		this.consume_vip = consume_vip;
	}
	public Double getDis_rate() {
		return dis_rate;
	}
	public void setDis_rate(Double dis_rate) {
		this.dis_rate = dis_rate;
	}
	public Double getRet_rate() {
		return ret_rate;
	}
	public void setRet_rate(Double ret_rate) {
		this.ret_rate = ret_rate;
	}
	public Double getTrans_value() {
		return trans_value;
	}
	public void setTrans_value(Double trans_value) {
		this.trans_value = trans_value;
	}
	public Date getGmt_create() {
		return gmt_create;
	}
	public void setGmt_create(Date gmt_create) {
		this.gmt_create = gmt_create;
	}
	public Date getGmt_modified() {
		return gmt_modified;
	}
	public void setGmt_modified(Date gmt_modified) {
		this.gmt_modified = gmt_modified;
	}
	public Double getArea() {
		return area;
	}
	public void setArea(Double area) {
		this.area = area;
	}
}
