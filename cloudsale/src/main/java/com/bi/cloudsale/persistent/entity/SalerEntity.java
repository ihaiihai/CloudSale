package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "cs_base_saler_info", catalog = "cloudsale")
public class SalerEntity implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String branch_no;
  private String sale_id;
  private String sale_name;
  private String other1;
  private String other2;
  private String other3;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getBranch_no() {
    return branch_no;
  }

  public void setBranch_no(String branch_no) {
    this.branch_no = branch_no;
  }

  public String getSale_id() {
    return sale_id;
  }

  public void setSale_id(String sale_id) {
    this.sale_id = sale_id;
  }

  public String getSale_name() {
    return sale_name;
  }

  public void setSale_name(String sale_name) {
    this.sale_name = sale_name;
  }

  public String getOther1() {
    return other1;
  }

  public void setOther1(String other1) {
    this.other1 = other1;
  }

  public String getOther2() {
    return other2;
  }

  public void setOther2(String other2) {
    this.other2 = other2;
  }

  public String getOther3() {
    return other3;
  }

  public void setOther3(String other3) {
    this.other3 = other3;
  }

  public Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
