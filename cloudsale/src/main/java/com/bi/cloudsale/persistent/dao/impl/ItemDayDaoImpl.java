package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.ItemDayDao;
import com.bi.cloudsale.persistent.entity.ItemDayEntity;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class ItemDayDaoImpl extends BaseDao<ItemDayEntity> implements ItemDayDao {

	public ItemDayDaoImpl() {
		super();
		setClazz(ItemDayEntity.class);
	}

	@Override
	public Long save(ItemDayEntity itemDayEntity)  throws HibernateException {
		itemDayEntity = createEntity(itemDayEntity);
		return itemDayEntity.getId();
	}

	@Override
	public void updateEntity(ItemDayEntity itemDayEntity) throws HibernateException {
		update(itemDayEntity);
	}

	@Override
	public List<ItemDayEntity> list(String hql) {
		return	findByHql(hql);
	}

	@Override
	public List<ItemDayEntity> list(String hql,Integer limit) {
		Query query = getCurrentSession().createQuery("from " + ItemDayEntity.class.getName() + " " + hql);
		query.setMaxResults(limit);
		return	query.list();
	}
}
