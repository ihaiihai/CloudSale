package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_sale_vip_day", catalog = "cloudsale")
public class VipDayEntity implements Serializable{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String card_id;
  private Long vip_yearsold;
  private String branch_no;
  private java.sql.Timestamp oper_date;
  private Double sale_qty;
  private Double ret_qty;
  private Double giv_qty;
  private Double sale_amt;
  private Double ret_amt;
  private Double giv_amt;
  private Double dis_amt;
  private Double cost_amt;
  private Double profit_amt;
  private Double sub_qty;
  private Double sub_amt;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getCard_id() {
    return card_id;
  }

  public void setCard_id(String card_id) {
    this.card_id = card_id;
  }

  public Long getVip_yearsold() {
    return vip_yearsold;
  }

  public void setVip_yearsold(Long vip_yearsold) {
    this.vip_yearsold = vip_yearsold;
  }

  public String getBranch_no() {
    return branch_no;
  }

  public void setBranch_no(String branch_no) {
    this.branch_no = branch_no;
  }

  public java.sql.Timestamp getOper_date() {
    return oper_date;
  }

  public void setOper_date(java.sql.Timestamp oper_date) {
    this.oper_date = oper_date;
  }

  public Double getSale_qty() {
    return sale_qty;
  }

  public void setSale_qty(Double sale_qty) {
    this.sale_qty = sale_qty;
  }

  public Double getRet_qty() {
    return ret_qty;
  }

  public void setRet_qty(Double ret_qty) {
    this.ret_qty = ret_qty;
  }

  public Double getGiv_qty() {
    return giv_qty;
  }

  public void setGiv_qty(Double giv_qty) {
    this.giv_qty = giv_qty;
  }

  public Double getSale_amt() {
    return sale_amt;
  }

  public void setSale_amt(Double sale_amt) {
    this.sale_amt = sale_amt;
  }

  public Double getRet_amt() {
    return ret_amt;
  }

  public void setRet_amt(Double ret_amt) {
    this.ret_amt = ret_amt;
  }

  public Double getGiv_amt() {
    return giv_amt;
  }

  public void setGiv_amt(Double giv_amt) {
    this.giv_amt = giv_amt;
  }

  public Double getDis_amt() {
    return dis_amt;
  }

  public void setDis_amt(Double dis_amt) {
    this.dis_amt = dis_amt;
  }

  public Double getCost_amt() {
    return cost_amt;
  }

  public void setCost_amt(Double cost_amt) {
    this.cost_amt = cost_amt;
  }

  public Double getProfit_amt() {
    return profit_amt;
  }

  public void setProfit_amt(Double profit_amt) {
    this.profit_amt = profit_amt;
  }

  public Double getSub_qty() {
    return sub_qty;
  }

  public void setSub_qty(Double sub_qty) {
    this.sub_qty = sub_qty;
  }

  public Double getSub_amt() {
    return sub_amt;
  }

  public void setSub_amt(Double sub_amt) {
    this.sub_amt = sub_amt;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
