package com.bi.cloudsale.persistent.dao.sys.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.persistent.dao.sys.RoleInfoDao;
import com.bi.cloudsale.persistent.entity.sys.RoleInfoEntity;

@Repository
@Transactional
public class RoleInfoDaoImpl extends BaseDao<RoleInfoEntity> implements RoleInfoDao {

	@Override
	public RoleInfoEntity queryByCode(String roleCode) {
		StringBuilder hqlSb = new StringBuilder("from RoleInfoEntity where roleCode = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(roleCode);
		return findOne(hqlSb.toString(), params);
	}

	@Override
	public void createRoleInfo(RoleInfoEntity roleInfoEntity) throws HibernateException {
		create(roleInfoEntity);
	}

	@Override
	public void deleteRoleInfo(String roleCode) throws HibernateException {
		RoleInfoEntity roleInfoEntity = this.queryByCode(roleCode);
		this.delete(roleInfoEntity);
	}

	@Override
	public void updateRoleInfo(RoleInfoEntity roleInfoEntity) throws HibernateException {
		this.update(roleInfoEntity);
	}

	@Override
	public List<RoleInfoEntity> queryByCondition(BaseQueryCondition baseQueryCondition) {
		StringBuilder hqlSb = new StringBuilder("from RoleInfoEntity");
		StringBuilder whereSb = new StringBuilder();
		Map<String,Object> mParams = new HashMap<String,Object>();
		if(!StringUtils.isEmpty(baseQueryCondition.getUserId())) {
			whereSb.append(" and userId = :userId ");
			mParams.put("userId", baseQueryCondition.getUserId());
		}
		if(!StringUtils.isEmpty(baseQueryCondition.getKey())) {
			whereSb.append(" and roleName like :roleName ");
			mParams.put("roleName", "%" + baseQueryCondition.getKey() + "%");
		}
		if(whereSb.length() > 0) {
			String whereStr = whereSb.toString();
			whereStr = whereStr.replaceFirst(" and", " where");
			hqlSb.append(whereStr);
		}
		return find(hqlSb.toString(), mParams);
	}

}
