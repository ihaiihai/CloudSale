package com.bi.cloudsale.persistent.dao.vip;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.bi.cloudsale.persistent.entity.vip.SaleVipDayEntity;

public interface SaleVipDayDao {

	List<SaleVipDayEntity> querySum(String userId, String orgId, Set<String> branchNo, Date startDate, Date endDate);
}
