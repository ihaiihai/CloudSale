package com.bi.cloudsale.persistent.dao.overall;

import java.util.List;

import org.hibernate.HibernateException;

import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.persistent.entity.overall.BranchIndEntity;

public interface OperationDao {

	void createBranchInd(BranchIndEntity branchIndEntity) throws HibernateException;
	
	void deleteBranchInd(String orgId, String branchNo) throws HibernateException;
	
	void updateBranchInd(BranchIndEntity branchIndEntity) throws HibernateException;
	
	List<BranchIndEntity> queryByCondition(BaseQueryCondition baseQueryCondition);
	
	BranchIndEntity queryByCode(String orgId, String branchNo);
	
	List<BranchIndEntity> queryByOrgId(String orgId);
}
