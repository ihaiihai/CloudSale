package com.bi.cloudsale.persistent.entity.item;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cs_base_item_info", catalog = "cloudsale")
public class BaseItemInfoEntity implements Serializable {

	private static final long serialVersionUID = -8958735528570009470L;
	
	private Long id;
	private String user_id;
	private String org_id;
	private String item_no;
	private String item_subno;
	private String item_name;
	private String item_clsno;
	private String item_brandno;
	private String unit_no;
	private String item_size;
	private String product_area;
	private String supcust_no;
	private String supcust_name;
	private Double price;
	private Double sale_price;
	private Double vip_price;
	private Double base_price;
	private Double trans_price;
	private Long year_id;
	private Long season_id;
	private String other1;
	private String other2;
	private String other3;
	private Date gmt_create;
	private Date gmt_modified;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getOrg_id() {
		return org_id;
	}
	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}
	public String getItem_no() {
		return item_no;
	}
	public void setItem_no(String item_no) {
		this.item_no = item_no;
	}
	public String getItem_subno() {
		return item_subno;
	}
	public void setItem_subno(String item_subno) {
		this.item_subno = item_subno;
	}
	public String getItem_name() {
		return item_name;
	}
	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}
	public String getItem_clsno() {
		return item_clsno;
	}
	public void setItem_clsno(String item_clsno) {
		this.item_clsno = item_clsno;
	}
	public String getItem_brandno() {
		return item_brandno;
	}
	public void setItem_brandno(String item_brandno) {
		this.item_brandno = item_brandno;
	}
	public String getUnit_no() {
		return unit_no;
	}
	public void setUnit_no(String unit_no) {
		this.unit_no = unit_no;
	}
	public String getItem_size() {
		return item_size;
	}
	public void setItem_size(String item_size) {
		this.item_size = item_size;
	}
	public String getProduct_area() {
		return product_area;
	}
	public void setProduct_area(String product_area) {
		this.product_area = product_area;
	}
	public String getSupcust_no() {
		return supcust_no;
	}
	public void setSupcust_no(String supcust_no) {
		this.supcust_no = supcust_no;
	}
	public String getSupcust_name() {
		return supcust_name;
	}
	public void setSupcust_name(String supcust_name) {
		this.supcust_name = supcust_name;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public Double getSale_price() {
		return sale_price;
	}
	public void setSale_price(Double sale_price) {
		this.sale_price = sale_price;
	}
	public Double getVip_price() {
		return vip_price;
	}
	public void setVip_price(Double vip_price) {
		this.vip_price = vip_price;
	}
	public Double getBase_price() {
		return base_price;
	}
	public void setBase_price(Double base_price) {
		this.base_price = base_price;
	}
	public Double getTrans_price() {
		return trans_price;
	}
	public void setTrans_price(Double trans_price) {
		this.trans_price = trans_price;
	}
	public Long getYear_id() {
		return year_id;
	}
	public void setYear_id(Long year_id) {
		this.year_id = year_id;
	}
	public Long getSeason_id() {
		return season_id;
	}
	public void setSeason_id(Long season_id) {
		this.season_id = season_id;
	}
	public String getOther1() {
		return other1;
	}
	public void setOther1(String other1) {
		this.other1 = other1;
	}
	public String getOther2() {
		return other2;
	}
	public void setOther2(String other2) {
		this.other2 = other2;
	}
	public String getOther3() {
		return other3;
	}
	public void setOther3(String other3) {
		this.other3 = other3;
	}
	public Date getGmt_create() {
		return gmt_create;
	}
	public void setGmt_create(Date gmt_create) {
		this.gmt_create = gmt_create;
	}
	public Date getGmt_modified() {
		return gmt_modified;
	}
	public void setGmt_modified(Date gmt_modified) {
		this.gmt_modified = gmt_modified;
	}
}
