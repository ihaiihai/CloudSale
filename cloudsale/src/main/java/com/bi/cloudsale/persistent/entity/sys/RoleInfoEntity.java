package com.bi.cloudsale.persistent.entity.sys;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cs_sys_role_info", catalog = "cloudsale")
public class RoleInfoEntity implements Serializable {

	private static final long serialVersionUID = -2869882747121349274L;
	
	private Long id;
	private String userId;
	private String orgId;
	private String roleCode;
	private String roleName;
	private Integer canSeeData;
	private Integer canSeeProfit;
	private String description;
	private Date gmtCreate;
	private Date gmtModified;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Basic(optional = false)
	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Basic(optional = false)
	@Column(name = "org_id")
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	
	@Basic(optional = false)
	@Column(name = "role_code")
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	
	@Basic(optional = false)
	@Column(name = "role_name")
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	@Basic(optional = false)
	@Column(name = "can_see_data")
	public Integer getCanSeeData() {
		return canSeeData;
	}
	public void setCanSeeData(Integer canSeeData) {
		this.canSeeData = canSeeData;
	}
	
	@Basic(optional = false)
	@Column(name = "can_see_profit")
	public Integer getCanSeeProfit() {
		return canSeeProfit;
	}
	public void setCanSeeProfit(Integer canSeeProfit) {
		this.canSeeProfit = canSeeProfit;
	}
	
	@Basic(optional = true)
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Basic(optional = false)
	@Column(name = "gmt_create")
	public Date getGmtCreate() {
		return gmtCreate;
	}
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	
	@Basic(optional = false)
	@Column(name = "gmt_modified")
	public Date getGmtModified() {
		return gmtModified;
	}
	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}
}
