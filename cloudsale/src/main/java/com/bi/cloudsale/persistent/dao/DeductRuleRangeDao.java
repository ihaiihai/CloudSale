package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.DeductRuleRangeEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface DeductRuleRangeDao {

	List<DeductRuleRangeEntity> findByDeductNo(String deductNo) throws HibernateException;

	void deleteByDeductNo(String deductNo) throws HibernateException;

	Long save(DeductRuleRangeEntity deductRuleRangeEntity)  throws HibernateException;
}
