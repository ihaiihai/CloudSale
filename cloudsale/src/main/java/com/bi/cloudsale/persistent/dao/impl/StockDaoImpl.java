package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.StockDao;
import com.bi.cloudsale.persistent.entity.StockEntity;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Repository
@Transactional
public class StockDaoImpl extends BaseDao<StockEntity> implements StockDao {

	public StockDaoImpl() {
		super();
		setClazz(StockEntity.class);
	}

	@Override
	public Long save(StockEntity stockEntity)  throws HibernateException {
		stockEntity = createEntity(stockEntity);
		return stockEntity.getId();
	}

	@Override
	public void updateEntity(StockEntity stockEntity) throws HibernateException {
		update(stockEntity);
	}

	@Override
	public List<StockEntity> list(String hql) {
		return	findByHql(hql);

	}

	@Override
	public List<StockEntity> list(String hql,Integer limit) {
		Query query = getCurrentSession().createQuery("from " + StockEntity.class.getName() + " " + hql);
		query.setMaxResults(limit);
		return	query.list();
	}

	@Override
	public List<Object> listBySql(String sql,Integer limit) {
		Query query = getCurrentSession().createQuery(sql);
		query.setMaxResults(limit);
		return	query.list();
	}

	@Override
	public List<Object> listBySql(String sql) {
			return getCurrentSession().createQuery(sql).list();
	}
}
