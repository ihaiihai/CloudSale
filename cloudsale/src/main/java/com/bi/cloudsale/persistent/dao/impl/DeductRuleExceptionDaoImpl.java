package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.persistent.dao.DeductRuleExceptionDao;
import com.bi.cloudsale.persistent.entity.DeductRuleExceptionEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class DeductRuleExceptionDaoImpl extends BaseDao<DeductRuleExceptionEntity> implements DeductRuleExceptionDao {

	public DeductRuleExceptionDaoImpl() {
		super();
		setClazz(DeductRuleExceptionEntity.class);
	}

	@Override
	public List<DeductRuleExceptionEntity> findByDeductNo(String deductNo) throws HibernateException {
		List<DeductRuleExceptionEntity> deductRuleExceptionEntities = findByHql(" where deduct_no = '"+deductNo+"'");
		return deductRuleExceptionEntities;
	}

	@Override
	public void deleteByDeductNo(String deductNo) throws HibernateException {
		excuteBySql("delete from cs_deduct_rule_exception where deduct_no = '"+deductNo+"'");
	}

	@Override
	public Long save(DeductRuleExceptionEntity deductRuleExceptionEntity)  throws HibernateException {
		deductRuleExceptionEntity.setGmt_create(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleExceptionEntity.setGmt_modified(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleExceptionEntity = createEntity(deductRuleExceptionEntity);
		return deductRuleExceptionEntity.getId();
	}
}
