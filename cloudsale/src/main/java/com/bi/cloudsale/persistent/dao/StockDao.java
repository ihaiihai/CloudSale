package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.StockEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface StockDao {

	Long save(StockEntity stockEntity) throws HibernateException;

	void updateEntity(StockEntity stockEntity) throws HibernateException;

	List<StockEntity> list(String hql);

	List<StockEntity> list(String hql,Integer limit);

	List<Object> listBySql(String sql);

	List<Object> listBySql(String sql,Integer limit);
}
