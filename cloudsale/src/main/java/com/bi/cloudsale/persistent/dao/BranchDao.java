package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.BranchEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface BranchDao {

	Long save(BranchEntity branchEntity) throws HibernateException;

	void updateEntity(BranchEntity branchEntity) throws HibernateException;

	List<BranchEntity> list(String hql);
}
