package com.bi.cloudsale.persistent.dao.saler.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.saler.SalerInfoDao;
import com.bi.cloudsale.persistent.entity.saler.SalerInfoEntity;

@Repository
@Transactional
public class SalerInfoDaoImpl extends BaseDao<SalerInfoEntity> implements SalerInfoDao {


}
