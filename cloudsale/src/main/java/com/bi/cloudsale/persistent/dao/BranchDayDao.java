package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.BranchDayEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface BranchDayDao {

	Long save(BranchDayEntity branchDayEntity) throws HibernateException;

	void updateEntity(BranchDayEntity branchDayEntity) throws HibernateException;

	List<BranchDayEntity> list( String hql);
	
	long querySaleAmt(String orgId, String startDate, String endDate);
}
