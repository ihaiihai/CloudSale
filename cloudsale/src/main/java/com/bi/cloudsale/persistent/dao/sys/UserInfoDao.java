package com.bi.cloudsale.persistent.dao.sys;

import java.util.List;

import org.hibernate.HibernateException;

import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.RemoteInfoDto;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;

public interface UserInfoDao {
	
	UserInfoEntity queryByUserId(String userId);

	UserInfoEntity queryByAccount(String account);

	List<UserInfoEntity> queryByMobile(String mobile);
	
	void createUser(UserInfoEntity userInfoEntity) throws HibernateException;
	
	void deleteUser(String userId) throws HibernateException;
	
	void updateUser(UserInfoEntity userInfoEntity) throws HibernateException;
	
	List<UserInfoEntity> queryByCondition(BaseQueryCondition baseQueryCondition);

	List<UserInfoEntity> queryByOrgId(String orgId);
	
	void updateRemoteInfo(RemoteInfoDto dto) throws HibernateException;
}
