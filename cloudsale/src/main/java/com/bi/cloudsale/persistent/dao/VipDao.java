package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.VipEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface VipDao {

	Long save(VipEntity vipEntity) throws HibernateException;

	void updateEntity(VipEntity vipEntity) throws HibernateException;

	List<VipEntity> list(String hql);

	List<Object> listBySql(String sql);
}
