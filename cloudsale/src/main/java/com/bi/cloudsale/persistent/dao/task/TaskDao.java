package com.bi.cloudsale.persistent.dao.task;

import java.util.List;

import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.persistent.entity.task.SaleAimEntity;

public interface TaskDao {

	List<SaleAimEntity> queryByCondition(TaskQueryCondition condition);
}
