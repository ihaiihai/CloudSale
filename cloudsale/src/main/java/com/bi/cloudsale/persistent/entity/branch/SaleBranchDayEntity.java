package com.bi.cloudsale.persistent.entity.branch;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cs_sale_branch_day", catalog = "cloudsale")
public class SaleBranchDayEntity implements Serializable {

	private static final long serialVersionUID = 3035061488359399496L;
	
	private Long id;
    private String user_id;
    private String org_id;
    private String branch_no;
    private Date oper_date;
    private Double sale_qty;
    private Double ret_qty;
    private Double giv_qty;
    private Double sale_amt;
    private Double ret_amt;
    private Double giv_amt;
    private Double dis_amt;
    private Double cost_amt;
    private Double profit_amt;
    private Double sub_qty;
    private Double sub_amt;
    private Long client_count;
    private Double client_amt;
    private Double client_qty;
    private Long vip_new;
    private Double consume_new;
    private Double consume_old;
    private Long bill_cnt;
    private Long bill_vip_cnt;
    private Date gmt_create;
    private Date gmt_modified;
	
	public SaleBranchDayEntity(String branch_no, Double sale_amt) {
		this.branch_no = branch_no;
		this.sale_amt = sale_amt;
	}
	
	public SaleBranchDayEntity(String branch_no, Double sale_amt, Double ret_qty, Double sale_qty, 
			Double dis_amt, Long bill_cnt, Long bill_vip_cnt) {
		this.branch_no = branch_no;
		this.sale_amt = sale_amt;
		this.ret_qty = ret_qty;
		this.sale_qty = sale_qty;
		this.dis_amt = dis_amt;
		this.bill_cnt = bill_cnt;
		this.bill_vip_cnt = bill_vip_cnt;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getOrg_id() {
		return org_id;
	}

	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}

	public String getBranch_no() {
		return branch_no;
	}

	public void setBranch_no(String branch_no) {
		this.branch_no = branch_no;
	}

	public Date getOper_date() {
		return oper_date;
	}

	public void setOper_date(Date oper_date) {
		this.oper_date = oper_date;
	}

	public Double getSale_qty() {
		return sale_qty;
	}

	public void setSale_qty(Double sale_qty) {
		this.sale_qty = sale_qty;
	}

	public Double getRet_qty() {
		return ret_qty;
	}

	public void setRet_qty(Double ret_qty) {
		this.ret_qty = ret_qty;
	}

	public Double getGiv_qty() {
		return giv_qty;
	}

	public void setGiv_qty(Double giv_qty) {
		this.giv_qty = giv_qty;
	}

	public Double getSale_amt() {
		return sale_amt;
	}

	public void setSale_amt(Double sale_amt) {
		this.sale_amt = sale_amt;
	}

	public Double getRet_amt() {
		return ret_amt;
	}

	public void setRet_amt(Double ret_amt) {
		this.ret_amt = ret_amt;
	}

	public Double getGiv_amt() {
		return giv_amt;
	}

	public void setGiv_amt(Double giv_amt) {
		this.giv_amt = giv_amt;
	}

	public Double getDis_amt() {
		return dis_amt;
	}

	public void setDis_amt(Double dis_amt) {
		this.dis_amt = dis_amt;
	}

	public Double getCost_amt() {
		return cost_amt;
	}

	public void setCost_amt(Double cost_amt) {
		this.cost_amt = cost_amt;
	}

	public Double getProfit_amt() {
		return profit_amt;
	}

	public void setProfit_amt(Double profit_amt) {
		this.profit_amt = profit_amt;
	}

	public Double getSub_qty() {
		return sub_qty;
	}

	public void setSub_qty(Double sub_qty) {
		this.sub_qty = sub_qty;
	}

	public Double getSub_amt() {
		return sub_amt;
	}

	public void setSub_amt(Double sub_amt) {
		this.sub_amt = sub_amt;
	}

	public Long getClient_count() {
		return client_count;
	}

	public void setClient_count(Long client_count) {
		this.client_count = client_count;
	}

	public Double getClient_amt() {
		return client_amt;
	}

	public void setClient_amt(Double client_amt) {
		this.client_amt = client_amt;
	}

	public Double getClient_qty() {
		return client_qty;
	}

	public void setClient_qty(Double client_qty) {
		this.client_qty = client_qty;
	}

	public Long getVip_new() {
		return vip_new;
	}

	public void setVip_new(Long vip_new) {
		this.vip_new = vip_new;
	}

	public Double getConsume_new() {
		return consume_new;
	}

	public void setConsume_new(Double consume_new) {
		this.consume_new = consume_new;
	}

	public Double getConsume_old() {
		return consume_old;
	}

	public void setConsume_old(Double consume_old) {
		this.consume_old = consume_old;
	}

	public Long getBill_cnt() {
		return bill_cnt;
	}

	public void setBill_cnt(Long bill_cnt) {
		this.bill_cnt = bill_cnt;
	}

	public Long getBill_vip_cnt() {
		return bill_vip_cnt;
	}

	public void setBill_vip_cnt(Long bill_vip_cnt) {
		this.bill_vip_cnt = bill_vip_cnt;
	}

	public Date getGmt_create() {
		return gmt_create;
	}

	public void setGmt_create(Date gmt_create) {
		this.gmt_create = gmt_create;
	}

	public Date getGmt_modified() {
		return gmt_modified;
	}

	public void setGmt_modified(Date gmt_modified) {
		this.gmt_modified = gmt_modified;
	}


}
