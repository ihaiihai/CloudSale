package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.persistent.dao.DeductRuleRangeDao;
import com.bi.cloudsale.persistent.entity.DeductRuleRangeEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class DeductRuleRangeDaoImpl extends BaseDao<DeductRuleRangeEntity> implements DeductRuleRangeDao {

	public DeductRuleRangeDaoImpl() {
		super();
		setClazz(DeductRuleRangeEntity.class);
	}

	@Override
	public List<DeductRuleRangeEntity> findByDeductNo(String deductNo) throws HibernateException {
		List<DeductRuleRangeEntity> deductRuleRangeEntities = findByHql(" where deduct_no = '"+deductNo+"'");
		return deductRuleRangeEntities;
	}

	@Override
	public void deleteByDeductNo(String deductNo) throws HibernateException {
		excuteBySql("delete from cs_deduct_rule_range where deduct_no = '"+deductNo+"'");
	}

	@Override
	public Long save(DeductRuleRangeEntity deductRuleRangeEntity)  throws HibernateException {
		deductRuleRangeEntity.setGmt_create(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleRangeEntity.setGmt_modified(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleRangeEntity = createEntity(deductRuleRangeEntity);
		return deductRuleRangeEntity.getId();
	}
}
