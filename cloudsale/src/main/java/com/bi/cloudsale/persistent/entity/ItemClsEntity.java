package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_base_item_cls", catalog = "cloudsale")
public class ItemClsEntity implements Serializable{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String item_clsno;
  private String item_clsname;
  private String cls_parent;
  private Long flow_product;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;
  @Transient
  private Integer checked = 0;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getItem_clsno() {
    return item_clsno;
  }

  public void setItem_clsno(String item_clsno) {
    this.item_clsno = item_clsno;
  }

  public String getItem_clsname() {
    return item_clsname;
  }

  public void setItem_clsname(String item_clsname) {
    this.item_clsname = item_clsname;
  }

  public String getCls_parent() {
    return cls_parent;
  }

  public void setCls_parent(String cls_parent) {
    this.cls_parent = cls_parent;
  }

  public Long getFlow_product() {
    return flow_product;
  }

  public void setFlow_product(Long flow_product) {
    this.flow_product = flow_product;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }

	public Integer getChecked() {
		return checked;
	}
	
	public void setChecked(Integer checked) {
		this.checked = checked;
	}
}
