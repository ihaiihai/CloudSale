package com.bi.cloudsale.persistent.entity.sys;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cs_sys_user_info", catalog = "cloudsale")
public class UserInfoEntity implements Serializable {

	private static final long serialVersionUID = 7268184001554107850L;
	
	private Long id;
	private String userId;
	private String parentUserId;
	private String account;
	private String password;
	private String realName;
	private String mobile;
	private Integer gender;
	private String organizeId;
	private String organizeName;
	private String areaCode;
	private String areaName;
	private String roleCode;
	private String roleName;
	private Integer authTime;
	private Integer authUserCount;
	private String clientUrl;
	private String erpType;
	private String erpVersion;
	private Integer accountType;
	private String description;
	private String remoteUrl;
	private String remotePort;
	private Date gmtCreate;
	private Date gmtModified;
	
	public UserInfoEntity() {
		
	}
	
	public UserInfoEntity(String userId, String account, String realName, String mobile, Integer gender, 
			String organizeName, String description, String areaName, String roleName) {
		this.userId = userId;
		this.account = account;
		this.realName = realName;
		this.mobile = mobile;
		this.gender = gender;
		this.organizeName = organizeName;
		this.description = description;
		this.areaName = areaName;
		this.roleName = roleName;
	}
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	@Basic(optional = false)
	@Column(name = "user_id")
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	
	@Basic(optional = false)
	@Column(name = "parent_user_id")
	public String getParentUserId() {
		return parentUserId;
	}
	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}
	
	@Basic(optional = false)
	@Column(name = "account")
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	
	@Basic(optional = false)
	@Column(name = "password")
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	@Basic(optional = true)
	@Column(name = "real_name")
	public String getRealName() {
		return realName;
	}
	public void setRealName(String realName) {
		this.realName = realName;
	}
	
	@Basic(optional = true)
	@Column(name = "mobile")
	public String getMobile() {
		return mobile;
	}
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	@Basic(optional = true)
	@Column(name = "gender")
	public Integer getGender() {
		return gender;
	}
	public void setGender(Integer gender) {
		this.gender = gender;
	}
	
	@Basic(optional = false)
	@Column(name = "organize_id")
	public String getOrganizeId() {
		return organizeId;
	}
	public void setOrganizeId(String organizeId) {
		this.organizeId = organizeId;
	}
	
	@Basic(optional = true)
	@Column(name = "organize_name")
	public String getOrganizeName() {
		return organizeName;
	}
	public void setOrganizeName(String organizeName) {
		this.organizeName = organizeName;
	}
	
	@Basic(optional = true)
	@Column(name = "area_code")
	public String getAreaCode() {
		return areaCode;
	}
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	@Basic(optional = true)
	@Column(name = "area_name")
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
	@Basic(optional = true)
	@Column(name = "role_code")
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	
	@Basic(optional = true)
	@Column(name = "role_name")
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	
	@Basic(optional = true)
	@Column(name = "auth_time")
	public Integer getAuthTime() {
		return authTime;
	}
	public void setAuthTime(Integer authTime) {
		this.authTime = authTime;
	}
	
	@Basic(optional = true)
	@Column(name = "auth_user_count")
	public Integer getAuthUserCount() {
		return authUserCount;
	}
	public void setAuthUserCount(Integer authUserCount) {
		this.authUserCount = authUserCount;
	}
	
	@Basic(optional = true)
	@Column(name = "client_url")
	public String getClientUrl() {
		return clientUrl;
	}
	public void setClientUrl(String clientUrl) {
		this.clientUrl = clientUrl;
	}
	
	@Basic(optional = true)
	@Column(name = "erp_type")
	public String getErpType() {
		return erpType;
	}
	public void setErpType(String erpType) {
		this.erpType = erpType;
	}
	
	@Basic(optional = true)
	@Column(name = "erp_version")
	public String getErpVersion() {
		return erpVersion;
	}
	public void setErpVersion(String erpVersion) {
		this.erpVersion = erpVersion;
	}
	
	@Basic(optional = true)
	@Column(name = "account_type")
	public Integer getAccountType() {
		return accountType;
	}
	public void setAccountType(Integer accountType) {
		this.accountType = accountType;
	}
	
	@Basic(optional = true)
	@Column(name = "description")
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	@Basic(optional = false)
	@Column(name = "remote_url")
	public String getRemoteUrl() {
		return remoteUrl;
	}

	public void setRemoteUrl(String remoteUrl) {
		this.remoteUrl = remoteUrl;
	}

	@Basic(optional = false)
	@Column(name = "remote_port")
	public String getRemotePort() {
		return remotePort;
	}

	public void setRemotePort(String remotePort) {
		this.remotePort = remotePort;
	}

	@Basic(optional = false)
	@Column(name = "gmt_create")
	public Date getGmtCreate() {
		return gmtCreate;
	}
	public void setGmtCreate(Date gmtCreate) {
		this.gmtCreate = gmtCreate;
	}
	
	@Basic(optional = false)
	@Column(name = "gmt_modified")
	public Date getGmtModified() {
		return gmtModified;
	}
	public void setGmtModified(Date gmtModified) {
		this.gmtModified = gmtModified;
	}
	
}
