package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.ItemClsDao;
import com.bi.cloudsale.persistent.entity.ItemClsEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class ItemClsDaoImpl extends BaseDao<ItemClsEntity> implements ItemClsDao {

	public ItemClsDaoImpl() {
		super();
		setClazz(ItemClsEntity.class);
	}

	@Override
	public Long save(ItemClsEntity itemClsEntity)  throws HibernateException {
		itemClsEntity = createEntity(itemClsEntity);
		return itemClsEntity.getId();
	}

	@Override
	public void updateEntity(ItemClsEntity itemClsEntity) throws HibernateException {
		update(itemClsEntity);
	}

	@Override
	public List<ItemClsEntity> list(String hql) {
		return	findByHql(hql);
	}
}
