package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_deduct_rule_master", catalog = "cloudsale")
public class DeductRuleMasterEntity implements Serializable{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String deduct_no;
  private String deduct_name;
  private String deduct_desc;
  private java.sql.Timestamp begin_date;
  private java.sql.Timestamp end_date;
  private Integer mission_cycle;
  private String use_range;
  private String cal_type;
  private String cal_mode;
  private String deduct_rate;
  private String deduct_mode;
  private String full_mode;
  private Double min_money;
  private Double reward_money;
  private String reward_cal;
  private java.sql.Timestamp oper_date;
  private String oper_id;
  private java.sql.Timestamp work_date;
  private String confirm_id;
  private Long is_enable;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
    return org_id;
  }

  public void setOrg_id(String org_id) {
    this.org_id = org_id;
  }

  public String getDeduct_no() {
    return deduct_no;
  }

  public void setDeduct_no(String deduct_no) {
    this.deduct_no = deduct_no;
  }

  public String getDeduct_name() {
    return deduct_name;
  }

  public void setDeduct_name(String deduct_name) {
    this.deduct_name = deduct_name;
  }

  public String getDeduct_desc() {
    return deduct_desc;
  }

  public void setDeduct_desc(String deduct_desc) {
    this.deduct_desc = deduct_desc;
  }

  public java.sql.Timestamp getBegin_date() {
    return begin_date;
  }

  public void setBegin_date(java.sql.Timestamp begin_date) {
    this.begin_date = begin_date;
  }

  public java.sql.Timestamp getEnd_date() {
    return end_date;
  }

  public void setEnd_date(java.sql.Timestamp end_date) {
    this.end_date = end_date;
  }

  public Integer getMission_cycle() {
	return mission_cycle;
  }

  public void setMission_cycle(Integer mission_cycle) {
	this.mission_cycle = mission_cycle;
  }

	public String getUse_range() {
    return use_range;
  }

  public void setUse_range(String use_range) {
    this.use_range = use_range;
  }

  public String getCal_type() {
    return cal_type;
  }

  public void setCal_type(String cal_type) {
    this.cal_type = cal_type;
  }

  public String getCal_mode() {
    return cal_mode;
  }

  public void setCal_mode(String cal_mode) {
    this.cal_mode = cal_mode;
  }

  public String getDeduct_rate() {
    return deduct_rate;
  }

  public void setDeduct_rate(String deduct_rate) {
    this.deduct_rate = deduct_rate;
  }

  public String getDeduct_mode() {
    return deduct_mode;
  }

  public void setDeduct_mode(String deduct_mode) {
    this.deduct_mode = deduct_mode;
  }

  public String getFull_mode() {
    return full_mode;
  }

  public void setFull_mode(String full_mode) {
    this.full_mode = full_mode;
  }

  public Double getMin_money() {
    return min_money;
  }

  public void setMin_money(Double min_money) {
    this.min_money = min_money;
  }

  public Double getReward_money() {
    return reward_money;
  }

  public void setReward_money(Double reward_money) {
    this.reward_money = reward_money;
  }

  public String getReward_cal() {
    return reward_cal;
  }

  public void setReward_cal(String reward_cal) {
    this.reward_cal = reward_cal;
  }

  public java.sql.Timestamp getOper_date() {
    return oper_date;
  }

  public void setOper_date(java.sql.Timestamp oper_date) {
    this.oper_date = oper_date;
  }

  public String getOper_id() {
    return oper_id;
  }

  public void setOper_id(String oper_id) {
    this.oper_id = oper_id;
  }

  public java.sql.Timestamp getWork_date() {
    return work_date;
  }

  public void setWork_date(java.sql.Timestamp work_date) {
    this.work_date = work_date;
  }

  public String getConfirm_id() {
    return confirm_id;
  }

  public void setConfirm_id(String confirm_id) {
    this.confirm_id = confirm_id;
  }

  public Long getIs_enable() {
    return is_enable;
  }

  public void setIs_enable(Long is_enable) {
    this.is_enable = is_enable;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
