package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.BranchDayDao;
import com.bi.cloudsale.persistent.dao.BrandDayDao;
import com.bi.cloudsale.persistent.entity.BranchDayEntity;
import com.bi.cloudsale.persistent.entity.BrandDayEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class BrandDayDaoImpl extends BaseDao<BrandDayEntity> implements BrandDayDao {

	public BrandDayDaoImpl() {
		super();
		setClazz(BrandDayEntity.class);
	}

	@Override
	public Long save(BrandDayEntity brandDayEntity)  throws HibernateException {
		brandDayEntity = createEntity(brandDayEntity);
		return brandDayEntity.getId();
	}

	@Override
	public void updateEntity(BrandDayEntity brandDayEntity) throws HibernateException {
		update(brandDayEntity);
	}

	@Override
	public List<BrandDayEntity> list(String hql) {
		return	findByHql(hql);
	}
}
