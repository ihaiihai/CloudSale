package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.PriceZoneEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface PriceZoneDao {

	Long save(PriceZoneEntity priceZoneEntity) throws HibernateException;

	void updateEntity(PriceZoneEntity priceZoneEntity) throws HibernateException;

	List<PriceZoneEntity> list(String hql);
}
