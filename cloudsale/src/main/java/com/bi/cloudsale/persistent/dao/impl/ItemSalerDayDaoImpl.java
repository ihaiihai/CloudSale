package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.ItemSalerDayDao;
import com.bi.cloudsale.persistent.entity.ItemSalerDayEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class ItemSalerDayDaoImpl extends BaseDao<ItemSalerDayEntity> implements ItemSalerDayDao {

	public ItemSalerDayDaoImpl() {
		super();
		setClazz(ItemSalerDayEntity.class);
	}

	@Override
	public Long save(ItemSalerDayEntity itemSalerDayEntity)  throws HibernateException {
		itemSalerDayEntity = createEntity(itemSalerDayEntity);
		return itemSalerDayEntity.getId();
	}

	@Override
	public void updateEntity(ItemSalerDayEntity itemSalerDayEntity) throws HibernateException {
		update(itemSalerDayEntity);
	}

	@Override
	public List<ItemSalerDayEntity> list(String hql) {
		return	findByHql(hql);
	}
}
