package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_base_price_zone", catalog = "cloudsale")
public class PriceZoneEntity implements Serializable{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String branch_no;
  private String price_zone_no;
  private String price_zone_name;
  private Double price_min;
  private Double price_max;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getBranch_no() {
    return branch_no;
  }

  public void setBranch_no(String branch_no) {
    this.branch_no = branch_no;
  }

  public String getPrice_zone_no() {
    return price_zone_no;
  }

  public void setPrice_zone_no(String price_zone_no) {
    this.price_zone_no = price_zone_no;
  }

  public String getPrice_zone_name() {
    return price_zone_name;
  }

  public void setPrice_zone_name(String price_zone_name) {
    this.price_zone_name = price_zone_name;
  }

  public Double getPrice_min() {
    return price_min;
  }

  public void setPrice_min(Double price_min) {
    this.price_min = price_min;
  }

  public Double getPrice_max() {
    return price_max;
  }

  public void setPrice_max(Double price_max) {
    this.price_max = price_max;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
