package com.bi.cloudsale.persistent.dao.sys.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.sys.ModuleInfoDao;
import com.bi.cloudsale.persistent.entity.sys.ModuleInfoEntity;

@Repository
@Transactional
public class ModuleInfoDaoImpl extends BaseDao<ModuleInfoEntity> implements ModuleInfoDao {
	
	@Override
	public List<ModuleInfoEntity> queryAll() {
		StringBuilder hqlSb = new StringBuilder("from ModuleInfoEntity where isEnable = 1");
		List<Object> params = new ArrayList<Object>();
		hqlSb.append(" order by level,sortNo asc");
		return find(hqlSb.toString(), params);
	}

	@Override
	public List<ModuleInfoEntity> queryByCodes(Set<String> ids) {
		StringBuilder hqlSb = new StringBuilder("from ModuleInfoEntity where isEnable = 1 and moduleId in (:ids)");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("ids", ids);
		return find(hqlSb.toString(), mParam);
	}

}
