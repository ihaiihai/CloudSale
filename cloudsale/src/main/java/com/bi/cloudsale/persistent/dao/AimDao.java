package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.AimEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface AimDao {

	Long save(AimEntity aimEntity) throws HibernateException;

	void updateEntity(AimEntity aimEntity) throws HibernateException;

	List<AimEntity> list(String hql);
}
