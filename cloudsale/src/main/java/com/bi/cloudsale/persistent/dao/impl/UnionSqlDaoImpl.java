package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.ItemClsDao;
import com.bi.cloudsale.persistent.dao.UnionSqlDao;
import com.bi.cloudsale.persistent.entity.ItemClsEntity;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Repository
@Transactional
public class UnionSqlDaoImpl implements UnionSqlDao {

    public UnionSqlDaoImpl() {
        super();
    }
    @Resource(name = "sessionFactoryForHSPlatform")
    private SessionFactory sessionFactory;

    @Override
    public List<Object> query(String sql) throws HibernateException{
        try {
            return getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }

    @Override
    public List<Object> query(String sql, Integer pageNo, Integer pageSize) throws HibernateException {
        try {
            Query query = getCurrentSession().createQuery(sql);
            query.setMaxResults(pageSize);
            query.setFirstResult((pageNo-1)*pageSize);
            return query.list();
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }

    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }
}
