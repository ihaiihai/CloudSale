package com.bi.cloudsale.persistent.dao.item.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.item.BaseItemInfoDao;
import com.bi.cloudsale.persistent.entity.item.BaseItemInfoEntity;

@Repository
@Transactional
public class BaseItemInfoDaoImpl extends BaseDao<BaseItemInfoEntity> implements BaseItemInfoDao {

	@Override
	public List<BaseItemInfoEntity> queryByCondition(String orgId, String clsNo, String key, Integer pageIndex,
			Integer pageSize) {
		StringBuilder hqlSb = new StringBuilder("from BaseItemInfoEntity where org_id = :org_id and item_clsno = :clsNo");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		mParam.put("clsNo", clsNo);
		if(StringUtils.isNotEmpty(key)) {
			hqlSb.append(" and (item_no like :key or item_name like :key)");
			mParam.put("key", "%"+key+"%");
		}
		if(pageSize != null) {
			return find(hqlSb.toString(), mParam, pageIndex, pageSize);
		}else {
			return find(hqlSb.toString(), mParam);
		}
	}

}
