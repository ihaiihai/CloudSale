package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.BranchDao;
import com.bi.cloudsale.persistent.entity.BranchEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class BranchDaoImpl extends BaseDao<BranchEntity> implements BranchDao {

	public BranchDaoImpl() {
		super();
		setClazz(BranchEntity.class);
	}

	@Override
	public Long save(BranchEntity branchEntity)  throws HibernateException {
		branchEntity = createEntity(branchEntity);
		return branchEntity.getId();
	}

	@Override
	public void updateEntity(BranchEntity branchEntity) throws HibernateException {
		update(branchEntity);
	}

	@Override
	public List<BranchEntity> list(String hql) {
		return	findByHql(hql);
	}
}
