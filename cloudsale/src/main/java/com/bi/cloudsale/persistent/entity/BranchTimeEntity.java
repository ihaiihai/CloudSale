package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_sale_branch_time", catalog = "cloudsale")
public class BranchTimeEntity implements Serializable{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String branch_no;
  private java.sql.Timestamp oper_date;
  private Long cnt_00;
  private Double amt_00;
  private Double avg_00;
  private Long cnt_01;
  private Double amt_01;
  private Double avg_01;
  private Long cnt_02;
  private Double amt_02;
  private Double avg_02;
  private Long cnt_03;
  private Double amt_03;
  private Double avg_03;
  private Long cnt_04;
  private Double amt_04;
  private Double avg_04;
  private Long cnt_05;
  private Double amt_05;
  private Double avg_05;
  private Long cnt_06;
  private Double amt_06;
  private Double avg_06;
  private Long cnt_07;
  private Double amt_07;
  private Double avg_07;
  private Long cnt_08;
  private Double amt_08;
  private Double avg_08;
  private Long cnt_09;
  private Double amt_09;
  private Double avg_09;
  private Long cnt_10;
  private Double amt_10;
  private Double avg_10;
  private Long cnt_11;
  private Double amt_11;
  private Double avg_11;
  private Long cnt_12;
  private Double amt_12;
  private Double avg_12;
  private Long cnt_13;
  private Double amt_13;
  private Double avg_13;
  private Long cnt_14;
  private Double amt_14;
  private Double avg_14;
  private Long cnt_15;
  private Double amt_15;
  private Double avg_15;
  private Long cnt_16;
  private Double amt_16;
  private Double avg_16;
  private Long cnt_17;
  private Double amt_17;
  private Double avg_17;
  private Long cnt_18;
  private Double amt_18;
  private Double avg_18;
  private Long cnt_19;
  private Double amt_19;
  private Double avg_19;
  private Long cnt_20;
  private Double amt_20;
  private Double avg_20;
  private Long cnt_21;
  private Double amt_21;
  private Double avg_21;
  private Long cnt_22;
  private Double amt_22;
  private Double avg_22;
  private Long cnt_23;
  private Double amt_23;
  private Double avg_23;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getBranch_no() {
    return branch_no;
  }

  public void setBranch_no(String branch_no) {
    this.branch_no = branch_no;
  }

  public java.sql.Timestamp getOper_date() {
    return oper_date;
  }

  public void setOper_date(java.sql.Timestamp oper_date) {
    this.oper_date = oper_date;
  }

  public Long getCnt_00() {
    return cnt_00;
  }

  public void setCnt_00(Long cnt_00) {
    this.cnt_00 = cnt_00;
  }

  public Double getAmt_00() {
    return amt_00;
  }

  public void setAmt_00(Double amt_00) {
    this.amt_00 = amt_00;
  }

  public Double getAvg_00() {
    return avg_00;
  }

  public void setAvg_00(Double avg_00) {
    this.avg_00 = avg_00;
  }

  public Long getCnt_01() {
    return cnt_01;
  }

  public void setCnt_01(Long cnt_01) {
    this.cnt_01 = cnt_01;
  }

  public Double getAmt_01() {
    return amt_01;
  }

  public void setAmt_01(Double amt_01) {
    this.amt_01 = amt_01;
  }

  public Double getAvg_01() {
    return avg_01;
  }

  public void setAvg_01(Double avg_01) {
    this.avg_01 = avg_01;
  }

  public Long getCnt_02() {
    return cnt_02;
  }

  public void setCnt_02(Long cnt_02) {
    this.cnt_02 = cnt_02;
  }

  public Double getAmt_02() {
    return amt_02;
  }

  public void setAmt_02(Double amt_02) {
    this.amt_02 = amt_02;
  }

  public Double getAvg_02() {
    return avg_02;
  }

  public void setAvg_02(Double avg_02) {
    this.avg_02 = avg_02;
  }

  public Long getCnt_03() {
    return cnt_03;
  }

  public void setCnt_03(Long cnt_03) {
    this.cnt_03 = cnt_03;
  }

  public Double getAmt_03() {
    return amt_03;
  }

  public void setAmt_03(Double amt_03) {
    this.amt_03 = amt_03;
  }

  public Double getAvg_03() {
    return avg_03;
  }

  public void setAvg_03(Double avg_03) {
    this.avg_03 = avg_03;
  }

  public Long getCnt_04() {
    return cnt_04;
  }

  public void setCnt_04(Long cnt_04) {
    this.cnt_04 = cnt_04;
  }

  public Double getAmt_04() {
    return amt_04;
  }

  public void setAmt_04(Double amt_04) {
    this.amt_04 = amt_04;
  }

  public Double getAvg_04() {
    return avg_04;
  }

  public void setAvg_04(Double avg_04) {
    this.avg_04 = avg_04;
  }

  public Long getCnt_05() {
    return cnt_05;
  }

  public void setCnt_05(Long cnt_05) {
    this.cnt_05 = cnt_05;
  }

  public Double getAmt_05() {
    return amt_05;
  }

  public void setAmt_05(Double amt_05) {
    this.amt_05 = amt_05;
  }

  public Double getAvg_05() {
    return avg_05;
  }

  public void setAvg_05(Double avg_05) {
    this.avg_05 = avg_05;
  }

  public Long getCnt_06() {
    return cnt_06;
  }

  public void setCnt_06(Long cnt_06) {
    this.cnt_06 = cnt_06;
  }

  public Double getAmt_06() {
    return amt_06;
  }

  public void setAmt_06(Double amt_06) {
    this.amt_06 = amt_06;
  }

  public Double getAvg_06() {
    return avg_06;
  }

  public void setAvg_06(Double avg_06) {
    this.avg_06 = avg_06;
  }

  public Long getCnt_07() {
    return cnt_07;
  }

  public void setCnt_07(Long cnt_07) {
    this.cnt_07 = cnt_07;
  }

  public Double getAmt_07() {
    return amt_07;
  }

  public void setAmt_07(Double amt_07) {
    this.amt_07 = amt_07;
  }

  public Double getAvg_07() {
    return avg_07;
  }

  public void setAvg_07(Double avg_07) {
    this.avg_07 = avg_07;
  }

  public Long getCnt_08() {
    return cnt_08;
  }

  public void setCnt_08(Long cnt_08) {
    this.cnt_08 = cnt_08;
  }

  public Double getAmt_08() {
    return amt_08;
  }

  public void setAmt_08(Double amt_08) {
    this.amt_08 = amt_08;
  }

  public Double getAvg_08() {
    return avg_08;
  }

  public void setAvg_08(Double avg_08) {
    this.avg_08 = avg_08;
  }

  public Long getCnt_09() {
    return cnt_09;
  }

  public void setCnt_09(Long cnt_09) {
    this.cnt_09 = cnt_09;
  }

  public Double getAmt_09() {
    return amt_09;
  }

  public void setAmt_09(Double amt_09) {
    this.amt_09 = amt_09;
  }

  public Double getAvg_09() {
    return avg_09;
  }

  public void setAvg_09(Double avg_09) {
    this.avg_09 = avg_09;
  }

  public Long getCnt_10() {
    return cnt_10;
  }

  public void setCnt_10(Long cnt_10) {
    this.cnt_10 = cnt_10;
  }

  public Double getAmt_10() {
    return amt_10;
  }

  public void setAmt_10(Double amt_10) {
    this.amt_10 = amt_10;
  }

  public Double getAvg_10() {
    return avg_10;
  }

  public void setAvg_10(Double avg_10) {
    this.avg_10 = avg_10;
  }

  public Long getCnt_11() {
    return cnt_11;
  }

  public void setCnt_11(Long cnt_11) {
    this.cnt_11 = cnt_11;
  }

  public Double getAmt_11() {
    return amt_11;
  }

  public void setAmt_11(Double amt_11) {
    this.amt_11 = amt_11;
  }

  public Double getAvg_11() {
    return avg_11;
  }

  public void setAvg_11(Double avg_11) {
    this.avg_11 = avg_11;
  }

  public Long getCnt_12() {
    return cnt_12;
  }

  public void setCnt_12(Long cnt_12) {
    this.cnt_12 = cnt_12;
  }

  public Double getAmt_12() {
    return amt_12;
  }

  public void setAmt_12(Double amt_12) {
    this.amt_12 = amt_12;
  }

  public Double getAvg_12() {
    return avg_12;
  }

  public void setAvg_12(Double avg_12) {
    this.avg_12 = avg_12;
  }

  public Long getCnt_13() {
    return cnt_13;
  }

  public void setCnt_13(Long cnt_13) {
    this.cnt_13 = cnt_13;
  }

  public Double getAmt_13() {
    return amt_13;
  }

  public void setAmt_13(Double amt_13) {
    this.amt_13 = amt_13;
  }

  public Double getAvg_13() {
    return avg_13;
  }

  public void setAvg_13(Double avg_13) {
    this.avg_13 = avg_13;
  }

  public Long getCnt_14() {
    return cnt_14;
  }

  public void setCnt_14(Long cnt_14) {
    this.cnt_14 = cnt_14;
  }

  public Double getAmt_14() {
    return amt_14;
  }

  public void setAmt_14(Double amt_14) {
    this.amt_14 = amt_14;
  }

  public Double getAvg_14() {
    return avg_14;
  }

  public void setAvg_14(Double avg_14) {
    this.avg_14 = avg_14;
  }

  public Long getCnt_15() {
    return cnt_15;
  }

  public void setCnt_15(Long cnt_15) {
    this.cnt_15 = cnt_15;
  }

  public Double getAmt_15() {
    return amt_15;
  }

  public void setAmt_15(Double amt_15) {
    this.amt_15 = amt_15;
  }

  public Double getAvg_15() {
    return avg_15;
  }

  public void setAvg_15(Double avg_15) {
    this.avg_15 = avg_15;
  }

  public Long getCnt_16() {
    return cnt_16;
  }

  public void setCnt_16(Long cnt_16) {
    this.cnt_16 = cnt_16;
  }

  public Double getAmt_16() {
    return amt_16;
  }

  public void setAmt_16(Double amt_16) {
    this.amt_16 = amt_16;
  }

  public Double getAvg_16() {
    return avg_16;
  }

  public void setAvg_16(Double avg_16) {
    this.avg_16 = avg_16;
  }

  public Long getCnt_17() {
    return cnt_17;
  }

  public void setCnt_17(Long cnt_17) {
    this.cnt_17 = cnt_17;
  }

  public Double getAmt_17() {
    return amt_17;
  }

  public void setAmt_17(Double amt_17) {
    this.amt_17 = amt_17;
  }

  public Double getAvg_17() {
    return avg_17;
  }

  public void setAvg_17(Double avg_17) {
    this.avg_17 = avg_17;
  }

  public Long getCnt_18() {
    return cnt_18;
  }

  public void setCnt_18(Long cnt_18) {
    this.cnt_18 = cnt_18;
  }

  public Double getAmt_18() {
    return amt_18;
  }

  public void setAmt_18(Double amt_18) {
    this.amt_18 = amt_18;
  }

  public Double getAvg_18() {
    return avg_18;
  }

  public void setAvg_18(Double avg_18) {
    this.avg_18 = avg_18;
  }

  public Long getCnt_19() {
    return cnt_19;
  }

  public void setCnt_19(Long cnt_19) {
    this.cnt_19 = cnt_19;
  }

  public Double getAmt_19() {
    return amt_19;
  }

  public void setAmt_19(Double amt_19) {
    this.amt_19 = amt_19;
  }

  public Double getAvg_19() {
    return avg_19;
  }

  public void setAvg_19(Double avg_19) {
    this.avg_19 = avg_19;
  }

  public Long getCnt_20() {
    return cnt_20;
  }

  public void setCnt_20(Long cnt_20) {
    this.cnt_20 = cnt_20;
  }

  public Double getAmt_20() {
    return amt_20;
  }

  public void setAmt_20(Double amt_20) {
    this.amt_20 = amt_20;
  }

  public Double getAvg_20() {
    return avg_20;
  }

  public void setAvg_20(Double avg_20) {
    this.avg_20 = avg_20;
  }

  public Long getCnt_21() {
    return cnt_21;
  }

  public void setCnt_21(Long cnt_21) {
    this.cnt_21 = cnt_21;
  }

  public Double getAmt_21() {
    return amt_21;
  }

  public void setAmt_21(Double amt_21) {
    this.amt_21 = amt_21;
  }

  public Double getAvg_21() {
    return avg_21;
  }

  public void setAvg_21(Double avg_21) {
    this.avg_21 = avg_21;
  }

  public Long getCnt_22() {
    return cnt_22;
  }

  public void setCnt_22(Long cnt_22) {
    this.cnt_22 = cnt_22;
  }

  public Double getAmt_22() {
    return amt_22;
  }

  public void setAmt_22(Double amt_22) {
    this.amt_22 = amt_22;
  }

  public Double getAvg_22() {
    return avg_22;
  }

  public void setAvg_22(Double avg_22) {
    this.avg_22 = avg_22;
  }

  public Long getCnt_23() {
    return cnt_23;
  }

  public void setCnt_23(Long cnt_23) {
    this.cnt_23 = cnt_23;
  }

  public Double getAmt_23() {
    return amt_23;
  }

  public void setAmt_23(Double amt_23) {
    this.amt_23 = amt_23;
  }

  public Double getAvg_23() {
    return avg_23;
  }

  public void setAvg_23(Double avg_23) {
    this.avg_23 = avg_23;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
