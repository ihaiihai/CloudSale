package com.bi.cloudsale.persistent.enums.sys;

public enum CanSeeProfitEnum {

	NONE("", 0),
	YES("可见", 1),
	NO("不可见", 2);

	private final String name;  
    private final Integer value;  
  
    public String getName() {  
        return name;  
    }  
  
    public Integer getValue() {  
        return value;  
    }  
  
    CanSeeProfitEnum(String name, Integer value) {  
        this.name = name;  
        this.value = value;  
    }
    
	public static CanSeeProfitEnum valueOf(Integer value) {
		for (CanSeeProfitEnum item : CanSeeProfitEnum.values()) {
			if (item.getValue() == value) {
				return item;
			}
		}
		return NONE;
	}
}
