package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.BranchEntity;
import com.bi.cloudsale.persistent.entity.BrandEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface BrandDao {

	Long save(BrandEntity brandEntity) throws HibernateException;

	void updateEntity(BrandEntity brandEntity) throws HibernateException;

	List<BrandEntity> list(String hql);
}
