package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.BrandDao;
import com.bi.cloudsale.persistent.entity.BrandEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class BrandDaoImpl extends BaseDao<BrandEntity> implements BrandDao {

	public BrandDaoImpl() {
		super();
		setClazz(BrandEntity.class);
	}

	@Override
	public Long save(BrandEntity brandEntity)  throws HibernateException {
		brandEntity = createEntity(brandEntity);
		return brandEntity.getId();
	}

	@Override
	public void updateEntity(BrandEntity brandEntity) throws HibernateException {
		update(brandEntity);
	}

	@Override
	public List<BrandEntity> list(String hql) {
		return	findByHql(hql);
	}
}
