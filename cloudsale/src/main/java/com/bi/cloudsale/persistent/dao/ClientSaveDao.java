package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;

import org.hibernate.HibernateException;

import java.util.List;
import java.util.Map;

public interface ClientSaveDao {
	
	Class getTableClassByTableName(String tableName);
}
