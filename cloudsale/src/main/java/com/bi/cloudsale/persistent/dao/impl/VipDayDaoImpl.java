package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.BranchDayDao;
import com.bi.cloudsale.persistent.dao.VipDayDao;
import com.bi.cloudsale.persistent.entity.BranchDayEntity;
import com.bi.cloudsale.persistent.entity.VipDayEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class VipDayDaoImpl extends BaseDao<VipDayEntity> implements VipDayDao {

	public VipDayDaoImpl() {
		super();
		setClazz(VipDayEntity.class);
	}

	@Override
	public Long save(VipDayEntity vipDayEntity)  throws HibernateException {
		vipDayEntity = createEntity(vipDayEntity);
		return vipDayEntity.getId();
	}

	@Override
	public void updateEntity(VipDayEntity vipDayEntity) throws HibernateException {
		update(vipDayEntity);
	}

	@Override
	public List<VipDayEntity> list(String hql) {
		return	findByHql(hql);
	}
}
