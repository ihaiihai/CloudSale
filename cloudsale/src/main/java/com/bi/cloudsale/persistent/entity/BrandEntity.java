package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_base_item_brand", catalog = "cloudsale")
public class BrandEntity implements Serializable{
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String item_brandno;
  private String item_brandname;
  private String brand_parent;
  private String product_area;
  private String other1;
  private String other2;
  private String other3;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;
  @Transient
  private Integer checked = 0;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
	return org_id;
  }

  public void setOrg_id(String org_id) {
	this.org_id = org_id;
  }

  public String getItem_brandno() {
    return item_brandno;
  }

  public void setItem_brandno(String item_brandno) {
    this.item_brandno = item_brandno;
  }

  public String getItem_brandname() {
    return item_brandname;
  }

  public void setItem_brandname(String item_brandname) {
    this.item_brandname = item_brandname;
  }

  public String getBrand_parent() {
    return brand_parent;
  }

  public void setBrand_parent(String brand_parent) {
    this.brand_parent = brand_parent;
  }

  public String getProduct_area() {
    return product_area;
  }

  public void setProduct_area(String product_area) {
    this.product_area = product_area;
  }

  public String getOther1() {
    return other1;
  }

  public void setOther1(String other1) {
    this.other1 = other1;
  }

  public String getOther2() {
    return other2;
  }

  public void setOther2(String other2) {
    this.other2 = other2;
  }

  public String getOther3() {
    return other3;
  }

  public void setOther3(String other3) {
    this.other3 = other3;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
  
	public Integer getChecked() {
		return checked;
	}
	
	public void setChecked(Integer checked) {
		this.checked = checked;
	}
}
