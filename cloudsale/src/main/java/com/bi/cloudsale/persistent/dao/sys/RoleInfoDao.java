package com.bi.cloudsale.persistent.dao.sys;

import java.util.List;

import org.hibernate.HibernateException;

import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.persistent.entity.sys.RoleInfoEntity;

public interface RoleInfoDao {

	RoleInfoEntity queryByCode(String roleCode);

	void createRoleInfo(RoleInfoEntity roleInfoEntity) throws HibernateException;
	
	void deleteRoleInfo(String roleCode) throws HibernateException;
	
	void updateRoleInfo(RoleInfoEntity roleInfoEntity) throws HibernateException;
	
	List<RoleInfoEntity> queryByCondition(BaseQueryCondition baseQueryCondition);
}
