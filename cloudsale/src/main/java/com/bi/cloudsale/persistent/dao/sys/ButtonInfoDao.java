package com.bi.cloudsale.persistent.dao.sys;

import java.util.List;

import com.bi.cloudsale.persistent.entity.sys.ButtonInfoEntity;

public interface ButtonInfoDao {

	List<ButtonInfoEntity> queryAll();
}
