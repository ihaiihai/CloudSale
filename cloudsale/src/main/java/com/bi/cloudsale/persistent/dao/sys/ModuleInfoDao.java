package com.bi.cloudsale.persistent.dao.sys;

import java.util.List;
import java.util.Set;

import com.bi.cloudsale.persistent.entity.sys.ModuleInfoEntity;

public interface ModuleInfoDao {

	List<ModuleInfoEntity> queryAll();
	
	List<ModuleInfoEntity> queryByCodes(Set<String> ids);
}
