package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.AimDao;
import com.bi.cloudsale.persistent.dao.ItemDao;
import com.bi.cloudsale.persistent.entity.AimEntity;
import com.bi.cloudsale.persistent.entity.ItemEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class ItemDaoImpl extends BaseDao<ItemEntity> implements ItemDao {

	public ItemDaoImpl() {
		super();
		setClazz(ItemEntity.class);
	}

	@Override
	public Long save(ItemEntity itemEntity)  throws HibernateException {
		itemEntity = createEntity(itemEntity);
		return itemEntity.getId();
	}

	@Override
	public void updateEntity(ItemEntity itemEntity) throws HibernateException {
		update(itemEntity);
	}

	@Override
	public List<ItemEntity> list(String hql) {
		return	findByHql(hql);
	}
}
