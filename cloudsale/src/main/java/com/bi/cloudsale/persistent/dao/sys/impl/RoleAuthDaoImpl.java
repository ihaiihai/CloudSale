package com.bi.cloudsale.persistent.dao.sys.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.sys.RoleAuthDao;
import com.bi.cloudsale.persistent.entity.sys.RoleAuthEntity;

@Repository
@Transactional
public class RoleAuthDaoImpl extends BaseDao<RoleAuthEntity> implements RoleAuthDao {

	@Override
	public RoleAuthEntity queryByRoleCode(String roleCode, Integer itemtype) {
		StringBuilder hqlSb = new StringBuilder("from RoleAuthEntity where roleCode = ? and itemType = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(roleCode);
		params.add(itemtype);
		return findOne(hqlSb.toString(), params);
	}

	@Override
	public void createRoleAuth(RoleAuthEntity roleAuthEntity) throws HibernateException {
		create(roleAuthEntity);
	}

	@Override
	public void deleteRoleAuth(String roleCode, Integer itemType) throws HibernateException {
		RoleAuthEntity roleAuthEntity = this.queryByRoleCode(roleCode, itemType);
		this.delete(roleAuthEntity);
	}

	@Override
	public void updateRoleAuth(RoleAuthEntity roleAuthEntity) throws HibernateException {
		this.update(roleAuthEntity);
	}

}
