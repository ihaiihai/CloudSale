package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.BrandDayEntity;
import com.bi.cloudsale.persistent.entity.ClsDayEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface ClsDayDao {

	Long save(ClsDayEntity clsDayEntity) throws HibernateException;

	void updateEntity(ClsDayEntity clsDayEntity) throws HibernateException;

	List<ClsDayEntity> list(String hql);
}
