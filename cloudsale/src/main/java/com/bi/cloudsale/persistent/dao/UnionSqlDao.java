package com.bi.cloudsale.persistent.dao;

import org.hibernate.HibernateException;

import java.util.List;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.persistent.dao
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/22 12:33
 * @Version: 1.0
 **/
public interface UnionSqlDao {
    List<Object> query(String sql) throws HibernateException;
    List<Object> query(String sql,Integer pageNo,Integer pageSize) throws HibernateException;
}
