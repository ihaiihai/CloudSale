package com.bi.cloudsale.persistent.dao.branch.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.branch.SaleBranchDayDao;
import com.bi.cloudsale.persistent.entity.branch.SaleBranchDayEntity;

@Repository
@Transactional
public class SaleBranchDayDaoImpl extends BaseDao<SaleBranchDayEntity> implements SaleBranchDayDao {

	@Override
	public double querySum(String userId, String orgId, Set<String> branchNo, Date startDate, Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select sum(sale_amt) from SaleBranchDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		return findDouble(hqlSb.toString(), mParam);
	}

	@Override
	public List<SaleBranchDayEntity> querySumByBranch(String userId, String orgId, Set<String> branchNo, Date startDate,
			Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select new SaleBranchDayEntity(branch_no,sum(sale_amt) as sale_amt) from SaleBranchDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		hqlSb.append(" GROUP BY branch_no");
		return find(hqlSb.toString(), mParam);
	}

	@Override
	public List<SaleBranchDayEntity> queryAllSumByBranch(String userId, String orgId, Set<String> branchNo,
			Date startDate, Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select new SaleBranchDayEntity(branch_no,sum(sale_amt) as sale_amt, sum(ret_qty) as ret_qty, sum(sale_qty) as sale_qty, sum(dis_amt) as dis_amt, sum(bill_cnt) as bill_cnt, sum(bill_vip_cnt) as bill_vip_cnt) from SaleBranchDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		hqlSb.append(" GROUP BY branch_no");
		return find(hqlSb.toString(), mParam);
	}

}
