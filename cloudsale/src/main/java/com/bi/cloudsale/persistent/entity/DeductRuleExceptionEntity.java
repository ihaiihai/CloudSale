package com.bi.cloudsale.persistent.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "cs_deduct_rule_exception", catalog = "cloudsale")
public class DeductRuleExceptionEntity implements Serializable{

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Basic(optional = false)
  @Column(name = "id")
  private Long id;
  private String user_id;
  private String org_id;
  private String deduct_no;
  private Long flow_id;
  private String item_no;
  private java.sql.Timestamp gmt_create;
  private java.sql.Timestamp gmt_modified;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getUser_id() {
    return user_id;
  }

  public void setUser_id(String user_id) {
    this.user_id = user_id;
  }

  public String getOrg_id() {
    return org_id;
  }

  public void setOrg_id(String org_id) {
    this.org_id = org_id;
  }

  public String getDeduct_no() {
    return deduct_no;
  }

  public void setDeduct_no(String deduct_no) {
    this.deduct_no = deduct_no;
  }

  public Long getFlow_id() {
    return flow_id;
  }

  public void setFlow_id(Long flow_id) {
    this.flow_id = flow_id;
  }

  public String getItem_no() {
    return item_no;
  }

  public void setItem_no(String item_no) {
    this.item_no = item_no;
  }

  public java.sql.Timestamp getGmt_create() {
    return gmt_create;
  }

  public void setGmt_create(java.sql.Timestamp gmt_create) {
    this.gmt_create = gmt_create;
  }

  public java.sql.Timestamp getGmt_modified() {
    return gmt_modified;
  }

  public void setGmt_modified(java.sql.Timestamp gmt_modified) {
    this.gmt_modified = gmt_modified;
  }
}
