package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.AimEntity;
import com.bi.cloudsale.persistent.entity.BranchTimeEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface BranchTimeDao {

	Long save(BranchTimeEntity branchTimeEntity) throws HibernateException;

	void updateEntity(BranchTimeEntity branchTimeEntity) throws HibernateException;

	List<BranchTimeEntity> list(String hql);
}
