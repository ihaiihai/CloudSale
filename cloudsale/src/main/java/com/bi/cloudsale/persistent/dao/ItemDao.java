package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.ItemEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface ItemDao {

	Long save(ItemEntity itemEntity) throws HibernateException;

	void updateEntity(ItemEntity itemEntity) throws HibernateException;

	List<ItemEntity> list(String hql);
}
