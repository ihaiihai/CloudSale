package com.bi.cloudsale.persistent.dao.brand.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.brand.SaleBrandDayDao;
import com.bi.cloudsale.persistent.entity.brand.SaleBrandDayEntity;

@Repository
@Transactional
public class SaleBrandDayDaoImpl extends BaseDao<SaleBrandDayEntity> implements SaleBrandDayDao {

	@Override
	public List<SaleBrandDayEntity> querySum(String userId, String orgId, Set<String> brandNo, Set<String> branchNo, 
			Date startDate, Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select new SaleBrandDayEntity(branch_no,item_brandno,sum(sale_amt) as sale_amt) from SaleBrandDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(brandNo != null && brandNo.size() > 0) {
			hqlSb.append(" and item_brandno in (:brandNo)");
			mParam.put("brandNo", brandNo);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		hqlSb.append(" GROUP BY branch_no,item_brandno");
		return find(hqlSb.toString(), mParam);
	}

	@Override
	public List<SaleBrandDayEntity> querySumByBranch(String userId, String orgId, 
			Set<String> branchNo, Date startDate, Date endDate) {
		StringBuilder hqlSb = new StringBuilder("select new SaleBrandDayEntity(branch_no,sum(sale_amt) as sale_amt) from SaleBrandDayEntity where org_id = :org_id");
		Map<String,Object> mParam = new HashMap<String,Object>();
		mParam.put("org_id", orgId);
		if(userId != null) {
			hqlSb.append(" and user_id = :user_id");
			mParam.put("user_id", userId);
		}
		if(branchNo != null && branchNo.size() > 0) {
			hqlSb.append(" and branch_no in (:branch_no)");
			mParam.put("branch_no", branchNo);
		}
		if(startDate != null) {
			hqlSb.append(" and oper_date >= :startDate");
			mParam.put("startDate", startDate);
		}
		if(endDate != null) {
			hqlSb.append(" and oper_date <= :endDate");
			mParam.put("endDate", endDate);
		}
		hqlSb.append(" GROUP BY branch_no");
		return find(hqlSb.toString(), mParam);
	}

}
