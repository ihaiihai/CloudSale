package com.bi.cloudsale.persistent.dao.sys;

import org.hibernate.HibernateException;

import com.bi.cloudsale.persistent.entity.sys.RoleAuthEntity;

public interface RoleAuthDao {

	RoleAuthEntity queryByRoleCode(String roleCode, Integer itemtype);
	
	void createRoleAuth(RoleAuthEntity roleAuthEntity) throws HibernateException;
	
	void deleteRoleAuth(String roleCode, Integer itemType) throws HibernateException;
	
	void updateRoleAuth(RoleAuthEntity roleAuthEntity) throws HibernateException;
}
