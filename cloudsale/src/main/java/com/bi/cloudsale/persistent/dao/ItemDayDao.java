package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.ItemDayEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface ItemDayDao {

	Long save(ItemDayEntity itemDayEntity) throws HibernateException;

	void updateEntity(ItemDayEntity itemDayEntity) throws HibernateException;

	List<ItemDayEntity> list(String hql);

	public List<ItemDayEntity> list(String hql,Integer limit);
}
