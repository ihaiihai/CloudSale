package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.VipDayEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface VipDayDao {

	Long save(VipDayEntity vipDayEntity) throws HibernateException;

	void updateEntity(VipDayEntity vipDayEntity) throws HibernateException;

	List<VipDayEntity> list(String hql);
}
