package com.bi.cloudsale.persistent.dao.branch;

import java.util.List;
import java.util.Set;

import com.bi.cloudsale.persistent.entity.branch.BaseBranchEntity;

public interface BaseBranchDao {

	List<BaseBranchEntity> queryByBranch(String userId, String orgId, Set<String> branchNo);
}
