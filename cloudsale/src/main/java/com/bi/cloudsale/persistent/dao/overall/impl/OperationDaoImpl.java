package com.bi.cloudsale.persistent.dao.overall.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.persistent.dao.overall.OperationDao;
import com.bi.cloudsale.persistent.entity.overall.BranchIndEntity;

@Repository
@Transactional
public class OperationDaoImpl extends BaseDao<BranchIndEntity> implements OperationDao {

	@Override
	public void createBranchInd(BranchIndEntity branchIndEntity) throws HibernateException {
		create(branchIndEntity);
	}

	@Override
	public void deleteBranchInd(String orgId, String branchNo) throws HibernateException {
		BranchIndEntity branchIndEntity = this.queryByCode(orgId, branchNo);
		delete(branchIndEntity);
	}

	@Override
	public void updateBranchInd(BranchIndEntity branchIndEntity) throws HibernateException {
		update(branchIndEntity);
	}

	@Override
	public List<BranchIndEntity> queryByCondition(BaseQueryCondition baseQueryCondition) {
		StringBuilder hqlSb = new StringBuilder("from BranchIndEntity where org_id = :org_id");
		Map<String,Object> mParams = new HashMap<String,Object>();
		mParams.put("org_id", baseQueryCondition.getOrgId());
		hqlSb.append(" order by branch_no");
		return find(hqlSb.toString(), mParams);
	}

	@Override
	public BranchIndEntity queryByCode(String orgId, String branchNo) {
		StringBuilder hqlSb = new StringBuilder("from BranchIndEntity where org_id = ? and branch_no = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(orgId);
		params.add(branchNo);
		return findOne(hqlSb.toString(), params);
	}

	@Override
	public List<BranchIndEntity> queryByOrgId(String orgId) {
		StringBuilder hqlSb = new StringBuilder("select new BranchIndEntity(a.branch_no, a.consume_saler, a.consume_per, a.consume_area, " + 
				"a.consume_day, a.bill_day, a.vip_rate, a.consume_vip, a.dis_rate, a.ret_rate, " + 
				"a.trans_value, b.area) from BranchIndEntity a, BranchEntity b where a.branch_no = b.branch_no and a.org_id = ?");
		List<Object> params = new ArrayList<Object>();
		params.add(orgId);
		return find(hqlSb.toString(), params);
	}

}
