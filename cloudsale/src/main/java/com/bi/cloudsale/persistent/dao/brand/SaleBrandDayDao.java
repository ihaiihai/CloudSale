package com.bi.cloudsale.persistent.dao.brand;

import java.util.Date;
import java.util.List;
import java.util.Set;

import com.bi.cloudsale.persistent.entity.brand.SaleBrandDayEntity;

public interface SaleBrandDayDao {

	List<SaleBrandDayEntity> querySum(String userId, String orgId, Set<String> brandNo, Set<String> branchNo, Date startDate, Date endDate);
	
	List<SaleBrandDayEntity> querySumByBranch(String userId, String orgId, Set<String> branchNo, Date startDate, Date endDate);
}
