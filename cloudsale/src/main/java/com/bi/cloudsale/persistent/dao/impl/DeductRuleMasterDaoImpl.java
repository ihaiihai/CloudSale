package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.persistent.dao.AimDao;
import com.bi.cloudsale.persistent.dao.DeductRuleMasterDao;
import com.bi.cloudsale.persistent.entity.AimEntity;
import com.bi.cloudsale.persistent.entity.DeductRuleMasterEntity;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class DeductRuleMasterDaoImpl extends BaseDao<DeductRuleMasterEntity> implements DeductRuleMasterDao {

	public DeductRuleMasterDaoImpl() {
		super();
		setClazz(DeductRuleMasterEntity.class);
	}

	@Override
	public DeductRuleMasterEntity findByDeductNo(String deductNo) throws HibernateException {
		List<DeductRuleMasterEntity> deductRuleMasterEntities = findByHql(" where deduct_no = '"+deductNo+"'");
		if(CollectionUtils.isNotEmpty(deductRuleMasterEntities)){
			return deductRuleMasterEntities.get(0);
		}
		return null;
	}

	@Override
	public void updateEntity(DeductRuleMasterEntity aimEntity) throws HibernateException {
		update(aimEntity);
	}

	@Override
	public Long save(DeductRuleMasterEntity deductRuleMasterEntity)  throws HibernateException {
		deductRuleMasterEntity.setGmt_create(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleMasterEntity.setGmt_modified(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleMasterEntity = createEntity(deductRuleMasterEntity);
		return deductRuleMasterEntity.getId();
	}

	@Override
	public List<DeductRuleMasterEntity> list(String hql) {
		return	findByHql(hql);
	}

	@Override
	public void deleteByDeductNo(String deductNo) throws HibernateException {
		excuteBySql("delete from cs_deduct_rule_master where deduct_no = '"+deductNo+"'");
	}
}
