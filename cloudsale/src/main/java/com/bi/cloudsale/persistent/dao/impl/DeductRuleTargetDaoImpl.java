package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.persistent.dao.DeductRuleTargetDao;
import com.bi.cloudsale.persistent.entity.DeductRuleTargetEntity;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@Repository
@Transactional
public class DeductRuleTargetDaoImpl extends BaseDao<DeductRuleTargetEntity> implements DeductRuleTargetDao {

	public DeductRuleTargetDaoImpl() {
		super();
		setClazz(DeductRuleTargetEntity.class);
	}

	@Override
	public List<DeductRuleTargetEntity> findByDeductNo(String deductNo) throws HibernateException {
		List<DeductRuleTargetEntity> deductRuleTargetEntities = findByHql(" where deduct_no = '"+deductNo+"'");
		return deductRuleTargetEntities;
	}

	@Override
	public void deleteByDeductNo(String deductNo) throws HibernateException {
		excuteBySql("delete from cs_deduct_rule_target where deduct_no = '"+deductNo+"'");
	}

	@Override
	public Long save(DeductRuleTargetEntity deductRuleTargetEntity)  throws HibernateException {
		deductRuleTargetEntity.setGmt_create(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleTargetEntity.setGmt_modified(Timestamp.valueOf(DateUtil.formatDate(new Date(), DateUtil.FULL_DATE_FORMAT)));
		deductRuleTargetEntity = createEntity(deductRuleTargetEntity);
		return deductRuleTargetEntity.getId();
	}
}
