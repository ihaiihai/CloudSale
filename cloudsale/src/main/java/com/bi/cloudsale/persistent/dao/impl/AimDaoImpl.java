package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.AimDao;
import com.bi.cloudsale.persistent.dao.BranchDayDao;
import com.bi.cloudsale.persistent.entity.AimEntity;
import com.bi.cloudsale.persistent.entity.BranchDayEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class AimDaoImpl extends BaseDao<AimEntity> implements AimDao {

	public AimDaoImpl() {
		super();
		setClazz(AimEntity.class);
	}

	@Override
	public Long save(AimEntity aimEntity)  throws HibernateException {
		aimEntity = createEntity(aimEntity);
		return aimEntity.getId();
	}

	@Override
	public void updateEntity(AimEntity aimEntity) throws HibernateException {
		update(aimEntity);
	}

	@Override
	public List<AimEntity> list(String hql) {
		return	findByHql(hql);
	}
}
