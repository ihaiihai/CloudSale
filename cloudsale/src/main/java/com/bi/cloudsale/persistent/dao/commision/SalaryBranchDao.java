package com.bi.cloudsale.persistent.dao.commision;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.bi.cloudsale.persistent.entity.commision.SalaryBranchEntity;

public interface SalaryBranchDao {

	Long save(SalaryBranchEntity SalaryBranchEntity) throws HibernateException;

	void updateEntity(SalaryBranchEntity SalaryBranchEntity) throws HibernateException;

	List<SalaryBranchEntity> list(String hql, List<Object> params);
	
	List<SalaryBranchEntity> queryByCondition(String orgId, Date startDate, Date endDate, String branchNos);
	
	void updateConfirm(String orgId, String account, Date startDate, Date endDate, String branchNos, String salerIds) throws HibernateException;
}
