package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.VipDao;
import com.bi.cloudsale.persistent.entity.VipEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class VipDaoImpl extends BaseDao<VipEntity> implements VipDao {

	public VipDaoImpl() {
		super();
		setClazz(VipEntity.class);
	}

	@Override
	public Long save(VipEntity vipEntity)  throws HibernateException {
		vipEntity = createEntity(vipEntity);
		return vipEntity.getId();
	}

	@Override
	public void updateEntity(VipEntity vipEntity) throws HibernateException {
		update(vipEntity);
	}

	@Override
	public List<VipEntity> list(String hql) {
		return	findByHql(hql);
	}

	@Override
	public List<Object> listBySql(String sql) {
		return getCurrentSession().createQuery(sql).list();
	}
}
