package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.SalerDayEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface SalerDayDao {

	Long save(SalerDayEntity salerDayEntity) throws HibernateException;

	void updateEntity(SalerDayEntity salerDayEntity) throws HibernateException;

	List<SalerDayEntity> list(String hql);
}
