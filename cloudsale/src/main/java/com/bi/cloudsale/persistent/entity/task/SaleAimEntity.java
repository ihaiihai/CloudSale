package com.bi.cloudsale.persistent.entity.task;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cs_sale_aim", catalog = "cloudsale")
public class SaleAimEntity implements Serializable{

	private static final long serialVersionUID = 2102814023460419246L;
	
	private Long id;
	private String user_id;
	private String org_id;
	private Integer aim_type;
	private Integer aim_cycle;
	private Integer cycle_year;
	private Integer cycle_month;
	private Integer cycle_week;
	private String branch_clsno;
	private String branch_no;
	private String item_clsno;
	private String item_brandno;
	private String sale_id;
	private Double aim_value;
	private String other1;
	private String other2;
	private String other3;
	private Date gmt_create;
	private Date gmt_modified;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id")
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public String getOrg_id() {
		return org_id;
	}

	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}

	public Integer getAim_type() {
		return aim_type;
	}

	public void setAim_type(Integer aim_type) {
		this.aim_type = aim_type;
	}

	public Integer getAim_cycle() {
		return aim_cycle;
	}

	public void setAim_cycle(Integer aim_cycle) {
		this.aim_cycle = aim_cycle;
	}

	public Integer getCycle_year() {
		return cycle_year;
	}

	public void setCycle_year(Integer cycle_year) {
		this.cycle_year = cycle_year;
	}

	public Integer getCycle_month() {
		return cycle_month;
	}

	public void setCycle_month(Integer cycle_month) {
		this.cycle_month = cycle_month;
	}

	public Integer getCycle_week() {
		return cycle_week;
	}

	public void setCycle_week(Integer cycle_week) {
		this.cycle_week = cycle_week;
	}

	public String getBranch_clsno() {
		return branch_clsno;
	}

	public void setBranch_clsno(String branch_clsno) {
		this.branch_clsno = branch_clsno;
	}

	public String getBranch_no() {
		return branch_no;
	}

	public void setBranch_no(String branch_no) {
		this.branch_no = branch_no;
	}

	public String getItem_clsno() {
		return item_clsno;
	}

	public void setItem_clsno(String item_clsno) {
		this.item_clsno = item_clsno;
	}

	public String getItem_brandno() {
		return item_brandno;
	}

	public void setItem_brandno(String item_brandno) {
		this.item_brandno = item_brandno;
	}

	public String getSale_id() {
		return sale_id;
	}

	public void setSale_id(String sale_id) {
		this.sale_id = sale_id;
	}

	public Double getAim_value() {
		return aim_value;
	}

	public void setAim_value(Double aim_value) {
		this.aim_value = aim_value;
	}

	public String getOther1() {
		return other1;
	}

	public void setOther1(String other1) {
		this.other1 = other1;
	}

	public String getOther2() {
		return other2;
	}

	public void setOther2(String other2) {
		this.other2 = other2;
	}

	public String getOther3() {
		return other3;
	}

	public void setOther3(String other3) {
		this.other3 = other3;
	}

	public Date getGmt_create() {
		return gmt_create;
	}

	public void setGmt_create(Date gmt_create) {
		this.gmt_create = gmt_create;
	}

	public Date getGmt_modified() {
		return gmt_modified;
	}

	public void setGmt_modified(Date gmt_modified) {
		this.gmt_modified = gmt_modified;
	}

}
