package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.AimEntity;
import com.bi.cloudsale.persistent.entity.DeductRuleMasterEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface DeductRuleMasterDao {

	DeductRuleMasterEntity findByDeductNo(String deductNo) throws HibernateException;

	void updateEntity(DeductRuleMasterEntity aimEntity) throws HibernateException;

	List<DeductRuleMasterEntity> list(String hql);

	Long save(DeductRuleMasterEntity deductRuleMasterEntity)  throws HibernateException;

	void deleteByDeductNo(String deductNo) throws HibernateException;
}
