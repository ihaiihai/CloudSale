package com.bi.cloudsale.persistent.dao.commision;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;

import com.bi.cloudsale.persistent.entity.commision.SalaryDetailEntity;

public interface SalaryDetailDao {

	Long save(SalaryDetailEntity SalaryDetailEntity) throws HibernateException;

	void updateEntity(SalaryDetailEntity SalaryDetailEntity) throws HibernateException;

	List<SalaryDetailEntity> list(String hql, List<Object> params);
	
	List<SalaryDetailEntity> queryByCondition(String orgId, Date startDate, Date endDate, String branchNos, String salerIds);
	
	void updateConfirm(String orgId, String account, Date startDate, Date endDate, String branchNos, String salerIds) throws HibernateException;
}
