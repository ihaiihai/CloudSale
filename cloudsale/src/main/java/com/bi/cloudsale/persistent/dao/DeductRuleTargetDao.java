package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.DeductRuleTargetEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface DeductRuleTargetDao {

	List<DeductRuleTargetEntity> findByDeductNo(String deductNo) throws HibernateException;

	void deleteByDeductNo(String deductNo) throws HibernateException;

	Long save(DeductRuleTargetEntity deductRuleTargetEntity)  throws HibernateException;
}
