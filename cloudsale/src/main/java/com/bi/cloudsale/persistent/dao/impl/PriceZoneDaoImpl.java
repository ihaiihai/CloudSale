package com.bi.cloudsale.persistent.dao.impl;

import com.bi.cloudsale.common.persistent.BaseDao;
import com.bi.cloudsale.persistent.dao.PriceZoneDao;
import com.bi.cloudsale.persistent.entity.PriceZoneEntity;
import org.hibernate.HibernateException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class PriceZoneDaoImpl extends BaseDao<PriceZoneEntity> implements PriceZoneDao {

	public PriceZoneDaoImpl() {
		super();
		setClazz(PriceZoneEntity.class);
	}

	@Override
	public Long save(PriceZoneEntity priceZoneEntity)  throws HibernateException {
		priceZoneEntity = createEntity(priceZoneEntity);
		return priceZoneEntity.getId();
	}

	@Override
	public void updateEntity(PriceZoneEntity priceZoneEntity) throws HibernateException {
		update(priceZoneEntity);
	}

	@Override
	public List<PriceZoneEntity> list(String hql) {
		return	findByHql(hql);
	}
}
