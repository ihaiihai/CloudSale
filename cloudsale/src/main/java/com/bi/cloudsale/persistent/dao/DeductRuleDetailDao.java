package com.bi.cloudsale.persistent.dao;

import com.bi.cloudsale.persistent.entity.DeductRuleDetailEntity;
import org.hibernate.HibernateException;

import java.util.List;

public interface DeductRuleDetailDao {

	List<DeductRuleDetailEntity> findByDeductNo(String deductNo) throws HibernateException;

	void deleteByDeductNo(String deductNo) throws HibernateException;

	Long save(DeductRuleDetailEntity deductRuleDetailEntity)  throws HibernateException;
}
