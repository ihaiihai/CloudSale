package com.bi.cloudsale.dto;

import java.io.Serializable;

public class LoginRequestDto implements Serializable{

	private static final long serialVersionUID = -6858844698717193234L;
	
	private String userAccount;
	private String userPwd;
	public String getUserAccount() {
		return userAccount;
	}
	public void setUserAccount(String userAccount) {
		this.userAccount = userAccount;
	}
	public String getUserPwd() {
		return userPwd;
	}
	public void setUserPwd(String userPwd) {
		this.userPwd = userPwd;
	}
}
