package com.bi.cloudsale.dto;

import java.io.Serializable;

public class RemoteInfoDto implements Serializable{

	private static final long serialVersionUID = -2819362193878421678L;
	
	private String userId;
	private String orgId;
	private String remoteUrl;
	private String remotePort;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getRemoteUrl() {
		return remoteUrl;
	}
	public void setRemoteUrl(String remoteUrl) {
		this.remoteUrl = remoteUrl;
	}
	public String getRemotePort() {
		return remotePort;
	}
	public void setRemotePort(String remotePort) {
		this.remotePort = remotePort;
	}

}
