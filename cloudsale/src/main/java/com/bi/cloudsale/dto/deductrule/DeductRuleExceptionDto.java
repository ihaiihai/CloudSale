package com.bi.cloudsale.dto.deductrule;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.dto.deductrule
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2019/2/15 14:04
 * @Version: 1.0
 **/
public class DeductRuleExceptionDto  implements Serializable {
    private Long id;
    private String user_id;
    private String org_id;
    private String deduct_no;
    private Long flow_id;
    private String item_no;
    private String item_name;
    private Long gmt_create;
    private Long gmt_modified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getDeduct_no() {
        return deduct_no;
    }

    public void setDeduct_no(String deduct_no) {
        this.deduct_no = deduct_no;
    }

    public Long getFlow_id() {
        return flow_id;
    }

    public void setFlow_id(Long flow_id) {
        this.flow_id = flow_id;
    }

    public String getItem_no() {
        return item_no;
    }

    public void setItem_no(String item_no) {
        this.item_no = item_no;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public Long getGmt_create() {
        return gmt_create;
    }

    public void setGmt_create(Long gmt_create) {
        this.gmt_create = gmt_create;
    }

    public Long getGmt_modified() {
        return gmt_modified;
    }

    public void setGmt_modified(Long gmt_modified) {
        this.gmt_modified = gmt_modified;
    }
}
