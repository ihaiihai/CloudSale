package com.bi.cloudsale.dto.deductrule;

import java.io.Serializable;
import java.util.List;

import com.bi.cloudsale.common.annotation.FieldName;

public class DeductRuleMasterDto implements Serializable {

	private Long id;
	@FieldName("用户编码")
	private String user_id;
	@FieldName("机构编码")
	private String org_id;
	@FieldName("方案流水号")
	private String deduct_no;
	@FieldName("方案名称")
	private String deduct_name;
	@FieldName("方案说明")
	private String deduct_desc;
	@FieldName("开始日期")
	private String begin_date;
	@FieldName("结束日期")
	private String end_date;
	@FieldName("长期有效")
	private String long_valid;
	@FieldName("奖励分成-导购")
	private String deduct_rate1;
	@FieldName("奖励分成-店长")
	private String deduct_rate2;
	@FieldName("奖励分成-督导")
	private String deduct_rate3;
	@FieldName("奖励分成-经理")
	private String deduct_rate4;
	@FieldName("计算周期(0日1周2月3季度4半年5年)")
	private Integer mission_cycle;
	@FieldName("适用范围(0全部门店1自定义门店2自定义员工)")
	private String use_range;
	@FieldName("自定义门店编码")
	private List<String> branch_nos;
	@FieldName("自定义员工编码")
	private List<String> saler_ids;
	@FieldName("计算类型(0按单1累计)")
	private String cal_type;
	@FieldName("计算方式(0单独计算1合计计算)")
	private String cal_mode;
	@FieldName("奖励分成")
	private String deduct_rate;
	@FieldName("方案类型(0商品1分类2品牌3商品组合4门店PK5员工PK)")
	private String deduct_mode;
	@FieldName("满类型(0满金额1满数量2大于m元奖励n元)")
	private String full_mode;
	@FieldName("大于m元")
	private Double min_money;
	@FieldName("奖励n元")
	private Double reward_money;
	@FieldName("奖励计算方式(0按金额1按超额部分2按销售额百分比)")
	private String reward_cal;
	@FieldName("制单日期")
	private Long oper_date;
	@FieldName("制单人")
	private String oper_id;
	@FieldName("审核日期")
	private Long work_date;
	@FieldName("审核人")
	private String confirm_id;
	@FieldName("是否生效(1有效；0无效)")
	private Long is_enable;
	private Long gmt_create;
	private Long gmt_modified;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public List<String> getBranch_nos() {
		return branch_nos;
	}

	public void setBranch_nos(List<String> branch_nos) {
		this.branch_nos = branch_nos;
	}

	public List<String> getSaler_ids() {
		return saler_ids;
	}

	public void setSaler_ids(List<String> saler_ids) {
		this.saler_ids = saler_ids;
	}

	public String getOrg_id() {
		return org_id;
	}

	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}

	public String getDeduct_no() {
		return deduct_no;
	}

	public void setDeduct_no(String deduct_no) {
		this.deduct_no = deduct_no;
	}

	public String getDeduct_name() {
		return deduct_name;
	}

	public void setDeduct_name(String deduct_name) {
		this.deduct_name = deduct_name;
	}

	public String getDeduct_desc() {
		return deduct_desc;
	}

	public void setDeduct_desc(String deduct_desc) {
		this.deduct_desc = deduct_desc;
	}

	public Integer getMission_cycle() {
		return mission_cycle;
	}

	public void setMission_cycle(Integer mission_cycle) {
		this.mission_cycle = mission_cycle;
	}

	public String getUse_range() {
		return use_range;
	}

	public void setUse_range(String use_range) {
		this.use_range = use_range;
	}

	public String getCal_type() {
		return cal_type;
	}

	public void setCal_type(String cal_type) {
		this.cal_type = cal_type;
	}

	public String getCal_mode() {
		return cal_mode;
	}

	public void setCal_mode(String cal_mode) {
		this.cal_mode = cal_mode;
	}

	public String getDeduct_rate() {
		return deduct_rate;
	}

	public void setDeduct_rate(String deduct_rate) {
		this.deduct_rate = deduct_rate;
	}

	public String getDeduct_mode() {
		return deduct_mode;
	}

	public void setDeduct_mode(String deduct_mode) {
		this.deduct_mode = deduct_mode;
	}

	public String getFull_mode() {
		return full_mode;
	}

	public void setFull_mode(String full_mode) {
		this.full_mode = full_mode;
	}

	public Double getMin_money() {
		return min_money;
	}

	public void setMin_money(Double min_money) {
		this.min_money = min_money;
	}

	public Double getReward_money() {
		return reward_money;
	}

	public void setReward_money(Double reward_money) {
		this.reward_money = reward_money;
	}

	public String getReward_cal() {
		return reward_cal;
	}

	public void setReward_cal(String reward_cal) {
		this.reward_cal = reward_cal;
	}

	public Long getOper_date() {
		return oper_date;
	}

	public void setOper_date(Long oper_date) {
		this.oper_date = oper_date;
	}

	public String getOper_id() {
		return oper_id;
	}

	public void setOper_id(String oper_id) {
		this.oper_id = oper_id;
	}

	public Long getWork_date() {
		return work_date;
	}

	public void setWork_date(Long work_date) {
		this.work_date = work_date;
	}

	public String getConfirm_id() {
		return confirm_id;
	}

	public void setConfirm_id(String confirm_id) {
		this.confirm_id = confirm_id;
	}

	public String getLong_valid() {
		return long_valid;
	}

	public void setLong_valid(String long_valid) {
		this.long_valid = long_valid;
	}

	public Long getIs_enable() {
		return is_enable;
	}

	public void setIs_enable(Long is_enable) {
		this.is_enable = is_enable;
	}

	public String getBegin_date() {
		return begin_date;
	}

	public void setBegin_date(String begin_date) {
		this.begin_date = begin_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public Long getGmt_create() {
		return gmt_create;
	}

	public void setGmt_create(Long gmt_create) {
		this.gmt_create = gmt_create;
	}

	public String getDeduct_rate1() {
		return deduct_rate1;
	}

	public void setDeduct_rate1(String deduct_rate1) {
		this.deduct_rate1 = deduct_rate1;
	}

	public String getDeduct_rate2() {
		return deduct_rate2;
	}

	public void setDeduct_rate2(String deduct_rate2) {
		this.deduct_rate2 = deduct_rate2;
	}

	public String getDeduct_rate3() {
		return deduct_rate3;
	}

	public void setDeduct_rate3(String deduct_rate3) {
		this.deduct_rate3 = deduct_rate3;
	}

	public String getDeduct_rate4() {
		return deduct_rate4;
	}

	public void setDeduct_rate4(String deduct_rate4) {
		this.deduct_rate4 = deduct_rate4;
	}

	public Long getGmt_modified() {
		return gmt_modified;
	}

	public void setGmt_modified(Long gmt_modified) {
		this.gmt_modified = gmt_modified;
	}
}
