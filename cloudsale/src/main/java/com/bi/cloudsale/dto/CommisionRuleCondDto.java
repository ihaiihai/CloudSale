package com.bi.cloudsale.dto;


import java.io.Serializable;
//提成方案 查询条件
public class CommisionRuleCondDto implements Serializable{

	private static final long serialVersionUID = -6858844698717193234L;

	private String orgId;
	private String begin;
	private String end;
	
	private String gmt_modify;

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getBegin() {
		return begin;
	}

	public void setBegin(String begin) {
		this.begin = begin;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getGmt_modify() {
		return gmt_modify;
	}

	public void setGmt_modify(String gmt_modify) {
		this.gmt_modify = gmt_modify;
	}
}
