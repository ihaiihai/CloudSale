package com.bi.cloudsale.dto.deductrule;

import java.io.Serializable;
import java.util.List;

import com.bi.cloudsale.persistent.entity.DeductRuleRangeEntity;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.dto
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2019/2/15 14:00
 * @Version: 1.0
 **/
public class DeductRuleDto implements Serializable{

    DeductRuleMasterDto deductRuleMasterDto;
    List<DeductRuleTargetDto> deductRuleTargetDtoList;
    List<DeductRuleDetailDto> deductRuleDetailDtoList;
    List<DeductRuleExceptionDto> deductRuleExceptionDtoList;
    List<DeductRuleRangeEntity> deductRuleRangeEntities;

    public DeductRuleMasterDto getDeductRuleMasterDto() {
        return deductRuleMasterDto;
    }

    public void setDeductRuleMasterDto(DeductRuleMasterDto deductRuleMasterDto) {
        this.deductRuleMasterDto = deductRuleMasterDto;
    }

    public List<DeductRuleTargetDto> getDeductRuleTargetDtoList() {
        return deductRuleTargetDtoList;
    }

    public void setDeductRuleTargetDtoList(List<DeductRuleTargetDto> deductRuleTargetDtoList) {
        this.deductRuleTargetDtoList = deductRuleTargetDtoList;
    }

    public List<DeductRuleDetailDto> getDeductRuleDetailDtoList() {
        return deductRuleDetailDtoList;
    }

    public void setDeductRuleDetailDtoList(List<DeductRuleDetailDto> deductRuleDetailDtoList) {
        this.deductRuleDetailDtoList = deductRuleDetailDtoList;
    }

    public List<DeductRuleExceptionDto> getDeductRuleExceptionDtoList() {
        return deductRuleExceptionDtoList;
    }

    public void setDeductRuleExceptionDtoList(List<DeductRuleExceptionDto> deductRuleExceptionDtoList) {
        this.deductRuleExceptionDtoList = deductRuleExceptionDtoList;
    }

	public List<DeductRuleRangeEntity> getDeductRuleRangeEntities() {
		return deductRuleRangeEntities;
	}

	public void setDeductRuleRangeEntities(List<DeductRuleRangeEntity> deductRuleRangeEntities) {
		this.deductRuleRangeEntities = deductRuleRangeEntities;
	}
}
