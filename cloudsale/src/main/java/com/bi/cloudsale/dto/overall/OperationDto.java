package com.bi.cloudsale.dto.overall;

public class OperationDto {

	private String 门店编号;
	private String 标题;
	private Double 人均日销;
	private Double 平米日均;
	private Double 日均销售;
	private Double 日均客流;
	private Double 会员占比;
	private Double 会员日均贡献;
	private Double 平均折扣;
	private Double 退货率;
	private Double 客单价;
	private Double 连带率;
	private Double 人均日销_目标;
	private Double 平米日均_目标;
	private Double 日均销售_目标;
	private Double 日均客流_目标;
	private Double 会员占比_目标;
	private Double 会员日均贡献_目标;
	private Double 平均折扣_目标;
	private Double 退货率_目标;
	private Double 客单价_目标;
	private Double 连带率_目标;
	private Double 评估得分;
	private Double 人均日销_得分;
	private Double 平米日均_得分;
	private Double 日均销售_得分;
	private Double 日均客流_得分;
	private Double 会员占比_得分;
	private Double 会员日均贡献_得分;
	private Double 平均折扣_得分;
	private Double 退货率_得分;
	private Double 客单价_得分;
	private Double 连带率_得分;
	
	public String get门店编号() {
		return 门店编号;
	}
	public void set门店编号(String 门店编号) {
		this.门店编号 = 门店编号;
	}
	public String get标题() {
		return 标题;
	}
	public void set标题(String 标题) {
		this.标题 = 标题;
	}
	public Double get人均日销() {
		return 人均日销;
	}
	public void set人均日销(Double 人均日销) {
		this.人均日销 = 人均日销;
	}
	public Double get平米日均() {
		return 平米日均;
	}
	public void set平米日均(Double 平米日均) {
		this.平米日均 = 平米日均;
	}
	public Double get日均销售() {
		return 日均销售;
	}
	public void set日均销售(Double 日均销售) {
		this.日均销售 = 日均销售;
	}
	public Double get日均客流() {
		return 日均客流;
	}
	public void set日均客流(Double 日均客流) {
		this.日均客流 = 日均客流;
	}
	public Double get会员占比() {
		return 会员占比;
	}
	public void set会员占比(Double 会员占比) {
		this.会员占比 = 会员占比;
	}
	public Double get会员日均贡献() {
		return 会员日均贡献;
	}
	public void set会员日均贡献(Double 会员日均贡献) {
		this.会员日均贡献 = 会员日均贡献;
	}
	public Double get平均折扣() {
		return 平均折扣;
	}
	public void set平均折扣(Double 平均折扣) {
		this.平均折扣 = 平均折扣;
	}
	public Double get退货率() {
		return 退货率;
	}
	public void set退货率(Double 退货率) {
		this.退货率 = 退货率;
	}
	public Double get客单价() {
		return 客单价;
	}
	public void set客单价(Double 客单价) {
		this.客单价 = 客单价;
	}
	public Double get连带率() {
		return 连带率;
	}
	public void set连带率(Double 连带率) {
		this.连带率 = 连带率;
	}
	public Double get人均日销_目标() {
		return 人均日销_目标;
	}
	public void set人均日销_目标(Double 人均日销_目标) {
		this.人均日销_目标 = 人均日销_目标;
	}
	public Double get平米日均_目标() {
		return 平米日均_目标;
	}
	public void set平米日均_目标(Double 平米日均_目标) {
		this.平米日均_目标 = 平米日均_目标;
	}
	public Double get日均销售_目标() {
		return 日均销售_目标;
	}
	public void set日均销售_目标(Double 日均销售_目标) {
		this.日均销售_目标 = 日均销售_目标;
	}
	public Double get日均客流_目标() {
		return 日均客流_目标;
	}
	public void set日均客流_目标(Double 日均客流_目标) {
		this.日均客流_目标 = 日均客流_目标;
	}
	public Double get会员占比_目标() {
		return 会员占比_目标;
	}
	public void set会员占比_目标(Double 会员占比_目标) {
		this.会员占比_目标 = 会员占比_目标;
	}
	public Double get会员日均贡献_目标() {
		return 会员日均贡献_目标;
	}
	public void set会员日均贡献_目标(Double 会员日均贡献_目标) {
		this.会员日均贡献_目标 = 会员日均贡献_目标;
	}
	public Double get平均折扣_目标() {
		return 平均折扣_目标;
	}
	public void set平均折扣_目标(Double 平均折扣_目标) {
		this.平均折扣_目标 = 平均折扣_目标;
	}
	public Double get退货率_目标() {
		return 退货率_目标;
	}
	public void set退货率_目标(Double 退货率_目标) {
		this.退货率_目标 = 退货率_目标;
	}
	public Double get客单价_目标() {
		return 客单价_目标;
	}
	public void set客单价_目标(Double 客单价_目标) {
		this.客单价_目标 = 客单价_目标;
	}
	public Double get连带率_目标() {
		return 连带率_目标;
	}
	public void set连带率_目标(Double 连带率_目标) {
		this.连带率_目标 = 连带率_目标;
	}
	public Double get评估得分() {
		return 评估得分;
	}
	public void set评估得分(Double 评估得分) {
		this.评估得分 = 评估得分;
	}
	public Double get人均日销_得分() {
		return 人均日销_得分;
	}
	public void set人均日销_得分(Double 人均日销_得分) {
		this.人均日销_得分 = 人均日销_得分;
	}
	public Double get平米日均_得分() {
		return 平米日均_得分;
	}
	public void set平米日均_得分(Double 平米日均_得分) {
		this.平米日均_得分 = 平米日均_得分;
	}
	public Double get日均销售_得分() {
		return 日均销售_得分;
	}
	public void set日均销售_得分(Double 日均销售_得分) {
		this.日均销售_得分 = 日均销售_得分;
	}
	public Double get日均客流_得分() {
		return 日均客流_得分;
	}
	public void set日均客流_得分(Double 日均客流_得分) {
		this.日均客流_得分 = 日均客流_得分;
	}
	public Double get会员占比_得分() {
		return 会员占比_得分;
	}
	public void set会员占比_得分(Double 会员占比_得分) {
		this.会员占比_得分 = 会员占比_得分;
	}
	public Double get会员日均贡献_得分() {
		return 会员日均贡献_得分;
	}
	public void set会员日均贡献_得分(Double 会员日均贡献_得分) {
		this.会员日均贡献_得分 = 会员日均贡献_得分;
	}
	public Double get平均折扣_得分() {
		return 平均折扣_得分;
	}
	public void set平均折扣_得分(Double 平均折扣_得分) {
		this.平均折扣_得分 = 平均折扣_得分;
	}
	public Double get退货率_得分() {
		return 退货率_得分;
	}
	public void set退货率_得分(Double 退货率_得分) {
		this.退货率_得分 = 退货率_得分;
	}
	public Double get客单价_得分() {
		return 客单价_得分;
	}
	public void set客单价_得分(Double 客单价_得分) {
		this.客单价_得分 = 客单价_得分;
	}
	public Double get连带率_得分() {
		return 连带率_得分;
	}
	public void set连带率_得分(Double 连带率_得分) {
		this.连带率_得分 = 连带率_得分;
	}
	
}
