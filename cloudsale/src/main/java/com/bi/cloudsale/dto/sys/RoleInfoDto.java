package com.bi.cloudsale.dto.sys;

public class RoleInfoDto {

	private String userId;
	private String orgId;
	private String roleCode;
	private String roleName;
	private Integer canSeeData;
	private Integer canSeeProfit;
	private String description;
	private String menuCheckedIds;
	private String buttonCheckedIds;
	private String canSeeDataStr;
	private String canSeeProfitStr;
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getOrgId() {
		return orgId;
	}
	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}
	public String getRoleCode() {
		return roleCode;
	}
	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Integer getCanSeeData() {
		return canSeeData;
	}
	public void setCanSeeData(Integer canSeeData) {
		this.canSeeData = canSeeData;
	}
	public Integer getCanSeeProfit() {
		return canSeeProfit;
	}
	public void setCanSeeProfit(Integer canSeeProfit) {
		this.canSeeProfit = canSeeProfit;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getMenuCheckedIds() {
		return menuCheckedIds;
	}
	public void setMenuCheckedIds(String menuCheckedIds) {
		this.menuCheckedIds = menuCheckedIds;
	}
	public String getButtonCheckedIds() {
		return buttonCheckedIds;
	}
	public void setButtonCheckedIds(String buttonCheckedIds) {
		this.buttonCheckedIds = buttonCheckedIds;
	}
	public String getCanSeeDataStr() {
		return canSeeDataStr;
	}
	public void setCanSeeDataStr(String canSeeDataStr) {
		this.canSeeDataStr = canSeeDataStr;
	}
	public String getCanSeeProfitStr() {
		return canSeeProfitStr;
	}
	public void setCanSeeProfitStr(String canSeeProfitStr) {
		this.canSeeProfitStr = canSeeProfitStr;
	}
}
