package com.bi.cloudsale.dto.deductrule;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.dto.deductrule
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2019/2/15 14:03
 * @Version: 1.0
 **/
public class DeductRuleTargetDto implements Serializable{
    private Long id;
    private String user_id;
    private String org_id;
    private String deduct_no;
    private Long flow_id;
    private Double target_money;
    private Long cal_type;
    private Double reward_value;
    private Double punish_money;
    private Long gmt_create;
    private Long gmt_modified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getDeduct_no() {
        return deduct_no;
    }

    public void setDeduct_no(String deduct_no) {
        this.deduct_no = deduct_no;
    }

    public Long getFlow_id() {
        return flow_id;
    }

    public void setFlow_id(Long flow_id) {
        this.flow_id = flow_id;
    }

    public Double getTarget_money() {
        return target_money;
    }

    public void setTarget_money(Double target_money) {
        this.target_money = target_money;
    }

    public Long getCal_type() {
        return cal_type;
    }

    public void setCal_type(Long cal_type) {
        this.cal_type = cal_type;
    }

    public Double getReward_value() {
        return reward_value;
    }

    public void setReward_value(Double reward_value) {
        this.reward_value = reward_value;
    }

    public Double getPunish_money() {
        return punish_money;
    }

    public void setPunish_money(Double punish_money) {
        this.punish_money = punish_money;
    }

    public Long getGmt_create() {
        return gmt_create;
    }

    public void setGmt_create(Long gmt_create) {
        this.gmt_create = gmt_create;
    }

    public Long getGmt_modified() {
        return gmt_modified;
    }

    public void setGmt_modified(Long gmt_modified) {
        this.gmt_modified = gmt_modified;
    }
}
