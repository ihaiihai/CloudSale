package com.bi.cloudsale.dto.deductrule;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.dto.deductrule
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2019/2/15 14:03
 * @Version: 1.0
 **/
public class DeductRuleDetailDto  implements Serializable {
    private Long id;
    private String user_id;
    private String org_id;
    private String deduct_no;
    private Long flow_id;
    private String group_no;
    private String info_no;
    private String info_name;
    private Double deduct_value;
    private String other1;
    private String other2;
    private String other3;
    private Long gmt_create;
    private Long gmt_modified;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOrg_id() {
        return org_id;
    }

    public void setOrg_id(String org_id) {
        this.org_id = org_id;
    }

    public String getDeduct_no() {
        return deduct_no;
    }

    public void setDeduct_no(String deduct_no) {
        this.deduct_no = deduct_no;
    }

    public Long getFlow_id() {
        return flow_id;
    }

    public void setFlow_id(Long flow_id) {
        this.flow_id = flow_id;
    }

    public String getGroup_no() {
        return group_no;
    }

    public void setGroup_no(String group_no) {
        this.group_no = group_no;
    }

    public String getInfo_no() {
        return info_no;
    }

    public void setInfo_no(String info_no) {
        this.info_no = info_no;
    }

    public String getInfo_name() {
        return info_name;
    }

    public void setInfo_name(String info_name) {
        this.info_name = info_name;
    }

    public Double getDeduct_value() {
        return deduct_value;
    }

    public void setDeduct_value(Double deduct_value) {
        this.deduct_value = deduct_value;
    }

    public String getOther1() {
        return other1;
    }

    public void setOther1(String other1) {
        this.other1 = other1;
    }

    public String getOther2() {
        return other2;
    }

    public void setOther2(String other2) {
        this.other2 = other2;
    }

    public String getOther3() {
        return other3;
    }

    public void setOther3(String other3) {
        this.other3 = other3;
    }

    public Long getGmt_create() {
        return gmt_create;
    }

    public void setGmt_create(Long gmt_create) {
        this.gmt_create = gmt_create;
    }

    public Long getGmt_modified() {
        return gmt_modified;
    }

    public void setGmt_modified(Long gmt_modified) {
        this.gmt_modified = gmt_modified;
    }
}
