package com.bi.cloudsale.dto.task;

import com.bi.cloudsale.common.annotation.FieldName;

public class FollowUpTaskDto {
	
	@FieldName("门店")
	private String 门店;
	@FieldName("类别")
	private String 类别;
	@FieldName("品牌")
	private String 品牌;
	@FieldName("营业员")
	private String 营业员;
	@FieldName("年度目标")
	private Double 年度目标;
	@FieldName("今年完成")
	private Double 今年完成;
	@FieldName("年进度")
	private Double 年进度;
	@FieldName("年任务预测")
	private Double 年任务预测;
	@FieldName("年完成率预测")
	private Double 年完成率预测;
	@FieldName("本月任务")
	private Double 本月任务;
	@FieldName("本月完成")
	private Double 本月完成;
	@FieldName("月进度")
	private Double 月进度;
	@FieldName("剩余任务")
	private Double 剩余任务;
	@FieldName("剩余天数")
	private Integer 剩余天数;
	@FieldName("本月任务预测")
	private Double 本月任务预测;
	@FieldName("预测完成率")
	private Double 预测完成率;
	
	public String get门店() {
		return 门店;
	}
	public void set门店(String 门店) {
		this.门店 = 门店;
	}
	public String get类别() {
		return 类别;
	}
	public void set类别(String 类别) {
		this.类别 = 类别;
	}
	public String get品牌() {
		return 品牌;
	}
	public void set品牌(String 品牌) {
		this.品牌 = 品牌;
	}
	public String get营业员() {
		return 营业员;
	}
	public void set营业员(String 营业员) {
		this.营业员 = 营业员;
	}
	public Double get年度目标() {
		return 年度目标;
	}
	public void set年度目标(Double 年度目标) {
		this.年度目标 = 年度目标;
	}
	public Double get今年完成() {
		return 今年完成;
	}
	public void set今年完成(Double 今年完成) {
		this.今年完成 = 今年完成;
	}
	public Double get年进度() {
		return 年进度;
	}
	public void set年进度(Double 年进度) {
		this.年进度 = 年进度;
	}
	public Double get年任务预测() {
		return 年任务预测;
	}
	public void set年任务预测(Double 年任务预测) {
		this.年任务预测 = 年任务预测;
	}
	public Double get年完成率预测() {
		return 年完成率预测;
	}
	public void set年完成率预测(Double 年完成率预测) {
		this.年完成率预测 = 年完成率预测;
	}
	public Double get本月任务() {
		return 本月任务;
	}
	public void set本月任务(Double 本月任务) {
		this.本月任务 = 本月任务;
	}
	public Double get本月完成() {
		return 本月完成;
	}
	public void set本月完成(Double 本月完成) {
		this.本月完成 = 本月完成;
	}
	public Double get月进度() {
		return 月进度;
	}
	public void set月进度(Double 月进度) {
		this.月进度 = 月进度;
	}
	public Double get剩余任务() {
		return 剩余任务;
	}
	public void set剩余任务(Double 剩余任务) {
		this.剩余任务 = 剩余任务;
	}
	public Integer get剩余天数() {
		return 剩余天数;
	}
	public void set剩余天数(Integer 剩余天数) {
		this.剩余天数 = 剩余天数;
	}
	public Double get本月任务预测() {
		return 本月任务预测;
	}
	public void set本月任务预测(Double 本月任务预测) {
		this.本月任务预测 = 本月任务预测;
	}
	public Double get预测完成率() {
		return 预测完成率;
	}
	public void set预测完成率(Double 预测完成率) {
		this.预测完成率 = 预测完成率;
	}
}
