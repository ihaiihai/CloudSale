package com.bi.cloudsale.dto.task;

import com.bi.cloudsale.common.annotation.FieldName;

public class SetUpTaskDto {
	
	@FieldName("门店编号")
	private String 门店编号;
	@FieldName("门店名称")
	private String 门店名称;
	@FieldName("类别编号")
	private String 类别编号;
	@FieldName("类别")
	private String 类别;
	@FieldName("品牌编号")
	private String 品牌编号;
	@FieldName("品牌")
	private String 品牌;
	@FieldName("营业员编号")
	private String 营业员编号;
	@FieldName("姓名")
	private String 姓名;
	@FieldName("前一年销售")
	private Double 前一年销售;
	@FieldName("前一年占比")
	private Double 前一年占比;
	@FieldName("销售任务")
	private Double 销售任务;
	@FieldName("业绩提升")
	private Double 业绩提升;
	@FieldName("月度目标_01")
	private Double 月度目标_01;
	@FieldName("月度目标_02")
	private Double 月度目标_02;
	@FieldName("月度目标_03")
	private Double 月度目标_03;
	@FieldName("月度目标_04")
	private Double 月度目标_04;
	@FieldName("月度目标_05")
	private Double 月度目标_05;
	@FieldName("月度目标_06")
	private Double 月度目标_06;
	@FieldName("月度目标_07")
	private Double 月度目标_07;
	@FieldName("月度目标_08")
	private Double 月度目标_08;
	@FieldName("月度目标_09")
	private Double 月度目标_09;
	@FieldName("月度目标_10")
	private Double 月度目标_10;
	@FieldName("月度目标_11")
	private Double 月度目标_11;
	@FieldName("月度目标_12")
	private Double 月度目标_12;
	
	public String get门店编号() {
		return 门店编号;
	}
	public void set门店编号(String 门店编号) {
		this.门店编号 = 门店编号;
	}
	public String get门店名称() {
		return 门店名称;
	}
	public void set门店名称(String 门店名称) {
		this.门店名称 = 门店名称;
	}
	public String get类别编号() {
		return 类别编号;
	}
	public void set类别编号(String 类别编号) {
		this.类别编号 = 类别编号;
	}
	public String get类别() {
		return 类别;
	}
	public void set类别(String 类别) {
		this.类别 = 类别;
	}
	public String get品牌编号() {
		return 品牌编号;
	}
	public void set品牌编号(String 品牌编号) {
		this.品牌编号 = 品牌编号;
	}
	public String get品牌() {
		return 品牌;
	}
	public void set品牌(String 品牌) {
		this.品牌 = 品牌;
	}
	public String get营业员编号() {
		return 营业员编号;
	}
	public void set营业员编号(String 营业员编号) {
		this.营业员编号 = 营业员编号;
	}
	public String get姓名() {
		return 姓名;
	}
	public void set姓名(String 姓名) {
		this.姓名 = 姓名;
	}
	public Double get前一年销售() {
		return 前一年销售;
	}
	public void set前一年销售(Double 前一年销售) {
		this.前一年销售 = 前一年销售;
	}
	public Double get前一年占比() {
		return 前一年占比;
	}
	public void set前一年占比(Double 前一年占比) {
		this.前一年占比 = 前一年占比;
	}
	public Double get销售任务() {
		return 销售任务;
	}
	public void set销售任务(Double 销售任务) {
		this.销售任务 = 销售任务;
	}
	public Double get业绩提升() {
		return 业绩提升;
	}
	public void set业绩提升(Double 业绩提升) {
		this.业绩提升 = 业绩提升;
	}
	public Double get月度目标_01() {
		return 月度目标_01;
	}
	public void set月度目标_01(Double 月度目标_01) {
		this.月度目标_01 = 月度目标_01;
	}
	public Double get月度目标_02() {
		return 月度目标_02;
	}
	public void set月度目标_02(Double 月度目标_02) {
		this.月度目标_02 = 月度目标_02;
	}
	public Double get月度目标_03() {
		return 月度目标_03;
	}
	public void set月度目标_03(Double 月度目标_03) {
		this.月度目标_03 = 月度目标_03;
	}
	public Double get月度目标_04() {
		return 月度目标_04;
	}
	public void set月度目标_04(Double 月度目标_04) {
		this.月度目标_04 = 月度目标_04;
	}
	public Double get月度目标_05() {
		return 月度目标_05;
	}
	public void set月度目标_05(Double 月度目标_05) {
		this.月度目标_05 = 月度目标_05;
	}
	public Double get月度目标_06() {
		return 月度目标_06;
	}
	public void set月度目标_06(Double 月度目标_06) {
		this.月度目标_06 = 月度目标_06;
	}
	public Double get月度目标_07() {
		return 月度目标_07;
	}
	public void set月度目标_07(Double 月度目标_07) {
		this.月度目标_07 = 月度目标_07;
	}
	public Double get月度目标_08() {
		return 月度目标_08;
	}
	public void set月度目标_08(Double 月度目标_08) {
		this.月度目标_08 = 月度目标_08;
	}
	public Double get月度目标_09() {
		return 月度目标_09;
	}
	public void set月度目标_09(Double 月度目标_09) {
		this.月度目标_09 = 月度目标_09;
	}
	public Double get月度目标_10() {
		return 月度目标_10;
	}
	public void set月度目标_10(Double 月度目标_10) {
		this.月度目标_10 = 月度目标_10;
	}
	public Double get月度目标_11() {
		return 月度目标_11;
	}
	public void set月度目标_11(Double 月度目标_11) {
		this.月度目标_11 = 月度目标_11;
	}
	public Double get月度目标_12() {
		return 月度目标_12;
	}
	public void set月度目标_12(Double 月度目标_12) {
		this.月度目标_12 = 月度目标_12;
	}

}
