package com.bi.cloudsale.dto.commision;

import java.sql.Date;

import com.bi.cloudsale.common.annotation.FieldName;

public class SalaryBranchDto {

	@FieldName("机构编码")
	private String org_id;
	@FieldName("业务日期")
	private Date oper_date;
	@FieldName("提成方案编码")
	private String deduct_no;
	@FieldName("提成说明")
	private String deduct_desc;
	@FieldName("门店编码")
	private String branch_no;
	@FieldName("其它门店编码")
	private String branch_list;
	@FieldName("员工编码")
	private String saler_id;
	@FieldName("其它员工编码")
	private String saler_list;
	@FieldName("目标金额")
	private Double aim_amt;
	@FieldName("完成金额")
	private Double fin_amt;
	@FieldName("提成金额")
	private Double deduct_amt;
	@FieldName("分成金额")
	private Double share_amt1;
	@FieldName("分成金额")
	private Double share_amt2;
	@FieldName("分成金额")
	private Double share_amt3;
	@FieldName("分成金额")
	private Double share_amt4;
	@FieldName("扣除金额")
	private Double off_amt;
	@FieldName("确认标记")
	private Integer confirm_flag;
	@FieldName("确认人")
	private String confirm_man;
	@FieldName("确认时间")
	private Date confirm_date;
	
	public String getOrg_id() {
		return org_id;
	}
	public void setOrg_id(String org_id) {
		this.org_id = org_id;
	}
	public Date getOper_date() {
		return oper_date;
	}
	public void setOper_date(Date oper_date) {
		this.oper_date = oper_date;
	}
	public String getDeduct_no() {
		return deduct_no;
	}
	public void setDeduct_no(String deduct_no) {
		this.deduct_no = deduct_no;
	}
	public String getDeduct_desc() {
		return deduct_desc;
	}
	public void setDeduct_desc(String deduct_desc) {
		this.deduct_desc = deduct_desc;
	}
	public String getBranch_no() {
		return branch_no;
	}
	public void setBranch_no(String branch_no) {
		this.branch_no = branch_no;
	}
	public String getBranch_list() {
		return branch_list;
	}
	public void setBranch_list(String branch_list) {
		this.branch_list = branch_list;
	}
	public String getSaler_id() {
		return saler_id;
	}
	public void setSaler_id(String saler_id) {
		this.saler_id = saler_id;
	}
	public String getSaler_list() {
		return saler_list;
	}
	public void setSaler_list(String saler_list) {
		this.saler_list = saler_list;
	}
	public Double getAim_amt() {
		return aim_amt;
	}
	public void setAim_amt(Double aim_amt) {
		this.aim_amt = aim_amt;
	}
	public Double getFin_amt() {
		return fin_amt;
	}
	public void setFin_amt(Double fin_amt) {
		this.fin_amt = fin_amt;
	}
	public Double getDeduct_amt() {
		return deduct_amt;
	}
	public void setDeduct_amt(Double deduct_amt) {
		this.deduct_amt = deduct_amt;
	}
	public Double getShare_amt1() {
		return share_amt1;
	}
	public void setShare_amt1(Double share_amt1) {
		this.share_amt1 = share_amt1;
	}
	public Double getShare_amt2() {
		return share_amt2;
	}
	public void setShare_amt2(Double share_amt2) {
		this.share_amt2 = share_amt2;
	}
	public Double getShare_amt3() {
		return share_amt3;
	}
	public void setShare_amt3(Double share_amt3) {
		this.share_amt3 = share_amt3;
	}
	public Double getShare_amt4() {
		return share_amt4;
	}
	public void setShare_amt4(Double share_amt4) {
		this.share_amt4 = share_amt4;
	}
	public Double getOff_amt() {
		return off_amt;
	}
	public void setOff_amt(Double off_amt) {
		this.off_amt = off_amt;
	}
	public Integer getConfirm_flag() {
		return confirm_flag;
	}
	public void setConfirm_flag(Integer confirm_flag) {
		this.confirm_flag = confirm_flag;
	}
	public String getConfirm_man() {
		return confirm_man;
	}
	public void setConfirm_man(String confirm_man) {
		this.confirm_man = confirm_man;
	}
	public Date getConfirm_date() {
		return confirm_date;
	}
	public void setConfirm_date(Date confirm_date) {
		this.confirm_date = confirm_date;
	}
	
}
