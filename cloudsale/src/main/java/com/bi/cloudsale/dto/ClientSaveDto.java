package com.bi.cloudsale.dto;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.io.Serializable;

public class ClientSaveDto implements Serializable{

	private static final long serialVersionUID = -6858844698717193234L;
	
	private String tableName;
	private JSONArray data;

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public JSONArray getData() {
		return data;
	}

	public void setData(JSONArray data) {
		this.data = data;
	}
}
