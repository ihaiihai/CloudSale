package com.bi.cloudsale.service.sys;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.dto.sys.RoleAuthDto;

public interface RoleAuthService {

	RoleAuthDto queryByRoleCode(String roleCode, Integer itemtype);
	
	void createRoleAuth(RoleAuthDto roleAuthDto) throws BaseException;
	
	void deleteRoleAuth(String roleCode, Integer itemType) throws BaseException;
	
	void updateRoleAuth(RoleAuthDto roleAuthDto) throws BaseException;
	
}
