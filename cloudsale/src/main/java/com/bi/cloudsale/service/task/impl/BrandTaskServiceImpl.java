package com.bi.cloudsale.service.task.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.utils.ArithUtil;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.dto.task.SetUpTaskDto;
import com.bi.cloudsale.persistent.dao.brand.SaleBrandDayDao;
import com.bi.cloudsale.persistent.dao.task.TaskDao;
import com.bi.cloudsale.persistent.entity.brand.SaleBrandDayEntity;
import com.bi.cloudsale.persistent.entity.task.SaleAimEntity;
import com.bi.cloudsale.service.task.BrandTaskService;

@Service
@Transactional
public class BrandTaskServiceImpl implements BrandTaskService {
	
	@Resource
	private TaskDao taskDao;
	@Resource
	private SaleBrandDayDao saleBrandDayDao;

	@Override
	public List<SetUpTaskDto> querySetUp(TaskQueryCondition condition) {
		List<SetUpTaskDto> dtoList = new ArrayList<SetUpTaskDto>();
		List<SaleAimEntity> entityList = taskDao.queryByCondition(condition);
		if(entityList != null && entityList.size() > 0) {
			Map<String,Map<String,SetUpTaskDto>> map = new HashMap<String,Map<String,SetUpTaskDto>>();
			Set<String> branchNo = new HashSet<String>();
			Set<String> brandNo = new HashSet<String>();
			for(SaleAimEntity entity : entityList) {
				SetUpTaskDto dto = null;
				if(findMapItem(map, entity.getBranch_no(), entity.getItem_brandno()) == null) {
					dto = new SetUpTaskDto();
					dto.set门店编号(entity.getBranch_no());
					dto.set门店名称(entity.getBranch_no());
					dto.set品牌编号(entity.getItem_brandno());
					dto.set品牌(entity.getItem_brandno());
					if(!branchNo.contains(entity.getBranch_no())) {
						branchNo.add(entity.getBranch_no());
					}
					if(!brandNo.contains(entity.getItem_brandno())) {
						brandNo.add(entity.getItem_brandno());
					}
					dtoList.add(dto);
					addMapItem(map, entity.getBranch_no(), entity.getItem_brandno(), dto);
				}else {
					dto = findMapItem(map, entity.getBranch_no(), entity.getItem_brandno());
				}
				if(entity.getAim_cycle() == 0) {
					dto.set销售任务(entity.getAim_value());
				}
				if(entity.getAim_cycle() == 1) {
					setMonthAim(dto, entity.getCycle_month(), entity.getAim_value());
				}
			}
			//去年按门店统计销售数据
			List<SaleBrandDayEntity> saleSumBranchList = saleBrandDayDao.querySumByBranch(condition.getUserId(), condition.getOrgId(), branchNo, DateUtil.getYearStart(condition.getYear() - 1), DateUtil.getYearEnd(condition.getYear() - 1));
			Map<String,Double> sumBranchMap = new HashMap<String,Double>();
			if(saleSumBranchList != null && saleSumBranchList.size() > 0) {
				for(SaleBrandDayEntity entity : saleSumBranchList) {
					sumBranchMap.put(entity.getBranch_no(), entity.getSale_amt());
				}
			}
			//去年按门店与品牌统计销售数据
			List<SaleBrandDayEntity> saleSumList = saleBrandDayDao.querySum(condition.getUserId(), condition.getOrgId(), brandNo, branchNo, DateUtil.getYearStart(condition.getYear() - 1), DateUtil.getYearEnd(condition.getYear() - 1));
			if(saleSumList != null && saleSumList.size() > 0) {
				for(SaleBrandDayEntity entity : saleSumList) {
					SetUpTaskDto dto = findMapItem(map, entity.getBranch_no(), entity.getItem_brandno());
					if(dto != null) {
						dto.set前一年销售(entity.getSale_amt());
						dto.set业绩提升(ArithUtil.getIncrease(entity.getSale_amt(), dto.get销售任务()));
						if(sumBranchMap.get(entity.getBranch_no()) != null) {
							dto.set前一年占比(ArithUtil.getPercent(entity.getSale_amt(), sumBranchMap.get(entity.getBranch_no())));
						}
					}
				}
			}
		}
		return dtoList;
	}
	
	private void addMapItem(Map<String,Map<String,SetUpTaskDto>> map, String branchNo, String brandNo, SetUpTaskDto dto) {
		Map<String,SetUpTaskDto> childMap = null;
		if(map.get(branchNo) != null) {
			childMap = map.get(branchNo);
		}else {
			childMap = new HashMap<String,SetUpTaskDto>();
		}
		childMap.put(brandNo, dto);
		map.put(branchNo, childMap);
	}
	
	private SetUpTaskDto findMapItem(Map<String,Map<String,SetUpTaskDto>> map, String branchNo, String brandNo) {
		if(map.get(branchNo) == null) {
			return null;
		}
		return map.get(branchNo).get(brandNo);
	}
	
	private void setMonthAim(SetUpTaskDto dto, Integer month, Double aimValue) {
        try {
        	Method method = dto.getClass().getMethod("set月度目标_" + String.format("%02d", month), Double.class);
			method.invoke(dto, aimValue);
		} catch (Exception e) {
			
		}
	}

}
