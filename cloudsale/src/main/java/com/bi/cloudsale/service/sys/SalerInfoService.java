package com.bi.cloudsale.service.sys;

import com.bi.cloudsale.dto.OptionDto;

import java.util.List;

public interface SalerInfoService {

	List<OptionDto> queryAllOptions(String orgId);
}
