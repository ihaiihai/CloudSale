package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.BranchDayMonthService;
import com.bi.cloudsale.service.RejectReportService;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
@Transactional
public class RejectReportServiceImpl implements RejectReportService {

    private static final Logger log = LoggerFactory.getLogger(RejectReportServiceImpl.class);

    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private ItemDayDao itemDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private VipDayDao vipDayDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {

        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray branchRejectTop10(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String item_name;
            double total_ret;

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getTotal_ret() {
                return total_ret;
            }

            public void setTotal_ret(double total_ret) {
                this.total_ret = total_ret;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = Maps.newLinkedHashMap();
//        List<ItemDayEntity> itemDayEntities = itemDayDao.list("where to_days(oper_date) <= (to_days('"+end+"'))" +
//                " and  to_days(oper_date) >= (to_days('"+begin+"')) and org_id = '"+orgId+"'"+branchSql+" order by ret_amt desc",10);
        List<Object> objects = unionSqlDao.query("select item_no, sum(ret_amt) from ItemDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days('"+end+"')) and to_days(oper_date) >= (to_days('"+begin+"'))"+
                branchSql+" group by item_no order by sum(ret_amt) desc",1,10);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_no = ((String)itemEntityObj[0]);
            Double total_ret = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(item_no);
            if(itemData!=null){
                itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
                dataMap.put(item_no,itemData);
            }
        }

        //商品名
        String item_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_nosStr)){
                item_nosStr = item_nosStr + ",'"+str+"'";
            }else{
                item_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_nosStr)){
            List<ItemEntity> itemEntities = itemDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'");
            for(ItemEntity itemEntity : itemEntities){
                dataMap.get(itemEntity.getItem_no()).setItem_name(itemEntity.getItem_name());
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("名称",itemData.getItem_name());
            resultJson.put("退货金额",NumberUtil.getDouble2Format(itemData.getTotal_ret()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchSaleRejectRetlv(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String cls_name;
            double total_sale;
            double total_ret;

            public String getCls_name() {
                return cls_name;
            }

            public void setCls_name(String cls_name) {
                this.cls_name = cls_name;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_ret() {
                return total_ret;
            }

            public void setTotal_ret(double total_ret) {
                this.total_ret = total_ret;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new HashMap<>();
//        List<ClsDayEntity> clsDayEntities = clsDayDao.list("where oper_date<= '"+end+"'" +
//                " and oper_date>= '"+begin+"' and org_id = '"+orgId+"'"+branchSql);
        List<Object> objects = unionSqlDao.query("select item_clsno, sum(sale_amt), sum(ret_qty) from ClsDayEntity where org_id ='"+orgId+"' and oper_date<= '"+end+"' and oper_date>= '"+begin+"'"+
                branchSql +" group by item_clsno");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String cls_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Double ret_qty = (Double)itemEntityObj[2];
            ItemData itemData = dataMap.get(cls_no);
            if(itemData!=null){
                itemData.setTotal_ret(itemData.getTotal_ret()+(ret_qty!=null?ret_qty:0));
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_ret(itemData.getTotal_ret()+(ret_qty!=null?ret_qty:0));
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(cls_no,itemData);
            }
        }

        //分类名
        String cls_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(cls_nosStr)){
                cls_nosStr = cls_nosStr + ",'"+str+"'";
            }else{
                cls_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(cls_nosStr)){
            List<ItemClsEntity> itemEntities = itemClsDao.list("where item_clsno in("+cls_nosStr+") and org_id = '"+orgId+"'");
            for(ItemClsEntity itemEntity : itemEntities){
                dataMap.get(itemEntity.getItem_clsno()).setCls_name(itemEntity.getItem_clsname());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("类别",key);
            resultJson.put("实销金额",NumberUtil.getDouble2Format(itemData.getTotal_sale()));
            resultJson.put("退货金额",NumberUtil.getDouble2Format(itemData.getTotal_ret()));
            resultJson.put("退货率",NumberUtil.getDouble2Format(itemData.getTotal_ret()/itemData.getTotal_sale()*100));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchClsRejectFenbu(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String cls_name;
            double total_ret;

            public String getCls_name() {
                return cls_name;
            }

            public void setCls_name(String cls_name) {
                this.cls_name = cls_name;
            }

            public double getTotal_ret() {
                return total_ret;
            }

            public void setTotal_ret(double total_ret) {
                this.total_ret = total_ret;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new LinkedHashMap<>();
//        List<ClsDayEntity> clsDayEntities = clsDayDao.list("where to_days(oper_date) <= (to_days('"+end+"'))" +
//                " and to_days(oper_date) >= (to_days('"+begin+"')) and org_id = '"+orgId+"'"+branchSql+" order by ret_amt desc ");
        List<Object> objects = unionSqlDao.query("select item_clsno, sum(ret_qty) from ClsDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days('"+end+"')) and to_days(oper_date) >= (to_days('"+begin+"'))"+
                branchSql+" group by item_clsno order by sum(ret_qty) desc");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String cls_no = ((String)itemEntityObj[0]);
            Double ret_qty = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(cls_no);
            if(itemData!=null){
                itemData.setTotal_ret(itemData.getTotal_ret()+(ret_qty!=null?ret_qty:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_ret(itemData.getTotal_ret()+(ret_qty!=null?ret_qty:0));
                dataMap.put(cls_no,itemData);
            }
        }

        //分类名
        String cls_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(cls_nosStr)){
                cls_nosStr = cls_nosStr + ",'"+str+"'";
            }else{
                cls_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(cls_nosStr)){
            List<ItemClsEntity> itemEntities = itemClsDao.list("where item_clsno in("+cls_nosStr+") and org_id = '"+orgId+"'");
            for(ItemClsEntity itemEntity : itemEntities){
                dataMap.get(itemEntity.getItem_clsno()).setCls_name(itemEntity.getItem_clsname());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",itemData.getCls_name());
            resultJson.put("value",NumberUtil.getDouble2Format(itemData.getTotal_ret()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchItemRejectDetail(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String item_name;
            double ret_cnt;
            double total_ret_amt;

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getRet_cnt() {
                return ret_cnt;
            }

            public void setRet_cnt(double ret_cnt) {
                this.ret_cnt = ret_cnt;
            }

            public double getTotal_ret_amt() {
                return total_ret_amt;
            }

            public void setTotal_ret_amt(double total_ret_amt) {
                this.total_ret_amt = total_ret_amt;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = Maps.newLinkedHashMap();
//        List<ItemDayEntity> itemDayEntities = itemDayDao.list("where to_days(oper_date) <= (to_days('"+end+"'))" +
//                " and  to_days(oper_date) >= (to_days('"+begin+"')) and org_id = '"+orgId+"'"+branchSql+" order by ret_amt desc ");
        List<Object> objects = unionSqlDao.query("select item_no, sum(ret_qty), sum(ret_amt) from ItemDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days('"+end+"')) and to_days(oper_date) >= (to_days('"+begin+"'))"+
                branchSql+" group by item_no order by sum(ret_amt) desc",1,20);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_no = ((String)itemEntityObj[0]);
            Double ret_qty = (Double)itemEntityObj[1];
            Double ret_amt = (Double)itemEntityObj[2];
            ItemData itemData = dataMap.get(item_no);
            if(itemData!=null){
                itemData.setRet_cnt(itemData.getRet_cnt()+(ret_qty!=null?ret_qty:0));
                itemData.setTotal_ret_amt(itemData.getTotal_ret_amt()+(ret_amt!=null?ret_amt:0));
            }else{
                itemData = new ItemData();
                itemData.setRet_cnt(itemData.getRet_cnt()+(ret_qty!=null?ret_qty:0));
                itemData.setTotal_ret_amt(itemData.getTotal_ret_amt()+(ret_amt!=null?ret_amt:0));
                dataMap.put(item_no,itemData);
            }
        }
        //分类名
        String item_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_nosStr)){
                item_nosStr = item_nosStr + ",'"+str+"'";
            }else{
                item_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_nosStr)){
            List<ItemEntity> itemEntities = itemDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'");
            for(ItemEntity itemEntity : itemEntities){
                dataMap.get(itemEntity.getItem_no()).setItem_name(itemEntity.getItem_name());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String item_no = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("条码",item_no);
            resultJson.put("自编码",item_no);
            resultJson.put("名称",itemData.getItem_name());
            resultJson.put("数量",itemData.getRet_cnt());
            resultJson.put("金额",itemData.getTotal_ret_amt());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchRetRetlv(String orgId, String begin, String end, String branchNo) throws BaseException {

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
//        List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) <= (to_days('"+end+"'))" +
//                " and  to_days(oper_date) >= (to_days('"+begin+"')) and org_id = '"+orgId+"'"+branchSql+"");
        double total_ret = 0;
        double total_sale = 0;
        double total_ret_cnt = 0;
        double total_cnt = 0;
        List<Object> objects = unionSqlDao.query("select sum(ret_amt),sum(dis_amt),sum(ret_qty),sum(sale_qty) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days('"+end+"')) and to_days(oper_date) >= (to_days('"+begin+"'))"+
                branchSql);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            Double ret_amt = ((Double)itemEntityObj[0]);
            Double dis_amt = (Double)itemEntityObj[1];
            Double ret_qty = (Double)itemEntityObj[2];
            Double sale_qty = (Double)itemEntityObj[3];

            total_ret += (ret_amt!=null?ret_amt:0);
            total_sale += (dis_amt!=null?dis_amt:0);
            total_ret_cnt += (ret_qty!=null?ret_qty:0);
            total_cnt += (sale_qty!=null?sale_qty:0);
        }

        JSONArray results = new JSONArray();
        JSONObject result = new JSONObject();
        result.put("name","退货金额");
        result.put("value",total_ret);
        result.put("max",total_sale);
        results.add(result);

        JSONObject result2 = new JSONObject();
        result2.put("name","退货率");
        result2.put("value",NumberUtil.getDouble2Format(total_ret_cnt/total_cnt*100));
        result2.put("max",100);
        results.add(result2);
        return results;
    }

}
