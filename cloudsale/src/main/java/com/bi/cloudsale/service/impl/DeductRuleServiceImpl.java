package com.bi.cloudsale.service.impl;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.dto.deductrule.*;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.service.DeductRuleService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

@Service
@Transactional
public class DeductRuleServiceImpl implements DeductRuleService {

    private static final Logger log = LoggerFactory.getLogger(DeductRuleServiceImpl.class);

    @Resource
    private DeductRuleMasterDao deductRuleMasterDao;
    @Resource
    private DeductRuleDetailDao deductRuleDetailDao;
    @Resource
    private DeductRuleExceptionDao deductRuleExceptionDao;
    @Resource
    private DeductRuleTargetDao deductRuleTargetDao;
    @Resource
    private DeductRuleRangeDao deductRuleRangeDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BrandDao brandDao;



    @Override
    public List<DeductRuleMasterDto> queryRules(String begin, String end, String branchNos, String ruleName, Long isEnable,String orgId, String gmt_modify) throws BaseException {
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";

        StringBuffer sqlBuffer = new StringBuffer();
        sqlBuffer.append(" where 1=1 and org_id='"+orgId+"'"+branchSql);
        if(StringUtils.isNotEmpty(ruleName)){
            sqlBuffer.append(" and deduct_name like '%"+ruleName+"%'");
        }
        if(isEnable!=null&&isEnable!=-1){
            sqlBuffer.append(" and is_enable = "+isEnable+"");
        }
        if(begin!=null&&end!=null){
            sqlBuffer.append(" and begin_date <= '"+ end+"'");
            sqlBuffer.append(" and begin_date >= '"+ begin+"'");
        }
        if(StringUtils.isNotEmpty(gmt_modify)) {
        		sqlBuffer.append(" and gmt_modified > '"+ gmt_modify+"'");
        		sqlBuffer.append(" order by gmt_modified");
        }else {
        		sqlBuffer.append(" order by begin_date");
        }
        
        List<DeductRuleMasterEntity> deductRuleMasterEntities = deductRuleMasterDao.list(sqlBuffer.toString());
        List<DeductRuleMasterDto> deductRuleMasterDtos = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(deductRuleMasterEntities)) {
            for (DeductRuleMasterEntity deductRuleMasterEntity : deductRuleMasterEntities) {
                DeductRuleMasterDto deductRuleMasterDto = new DeductRuleMasterDto();

                deductRuleMasterDto.setBegin_date(DateUtil.formatDate(deductRuleMasterEntity.getBegin_date(), DateUtil.SIMPLE_DATE_FORMAT));
                deductRuleMasterDto.setEnd_date(DateUtil.formatDate(deductRuleMasterEntity.getEnd_date(), DateUtil.SIMPLE_DATE_FORMAT));

                deductRuleMasterDto.setCal_mode(deductRuleMasterEntity.getCal_mode());
                deductRuleMasterDto.setCal_type(deductRuleMasterEntity.getCal_type());
                deductRuleMasterDto.setConfirm_id(deductRuleMasterEntity.getConfirm_id());
                deductRuleMasterDto.setDeduct_desc(deductRuleMasterEntity.getDeduct_desc());
                deductRuleMasterDto.setDeduct_mode(deductRuleMasterEntity.getDeduct_mode());
                deductRuleMasterDto.setDeduct_name(deductRuleMasterEntity.getDeduct_name());
                deductRuleMasterDto.setDeduct_no(deductRuleMasterEntity.getDeduct_no());
                deductRuleMasterDto.setDeduct_rate(deductRuleMasterEntity.getDeduct_rate());
                deductRuleMasterDto.setFull_mode(deductRuleMasterEntity.getFull_mode());
                deductRuleMasterDto.setGmt_create(deductRuleMasterEntity.getGmt_create().getTime());
                deductRuleMasterDto.setGmt_modified(deductRuleMasterEntity.getGmt_modified().getTime());
                deductRuleMasterDto.setId(deductRuleMasterEntity.getId());
                deductRuleMasterDto.setIs_enable(deductRuleMasterEntity.getIs_enable());
                deductRuleMasterDto.setReward_cal(deductRuleMasterEntity.getReward_cal());
                deductRuleMasterDto.setReward_money(deductRuleMasterEntity.getReward_money());
                deductRuleMasterDto.setUse_range(deductRuleMasterEntity.getUse_range());
                deductRuleMasterDto.setUser_id(deductRuleMasterEntity.getUser_id());
                deductRuleMasterDtos.add(deductRuleMasterDto);
            }
        }
        return deductRuleMasterDtos;
    }

    @Override
    public String setEnable(String deductNo, Long isEnable,String userId) throws BaseException {
        DeductRuleMasterEntity deductRuleMasterEntity = deductRuleMasterDao.findByDeductNo(deductNo);
        if(deductRuleMasterEntity!=null){
            deductRuleMasterEntity.setIs_enable(isEnable);
            deductRuleMasterEntity.setUser_id(userId);
            deductRuleMasterDao.updateEntity(deductRuleMasterEntity);
        }
        return "ok";
    }

    @Override
    public void saveOrUpdateRule(String orgId,String userId,DeductRuleDto deductRuleDto) throws BaseException {
        DeductRuleMasterDto deductRuleMasterDto = deductRuleDto.getDeductRuleMasterDto();
        List<DeductRuleTargetDto> deductRuleTargetDtoList = deductRuleDto.getDeductRuleTargetDtoList();
        List<DeductRuleDetailDto> deductRuleDetailDtoList = deductRuleDto.getDeductRuleDetailDtoList();
        List<DeductRuleExceptionDto> deductRuleExceptionDtoList = deductRuleDto.getDeductRuleExceptionDtoList();
        //0.参数校验
//        if(deductRuleMasterDto==null||
//                CollectionUtils.isEmpty(deductRuleTargetDtoList)||
//                CollectionUtils.isEmpty(deductRuleDetailDtoList)){
//            throw new BaseException("-1","奖励规则与商品明细不能为空");
//        }
        String deductNo = deductRuleMasterDto.getDeduct_no();
        //1.已存在规则
        if(StringUtils.isNotEmpty(deductNo)){
            //1.1删除所有相关记录，保留reductNo
            deductRuleMasterDao.deleteByDeductNo(deductNo);
            deductRuleDetailDao.deleteByDeductNo(deductNo);
            deductRuleRangeDao.deleteByDeductNo(deductNo);
            deductRuleTargetDao.deleteByDeductNo(deductNo);
            deductRuleExceptionDao.deleteByDeductNo(deductNo);
        }else{//2.不存在规则
            //2.1.新建ductNo
            deductNo = "IC"+DateUtil.formatDate(new Date(),"yyyyMMdd"+((int)(Math.random()*(9999-1000+1))+1000));
        }
        //3插master表
        DeductRuleMasterEntity deductRuleMasterEntity = convertToMaster(orgId,userId,deductNo,deductRuleMasterDto);
        deductRuleMasterDao.save(deductRuleMasterEntity);
        String use_range = deductRuleMasterDto.getUse_range();
        if(Integer.valueOf(use_range)>0){
            List<DeductRuleRangeDto> deductRuleRangeDtoList = new ArrayList<>();
            //4如果use_range>0,插range表
            if(StringUtils.equals("1",use_range)){
               //1自定义门店
                List<String>branch_nos = deductRuleMasterDto.getBranch_nos();
                for(String branch_no:branch_nos){
                    DeductRuleRangeDto deductRuleRangeDto = new DeductRuleRangeDto();
                    deductRuleRangeDto.setBranch_no(branch_no);
                    deductRuleRangeDtoList.add(deductRuleRangeDto);
                }
            }else if(StringUtils.equals("2",use_range)){
                //2自定义员工
                List<String> saler_ids = deductRuleMasterDto.getSaler_ids();
                for(String saler_id:saler_ids){
                    DeductRuleRangeDto deductRuleRangeDto = new DeductRuleRangeDto();
                    deductRuleRangeDto.setSale_id(saler_id);
                    deductRuleRangeDtoList.add(deductRuleRangeDto);
                }
            }
            List<DeductRuleRangeEntity> deductRuleMasterEntities = convertToRanges(orgId,userId,deductNo,deductRuleRangeDtoList);
            for(DeductRuleRangeEntity deductRuleRangeEntity:deductRuleMasterEntities){
                deductRuleRangeDao.save(deductRuleRangeEntity);
            }
        }
        String deductMode = deductRuleMasterDto.getDeduct_mode();
        //5插detail表
        List<DeductRuleDetailEntity> deductRuleDetailEntities = convertToDetails(orgId,userId,deductNo,deductMode,deductRuleDetailDtoList);
        for(DeductRuleDetailEntity deductRuleDetailEntity:deductRuleDetailEntities){
            deductRuleDetailDao.save(deductRuleDetailEntity);
        }
        //6插target表
        List<DeductRuleTargetEntity> deductRuleTargetEntities = convertToTargets(orgId,userId,deductNo,deductRuleTargetDtoList);
        for(DeductRuleTargetEntity deductRuleTargetEntity:deductRuleTargetEntities){
            deductRuleTargetDao.save(deductRuleTargetEntity);
        }
        //7插exception表
        List<DeductRuleExceptionEntity> deductRuleExceptionEntities = convertToExceptions(orgId,userId,deductNo,deductRuleExceptionDtoList);
        for(DeductRuleExceptionEntity deductRuleExceptionEntity:deductRuleExceptionEntities){
            deductRuleExceptionDao.save(deductRuleExceptionEntity);
        }
    }


    @Override
    public DeductRuleDto ruleDetail(String deductNo) throws BaseException {
        DeductRuleDto deductRuleDto = new DeductRuleDto();

        DeductRuleMasterEntity deductRuleMasterEntity = deductRuleMasterDao.findByDeductNo(deductNo);
        if(deductRuleMasterEntity == null) {
        	throw new BaseException("-1","没有查到对应方案"+deductNo);
        }
        DeductRuleMasterDto deductRuleMasterDto = convertToMaster(deductRuleMasterEntity);
        deductRuleDto.setDeductRuleMasterDto(deductRuleMasterDto);

        List<DeductRuleTargetEntity> deductRuleTargetEntities = deductRuleTargetDao.findByDeductNo(deductNo);
        List<DeductRuleTargetDto> deductRuleTargetDtoList = convertToTargets(deductRuleTargetEntities);
        deductRuleDto.setDeductRuleTargetDtoList(deductRuleTargetDtoList);

        List<DeductRuleDetailEntity> deductRuleDetailEntities = deductRuleDetailDao.findByDeductNo(deductNo);
        List<DeductRuleDetailDto> deductRuleDetailDtoList = convertToDetails(deductRuleDetailEntities,deductRuleMasterDto.getDeduct_mode());
        deductRuleDto.setDeductRuleDetailDtoList(deductRuleDetailDtoList);

        List<DeductRuleRangeEntity> deductRuleRangeEntities = deductRuleRangeDao.findByDeductNo(deductNo);
        deductRuleDto.setDeductRuleRangeEntities(deductRuleRangeEntities);

        List<DeductRuleRangeDto> deductRuleRangeDtoList = convertToRanges(deductRuleRangeEntities);
        if(CollectionUtils.isNotEmpty(deductRuleRangeDtoList)){
            if(StringUtils.equals("1",deductRuleMasterDto.getUse_range())){
                List<String>branch_nos = new ArrayList<>();
                for(DeductRuleRangeDto deductRuleRangeDto:deductRuleRangeDtoList){
                    branch_nos.add(deductRuleRangeDto.getBranch_no());
                }
                deductRuleMasterDto.setBranch_nos(branch_nos);
            }else if(StringUtils.equals("2",deductRuleMasterDto.getUse_range())){
                List<String>saler_ids = new ArrayList<>();
                for(DeductRuleRangeDto deductRuleRangeDto:deductRuleRangeDtoList){
                    saler_ids.add(deductRuleRangeDto.getSale_id());
                }
                deductRuleMasterDto.setSaler_ids(saler_ids);
            }
        }
        List<DeductRuleExceptionEntity> deductRuleExceptionEntities = deductRuleExceptionDao.findByDeductNo(deductNo);
        List<DeductRuleExceptionDto> deductRuleExceptionDtoList = convertToExceptions(deductRuleExceptionEntities);
        deductRuleDto.setDeductRuleExceptionDtoList(deductRuleExceptionDtoList);

        return deductRuleDto;
    }

    DeductRuleMasterEntity convertToMaster(String orgId,String userId,String deductNo,DeductRuleMasterDto deductRuleMasterDto){
        DeductRuleMasterEntity deductRuleMasterEntity = new DeductRuleMasterEntity();
        deductRuleMasterEntity.setUser_id(userId);
        deductRuleMasterEntity.setIs_enable(1L);

        if(StringUtils.equals("true",deductRuleMasterDto.getLong_valid())){
        	deductRuleMasterEntity.setBegin_date(new Timestamp(new Date().getTime()));
            deductRuleMasterEntity.setEnd_date(new Timestamp(DateUtil.formatStringToDate("2222-12-31",DateUtil.SIMPLE_DATE_FORMAT).getTime()));
        }else {
        	String begin_date = deductRuleMasterDto.getBegin_date();
            String end_date = deductRuleMasterDto.getEnd_date();
        	deductRuleMasterEntity.setBegin_date(new Timestamp(DateUtil.formatStringToDate(begin_date,DateUtil.SIMPLE_DATE_FORMAT).getTime()));
            deductRuleMasterEntity.setEnd_date(new Timestamp(DateUtil.formatStringToDate(end_date,DateUtil.SIMPLE_DATE_FORMAT).getTime()));
        }
        deductRuleMasterEntity.setCal_mode(deductRuleMasterDto.getCal_mode());
        deductRuleMasterEntity.setCal_type(deductRuleMasterDto.getCal_type());
        deductRuleMasterEntity.setConfirm_id("");
        deductRuleMasterEntity.setDeduct_desc(deductRuleMasterDto.getDeduct_desc());
        deductRuleMasterEntity.setDeduct_mode(deductRuleMasterDto.getDeduct_mode());
        deductRuleMasterEntity.setDeduct_name(deductRuleMasterDto.getDeduct_name());
        deductRuleMasterEntity.setDeduct_no(deductNo);
        deductRuleMasterEntity.setDeduct_rate(deductRuleMasterDto.getDeduct_rate1()+"|"+deductRuleMasterDto.getDeduct_rate2()+"|"+deductRuleMasterDto.getDeduct_rate3()+"|"+deductRuleMasterDto.getDeduct_rate4());

        deductRuleMasterEntity.setFull_mode(deductRuleMasterDto.getFull_mode());
        deductRuleMasterEntity.setMin_money(deductRuleMasterDto.getMin_money());
        deductRuleMasterEntity.setOper_date(new Timestamp(System.currentTimeMillis()));
        deductRuleMasterEntity.setOper_id(userId);
        deductRuleMasterEntity.setOrg_id(orgId);
        deductRuleMasterEntity.setReward_cal(deductRuleMasterDto.getReward_cal());
        deductRuleMasterEntity.setReward_money(deductRuleMasterDto.getReward_money());
        deductRuleMasterEntity.setUse_range(deductRuleMasterDto.getUse_range());
        deductRuleMasterEntity.setWork_date(null);
        return deductRuleMasterEntity;
    }

    List<DeductRuleRangeEntity> convertToRanges(String orgId,String userId,String deductNo,List<DeductRuleRangeDto> deductRuleRangeDtoList){
        List<DeductRuleRangeEntity> deductRuleRangeEntities = new ArrayList<>();
        long index = 1L;
        for(DeductRuleRangeDto deductRuleRangeDto :deductRuleRangeDtoList){
            DeductRuleRangeEntity deductRuleRangeEntity = new DeductRuleRangeEntity();
            deductRuleRangeEntity.setDeduct_no(deductNo);
            deductRuleRangeEntity.setBranch_no(deductRuleRangeDto.getBranch_no());
            deductRuleRangeEntity.setSale_id(deductRuleRangeDto.getSale_id());
            deductRuleRangeEntity.setOrg_id(orgId);
            deductRuleRangeEntity.setUser_id(userId);
            deductRuleRangeEntities.add(deductRuleRangeEntity);
            index++;
        }
        return deductRuleRangeEntities;
    }

    List<DeductRuleDetailEntity> convertToDetails(String orgId,String userId,String deductNo,String deductMode,List<DeductRuleDetailDto> deductRuleDetailDtoList){
        List<DeductRuleDetailEntity> deductRuleDetailEntities = new ArrayList<>();
        long index = 1L;
        for(DeductRuleDetailDto deductRuleDetailDto :deductRuleDetailDtoList){
            DeductRuleDetailEntity deductRuleDetailEntity = new DeductRuleDetailEntity();
            deductRuleDetailEntity.setDeduct_no(deductNo);
            deductRuleDetailEntity.setFlow_id(index);
            deductRuleDetailEntity.setDeduct_value(deductRuleDetailDto.getDeduct_value());
            if("3".equals(deductMode)){
                deductRuleDetailEntity.setGroup_no("1");
            }
            deductRuleDetailEntity.setInfo_no(deductRuleDetailDto.getInfo_no());
            deductRuleDetailEntity.setOther1(deductRuleDetailDto.getOther1());
            deductRuleDetailEntity.setOther2(deductRuleDetailDto.getOther2());
            deductRuleDetailEntity.setOther3(deductRuleDetailDto.getOther3());
            deductRuleDetailEntity.setOrg_id(orgId);
            deductRuleDetailEntity.setUser_id(userId);
            deductRuleDetailEntities.add(deductRuleDetailEntity);
            index++;
        }
        return deductRuleDetailEntities;
    }

    List<DeductRuleTargetEntity> convertToTargets(String orgId,String userId,String deductNo,List<DeductRuleTargetDto> deductRuleTargetDtoList){
        List<DeductRuleTargetEntity> deductRuleTargetEntities = new ArrayList<>();
        long index = 1L;
        for(DeductRuleTargetDto deductRuleTargetDto :deductRuleTargetDtoList){
            DeductRuleTargetEntity deductRuleTargetEntity = new DeductRuleTargetEntity();
            deductRuleTargetEntity.setDeduct_no(deductNo);
            deductRuleTargetEntity.setFlow_id(index);
            deductRuleTargetEntity.setCal_type(deductRuleTargetDto.getCal_type());
            deductRuleTargetEntity.setPunish_money(deductRuleTargetDto.getPunish_money());
            deductRuleTargetEntity.setReward_value(deductRuleTargetDto.getReward_value());
            deductRuleTargetEntity.setTarget_money(deductRuleTargetDto.getTarget_money());
            deductRuleTargetEntity.setOrg_id(orgId);
            deductRuleTargetEntity.setUser_id(userId);
            deductRuleTargetEntities.add(deductRuleTargetEntity);
            index++;
        }
        return deductRuleTargetEntities;
    }

    List<DeductRuleExceptionEntity> convertToExceptions(String orgId,String userId,String deductNo,List<DeductRuleExceptionDto> deductRuleExceptionDtoList){
        List<DeductRuleExceptionEntity> deductRuleExceptionEntities = new ArrayList<>();
        long index = 1L;
        for(DeductRuleExceptionDto deductRuleExceptionDto :deductRuleExceptionDtoList){
            DeductRuleExceptionEntity deductRuleExceptionEntity = new DeductRuleExceptionEntity();
            deductRuleExceptionEntity.setDeduct_no(deductNo);
            deductRuleExceptionEntity.setFlow_id(index);
            deductRuleExceptionEntity.setItem_no(deductRuleExceptionDto.getItem_no());
            deductRuleExceptionEntity.setOrg_id(orgId);
            deductRuleExceptionEntity.setUser_id(userId);
            deductRuleExceptionEntities.add(deductRuleExceptionEntity);
            index++;
        }
        return deductRuleExceptionEntities;
    }

    DeductRuleMasterDto convertToMaster(DeductRuleMasterEntity deductRuleMasterEntity){
        DeductRuleMasterDto deductRuleMasterDto = new DeductRuleMasterDto();
        if(StringUtils.equals("2222-12-31",DateUtil.formatDate(new Date(deductRuleMasterEntity.getEnd_date().getTime()),DateUtil.SIMPLE_DATE_FORMAT))){
            deductRuleMasterDto.setLong_valid("true");
        }else{
        	deductRuleMasterDto.setBegin_date(DateUtil.formatDate(deductRuleMasterEntity.getBegin_date(), DateUtil.SIMPLE_DATE_FORMAT));
            deductRuleMasterDto.setEnd_date(DateUtil.formatDate(deductRuleMasterEntity.getEnd_date(), DateUtil.SIMPLE_DATE_FORMAT));
            deductRuleMasterDto.setLong_valid("0");
        }
        deductRuleMasterDto.setCal_mode(deductRuleMasterEntity.getCal_mode());
        deductRuleMasterDto.setCal_type(deductRuleMasterEntity.getCal_type());
        deductRuleMasterDto.setConfirm_id(deductRuleMasterEntity.getConfirm_id());
        deductRuleMasterDto.setDeduct_desc(deductRuleMasterEntity.getDeduct_desc());
        deductRuleMasterDto.setDeduct_mode(deductRuleMasterEntity.getDeduct_mode());
        deductRuleMasterDto.setDeduct_name(deductRuleMasterEntity.getDeduct_name());
        deductRuleMasterDto.setDeduct_no(deductRuleMasterEntity.getDeduct_no());
        deductRuleMasterDto.setDeduct_rate(deductRuleMasterEntity.getDeduct_rate());
        deductRuleMasterDto.setDeduct_rate1(deductRuleMasterEntity.getDeduct_rate().split("\\|")[0]);
        deductRuleMasterDto.setDeduct_rate2(deductRuleMasterEntity.getDeduct_rate().split("\\|")[1]);
        deductRuleMasterDto.setDeduct_rate3(deductRuleMasterEntity.getDeduct_rate().split("\\|")[2]);
        deductRuleMasterDto.setDeduct_rate4(deductRuleMasterEntity.getDeduct_rate().split("\\|")[3]);
        deductRuleMasterDto.setFull_mode(deductRuleMasterEntity.getFull_mode());
        deductRuleMasterDto.setGmt_create(deductRuleMasterEntity.getGmt_create().getTime());
        deductRuleMasterDto.setGmt_modified(deductRuleMasterEntity.getGmt_modified().getTime());
        deductRuleMasterDto.setId(deductRuleMasterEntity.getId());
        deductRuleMasterDto.setIs_enable(deductRuleMasterEntity.getIs_enable());
        deductRuleMasterDto.setMin_money(deductRuleMasterEntity.getMin_money());
        deductRuleMasterDto.setOper_date(deductRuleMasterEntity.getOper_date().getTime());
        deductRuleMasterDto.setOper_id(deductRuleMasterEntity.getOper_id());
        deductRuleMasterDto.setOrg_id(deductRuleMasterEntity.getOrg_id());
        deductRuleMasterDto.setReward_cal(deductRuleMasterEntity.getReward_cal());
        deductRuleMasterDto.setReward_money(deductRuleMasterEntity.getReward_money());
        deductRuleMasterDto.setUse_range(deductRuleMasterEntity.getUse_range());
        deductRuleMasterDto.setWork_date(deductRuleMasterEntity.getWork_date()!=null?deductRuleMasterEntity.getWork_date().getTime():null);
        return deductRuleMasterDto;
    }

    List<DeductRuleTargetDto> convertToTargets(List<DeductRuleTargetEntity> deductRuleTargetEntities){
        List<DeductRuleTargetDto> deductRuleTargetDtoList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(deductRuleTargetEntities)) {
            for(DeductRuleTargetEntity deductRuleTargetEntity : deductRuleTargetEntities){
                DeductRuleTargetDto deductRuleTargetDto = new DeductRuleTargetDto();
                deductRuleTargetDto.setCal_type(deductRuleTargetEntity.getCal_type());
                deductRuleTargetDto.setDeduct_no(deductRuleTargetEntity.getDeduct_no());
                deductRuleTargetDto.setFlow_id(deductRuleTargetEntity.getFlow_id());
                deductRuleTargetDto.setGmt_create(deductRuleTargetEntity.getCal_type());
                deductRuleTargetDto.setGmt_modified(deductRuleTargetEntity.getGmt_modified().getTime());
                deductRuleTargetDto.setId(deductRuleTargetEntity.getId());
                deductRuleTargetDto.setOrg_id(deductRuleTargetEntity.getOrg_id());
                deductRuleTargetDto.setPunish_money(deductRuleTargetEntity.getPunish_money());
                deductRuleTargetDto.setReward_value(deductRuleTargetEntity.getReward_value());
                deductRuleTargetDto.setTarget_money(deductRuleTargetEntity.getTarget_money());
                deductRuleTargetDto.setUser_id(deductRuleTargetEntity.getUser_id());
                deductRuleTargetDtoList.add(deductRuleTargetDto);
            }
        }
        return deductRuleTargetDtoList;
    }

    List<DeductRuleDetailDto> convertToDetails(List<DeductRuleDetailEntity> deductRuleDetailEntities,String deduct_mode){
        List<DeductRuleDetailDto> deductRuleDetailDtoList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(deductRuleDetailEntities)) {
            for(DeductRuleDetailEntity deductRuleDetailEntity : deductRuleDetailEntities){
                DeductRuleDetailDto deductRuleDetailDto = new DeductRuleDetailDto();
                deductRuleDetailDto.setDeduct_no(deductRuleDetailEntity.getDeduct_no());
                deductRuleDetailDto.setDeduct_value(deductRuleDetailEntity.getDeduct_value());
                deductRuleDetailDto.setFlow_id(deductRuleDetailEntity.getFlow_id());
                deductRuleDetailDto.setGmt_create(deductRuleDetailEntity.getGmt_create().getTime());
                deductRuleDetailDto.setGmt_modified(deductRuleDetailEntity.getGmt_modified().getTime());
                deductRuleDetailDto.setGroup_no(deductRuleDetailEntity.getGroup_no());
                deductRuleDetailDto.setId(deductRuleDetailEntity.getId());
                deductRuleDetailDto.setInfo_no(deductRuleDetailEntity.getInfo_no());
                if(StringUtils.equals(deduct_mode,"0")||StringUtils.equals(deduct_mode,"3")){
                    String item_no = deductRuleDetailEntity.getInfo_no();
                    List<ItemEntity> itemEntities = itemDao.list("where item_no ='"+item_no+"'");
                    //商品名
                    deductRuleDetailDto.setInfo_name(itemEntities.get(0).getItem_name());
                }else if(StringUtils.equals(deduct_mode,"1")){
                    //分类名
                    String item_clsno = deductRuleDetailEntity.getInfo_no();
                    List<ItemClsEntity> clsEntities = itemClsDao.list("where item_clsno ='"+item_clsno+"'");
                    deductRuleDetailDto.setInfo_name(clsEntities.get(0).getItem_clsname());
                }else if(StringUtils.equals(deduct_mode,"2")){
                    //品牌名
                    String item_brandno = deductRuleDetailEntity.getInfo_no();
                    List<BrandEntity> brandEntities = brandDao.list("where item_brandno ='"+item_brandno+"'");
                    deductRuleDetailDto.setInfo_name(brandEntities.get(0).getItem_brandname());
                }
                deductRuleDetailDto.setOrg_id(deductRuleDetailEntity.getOrg_id());
                deductRuleDetailDto.setOther1(deductRuleDetailEntity.getOther1());
                deductRuleDetailDto.setOther2(deductRuleDetailEntity.getOther2());
                deductRuleDetailDto.setOther3(deductRuleDetailEntity.getOther3());
                deductRuleDetailDto.setUser_id(deductRuleDetailEntity.getUser_id());
                deductRuleDetailDtoList.add(deductRuleDetailDto);
            }
        }
        return deductRuleDetailDtoList;
    }

    List<DeductRuleRangeDto> convertToRanges(List<DeductRuleRangeEntity> deductRuleRangeEntities){
        List<DeductRuleRangeDto> deductRuleRangeDtoList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(deductRuleRangeEntities)) {
            for(DeductRuleRangeEntity deductRuleRangeEntity : deductRuleRangeEntities){
                DeductRuleRangeDto deductRuleRangeDto = new DeductRuleRangeDto();
                deductRuleRangeDto.setBranch_no(deductRuleRangeEntity.getBranch_no());
                deductRuleRangeDto.setDeduct_no(deductRuleRangeEntity.getDeduct_no());
                deductRuleRangeDto.setGmt_create(deductRuleRangeEntity.getGmt_create().getTime());
                deductRuleRangeDto.setGmt_modified(deductRuleRangeEntity.getGmt_modified().getTime());
                deductRuleRangeDto.setId(deductRuleRangeEntity.getId());
                deductRuleRangeDto.setOrg_id(deductRuleRangeEntity.getOrg_id());
                deductRuleRangeDto.setSale_id(deductRuleRangeEntity.getSale_id());
                deductRuleRangeDto.setUser_id(deductRuleRangeEntity.getUser_id());
                deductRuleRangeDtoList.add(deductRuleRangeDto);
            }
        }
        return deductRuleRangeDtoList;
    }

    List<DeductRuleExceptionDto> convertToExceptions(List<DeductRuleExceptionEntity> deductRuleExceptionEntities){
        List<DeductRuleExceptionDto> deductRuleExceptionDtoList = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(deductRuleExceptionEntities)) {
            for(DeductRuleExceptionEntity deductRuleExceptionEntity : deductRuleExceptionEntities){
                DeductRuleExceptionDto deductRuleExceptionDto = new DeductRuleExceptionDto();
                deductRuleExceptionDto.setDeduct_no(deductRuleExceptionEntity.getDeduct_no());
                deductRuleExceptionDto.setFlow_id(deductRuleExceptionEntity.getFlow_id());
                deductRuleExceptionDto.setGmt_create(deductRuleExceptionEntity.getGmt_create().getTime());
                deductRuleExceptionDto.setGmt_modified(deductRuleExceptionEntity.getGmt_modified().getTime());
                deductRuleExceptionDto.setId(deductRuleExceptionEntity.getId());
                deductRuleExceptionDto.setItem_no(deductRuleExceptionEntity.getItem_no());
                String item_no = deductRuleExceptionEntity.getItem_no();
                //商品名
                List<ItemEntity> itemEntities = itemDao.list("where item_no ='"+item_no+"'");
                //商品名
                deductRuleExceptionDto.setItem_name(itemEntities.get(0).getItem_name());
                deductRuleExceptionDto.setOrg_id(deductRuleExceptionEntity.getOrg_id());
                deductRuleExceptionDto.setUser_id(deductRuleExceptionEntity.getUser_id());
                deductRuleExceptionDtoList.add(deductRuleExceptionDto);
            }
        }
        return deductRuleExceptionDtoList;
    }

	@Override
	public void deleteRule(String deductNo, String userId) throws BaseException {
		//1.已存在规则
        if(StringUtils.isNotEmpty(deductNo)){
            //1.1删除所有相关记录，保留reductNo
            deductRuleMasterDao.deleteByDeductNo(deductNo);
            deductRuleDetailDao.deleteByDeductNo(deductNo);
            deductRuleRangeDao.deleteByDeductNo(deductNo);
            deductRuleTargetDao.deleteByDeductNo(deductNo);
            deductRuleExceptionDao.deleteByDeductNo(deductNo);
        }
	}
}
