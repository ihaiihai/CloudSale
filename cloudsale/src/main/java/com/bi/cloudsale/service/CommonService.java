package com.bi.cloudsale.service;

import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.dto.OptionDto;

public interface CommonService {

	Map<String,Object> queryAllBranch(String orgId);
	
	Map<String,Object> queryAllCls(String orgId);
	
	Map<String,Object> queryAllBrand(String orgId);
	
	Map<String,Object> queryAllSaler(String orgId);
	
	List<OptionDto> queryBranchOptions(String orgId);
	
	List<OptionDto> queryClsOptions(String orgId);
	
	List<OptionDto> queryBrandOptions(String orgId);
	
	List<OptionDto> querySalerOptions(String orgId);

	JSONObject itemDetail(String orgId ,String itemNo);
	JSONObject itemClsDetail(String orgId, String itemClsNo);
	JSONObject itemBrandDetail(String orgId, String brandNo);
}
