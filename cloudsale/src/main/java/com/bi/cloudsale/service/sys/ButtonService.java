package com.bi.cloudsale.service.sys;

import java.util.List;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.ButtonDto;

public interface ButtonService {

	List<ButtonDto> queryAll() throws BaseException;
	
	List<OptionDto> queryAllOptions();
}
