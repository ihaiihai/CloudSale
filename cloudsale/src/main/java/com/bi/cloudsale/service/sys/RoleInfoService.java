package com.bi.cloudsale.service.sys;

import java.util.List;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.RoleInfoDto;

public interface RoleInfoService {

	void createRoleInfo(RoleInfoDto roleInfoDto) throws BaseException;
	
	void deleteRoleInfo(String roleCode) throws BaseException;
	
	void updateRoleInfo(RoleInfoDto roleInfoDto) throws BaseException;
	
	List<RoleInfoDto> queryByCondition(BaseQueryCondition baseQueryCondition);
	
	RoleInfoDto queryByCode(String roleCode);
	
	List<OptionDto> queryOptions(String userId);
}
