package com.bi.cloudsale.service.commision.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.dto.commision.SalarySalerDto;
import com.bi.cloudsale.persistent.dao.commision.SalarySalerDao;
import com.bi.cloudsale.persistent.entity.commision.SalarySalerEntity;
import com.bi.cloudsale.service.commision.SalarySalerService;
import com.bi.cloudsale.service.overall.impl.OperationServiceImpl;

@Service
@Transactional
public class SalarySalerServiceImpl implements SalarySalerService {
	
	private static final Logger log = LoggerFactory.getLogger(OperationServiceImpl.class);
	
	@Resource
	private SalarySalerDao salarSalerDao;

	@Override
	public List<SalarySalerDto> queryByCondition(String orgId, Date startDate, Date endDate, String branchNos,
			String salerIds) {
		List<SalarySalerDto> dtoList = new ArrayList<SalarySalerDto>();
		List<SalarySalerEntity> entityList = salarSalerDao.queryByCondition(orgId, startDate, endDate, branchNos, salerIds);
		try {
			BeanUtil.copyBeanPropertiesList(dtoList, entityList, SalarySalerDto.class);
		} catch (BaseException e) {
			log.error(e.getMessage(), e);
		}
		return dtoList;
	}

	@Override
	public void updateConfirm(String orgId, String account, Date startDate, Date endDate, String branchNos,
			String salerIds) throws BaseException {
		try {
			salarSalerDao.updateConfirm(orgId, account, startDate, endDate, branchNos, salerIds);
		}catch(HibernateException e) {
			throw new BaseException("更新失败", e);
		}
	}

}
