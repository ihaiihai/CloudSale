package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.BranchAimService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class BranchAimServiceImpl implements BranchAimService {

    private static final Logger log = LoggerFactory.getLogger(BranchAimServiceImpl.class);

    @Resource
    private BrandDao brandDao;
    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private SalerDayDao salerDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private BrandDayDao brandDayDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {

        List<BranchEntity> branchEntities = branchDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray brands(String orgId) throws BaseException {


        List<BrandEntity> brandEntities = brandDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BrandEntity brandEntity:brandEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("brand_no",brandEntity.getItem_brandno());
            jsonObject.put("brand_name",brandEntity.getItem_brandname());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray queryWholeAimGain(String orgId, String year,String month, String branchNos) throws BaseException {
        class ItemData{
            String branch_name;
            double year_aim;
            double year_sale;
            double month_sale;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public double getYear_aim() {
                return year_aim;
            }

            public void setYear_aim(double year_aim) {
                this.year_aim = year_aim;
            }

            public double getYear_sale() {
                return year_sale;
            }

            public void setYear_sale(double year_sale) {
                this.year_sale = year_sale;
            }

            public double getMonth_sale() {
                return month_sale;
            }

            public void setMonth_sale(double month_sale) {
                this.month_sale = month_sale;
            }
        }
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 0 " +
                "and cycle_year= '"+year+"' " +
                "and aim_type = 1"+branchSql);
        Map<String ,ItemData> dataMap = new HashMap<>();
        for(AimEntity aimEntity : aimEntities){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String branch_no = aimEntity.getBranch_no();
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setYear_aim(aim);
            }else{
                itemData = new ItemData();
                itemData.setYear_aim(aim);
                dataMap.put(branch_no,itemData);
            }
        }
//        List<BranchDayEntity> branchDayEntities_year = branchDayDao.list("where org_id ='"+orgId+"' and year(oper_date)<='"+year+"' and"+
//                " year(oper_date)>='"+year+"'"+branchSql);
        List<Object> objects = unionSqlDao.query("select branch_no as branch_no, sum(sale_amt)as sale_amt from BranchDayEntity where org_id ='"+orgId+"' and year(oper_date)<='"+year+"' and"+
                " year(oper_date)>='"+year+"'"+branchSql+" group by branch_no");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = (String)itemEntityObj[0];
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setYear_sale(itemData.getYear_sale()+(total_sale!=null?total_sale:0));
            }
        }
//        List<BranchDayEntity> branchDayEntities_month = branchDayDao.list("where org_id ='"+orgId+"' and year(oper_date)='"+year+"' and month(oper_date) = '"+month+"'"+branchSql);
        List<Object> objects_month = unionSqlDao.query("select branch_no as branch_no, sum(sale_amt)as sale_amt from BranchDayEntity where org_id ='"+orgId+"' and year(oper_date)='"+year+"' and"+
                " month(oper_date)>='"+month+"'"+branchSql+" group by branch_no");
        for(Object object : objects_month){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = (String)itemEntityObj[0];
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setMonth_sale(itemData.getMonth_sale()+(total_sale!=null?total_sale:0));
            }
        }
        //门店名
        String branch_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id ='"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                dataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
            }
        }
        Calendar ca = Calendar.getInstance();//创建一个日期实例
        ca.setTime(new Date());//实例化一个日期
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("门店编号",key);
            resultJson.put("门店名称",itemData.getBranch_name());
            resultJson.put("年度目标",NumberUtil.getDouble2Format(itemData.getYear_aim()));
            resultJson.put("今年完成",itemData.getYear_sale());
            resultJson.put("年进度",NumberUtil.getDouble2Format(itemData.getYear_sale()/itemData.getYear_aim()));
            resultJson.put("年任务预测",itemData.getYear_sale()/ca.get(Calendar.DAY_OF_YEAR)*365);
            resultJson.put("年完成率预测",NumberUtil.getDouble2Format(itemData.getYear_sale()/ca.get(Calendar.DAY_OF_YEAR)*365/itemData.getYear_aim()));
            resultJson.put("本月任务",NumberUtil.getDouble2Format(itemData.getYear_aim()/12));
            resultJson.put("本月完成",itemData.getMonth_sale());
            resultJson.put("月进度",NumberUtil.getDouble2Format(itemData.getMonth_sale()/(itemData.getYear_aim()/12)));
            resultJson.put("剩余任务",NumberUtil.getDouble2Format(itemData.getYear_aim()/12-itemData.getMonth_sale()));
            resultJson.put("剩余天数",ca.getActualMaximum(java.util.Calendar.DAY_OF_MONTH)-ca.get(Calendar.DAY_OF_MONTH));
            resultJson.put("本月任务预测",NumberUtil.getDouble2Format(itemData.getMonth_sale()/ca.get(Calendar.DAY_OF_MONTH)*java.util.Calendar.DAY_OF_MONTH));
            resultJson.put("预测完成率",NumberUtil.getDouble2Format(itemData.getMonth_sale()/ca.get(Calendar.DAY_OF_MONTH)*java.util.Calendar.DAY_OF_MONTH/(itemData.getYear_aim()/12)));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public String setAim(String orgId,String userId, JSONObject params) throws BaseException, ParseException {
        String branchNo =params.getString("areaCode");
        String clsNo = params.getString("clsCode");
        String brandNo = params.getString("brandCode");
        String salerNo = params.getString("salerCode");
//        Double key_value =params.getDouble("key_value");
        String flag =params.getString("flag");
        long year =params.getLong("oper_date");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = sdf.parse(oper_date);
        double yearValue = params.getDoubleValue("key_value");
        switch (flag){
	        case "ZT":
	            saveBranchYearAim(orgId,userId,branchNo,year,yearValue);
	            break;
	        case "PL":
	            saveClsYearAim(orgId,userId,branchNo,clsNo,year,yearValue);
	            break;
	        case "PP":
	            saveBrandYearAim(orgId,userId,branchNo,brandNo,year,yearValue);
	            break;
	        case "GR":
	            saveSalerYearAim(orgId,userId,branchNo,salerNo,year,yearValue);
	            break;
	    }
        JSONArray paramsArray = params.getJSONArray("month");
        for(int i=0;i<paramsArray.size();i++){
        		JSONObject jsonObject = paramsArray.getJSONObject(i);
            long month =jsonObject.getLong("month");
            double value = jsonObject.getDoubleValue("value");
            switch (flag){
                case "ZT":
                    saveBranchMonthAim(orgId,userId,branchNo,year,month,value);
                    break;
                case "PL":
                    saveClsMonthAim(orgId,userId,branchNo,clsNo,year,month,value);
                    break;
                case "PP":
                    saveBrandMonthAim(orgId,userId,branchNo,brandNo,year,month,value);
                    break;
                case "GR":
                    saveSalerMonthAim(orgId,userId,branchNo,salerNo,year,month,value);
                    break;
            }
        }
        return "OK";
    }

    private void saveBranchMonthAim(String orgId,String userId,String branch_no,long year,long month,double aim) {
        //1.检查是否存在已有的月份目标
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' and aim_cycle = 1 and cycle_month= '"+month+"' and aim_type=1 and branch_no = '"+branch_no+"'");
        if(CollectionUtils.isNotEmpty(aimEntities)){
            ////1.1.有，更新月目标
            AimEntity aimEntity = aimEntities.get(0);
            aimEntity.setAim_value(aim);
            aimEntity.setUser_id(userId);
            aimDao.updateEntity(aimEntity);
        }else{
            ////1.2.无，新建月目标
            AimEntity aimEntity = new AimEntity();
            aimEntity.setBranch_no(branch_no);
            aimEntity.setAim_cycle(1L);
            aimEntity.setAim_type(1L);
            aimEntity.setUser_id(userId);
            aimEntity.setCycle_year(year);
            aimEntity.setCycle_month(month);
            aimEntity.setAim_value(aim);
            aimEntity.setUser_id(userId);
            aimEntity.setOrg_id(orgId);
            aimDao.save(aimEntity);
        }
    }

    private void saveBranchYearAim(String orgId,String userId,String branch_no,long year,double aim) {
        List<AimEntity> aimEntities_year = aimDao.list("where org_id ='"+orgId+"' and aim_cycle = 0 and cycle_year= '"+year+"' and aim_type=1 and branch_no = '"+branch_no+"'");
        if(CollectionUtils.isNotEmpty(aimEntities_year)){
            ////2.1.有，更新年目标
            AimEntity aimEntity = aimEntities_year.get(0);
            if(CollectionUtils.isNotEmpty(aimEntities_year)){
                aimEntity.setAim_value(aimEntity.getAim_value()+(aim- aimEntities_year.get(0).getAim_value()));
            }else{
                aimEntity.setAim_value(aimEntity.getAim_value()+aim);
            }
            aimDao.updateEntity(aimEntity);
        }else{
            ////2.2.无，新建年目标
            AimEntity aimEntity = new AimEntity();
            aimEntity.setBranch_no(branch_no);
            aimEntity.setAim_cycle(0L);
            aimEntity.setAim_type(1L);
            aimEntity.setUser_id(userId);
            aimEntity.setOrg_id(orgId);
            aimEntity.setCycle_year((long)year);
            aimEntity.setAim_value(aim);
            aimDao.save(aimEntity);
        }
    }

    private void saveClsMonthAim(String orgId,String userId,String branch_no, String itemCls_no,long year,long month,double aim) {
        //1.检查是否存在已有的月份目标
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' and branch_no = '"+branch_no+"' and aim_cycle = 1 and cycle_month= '"+month+"' and aim_type=2 and item_clsno ='"+itemCls_no+"'");
        if(CollectionUtils.isNotEmpty(aimEntities)){
            ////1.1.有，更新月目标
            AimEntity aimEntity = aimEntities.get(0);
            aimEntity.setAim_value(aim);
            aimDao.updateEntity(aimEntity);
        }else{
            ////1.2.无，新建月目标
            AimEntity aimEntity = new AimEntity();
            aimEntity.setBranch_no(branch_no);
            aimEntity.setItem_clsno(itemCls_no);
            aimEntity.setAim_type(2L);
            aimEntity.setAim_cycle(1L);
            aimEntity.setUser_id(userId);
            aimEntity.setOrg_id(orgId);
            aimEntity.setCycle_year(year);
            aimEntity.setCycle_month(month);
            aimEntity.setAim_value(aim);
            aimDao.save(aimEntity);
        }
    }

    private void saveClsYearAim(String orgId,String userId,String branch_no, String itemCls_no,long year,double aim) {
        List<AimEntity> aimEntities_year = aimDao.list("where org_id ='"+orgId+"' and branch_no = '"+branch_no+"' and aim_cycle = 0 and cycle_year= '"+year+"' and aim_type=1 and item_clsno = '"+itemCls_no+"'");
        if(CollectionUtils.isNotEmpty(aimEntities_year)){
            ////2.1.有，更新年目标
            AimEntity aimEntity = aimEntities_year.get(0);
            if(CollectionUtils.isNotEmpty(aimEntities_year)){
                aimEntity.setAim_value(aimEntity.getAim_value()+(aim- aimEntities_year.get(0).getAim_value()));
            }else{
                aimEntity.setAim_value(aimEntity.getAim_value()+aim);
            }
            aimDao.updateEntity(aimEntity);
        }else{
            ////2.2.无，新建年目标
            AimEntity aimEntity = new AimEntity();
            aimEntity.setBranch_no(branch_no);
            aimEntity.setItem_clsno(itemCls_no);
            aimEntity.setAim_type(2L);
            aimEntity.setAim_cycle(0L);
            aimEntity.setUser_id(userId);
            aimEntity.setOrg_id(orgId);
            aimEntity.setCycle_year(year);
            aimEntity.setAim_value(aim);
            aimDao.save(aimEntity);
        }
    }
    
    private void saveBrandMonthAim(String orgId,String userId,String branch_no,String item_brandno,long year,long month,double aim) {
        //1.检查是否存在已有的月份目标
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' and branch_no = '"+branch_no+"' and aim_cycle = 1 and cycle_month= '"+month+"' and aim_type=3 and item_brandno ='"+item_brandno+"'");
        if(CollectionUtils.isNotEmpty(aimEntities)){
            ////1.1.有，更新月目标
            AimEntity aimEntity = aimEntities.get(0);
            aimEntity.setAim_value(aim);
            aimDao.updateEntity(aimEntity);
        }else{
            ////1.2.无，新建月目标
            AimEntity aimEntity = new AimEntity();
            aimEntity.setBranch_no(branch_no);
            aimEntity.setItem_brandno(item_brandno);
            aimEntity.setAim_type(3L);
            aimEntity.setAim_cycle(1L);
            aimEntity.setUser_id(userId);
            aimEntity.setOrg_id(orgId);
            aimEntity.setCycle_year(year);
            aimEntity.setCycle_month(month);
            aimEntity.setAim_value(aim);
            aimDao.save(aimEntity);
        }
    }
    
    private void saveBrandYearAim(String orgId,String userId,String branch_no,String item_brandno,long year,double aim) {
        List<AimEntity> aimEntities_year = aimDao.list("where org_id ='"+orgId+"' and branch_no = '"+branch_no+"' and aim_cycle = 0 and cycle_year= '"+year+"' and aim_type=3 and item_brandno = '"+item_brandno+"'");
        if(CollectionUtils.isNotEmpty(aimEntities_year)){
            ////2.1.有，更新年目标
            AimEntity aimEntity = aimEntities_year.get(0);
            if(CollectionUtils.isNotEmpty(aimEntities_year)){
                aimEntity.setAim_value(aimEntity.getAim_value()+(aim- aimEntities_year.get(0).getAim_value()));
            }else{
                aimEntity.setAim_value(aimEntity.getAim_value()+aim);
            }
            aimDao.updateEntity(aimEntity);
        }else{
            ////2.2.无，新建年目标
            AimEntity aimEntity = new AimEntity();
            aimEntity.setBranch_no(branch_no);
            aimEntity.setItem_brandno(item_brandno);
            aimEntity.setAim_type(3L);
            aimEntity.setAim_cycle(0L);
            aimEntity.setUser_id(userId);
            aimEntity.setOrg_id(orgId);
            aimEntity.setCycle_year(year);
            aimEntity.setAim_value(aim);
            aimDao.save(aimEntity);
        }
    }
    
    private void saveSalerMonthAim(String orgId,String userId,String branch_no,String sale_id,long year,long month,double aim) {
        //1.检查是否存在已有的月份目标
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' and branch_no = '"+branch_no+"' and aim_cycle = 1 and cycle_month= '"+month+"' and aim_type=4 and sale_id ='"+sale_id+"'");
        if(CollectionUtils.isNotEmpty(aimEntities)){
            ////1.1.有，更新月目标
            AimEntity aimEntity = aimEntities.get(0);
            aimEntity.setAim_value(aim);
            aimDao.updateEntity(aimEntity);
        }else{
            ////1.2.无，新建月目标
            AimEntity aimEntity = new AimEntity();
            aimEntity.setBranch_no(branch_no);
            aimEntity.setSale_id(sale_id);
            aimEntity.setAim_type(4L);
            aimEntity.setAim_cycle(1L);
            aimEntity.setUser_id(userId);
            aimEntity.setOrg_id(orgId);
            aimEntity.setCycle_year(year);
            aimEntity.setCycle_month(month);
            aimEntity.setAim_value(aim);
            aimDao.save(aimEntity);
        }
    }
    
    private void saveSalerYearAim(String orgId,String userId,String branch_no,String sale_id,long year,double aim) {
        List<AimEntity> aimEntities_year = aimDao.list("where org_id ='"+orgId+"' and branch_no = '"+branch_no+"' and aim_cycle = 0 and cycle_year= '"+year+"' and aim_type=4 and sale_id = '"+sale_id+"'");
        if(CollectionUtils.isNotEmpty(aimEntities_year)){
            ////2.1.有，更新年目标
            AimEntity aimEntity = aimEntities_year.get(0);
            if(CollectionUtils.isNotEmpty(aimEntities_year)){
                aimEntity.setAim_value(aimEntity.getAim_value()+(aim- aimEntities_year.get(0).getAim_value()));
            }else{
                aimEntity.setAim_value(aimEntity.getAim_value()+aim);
            }
            aimDao.updateEntity(aimEntity);
        }else{
            ////2.2.无，新建年目标
            AimEntity aimEntity = new AimEntity();
            aimEntity.setBranch_no(branch_no);
            aimEntity.setSale_id(sale_id);
            aimEntity.setAim_type(4L);
            aimEntity.setAim_cycle(0L);
            aimEntity.setUser_id(userId);
            aimEntity.setOrg_id(orgId);
            aimEntity.setCycle_year(year);
            aimEntity.setAim_value(aim);
            aimDao.save(aimEntity);
        }
    }
    
    @Override
    public JSONArray queryWholeAimDetail(String orgId, String year, String branchNos) throws BaseException {
        class ItemData{
            String branch_name;
            long month;
            double month_aim;
            double total_sale_lastyear;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public long getMonth() {
                return month;
            }

            public void setMonth(long month) {
                this.month = month;
            }

            public double getMonth_aim() {
                return month_aim;
            }

            public void setMonth_aim(double month_aim) {
                this.month_aim = month_aim;
            }

            public double getTotal_sale_lastyear() {
                return total_sale_lastyear;
            }

            public void setTotal_sale_lastyear(double total_sale_lastyear) {
                this.total_sale_lastyear = total_sale_lastyear;
            }
        }
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 1 " +
                "and aim_type = 1"+branchSql);
        Map<String ,ItemData> dataMap = new TreeMap<>();
        for(AimEntity aimEntity : aimEntities){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String branch_no = aimEntity.getBranch_no();
            Long aimMonth = aimEntity.getCycle_month();
            ItemData itemData = dataMap.get(branch_no+"@@"+(aimMonth>9?aimMonth:"0"+aimMonth));
            if(itemData==null){
                itemData = new ItemData();
                dataMap.put(branch_no+"@@"+(aimMonth>9?aimMonth:"0"+aimMonth),itemData);
            }
            itemData.setMonth(aimMonth);
            itemData.setMonth_aim(aim);
        }
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id ='"+orgId+"' " +
//                "and year(oper_date)="+(Integer.valueOf(year)-1)+branchSql);
        List<Object> objects_month = unionSqlDao.query("select concat(branch_no,\n" +
                "(\n" +
                "        CASE char_length(month(oper_date))\n" +
                "        WHEN 1 THEN\n" +
                "            CONCAT('0',month(oper_date))\n" +
                "        ELSE\n" +
                "           month(oper_date)\n" +
                "        END\n" +
                "    )\n" +
                ")\n" +
                ", sum(sale_amt)\n" +
                " from BranchDayEntity \n" +
                " where org_id ='"+orgId+"' \n" +
                "and year(oper_date)="+(Integer.valueOf(year)-1)+branchSql+"\n"+
                "GROUP BY branch_no ");
        for(Object object : objects_month){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = (String)itemEntityObj[0];
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale_lastyear(itemData.getTotal_sale_lastyear()+(total_sale!=null?total_sale:0));
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String[] key = entry.getKey().split("@@");
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店编号",key[0]);
            resultJson.put("门店名称",key[0]);
            resultJson.put("月份简称",Integer.valueOf(key[1]));
            resultJson.put("月份",Integer.valueOf(key[1]));
            resultJson.put("系数",0);
            resultJson.put("前一年销售",itemData.getTotal_sale_lastyear());
            resultJson.put("月度目标",itemData.getMonth_aim());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray queryClsAimGain(String orgId, String year, String month, String branchNos) throws BaseException {
        class ItemData{
            String branch_no;
            String itemcls_name;
            double year_aim;
            double month_aim;
            double year_sale;
            double month_sale;

            public String getBranch_no() {
                return branch_no;
            }

            public void setBranch_no(String branch_no) {
                this.branch_no = branch_no;
            }

            public String getItemcls_name() {
                return itemcls_name;
            }

            public double getYear_aim() {
                return year_aim;
            }

            public void setYear_aim(double year_aim) {
                this.year_aim = year_aim;
            }

            public void setItemcls_name(String itemcls_name) {
                this.itemcls_name = itemcls_name;
            }

            public double getMonth_aim() {
                return month_aim;
            }

            public void setMonth_aim(double month_aim) {
                this.month_aim = month_aim;
            }

            public double getYear_sale() {
                return year_sale;
            }

            public void setYear_sale(double year_sale) {
                this.year_sale = year_sale;
            }

            public double getMonth_sale() {
                return month_sale;
            }

            public void setMonth_sale(double month_sale) {
                this.month_sale = month_sale;
            }
        }
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 0 " +
                "and cycle_year= '"+year+"' " +
                "and aim_type = 2"+branchSql);
        Map<String ,ItemData> dataMap = new HashMap<>();
        for(AimEntity aimEntity : aimEntities){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String item_clsno = aimEntity.getItem_clsno();
            String branch_no = aimEntity.getBranch_no();
            ItemData itemData = dataMap.get(item_clsno);
            if(itemData!=null){
                itemData.setBranch_no(branch_no);
                itemData.setYear_aim(aim);
            }else{
                itemData = new ItemData();
                itemData.setBranch_no(branch_no);
                itemData.setYear_aim(aim);
                dataMap.put(item_clsno,itemData);
            }
        }
        List<AimEntity> aimEntities_month = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 1 " +
                "and cycle_year= '"+year+"' " +
                "and cycle_month= '"+month+"' " +
                "and aim_type = 2"+branchSql);
        for(AimEntity aimEntity : aimEntities_month){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String item_clsno = aimEntity.getItem_clsno();
            String branch_no = aimEntity.getBranch_no();
            ItemData itemData = dataMap.get(item_clsno);
            if(itemData!=null){
                itemData.setMonth_aim(aim);
            }else{
                itemData = new ItemData();
                itemData.setMonth_aim(aim);
                dataMap.put(item_clsno,itemData);
            }
        }
        //分类名
        String itemcls_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(itemcls_nosStr)){
                itemcls_nosStr = itemcls_nosStr + ",'"+str+"'";
            }else{
                itemcls_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(itemcls_nosStr)){
            List<Object> objects_year = unionSqlDao.query("select item_clsno as item_clsno, sum(sale_amt)as sale_amt from ClsDayEntity where org_id ='"+orgId+"' and year(oper_date)='"+year+"'"+
                    branchSql+" and item_clsno in("+itemcls_nosStr+") group by item_clsno");
            for(Object object : objects_year){
                Object[]itemEntityObj = (Object[])object;
                String item_clsno = (String)itemEntityObj[0];
                Double total_sale = (Double)itemEntityObj[1];
                ItemData itemData = dataMap.get(item_clsno);
                if(itemData!=null){
                    itemData.setYear_sale(itemData.getYear_sale()+(total_sale!=null?total_sale:0));
                }
            }
            List<Object> objects_month = unionSqlDao.query("select item_clsno as item_clsno, sum(sale_amt)as sale_amt from ClsDayEntity where org_id ='"+orgId+"' and month(oper_date) = month(NOW())"+
                    branchSql+" and item_clsno in("+itemcls_nosStr+") group by item_clsno");
            for(Object object : objects_month){
                Object[]itemEntityObj = (Object[])object;
                String item_clsno = (String)itemEntityObj[0];
                Double total_sale = (Double)itemEntityObj[1];
                ItemData itemData = dataMap.get(item_clsno);
                if(itemData!=null){
                    itemData.setMonth_sale(itemData.getMonth_sale()+(total_sale!=null?total_sale:0));
                }
            }

            List<ItemClsEntity> itemClsEntities = itemClsDao.list("where item_clsno in("+itemcls_nosStr+") and org_id ='"+orgId+"'");
            for(ItemClsEntity itemClsEntity : itemClsEntities){
                dataMap.get(itemClsEntity.getItem_clsno()).setItemcls_name(itemClsEntity.getItem_clsname());
            }
        }
        Calendar ca = Calendar.getInstance();//创建一个日期实例
        ca.setTime(new Date());//实例化一个日期
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("门店",itemData.getBranch_no());
            resultJson.put("类别",key);
            resultJson.put("年度目标",NumberUtil.getDouble2Format(itemData.getYear_aim()));
            resultJson.put("今年完成",itemData.getYear_sale());
            resultJson.put("年进度",NumberUtil.getDouble2Format(itemData.getYear_sale()/itemData.getYear_aim()));
            resultJson.put("年任务预测",itemData.getYear_sale()/ca.get(Calendar.DAY_OF_YEAR)*365);
            resultJson.put("年完成率预测",NumberUtil.getDouble2Format(itemData.getYear_sale()/ca.get(Calendar.DAY_OF_YEAR)*365/itemData.getYear_aim()));
            resultJson.put("本月任务",itemData.getMonth_aim());
            resultJson.put("本月完成",itemData.getMonth_sale());
            resultJson.put("月进度",NumberUtil.getDouble2Format(itemData.getMonth_sale()/(itemData.getMonth_aim())));
            resultJson.put("剩余任务",itemData.getMonth_aim()-itemData.getMonth_sale());
            resultJson.put("剩余天数",ca.getActualMaximum(java.util.Calendar.DAY_OF_MONTH)-ca.get(Calendar.DAY_OF_MONTH));
            resultJson.put("本月任务预测",NumberUtil.getDouble2Format(itemData.getMonth_sale()/ca.get(Calendar.DAY_OF_MONTH)*java.util.Calendar.DAY_OF_MONTH));
            resultJson.put("预测完成率",NumberUtil.getDouble2Format(itemData.getMonth_sale()/ca.get(Calendar.DAY_OF_MONTH)*java.util.Calendar.DAY_OF_MONTH/(itemData.getMonth_aim())));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray queryClsAimDetail(String orgId, String year, String branchNos) throws BaseException {
        class ItemData{
            String branch_name;
            String itemcls_name;
            double year_aim;
            double tatal_sale_lastyear;
            double cls_sale_lastyear;
            double month_aim1;
            double month_aim2;
            double month_aim3;
            double month_aim4;
            double month_aim5;
            double month_aim6;
            double month_aim7;
            double month_aim8;
            double month_aim9;
            double month_aim10;
            double month_aim11;
            double month_aim12;
            double year_sale;

            public double getYear_sale() {
                return year_sale;
            }

            public void setYear_sale(double year_sale) {
                this.year_sale = year_sale;
            }

            public double getTatal_sale_lastyear() {
                return tatal_sale_lastyear;
            }

            public void setTatal_sale_lastyear(double tatal_sale_lastyear) {
                this.tatal_sale_lastyear = tatal_sale_lastyear;
            }

            public double getCls_sale_lastyear() {
                return cls_sale_lastyear;
            }

            public void setCls_sale_lastyear(double cls_sale_lastyear) {
                this.cls_sale_lastyear = cls_sale_lastyear;
            }

            public double getMonth_aim1() {
                return month_aim1;
            }

            public void setMonth_aim1(double month_aim1) {
                this.month_aim1 = month_aim1;
            }

            public double getMonth_aim2() {
                return month_aim2;
            }

            public void setMonth_aim2(double month_aim2) {
                this.month_aim2 = month_aim2;
            }

            public double getMonth_aim3() {
                return month_aim3;
            }

            public void setMonth_aim3(double month_aim3) {
                this.month_aim3 = month_aim3;
            }

            public double getMonth_aim4() {
                return month_aim4;
            }

            public void setMonth_aim4(double month_aim4) {
                this.month_aim4 = month_aim4;
            }

            public double getMonth_aim5() {
                return month_aim5;
            }

            public void setMonth_aim5(double month_aim5) {
                this.month_aim5 = month_aim5;
            }

            public double getMonth_aim6() {
                return month_aim6;
            }

            public void setMonth_aim6(double month_aim6) {
                this.month_aim6 = month_aim6;
            }

            public double getMonth_aim7() {
                return month_aim7;
            }

            public void setMonth_aim7(double month_aim7) {
                this.month_aim7 = month_aim7;
            }

            public double getMonth_aim8() {
                return month_aim8;
            }

            public void setMonth_aim8(double month_aim8) {
                this.month_aim8 = month_aim8;
            }

            public double getMonth_aim9() {
                return month_aim9;
            }

            public void setMonth_aim9(double month_aim9) {
                this.month_aim9 = month_aim9;
            }

            public double getMonth_aim10() {
                return month_aim10;
            }

            public void setMonth_aim10(double month_aim10) {
                this.month_aim10 = month_aim10;
            }

            public double getMonth_aim11() {
                return month_aim11;
            }

            public void setMonth_aim11(double month_aim11) {
                this.month_aim11 = month_aim11;
            }

            public double getMonth_aim12() {
                return month_aim12;
            }

            public void setMonth_aim12(double month_aim12) {
                this.month_aim12 = month_aim12;
            }

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public String getItemcls_name() {
                return itemcls_name;
            }

            public void setItemcls_name(String itemcls_name) {
                this.itemcls_name = itemcls_name;
            }

            public double getYear_aim() {
                return year_aim;
            }

            public void setYear_aim(double year_aim) {
                this.year_aim = year_aim;
            }
        }
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 1 " +
                "and aim_type = 2"+branchSql);
        Map<String ,ItemData> dataMap = new HashMap<>();
        for(AimEntity aimEntity : aimEntities){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String branch_no = aimEntity.getBranch_no();
            String item_clsno = aimEntity.getItem_clsno();
            Long aimMonth = aimEntity.getCycle_month();
            ItemData itemData = dataMap.get(branch_no+"@@"+item_clsno);
            if(itemData==null){
                itemData = new ItemData();
                dataMap.put(branch_no+"@@"+item_clsno,itemData);
            }
            switch (aimMonth.intValue()){
                case 1: itemData.setMonth_aim1(aim);break;
                case 2: itemData.setMonth_aim2(aim);break;
                case 3: itemData.setMonth_aim3(aim);break;
                case 4: itemData.setMonth_aim4(aim);break;
                case 5: itemData.setMonth_aim5(aim);break;
                case 6: itemData.setMonth_aim6(aim);break;
                case 7: itemData.setMonth_aim7(aim);break;
                case 8: itemData.setMonth_aim8(aim);break;
                case 9: itemData.setMonth_aim9(aim);break;
                case 10: itemData.setMonth_aim10(aim);break;
                case 11: itemData.setMonth_aim11(aim);break;
                case 12: itemData.setMonth_aim12(aim);break;
            }
        }
        List<AimEntity> aimEntities_year = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 0 " +
                "and aim_type = 2 and cycle_year='"+year+"'"+branchSql);
        if(CollectionUtils.isNotEmpty(aimEntities_year)){
            for(AimEntity entity : aimEntities_year) {
                String branch_no = entity.getBranch_no();
                String item_clsno = entity.getItem_clsno();
                ItemData itemData = dataMap.get(branch_no+"@@"+item_clsno);
                if(itemData!=null){
                    itemData.setYear_aim(entity.getAim_value());
                }
            }
        }
        double total_lastyear = 0;
        List<Object> objectList_lastyear = unionSqlDao.query("select\n" +
                "       branch_no,\n" +
                "        item_clsno,\n" +
                "     \n" +
                "        sum(sale_amt)\n" +
                "    from\n" +
                "        ClsDayEntity\n" +
                "    where\n" +
                "        org_id='"+orgId+"' \n" +
                "        and year(oper_date)='"+(Integer.valueOf(year)-1)+"'\n " +branchSql+
                "  GROUP BY branch_no,item_clsno");
        for(Object object : objectList_lastyear){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = (String)itemEntityObj[0];
            String item_clsno = (String)itemEntityObj[1];
            Double total_sale = (Double)itemEntityObj[2];
            total_lastyear += total_sale;
            ItemData itemData = dataMap.get(branch_no+"@@"+item_clsno);
            if(itemData!=null){
                itemData.setCls_sale_lastyear(itemData.getCls_sale_lastyear()+total_sale);
            }
        }

        List<Object> objectList = unionSqlDao.query("select\n" +
                "       branch_no,\n" +
                "        item_clsno,\n" +
                "     \n" +
                "        sum(sale_amt)\n" +
                "    from\n" +
                "        ClsDayEntity\n" +
                "    where\n" +
                "        org_id='"+orgId+"' \n" +
                "        and year(oper_date)=year(now())\n" +branchSql+
                "  GROUP BY branch_no,item_clsno");
        for(Object object : objectList){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = (String)itemEntityObj[0];
            String item_clsno = (String)itemEntityObj[1];
            Double total_sale = (Double)itemEntityObj[2];
            ItemData itemData = dataMap.get(branch_no+"@@"+item_clsno);
            if(itemData!=null){
                itemData.setYear_sale(itemData.getYear_sale()+total_sale);
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String[] key = entry.getKey().split("@@");
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店编号",key[0]);
            resultJson.put("门店名称",key[0]);
            resultJson.put("类别编号",key[1]);
            resultJson.put("类别",key[1]);
            resultJson.put("前一年销售",itemData.getCls_sale_lastyear());
            resultJson.put("平均占比",NumberUtil.getDouble2Format(itemData.getCls_sale_lastyear()/total_lastyear));
            resultJson.put("销售任务",itemData.getYear_aim());
            resultJson.put("业绩提升",NumberUtil.getDouble2Format((itemData.getYear_sale()-itemData.getCls_sale_lastyear())/itemData.getCls_sale_lastyear()));
            resultJson.put("月度目标_01",itemData.getMonth_aim1());
            resultJson.put("月度目标_02",itemData.getMonth_aim2());
            resultJson.put("月度目标_03",itemData.getMonth_aim3());
            resultJson.put("月度目标_04",itemData.getMonth_aim4());
            resultJson.put("月度目标_05",itemData.getMonth_aim5());
            resultJson.put("月度目标_06",itemData.getMonth_aim6());
            resultJson.put("月度目标_07",itemData.getMonth_aim7());
            resultJson.put("月度目标_08",itemData.getMonth_aim8());
            resultJson.put("月度目标_09",itemData.getMonth_aim9());
            resultJson.put("月度目标_10",itemData.getMonth_aim10());
            resultJson.put("月度目标_11",itemData.getMonth_aim11());
            resultJson.put("月度目标_12",itemData.getMonth_aim12());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray queryBrandAimGain(String orgId, String year, String month, String branchNos) throws BaseException {
        class ItemData{
            String branch_no;
            String brand_name;
            double month_aim;
            double year_aim;
            double year_sale;
            double month_sale;

            public double getMonth_aim() {
                return month_aim;
            }

            public void setMonth_aim(double month_aim) {
                this.month_aim = month_aim;
            }

            public String getBranch_no() {
                return branch_no;
            }

            public void setBranch_no(String branch_no) {
                this.branch_no = branch_no;
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            public double getYear_aim() {
                return year_aim;
            }

            public void setYear_aim(double year_aim) {
                this.year_aim = year_aim;
            }

            public double getYear_sale() {
                return year_sale;
            }

            public void setYear_sale(double year_sale) {
                this.year_sale = year_sale;
            }

            public double getMonth_sale() {
                return month_sale;
            }

            public void setMonth_sale(double month_sale) {
                this.month_sale = month_sale;
            }
        }
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 0 " +
                "and cycle_year= '"+year+"' " +
                "and aim_type = 3"+branchSql);
        Map<String ,ItemData> dataMap = new HashMap<>();
        for(AimEntity aimEntity : aimEntities){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String item_brandno = aimEntity.getItem_brandno();
            ItemData itemData = dataMap.get(item_brandno);
            if(itemData!=null){
                itemData.setYear_aim(aim);
                itemData.setBranch_no(aimEntity.getBranch_no());
            }else{
                itemData = new ItemData();
                itemData.setYear_aim(aim);
                itemData.setBranch_no(aimEntity.getBranch_no());
                dataMap.put(item_brandno,itemData);
            }

        }

        List<AimEntity> aimEntities_month = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 1 " +
                "and cycle_year= '"+year+"' " +
                "and cycle_month= '"+month+"' " +
                "and aim_type = 3"+branchSql);
        for(AimEntity aimEntity : aimEntities_month){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String item_brandno = aimEntity.getItem_brandno();
            ItemData itemData = dataMap.get(item_brandno);
            if(itemData!=null){
                itemData.setMonth_aim(aim);
            }else{
                itemData = new ItemData();
                itemData.setMonth_aim(aim);
                dataMap.put(item_brandno,itemData);
            }
        }
        String item_brandnosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_brandnosStr)){
                item_brandnosStr = item_brandnosStr + ",'"+str+"'";
            }else{
                item_brandnosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_brandnosStr)){
//            List<BrandDayEntity> brandDayEntities_year = brandDayDao.list("where org_id ='"+orgId+"' and year(oper_date)='"+year+"' and"+
//                    " item_brandno in("+item_brandnosStr+")"+branchSql);
            List<Object> objects_year = unionSqlDao.query("select item_brandno as item_brandno, sum(sale_amt)as sale_amt from BrandDayEntity where org_id ='"+orgId+"' and year(oper_date)='"+year+"'"+
                    branchSql+" and item_brandno in("+item_brandnosStr+") group by item_brandno");
            for(Object object : objects_year){
                Object[]itemEntityObj = (Object[])object;
                String item_brandno = (String)itemEntityObj[0];
                Double total_sale = (Double)itemEntityObj[1];
                ItemData itemData = dataMap.get(item_brandno);
                if(itemData!=null){
                    itemData.setYear_sale(itemData.getYear_sale()+(total_sale!=null?total_sale:0));
                }
            }
//            List<BrandDayEntity> brandDayEntities_month = brandDayDao.list("where org_id ='"+orgId+"' and month(oper_date) = month(NOW())"+
//                    " and item_brandno in("+item_brandnosStr+")"+branchSql);
            List<Object> objects_month = unionSqlDao.query("select item_brandno as item_brandno, sum(sale_amt)as sale_amt from BrandDayEntity where org_id ='"+orgId+"' and month(oper_date) = month(NOW())"+
                    branchSql+" and item_brandno in("+item_brandnosStr+") group by item_brandno");
            for(Object object : objects_month){
                Object[]itemEntityObj = (Object[])object;
                String item_brandno = (String)itemEntityObj[0];
                Double total_sale = (Double)itemEntityObj[1];
                ItemData itemData = dataMap.get(item_brandno);
                if(itemData!=null){
                    itemData.setMonth_sale(itemData.getMonth_sale()+(total_sale!=null?total_sale:0));
                }
            }
        }
        Calendar ca = Calendar.getInstance();//创建一个日期实例
        ca.setTime(new Date());//实例化一个日期
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("门店",itemData.getBranch_no());
            resultJson.put("品牌",key);
            resultJson.put("年度目标",NumberUtil.getDouble2Format(itemData.getYear_aim()));
            resultJson.put("今年完成",itemData.getYear_sale());
            resultJson.put("年进度",NumberUtil.getDouble2Format(itemData.getYear_sale()/itemData.getYear_aim()));
            resultJson.put("年任务预测",itemData.getYear_sale()/ca.get(Calendar.DAY_OF_YEAR)*365);
            resultJson.put("年完成率预测",NumberUtil.getDouble2Format(itemData.getYear_sale()/ca.get(Calendar.DAY_OF_YEAR)*365/itemData.getYear_aim()));
            resultJson.put("本月任务",NumberUtil.getDouble2Format(itemData.getMonth_aim()));
            resultJson.put("本月完成",itemData.getMonth_sale());
            resultJson.put("月进度",NumberUtil.getDouble2Format(itemData.getMonth_sale()/(itemData.getMonth_aim())));
            resultJson.put("剩余任务",NumberUtil.getDouble2Format(itemData.getMonth_aim()-itemData.getMonth_sale()));
            resultJson.put("剩余天数",ca.getActualMaximum(java.util.Calendar.DAY_OF_MONTH)-ca.get(Calendar.DAY_OF_MONTH));
            resultJson.put("本月任务预测",NumberUtil.getDouble2Format(itemData.getMonth_sale()/ca.get(Calendar.DAY_OF_MONTH)*java.util.Calendar.DAY_OF_MONTH));
            resultJson.put("预测完成率",NumberUtil.getDouble2Format(itemData.getMonth_sale()/ca.get(Calendar.DAY_OF_MONTH)*java.util.Calendar.DAY_OF_MONTH/(itemData.getMonth_aim())));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray queryBrandAimDetail(String orgId, String year, String branchNos) throws BaseException {
        class ItemData{
            String branch_name;
            long month;
            double month_aim;
            double total_sale_lastyear;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public long getMonth() {
                return month;
            }

            public void setMonth(long month) {
                this.month = month;
            }

            public double getMonth_aim() {
                return month_aim;
            }

            public void setMonth_aim(double month_aim) {
                this.month_aim = month_aim;
            }

            public double getTotal_sale_lastyear() {
                return total_sale_lastyear;
            }

            public void setTotal_sale_lastyear(double total_sale_lastyear) {
                this.total_sale_lastyear = total_sale_lastyear;
            }
        }
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 1 " +
                "and cycle_year = '"+year+"' " +
                "and aim_type = 3"+branchSql);
        Map<String ,ItemData> dataMap = new HashMap<>();
        for(AimEntity aimEntity : aimEntities){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String branch_no = aimEntity.getBranch_no();
            String brand_no = aimEntity.getItem_brandno();
            Long aimMonth = aimEntity.getCycle_month();
            ItemData itemData = dataMap.get(branch_no+"@@"+brand_no+"@@"+aimMonth);
            if(itemData==null){
                itemData = new ItemData();
                dataMap.put(branch_no+"@@"+brand_no+"@@"+aimMonth,itemData);
            }
            itemData.setMonth(aimMonth);
            itemData.setMonth_aim(aim);
        }
        String item_brandnosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_brandnosStr)){
                item_brandnosStr = item_brandnosStr + ",'"+str.split("@@")[1]+"'";
            }else{
                item_brandnosStr =  "'"+str.split("@@")[1]+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_brandnosStr)){
//            List<BrandDayEntity>brandDayEntities= brandDayDao.list("where org_id ='"+orgId+"' " +
//                    "and year(oper_date)="+(Integer.valueOf(year)-1)+" and item_brandno in("+item_brandnosStr+")"+branchSql);
            List<Object> objects_year = unionSqlDao.query("select concat(branch_no,'@@',item_brandno,'@@',month(oper_date)), sum(sale_amt)as sale_amt from BrandDayEntity where org_id ='"+orgId+"' and year(oper_date)='"+(Integer.valueOf(year)-1)+"'"+
                    branchSql+" and item_brandno in("+item_brandnosStr+") group by item_brandno,branch_no");
            for(Object object : objects_year){
                Object[]itemEntityObj = (Object[])object;
                String item_brandno = (String)itemEntityObj[0];
                Double total_sale = (Double)itemEntityObj[1];
                ItemData itemData = dataMap.get(item_brandno);
                if(itemData!=null){
                    itemData.setTotal_sale_lastyear(itemData.getTotal_sale_lastyear()+(total_sale!=null?total_sale:0));
                }
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String[] key = entry.getKey().split("@@");
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店编号",key[0]);
            resultJson.put("门店名称",key[0]);
            resultJson.put("品牌",key[1]);
            resultJson.put("月份",key[2]);
            resultJson.put("前一年销售",itemData.getTotal_sale_lastyear());
            resultJson.put("月度目标",itemData.getMonth_aim());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray querySalerAimGain(String orgId, String year, String month, String branchNos) throws BaseException {
        class ItemData{
            String branch_no;
            String sale_name;
            long year;
            long month;
            double month_aim;
            double year_aim;
            double year_sale;
            double month_sale;

            public double getMonth_aim() {
                return month_aim;
            }

            public void setMonth_aim(double month_aim) {
                this.month_aim = month_aim;
            }

            public String getBranch_no() {
                return branch_no;
            }

            public void setBranch_no(String branch_no) {
                this.branch_no = branch_no;
            }

            public String getSale_name() {
                return sale_name;
            }

            public void setSale_name(String sale_name) {
                this.sale_name = sale_name;
            }

            public long getYear() {
                return year;
            }

            public void setYear(long year) {
                this.year = year;
            }

            public long getMonth() {
                return month;
            }

            public void setMonth(long month) {
                this.month = month;
            }

            public double getYear_aim() {
                return year_aim;
            }

            public void setYear_aim(double year_aim) {
                this.year_aim = year_aim;
            }

            public double getYear_sale() {
                return year_sale;
            }

            public void setYear_sale(double year_sale) {
                this.year_sale = year_sale;
            }

            public double getMonth_sale() {
                return month_sale;
            }

            public void setMonth_sale(double month_sale) {
                this.month_sale = month_sale;
            }
        }
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 0 " +
                "and cycle_year= '"+year+"' " +
                "and aim_type = 4"+branchSql);
        Map<String ,ItemData> dataMap = new HashMap<>();
        for(AimEntity aimEntity : aimEntities){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String sale_id = aimEntity.getSale_id();
            String branch_no = aimEntity.getBranch_no();
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setYear_aim(aim);
                itemData.setBranch_no(branch_no);
                itemData.setYear(Long.valueOf(year));
            }else{
                itemData = new ItemData();
                itemData.setYear_aim(aim);
                itemData.setBranch_no(branch_no);
                itemData.setYear(Long.valueOf(year));
                dataMap.put(sale_id,itemData);
            }
        }
        List<AimEntity> aimEntities_month = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 1 " +
                "and cycle_year= '"+year+"' " +
                "and cycle_month= '"+month+"' " +
                "and aim_type = 4"+branchSql);
        for(AimEntity aimEntity : aimEntities_month){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String sale_id = aimEntity.getSale_id();
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setMonth_aim(aim);
                itemData.setMonth(Long.valueOf(month));
            }
        }
        String sale_idsStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(sale_idsStr)){
                sale_idsStr = sale_idsStr + ",'"+str+"'";
            }else{
                sale_idsStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(sale_idsStr)){
//            List<SalerDayEntity> salerDayEntities_year = salerDayDao.list("where org_id ='"+orgId+"' and year(oper_date)='"+year+"' and"+
//                    " sale_id in("+sale_idsStr+")"+branchSql);
            List<Object> objects_year = unionSqlDao.query("select sale_id, sum(sale_amt)as sale_amt from SalerDayEntity where org_id ='"+orgId+"' and year(oper_date)='"+year+"'"+
                    branchSql+" and sale_id in("+sale_idsStr+") group by sale_id");
            for(Object object : objects_year){
                Object[]itemEntityObj = (Object[])object;
                String sale_id = (String)itemEntityObj[0];
                Double total_sale = (Double)itemEntityObj[1];
                ItemData itemData = dataMap.get(sale_id);
                if(itemData!=null){
                    itemData.setYear_sale(itemData.getYear_sale()+(total_sale!=null?total_sale:0));
                }
            }
//            List<SalerDayEntity> salerDayEntities_month = salerDayDao.list("where org_id ='"+orgId+"' and month(oper_date) = month(NOW()) and sale_id in("+sale_idsStr+")"+branchSql);
            List<Object> objects_month = unionSqlDao.query("select sale_id, sum(sale_amt)as sale_amt from SalerDayEntity where org_id ='"+orgId+"' and month(oper_date) = month(NOW())"+
                    branchSql+" and sale_id in("+sale_idsStr+") group by sale_id");
            for(Object object : objects_month){
                Object[]itemEntityObj = (Object[])object;
                String sale_id = (String)itemEntityObj[0];
                Double total_sale = (Double)itemEntityObj[1];
                ItemData itemData = dataMap.get(sale_id);
                if(itemData!=null){
                    itemData.setMonth_sale(itemData.getMonth_sale()+(total_sale!=null?total_sale:0));
                }
            }
            if(StringUtils.isNotEmpty(sale_idsStr)){
                List<SalerEntity> salerEntities = salerDao.list("where sale_id in("+sale_idsStr+") and org_id ='"+orgId+"'");
                for(SalerEntity salerEntity : salerEntities){
                    dataMap.get(salerEntity.getSale_id()).setSale_name(salerEntity.getSale_name());
                }
            }
        }
        Calendar ca = Calendar.getInstance();//创建一个日期实例
        ca.setTime(new Date());//实例化一个日期
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("员工编号",key);
            resultJson.put("姓名",itemData.getSale_name());
            resultJson.put("门店",itemData.getBranch_no());
            resultJson.put("key_no",itemData.getYear()+"-"+itemData.getMonth()+"-01");
            resultJson.put("flag","GRZTYD");
            resultJson.put("任务",itemData.getYear_aim());
            resultJson.put("完成",itemData.getMonth_sale());
            resultJson.put("进度",NumberUtil.getDouble2Format(itemData.getMonth_sale()/(itemData.getYear_aim()/12)));
            resultJson.put("预测完成率",NumberUtil.getDouble2Format(itemData.getYear_sale()/ca.get(Calendar.DAY_OF_YEAR)*365/itemData.getYear_aim()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray querySalerAimDetail(String orgId, String year, String branchNos) throws BaseException {
        class ItemData{
            String saler_name;
            long month;
            double month_aim;
            double total_sale_lastyear;

            public String getSaler_name() {
                return saler_name;
            }

            public void setSaler_name(String saler_name) {
                this.saler_name = saler_name;
            }

            public long getMonth() {
                return month;
            }

            public void setMonth(long month) {
                this.month = month;
            }

            public double getMonth_aim() {
                return month_aim;
            }

            public void setMonth_aim(double month_aim) {
                this.month_aim = month_aim;
            }

            public double getTotal_sale_lastyear() {
                return total_sale_lastyear;
            }

            public void setTotal_sale_lastyear(double total_sale_lastyear) {
                this.total_sale_lastyear = total_sale_lastyear;
            }
        }
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";

        List<AimEntity> aimEntities = aimDao.list("where org_id ='"+orgId+"' " +
                "and aim_cycle = 1 " +
                "and aim_type = 4"+branchSql);
        Map<String ,ItemData> dataMap = new HashMap<>();
        for(AimEntity aimEntity : aimEntities){
            double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
            String branch_no = aimEntity.getBranch_no();
            String sale_id = aimEntity.getSale_id();
            Long aimMonth = aimEntity.getCycle_month();
            ItemData itemData = dataMap.get(branch_no+"@@"+sale_id+"@@"+aimMonth);
            if(itemData==null){
                itemData = new ItemData();
                dataMap.put(branch_no+"@@"+sale_id+"@@"+aimMonth,itemData);
            }
            itemData.setMonth(aimMonth);
            itemData.setMonth_aim(aim);
        }
        String sale_idsStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(sale_idsStr)){
                sale_idsStr = sale_idsStr + ",'"+str.split("@@")[1]+"'";
            }else{
                sale_idsStr =  "'"+str.split("@@")[1]+"'";
            }
        }
        if(StringUtils.isNotEmpty(sale_idsStr)){
//            List<SalerDayEntity>salerDayEntities= salerDayDao.list("where org_id ='"+orgId+"' " +
//                    "and year(oper_date)="+(Integer.valueOf(year)-1)+" and sale_id in("+sale_idsStr+")"+branchSql);
            List<Object> objects_year = unionSqlDao.query("select concat(branch_no,'@@',sale_id,'@@',month(oper_date)), sum(sale_amt)as sale_amt from SalerDayEntity where org_id ='"+orgId+"' and year(oper_date)='"+(Integer.valueOf(year)-1)+"'"+
                    branchSql+" and sale_id in("+sale_idsStr+") group by sale_id,branch_no");
            for(Object object : objects_year){
                Object[]itemEntityObj = (Object[])object;
                String sale_id = (String)itemEntityObj[0];
                Double total_sale = (Double)itemEntityObj[1];
                ItemData itemData = dataMap.get(sale_id);
                if(itemData!=null){
                    itemData.setTotal_sale_lastyear(itemData.getTotal_sale_lastyear()+(total_sale!=null?total_sale:0));
                }
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String[] key = entry.getKey().split("@@");
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店编号",key[0]);
            resultJson.put("门店名称",key[0]);
            resultJson.put("营业员编号",key[1]);
            resultJson.put("姓名",key[1]);
            resultJson.put("职务",0);
            resultJson.put("任务系数",1);
            resultJson.put("门店系数",1);
            resultJson.put("任务",itemData.getMonth_aim());
            resultJson.put("类型",itemData.getMonth());
            results.add(resultJson);
        }
        return results;
    }
}
