package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/15 17:45
 * @Version: 1.0
 **/
public interface JiegoubaogaoService {
    JSONArray branches(String orgId) throws BaseException;
    JSONArray clses(String orgId) throws BaseException;
    JSONArray brands(String orgId) throws BaseException;
    JSONArray items(String orgId) throws BaseException;

    JSONArray query(String orgId, String type,String begin,String end, String branchNos, String itemClses, String brands,JSONArray priceZones) throws BaseException;
    JSONArray itemSaleByClsno(String orgId,String branchNo, String clsNo,JSONArray priceZones,String begin,String end,boolean onlyHasStock) throws BaseException;
}
