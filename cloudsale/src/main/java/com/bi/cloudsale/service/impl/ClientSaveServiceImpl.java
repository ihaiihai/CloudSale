package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSON;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.dto.ClientSaveDto;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.commision.SalaryBranchDao;
import com.bi.cloudsale.persistent.dao.commision.SalaryDetailDao;
import com.bi.cloudsale.persistent.dao.commision.SalarySalerDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.commision.SalaryBranchEntity;
import com.bi.cloudsale.persistent.entity.commision.SalaryDetailEntity;
import com.bi.cloudsale.persistent.entity.commision.SalarySalerEntity;
import com.bi.cloudsale.service.ClientService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class ClientSaveServiceImpl implements ClientService {

	private static final Logger log = LoggerFactory.getLogger(ClientSaveServiceImpl.class);

	@Resource
	private ClientSaveDao clientSaveDao;

	@Resource
	private BranchDayDao branchDayDao;
	@Resource
	private BrandDayDao brandDayDao;
	@Resource
	private ClsDayDao clsDayDao;
	@Resource
	private ItemDayDao itemDayDao;
	@Resource
	private ItemSalerDayDao itemSalerDayDao;
	@Resource
	private SalerDayDao salerDayDao;
	@Resource
	private VipDayDao vipDayDao;

	@Resource
	private AimDao aimDao;

	@Resource
	private BranchDao branchDao;

	@Resource
	private SalerDao salerDao;

	@Resource
	private ItemDao itemDao;
	@Resource
	private BrandDao brandDao;

	@Resource
	private StockDao stockDao;

	@Resource
	private BranchTimeDao branchTimeDao;

	@Resource
	private ItemClsDao itemClsDao;

	@Resource
	private PriceZoneDao priceZoneDao;

	@Resource
	private VipDao vipDao;

	@Resource
	private BranchClsDao branchClsDao;
	
	@Resource
	private SalaryDetailDao salaryDetailDao;
	
	@Resource
	private SalaryBranchDao salaryBranchDao;
	
	@Resource
	private SalarySalerDao salarySalerDao;

	@Override
	public Long saveOrUpdate(ClientSaveDto clientSaveDto) throws BaseException {
		String tableName = clientSaveDto.getTableName();
		String data = clientSaveDto.getData().toJSONString();
		Class tableClass = clientSaveDao.getTableClassByTableName(tableName);
		if(tableClass!=null){

			//日报相关表
			if(tableClass == BranchDayEntity.class){
				saveOrUpdateBranchDayEntity(data);
			}else if(tableClass == BrandDayEntity.class){
				saveOrUpdateBrandDayEntity(data);
			}else if(tableClass == ClsDayEntity.class){
				saveOrUpdateClsDayEntity(data);
			}else if(tableClass == ItemDayEntity.class){
				saveOrUpdateItemDayEntity(data);
			}else if(tableClass == ItemSalerDayEntity.class){
				saveOrUpdateItemSalerDayEntity(data);
			}else if(tableClass == SalerDayEntity.class){
				saveOrUpdateSalerDayEntity(data);
			}else if(tableClass == VipDayEntity.class){
				saveOrUpdateVipDayEntity(data);
			}
			//门店时段表
			else if(tableClass == BranchTimeEntity.class){
				saveOrUpdateBranchTimeEntity(data);
			}

			//目标表
			else if(tableClass == AimEntity.class){
				saveOrUpdateAimEntity(data);
			}

			//门店表
			else if(tableClass == BranchEntity.class){
				saveOrUpdateBranchEntity(data);
			}

			//店员表
			else if(tableClass == SalerEntity.class){
				saveOrUpdateSalerEntity(data);
			}

			//商品表
			else if(tableClass == ItemEntity.class){
				saveOrUpdateItemEntity(data);
			}

			//品牌表
			else if(tableClass == BrandEntity.class){
				saveOrUpdateBrandEntity(data);
			}

			//库存表
			else if(tableClass == StockEntity.class){
				saveOrUpdateStockEntity(data);
			}

			//商品分类表
			else if(tableClass == ItemClsEntity.class){
				saveOrUpdateItemClsEntity(data);
			}

			//商品价格区间表
			else if(tableClass == PriceZoneEntity.class){
				saveOrUpdatePriceZoneEntity(data);
			}

			//vip会员表
			else if(tableClass == VipEntity.class){
				saveOrUpdateVipEntity(data);
			}

			//门店分类表
			else if(tableClass == BranchClsEntity.class){
				saveOrUpdateBranchClsEntity(data);
			}
			
			//提成明细
			else if(tableClass == SalaryDetailEntity.class) {
				saveOrUpdateSalaryDetailEntity(data);
			}
			
			//门店提成
			else if(tableClass == SalaryBranchEntity.class) {
				saveOrUpdateSalaryBranchEntity(data);
			}
			
			//员工提成
			else if(tableClass == SalarySalerEntity.class) {
				saveOrUpdateSalarySalerEntity(data);
			}
		}else{
			throw new BaseException(tableName+" 表不存在",new Exception());
		}
		return 0L;
	}

	/**
	 * 门店日报
	 */
	private void saveOrUpdateBranchDayEntity(String data) throws BaseException {
		try{
			List<BranchDayEntity> branchDayEntities = JSON.parseArray(data, BranchDayEntity.class);
			for(BranchDayEntity branchDayEntity:branchDayEntities){
				String orgId = branchDayEntity.getOrg_id();
				String branchNo = branchDayEntity.getBranch_no();
				//1.判断是否已经存在今天的数据
				List<BranchDayEntity> todayBranchDayEntities = getBranchDayTodayData(orgId,branchNo);
				//2.根据存在状态悬着更新还是新建
				if(CollectionUtils.isNotEmpty(todayBranchDayEntities)){
					//2.1 update
					BranchDayEntity entityHas = todayBranchDayEntities.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, branchDayEntity);
					entityHas.setId(id);
					branchDayDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					branchDayDao.save(branchDayEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<BranchDayEntity> getBranchDayTodayData(String orgId,String branchNo) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branchNo)){
			throw new Exception("org_id 或 branch_no 为空");
		}
		return branchDayDao.list("where to_days(oper_date) = to_days(now()) " +
				"and org_id = '"+orgId+"' and branch_no = '"+branchNo+"'");
	}
	/**
	 * 品牌日报
	 */
	private void saveOrUpdateBrandDayEntity(String data) throws BaseException {
		try{
			List<BrandDayEntity> branchDayEntities = JSON.parseArray(data, BrandDayEntity.class);
			for(BrandDayEntity brandDayEntity:branchDayEntities){
				String orgId = brandDayEntity.getOrg_id();
				String branchNo = brandDayEntity.getBranch_no();
				String itemBrandno = brandDayEntity.getItem_brandno();
				//1.判断是否已经存在今天的数据
				List<BrandDayEntity> todayBrandDayEntities = getBrandDayTodayData(orgId,branchNo,itemBrandno);
				//2.根据存在状态悬着更新还是新建
				if(CollectionUtils.isNotEmpty(todayBrandDayEntities)){
					//2.1 update
					BrandDayEntity entityHas = todayBrandDayEntities.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, brandDayEntity);
					entityHas.setId(id);
					brandDayDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					brandDayDao.save(brandDayEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<BrandDayEntity> getBrandDayTodayData(String orgId,String branchNo,String itemBrandno) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branchNo)||StringUtils.isEmpty(itemBrandno)){
			throw new Exception("org_id 或 branch_no 或 item_brandno 为空");
		}
		return brandDayDao.list("where to_days(oper_date) = to_days(now()) " +
				"and org_id = '"+orgId+"' and branch_no = '"+branchNo+"' and item_brandno = '"+itemBrandno+"' ");
	}

	/**
	 * 分类日报
	 */
	private void saveOrUpdateClsDayEntity(String data) throws BaseException {
		try{
			List<ClsDayEntity> clsDayEntities = JSON.parseArray(data, ClsDayEntity.class);
			for(ClsDayEntity clsDayEntity:clsDayEntities){
				String orgId = clsDayEntity.getOrg_id();
				String branchNo = clsDayEntity.getBranch_no();
				String itemClsno = clsDayEntity.getItem_clsno();
				//1.判断是否已经存在今天的数据
				List<ClsDayEntity> todayClsDayEntities = getClsDayTodayData(orgId,branchNo,itemClsno);
				//2.根据存在状态悬着更新还是新建
				if(CollectionUtils.isNotEmpty(todayClsDayEntities)){
					//2.1 update
					ClsDayEntity entityHas = todayClsDayEntities.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, clsDayEntity);
					entityHas.setId(id);
					clsDayDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					clsDayDao.save(clsDayEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<ClsDayEntity> getClsDayTodayData(String orgId,String branchNo,String itemClsno) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branchNo)||StringUtils.isEmpty(itemClsno)){
			throw new Exception("org_id 或 branch_no 或 item_clsno 为空");
		}
		return clsDayDao.list("where to_days(oper_date) = to_days(now()) " +
				"and org_id = '"+orgId+"' and branch_no = '"+branchNo+"' and item_clsno = '"+itemClsno+"' ");
	}

	/**
	 * 商品日报
	 */
	private void saveOrUpdateItemDayEntity(String data) throws BaseException {
		try{
			List<ItemDayEntity> itemDayEntities = JSON.parseArray(data, ItemDayEntity.class);
			for(ItemDayEntity itemDayEntity:itemDayEntities){
				String orgId = itemDayEntity.getOrg_id();
				String branchNo = itemDayEntity.getBranch_no();
				String itemNo = itemDayEntity.getItem_no();
				//1.判断是否已经存在今天的数据
				List<ItemDayEntity> todayItemDayEntities = getItemDayTodayData(orgId,branchNo,itemNo);
				//2.根据存在状态悬着更新还是新建
				if(CollectionUtils.isNotEmpty(todayItemDayEntities)){
					//2.1 update
					ItemDayEntity entityHas = todayItemDayEntities.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, itemDayEntity);
					entityHas.setId(id);
					itemDayDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					itemDayDao.save(itemDayEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<ItemDayEntity> getItemDayTodayData(String orgId,String branchNo,String itemNo) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branchNo)||StringUtils.isEmpty(itemNo)){
			throw new Exception("org_id 或 branch_no 或 item_no 为空");
		}
		return itemDayDao.list("where to_days(oper_date) = to_days(now()) " +
				"and org_id = '"+orgId+"' and branch_no = '"+branchNo+"' and item_no = '"+itemNo+"' ");
	}

	/**
	 * 商品售货员日报
	 */
	private void saveOrUpdateItemSalerDayEntity(String data) throws BaseException {
		try{
			List<ItemSalerDayEntity> itemSalerDayEntities = JSON.parseArray(data, ItemSalerDayEntity.class);
			for(ItemSalerDayEntity itemSalerDayEntity:itemSalerDayEntities){
				String orgId = itemSalerDayEntity.getOrg_id();
				String branchNo = itemSalerDayEntity.getBranch_no();
				String itemNo = itemSalerDayEntity.getItem_no();
				String salerId = itemSalerDayEntity.getSale_id();
				//1.判断是否已经存在今天的数据
				List<ItemSalerDayEntity> todayItemSalerDayEntities = getItemSalerDayTodayData(orgId,branchNo,itemNo,salerId);
				//2.根据存在状态悬着更新还是新建
				if(CollectionUtils.isNotEmpty(todayItemSalerDayEntities)){
					//2.1 update
					ItemSalerDayEntity entityHas = todayItemSalerDayEntities.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, itemSalerDayEntity);
					entityHas.setId(id);
					itemSalerDayDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					itemSalerDayDao.save(itemSalerDayEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<ItemSalerDayEntity> getItemSalerDayTodayData(String orgId,String branchNo,String itemNo,String salerId) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branchNo)||StringUtils.isEmpty(salerId)||StringUtils.isEmpty(itemNo)){
			throw new Exception("org_id 或 branch_no 或 sale_id 或item_no 为空");
		}
		return itemSalerDayDao.list("where to_days(oper_date) = to_days(now()) " +
				"and org_id = '"+orgId+"' and branch_no = '"+branchNo+"' and sale_id = '"+salerId+"' and item_no = '"+itemNo+"'");
	}

	/**
	 * 售货员日报
	 */
	private void saveOrUpdateSalerDayEntity(String data) throws BaseException {
		try{
			List<SalerDayEntity> salerDayEntities = JSON.parseArray(data, SalerDayEntity.class);
			for(SalerDayEntity salerDayEntity:salerDayEntities){
				String orgId = salerDayEntity.getOrg_id();
				String branchNo = salerDayEntity.getBranch_no();
				String salerId = salerDayEntity.getSale_id();
				//1.判断是否已经存在今天的数据
				List<SalerDayEntity> todayItemSalerDayEntities = getSalerDayTodayData(orgId,branchNo,salerId);
				//2.根据存在状态悬着更新还是新建
				if(CollectionUtils.isNotEmpty(todayItemSalerDayEntities)){
					//2.1 update
					SalerDayEntity entityHas = todayItemSalerDayEntities.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, salerDayEntity);
					entityHas.setId(id);
					salerDayDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					salerDayDao.save(salerDayEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<SalerDayEntity> getSalerDayTodayData(String orgId,String branchNo,String salerId) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branchNo)||StringUtils.isEmpty(salerId)){
			throw new Exception("org_id 或 branch_no 或 sale_id 为空");
		}
		return salerDayDao.list("where to_days(oper_date) = to_days(now()) " +
				"and org_id = '"+orgId+"' and branch_no = '"+branchNo+"' and sale_id = '"+salerId+"' ");
	}

	/**
	 * 会员日报
	 */
	private void saveOrUpdateVipDayEntity(String data) throws BaseException {
		try{
			List<VipDayEntity> vipDayEntities = JSON.parseArray(data, VipDayEntity.class);
			for(VipDayEntity vipDayEntity:vipDayEntities){
				String orgId = vipDayEntity.getOrg_id();
				String branchNo = vipDayEntity.getBranch_no();
				String cardId = vipDayEntity.getCard_id();
				//1.判断是否已经存在今天的数据
				List<VipDayEntity> todayVipDayEntities = getVipDayTodayData(orgId,branchNo,cardId);
				//2.根据存在状态悬着更新还是新建
				if(CollectionUtils.isNotEmpty(todayVipDayEntities)){
					//2.1 update
					VipDayEntity entityHas = todayVipDayEntities.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, vipDayEntity);
					entityHas.setId(id);
					vipDayDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					vipDayDao.save(vipDayEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<VipDayEntity> getVipDayTodayData(String orgId,String branchNo,String cardId) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branchNo)||StringUtils.isEmpty(cardId)){
			throw new Exception("org_id 或 branch_no 或 card_id 为空");
		}
		return vipDayDao.list("where to_days(oper_date) = to_days(now()) " +
				"and org_id = '"+orgId+"' and branch_no = '"+branchNo+"' and card_id = '"+cardId+"' ");
	}

	/**
	 * 目标表
	 */
	private void saveOrUpdateAimEntity(String data) throws BaseException {
		try{
			List<AimEntity> aimEntityList = JSON.parseArray(data, AimEntity.class);
			for(AimEntity aimEntity:aimEntityList){
				String orgId = aimEntity.getOrg_id();
				Long aimType = aimEntity.getAim_type();
				Long aimCycle = aimEntity.getAim_cycle();
				Long cycleYear = aimEntity.getCycle_year();
				Long cycleMonth = aimEntity.getCycle_month();
				Long cycleWeek = aimEntity.getCycle_week();
				String branch_clsno = aimEntity.getBranch_clsno();
				String branch_no = aimEntity.getBranch_no();
				String item_clsno = aimEntity.getItem_clsno();
				String item_brandno = aimEntity.getItem_brandno();
				String sale_id = aimEntity.getSale_id();
				//1.判断是否已经存在相同的数据
				List<AimEntity> aimEntities = getAimData(orgId,aimType,aimCycle,cycleYear,cycleMonth,cycleWeek,branch_clsno,branch_no,item_clsno,item_brandno,sale_id);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(aimEntities)){
					//2.1 update
					AimEntity entityHas = aimEntities.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, aimEntity);
					entityHas.setId(id);
					aimDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					aimDao.save(aimEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<AimEntity> getAimData(String orgId,Long aimType,Long aimCycle,
								  Long cycleYear,Long cycleMonth,Long cycleWeek,
								  String branch_clsno,String branch_no,String item_clsno,String item_brandno,
								  String sale_id) throws Exception{
		if(StringUtils.isEmpty(orgId)||aimType==null){
			throw new Exception("org_id 或 aim_type 为空");
		}
		return aimDao.list("where org_id = '"+orgId+"' and aim_type = '"+aimType+"'" +
				" and aim_cycle = '"+aimCycle+"' and cycle_year = '"+cycleYear+"'"+
				" and cycle_month = '"+cycleMonth+"' and cycle_week = '"+cycleWeek+"'"+
				" and branch_clsno = '"+branch_clsno+"' and branch_no = '"+branch_no+"'"+
				" and item_clsno = '"+item_clsno+"' and item_brandno = '"+item_brandno+"'  and sale_id = '"+sale_id+"'"
		);
	}

	/**
	 * 门店表
	 */
	private void saveOrUpdateBranchEntity(String data) throws BaseException {
		try{
			List<BranchEntity> branchEntities = JSON.parseArray(data, BranchEntity.class);
			for(BranchEntity branchEntity:branchEntities){
				String orgId = branchEntity.getOrg_id();
				String branch_no = branchEntity.getBranch_no();
				String branch_clsno = branchEntity.getBranch_clsno();

				//1.判断是否已经存在相同的数据
				List<BranchEntity> branchEntities1 = getBranchData(orgId,branch_no,branch_clsno);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(branchEntities1)){
					//2.1 update
					BranchEntity entityHas = branchEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, branchEntity);
					entityHas.setId(id);
					branchDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					branchDao.save(branchEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<BranchEntity> getBranchData(String orgId,String branch_no,String branch_clsno) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branch_no)||StringUtils.isEmpty(branch_clsno)){
			throw new Exception("org_id 或 branch_no 或 branch_clsno 为空");
		}
		return branchDao.list("where org_id = '"+orgId+"' and branch_no = '"+branch_no+"'" +
				" and branch_clsno = '"+branch_clsno+"'"
		);
	}

	/**
	 * 店员表
	 */
	private void saveOrUpdateSalerEntity(String data) throws BaseException {
		try{
			List<SalerEntity> salerEntities = JSON.parseArray(data, SalerEntity.class);
			for(SalerEntity salerEntity:salerEntities){
				String orgId = salerEntity.getOrg_id();
				String saler_id = salerEntity.getSale_id();

				//1.判断是否已经存在相同的数据
				List<SalerEntity> salerEntities1 = getSalerData(orgId,saler_id);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(salerEntities1)){
					//2.1 update
					SalerEntity entityHas = salerEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, salerEntity);
					entityHas.setId(id);
					salerDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					salerDao.save(salerEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<SalerEntity> getSalerData(String orgId,String saler_id) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(saler_id)){
			throw new Exception("org_id 或 saler_id 为空");
		}
		return salerDao.list("where org_id = '"+orgId+"' and sale_id = '"+saler_id+"'");
	}

	/**
	 * 商品表
	 */
	private void saveOrUpdateItemEntity(String data) throws BaseException {
		try{
			List<ItemEntity> itemEntities = JSON.parseArray(data, ItemEntity.class);
			for(ItemEntity itemEntity:itemEntities){
				String orgId = itemEntity.getOrg_id();
				String item_no = itemEntity.getItem_no();
				String item_subno = itemEntity.getItem_subno();

				//1.判断是否已经存在相同的数据
				List<ItemEntity> itemEntities1 = getItemData(orgId,item_no,item_subno);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(itemEntities1)){
					//2.1 update
					ItemEntity entityHas = itemEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, itemEntity);
					entityHas.setId(id);
					itemDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					itemDao.save(itemEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<ItemEntity> getItemData(String orgId,String item_no,String item_subno) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(item_no)||StringUtils.isEmpty(item_subno)){
			throw new Exception("org_id 或 item_no 或 item_subno 为空");
		}
		return itemDao.list("where org_id = '"+orgId+"' and item_no = '"+item_no+"' and item_subno ='"+item_subno+"'");
	}

	/**
	 * 库存表
	 */
	private void saveOrUpdateStockEntity(String data) throws BaseException {
		try{
			List<StockEntity> stockEntities = JSON.parseArray(data, StockEntity.class);
			for(StockEntity stockEntity:stockEntities){
				String orgId = stockEntity.getOrg_id();
				String branch_no = stockEntity.getBranch_no();
				String item_no = stockEntity.getItem_no();

				//1.判断是否已经存在相同的数据
				List<StockEntity> stockEntities1 = getStockData(orgId,branch_no,item_no);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(stockEntities1)){
					//2.1 update
					StockEntity entityHas = stockEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, stockEntity);
					entityHas.setId(id);
					stockDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					stockDao.save(stockEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<StockEntity> getStockData(String orgId,String branch_no,String item_no) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branch_no)||StringUtils.isEmpty(item_no)){
			throw new Exception("org_id 或 branch_no 或 item_no 为空");
		}
		return stockDao.list("where org_id = '"+orgId+"' and branch_no = '"+branch_no+"' and item_no ='"+item_no+"'");
	}

	/**
	 * 门店时段表
	 */
	private void saveOrUpdateBranchTimeEntity(String data) throws BaseException {
		try{
			List<BranchTimeEntity> branchTimeEntities = JSON.parseArray(data, BranchTimeEntity.class);
			for(BranchTimeEntity branchTimeEntity:branchTimeEntities){
				String orgId = branchTimeEntity.getOrg_id();
				String branch_no = branchTimeEntity.getBranch_no();

				//1.判断是否已经存在相同的数据
				List<BranchTimeEntity> branchTimeEntities1 = getBranchTimeData(orgId,branch_no);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(branchTimeEntities1)){
					//2.1 update
					BranchTimeEntity entityHas = branchTimeEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, branchTimeEntity);
					entityHas.setId(id);
					branchTimeDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					branchTimeDao.save(branchTimeEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<BranchTimeEntity> getBranchTimeData(String orgId,String branch_no) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branch_no)){
			throw new Exception("org_id 或 branch_no 为空");
		}
		return branchTimeDao.list("where org_id = '"+orgId+"' and branch_no = '"+branch_no+"'");
	}

	/**
	 * 商品分类表
	 */
	private void saveOrUpdateItemClsEntity(String data) throws BaseException {
		try{
			List<ItemClsEntity> itemClsEntities = JSON.parseArray(data, ItemClsEntity.class);
			for(ItemClsEntity itemClsEntity:itemClsEntities){
				String orgId = itemClsEntity.getOrg_id();
				String cls_no = itemClsEntity.getItem_clsno();

				//1.判断是否已经存在相同的数据
				List<ItemClsEntity> itemClsEntities1 = getItemClsEntityData(orgId,cls_no);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(itemClsEntities1)){
					//2.1 update
					ItemClsEntity entityHas = itemClsEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, itemClsEntity);
					entityHas.setId(id);
					itemClsDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					itemClsDao.save(itemClsEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<ItemClsEntity> getItemClsEntityData(String orgId,String cls_no) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(cls_no)){
			throw new Exception("org_id 或 item_clsno 为空");
		}
		return itemClsDao.list("where org_id = '"+orgId+"' and item_clsno = '"+cls_no+"'");
	}

	/**
	 * 商品价格区间表
	 */
	private void saveOrUpdatePriceZoneEntity(String data) throws BaseException {
		try{
			List<PriceZoneEntity> priceZoneEntities = JSON.parseArray(data, PriceZoneEntity.class);
			for(PriceZoneEntity priceZoneEntity:priceZoneEntities){
				String orgId = priceZoneEntity.getOrg_id();
				String price_zone_no = priceZoneEntity.getPrice_zone_no();

				//1.判断是否已经存在相同的数据
				List<PriceZoneEntity> priceZoneEntities1 = getPriceZoneData(orgId,price_zone_no);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(priceZoneEntities1)){
					//2.1 update
					PriceZoneEntity entityHas = priceZoneEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, priceZoneEntity);
					entityHas.setId(id);
					priceZoneDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					priceZoneDao.save(priceZoneEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<PriceZoneEntity> getPriceZoneData(String orgId,String price_zone_no) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(price_zone_no)){
			throw new Exception("org_id 或 price_zone_no 为空");
		}
		return priceZoneDao.list("where org_id = '"+orgId+"' and price_zone_no = '"+price_zone_no+"'");
	}

	/**
	 * vip会员信息表
	 */
	private void saveOrUpdateVipEntity(String data) throws BaseException {
		try{
			List<VipEntity> vipEntities = JSON.parseArray(data, VipEntity.class);
			for(VipEntity vipEntity:vipEntities){
				String orgId = vipEntity.getOrg_id();
				String card_id = vipEntity.getCard_id();
				String card_no = vipEntity.getCard_no();

				//1.判断是否已经存在相同的数据
				List<VipEntity> vipEntities1 = getVipData(orgId,card_id,card_no);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(vipEntities1)){
					//2.1 update
					VipEntity entityHas = vipEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, vipEntity);
					entityHas.setId(id);
					vipDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					vipDao.save(vipEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<VipEntity> getVipData(String orgId,String card_id,String card_no) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(card_id)||StringUtils.isEmpty(card_no)){
			throw new Exception("org_id 或 price_zone_no 或 card_no 为空");
		}
		return vipDao.list("where org_id = '"+orgId+"' and card_id = '"+card_id+"' and card_no='"+card_no+"'");
	}

	/**
	 * 门店分类表
	 */
	private void saveOrUpdateBranchClsEntity(String data) throws BaseException {
		try{
			List<BranchClsEntity> branchClsEntities = JSON.parseArray(data, BranchClsEntity.class);
			for(BranchClsEntity branchClsEntity:branchClsEntities){
				String orgId = branchClsEntity.getOrg_id();
				String branch_clsno = branchClsEntity.getBranch_clsno();

				//1.判断是否已经存在相同的数据
				List<BranchClsEntity> branchClsEntities1 = getBranchClData(orgId,branch_clsno);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(branchClsEntities1)){
					//2.1 update
					BranchClsEntity entityHas = branchClsEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, branchClsEntity);
					entityHas.setId(id);
					branchClsDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					branchClsDao.save(branchClsEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<BranchClsEntity> getBranchClData(String orgId,String branch_clsno) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(branch_clsno)){
			throw new Exception("org_id 或 branch_clsno 为空");
		}
		return branchClsDao.list("where org_id = '"+orgId+"' and branch_clsno = '"+branch_clsno+"'");
	}

	/**
	 * 品牌表
	 */
	private void saveOrUpdateBrandEntity(String data) throws BaseException {
		try{
			List<BrandEntity> brandEntities = JSON.parseArray(data, BrandEntity.class);
			for(BrandEntity brandEntity:brandEntities){
				String orgId = brandEntity.getOrg_id();
				String item_brandno = brandEntity.getItem_brandno();

				//1.判断是否已经存在相同的数据
				List<BrandEntity> brandEntities1 = getBrandData(orgId,item_brandno);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(brandEntities1)){
					//2.1 update
					BrandEntity entityHas = brandEntities1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, brandEntity);
					entityHas.setId(id);
					brandDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					brandDao.save(brandEntity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}

	List<BrandEntity> getBrandData(String orgId,String item_brandno) throws Exception{
		if(StringUtils.isEmpty(orgId)||StringUtils.isEmpty(item_brandno)){
			throw new Exception("org_id 或 item_brandno 为空");
		}
		return brandDao.list("where org_id = '"+orgId+"' and item_brandno = '"+item_brandno+"'");
	}
	
	/*
	 * 提成明细表
	 */
	private void saveOrUpdateSalaryDetailEntity(String data) throws BaseException {
		try{
			List<SalaryDetailEntity> entityList = JSON.parseArray(data, SalaryDetailEntity.class);
			for(SalaryDetailEntity entity:entityList){
				String orgId = entity.getOrg_id();
				java.util.Date operDate = entity.getOper_date();
				String deduct_no = entity.getDeduct_no();
				String branch_no = entity.getBranch_no();
				String saler_id = entity.getSaler_id();
				String item_no = entity.getItem_no();
				String voucher_no = entity.getVoucher_no();

				//1.判断是否已经存在相同的数据
				if(StringUtils.isEmpty(orgId)||operDate==null||StringUtils.isEmpty(deduct_no) || StringUtils.isEmpty(branch_no) || StringUtils.isEmpty(saler_id) || StringUtils.isEmpty(item_no) || StringUtils.isEmpty(voucher_no)){
					throw new Exception("org_id、operDate、deduct_no、branch_no、saler_id、item_no、voucher_no为必需字段，不能为空");
				}
				List<Object> params = new ArrayList<Object>();
				params.add(operDate);
				params.add(orgId);
				params.add(deduct_no);
				params.add(branch_no);
				params.add(saler_id);
				params.add(item_no);
				params.add(voucher_no);
				List<SalaryDetailEntity> entityList1 = salaryDetailDao.list("where to_days(oper_date) = to_days(?) and org_id = ? and deduct_no = ? and branch_no = ? and saler_id = ? and item_no = ? and voucher_no = ?", params);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(entityList1)){
					//2.1 update
					SalaryDetailEntity entityHas = entityList1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, entity);
					entityHas.setId(id);
					salaryDetailDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					salaryDetailDao.save(entity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}
	
	/*
	 * 门店提成表
	 */
	private void saveOrUpdateSalaryBranchEntity(String data) throws BaseException {
		try{
			List<SalaryBranchEntity> entityList = JSON.parseArray(data, SalaryBranchEntity.class);
			for(SalaryBranchEntity entity:entityList){
				String orgId = entity.getOrg_id();
				java.util.Date operDate = entity.getOper_date();
				String deduct_no = entity.getDeduct_no();
				String branch_no = entity.getBranch_no();

				//1.判断是否已经存在相同的数据
				if(StringUtils.isEmpty(orgId)||operDate==null||StringUtils.isEmpty(deduct_no) || StringUtils.isEmpty(branch_no)){
					throw new Exception("org_id、operDate、deduct_no、branch_no为必需字段，不能为空");
				}
				List<Object> params = new ArrayList<Object>();
				params.add(operDate);
				params.add(orgId);
				params.add(deduct_no);
				params.add(branch_no);
				List<SalaryBranchEntity> entityList1 = salaryBranchDao.list("where to_days(oper_date) = to_days(?) and org_id = ? and deduct_no = ? and branch_no = ? ", params);
				//2.根据存在状态选择更新还是新建
				if(CollectionUtils.isNotEmpty(entityList1)){
					//2.1 update
					SalaryBranchEntity entityHas = entityList1.get(0);
					Long id = entityHas.getId();
					BeanUtil.copyBeanProperties(entityHas, entity);
					entityHas.setId(id);
					salaryBranchDao.updateEntity(entityHas);
				}else{
					//2.2 insert
					salaryBranchDao.save(entity);
				}
			}
		}catch (Exception e){
			throw new BaseException(e.getMessage(),e);
		}
	}
	
	/*
	 * 销售员提成表
	 */
	private void saveOrUpdateSalarySalerEntity(String data) throws BaseException {
		List<SalarySalerEntity> entityList = JSON.parseArray(data, SalarySalerEntity.class);
		for(SalarySalerEntity entity:entityList){
			String orgId = entity.getOrg_id();
			java.util.Date operDate = entity.getOper_date();
			String deduct_no = entity.getDeduct_no();
			String branch_no = entity.getBranch_no();
			String saler_id = entity.getSaler_id();
			//1.判断是否已经存在相同的数据
			if(StringUtils.isEmpty(orgId)||operDate==null||StringUtils.isEmpty(deduct_no) || StringUtils.isEmpty(branch_no) || StringUtils.isEmpty(saler_id)){
				throw new BaseException("-1", "org_id、operDate、deduct_no、branch_no、saler_id为必需字段，不能为空");
			}
			List<Object> params = new ArrayList<Object>();
			params.add(operDate);
			params.add(orgId);
			params.add(deduct_no);
			params.add(branch_no);
			params.add(saler_id);
			List<SalarySalerEntity> entityList1 = salarySalerDao.list("where to_days(oper_date) = to_days(?) and org_id = ? and deduct_no = ? and branch_no = ? and saler_id = ? ", params);
			//2.根据存在状态选择更新还是新建
			if(CollectionUtils.isNotEmpty(entityList1)){
				//2.1 update
				SalarySalerEntity entityHas = entityList1.get(0);
				Long id = entityHas.getId();
				BeanUtil.copyBeanProperties(entityHas, entity);
				entityHas.setId(id);
				salarySalerDao.updateEntity(entityHas);
			}else{
				//2.2 insert
				salarySalerDao.save(entity);
			}
		}
	}
}
