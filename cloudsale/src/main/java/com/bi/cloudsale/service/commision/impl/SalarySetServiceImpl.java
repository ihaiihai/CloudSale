package com.bi.cloudsale.service.commision.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.dto.commision.ItemInfoDto;
import com.bi.cloudsale.persistent.dao.item.BaseItemInfoDao;
import com.bi.cloudsale.persistent.entity.item.BaseItemInfoEntity;
import com.bi.cloudsale.service.commision.SalarySetService;

@Service
@Transactional
public class SalarySetServiceImpl implements SalarySetService {
	
	private static final Logger log = LoggerFactory.getLogger(SalarySetServiceImpl.class);
	
	@Resource
	private BaseItemInfoDao baseItemInfoDao;

	@Override
	public List<ItemInfoDto> queryByCondition(String orgId, String clsNo, String key, Integer pageIndex,
			Integer pageSize) {
		List<ItemInfoDto> dtoList = new ArrayList<ItemInfoDto>();
		List<BaseItemInfoEntity> entityList = baseItemInfoDao.queryByCondition(orgId, clsNo, key, pageIndex, pageSize);
		try {
			BeanUtil.copyBeanPropertiesList(dtoList, entityList, ItemInfoDto.class);
		} catch (BaseException e) {
			log.error(e.getMessage(), e);
		}
		return dtoList;
	}

	
}
