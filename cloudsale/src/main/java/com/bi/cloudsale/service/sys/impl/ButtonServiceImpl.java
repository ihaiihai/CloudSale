package com.bi.cloudsale.service.sys.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.ButtonDto;
import com.bi.cloudsale.persistent.dao.sys.ButtonInfoDao;
import com.bi.cloudsale.persistent.dao.sys.ModuleInfoDao;
import com.bi.cloudsale.persistent.entity.sys.ButtonInfoEntity;
import com.bi.cloudsale.service.sys.ButtonService;

@Service
@Transactional
public class ButtonServiceImpl implements ButtonService {
	
	@Resource
	private ButtonInfoDao buttonInfoDao;
	@Resource
	private ModuleInfoDao moduleInfoDao;

	@Override
	public List<ButtonDto> queryAll() throws BaseException {
		List<ButtonDto> dtoList = new ArrayList<ButtonDto>();
		List<ButtonInfoEntity> entityList = buttonInfoDao.queryAll();
		BeanUtil.copyBeanPropertiesList(dtoList, entityList, ButtonDto.class);
		return dtoList;
	}

	@Override
	public List<OptionDto> queryAllOptions() {
		List<OptionDto> list = new ArrayList<OptionDto>();
		Map<String,OptionDto> map = new HashMap<String,OptionDto>();
		List<ButtonInfoEntity> buttonList = buttonInfoDao.queryAll();
		if(buttonList != null && buttonList.size() > 0) {
			Iterator<ButtonInfoEntity> iter = buttonList.iterator();
			while(iter.hasNext()) {
				ButtonInfoEntity buttonInfoEntity = iter.next();
				OptionDto optionDto = new OptionDto();
				optionDto.setId(buttonInfoEntity.getButtonCode());
				optionDto.setLabel(buttonInfoEntity.getButtonName());
				if(map.get(buttonInfoEntity.getModuleId()) == null) {
					OptionDto parentDto = new OptionDto();
					parentDto.setId(buttonInfoEntity.getModuleId());
					parentDto.setLabel(buttonInfoEntity.getModuleName());
					parentDto.setChildren(new ArrayList<OptionDto>());
					map.put(buttonInfoEntity.getModuleId(), parentDto);
					list.add(parentDto);
				}
				list.get(list.indexOf(map.get(buttonInfoEntity.getModuleId()))).getChildren().add(optionDto);
			}
		}
		return list;
	}

}
