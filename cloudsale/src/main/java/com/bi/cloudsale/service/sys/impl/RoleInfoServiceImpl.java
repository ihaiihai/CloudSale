package com.bi.cloudsale.service.sys.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.RoleAuthDto;
import com.bi.cloudsale.dto.sys.RoleInfoDto;
import com.bi.cloudsale.persistent.dao.sys.RoleAuthDao;
import com.bi.cloudsale.persistent.dao.sys.RoleInfoDao;
import com.bi.cloudsale.persistent.entity.sys.RoleAuthEntity;
import com.bi.cloudsale.persistent.entity.sys.RoleInfoEntity;
import com.bi.cloudsale.persistent.enums.sys.CanSeeDataEnum;
import com.bi.cloudsale.persistent.enums.sys.CanSeeProfitEnum;
import com.bi.cloudsale.persistent.enums.sys.RoleAuthTypeEnum;
import com.bi.cloudsale.service.sys.RoleAuthService;
import com.bi.cloudsale.service.sys.RoleInfoService;

@Service
@Transactional
public class RoleInfoServiceImpl implements RoleInfoService {
	
	private static final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);
	
	@Resource
	private RoleInfoDao roleInfoDao;
	@Resource
	private RoleAuthDao roleAuthDao;
	@Resource
	private RoleAuthService roleAuthService;

	@Override
	public void createRoleInfo(RoleInfoDto roleInfoDto) throws BaseException {
		RoleInfoEntity entity = new RoleInfoEntity();
		BeanUtil.copyProperties(roleInfoDto, entity);
		if(StringUtils.isEmpty(entity.getRoleCode())) {
			entity.setRoleCode(UUID.randomUUID().toString().replaceAll("-", ""));
		}
		try {
			roleInfoDao.createRoleInfo(entity);
			
			RoleAuthEntity roleAuthEntity = new RoleAuthEntity();
			roleAuthEntity.setUserId(roleInfoDto.getUserId());
			roleAuthEntity.setOrgId(roleInfoDto.getOrgId());
			roleAuthEntity.setRoleCode(entity.getRoleCode());
			roleAuthEntity.setItemId(roleInfoDto.getMenuCheckedIds());
			roleAuthEntity.setItemType(RoleAuthTypeEnum.MENU.getValue());
			roleAuthDao.createRoleAuth(roleAuthEntity);
			
			roleAuthEntity = new RoleAuthEntity();
			roleAuthEntity.setUserId(roleInfoDto.getUserId());
			roleAuthEntity.setOrgId(roleInfoDto.getOrgId());
			roleAuthEntity.setRoleCode(entity.getRoleCode());
			roleAuthEntity.setItemId(roleInfoDto.getButtonCheckedIds());
			roleAuthEntity.setItemType(RoleAuthTypeEnum.BUTTON.getValue());
			roleAuthDao.createRoleAuth(roleAuthEntity);
			
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public void deleteRoleInfo(String roleCode) throws BaseException {
		try {
			roleInfoDao.deleteRoleInfo(roleCode);
			roleAuthDao.deleteRoleAuth(roleCode, RoleAuthTypeEnum.MENU.getValue());
			roleAuthDao.deleteRoleAuth(roleCode, RoleAuthTypeEnum.BUTTON.getValue());
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public void updateRoleInfo(RoleInfoDto roleInfoDto) throws BaseException {
		try {
			RoleInfoEntity entity = roleInfoDao.queryByCode(roleInfoDto.getRoleCode());
			entity.setRoleName(roleInfoDto.getRoleName());
			entity.setCanSeeData(roleInfoDto.getCanSeeData());
			entity.setCanSeeProfit(roleInfoDto.getCanSeeProfit());
			entity.setDescription(roleInfoDto.getDescription());
			roleInfoDao.updateRoleInfo(entity);
			
			RoleAuthDto roleAuthDto = new RoleAuthDto();
			roleAuthDto.setRoleCode(entity.getRoleCode());
			roleAuthDto.setItemId(roleInfoDto.getMenuCheckedIds());
			roleAuthDto.setItemType(RoleAuthTypeEnum.MENU.getValue());
			roleAuthService.updateRoleAuth(roleAuthDto);
			
			roleAuthDto = new RoleAuthDto();
			roleAuthDto.setRoleCode(entity.getRoleCode());
			roleAuthDto.setItemId(roleInfoDto.getButtonCheckedIds());
			roleAuthDto.setItemType(RoleAuthTypeEnum.BUTTON.getValue());
			roleAuthService.updateRoleAuth(roleAuthDto);
			
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public List<RoleInfoDto> queryByCondition(BaseQueryCondition baseQueryCondition) {
		List<RoleInfoDto> dtos = new ArrayList<RoleInfoDto>();
		List<RoleInfoEntity> entitys = roleInfoDao.queryByCondition(baseQueryCondition);
		if(entitys != null && entitys.size() > 0) {
			Iterator<RoleInfoEntity> iter = entitys.iterator();
			while(iter.hasNext()) {
				RoleInfoEntity roleInfoEntity = iter.next();
				RoleInfoDto roleInfoDto = new RoleInfoDto();
				BeanUtil.copyProperties(roleInfoEntity, roleInfoDto);
				roleInfoDto.setCanSeeDataStr(CanSeeDataEnum.valueOf(roleInfoDto.getCanSeeData()).getName());
				roleInfoDto.setCanSeeProfitStr(CanSeeProfitEnum.valueOf(roleInfoDto.getCanSeeProfit()).getName());
				dtos.add(roleInfoDto);
			}
		}
		return dtos;
	}

	@Override
	public RoleInfoDto queryByCode(String roleCode) {
		RoleInfoDto dto = new RoleInfoDto();
		RoleInfoEntity entity = roleInfoDao.queryByCode(roleCode);
		if(entity != null) {
			BeanUtil.copyProperties(entity, dto);
			RoleAuthEntity menuAuthEntity = roleAuthDao.queryByRoleCode(roleCode, RoleAuthTypeEnum.MENU.getValue());
			if(menuAuthEntity != null) {
				dto.setMenuCheckedIds(menuAuthEntity.getItemId());
			}
			RoleAuthEntity muttonAuthEntity = roleAuthDao.queryByRoleCode(roleCode, RoleAuthTypeEnum.BUTTON.getValue());
			if(muttonAuthEntity != null) {
				dto.setButtonCheckedIds(muttonAuthEntity.getItemId());
			}
		}
		return dto;
	}

	@Override
	public List<OptionDto> queryOptions(String userId) {
		List<OptionDto> list = new ArrayList<OptionDto>();
		BaseQueryCondition baseQueryCondition = new BaseQueryCondition();
		baseQueryCondition.setUserId(userId);
		List<RoleInfoEntity> entitys = roleInfoDao.queryByCondition(baseQueryCondition);
		if(entitys != null && entitys.size() > 0) {
			Iterator<RoleInfoEntity> iter = entitys.iterator();
			while(iter.hasNext()) {
				RoleInfoEntity roleInfoEntity = iter.next();
				OptionDto optionDto = new OptionDto();
				optionDto.setId(roleInfoEntity.getRoleCode());
				optionDto.setLabel(roleInfoEntity.getRoleName());
				list.add(optionDto);
			}
		}
		return list;
	}

}
