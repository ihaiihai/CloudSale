package com.bi.cloudsale.service.item;

import java.util.List;

import com.bi.cloudsale.dto.item.BaseItemInfoDto;

public interface BaseItemInfoService {

	List<BaseItemInfoDto> queryByCondition(String orgId, String clsNo, String key, Integer pageIndex, Integer pageSize);
}
