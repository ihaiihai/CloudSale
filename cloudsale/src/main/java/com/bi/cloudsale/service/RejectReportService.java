package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/15 17:45
 * @Version: 1.0
 **/
public interface RejectReportService {
    JSONArray branches(String orgId) throws BaseException;
    JSONArray branchRejectTop10(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray branchSaleRejectRetlv(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray branchClsRejectFenbu(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray branchItemRejectDetail(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray branchRetRetlv(String orgId,String begin,String end,String branchNo) throws BaseException;
}
