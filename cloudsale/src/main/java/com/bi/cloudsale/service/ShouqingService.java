package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;

import java.text.ParseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/15 17:45
 * @Version: 1.0
 **/
public interface ShouqingService {
    JSONArray branches(String orgId) throws BaseException;
    JSONArray clses(String orgId) throws BaseException;
    JSONArray brands(String orgId) throws BaseException;
    JSONArray items(String orgId) throws BaseException;

    JSONArray query(String orgId, String type,String begin,String end, String branchNos, String itemClses, String brands,Integer pageNo,Integer pageSize) throws BaseException;
    JSONArray itemSaleStock(String orgId,String itemNo, String begin,String end) throws BaseException;
}
