package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/22 21:51
 * @Version: 1.0
 **/
public interface CuxiaoReportService {
    JSONArray xiaoshou_lirun_maoli(String orgId, String begin, String end, String branchNos) throws BaseException;
    JSONArray kehu_huiyuan(String orgId, String begin, String end, String branchNos) throws BaseException;
    JSONArray tongqi_huanbi(String orgId, String begin, String end, String branchNos) throws BaseException;
    JSONArray keliu_kedan(String orgId, String begin, String end, String branchNos) throws BaseException;
    JSONArray huangshi(String orgId, String begin, String end, String branchNos) throws BaseException;
    JSONArray baofadu(String orgId, String begin, String end, String branchNos) throws BaseException;
    JSONArray zhekou(String orgId, String begin, String end, String branchNos) throws BaseException;
    JSONArray item_rank(String orgId, String begin, String end, String branchNos) throws BaseException;
}
