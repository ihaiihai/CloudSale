package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.service.ClientFenbuService;
import com.bi.cloudsale.service.CuxiaoReportService;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

@Service
@Transactional
public class CuxiaoReportServiceImpl implements CuxiaoReportService {

    private static final Logger log = LoggerFactory.getLogger(CuxiaoReportServiceImpl.class);

    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private ItemDayDao itemDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private BrandDayDao brandDayDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private VipDayDao vipDayDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private SalerDayDao salerDayDao;
    @Resource
    private BrandDao brandDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray xiaoshou_lirun_maoli(String orgId, String begin, String end, String branchNos) throws BaseException {
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        String brand_noSql = "";
        if(StringUtils.isNotEmpty(branch_nos_str)){
            brand_noSql = " and t1.branch_no in ("+branch_nos_str+")";
        }
        double all_sale =0;
        double all_cost =0;
        List<Object> all_sales = unionSqlDao.query("select sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+brand_noSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[0];
            all_sale += total_sale!=null?total_sale:0;
            Double total_cost = (Double)itemEntityObj[1];
            all_cost += total_cost!=null?total_cost:0;
        }

        JSONArray results = new JSONArray();
        JSONObject resultJson = new JSONObject();
        resultJson.put("销售",NumberUtil.getDouble2Format(all_sale));
        resultJson.put("利润",NumberUtil.getDouble2Format(all_sale-all_cost));
        results.add(resultJson);
        JSONObject resultJson1 = new JSONObject();
        resultJson1.put("name","完成销售共计【"+NumberUtil.getDouble2Format(all_sale)+"元】,毛利率【"+NumberUtil.getPercentFormat((all_sale-all_cost)/all_sale)+"】");
        results.add(resultJson1);
        return results;
    }

    @Override
    public JSONArray kehu_huiyuan(String orgId, String begin, String end, String branchNos) throws BaseException {
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        String brand_noSql = "";
        if(StringUtils.isNotEmpty(branch_nos_str)){
            brand_noSql = " and branch_no in ("+branch_nos_str+")";
        }
        Long total_new_vips = 0L;
        List<Object> newJoinVips = unionSqlDao.query("select count(*) from VipEntity where org_id = '"+orgId+"'"+brand_noSql+" and gmt_create >='"+begin+"' and gmt_create <='"+end+"'");
        if(CollectionUtils.isNotEmpty(newJoinVips)){
            for(Object itemEntity:newJoinVips){
                total_new_vips = (Long)itemEntity;
            }
        }
        Long total_bill_cnt = 0L;
        Long total_bill_vip_cnt = 0L;
        List<Object> all_sales = unionSqlDao.query("select sum(t1.bill_cnt) as bill_cnt,sum(t1.bill_vip_cnt) as bill_vip_cnt from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+brand_noSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Long bill_cnt = (Long)itemEntityObj[0];
            total_bill_cnt += bill_cnt!=null?bill_cnt:0;
            Long bill_vip_cnt = (Long)itemEntityObj[1];
            total_bill_vip_cnt += bill_vip_cnt!=null?bill_vip_cnt:0;
        }
        JSONArray results = new JSONArray();
        JSONObject resultJson = new JSONObject();
        resultJson.put("新增客户",NumberUtil.getDouble2Format(total_bill_cnt-total_bill_vip_cnt));
        resultJson.put("新增会员",NumberUtil.getDouble2Format(total_new_vips));
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray tongqi_huanbi(String orgId, String begin, String end, String branchNos) throws BaseException {
        class ItemData{
            double total_sale;
            double client_cnt;
            double total_sale_month;
            double client_cnt_month;
            double total_sale_year;
            double client_cnt_year;

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getClient_cnt() {
                return client_cnt;
            }

            public void setClient_cnt(double client_cnt) {
                this.client_cnt = client_cnt;
            }

            public double getTotal_sale_month() {
                return total_sale_month;
            }

            public void setTotal_sale_month(double total_sale_month) {
                this.total_sale_month = total_sale_month;
            }

            public double getClient_cnt_month() {
                return client_cnt_month;
            }

            public void setClient_cnt_month(double client_cnt_month) {
                this.client_cnt_month = client_cnt_month;
            }

            public double getTotal_sale_year() {
                return total_sale_year;
            }

            public void setTotal_sale_year(double total_sale_year) {
                this.total_sale_year = total_sale_year;
            }

            public double getClient_cnt_year() {
                return client_cnt_year;
            }

            public void setClient_cnt_year(double client_cnt_year) {
                this.client_cnt_year = client_cnt_year;
            }
        }
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        String brand_noSql = "";
        if(StringUtils.isNotEmpty(branch_nos_str)){
            brand_noSql = " and branch_no in ("+branch_nos_str+")";
        }
        Map<Long,ItemData> dataMap = Maps.newLinkedHashMap();
        //1销量、销售额
        List<BranchTimeEntity> branchTimeEntities = branchTimeDao.list("where org_id='"+orgId+"' and oper_date<='" +end+
                "' and oper_date >= '"+begin+"'"+brand_noSql);
        for(BranchTimeEntity branchTimeEntity:branchTimeEntities){
            Long hour00 = 0L;
            ItemData dataDay00 = dataMap.get(hour00);
            if(dataDay00!=null){
                dataDay00.setTotal_sale(dataDay00.getTotal_sale() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataDay00.setClient_cnt(dataDay00.getClient_cnt()+ (branchTimeEntity.getCnt_00()!=null?branchTimeEntity.getCnt_00():0));
            }else{
                dataDay00 = new ItemData();
                dataDay00.setTotal_sale(dataDay00.getTotal_sale() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataDay00.setClient_cnt(dataDay00.getClient_cnt()+ (branchTimeEntity.getCnt_00()!=null?branchTimeEntity.getCnt_00():0));
                dataMap.put(hour00,dataDay00);
            }
            Long hour01 = 1L;
            ItemData dataDay01 = dataMap.get(hour01);
            if(dataDay01!=null){
                dataDay01.setTotal_sale(dataDay01.getTotal_sale() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataDay01.setClient_cnt(dataDay01.getClient_cnt()+ (branchTimeEntity.getCnt_01()!=null?branchTimeEntity.getCnt_01():0));
            }else{
                dataDay01 = new ItemData();
                dataDay01.setTotal_sale(dataDay01.getTotal_sale() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataDay01.setClient_cnt(dataDay01.getClient_cnt()+ (branchTimeEntity.getCnt_01()!=null?branchTimeEntity.getCnt_01():0));
                dataMap.put(hour01,dataDay01);
            }
            Long hour02 = 2L;
            ItemData dataDay02 = dataMap.get(hour02);
            if(dataDay02!=null){
                dataDay02.setTotal_sale(dataDay02.getTotal_sale() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataDay02.setClient_cnt(dataDay02.getClient_cnt()+ (branchTimeEntity.getCnt_02()!=null?branchTimeEntity.getCnt_02():0));
            }else{
                dataDay02 = new ItemData();
                dataDay02.setTotal_sale(dataDay02.getTotal_sale() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataDay02.setClient_cnt(dataDay02.getClient_cnt()+ (branchTimeEntity.getCnt_02()!=null?branchTimeEntity.getCnt_02():0));
                dataMap.put(hour02,dataDay02);
            }
            Long hour03 = 3L;
            ItemData dataDay03 = dataMap.get(hour03);
            if(dataDay03!=null){
                dataDay03.setTotal_sale(dataDay03.getTotal_sale() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataDay03.setClient_cnt(dataDay03.getClient_cnt()+ (branchTimeEntity.getCnt_03()!=null?branchTimeEntity.getCnt_03():0));
            }else{
                dataDay03 = new ItemData();
                dataDay03.setTotal_sale(dataDay03.getTotal_sale() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataDay03.setClient_cnt(dataDay03.getClient_cnt()+ (branchTimeEntity.getCnt_03()!=null?branchTimeEntity.getCnt_03():0));
                dataMap.put(hour03,dataDay03);
            }
            Long hour04 = 4L;
            ItemData dataDay04 = dataMap.get(hour04);
            if(dataDay04!=null){
                dataDay04.setTotal_sale(dataDay04.getTotal_sale() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataDay04.setClient_cnt(dataDay04.getClient_cnt()+ (branchTimeEntity.getCnt_04()!=null?branchTimeEntity.getCnt_04():0));
            }else{
                dataDay04 = new ItemData();
                dataDay04.setTotal_sale(dataDay04.getTotal_sale() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataDay04.setClient_cnt(dataDay04.getClient_cnt()+ (branchTimeEntity.getCnt_04()!=null?branchTimeEntity.getCnt_04():0));
                dataMap.put(hour04,dataDay04);
            }
            Long hour05 = 5L;
            ItemData dataDay05 = dataMap.get(hour05);
            if(dataDay05!=null){
                dataDay05.setTotal_sale(dataDay05.getTotal_sale() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataDay05.setClient_cnt(dataDay05.getClient_cnt()+ (branchTimeEntity.getCnt_05()!=null?branchTimeEntity.getCnt_05():0));
            }else{
                dataDay05 = new ItemData();
                dataDay05.setTotal_sale(dataDay05.getTotal_sale() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataDay05.setClient_cnt(dataDay05.getClient_cnt()+ (branchTimeEntity.getCnt_05()!=null?branchTimeEntity.getCnt_05():0));
                dataMap.put(hour05,dataDay05);
            }
            Long hour06 = 6L;
            ItemData dataDay06 = dataMap.get(hour06);
            if(dataDay06!=null){
                dataDay06.setTotal_sale(dataDay06.getTotal_sale() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataDay06.setClient_cnt(dataDay06.getClient_cnt()+ (branchTimeEntity.getCnt_06()!=null?branchTimeEntity.getCnt_06():0));
            }else{
                dataDay06 = new ItemData();
                dataDay06.setTotal_sale(dataDay06.getTotal_sale() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataDay06.setClient_cnt(dataDay06.getClient_cnt()+ (branchTimeEntity.getCnt_06()!=null?branchTimeEntity.getCnt_06():0));
                dataMap.put(hour06,dataDay06);
            }
            Long hour07 = 7L;
            ItemData dataDay07 = dataMap.get(hour07);
            if(dataDay07!=null){
                dataDay07.setTotal_sale(dataDay07.getTotal_sale() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataDay07.setClient_cnt(dataDay07.getClient_cnt()+ (branchTimeEntity.getCnt_07()!=null?branchTimeEntity.getCnt_07():0));
            }else{
                dataDay07 = new ItemData();
                dataDay07.setTotal_sale(dataDay07.getTotal_sale() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataDay07.setClient_cnt(dataDay07.getClient_cnt()+ (branchTimeEntity.getCnt_07()!=null?branchTimeEntity.getCnt_07():0));
                dataMap.put(hour07,dataDay07);
            }
            Long hour08 = 8L;
            ItemData dataDay08 = dataMap.get(hour08);
            if(dataDay08!=null){
                dataDay08.setTotal_sale(dataDay08.getTotal_sale() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataDay08.setClient_cnt(dataDay08.getClient_cnt()+ (branchTimeEntity.getCnt_08()!=null?branchTimeEntity.getCnt_08():0));
            }else{
                dataDay08 = new ItemData();
                dataDay08.setTotal_sale(dataDay08.getTotal_sale() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataDay08.setClient_cnt(dataDay08.getClient_cnt()+ (branchTimeEntity.getCnt_08()!=null?branchTimeEntity.getCnt_08():0));
                dataMap.put(hour08,dataDay08);
            }
            Long hour09 = 9L;
            ItemData dataDay09 = dataMap.get(hour09);
            if(dataDay09!=null){
                dataDay09.setTotal_sale(dataDay09.getTotal_sale() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataDay09.setClient_cnt(dataDay09.getClient_cnt()+ (branchTimeEntity.getCnt_09()!=null?branchTimeEntity.getCnt_09():0));
            }else{
                dataDay09 = new ItemData();
                dataDay09.setTotal_sale(dataDay09.getTotal_sale() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataDay09.setClient_cnt(dataDay09.getClient_cnt()+ (branchTimeEntity.getCnt_09()!=null?branchTimeEntity.getCnt_09():0));
                dataMap.put(hour09,dataDay09);
            }
            Long hour10 = 10L;
            ItemData dataDay10 = dataMap.get(hour10);
            if(dataDay10!=null){
                dataDay10.setTotal_sale(dataDay10.getTotal_sale() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataDay10.setClient_cnt(dataDay10.getClient_cnt()+ (branchTimeEntity.getCnt_10()!=null?branchTimeEntity.getCnt_10():0));
            }else{
                dataDay10 = new ItemData();
                dataDay10.setTotal_sale(dataDay10.getTotal_sale() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataDay10.setClient_cnt(dataDay10.getClient_cnt()+ (branchTimeEntity.getCnt_10()!=null?branchTimeEntity.getCnt_10():0));
                dataMap.put(hour10,dataDay10);
            }
            Long hour11 = 11L;
            ItemData dataDay11 = dataMap.get(hour11);
            if(dataDay11!=null){
                dataDay11.setTotal_sale(dataDay11.getTotal_sale() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataDay11.setClient_cnt(dataDay11.getClient_cnt()+ (branchTimeEntity.getCnt_11()!=null?branchTimeEntity.getCnt_11():0));
            }else{
                dataDay11 = new ItemData();
                dataDay11.setTotal_sale(dataDay11.getTotal_sale() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataDay11.setClient_cnt(dataDay11.getClient_cnt()+ (branchTimeEntity.getCnt_11()!=null?branchTimeEntity.getCnt_11():0));
                dataMap.put(hour11,dataDay11);
            }
            Long hour12 = 12L;
            ItemData dataDay12 = dataMap.get(hour12);
            if(dataDay12!=null){
                dataDay12.setTotal_sale(dataDay12.getTotal_sale() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataDay12.setClient_cnt(dataDay12.getClient_cnt()+ (branchTimeEntity.getCnt_12()!=null?branchTimeEntity.getCnt_12():0));
            }else{
                dataDay12 = new ItemData();
                dataDay12.setTotal_sale(dataDay12.getTotal_sale() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataDay12.setClient_cnt(dataDay12.getClient_cnt()+ (branchTimeEntity.getCnt_12()!=null?branchTimeEntity.getCnt_12():0));
                dataMap.put(hour12,dataDay12);
            }
            Long hour13 = 13L;
            ItemData dataDay13 = dataMap.get(hour13);
            if(dataDay13!=null){
                dataDay13.setTotal_sale(dataDay13.getTotal_sale() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataDay13.setClient_cnt(dataDay13.getClient_cnt()+ (branchTimeEntity.getCnt_13()!=null?branchTimeEntity.getCnt_13():0));
            }else{
                dataDay13 = new ItemData();
                dataDay13.setTotal_sale(dataDay13.getTotal_sale() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataDay13.setClient_cnt(dataDay13.getClient_cnt()+ (branchTimeEntity.getCnt_13()!=null?branchTimeEntity.getCnt_13():0));
                dataMap.put(hour13,dataDay13);
            }
            Long hour14 = 14L;
            ItemData dataDay14 = dataMap.get(hour14);
            if(dataDay14!=null){
                dataDay14.setTotal_sale(dataDay14.getTotal_sale() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataDay14.setClient_cnt(dataDay14.getClient_cnt()+ (branchTimeEntity.getCnt_14()!=null?branchTimeEntity.getCnt_14():0));
            }else{
                dataDay14 = new ItemData();
                dataDay14.setTotal_sale(dataDay14.getTotal_sale() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataDay14.setClient_cnt(dataDay14.getClient_cnt()+ (branchTimeEntity.getCnt_14()!=null?branchTimeEntity.getCnt_14():0));
                dataMap.put(hour14,dataDay14);
            }
            Long hour15 = 15L;
            ItemData dataDay15 = dataMap.get(hour15);
            if(dataDay15!=null){
                dataDay15.setTotal_sale(dataDay15.getTotal_sale() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataDay15.setClient_cnt(dataDay15.getClient_cnt()+ (branchTimeEntity.getCnt_15()!=null?branchTimeEntity.getCnt_15():0));
            }else{
                dataDay15 = new ItemData();
                dataDay15.setTotal_sale(dataDay15.getTotal_sale() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataDay15.setClient_cnt(dataDay15.getClient_cnt()+ (branchTimeEntity.getCnt_15()!=null?branchTimeEntity.getCnt_15():0));
                dataMap.put(hour15,dataDay15);
            }
            Long hour16 = 16L;
            ItemData dataDay16 = dataMap.get(hour16);
            if(dataDay16!=null){
                dataDay16.setTotal_sale(dataDay16.getTotal_sale() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataDay16.setClient_cnt(dataDay16.getClient_cnt()+ (branchTimeEntity.getCnt_16()!=null?branchTimeEntity.getCnt_16():0));
            }else{
                dataDay16 = new ItemData();
                dataDay16.setTotal_sale(dataDay16.getTotal_sale() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataDay16.setClient_cnt(dataDay16.getClient_cnt()+ (branchTimeEntity.getCnt_16()!=null?branchTimeEntity.getCnt_16():0));
                dataMap.put(hour16,dataDay16);
            }
            Long hour17 = 17L;
            ItemData dataDay17 = dataMap.get(hour17);
            if(dataDay17!=null){
                dataDay17.setTotal_sale(dataDay17.getTotal_sale() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataDay17.setClient_cnt(dataDay17.getClient_cnt()+ (branchTimeEntity.getCnt_17()!=null?branchTimeEntity.getCnt_17():0));
            }else{
                dataDay17 = new ItemData();
                dataDay17.setTotal_sale(dataDay17.getTotal_sale() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataDay17.setClient_cnt(dataDay17.getClient_cnt()+ (branchTimeEntity.getCnt_17()!=null?branchTimeEntity.getCnt_17():0));
                dataMap.put(hour17,dataDay17);
            }
            Long hour18 =18L;
            ItemData dataDay18 = dataMap.get(hour18);
            if(dataDay18!=null){
                dataDay18.setTotal_sale(dataDay18.getTotal_sale() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataDay18.setClient_cnt(dataDay18.getClient_cnt()+ (branchTimeEntity.getCnt_18()!=null?branchTimeEntity.getCnt_18():0));
            }else{
                dataDay18 = new ItemData();
                dataDay18.setTotal_sale(dataDay18.getTotal_sale() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataDay18.setClient_cnt(dataDay18.getClient_cnt()+ (branchTimeEntity.getCnt_18()!=null?branchTimeEntity.getCnt_18():0));
                dataMap.put(hour18,dataDay18);
            }
            Long hour19 = 19L;
            ItemData dataDay19 = dataMap.get(hour19);
            if(dataDay19!=null){
                dataDay19.setTotal_sale(dataDay19.getTotal_sale() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataDay19.setClient_cnt(dataDay19.getClient_cnt()+ (branchTimeEntity.getCnt_19()!=null?branchTimeEntity.getCnt_19():0));
            }else{
                dataDay19 = new ItemData();
                dataDay19.setTotal_sale(dataDay19.getTotal_sale() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataDay19.setClient_cnt(dataDay19.getClient_cnt()+ (branchTimeEntity.getCnt_19()!=null?branchTimeEntity.getCnt_19():0));
                dataMap.put(hour19,dataDay19);
            }
            Long hour20 = 20L;
            ItemData dataDay20 = dataMap.get(hour20);
            if(dataDay20!=null){
                dataDay20.setTotal_sale(dataDay20.getTotal_sale() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataDay20.setClient_cnt(dataDay20.getClient_cnt()+ (branchTimeEntity.getCnt_20()!=null?branchTimeEntity.getCnt_20():0));
            }else{
                dataDay20 = new ItemData();
                dataDay20.setTotal_sale(dataDay20.getTotal_sale() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataDay20.setClient_cnt(dataDay20.getClient_cnt()+ (branchTimeEntity.getCnt_20()!=null?branchTimeEntity.getCnt_20():0));
                dataMap.put(hour20,dataDay20);
            }
            Long hour21 = 21L;
            ItemData dataDay21 = dataMap.get(hour21);
            if(dataDay21!=null){
                dataDay21.setTotal_sale(dataDay21.getTotal_sale() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataDay21.setClient_cnt(dataDay21.getClient_cnt()+ (branchTimeEntity.getCnt_21()!=null?branchTimeEntity.getCnt_21():0));
            }else{
                dataDay21 = new ItemData();
                dataDay21.setTotal_sale(dataDay21.getTotal_sale() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataDay21.setClient_cnt(dataDay21.getClient_cnt()+ (branchTimeEntity.getCnt_21()!=null?branchTimeEntity.getCnt_21():0));
                dataMap.put(hour21,dataDay21);
            }
            Long hour22 = 22L;
            ItemData dataDay22 = dataMap.get(hour22);
            if(dataDay22!=null){
                dataDay22.setTotal_sale(dataDay22.getTotal_sale() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataDay22.setClient_cnt(dataDay22.getClient_cnt()+ (branchTimeEntity.getCnt_22()!=null?branchTimeEntity.getCnt_22():0));
            }else{
                dataDay22 = new ItemData();
                dataDay22.setTotal_sale(dataDay22.getTotal_sale() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataDay22.setClient_cnt(dataDay22.getClient_cnt()+ (branchTimeEntity.getCnt_22()!=null?branchTimeEntity.getCnt_22():0));
                dataMap.put(hour22,dataDay22);
            }
            Long hour23 = 23L;
            ItemData dataDay23 = dataMap.get(hour23);
            if(dataDay23!=null){
                dataDay23.setTotal_sale(dataDay23.getTotal_sale() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataDay23.setClient_cnt(dataDay23.getClient_cnt()+ (branchTimeEntity.getCnt_23()!=null?branchTimeEntity.getCnt_23():0));
            }else{
                dataDay23 = new ItemData();
                dataDay23.setTotal_sale(dataDay23.getTotal_sale() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataDay23.setClient_cnt(dataDay23.getClient_cnt()+ (branchTimeEntity.getCnt_23()!=null?branchTimeEntity.getCnt_23():0));
                dataMap.put(hour23,dataDay23);
            }
        }
        List<BranchTimeEntity> branchTimeEntities_month = branchTimeDao.list("where org_id='"+orgId+"' and to_days(oper_date)<=(to_days('" +end+
                "')-30) and to_days(oper_date) >= (to_days('"+begin+"')-30)"+brand_noSql);
        for(BranchTimeEntity branchTimeEntity:branchTimeEntities_month){
            Long hour00 = 0L;
            ItemData dataDay00 = dataMap.get(hour00);
            if(dataDay00!=null){
                dataDay00.setTotal_sale_month(dataDay00.getTotal_sale_month() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataDay00.setClient_cnt_month(dataDay00.getClient_cnt_month()+ (branchTimeEntity.getCnt_00()!=null?branchTimeEntity.getCnt_00():0));
            }else{
                dataDay00 = new ItemData();
                dataDay00.setTotal_sale_month(dataDay00.getTotal_sale_month() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataDay00.setClient_cnt_month(dataDay00.getClient_cnt_month()+ (branchTimeEntity.getCnt_00()!=null?branchTimeEntity.getCnt_00():0));
                dataMap.put(hour00,dataDay00);
            }
            Long hour01 = 1L;
            ItemData dataDay01 = dataMap.get(hour01);
            if(dataDay01!=null){
                dataDay01.setTotal_sale_month(dataDay01.getTotal_sale_month() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataDay01.setClient_cnt_month(dataDay01.getClient_cnt_month()+ (branchTimeEntity.getCnt_01()!=null?branchTimeEntity.getCnt_01():0));
            }else{
                dataDay01 = new ItemData();
                dataDay01.setTotal_sale_month(dataDay01.getTotal_sale_month() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataDay01.setClient_cnt_month(dataDay01.getClient_cnt_month()+ (branchTimeEntity.getCnt_01()!=null?branchTimeEntity.getCnt_01():0));
                dataMap.put(hour01,dataDay01);
            }
            Long hour02 = 2L;
            ItemData dataDay02 = dataMap.get(hour02);
            if(dataDay02!=null){
                dataDay02.setTotal_sale_month(dataDay02.getTotal_sale_month() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataDay02.setClient_cnt_month(dataDay02.getClient_cnt_month()+ (branchTimeEntity.getCnt_02()!=null?branchTimeEntity.getCnt_02():0));
            }else{
                dataDay02 = new ItemData();
                dataDay02.setTotal_sale_month(dataDay02.getTotal_sale_month() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataDay02.setClient_cnt_month(dataDay02.getClient_cnt_month()+ (branchTimeEntity.getCnt_02()!=null?branchTimeEntity.getCnt_02():0));
                dataMap.put(hour02,dataDay02);
            }
            Long hour03 = 3L;
            ItemData dataDay03 = dataMap.get(hour03);
            if(dataDay03!=null){
                dataDay03.setTotal_sale_month(dataDay03.getTotal_sale_month() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataDay03.setClient_cnt_month(dataDay03.getClient_cnt_month()+ (branchTimeEntity.getCnt_03()!=null?branchTimeEntity.getCnt_03():0));
            }else{
                dataDay03 = new ItemData();
                dataDay03.setTotal_sale_month(dataDay03.getTotal_sale_month() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataDay03.setClient_cnt_month(dataDay03.getClient_cnt_month()+ (branchTimeEntity.getCnt_03()!=null?branchTimeEntity.getCnt_03():0));
                dataMap.put(hour03,dataDay03);
            }
            Long hour04 = 4L;
            ItemData dataDay04 = dataMap.get(hour04);
            if(dataDay04!=null){
                dataDay04.setTotal_sale_month(dataDay04.getTotal_sale_month() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataDay04.setClient_cnt_month(dataDay04.getClient_cnt_month()+ (branchTimeEntity.getCnt_04()!=null?branchTimeEntity.getCnt_04():0));
            }else{
                dataDay04 = new ItemData();
                dataDay04.setTotal_sale_month(dataDay04.getTotal_sale_month() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataDay04.setClient_cnt_month(dataDay04.getClient_cnt_month()+ (branchTimeEntity.getCnt_04()!=null?branchTimeEntity.getCnt_04():0));
                dataMap.put(hour04,dataDay04);
            }
            Long hour05 = 5L;
            ItemData dataDay05 = dataMap.get(hour05);
            if(dataDay05!=null){
                dataDay05.setTotal_sale_month(dataDay05.getTotal_sale_month() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataDay05.setClient_cnt_month(dataDay05.getClient_cnt_month()+ (branchTimeEntity.getCnt_05()!=null?branchTimeEntity.getCnt_05():0));
            }else{
                dataDay05 = new ItemData();
                dataDay05.setTotal_sale_month(dataDay05.getTotal_sale_month() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataDay05.setClient_cnt_month(dataDay05.getClient_cnt_month()+ (branchTimeEntity.getCnt_05()!=null?branchTimeEntity.getCnt_05():0));
                dataMap.put(hour05,dataDay05);
            }
            Long hour06 = 6L;
            ItemData dataDay06 = dataMap.get(hour06);
            if(dataDay06!=null){
                dataDay06.setTotal_sale_month(dataDay06.getTotal_sale_month() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataDay06.setClient_cnt_month(dataDay06.getClient_cnt_month()+ (branchTimeEntity.getCnt_06()!=null?branchTimeEntity.getCnt_06():0));
            }else{
                dataDay06 = new ItemData();
                dataDay06.setTotal_sale_month(dataDay06.getTotal_sale_month() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataDay06.setClient_cnt_month(dataDay06.getClient_cnt_month()+ (branchTimeEntity.getCnt_06()!=null?branchTimeEntity.getCnt_06():0));
                dataMap.put(hour06,dataDay06);
            }
            Long hour07 = 7L;
            ItemData dataDay07 = dataMap.get(hour07);
            if(dataDay07!=null){
                dataDay07.setTotal_sale_month(dataDay07.getTotal_sale_month() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataDay07.setClient_cnt_month(dataDay07.getClient_cnt_month()+ (branchTimeEntity.getCnt_07()!=null?branchTimeEntity.getCnt_07():0));
            }else{
                dataDay07 = new ItemData();
                dataDay07.setTotal_sale_month(dataDay07.getTotal_sale_month() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataDay07.setClient_cnt_month(dataDay07.getClient_cnt_month()+ (branchTimeEntity.getCnt_07()!=null?branchTimeEntity.getCnt_07():0));
                dataMap.put(hour07,dataDay07);
            }
            Long hour08 = 8L;
            ItemData dataDay08 = dataMap.get(hour08);
            if(dataDay08!=null){
                dataDay08.setTotal_sale_month(dataDay08.getTotal_sale_month() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataDay08.setClient_cnt_month(dataDay08.getClient_cnt_month()+ (branchTimeEntity.getCnt_08()!=null?branchTimeEntity.getCnt_08():0));
            }else{
                dataDay08 = new ItemData();
                dataDay08.setTotal_sale_month(dataDay08.getTotal_sale_month() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataDay08.setClient_cnt_month(dataDay08.getClient_cnt_month()+ (branchTimeEntity.getCnt_08()!=null?branchTimeEntity.getCnt_08():0));
                dataMap.put(hour08,dataDay08);
            }
            Long hour09 = 9L;
            ItemData dataDay09 = dataMap.get(hour09);
            if(dataDay09!=null){
                dataDay09.setTotal_sale_month(dataDay09.getTotal_sale_month() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataDay09.setClient_cnt_month(dataDay09.getClient_cnt_month()+ (branchTimeEntity.getCnt_09()!=null?branchTimeEntity.getCnt_09():0));
            }else{
                dataDay09 = new ItemData();
                dataDay09.setTotal_sale_month(dataDay09.getTotal_sale_month() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataDay09.setClient_cnt_month(dataDay09.getClient_cnt_month()+ (branchTimeEntity.getCnt_09()!=null?branchTimeEntity.getCnt_09():0));
                dataMap.put(hour09,dataDay09);
            }
            Long hour10 = 10L;
            ItemData dataDay10 = dataMap.get(hour10);
            if(dataDay10!=null){
                dataDay10.setTotal_sale_month(dataDay10.getTotal_sale_month() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataDay10.setClient_cnt_month(dataDay10.getClient_cnt_month()+ (branchTimeEntity.getCnt_10()!=null?branchTimeEntity.getCnt_10():0));
            }else{
                dataDay10 = new ItemData();
                dataDay10.setTotal_sale_month(dataDay10.getTotal_sale_month() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataDay10.setClient_cnt_month(dataDay10.getClient_cnt_month()+ (branchTimeEntity.getCnt_10()!=null?branchTimeEntity.getCnt_10():0));
                dataMap.put(hour10,dataDay10);
            }
            Long hour11 = 11L;
            ItemData dataDay11 = dataMap.get(hour11);
            if(dataDay11!=null){
                dataDay11.setTotal_sale_month(dataDay11.getTotal_sale_month() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataDay11.setClient_cnt_month(dataDay11.getClient_cnt_month()+ (branchTimeEntity.getCnt_11()!=null?branchTimeEntity.getCnt_11():0));
            }else{
                dataDay11 = new ItemData();
                dataDay11.setTotal_sale_month(dataDay11.getTotal_sale_month() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataDay11.setClient_cnt_month(dataDay11.getClient_cnt_month()+ (branchTimeEntity.getCnt_11()!=null?branchTimeEntity.getCnt_11():0));
                dataMap.put(hour11,dataDay11);
            }
            Long hour12 = 12L;
            ItemData dataDay12 = dataMap.get(hour12);
            if(dataDay12!=null){
                dataDay12.setTotal_sale_month(dataDay12.getTotal_sale_month() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataDay12.setClient_cnt_month(dataDay12.getClient_cnt_month()+ (branchTimeEntity.getCnt_12()!=null?branchTimeEntity.getCnt_12():0));
            }else{
                dataDay12 = new ItemData();
                dataDay12.setTotal_sale_month(dataDay12.getTotal_sale_month() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataDay12.setClient_cnt_month(dataDay12.getClient_cnt_month()+ (branchTimeEntity.getCnt_12()!=null?branchTimeEntity.getCnt_12():0));
                dataMap.put(hour12,dataDay12);
            }
            Long hour13 = 13L;
            ItemData dataDay13 = dataMap.get(hour13);
            if(dataDay13!=null){
                dataDay13.setTotal_sale_month(dataDay13.getTotal_sale_month() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataDay13.setClient_cnt_month(dataDay13.getClient_cnt_month()+ (branchTimeEntity.getCnt_13()!=null?branchTimeEntity.getCnt_13():0));
            }else{
                dataDay13 = new ItemData();
                dataDay13.setTotal_sale_month(dataDay13.getTotal_sale_month() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataDay13.setClient_cnt_month(dataDay13.getClient_cnt_month()+ (branchTimeEntity.getCnt_13()!=null?branchTimeEntity.getCnt_13():0));
                dataMap.put(hour13,dataDay13);
            }
            Long hour14 = 14L;
            ItemData dataDay14 = dataMap.get(hour14);
            if(dataDay14!=null){
                dataDay14.setTotal_sale_month(dataDay14.getTotal_sale_month() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataDay14.setClient_cnt_month(dataDay14.getClient_cnt_month()+ (branchTimeEntity.getCnt_14()!=null?branchTimeEntity.getCnt_14():0));
            }else{
                dataDay14 = new ItemData();
                dataDay14.setTotal_sale_month(dataDay14.getTotal_sale_month() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataDay14.setClient_cnt_month(dataDay14.getClient_cnt_month()+ (branchTimeEntity.getCnt_14()!=null?branchTimeEntity.getCnt_14():0));
                dataMap.put(hour14,dataDay14);
            }
            Long hour15 = 15L;
            ItemData dataDay15 = dataMap.get(hour15);
            if(dataDay15!=null){
                dataDay15.setTotal_sale_month(dataDay15.getTotal_sale_month() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataDay15.setClient_cnt_month(dataDay15.getClient_cnt_month()+ (branchTimeEntity.getCnt_15()!=null?branchTimeEntity.getCnt_15():0));
            }else{
                dataDay15 = new ItemData();
                dataDay15.setTotal_sale_month(dataDay15.getTotal_sale_month() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataDay15.setClient_cnt_month(dataDay15.getClient_cnt_month()+ (branchTimeEntity.getCnt_15()!=null?branchTimeEntity.getCnt_15():0));
                dataMap.put(hour15,dataDay15);
            }
            Long hour16 = 16L;
            ItemData dataDay16 = dataMap.get(hour16);
            if(dataDay16!=null){
                dataDay16.setTotal_sale_month(dataDay16.getTotal_sale_month() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataDay16.setClient_cnt_month(dataDay16.getClient_cnt_month()+ (branchTimeEntity.getCnt_16()!=null?branchTimeEntity.getCnt_16():0));
            }else{
                dataDay16 = new ItemData();
                dataDay16.setTotal_sale_month(dataDay16.getTotal_sale_month() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataDay16.setClient_cnt_month(dataDay16.getClient_cnt_month()+ (branchTimeEntity.getCnt_16()!=null?branchTimeEntity.getCnt_16():0));
                dataMap.put(hour16,dataDay16);
            }
            Long hour17 = 17L;
            ItemData dataDay17 = dataMap.get(hour17);
            if(dataDay17!=null){
                dataDay17.setTotal_sale_month(dataDay17.getTotal_sale_month() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataDay17.setClient_cnt_month(dataDay17.getClient_cnt_month()+ (branchTimeEntity.getCnt_17()!=null?branchTimeEntity.getCnt_17():0));
            }else{
                dataDay17 = new ItemData();
                dataDay17.setTotal_sale_month(dataDay17.getTotal_sale_month() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataDay17.setClient_cnt_month(dataDay17.getClient_cnt_month()+ (branchTimeEntity.getCnt_17()!=null?branchTimeEntity.getCnt_17():0));
                dataMap.put(hour17,dataDay17);
            }
            Long hour18 =18L;
            ItemData dataDay18 = dataMap.get(hour18);
            if(dataDay18!=null){
                dataDay18.setTotal_sale_month(dataDay18.getTotal_sale_month() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataDay18.setClient_cnt_month(dataDay18.getClient_cnt_month()+ (branchTimeEntity.getCnt_18()!=null?branchTimeEntity.getCnt_18():0));
            }else{
                dataDay18 = new ItemData();
                dataDay18.setTotal_sale_month(dataDay18.getTotal_sale_month() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataDay18.setClient_cnt_month(dataDay18.getClient_cnt_month()+ (branchTimeEntity.getCnt_18()!=null?branchTimeEntity.getCnt_18():0));
                dataMap.put(hour18,dataDay18);
            }
            Long hour19 = 19L;
            ItemData dataDay19 = dataMap.get(hour19);
            if(dataDay19!=null){
                dataDay19.setTotal_sale_month(dataDay19.getTotal_sale_month() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataDay19.setClient_cnt_month(dataDay19.getClient_cnt_month()+ (branchTimeEntity.getCnt_19()!=null?branchTimeEntity.getCnt_19():0));
            }else{
                dataDay19 = new ItemData();
                dataDay19.setTotal_sale_month(dataDay19.getTotal_sale_month() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataDay19.setClient_cnt_month(dataDay19.getClient_cnt_month()+ (branchTimeEntity.getCnt_19()!=null?branchTimeEntity.getCnt_19():0));
                dataMap.put(hour19,dataDay19);
            }
            Long hour20 = 20L;
            ItemData dataDay20 = dataMap.get(hour20);
            if(dataDay20!=null){
                dataDay20.setTotal_sale_month(dataDay20.getTotal_sale_month() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataDay20.setClient_cnt_month(dataDay20.getClient_cnt_month()+ (branchTimeEntity.getCnt_20()!=null?branchTimeEntity.getCnt_20():0));
            }else{
                dataDay20 = new ItemData();
                dataDay20.setTotal_sale_month(dataDay20.getTotal_sale_month() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataDay20.setClient_cnt_month(dataDay20.getClient_cnt_month()+ (branchTimeEntity.getCnt_20()!=null?branchTimeEntity.getCnt_20():0));
                dataMap.put(hour20,dataDay20);
            }
            Long hour21 = 21L;
            ItemData dataDay21 = dataMap.get(hour21);
            if(dataDay21!=null){
                dataDay21.setTotal_sale_month(dataDay21.getTotal_sale_month() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataDay21.setClient_cnt_month(dataDay21.getClient_cnt_month()+ (branchTimeEntity.getCnt_21()!=null?branchTimeEntity.getCnt_21():0));
            }else{
                dataDay21 = new ItemData();
                dataDay21.setTotal_sale_month(dataDay21.getTotal_sale_month() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataDay21.setClient_cnt_month(dataDay21.getClient_cnt_month()+ (branchTimeEntity.getCnt_21()!=null?branchTimeEntity.getCnt_21():0));
                dataMap.put(hour21,dataDay21);
            }
            Long hour22 = 22L;
            ItemData dataDay22 = dataMap.get(hour22);
            if(dataDay22!=null){
                dataDay22.setTotal_sale_month(dataDay22.getTotal_sale_month() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataDay22.setClient_cnt_month(dataDay22.getClient_cnt_month()+ (branchTimeEntity.getCnt_22()!=null?branchTimeEntity.getCnt_22():0));
            }else{
                dataDay22 = new ItemData();
                dataDay22.setTotal_sale_month(dataDay22.getTotal_sale_month() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataDay22.setClient_cnt_month(dataDay22.getClient_cnt_month()+ (branchTimeEntity.getCnt_22()!=null?branchTimeEntity.getCnt_22():0));
                dataMap.put(hour22,dataDay22);
            }
            Long hour23 = 23L;
            ItemData dataDay23 = dataMap.get(hour23);
            if(dataDay23!=null){
                dataDay23.setTotal_sale_month(dataDay23.getTotal_sale_month() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataDay23.setClient_cnt_month(dataDay23.getClient_cnt_month()+ (branchTimeEntity.getCnt_23()!=null?branchTimeEntity.getCnt_23():0));
            }else{
                dataDay23 = new ItemData();
                dataDay23.setTotal_sale_month(dataDay23.getTotal_sale_month() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataDay23.setClient_cnt_month(dataDay23.getClient_cnt_month()+ (branchTimeEntity.getCnt_23()!=null?branchTimeEntity.getCnt_23():0));
                dataMap.put(hour23,dataDay23);
            }
        }
        List<BranchTimeEntity> branchTimeEntities_year = branchTimeDao.list("where org_id='"+orgId+"' and to_days(oper_date)<=(to_days('" +end+
                "')-30) and to_days(oper_date) >= (to_days('"+begin+"')-30)"+brand_noSql);
        for(BranchTimeEntity branchTimeEntity:branchTimeEntities_year){
            Long hour00 = 0L;
            ItemData dataDay00 = dataMap.get(hour00);
            if(dataDay00!=null){
                dataDay00.setClient_cnt_year(dataDay00.getClient_cnt_year() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataDay00.setClient_cnt_year(dataDay00.getClient_cnt_year()+ (branchTimeEntity.getCnt_00()!=null?branchTimeEntity.getCnt_00():0));
            }else{
                dataDay00 = new ItemData();
                dataDay00.setClient_cnt_year(dataDay00.getClient_cnt_year() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataDay00.setClient_cnt_year(dataDay00.getClient_cnt_year()+ (branchTimeEntity.getCnt_00()!=null?branchTimeEntity.getCnt_00():0));
                dataMap.put(hour00,dataDay00);
            }
            Long hour01 = 1L;
            ItemData dataDay01 = dataMap.get(hour01);
            if(dataDay01!=null){
                dataDay01.setClient_cnt_year(dataDay01.getClient_cnt_year() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataDay01.setClient_cnt_year(dataDay01.getClient_cnt_year()+ (branchTimeEntity.getCnt_01()!=null?branchTimeEntity.getCnt_01():0));
            }else{
                dataDay01 = new ItemData();
                dataDay01.setClient_cnt_year(dataDay01.getClient_cnt_year() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataDay01.setClient_cnt_year(dataDay01.getClient_cnt_year()+ (branchTimeEntity.getCnt_01()!=null?branchTimeEntity.getCnt_01():0));
                dataMap.put(hour01,dataDay01);
            }
            Long hour02 = 2L;
            ItemData dataDay02 = dataMap.get(hour02);
            if(dataDay02!=null){
                dataDay02.setClient_cnt_year(dataDay02.getClient_cnt_year() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataDay02.setClient_cnt_year(dataDay02.getClient_cnt_year()+ (branchTimeEntity.getCnt_02()!=null?branchTimeEntity.getCnt_02():0));
            }else{
                dataDay02 = new ItemData();
                dataDay02.setClient_cnt_year(dataDay02.getClient_cnt_year() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataDay02.setClient_cnt_year(dataDay02.getClient_cnt_year()+ (branchTimeEntity.getCnt_02()!=null?branchTimeEntity.getCnt_02():0));
                dataMap.put(hour02,dataDay02);
            }
            Long hour03 = 3L;
            ItemData dataDay03 = dataMap.get(hour03);
            if(dataDay03!=null){
                dataDay03.setClient_cnt_year(dataDay03.getClient_cnt_year() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataDay03.setClient_cnt_year(dataDay03.getClient_cnt_year()+ (branchTimeEntity.getCnt_03()!=null?branchTimeEntity.getCnt_03():0));
            }else{
                dataDay03 = new ItemData();
                dataDay03.setClient_cnt_year(dataDay03.getClient_cnt_year() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataDay03.setClient_cnt_year(dataDay03.getClient_cnt_year()+ (branchTimeEntity.getCnt_03()!=null?branchTimeEntity.getCnt_03():0));
                dataMap.put(hour03,dataDay03);
            }
            Long hour04 = 4L;
            ItemData dataDay04 = dataMap.get(hour04);
            if(dataDay04!=null){
                dataDay04.setClient_cnt_year(dataDay04.getClient_cnt_year() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataDay04.setClient_cnt_year(dataDay04.getClient_cnt_year()+ (branchTimeEntity.getCnt_04()!=null?branchTimeEntity.getCnt_04():0));
            }else{
                dataDay04 = new ItemData();
                dataDay04.setClient_cnt_year(dataDay04.getClient_cnt_year() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataDay04.setClient_cnt_year(dataDay04.getClient_cnt_year()+ (branchTimeEntity.getCnt_04()!=null?branchTimeEntity.getCnt_04():0));
                dataMap.put(hour04,dataDay04);
            }
            Long hour05 = 5L;
            ItemData dataDay05 = dataMap.get(hour05);
            if(dataDay05!=null){
                dataDay05.setClient_cnt_year(dataDay05.getClient_cnt_year() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataDay05.setClient_cnt_year(dataDay05.getClient_cnt_year()+ (branchTimeEntity.getCnt_05()!=null?branchTimeEntity.getCnt_05():0));
            }else{
                dataDay05 = new ItemData();
                dataDay05.setClient_cnt_year(dataDay05.getClient_cnt_year() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataDay05.setClient_cnt_year(dataDay05.getClient_cnt_year()+ (branchTimeEntity.getCnt_05()!=null?branchTimeEntity.getCnt_05():0));
                dataMap.put(hour05,dataDay05);
            }
            Long hour06 = 6L;
            ItemData dataDay06 = dataMap.get(hour06);
            if(dataDay06!=null){
                dataDay06.setClient_cnt_year(dataDay06.getClient_cnt_year() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataDay06.setClient_cnt_year(dataDay06.getClient_cnt_year()+ (branchTimeEntity.getCnt_06()!=null?branchTimeEntity.getCnt_06():0));
            }else{
                dataDay06 = new ItemData();
                dataDay06.setClient_cnt_year(dataDay06.getClient_cnt_year() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataDay06.setClient_cnt_year(dataDay06.getClient_cnt_year()+ (branchTimeEntity.getCnt_06()!=null?branchTimeEntity.getCnt_06():0));
                dataMap.put(hour06,dataDay06);
            }
            Long hour07 = 7L;
            ItemData dataDay07 = dataMap.get(hour07);
            if(dataDay07!=null){
                dataDay07.setClient_cnt_year(dataDay07.getClient_cnt_year() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataDay07.setClient_cnt_year(dataDay07.getClient_cnt_year()+ (branchTimeEntity.getCnt_07()!=null?branchTimeEntity.getCnt_07():0));
            }else{
                dataDay07 = new ItemData();
                dataDay07.setClient_cnt_year(dataDay07.getClient_cnt_year() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataDay07.setClient_cnt_year(dataDay07.getClient_cnt_year()+ (branchTimeEntity.getCnt_07()!=null?branchTimeEntity.getCnt_07():0));
                dataMap.put(hour07,dataDay07);
            }
            Long hour08 = 8L;
            ItemData dataDay08 = dataMap.get(hour08);
            if(dataDay08!=null){
                dataDay08.setClient_cnt_year(dataDay08.getClient_cnt_year() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataDay08.setClient_cnt_year(dataDay08.getClient_cnt_year()+ (branchTimeEntity.getCnt_08()!=null?branchTimeEntity.getCnt_08():0));
            }else{
                dataDay08 = new ItemData();
                dataDay08.setClient_cnt_year(dataDay08.getClient_cnt_year() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataDay08.setClient_cnt_year(dataDay08.getClient_cnt_year()+ (branchTimeEntity.getCnt_08()!=null?branchTimeEntity.getCnt_08():0));
                dataMap.put(hour08,dataDay08);
            }
            Long hour09 = 9L;
            ItemData dataDay09 = dataMap.get(hour09);
            if(dataDay09!=null){
                dataDay09.setClient_cnt_year(dataDay09.getClient_cnt_year() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataDay09.setClient_cnt_year(dataDay09.getClient_cnt_year()+ (branchTimeEntity.getCnt_09()!=null?branchTimeEntity.getCnt_09():0));
            }else{
                dataDay09 = new ItemData();
                dataDay09.setClient_cnt_year(dataDay09.getClient_cnt_year() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataDay09.setClient_cnt_year(dataDay09.getClient_cnt_year()+ (branchTimeEntity.getCnt_09()!=null?branchTimeEntity.getCnt_09():0));
                dataMap.put(hour09,dataDay09);
            }
            Long hour10 = 10L;
            ItemData dataDay10 = dataMap.get(hour10);
            if(dataDay10!=null){
                dataDay10.setClient_cnt_year(dataDay10.getClient_cnt_year() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataDay10.setClient_cnt_year(dataDay10.getClient_cnt_year()+ (branchTimeEntity.getCnt_10()!=null?branchTimeEntity.getCnt_10():0));
            }else{
                dataDay10 = new ItemData();
                dataDay10.setClient_cnt_year(dataDay10.getClient_cnt_year() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataDay10.setClient_cnt_year(dataDay10.getClient_cnt_year()+ (branchTimeEntity.getCnt_10()!=null?branchTimeEntity.getCnt_10():0));
                dataMap.put(hour10,dataDay10);
            }
            Long hour11 = 11L;
            ItemData dataDay11 = dataMap.get(hour11);
            if(dataDay11!=null){
                dataDay11.setClient_cnt_year(dataDay11.getClient_cnt_year() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataDay11.setClient_cnt_year(dataDay11.getClient_cnt_year()+ (branchTimeEntity.getCnt_11()!=null?branchTimeEntity.getCnt_11():0));
            }else{
                dataDay11 = new ItemData();
                dataDay11.setClient_cnt_year(dataDay11.getClient_cnt_year() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataDay11.setClient_cnt_year(dataDay11.getClient_cnt_year()+ (branchTimeEntity.getCnt_11()!=null?branchTimeEntity.getCnt_11():0));
                dataMap.put(hour11,dataDay11);
            }
            Long hour12 = 12L;
            ItemData dataDay12 = dataMap.get(hour12);
            if(dataDay12!=null){
                dataDay12.setClient_cnt_year(dataDay12.getClient_cnt_year() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataDay12.setClient_cnt_year(dataDay12.getClient_cnt_year()+ (branchTimeEntity.getCnt_12()!=null?branchTimeEntity.getCnt_12():0));
            }else{
                dataDay12 = new ItemData();
                dataDay12.setClient_cnt_year(dataDay12.getClient_cnt_year() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataDay12.setClient_cnt_year(dataDay12.getClient_cnt_year()+ (branchTimeEntity.getCnt_12()!=null?branchTimeEntity.getCnt_12():0));
                dataMap.put(hour12,dataDay12);
            }
            Long hour13 = 13L;
            ItemData dataDay13 = dataMap.get(hour13);
            if(dataDay13!=null){
                dataDay13.setClient_cnt_year(dataDay13.getClient_cnt_year() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataDay13.setClient_cnt_year(dataDay13.getClient_cnt_year()+ (branchTimeEntity.getCnt_13()!=null?branchTimeEntity.getCnt_13():0));
            }else{
                dataDay13 = new ItemData();
                dataDay13.setClient_cnt_year(dataDay13.getClient_cnt_year() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataDay13.setClient_cnt_year(dataDay13.getClient_cnt_year()+ (branchTimeEntity.getCnt_13()!=null?branchTimeEntity.getCnt_13():0));
                dataMap.put(hour13,dataDay13);
            }
            Long hour14 = 14L;
            ItemData dataDay14 = dataMap.get(hour14);
            if(dataDay14!=null){
                dataDay14.setClient_cnt_year(dataDay14.getClient_cnt_year() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataDay14.setClient_cnt_year(dataDay14.getClient_cnt_year()+ (branchTimeEntity.getCnt_14()!=null?branchTimeEntity.getCnt_14():0));
            }else{
                dataDay14 = new ItemData();
                dataDay14.setClient_cnt_year(dataDay14.getClient_cnt_year() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataDay14.setClient_cnt_year(dataDay14.getClient_cnt_year()+ (branchTimeEntity.getCnt_14()!=null?branchTimeEntity.getCnt_14():0));
                dataMap.put(hour14,dataDay14);
            }
            Long hour15 = 15L;
            ItemData dataDay15 = dataMap.get(hour15);
            if(dataDay15!=null){
                dataDay15.setClient_cnt_year(dataDay15.getClient_cnt_year() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataDay15.setClient_cnt_year(dataDay15.getClient_cnt_year()+ (branchTimeEntity.getCnt_15()!=null?branchTimeEntity.getCnt_15():0));
            }else{
                dataDay15 = new ItemData();
                dataDay15.setClient_cnt_year(dataDay15.getClient_cnt_year() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataDay15.setClient_cnt_year(dataDay15.getClient_cnt_year()+ (branchTimeEntity.getCnt_15()!=null?branchTimeEntity.getCnt_15():0));
                dataMap.put(hour15,dataDay15);
            }
            Long hour16 = 16L;
            ItemData dataDay16 = dataMap.get(hour16);
            if(dataDay16!=null){
                dataDay16.setClient_cnt_year(dataDay16.getClient_cnt_year() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataDay16.setClient_cnt_year(dataDay16.getClient_cnt_year()+ (branchTimeEntity.getCnt_16()!=null?branchTimeEntity.getCnt_16():0));
            }else{
                dataDay16 = new ItemData();
                dataDay16.setClient_cnt_year(dataDay16.getClient_cnt_year() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataDay16.setClient_cnt_year(dataDay16.getClient_cnt_year()+ (branchTimeEntity.getCnt_16()!=null?branchTimeEntity.getCnt_16():0));
                dataMap.put(hour16,dataDay16);
            }
            Long hour17 = 17L;
            ItemData dataDay17 = dataMap.get(hour17);
            if(dataDay17!=null){
                dataDay17.setClient_cnt_year(dataDay17.getClient_cnt_year() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataDay17.setClient_cnt_year(dataDay17.getClient_cnt_year()+ (branchTimeEntity.getCnt_17()!=null?branchTimeEntity.getCnt_17():0));
            }else{
                dataDay17 = new ItemData();
                dataDay17.setClient_cnt_year(dataDay17.getClient_cnt_year() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataDay17.setClient_cnt_year(dataDay17.getClient_cnt_year()+ (branchTimeEntity.getCnt_17()!=null?branchTimeEntity.getCnt_17():0));
                dataMap.put(hour17,dataDay17);
            }
            Long hour18 =18L;
            ItemData dataDay18 = dataMap.get(hour18);
            if(dataDay18!=null){
                dataDay18.setClient_cnt_year(dataDay18.getClient_cnt_year() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataDay18.setClient_cnt_year(dataDay18.getClient_cnt_year()+ (branchTimeEntity.getCnt_18()!=null?branchTimeEntity.getCnt_18():0));
            }else{
                dataDay18 = new ItemData();
                dataDay18.setClient_cnt_year(dataDay18.getClient_cnt_year() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataDay18.setClient_cnt_year(dataDay18.getClient_cnt_year()+ (branchTimeEntity.getCnt_18()!=null?branchTimeEntity.getCnt_18():0));
                dataMap.put(hour18,dataDay18);
            }
            Long hour19 = 19L;
            ItemData dataDay19 = dataMap.get(hour19);
            if(dataDay19!=null){
                dataDay19.setClient_cnt_year(dataDay19.getClient_cnt_year() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataDay19.setClient_cnt_year(dataDay19.getClient_cnt_year()+ (branchTimeEntity.getCnt_19()!=null?branchTimeEntity.getCnt_19():0));
            }else{
                dataDay19 = new ItemData();
                dataDay19.setClient_cnt_year(dataDay19.getClient_cnt_year() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataDay19.setClient_cnt_year(dataDay19.getClient_cnt_year()+ (branchTimeEntity.getCnt_19()!=null?branchTimeEntity.getCnt_19():0));
                dataMap.put(hour19,dataDay19);
            }
            Long hour20 = 20L;
            ItemData dataDay20 = dataMap.get(hour20);
            if(dataDay20!=null){
                dataDay20.setClient_cnt_year(dataDay20.getClient_cnt_year() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataDay20.setClient_cnt_year(dataDay20.getClient_cnt_year()+ (branchTimeEntity.getCnt_20()!=null?branchTimeEntity.getCnt_20():0));
            }else{
                dataDay20 = new ItemData();
                dataDay20.setClient_cnt_year(dataDay20.getClient_cnt_year() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataDay20.setClient_cnt_year(dataDay20.getClient_cnt_year()+ (branchTimeEntity.getCnt_20()!=null?branchTimeEntity.getCnt_20():0));
                dataMap.put(hour20,dataDay20);
            }
            Long hour21 = 21L;
            ItemData dataDay21 = dataMap.get(hour21);
            if(dataDay21!=null){
                dataDay21.setClient_cnt_year(dataDay21.getClient_cnt_year() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataDay21.setClient_cnt_year(dataDay21.getClient_cnt_year()+ (branchTimeEntity.getCnt_21()!=null?branchTimeEntity.getCnt_21():0));
            }else{
                dataDay21 = new ItemData();
                dataDay21.setClient_cnt_year(dataDay21.getClient_cnt_year() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataDay21.setClient_cnt_year(dataDay21.getClient_cnt_year()+ (branchTimeEntity.getCnt_21()!=null?branchTimeEntity.getCnt_21():0));
                dataMap.put(hour21,dataDay21);
            }
            Long hour22 = 22L;
            ItemData dataDay22 = dataMap.get(hour22);
            if(dataDay22!=null){
                dataDay22.setClient_cnt_year(dataDay22.getClient_cnt_year() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataDay22.setClient_cnt_year(dataDay22.getClient_cnt_year()+ (branchTimeEntity.getCnt_22()!=null?branchTimeEntity.getCnt_22():0));
            }else{
                dataDay22 = new ItemData();
                dataDay22.setClient_cnt_year(dataDay22.getClient_cnt_year() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataDay22.setClient_cnt_year(dataDay22.getClient_cnt_year()+ (branchTimeEntity.getCnt_22()!=null?branchTimeEntity.getCnt_22():0));
                dataMap.put(hour22,dataDay22);
            }
            Long hour23 = 23L;
            ItemData dataDay23 = dataMap.get(hour23);
            if(dataDay23!=null){
                dataDay23.setClient_cnt_year(dataDay23.getClient_cnt_year() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataDay23.setClient_cnt_year(dataDay23.getClient_cnt_year()+ (branchTimeEntity.getCnt_23()!=null?branchTimeEntity.getCnt_23():0));
            }else{
                dataDay23 = new ItemData();
                dataDay23.setClient_cnt_year(dataDay23.getClient_cnt_year() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataDay23.setClient_cnt_year(dataDay23.getClient_cnt_year()+ (branchTimeEntity.getCnt_23()!=null?branchTimeEntity.getCnt_23():0));
                dataMap.put(hour23,dataDay23);
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<Long, ItemData> entry : dataMap.entrySet()) {
            Long key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("时间",key);
            resultJson.put("本期客均",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getClient_cnt()));
            resultJson.put("本期客单",NumberUtil.getDouble2Format(itemData.getClient_cnt()));

            resultJson.put("环比客均",NumberUtil.getDouble2Format(itemData.getTotal_sale_month()/itemData.getClient_cnt_month()));
            resultJson.put("环比客单",NumberUtil.getDouble2Format(itemData.getClient_cnt_month()));

            resultJson.put("同比客均",NumberUtil.getDouble2Format(itemData.getTotal_sale_year()/itemData.getClient_cnt_year()));
            resultJson.put("同比客单",NumberUtil.getDouble2Format(itemData.getClient_cnt_year()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray keliu_kedan(String orgId, String begin, String end, String branchNos) throws BaseException {
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        String brand_noSql = "";
        if(StringUtils.isNotEmpty(branch_nos_str)){
            brand_noSql = " and branch_no in ("+branch_nos_str+")";
        }
        Long total_bill_cnt = 0L;
        Double total_sale = 0d;
        List<Object> all_sales = unionSqlDao.query("select sum(t1.bill_cnt) as bill_cnt,sum(t1.sale_amt) as sale_amt from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+brand_noSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Long bill_cnt = (Long)itemEntityObj[0];
            total_bill_cnt += bill_cnt!=null?bill_cnt:0;
            Double sale_amt = (Double)itemEntityObj[1];
            total_sale += sale_amt!=null?sale_amt:0;
        }
        Long total_bill_cnt_month = 0L;
        List<Object> all_sales_month = unionSqlDao.query("select sum(t1.bill_cnt) as bill_cnt from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+brand_noSql
                +" and to_days(t1.oper_date)>=(to_days('"+begin+"')-30) and to_days(t1.oper_date)<=(to_days('"+end+"')-30)");
        for(Object itemEntity:all_sales_month){
            Long bill_cnt = (Long)itemEntity;
            total_bill_cnt_month += bill_cnt!=null?bill_cnt:0;
        }
        Long total_bill_cnt_year = 0L;
        List<Object> all_sales_year = unionSqlDao.query("select sum(t1.bill_cnt) as bill_cnt from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+brand_noSql
                +" and to_days(t1.oper_date)>=(to_days('"+begin+"')-365) and to_days(t1.oper_date)<=(to_days('"+end+"')-365)");
        for(Object itemEntity:all_sales_year){
            Long bill_cnt = (Long)itemEntity;
            total_bill_cnt_year += bill_cnt!=null?bill_cnt:0;
        }
        JSONArray results = new JSONArray();
        JSONObject resultJson = new JSONObject();
        String huanbiStr = (total_bill_cnt_month-total_bill_cnt)>0?"上涨"+(total_bill_cnt_month-total_bill_cnt):"下降"+(total_bill_cnt-total_bill_cnt_month);
        String tongbiStr = (total_bill_cnt_year-total_bill_cnt)>0?"上涨"+(total_bill_cnt_year-total_bill_cnt):"下降"+(total_bill_cnt-total_bill_cnt_year);
        resultJson.put("name","本期客流【"+total_bill_cnt+"人】,客单价【"+NumberUtil.getDouble2Format(total_sale/total_bill_cnt)+"元/人】,客流环比【"+
                huanbiStr+"人】,同比【"+
                tongbiStr+"人】");
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray huangshi(String orgId, String begin, String end, String branchNos) throws BaseException {
        class ItemData{
            double total_sale;
            double total_cost;

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }
        }
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        String brand_noSql = "";
        if(StringUtils.isNotEmpty(branch_nos_str)){
            brand_noSql = " and branch_no in ("+branch_nos_str+")";
        }
        Map<String,ItemData> dataMap = Maps.newLinkedHashMap();
//        List<BranchDayEntity>  branchDayEntities = branchDayDao.list("where oper_date <= '"+end+"'" +" and oper_date >= '"+begin+"'" +
//                " and org_id = '"+orgId+"'"+brand_noSql);
        List<Object> objects = unionSqlDao.query("select oper_date, sum(sale_amt),sum(cost_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date <= '"+end+"' and oper_date >= '"+begin+"'"+
                brand_noSql+" group by oper_date order by oper_date");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String oper_date = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
            Double total_sale = ((Double)itemEntityObj[1]);
            Double total_cost = (Double)itemEntityObj[2];
            ItemData itemData = dataMap.get(oper_date);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataMap.put(oper_date,itemData);
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("日期",key);
            resultJson.put("成交金额",NumberUtil.getDouble2Format(itemData.getTotal_sale()));
            resultJson.put("利润",NumberUtil.getDouble2Format(itemData.getTotal_sale()-itemData.getTotal_cost()));
            resultJson.put("黄氏金额",0);
            resultJson.put("黄氏利润",0);
            resultJson.put("标记","促销前");
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray baofadu(String orgId, String begin, String end, String branchNos) throws BaseException {
        JSONArray results = new JSONArray();
        JSONObject resultJson = new JSONObject();
        resultJson.put("name","本次促销爆发度为【0%】,衰减度为【0%】");
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray zhekou(String orgId, String begin, String end, String branchNos) throws BaseException {
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        String brand_noSql = "";
        if(StringUtils.isNotEmpty(branch_nos_str)){
            brand_noSql = " and t1.branch_no in ("+branch_nos_str+")";
        }
        double all_sale =0;
        double all_dis =0;
        List<Object> all_sales = unionSqlDao.query("select sum(t1.sale_amt) as sale_amt,sum(t1.dis_amt) as cost_amt from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+brand_noSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[0];
            all_sale += total_sale!=null?total_sale:0;
            Double total_dis = (Double)itemEntityObj[1];
            all_dis += total_dis!=null?total_dis:0;
        }

        JSONArray results = new JSONArray();
        JSONObject resultJson = new JSONObject();
        resultJson.put("折扣",NumberUtil.getDouble2Format(all_dis/all_sale*100));
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray item_rank(String orgId, String begin, String end, String branchNos) throws BaseException {
        class ItemData{
            String item_name;
            double total_sale;

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }
        }
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        Map<String,ItemData> itemDataMap = Maps.newLinkedHashMap();
        String brand_noSql = "";
        if(StringUtils.isNotEmpty(branch_nos_str)){
            brand_noSql = " and t1.branch_no in ("+branch_nos_str+")";
        }
        List<Object> all_sales = unionSqlDao.query("select t1.item_no as item_no, sum(t1.sale_amt) as sale_amt_order from ItemDayEntity t1 where t1.org_id = '"+orgId+"'"+brand_noSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'"+ " group by t1.item_no order by sale_amt_order desc",1,10);
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_no = (String)itemEntityObj[0];
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = itemDataMap.get(item_no);
            if (itemData!=null) {
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(total_sale!=null?total_sale:0);
                itemDataMap.put(item_no,itemData);
            }
        }
        String item_nosStr = "";
        Set<String> keys = itemDataMap.keySet() ;// 得到全部的key,branch_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_nosStr)){
                item_nosStr = item_nosStr + ",'"+str+"'";
            }else{
                item_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_nosStr)){
            List<ItemEntity> itemEntities = itemDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'");
            for(ItemEntity itemEntity : itemEntities){
                if( itemDataMap.get(itemEntity.getItem_no())!=null){
                    itemDataMap.get(itemEntity.getItem_no()).setItem_name(itemEntity.getItem_name());
                }
            }

        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : itemDataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("条码",key);
            resultJson.put("名称",itemData.getItem_name());
            resultJson.put("销售额",NumberUtil.getDouble2Format(itemData.getTotal_sale()));
            results.add(resultJson);
        }
        return results;
    }
}
