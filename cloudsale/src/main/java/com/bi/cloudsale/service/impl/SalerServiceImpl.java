package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.CompanyDayMonthService;
import com.bi.cloudsale.service.SalerService;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
@Transactional
public class SalerServiceImpl implements SalerService {

    private static final Logger log = LoggerFactory.getLogger(SalerServiceImpl.class);

    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private ItemDayDao itemDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private VipDayDao vipDayDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private SalerDayDao salerDayDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONObject salerInfo(String orgId, String begin, String end, String branchNo) throws BaseException {
        Integer total = 0;
        Integer newJoin =0;
        Integer left = 0;
        Integer hasGains = 0;
        Double total_sale = 0d;

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";

        List<SalerEntity> salerEntities = salerDao.list("where org_id = '"+orgId+"'"+branchSql);
        if(CollectionUtils.isNotEmpty(salerEntities)){
            total = salerEntities.size();
        }
        salerEntities = salerDao.list("where org_id = '"+orgId+"'"+branchSql+" and gmt_create >='"+begin+"' and gmt_create <='"+end+"'");
        if(CollectionUtils.isNotEmpty(salerEntities)){
            newJoin = salerEntities.size();
        }
//        List<SalerDayEntity> salerDayEntities= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')" + branchSql);
        List<Object> objects = unionSqlDao.query("select sum(sale_amt) from SalerDayEntity where org_id ='"+orgId+"' and  to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql);
        for(Object object : objects){
            Double sale_amt = (Double)object;
            total_sale += sale_amt!=null?sale_amt:0;
        }
        List<Object> hasGainCnt_obj = unionSqlDao.query("SELECT id,sale_id FROM SalerDayEntity where org_id ='"+orgId+"' and  to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql);
        if(CollectionUtils.isNotEmpty(hasGainCnt_obj)){
            hasGains = hasGainCnt_obj.size();
        }
        JSONObject resultJson = new JSONObject();
        resultJson.put("目前在职",total);
        resultJson.put("本期入职人数",newJoin);
        resultJson.put("本期离职人数",left);
        resultJson.put("有销售人数",hasGains);
        resultJson.put("有销售人均贡献",NumberUtil.getDouble2Format(total_sale/total));
        return resultJson;
    }

    @Override
    public JSONArray salerRank(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String name;
            double total_sale;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> itemDataMap = Maps.newLinkedHashMap();
//        List<SalerDayEntity> salerDayEntities= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')" + branchSql);
        List<Object> objects = unionSqlDao.query("select sale_id, sum(sale_amt) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql+" group by sale_id order by sum(sale_amt) desc");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            Double sale_amt = (Double)itemEntityObj[1];
            ItemData itemData = itemDataMap.get(sale_id);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(sale_amt!=null?sale_amt:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(sale_amt!=null?sale_amt:0));
                itemDataMap.put(sale_id,itemData);
            }
        }
        String saler_idsStr = "";
        Set<String> keys = itemDataMap.keySet() ;// 得到全部的key,saler_id
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(saler_idsStr)){
                saler_idsStr = saler_idsStr + ",'"+str+"'";
            }else{
                saler_idsStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(saler_idsStr)){
            //2员工名
            List<SalerEntity> salerEntities = salerDao.list("where sale_id in("+saler_idsStr+") and org_id = '"+orgId+"'");
            for(SalerEntity salerEntity : salerEntities){
                itemDataMap.get(salerEntity.getSale_id()).setName(salerEntity.getSale_name());
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : itemDataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",itemData.getName());
            resultJson.put("value",itemData.getTotal_sale());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray salerDetail(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String name;
            double total_sale;
            double total_sale_cnt;
            double total_bill_cnt;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_bill_cnt() {
                return total_bill_cnt;
            }

            public void setTotal_bill_cnt(double total_bill_cnt) {
                this.total_bill_cnt = total_bill_cnt;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> itemDataMap = new HashMap<>();
//        List<SalerDayEntity> salerDayEntities= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')" + branchSql);
        List<Object> objects = unionSqlDao.query("select sale_id, sum(sale_amt),sum(bill_cnt),sum(sale_qty) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql+" group by sale_id");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            Double sale_amt = (Double)itemEntityObj[1];
            Long bill_cnt = (Long)itemEntityObj[2];
            Double sale_qty = (Double)itemEntityObj[3];

            ItemData itemData = itemDataMap.get(sale_id);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(sale_amt!=null?sale_amt:0));
                itemData.setTotal_bill_cnt(itemData.getTotal_bill_cnt()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTotal_sale_cnt(itemData.getTotal_sale_cnt()+(sale_qty!=null?sale_qty:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(sale_amt!=null?sale_amt:0));
                itemData.setTotal_bill_cnt(itemData.getTotal_bill_cnt()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTotal_sale_cnt(itemData.getTotal_sale_cnt()+(sale_qty!=null?sale_qty:0));
                itemDataMap.put(sale_id,itemData);
            }
        }
        String saler_idsStr = "";
        Set<String> keys = itemDataMap.keySet() ;// 得到全部的key,saler_id
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(saler_idsStr)){
                saler_idsStr = saler_idsStr + ",'"+str+"'";
            }else{
                saler_idsStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(saler_idsStr)){
            //2员工名
            List<SalerEntity> salerEntities = salerDao.list("where sale_id in("+saler_idsStr+") and org_id = '"+orgId+"'");
            for(SalerEntity salerEntity : salerEntities){
                itemDataMap.get(salerEntity.getSale_id()).setName(salerEntity.getSale_name());
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : itemDataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("value",itemData.getTotal_sale_cnt()+","
                    +NumberUtil.getDouble2Format(itemData.getTotal_sale())+","
                    +NumberUtil.getDouble2Format(itemData.getTotal_sale_cnt()/itemData.getTotal_bill_cnt())+","
                    +itemData.getName());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray salerRadio(String orgId, String begin, String end, String branchNo) throws BaseException {

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
//        List<SalerDayEntity> salerDayEntities= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')" + branchSql);

        Double total_sale = 0d;
        Double total_bill_cnt = 0d;
        Double total_sale_cnt = 0d;
        Double total_ret_cnt = 0d;
        List<Object> objects = unionSqlDao.query("select sum(sale_amt),sum(bill_cnt),sum(sale_qty),sum(ret_qty) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            Double sale_amt = ((Double)itemEntityObj[0]);
            Long bill_cnt = (Long)itemEntityObj[1];
            Double sale_qty = ((Double)itemEntityObj[2]);
            Double ret_qty = ((Double)itemEntityObj[3]);

            total_sale +=sale_amt!=null?sale_amt:0;
            total_bill_cnt += bill_cnt!=null?bill_cnt:0;
            total_sale_cnt += sale_qty!=null?sale_qty:0;
            total_ret_cnt +=ret_qty!=null?ret_qty:0;
        }
        Integer saler_cnt = 0;
        List<SalerEntity> salerEntities = salerDao.list("where org_id = '"+orgId+"'"+branchSql);
        if(CollectionUtils.isNotEmpty(salerEntities)){
            saler_cnt = salerEntities.size();
        }
        Double area = 0d;
        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'"+branchSql);
        for(BranchEntity branchEntity:branchEntities){
            area += branchEntity.getArea();
        }
        JSONArray results = new JSONArray();
        JSONObject resultJson = new JSONObject();
        resultJson.put("客单价",NumberUtil.getDouble2Format(total_sale/total_bill_cnt));
        resultJson.put("连带率",NumberUtil.getDouble2Format(total_sale_cnt/total_bill_cnt));
        resultJson.put("退货率",NumberUtil.getDouble2Format(total_ret_cnt/total_sale_cnt));
        resultJson.put("人均销售",NumberUtil.getDouble2Format(total_sale/saler_cnt));
        resultJson.put("坪效",NumberUtil.getDouble2Format(total_sale/area));
        resultJson.put("时间差",0);
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray salerOther(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String saler_name;
            String branch_no;
            Long join_time;
            double total_sale;
            double bill_cnt;
            double sale_qty;
            double valid_days;
            double total_sale_last_month;
            double bill_cnt_last_month;
            double sale_qty_last_month;
            double valid_days_last_month;
            double total_sale_last_year;
            double bill_cnt_last_year;
            double sale_qty_last_year;
            double valid_days_last_year;

            public String getSaler_name() {
                return saler_name;
            }

            public void setSaler_name(String saler_name) {
                this.saler_name = saler_name;
            }

            public String getBranch_no() {
                return branch_no;
            }

            public void setBranch_no(String branch_no) {
                this.branch_no = branch_no;
            }

            public Long getJoin_time() {
                return join_time;
            }

            public void setJoin_time(Long join_time) {
                this.join_time = join_time;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getValid_days() {
                return valid_days;
            }

            public void setValid_days(double valid_days) {
                this.valid_days = valid_days;
            }

            public double getBill_cnt() {
                return bill_cnt;
            }

            public void setBill_cnt(double bill_cnt) {
                this.bill_cnt = bill_cnt;
            }

            public double getSale_qty() {
                return sale_qty;
            }

            public void setSale_qty(double sale_qty) {
                this.sale_qty = sale_qty;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getValid_days_last_month() {
                return valid_days_last_month;
            }

            public void setValid_days_last_month(double valid_days_last_month) {
                this.valid_days_last_month = valid_days_last_month;
            }

            public double getBill_cnt_last_month() {
                return bill_cnt_last_month;
            }

            public void setBill_cnt_last_month(double bill_cnt_last_month) {
                this.bill_cnt_last_month = bill_cnt_last_month;
            }

            public double getSale_qty_last_month() {
                return sale_qty_last_month;
            }

            public void setSale_qty_last_month(double sale_qty_last_month) {
                this.sale_qty_last_month = sale_qty_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getValid_days_last_year() {
                return valid_days_last_year;
            }

            public void setValid_days_last_year(double valid_days_last_year) {
                this.valid_days_last_year = valid_days_last_year;
            }

            public double getBill_cnt_last_year() {
                return bill_cnt_last_year;
            }

            public void setBill_cnt_last_year(double bill_cnt_last_year) {
                this.bill_cnt_last_year = bill_cnt_last_year;
            }

            public double getSale_qty_last_year() {
                return sale_qty_last_year;
            }

            public void setSale_qty_last_year(double sale_qty_last_year) {
                this.sale_qty_last_year = sale_qty_last_year;
            }

        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        //本期
        Map<String,ItemData> dataMap = new HashMap<>();
//        List<SalerDayEntity> salerDayEntities= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')" + branchSql);
        List<Object> objects = unionSqlDao.query("select sale_id, sum(sale_amt), sum(bill_cnt), sum(sale_qty) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql+" group by sale_id");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Long bill_cnt = (Long)itemEntityObj[2];
            Double sale_qty = (Double)itemEntityObj[3];

            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setBill_cnt(itemData.getBill_cnt()+(bill_cnt!=null?bill_cnt:0));
                itemData.setSale_qty(itemData.getSale_qty()+(sale_qty!=null?sale_qty:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setBill_cnt(itemData.getBill_cnt()+(bill_cnt!=null?bill_cnt:0));
                itemData.setSale_qty(itemData.getSale_qty()+(sale_qty!=null?sale_qty:0));
                dataMap.put(sale_id,itemData);
            }
        }
        List<Object> objects_cnt = unionSqlDao.query("select sale_id,oper_date from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql);
        for(Object object : objects_cnt){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setValid_days(itemData.getValid_days()+1);
            }
        }

        String sale_idsStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(sale_idsStr)){
                sale_idsStr = sale_idsStr + ",'"+str+"'";
            }else{
                sale_idsStr =  "'"+str+"'";
            }
        }
        String sale_idsql="";
        if(StringUtils.isNotEmpty(sale_idsStr)){
            sale_idsql = " and sale_id in("+sale_idsStr+")";
        }
        List<SalerEntity> salerEntities = salerDao.list("where org_id = '"+orgId +"'"+ sale_idsql );
        for(SalerEntity salerEntity : salerEntities){
            String sale_id = salerEntity.getSale_id();
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setBranch_no(salerEntity.getBranch_no());
                itemData.setSaler_name(salerEntity.getSale_name());
                itemData.setJoin_time(salerEntity.getGmt_create().getTime());
            }
        }

        //环比
//        List<SalerDayEntity> salerDayEntities_last_month= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)" + branchSql);
        List<Object> objects_last_month = unionSqlDao.query("select sale_id, sum(sale_amt), sum(bill_cnt), sum(sale_qty) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)"+
                branchSql+" group by sale_id");
        for(Object object : objects_last_month){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Long bill_cnt = (Long)itemEntityObj[2];
            Double sale_qty = (Double)itemEntityObj[3];

            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                itemData.setBill_cnt_last_month(itemData.getBill_cnt_last_month()+(bill_cnt!=null?bill_cnt:0));
                itemData.setSale_qty_last_month(itemData.getSale_qty_last_month()+(sale_qty!=null?sale_qty:0));
            }
        }
        List<Object> objects_cnt_last_month = unionSqlDao.query("select sale_id,oper_date from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)"+
                branchSql);
        for(Object object : objects_cnt_last_month){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setValid_days_last_month(itemData.getValid_days()+1);
            }
        }

        //同比
//        List<SalerDayEntity> salerDayEntities_last_year= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)" + branchSql);
        List<Object> objects_last_year = unionSqlDao.query("select sale_id, sum(sale_amt), sum(bill_cnt), sum(sale_qty) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)"+
                branchSql+" group by sale_id");
        for(Object object : objects_last_year){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Long bill_cnt = (Long)itemEntityObj[2];
            Double sale_qty = (Double)itemEntityObj[3];

            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                itemData.setBill_cnt_last_year(itemData.getBill_cnt_last_year()+(bill_cnt!=null?bill_cnt:0));
                itemData.setSale_qty_last_year(itemData.getSale_qty_last_year()+(sale_qty!=null?sale_qty:0));
            }
        }
        List<Object> objects_cnt_last_year = unionSqlDao.query("select sale_id,oper_date from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)"+
                branchSql);
        for(Object object : objects_cnt_last_year){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setValid_days_last_year(itemData.getValid_days()+1);
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("员工编号",key);
            resultJson.put("门店名称",itemData.getBranch_no());
            resultJson.put("姓名",itemData.getSaler_name());
            if(itemData.getJoin_time()!=null){
                resultJson.put("入职时间",DateUtil.formatDate(new Date(itemData.getJoin_time()),DateUtil.SIMPLE_DATE_FORMAT));
                resultJson.put("工龄",(System.currentTimeMillis()-itemData.getJoin_time())/1000/60/60/24/12+"月");
            }
            resultJson.put("销售_本期",NumberUtil.getDouble2Format(itemData.getTotal_sale()));
            resultJson.put("有效天数_本期",itemData.getValid_days());
            resultJson.put("成交单数_本期",itemData.getBill_cnt());
            resultJson.put("销量_本期",itemData.getSale_qty());
            resultJson.put("客单价_本期",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getBill_cnt()));
            resultJson.put("连带率_本期",NumberUtil.getDouble2Format(itemData.getSale_qty()/itemData.getBill_cnt()*100));

            resultJson.put("销售_环比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()));
            resultJson.put("有效天数_环比",itemData.getValid_days_last_month());
            resultJson.put("成交单数_环比",itemData.getBill_cnt_last_month());
            resultJson.put("销量_环比",itemData.getSale_qty_last_month());
            resultJson.put("客单价_环比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/itemData.getBill_cnt_last_month()));
            resultJson.put("连带率_环比",NumberUtil.getDouble2Format(itemData.getSale_qty_last_month()/itemData.getBill_cnt_last_month()*100));

            resultJson.put("销售_同比",itemData.getTotal_sale_last_year());
            resultJson.put("有效天数_同比",itemData.getValid_days_last_year());
            resultJson.put("成交单数_同比",itemData.getBill_cnt_last_year());
            resultJson.put("销量_同比",itemData.getSale_qty_last_year());
            resultJson.put("客单价_同比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/itemData.getBill_cnt_last_year()));
            resultJson.put("连带率_同比",NumberUtil.getDouble2Format(itemData.getSale_qty_last_year()/itemData.getBill_cnt_last_year()*100));
            results.add(resultJson);
        }
        return results;
    }

}
