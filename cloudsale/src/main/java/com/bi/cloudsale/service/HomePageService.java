package com.bi.cloudsale.service;

import java.util.Date;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.dto.ClientSaveDto;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/15 17:45
 * @Version: 1.0
 **/
public interface HomePageService {
    JSONArray yesterdayGain(String orgId) throws BaseException;
    JSONArray todayRealtimeIndex(String orgId) throws BaseException;
    JSONArray todayLowestTarget(String orgId) throws BaseException;
    JSONArray monthGain(String orgId) throws BaseException;
    JSONArray monthGainPercent(String orgId) throws BaseException;
    JSONArray yesterdayTZDL(String orgId) throws BaseException;
    JSONArray todayBranchSale(String orgId) throws BaseException;
    JSONArray someDayBranchSale(String orgId,String oper_date) throws BaseException;
    JSONArray someDayBranchClsSale(String orgId,String branchNo,Date oper_date) throws BaseException;
    JSONArray yestdayBranchSale(String orgId) throws BaseException;
    JSONArray last30BranchBillCnt(String orgId) throws BaseException;
    JSONArray last30BranchNewClient(String orgId) throws BaseException;
    JSONArray last30BranchNewVip(String orgId) throws BaseException;
    JSONArray yesterdayRetDetail(String orgId) throws BaseException;
    JSONArray yesterdayDisDetail(String orgId) throws BaseException;
    JSONArray yesterdayClientAmtDetail(String orgId) throws BaseException;
    JSONArray yesterdayLiandailvDetail(String orgId) throws BaseException;
    JSONArray todayRealtimeAim(String orgId) throws BaseException;
}
