package com.bi.cloudsale.service.sys.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.ModuleDto;
import com.bi.cloudsale.persistent.dao.sys.ModuleInfoDao;
import com.bi.cloudsale.persistent.dao.sys.RoleAuthDao;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.sys.ModuleInfoEntity;
import com.bi.cloudsale.persistent.entity.sys.RoleAuthEntity;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.persistent.enums.sys.RoleAuthTypeEnum;
import com.bi.cloudsale.service.sys.ModuleService;

@Service
@Transactional
public class ModuleServiceImpl implements ModuleService {
	
	private static final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);
	
	@Resource
	private ModuleInfoDao moduleDao;
	@Resource
	private RoleAuthDao roleAuthDao;
	@Resource
	private UserInfoDao userInfoDao;

	@Override
	public List<ModuleDto> queryAll(String userId){
		List<ModuleDto> dtoList = new ArrayList<ModuleDto>();
		List<ModuleInfoEntity> entityList = moduleDao.queryAll();
		UserInfoEntity userEntity = userInfoDao.queryByUserId(userId);
		RoleAuthEntity roleAuthEntity = roleAuthDao.queryByRoleCode(userEntity.getRoleCode(), RoleAuthTypeEnum.MENU.getValue());
		if(roleAuthEntity != null) {
			String menuIds = roleAuthEntity.getItemId();
			if(StringUtils.isNotEmpty(menuIds)) {
				List<String> idsList = Arrays.asList(menuIds.split(","));
				Iterator<ModuleInfoEntity> iter = entityList.iterator();
				try {
					while(iter.hasNext()) {
						ModuleInfoEntity moduleInfoEntity = iter.next();
						if(idsList.contains(moduleInfoEntity.getModuleId())) {
							ModuleDto moduleDto = new ModuleDto();
							BeanUtil.copyBeanProperties(moduleDto, moduleInfoEntity);
							dtoList.add(moduleDto);
						}
					}
				} catch (BaseException e) {
					log.error(e.getMessage(), e);
				}
			}
		}
		return dtoList;
	}

	@Override
	public List<OptionDto> queryAllOptions(String userId) {
		List<OptionDto> list = new ArrayList<OptionDto>();
		UserInfoEntity userEntity = userInfoDao.queryByUserId(userId);
		RoleAuthEntity roleAuthEntity = roleAuthDao.queryByRoleCode(userEntity.getRoleCode(), RoleAuthTypeEnum.MENU.getValue());
		if(roleAuthEntity != null) {
			String menuIds = roleAuthEntity.getItemId();
			if(StringUtils.isNotEmpty(menuIds)) {
				List<String> idsList = Arrays.asList(menuIds.split(","));
				List<ModuleInfoEntity> entitys = moduleDao.queryAll();
				Map<String,OptionDto> map = new HashMap<String,OptionDto>();
				if(entitys != null && entitys.size() > 0) {
					Iterator<ModuleInfoEntity> iter = entitys.iterator();
					while(iter.hasNext()) {
						ModuleInfoEntity moduleInfoEntity = iter.next();
						if(idsList.contains(moduleInfoEntity.getModuleId())) {
							OptionDto optionDto =  new OptionDto();
							optionDto.setId(moduleInfoEntity.getModuleId());
							optionDto.setLabel(moduleInfoEntity.getModuleName());
							if("0".equals(moduleInfoEntity.getParentId())) {
								list.add(optionDto);
							}else {
								OptionDto parentOptionDto = map.get(moduleInfoEntity.getParentId());
								if(parentOptionDto.getChildren() == null) {
									parentOptionDto.setChildren(new ArrayList<OptionDto>());
								}
								parentOptionDto.getChildren().add(optionDto);
							}
							map.put(moduleInfoEntity.getModuleId(), optionDto);
						}
					}
				}
			}
		}
		return list;
	}
	
}
