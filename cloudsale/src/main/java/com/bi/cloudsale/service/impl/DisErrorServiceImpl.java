package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.DisErrorService;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
@Transactional
public class DisErrorServiceImpl implements DisErrorService {

    private static final Logger log = LoggerFactory.getLogger(DisErrorServiceImpl.class);

    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private ItemDayDao itemDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BrandDayDao brandDayDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private VipDayDao vipDayDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {



        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray branchDislv(String orgId, String begin, String end, String branchNo) throws BaseException {

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
//        List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) <= (to_days('"+end+"'))" +
//                " and  to_days(oper_date) >= (to_days('"+begin+"')) and org_id = '"+orgId+"'"+branchSql+"");
        double total_dis = 0;
        double total_sale = 0;
        List<Object> objects = unionSqlDao.query("select sum(dis_amt),sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= to_days('"+end+"')" +
                " and to_days(oper_date) >= to_days('"+begin+"')"+ branchSql);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            Double total_sale1 = ((Double)itemEntityObj[0]);
            Double total_dis1 = (Double)itemEntityObj[1];
            total_dis += (total_dis1!=null?total_dis1:0);
            total_sale += (total_sale1!=null?total_sale1:0);
        }
        JSONArray results = new JSONArray();
        JSONObject result = new JSONObject();
        result.put("name","全店折扣");
        result.put("value",NumberUtil.getDouble2Format(total_dis/total_sale*100));
        results.add(result);

        return results;
    }

    @Override
    public JSONArray branchClsDisFenbu(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String cls_no;
            double total_amt;
            double total_dis_amt;

            public String getCls_no() {
                return cls_no;
            }

            public void setCls_no(String cls_no) {
                this.cls_no = cls_no;
            }

            public double getTotal_amt() {
                return total_amt;
            }

            public void setTotal_amt(double total_amt) {
                this.total_amt = total_amt;
            }

            public double getTotal_dis_amt() {
                return total_dis_amt;
            }

            public void setTotal_dis_amt(double total_dis_amt) {
                this.total_dis_amt = total_dis_amt;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new LinkedHashMap<>();
//        List<ClsDayEntity> clsDayEntities = clsDayDao.list("where to_days(oper_date) <= (to_days('"+end+"'))" +
//                " and  to_days(oper_date) >= (to_days('"+begin+"')) and org_id = '"+orgId+"'"+branchSql);
        List<Object> objects = unionSqlDao.query("select item_clsno,sum(sale_amt),sum(dis_amt) from ClsDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days('"+end+"')) and to_days(oper_date) >= (to_days('"+begin+"'))"+
                branchSql+" group by item_clsno");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_clsno = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Double total_dis = (Double)itemEntityObj[2];
            ItemData itemData = dataMap.get(item_clsno);
            if(itemData!=null){
                itemData.setTotal_dis_amt(itemData.getTotal_dis_amt()+(total_dis!=null?total_dis:0));
                itemData.setTotal_amt(itemData.getTotal_amt()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_dis_amt(itemData.getTotal_dis_amt()+(total_dis!=null?total_dis:0));
                itemData.setTotal_amt(itemData.getTotal_amt()+(total_sale!=null?total_sale:0));
                dataMap.put(item_clsno,itemData);
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String item_no = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("类别",item_no);
            resultJson.put("销售金额",itemData.getTotal_amt());
            resultJson.put("折让金额",itemData.getTotal_dis_amt());
            resultJson.put("平均折让",NumberUtil.getDouble2Format(itemData.getTotal_dis_amt()/itemData.getTotal_amt()*100));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchClsBranchDisFenbu(String orgId, String begin, String end, String branchNo, String clsNo) throws BaseException {

        class ItemData{
            double total_amt;
            double total_dis_amt;

            public double getTotal_amt() {
                return total_amt;
            }

            public void setTotal_amt(double total_amt) {
                this.total_amt = total_amt;
            }

            public double getTotal_dis_amt() {
                return total_dis_amt;
            }

            public void setTotal_dis_amt(double total_dis_amt) {
                this.total_dis_amt = total_dis_amt;
            }
        }

        List<ItemEntity> itemEntities = itemDao.list("where item_clsno = '"+clsNo+"' and org_id = '"+orgId+"'");
        String brand_nos = "";
        for(ItemEntity itemEntity:itemEntities){
            String brand_no = itemEntity.getItem_brandno();
            if(StringUtils.isNotEmpty(brand_nos)){
                brand_nos = brand_nos + ",'"+brand_no+"'";
            }else{
                brand_nos =  "'"+brand_no+"'";
            }
        }
        String brandSql = StringUtils.isEmpty(branchNo)?"":" and brand_no = '"+branchNo+"'";
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new LinkedHashMap<>();
//        List<BrandDayEntity> itemDayEntities = brandDayDao.list("where to_days(oper_date) <= (to_days('"+end+"'))" +
//                " and to_days(oper_date) >= (to_days('"+begin+"')) and org_id = '"+orgId+"'"+branchSql+brandSql);
        List<Object> objects = unionSqlDao.query("select item_brandno, sum(sale_amt),sum(dis_amt) from BrandDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days('"+end+"')) and to_days(oper_date) >= (to_days('"+begin+"'))"+
                branchSql + brandSql +" group by item_brandno");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_brandno = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Double total_dis = (Double)itemEntityObj[2];
            ItemData itemData = dataMap.get(item_brandno);
            if(itemData!=null){
                itemData.setTotal_dis_amt(itemData.getTotal_dis_amt()+(total_dis!=null?total_dis:0));
                itemData.setTotal_amt(itemData.getTotal_amt()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_dis_amt(itemData.getTotal_dis_amt()+(total_dis!=null?total_dis:0));
                itemData.setTotal_amt(itemData.getTotal_amt()+(total_sale!=null?total_sale:0));
                dataMap.put(item_brandno,itemData);
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String brand_no = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("品牌",brand_no);
            resultJson.put("同单销售额",itemData.getTotal_amt());
            resultJson.put("同单折让额",itemData.getTotal_dis_amt());
            resultJson.put("同单折让率",NumberUtil.getDouble2Format(itemData.getTotal_dis_amt()/itemData.getTotal_amt()*100));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchBestSaleItemDis(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            double total_sale;
            String item_name;
            String item_subno;
            double total_dis;

            public String getItem_subno() {
                return item_subno;
            }

            public void setItem_subno(String item_subno) {
                this.item_subno = item_subno;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getTotal_dis() {
                return total_dis;
            }

            public void setTotal_dis(double total_dis) {
                this.total_dis = total_dis;
            }
        }

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = Maps.newLinkedHashMap();
        //1销量、销售额
//        List<ItemDayEntity> itemDayEntities = itemDayDao.list("where to_days(oper_date) <= (to_days('"+end+"'))"  +
//                " and to_days(oper_date) >= (to_days('"+begin+"'))" +
//                " and org_id = '"+orgId+"'"+branchSql+" order by sale_amt desc",50);
        List<Object> objects = unionSqlDao.query("select item_no, sum(sale_amt),sum(dis_amt) from ItemDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days('"+end+"')) and to_days(oper_date) >= (to_days('"+begin+"'))"+
                branchSql+" group by item_no order by sum(sale_amt) desc",1,50);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Double total_dis = (Double)itemEntityObj[2];
            ItemData dataDay = dataMap.get(item_no);
            if(dataDay!=null){
                dataDay.setTotal_sale(dataDay.getTotal_sale() +  (total_sale!=null?total_sale:0));
                dataDay.setTotal_dis(dataDay.getTotal_dis()+ (total_dis!=null?total_dis:0));
            }else{
                dataDay = new ItemData();
                dataDay.setTotal_sale(dataDay.getTotal_sale() +  (total_sale!=null?total_sale:0));
                dataDay.setTotal_dis(dataDay.getTotal_dis()+ (total_dis!=null?total_dis:0));
                dataMap.put(item_no,dataDay);
            }
        }
        String item_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,branch_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_nosStr)){
                item_nosStr = item_nosStr + ",'"+str+"'";
            }else{
                item_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_nosStr)){
            //2商品名
            List<ItemEntity> itemEntities = itemDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'");
            for(ItemEntity itemEntity : itemEntities){
                dataMap.get(itemEntity.getItem_no()).setItem_name(itemEntity.getItem_name());
                dataMap.get(itemEntity.getItem_no()).setItem_subno(itemEntity.getItem_subno());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("item_no",key);
            resultJson.put("条码",itemData.getItem_subno());
            resultJson.put("商品名称",itemData.getItem_name());
            resultJson.put("销售金额",itemData.getTotal_sale());
            resultJson.put("折让金额",itemData.getTotal_dis());
            resultJson.put("折扣率",NumberUtil.getDouble2Format(itemData.getTotal_dis()/itemData.getTotal_sale()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchErrorlv(String orgId, String branchNo) throws BaseException {

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        List<StockEntity> stockEntities = stockDao.list("where org_id = '"+orgId+"'"+branchSql+"");
        double total_error_cnt = 0;
        double total_cnt = 0;
        if(CollectionUtils.isNotEmpty(stockEntities)){
            total_cnt = stockEntities.size();
        }
        for(StockEntity stockEntity:stockEntities){
            double stock_qty = stockEntity.getStock_qty()!=null?stockEntity.getStock_qty():0;
            if(stock_qty<0){
                total_error_cnt = total_error_cnt +1;
            }
        }
        JSONArray results = new JSONArray();
        JSONObject result = new JSONObject();
        result.put("异常率",NumberUtil.getDouble2Format(total_error_cnt/total_cnt*100));
        results.add(result);

        return results;
    }

    @Override
    public JSONArray branchErrorFenbu(String orgId, String branchNo) throws BaseException {
        class ItemData{
            double total_error;
            String cls_name;

            public double getTotal_error() {
                return total_error;
            }

            public void setTotal_error(double total_error) {
                this.total_error = total_error;
            }

            public String getCls_name() {
                return cls_name;
            }

            public void setCls_name(String cls_name) {
                this.cls_name = cls_name;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and a.branch_no = '"+branchNo+"'";
        List<Object> sqlResults = unionSqlDao.query("SELECT c.item_clsname,c.item_clsno,b.item_name,b.item_no,a.stock_qty " +
                "FROM StockEntity a,ItemEntity b, ItemClsEntity c " +
                "where a.item_no=b.item_no and b.item_clsno=c.item_clsno and c.org_id = '"+orgId+"'"+branchSql);
        for(Object itemEntity : sqlResults){
            Object[]itemEntityObj = (Object[])itemEntity;
            String cls_name = itemEntityObj[0].toString();
            String cls_no = itemEntityObj[1].toString();
            Double stock_qty = (Double)itemEntityObj[4];
            if(stock_qty<0){
                ItemData itemData = dataMap.get(cls_no);
                if(itemData==null){
                    itemData = new ItemData();
                    itemData.setCls_name(cls_name);
                    itemData.setTotal_error(stock_qty);
                    dataMap.put(cls_no,itemData);
                }else{
                    itemData.setCls_name(cls_name);
                    itemData.setTotal_error(itemData.getTotal_error()+stock_qty);
                }
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",itemData.getCls_name());
            resultJson.put("value",itemData.getTotal_error());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchErrorDetail(String orgId, String branchNo, Integer pageSize, Integer pageNo,String clsNo) throws BaseException {
        class ItemData{
            String branch_name;
            String item_no;
            String item_subno;
            String item_name;
            String brand_name;
            double item_sale_price;
            double stock_cnt;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public String getItem_subno() {
                return item_subno;
            }

            public void setItem_subno(String item_subno) {
                this.item_subno = item_subno;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }

            public double getItem_sale_price() {
                return item_sale_price;
            }

            public void setItem_sale_price(double item_sale_price) {
                this.item_sale_price = item_sale_price;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and a.branch_no = '"+branchNo+"'";
        String clsNoSql = StringUtils.isEmpty(clsNo)?"":" and c.item_clsno = '"+clsNo+"'";
        List<Object> sqlResults = unionSqlDao.query("SELECT c.item_clsname,c.item_clsno,b.item_name,b.item_no,b.item_subno,b.sale_price,a.stock_qty,d.branch_name,e.item_brandname\n" +
                " FROM StockEntity a,ItemEntity b, ItemClsEntity c, BranchEntity d,BrandEntity e \n" +
                "where a.item_no=b.item_no and b.item_clsno=c.item_clsno and d.branch_no=a.branch_no\n" +
                "and e.item_brandno = b.item_brandno and c.org_id = '"+orgId+"'"+branchSql+clsNoSql,pageNo,pageSize);
        for(Object itemEntity : sqlResults){
            Object[]itemEntityObj = (Object[])itemEntity;
            String cls_name = itemEntityObj[0].toString();
            String cls_no = itemEntityObj[1].toString();
            String item_name = itemEntityObj[2].toString();
            String item_no = (String)itemEntityObj[3];
            String item_subno = (String)itemEntityObj[4];
            Double sale_price = (Double)itemEntityObj[5];
            Double stock_qty = (Double)itemEntityObj[6];
            String branch_name = itemEntityObj[7].toString();
            String brand_name = itemEntityObj[7].toString();
            if(stock_qty<0){
                ItemData itemData = dataMap.get(cls_no+item_no);
                if(itemData==null){
                    itemData = new ItemData();
                    itemData.setItem_name(item_name);
                    itemData.setItem_subno(item_subno);
                    itemData.setBranch_name(branch_name);
                    itemData.setBrand_name(brand_name);
                    itemData.setItem_no(item_no);
                    itemData.setItem_sale_price(sale_price);
                    itemData.setStock_cnt(stock_qty);
                    dataMap.put(cls_no,itemData);
                }else{
                    itemData.setItem_name(item_name);
                    itemData.setItem_subno(item_subno);
                    itemData.setBranch_name(branch_name);
                    itemData.setBrand_name(brand_name);
                    itemData.setItem_no(item_no);
                    itemData.setItem_sale_price(sale_price);
                    itemData.setStock_cnt(stock_qty);
                }
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店",itemData.getBranch_name());
            resultJson.put("item_no",itemData.getItem_no());
            resultJson.put("条码",itemData.getItem_subno());
            resultJson.put("名称",itemData.getItem_name());
            resultJson.put("品牌",itemData.getBrand_name());
            resultJson.put("零售价",itemData.getItem_sale_price());
            resultJson.put("库存数量",itemData.getStock_cnt());
            resultJson.put("库存零售金额",NumberUtil.getDouble2Format(itemData.getItem_sale_price()*itemData.getStock_cnt()));
            results.add(resultJson);
        }
        return results;
    }

}
