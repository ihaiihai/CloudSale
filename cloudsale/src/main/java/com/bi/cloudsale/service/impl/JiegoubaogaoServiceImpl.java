package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.entity.BranchEntity;
import com.bi.cloudsale.persistent.entity.BrandEntity;
import com.bi.cloudsale.persistent.entity.ItemClsEntity;
import com.bi.cloudsale.persistent.entity.ItemEntity;
import com.bi.cloudsale.service.JiegoubaogaoService;
import com.bi.cloudsale.service.ShouqingService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class JiegoubaogaoServiceImpl implements JiegoubaogaoService {

    private static final Logger log = LoggerFactory.getLogger(JiegoubaogaoServiceImpl.class);

    @Resource
    private BrandDao brandDao;
    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private ClsDayDao itemClsDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private SalerDayDao salerDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private BrandDayDao brandDayDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {

        List<BranchEntity> branchEntities = branchDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray brands(String orgId) throws BaseException {

        List<BrandEntity> brandEntities = brandDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BrandEntity brandEntity:brandEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("brand_no",brandEntity.getItem_brandno());
            jsonObject.put("brand_name",brandEntity.getItem_brandname());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray items(String orgId) throws BaseException {
        List<ItemEntity> itemEntities = itemDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(ItemEntity itemEntity:itemEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("item_no",itemEntity.getItem_no());
            jsonObject.put("item_name",itemEntity.getItem_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray clses(String orgId) throws BaseException {
        List<ItemClsEntity> itemClsEntities = itemClsDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(ItemClsEntity itemClsEntity:itemClsEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("item_clsno",itemClsEntity.getItem_clsno());
            jsonObject.put("item_clsname",itemClsEntity.getItem_clsname());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray query(String orgId, String type, String begin,String end, String branchNos, String itemClses, String brands,JSONArray priceZones) throws BaseException {
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and t1.branch_no in("+branchSqlin+")":"";
        String itemClsSqlin = "";
        if(StringUtils.isNotEmpty(itemClses)){
            String[] item_clsnos = itemClses.split(",");
            for(String item_clsno:item_clsnos){
                if(!"".equals(itemClsSqlin)){
                    itemClsSqlin = itemClsSqlin+","+"'"+item_clsno+"'";
                }else{
                    itemClsSqlin = "'"+item_clsno+"'";
                }
            }
        }
        String itemClsSql = StringUtils.isNotEmpty(itemClsSqlin)?" and t4.item_clsno in("+itemClsSqlin+")":"";
        String brandSqlin = "";
        if(StringUtils.isNotEmpty(brands)){
            String[] brand_nos = brands.split(",");
            for(String brand_no:brand_nos){
                if(!"".equals(brandSqlin)){
                    brandSqlin = brandSqlin+","+"'"+brand_no+"'";
                }else{
                    brandSqlin = "'"+brand_no+"'";
                }
            }
        }
        String brandSql = StringUtils.isNotEmpty(brandSqlin)?" and t4.item_brandno in("+brandSqlin+")":"";
        String priceZoneSql = "";
        if(priceZones!=null&&priceZones.size()>0){
            if(priceZones.size()==1){
                JSONObject job = priceZones.getJSONObject(0);
                String low =job.getString("low");
                String high =job.getString("high");
                priceZoneSql = " and t3.sale_price>='"+low+"' and t3.sale_price<='"+high+"' ";
            }else{
                for(int i=0;i<priceZones.size();i++){
                    JSONObject job = priceZones.getJSONObject(i);
                    String low =job.getString("low");
                    String high =job.getString("high");
                    if(i==0){
                        priceZoneSql = " and ((t3.sale_price>='"+low+"' and t3.sale_price<='"+high+"') or ";
                    }else if(i==(priceZones.size()-1)){
                        priceZoneSql =priceZoneSql+ " ( t3.sale_price>='"+low+"' and t3.sale_price<='"+high+"'))";
                    }else{
                        priceZoneSql =priceZoneSql+ " ( t3.sale_price>='"+low+"' and t3.sale_price<='"+high+"') or";
                    }
                }
            }
        }
        class ItemData{
            String item_no;
            String branch_no;
            String item_clsno;
            String item_clsname;
            String item_brandno;
            String item_brandname;
            double sale_cnt;
            double total_sale;
            double total_cost;
            double total_bill;
            double total_ret;
            double total_dis;
            double total_ret_cnt;
            double total_bill_vip_cnt;
            double stock_cnt;
            double stock_sale;
            double stock_cost;

            Set<String> hasSaleItemNos;
            Set<String> hasStockItemNos;

            public String getItem_brandname() {
                return item_brandname;
            }

            public void setItem_brandname(String item_brandname) {
                this.item_brandname = item_brandname;
            }

            public double getTotal_dis() {
                return total_dis;
            }

            public void setTotal_dis(double total_dis) {
                this.total_dis = total_dis;
            }

            public double getTotal_bill_vip_cnt() {
                return total_bill_vip_cnt;
            }

            public void setTotal_bill_vip_cnt(double total_bill_vip_cnt) {
                this.total_bill_vip_cnt = total_bill_vip_cnt;
            }

            public Set<String> getHasStockItemNos() {
                return hasStockItemNos;
            }

            public void setHasStockItemNos(Set<String> hasStockItemNos) {
                this.hasStockItemNos = hasStockItemNos;
            }

            public String getItem_clsname() {
                return item_clsname;
            }

            public void setItem_clsname(String item_clsname) {
                this.item_clsname = item_clsname;
            }

            public Set<String> getHasSaleItemNos() {
                return hasSaleItemNos;
            }

            public void setHasSaleItemNos(Set<String> hasSaleItemNos) {
                this.hasSaleItemNos = hasSaleItemNos;
            }

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public String getBranch_no() {
                return branch_no;
            }

            public void setBranch_no(String branch_no) {
                this.branch_no = branch_no;
            }

            public String getItem_clsno() {
                return item_clsno;
            }

            public void setItem_clsno(String item_clsno) {
                this.item_clsno = item_clsno;
            }

            public String getItem_brandno() {
                return item_brandno;
            }

            public void setItem_brandno(String item_brandno) {
                this.item_brandno = item_brandno;
            }

            public double getSale_cnt() {
                return sale_cnt;
            }

            public void setSale_cnt(double sale_cnt) {
                this.sale_cnt = sale_cnt;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_bill() {
                return total_bill;
            }

            public void setTotal_bill(double total_bill) {
                this.total_bill = total_bill;
            }

            public double getTotal_ret() {
                return total_ret;
            }

            public void setTotal_ret(double total_ret) {
                this.total_ret = total_ret;
            }

            public double getTotal_ret_cnt() {
                return total_ret_cnt;
            }

            public void setTotal_ret_cnt(double total_ret_cnt) {
                this.total_ret_cnt = total_ret_cnt;
            }

            public double getStock_sale() {
                return stock_sale;
            }

            public void setStock_sale(double stock_sale) {
                this.stock_sale = stock_sale;
            }

            public double getStock_cost() {
                return stock_cost;
            }

            public void setStock_cost(double stock_cost) {
                this.stock_cost = stock_cost;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();
        if(StringUtils.equals(type,"cls")){
            List<Object> objectList = unionSqlDao.query(" SELECT t1.sale_amt,t1.sale_qty,t1.bill_cnt,t1.ret_amt,t1.ret_qty,t1.cost_amt,t1.dis_amt,t1.bill_vip_cnt,t1.item_no,\n" +
                    " t2.stock_qty,t2.branch_no,t2.cost_price,t2.cost_amt, \n" +
                    " t4.item_clsno,t4.item_clsname\n" +
                    "  FROM ItemDayEntity t1,StockEntity t2,ItemEntity t3,ItemClsEntity t4\n" +
                    " where t1.item_no=t2.item_no and t3.item_no=t2.item_no and t3.item_clsno=t4.item_clsno" +
                    " and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' and t1.org_id='"+orgId+"'"+priceZoneSql+branchSql+itemClsSql);
            for(Object itemEntity: objectList){
                Object[]itemEntityObj = (Object[])itemEntity;
                Double total_sale = (Double)itemEntityObj[0];
                Double total_sale_cnt = (Double)itemEntityObj[1];
                Long bill_cnt = (Long)itemEntityObj[2];
                Double total_ret = (Double)itemEntityObj[3];
                Double ret_cnt = (Double)itemEntityObj[4];
                Double total_cost = (Double)itemEntityObj[5];
                Double total_dis = (Double)itemEntityObj[6];
                Long bill_vip_cnt = (Long)itemEntityObj[7];
                String item_no = (String)itemEntityObj[8];
                Double stock_cnt = (Double)itemEntityObj[9];
                String branch_no = (String)itemEntityObj[10];
                Double cost_price = (Double)itemEntityObj[11];
                Double cost_amt = (Double)itemEntityObj[12];
                String itemClsno = (String)itemEntityObj[13];
                String item_clsname = (String)itemEntityObj[14];

                ItemData itemData = dataMap.get(branch_no+"@@"+itemClsno);
                if(itemData!=null){
                    if((stock_cnt!=null?stock_cnt:0)>0){
                        itemData.getHasStockItemNos().add(item_no);
                    }
                    itemData.getHasSaleItemNos().add(item_no);
                    itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                    itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                    itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                    itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                    itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
                    itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                    itemData.setTotal_bill_vip_cnt(itemData.getTotal_bill_vip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
                    itemData.setTotal_dis(itemData.getTotal_dis()+(total_dis!=null?total_dis:0));
                }else{
                    itemData = new ItemData();
                    itemData.setHasSaleItemNos(new HashSet<String>());
                    itemData.setHasStockItemNos(new HashSet<String>());
                    itemData.setItem_clsname(item_clsname);
                    itemData.setBranch_no(branch_no);
                    itemData.setStock_cnt((stock_cnt!=null?stock_cnt:0));
                    itemData.setItem_clsno(itemClsno);
                    itemData.setStock_cost(cost_price!=null?cost_price:0);
                    itemData.setStock_sale(cost_amt!=null?cost_amt:0);

                    if((stock_cnt!=null?stock_cnt:0)>0){
                       itemData.getHasStockItemNos().add(item_no);
                    }
                    itemData.getHasSaleItemNos().add(item_no);
                    itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                    itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                    itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                    itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                    itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
                    itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                    itemData.setTotal_bill_vip_cnt(itemData.getTotal_bill_vip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
                    itemData.setTotal_dis(itemData.getTotal_dis()+(total_dis!=null?total_dis:0));

                    dataMap.put(branch_no+"@@"+itemClsno,itemData);
                }
            }
        }else if(StringUtils.equals(type,"branch")){
            List<Object> objectList = unionSqlDao.query(" SELECT t1.sale_amt,t1.sale_qty,t1.bill_cnt,t1.ret_amt,t1.ret_qty,t1.cost_amt,t1.dis_amt,t1.bill_vip_cnt,t1.item_no,\n" +
                    " t2.stock_qty,t2.branch_no,t2.cost_price,t2.cost_amt, \n" +
                    " t4.item_clsno,t4.item_clsname\n" +
                    "  FROM ItemDayEntity t1,StockEntity t2,ItemEntity t3,ItemClsEntity t4\n" +
                    " where t1.item_no=t2.item_no and t3.item_no=t2.item_no and t3.item_clsno=t4.item_clsno" +
                    " and t1.oper_date>'"+begin+"' and t1.oper_date<'"+end+"' and t1.org_id='"+orgId+"'"+priceZoneSql+branchSql+itemClsSql);
            for(Object itemEntity: objectList){
                Object[]itemEntityObj = (Object[])itemEntity;
                Double total_sale = (Double)itemEntityObj[0];
                Double total_sale_cnt = (Double)itemEntityObj[1];
                Long bill_cnt = (Long)itemEntityObj[2];
                Double total_ret = (Double)itemEntityObj[3];
                Double ret_cnt = (Double)itemEntityObj[4];
                Double total_cost = (Double)itemEntityObj[5];
                Double total_dis = (Double)itemEntityObj[6];
                Long bill_vip_cnt = (Long)itemEntityObj[7];
                String item_no = (String)itemEntityObj[8];
                Double stock_cnt = (Double)itemEntityObj[9];
                String branch_no = (String)itemEntityObj[10];
                Double cost_price = (Double)itemEntityObj[11];
                Double cost_amt = (Double)itemEntityObj[12];
                String itemClsno = (String)itemEntityObj[13];
                String item_clsname = (String)itemEntityObj[14];

                ItemData itemData = dataMap.get(branch_no+"@@"+branch_no);
                if(itemData!=null){
                    if((stock_cnt!=null?stock_cnt:0)>0){
                        itemData.getHasStockItemNos().add(item_no);
                    }
                    itemData.getHasSaleItemNos().add(item_no);
                    itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                    itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                    itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                    itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                    itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
                    itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                    itemData.setTotal_bill_vip_cnt(itemData.getTotal_bill_vip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
                    itemData.setTotal_dis(itemData.getTotal_dis()+(total_dis!=null?total_dis:0));
                }else{
                    itemData = new ItemData();
                    itemData.setHasSaleItemNos(new HashSet<String>());
                    itemData.setHasStockItemNos(new HashSet<String>());
                    itemData.setItem_clsname(item_clsname);
                    itemData.setBranch_no(branch_no);
                    itemData.setStock_cnt((stock_cnt!=null?stock_cnt:0));
                    itemData.setItem_clsno(itemClsno);
                    itemData.setStock_cost(cost_price!=null?cost_price:0);
                    itemData.setStock_sale(cost_amt!=null?cost_amt:0);

                    if((stock_cnt!=null?stock_cnt:0)>0){
                        itemData.getHasStockItemNos().add(item_no);
                    }
                    itemData.getHasSaleItemNos().add(item_no);
                    itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                    itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                    itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                    itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                    itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
                    itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                    itemData.setTotal_bill_vip_cnt(itemData.getTotal_bill_vip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
                    itemData.setTotal_dis(itemData.getTotal_dis()+(total_dis!=null?total_dis:0));

                    dataMap.put(branch_no+"@@"+branch_no,itemData);
                }
            }
        }
        else if(StringUtils.equals(type,"item_brand")){
            List<Object> objectList = unionSqlDao.query(" SELECT t1.sale_amt,t1.sale_qty,t1.bill_cnt,t1.ret_amt,t1.ret_qty,t1.cost_amt,t1.dis_amt,t1.bill_vip_cnt,t1.item_no,\n" +
                    " t2.stock_qty,t2.branch_no,t2.cost_price,t2.cost_amt, \n" +
                    " t4.item_brandno,t4.item_brandname\n" +
                    "  FROM ItemDayEntity t1,StockEntity t2,ItemEntity t3,BrandEntity t4\n" +
                    " where t1.item_no=t2.item_no and t3.item_no=t2.item_no and t3.item_brandno=t4.item_brandno" +
                    " and t1.oper_date>'"+begin+"' and t1.oper_date<'"+end+"' and t1.org_id='"+orgId+"'"+priceZoneSql+branchSql+brandSql);
            for(Object itemEntity: objectList){
                Object[]itemEntityObj = (Object[])itemEntity;
                Double total_sale = (Double)itemEntityObj[0];
                Double total_sale_cnt = (Double)itemEntityObj[1];
                Long bill_cnt = (Long)itemEntityObj[2];
                Double total_ret = (Double)itemEntityObj[3];
                Double ret_cnt = (Double)itemEntityObj[4];
                Double total_cost = (Double)itemEntityObj[5];
                Double total_dis = (Double)itemEntityObj[6];
                Long bill_vip_cnt = (Long)itemEntityObj[7];
                String item_no = (String)itemEntityObj[8];
                Double stock_cnt = (Double)itemEntityObj[9];
                String branch_no = (String)itemEntityObj[10];
                Double cost_price = (Double)itemEntityObj[11];
                Double cost_amt = (Double)itemEntityObj[12];
                String item_brandno = (String)itemEntityObj[13];
                String item_brandname = (String)itemEntityObj[14];

                ItemData itemData = dataMap.get(branch_no+"@@"+item_brandno);
                if(itemData!=null){
                    if((stock_cnt!=null?stock_cnt:0)>0){
                        itemData.getHasStockItemNos().add(item_no);
                    }
                    itemData.getHasSaleItemNos().add(item_no);
                    itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                    itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                    itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                    itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                    itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
                    itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                    itemData.setTotal_bill_vip_cnt(itemData.getTotal_bill_vip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
                    itemData.setTotal_dis(itemData.getTotal_dis()+(total_dis!=null?total_dis:0));
                }else{
                    itemData = new ItemData();
                    itemData.setHasSaleItemNos(new HashSet<String>());
                    itemData.setHasStockItemNos(new HashSet<String>());
                    itemData.setItem_brandname(item_brandname);
                    itemData.setBranch_no(branch_no);
                    itemData.setStock_cnt((stock_cnt!=null?stock_cnt:0));
                    itemData.setItem_brandno(item_brandno);
                    itemData.setStock_cost(cost_price!=null?cost_price:0);
                    itemData.setStock_sale(cost_amt!=null?cost_amt:0);

                    if((stock_cnt!=null?stock_cnt:0)>0){
                        itemData.getHasStockItemNos().add(item_no);
                    }
                    itemData.getHasSaleItemNos().add(item_no);
                    itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                    itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                    itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                    itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                    itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
                    itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                    itemData.setTotal_bill_vip_cnt(itemData.getTotal_bill_vip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
                    itemData.setTotal_dis(itemData.getTotal_dis()+(total_dis!=null?total_dis:0));

                    dataMap.put(branch_no+"@@"+item_brandno,itemData);
                }
            }
        }else if(StringUtils.equals(type,"main_supcust")){

        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long days = 0;
        try {
            days = (sdf.parse(end).getTime() - sdf.parse(begin).getTime())/1000/60/60/24;
        } catch (Exception e) {}
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String[] key = entry.getKey().split("@@");
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("门店名称",key[0]);

            resultJson.put("分类",key[1]);
            resultJson.put("有销售品牌数_10",itemData.getHasSaleItemNos().size());
            resultJson.put("有销售SKU数_10",0);
            resultJson.put("销售金额_10",itemData.getTotal_sale());
            resultJson.put("销售数量_10",itemData.getSale_cnt());
            resultJson.put("毛利贡献_10",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("会员数_10",itemData.getTotal_bill_vip_cnt());
            resultJson.put("平均折扣_10",NumberUtil.getDouble2Format(itemData.getTotal_dis()/itemData.getSale_cnt()));
            resultJson.put("连带率_10",NumberUtil.getDouble2Format(itemData.getSale_cnt()/itemData.getTotal_bill()));
            resultJson.put("退货率_10",NumberUtil.getDouble2Format(itemData.getTotal_ret_cnt()/itemData.getSale_cnt()));
            resultJson.put("有库存品牌数_10",itemData.getHasStockItemNos().size());
            resultJson.put("有库存SKU数_10",0);
            resultJson.put("库存金额_10",itemData.getStock_sale());
            resultJson.put("库存数量_10",itemData.getStock_cnt());
            resultJson.put("可用天数_10",NumberUtil.getDouble2Format(itemData.getStock_cnt()/(itemData.getSale_cnt()/days)));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray itemSaleByClsno(String orgId,String branchNo, String clsNo,JSONArray priceZones, String begin, String end,boolean onlyHasStock) throws BaseException {
        String priceZoneSql = "";
        if(priceZones!=null&&priceZones.size()>0){
            if(priceZones.size()==1){
                JSONObject job = priceZones.getJSONObject(0);
                String low =job.getString("low");
                String high =job.getString("high");
                priceZoneSql = " and t3.sale_price>='"+low+"' and t3.sale_price<='"+high+"' ";
            }else{
                for(int i=0;i<priceZones.size();i++){
                    JSONObject job = priceZones.getJSONObject(i);
                    String low =job.getString("low");
                    String high =job.getString("high");
                    if(i==0){
                        priceZoneSql = " and ((t3.sale_price>='"+low+"' and t3.sale_price<='"+high+"') or ";
                    }else if(i==(priceZones.size()-1)){
                        priceZoneSql =priceZoneSql+ " ( t3.sale_price>='"+low+"' and t3.sale_price<='"+high+"'))";
                    }else{
                        priceZoneSql =priceZoneSql+ " ( t3.sale_price>='"+low+"' and t3.sale_price<='"+high+"') or";
                    }
                }
            }
        }
        class ItemData{
            String item_no;
            double sale_price;
            String item_name;
            double price;
            String branch_no;
            double sale_cnt;
            double total_sale;
            double total_cost;
            double total_bill;
            double total_ret;
            double total_dis;
            double total_ret_cnt;
            double total_bill_vip_cnt;
            double stock_cnt;
            double stock_sale;
            double stock_cost;

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getSale_price() {
                return sale_price;
            }

            public void setSale_price(double sale_price) {
                this.sale_price = sale_price;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public double getTotal_dis() {
                return total_dis;
            }

            public void setTotal_dis(double total_dis) {
                this.total_dis = total_dis;
            }

            public double getTotal_bill_vip_cnt() {
                return total_bill_vip_cnt;
            }

            public void setTotal_bill_vip_cnt(double total_bill_vip_cnt) {
                this.total_bill_vip_cnt = total_bill_vip_cnt;
            }


            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public String getBranch_no() {
                return branch_no;
            }

            public void setBranch_no(String branch_no) {
                this.branch_no = branch_no;
            }

            public double getSale_cnt() {
                return sale_cnt;
            }

            public void setSale_cnt(double sale_cnt) {
                this.sale_cnt = sale_cnt;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_bill() {
                return total_bill;
            }

            public void setTotal_bill(double total_bill) {
                this.total_bill = total_bill;
            }

            public double getTotal_ret() {
                return total_ret;
            }

            public void setTotal_ret(double total_ret) {
                this.total_ret = total_ret;
            }

            public double getTotal_ret_cnt() {
                return total_ret_cnt;
            }

            public void setTotal_ret_cnt(double total_ret_cnt) {
                this.total_ret_cnt = total_ret_cnt;
            }

            public double getStock_sale() {
                return stock_sale;
            }

            public void setStock_sale(double stock_sale) {
                this.stock_sale = stock_sale;
            }

            public double getStock_cost() {
                return stock_cost;
            }

            public void setStock_cost(double stock_cost) {
                this.stock_cost = stock_cost;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();
        List<Object> objectList = unionSqlDao.query(" SELECT t1.sale_amt,t1.sale_qty,t1.bill_cnt,t1.ret_amt,t1.ret_qty,t1.cost_amt,t1.dis_amt,t1.bill_vip_cnt,t1.item_no,\n" +
                " t2.stock_qty,t2.branch_no,t2.cost_price,t2.cost_amt, \n" +
                " t3.item_name,t3.sale_price,t3.price\n" +
                "  FROM ItemDayEntity t1,StockEntity t2,ItemEntity t3\n" +
                " where t1.item_no=t2.item_no and t3.item_no=t2.item_no" +
                " and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' and t1.org_id='"+orgId+"' and t2.branch_no='"+branchNo+"' and t3.item_clsno='"+clsNo+"'"+priceZoneSql);
        for(Object itemEntity: objectList){
            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[0];
            Double total_sale_cnt = (Double)itemEntityObj[1];
            Long bill_cnt = (Long)itemEntityObj[2];
            Double total_ret = (Double)itemEntityObj[3];
            Double ret_cnt = (Double)itemEntityObj[4];
            Double total_cost = (Double)itemEntityObj[5];
            Double total_dis = (Double)itemEntityObj[6];
            Long bill_vip_cnt = (Long)itemEntityObj[7];
            String item_no = (String)itemEntityObj[8];
            Double stock_cnt = (Double)itemEntityObj[9];
            String branch_no = (String)itemEntityObj[10];
            Double cost_price = (Double)itemEntityObj[11];
            Double cost_amt = (Double)itemEntityObj[12];
            String item_name = (String)itemEntityObj[13];
            Double sale_price = (Double)itemEntityObj[14];
            Double price = (Double)itemEntityObj[14];

            if(onlyHasStock){
                if(stock_cnt!=null&&stock_cnt<=0){
                    continue;
                }
            }

            ItemData itemData = dataMap.get(item_no);
            if(itemData!=null){

                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
                itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                itemData.setTotal_bill_vip_cnt(itemData.getTotal_bill_vip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
                itemData.setTotal_dis(itemData.getTotal_dis()+(total_dis!=null?total_dis:0));
            }else{
                itemData = new ItemData();

                itemData.setBranch_no(branch_no);
                itemData.setStock_cnt((stock_cnt!=null?stock_cnt:0));
                itemData.setStock_cost(cost_price!=null?cost_price:0);
                itemData.setStock_sale(cost_amt!=null?cost_amt:0);
                itemData.setItem_name(item_name);
                itemData.setPrice(price!=null?price:0);
                itemData.setSale_price(sale_price!=null?sale_price:0);

                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                itemData.setTotal_ret(itemData.getTotal_ret()+(total_ret!=null?total_ret:0));
                itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                itemData.setTotal_bill_vip_cnt(itemData.getTotal_bill_vip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
                itemData.setTotal_dis(itemData.getTotal_dis()+(total_dis!=null?total_dis:0));

                dataMap.put(item_no,itemData);
            }
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        long days = 0;
        try {
            days = (sdf.parse(end).getTime() - sdf.parse(begin).getTime())/1000/60/60/24;
        } catch (Exception e) {}
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("item_no",key);
            resultJson.put("条码",key);
            resultJson.put("货号",key);
            resultJson.put("商品名称",itemData.getItem_name());
            resultJson.put("零售价",itemData.getSale_price());
            resultJson.put("成本价",itemData.getPrice());
            resultJson.put("销售金额",itemData.getSale_price());
            resultJson.put("销售数量",itemData.getSale_cnt());
            resultJson.put("毛利贡献",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("会员数",itemData.getTotal_bill_vip_cnt());
            resultJson.put("原价金额",itemData.getSale_cnt()*itemData.getSale_price());
            resultJson.put("平均折扣",NumberUtil.getDouble2Format(itemData.getTotal_dis()/itemData.getTotal_sale()));
            resultJson.put("连带率",NumberUtil.getDouble2Format(itemData.getSale_cnt()/itemData.getTotal_bill()));
            resultJson.put("退货率",0);
            resultJson.put("库存金额",itemData.getStock_sale());
            resultJson.put("库存数量",itemData.getStock_cnt());
            resultJson.put("可用天数",NumberUtil.getDouble2Format(itemData.getStock_cnt()/(itemData.getSale_cnt()/days)));
            results.add(resultJson);
        }
        return results;
    }

}
