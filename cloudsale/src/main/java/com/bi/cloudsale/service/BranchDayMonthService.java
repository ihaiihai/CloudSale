package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/15 17:45
 * @Version: 1.0
 **/
public interface BranchDayMonthService {
    JSONArray branches(String orgId) throws BaseException;
    JSONArray todayLowestTarget(String orgId,String branchNo) throws BaseException;
    JSONArray monthGain(String orgId,String branchNo) throws BaseException;
    JSONArray  dayMonthBranchGainAbstract(String orgId,String branchNo) throws BaseException;
    JSONArray  yesterDayBranchGain(String orgId,String branchNo) throws BaseException;
    JSONArray  yesterdayBranchBestItem(String orgId,String branchNo) throws BaseException;
    JSONArray  yesterdayPeriod(String orgId,String branchNo) throws BaseException;
    JSONArray  yesterdayCls(String orgId,String branchNo) throws BaseException;
    JSONArray  yesterdayPriceZone(String orgId,String branchNo) throws BaseException;
    JSONArray  yesterdayBranchBadItem(String orgId,String branchNo) throws BaseException;
    JSONArray  yesterdayBranchExceptionStockItem(String orgId,String branchNo) throws BaseException;
    JSONArray  monthBranchTargetView(String orgId,String branchNo,String beginDate,String endDate) throws BaseException;
    JSONArray  monthBranchVipView(String orgId,String branchNo,String beginDate,String endDate) throws BaseException;
    JSONArray monthBranchSaleWanchenglv(String orgId,String branchNo,String beginDate,String endDate) throws BaseException;
    JSONArray monthBranchSaleBodong(String orgId,String branchNo,String beginDate,String endDate) throws BaseException;
    JSONArray monthBranchSaleFenbu(String orgId,String beginDate,String endDate) throws BaseException;
    JSONArray monthBranchClsFenbu(String orgId,String branchNo,String beginDate,String endDate) throws BaseException;
    JSONArray monthBranchSaleTongbiHuanbiZhanbi(String orgId,String beginDate,String endDate) throws BaseException;
}
