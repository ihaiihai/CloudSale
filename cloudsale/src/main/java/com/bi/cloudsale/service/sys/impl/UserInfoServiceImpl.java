package com.bi.cloudsale.service.sys.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.RemoteInfoDto;
import com.bi.cloudsale.dto.sys.UserInfoDto;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.sys.UserInfoService;

@Service
@Transactional
public class UserInfoServiceImpl implements UserInfoService {
	
	private static final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);

	@Resource
	private UserInfoDao userInfoDao;
	
	@Override
	public UserInfoDto queryByAccount(String account) throws BaseException {
		UserInfoEntity entity = userInfoDao.queryByAccount(account);
		if(entity != null) {
			UserInfoDto dto = new UserInfoDto();
			BeanUtil.copyProperties(entity, dto);
			return dto;
		}else {
			return null;
		}
	}

	@Override
	public UserInfoDto queryByMobile(String mobile) throws BaseException {
		List<UserInfoEntity> entities = userInfoDao.queryByMobile(mobile);
		if(CollectionUtils.isNotEmpty(entities)&&entities.size()==1) {
			UserInfoDto dto = new UserInfoDto();
			BeanUtil.copyProperties(entities.get(0), dto);
			return dto;
		}else {
			return null;
		}
	}

	@Override
	public void createUser(UserInfoDto userInfoDto) throws BaseException {
		UserInfoEntity hasEntity = userInfoDao.queryByAccount(userInfoDto.getAccount());
		if(hasEntity != null) {
			throw new BaseException("101", "已存在相同的账号");
		}
		UserInfoEntity entity = new UserInfoEntity();
		BeanUtil.copyProperties(userInfoDto, entity);
		entity.setUserId(UUID.randomUUID().toString().replaceAll("-", ""));
		if(entity.getAccountType() == null) {
			entity.setAccountType(0);
		}
		if(StringUtils.isNotEmpty(entity.getOrganizeName())) {
			entity.setOrganizeId(UUID.randomUUID().toString().replaceAll("-", ""));
		}
		UserInfoEntity parentUserInfo = userInfoDao.queryByUserId(userInfoDto.getParentUserId());
		if(parentUserInfo == null) {
			throw new BaseException("102", "无新建账号权限");
		}
		if(parentUserInfo.getAccountType() == 2) {
			//系统管理员新建的账号不继承权限
		}else {
			entity.setOrganizeId(parentUserInfo.getOrganizeId());
			entity.setOrganizeName(parentUserInfo.getOrganizeName());
			entity.setAuthTime(parentUserInfo.getAuthTime());
			entity.setAuthUserCount(parentUserInfo.getAuthUserCount());
		}
		try {
			entity.setGmtCreate(new Date());
			entity.setGmtModified(new Date());
			userInfoDao.createUser(entity);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public void deleteUser(String userId) throws BaseException {
		try {
			userInfoDao.deleteUser(userId);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public void updateUser(UserInfoDto userInfoDto) throws BaseException {
		try {
			UserInfoEntity entity = userInfoDao.queryByUserId(userInfoDto.getUserId());
			BeanUtil.copyProperties(userInfoDto, entity);
			entity.setGmtModified(new Date());
			userInfoDao.updateUser(entity);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public List<UserInfoDto> queryByCondition(BaseQueryCondition baseQueryCondition) {
		List<UserInfoDto> dtos = new ArrayList<UserInfoDto>();
		List<UserInfoEntity> entitys = userInfoDao.queryByCondition(baseQueryCondition);
		if(entitys != null && entitys.size() > 0) {
			Iterator<UserInfoEntity> iter = entitys.iterator();
			while(iter.hasNext()) {
				UserInfoEntity userInfoEntity = iter.next();
				UserInfoDto userInfoDto = new UserInfoDto();
				BeanUtil.copyProperties(userInfoEntity, userInfoDto);
				userInfoDto.setPassword("");
				dtos.add(userInfoDto);
			}
		}
		return dtos;
	}

	@Override
	public UserInfoDto queryByUserId(String userId) {
		UserInfoEntity entity = userInfoDao.queryByUserId(userId);
		if(entity != null) {
			UserInfoDto dto = new UserInfoDto();
			BeanUtil.copyProperties(entity, dto);
			return dto;
		}else {
			return null;
		}
	}

	@Override
	public List<UserInfoDto> queryByOrgId(String orgId) {
		List<UserInfoDto> dtos = new ArrayList<UserInfoDto>();
		List<UserInfoEntity> entitys = userInfoDao.queryByOrgId(orgId);
		if(entitys != null && entitys.size() > 0) {
			Iterator<UserInfoEntity> iter = entitys.iterator();
			while(iter.hasNext()) {
				UserInfoEntity userInfoEntity = iter.next();
				UserInfoDto userInfoDto = new UserInfoDto();
				BeanUtil.copyProperties(userInfoEntity, userInfoDto);
				userInfoDto.setPassword("");
				dtos.add(userInfoDto);
			}
		}
		return dtos;
	}

	@Override
	public void updateRemoteInfo(RemoteInfoDto dto) throws BaseException {
		try {
			userInfoDao.updateRemoteInfo(dto);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}
}
