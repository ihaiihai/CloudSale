package com.bi.cloudsale.service;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.dto.deductrule.DeductRuleDto;
import com.bi.cloudsale.dto.deductrule.DeductRuleMasterDto;

import java.util.List;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/15 17:45
 * @Version: 1.0
 **/
public interface DeductRuleService {

    List<DeductRuleMasterDto> queryRules(String start, String end, String branchNos, String ruleName, Long isEnable,String orgId, String gmt_modify) throws BaseException;
    DeductRuleDto ruleDetail(String deductNo) throws BaseException;;
    String setEnable(String deductNo, Long isEnable,String userId) throws BaseException;
    void deleteRule(String deductNo, String userId) throws BaseException;
    void saveOrUpdateRule(String orgId,String userId,DeductRuleDto deductRuleDto) throws BaseException;
}
