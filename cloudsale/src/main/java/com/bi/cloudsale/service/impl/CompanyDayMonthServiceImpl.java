package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.BranchDayMonthService;
import com.bi.cloudsale.service.CompanyDayMonthService;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class CompanyDayMonthServiceImpl implements CompanyDayMonthService {

    private static final Logger log = LoggerFactory.getLogger(CompanyDayMonthServiceImpl.class);

    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private BrandDao brandDao;
    @Resource
    private ItemDayDao itemDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private VipDayDao vipDayDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private SalerDayDao salerDayDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray clsFenbu(String orgId) throws BaseException {

        class ItemData{
            double tatal_sale;
            String cls_name;

            public double getTatal_sale() {
                return tatal_sale;
            }

            public void setTatal_sale(double tatal_sale) {
                this.tatal_sale = tatal_sale;
            }

            public String getCls_name() {
                return cls_name;
            }

            public void setCls_name(String cls_name) {
                this.cls_name = cls_name;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();
        //1分类销售额
//        List<ClsDayEntity>  clsDayEntities = clsDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"'");
        List<Object> objects = unionSqlDao.query("select item_clsno, sum(sale_amt) from ClsDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1) group by item_clsno");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String clsno = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(clsno);
            if(itemData!=null){
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(clsno,itemData);
            }
        }

        //2对应分类名
        String cls_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(cls_nosStr)){
                cls_nosStr = cls_nosStr + ",'"+str+"'";
            }else{
                cls_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(cls_nosStr)){
            List<ItemClsEntity> itemClsEntities = itemClsDao.list("where item_clsno in("+cls_nosStr+") and org_id = '"+orgId+"'");
            for(ItemClsEntity itemClsEntity : itemClsEntities){
                dataMap.get(itemClsEntity.getItem_clsno()).setCls_name(itemClsEntity.getItem_clsname());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",itemData.getCls_name());
            resultJson.put("value",itemData.getTatal_sale());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray clsFenbuDetail(String orgId, String itemClsName) throws BaseException {
        class ItemData{
            double tatal_sale;
            String brand_name;

            public double getTatal_sale() {
                return tatal_sale;
            }

            public void setTatal_sale(double tatal_sale) {
                this.tatal_sale = tatal_sale;
            }

            public String getBrand_name() {
                return brand_name;
            }

            public void setBrand_name(String brand_name) {
                this.brand_name = brand_name;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();
        //1分类销售额
//        List<ClsDayEntity>  clsDayEntities = clsDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"'");
        List<Object> objects = unionSqlDao.query("select t.item_brandno, sum(t.sale_amt) from BrandDayEntity t,ItemClsEntity p,ItemEntity h where t.org_id ='"+orgId+"'" +
                " and t.item_brandno=h.item_brandno and p.item_clsno = h.item_clsno " +
                " and p.item_clsname = '"+itemClsName+"'" +
                " and to_days(t.oper_date) = (to_days(now())-1) group by t.item_brandno");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_brandno = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(item_brandno);
            if(itemData!=null){
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(item_brandno,itemData);
            }
        }

        //2对应品牌名
        String brand_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(brand_nosStr)){
                brand_nosStr = brand_nosStr + ",'"+str+"'";
            }else{
                brand_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(brand_nosStr)){
            List<BrandEntity> brandEntities = brandDao.list("where item_brandno in("+brand_nosStr+") and org_id = '"+orgId+"'");
            for(BrandEntity brandEntity : brandEntities){
                dataMap.get(brandEntity.getItem_brandno()).setBrand_name(brandEntity.getItem_brandname());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",itemData.getBrand_name());
            resultJson.put("value",itemData.getTatal_sale());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray smallTitle(String orgId) throws BaseException {


        Double bill_cnt =0d;
        Double sale = 0d;
        Double bill_cnt_last_year = 0d;
        Double sale_last_year = 0d;
        // 昨天 订单数、销售额
//        List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"'");
        List<Object> objects = unionSqlDao.query("select sum(bill_cnt), sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            Long total_bill_cnt = (Long)itemEntityObj[0];
            Double total_sale = (Double)itemEntityObj[1];
            bill_cnt +=total_bill_cnt!=null?total_bill_cnt:0;
            sale +=total_sale!=null?total_sale:0;
        }
        // 去年昨天 订单数、销售额
//        List<BranchDayEntity> branchDayEntities_last_year = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1-365)" +
//                " and org_id = '"+orgId+"'");
        List<Object> objects_last_year = unionSqlDao.query("select sum(bill_cnt), sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1-365)");
        for(Object object : objects_last_year){
            Object[]itemEntityObj = (Object[])object;
            Long total_bill_cnt = (Long)itemEntityObj[0];
            Double total_sale = (Double)itemEntityObj[1];
            bill_cnt_last_year +=total_bill_cnt!=null?total_bill_cnt:0;
            sale_last_year +=total_sale!=null?total_sale:0;
        }
        JSONArray results = new JSONArray();
        JSONObject resultJson = new JSONObject();
        resultJson.put("标题2",String.format("昨日整体客流【%s人】,同比去年上涨%s,客单价【%s元/人】,同比去年上涨%s",
                bill_cnt,
                NumberUtil.getDouble2Format((bill_cnt-bill_cnt_last_year)/bill_cnt_last_year),
                NumberUtil.getDouble2Format(sale/bill_cnt),
                NumberUtil.getDouble2Format((sale/bill_cnt-sale_last_year/bill_cnt_last_year)/(sale_last_year/bill_cnt_last_year))));
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray shijianduan(String orgId) throws BaseException {
        class ItemData{
            double total_sale;
            double total_sale_sametime;
            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_sale_sametime() {
                return total_sale_sametime;
            }

            public void setTotal_sale_sametime(double total_sale_sametime) {
                this.total_sale_sametime = total_sale_sametime;
            }
        }
        Map<Long,ItemData> dataMap = new HashMap<>();
        //1销量、销售额
        List<BranchTimeEntity> branchTimeEntities = branchTimeDao.list("where to_days(oper_date) = (to_days(now())-1)" +
                " and org_id = '"+orgId+"'");
        for(BranchTimeEntity branchTimeEntity:branchTimeEntities){
            Long hour00 = 0L;
            ItemData dataDay00 = dataMap.get(hour00);
            if(dataDay00!=null){
                dataDay00.setTotal_sale(dataDay00.getTotal_sale() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
            }else{
                dataDay00 = new ItemData();
                dataDay00.setTotal_sale(dataDay00.getTotal_sale() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataMap.put(hour00,dataDay00);
            }
            Long hour01 = 1L;
            ItemData dataDay01 = dataMap.get(hour01);
            if(dataDay01!=null){
                dataDay01.setTotal_sale(dataDay01.getTotal_sale() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
            }else{
                dataDay01 = new ItemData();
                dataDay01.setTotal_sale(dataDay01.getTotal_sale() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataMap.put(hour01,dataDay01);
            }
            Long hour02 = 2L;
            ItemData dataDay02 = dataMap.get(hour02);
            if(dataDay02!=null){
                dataDay02.setTotal_sale(dataDay02.getTotal_sale() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
            }else{
                dataDay02 = new ItemData();
                dataDay02.setTotal_sale(dataDay02.getTotal_sale() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataMap.put(hour02,dataDay02);
            }
            Long hour03 = 3L;
            ItemData dataDay03 = dataMap.get(hour03);
            if(dataDay03!=null){
                dataDay03.setTotal_sale(dataDay03.getTotal_sale() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
            }else{
                dataDay03 = new ItemData();
                dataDay03.setTotal_sale(dataDay03.getTotal_sale() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataMap.put(hour03,dataDay03);
            }
            Long hour04 = 4L;
            ItemData dataDay04 = dataMap.get(hour04);
            if(dataDay04!=null){
                dataDay04.setTotal_sale(dataDay04.getTotal_sale() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
            }else{
                dataDay04 = new ItemData();
                dataDay04.setTotal_sale(dataDay04.getTotal_sale() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataMap.put(hour04,dataDay04);
            }
            Long hour05 = 5L;
            ItemData dataDay05 = dataMap.get(hour05);
            if(dataDay05!=null){
                dataDay05.setTotal_sale(dataDay05.getTotal_sale() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
            }else{
                dataDay05 = new ItemData();
                dataDay05.setTotal_sale(dataDay05.getTotal_sale() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataMap.put(hour05,dataDay05);
            }
            Long hour06 = 6L;
            ItemData dataDay06 = dataMap.get(hour06);
            if(dataDay06!=null){
                dataDay06.setTotal_sale(dataDay06.getTotal_sale() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
            }else{
                dataDay06 = new ItemData();
                dataDay06.setTotal_sale(dataDay06.getTotal_sale() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataMap.put(hour06,dataDay06);
            }
            Long hour07 = 7L;
            ItemData dataDay07 = dataMap.get(hour07);
            if(dataDay07!=null){
                dataDay07.setTotal_sale(dataDay07.getTotal_sale() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
            }else{
                dataDay07 = new ItemData();
                dataDay07.setTotal_sale(dataDay07.getTotal_sale() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataMap.put(hour07,dataDay07);
            }
            Long hour08 = 8L;
            ItemData dataDay08 = dataMap.get(hour08);
            if(dataDay08!=null){
                dataDay08.setTotal_sale(dataDay08.getTotal_sale() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
            }else{
                dataDay08 = new ItemData();
                dataDay08.setTotal_sale(dataDay08.getTotal_sale() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataMap.put(hour08,dataDay08);
            }
            Long hour09 = 9L;
            ItemData dataDay09 = dataMap.get(hour09);
            if(dataDay09!=null){
                dataDay09.setTotal_sale(dataDay09.getTotal_sale() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
            }else{
                dataDay09 = new ItemData();
                dataDay09.setTotal_sale(dataDay09.getTotal_sale() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataMap.put(hour09,dataDay09);
            }
            Long hour10 = 10L;
            ItemData dataDay10 = dataMap.get(hour10);
            if(dataDay10!=null){
                dataDay10.setTotal_sale(dataDay10.getTotal_sale() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
            }else{
                dataDay10 = new ItemData();
                dataDay10.setTotal_sale(dataDay10.getTotal_sale() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataMap.put(hour10,dataDay10);
            }
            Long hour11 = 11L;
            ItemData dataDay11 = dataMap.get(hour11);
            if(dataDay11!=null){
                dataDay11.setTotal_sale(dataDay11.getTotal_sale() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
            }else{
                dataDay11 = new ItemData();
                dataDay11.setTotal_sale(dataDay11.getTotal_sale() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataMap.put(hour11,dataDay11);
            }
            Long hour12 = 12L;
            ItemData dataDay12 = dataMap.get(hour12);
            if(dataDay12!=null){
                dataDay12.setTotal_sale(dataDay12.getTotal_sale() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
            }else{
                dataDay12 = new ItemData();
                dataDay12.setTotal_sale(dataDay12.getTotal_sale() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataMap.put(hour12,dataDay12);
            }
            Long hour13 = 13L;
            ItemData dataDay13 = dataMap.get(hour13);
            if(dataDay13!=null){
                dataDay13.setTotal_sale(dataDay13.getTotal_sale() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
            }else{
                dataDay13 = new ItemData();
                dataDay13.setTotal_sale(dataDay13.getTotal_sale() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataMap.put(hour13,dataDay13);
            }
            Long hour14 = 14L;
            ItemData dataDay14 = dataMap.get(hour14);
            if(dataDay14!=null){
                dataDay14.setTotal_sale(dataDay14.getTotal_sale() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
            }else{
                dataDay14 = new ItemData();
                dataDay14.setTotal_sale(dataDay14.getTotal_sale() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataMap.put(hour14,dataDay14);
            }
            Long hour15 = 15L;
            ItemData dataDay15 = dataMap.get(hour15);
            if(dataDay15!=null){
                dataDay15.setTotal_sale(dataDay15.getTotal_sale() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
            }else{
                dataDay15 = new ItemData();
                dataDay15.setTotal_sale(dataDay15.getTotal_sale() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataMap.put(hour15,dataDay15);
            }
            Long hour16 = 16L;
            ItemData dataDay16 = dataMap.get(hour16);
            if(dataDay16!=null){
                dataDay16.setTotal_sale(dataDay16.getTotal_sale() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
            }else{
                dataDay16 = new ItemData();
                dataDay16.setTotal_sale(dataDay16.getTotal_sale() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataMap.put(hour16,dataDay16);
            }
            Long hour17 = 17L;
            ItemData dataDay17 = dataMap.get(hour17);
            if(dataDay17!=null){
                dataDay17.setTotal_sale(dataDay17.getTotal_sale() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
            }else{
                dataDay17 = new ItemData();
                dataDay17.setTotal_sale(dataDay17.getTotal_sale() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataMap.put(hour17,dataDay17);
            }
            Long hour18 =18L;
            ItemData dataDay18 = dataMap.get(hour18);
            if(dataDay18!=null){
                dataDay18.setTotal_sale(dataDay18.getTotal_sale() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
            }else{
                dataDay18 = new ItemData();
                dataDay18.setTotal_sale(dataDay18.getTotal_sale() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataMap.put(hour18,dataDay18);
            }
            Long hour19 = 19L;
            ItemData dataDay19 = dataMap.get(hour19);
            if(dataDay19!=null){
                dataDay19.setTotal_sale(dataDay19.getTotal_sale() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
            }else{
                dataDay19 = new ItemData();
                dataDay19.setTotal_sale(dataDay19.getTotal_sale() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataMap.put(hour19,dataDay19);
            }
            Long hour20 = 20L;
            ItemData dataDay20 = dataMap.get(hour20);
            if(dataDay20!=null){
                dataDay20.setTotal_sale(dataDay20.getTotal_sale() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
            }else{
                dataDay20 = new ItemData();
                dataDay20.setTotal_sale(dataDay20.getTotal_sale() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataMap.put(hour20,dataDay20);
            }
            Long hour21 = 21L;
            ItemData dataDay21 = dataMap.get(hour21);
            if(dataDay21!=null){
                dataDay21.setTotal_sale(dataDay21.getTotal_sale() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
            }else{
                dataDay21 = new ItemData();
                dataDay21.setTotal_sale(dataDay21.getTotal_sale() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataMap.put(hour21,dataDay21);
            }
            Long hour22 = 22L;
            ItemData dataDay22 = dataMap.get(hour22);
            if(dataDay22!=null){
                dataDay22.setTotal_sale(dataDay22.getTotal_sale() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
            }else{
                dataDay22 = new ItemData();
                dataDay22.setTotal_sale(dataDay22.getTotal_sale() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataMap.put(hour22,dataDay22);
            }
            Long hour23 = 23L;
            ItemData dataDay23 = dataMap.get(hour23);
            if(dataDay23!=null){
                dataDay23.setTotal_sale(dataDay23.getTotal_sale() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
            }else{
                dataDay23 = new ItemData();
                dataDay23.setTotal_sale(dataDay23.getTotal_sale() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataMap.put(hour23,dataDay23);
            }
        }
        List<BranchTimeEntity> branchTimeEntities_sametime = branchTimeDao.list("where to_days(oper_date) = (to_days(now())-1-365)" +
                " and org_id = '"+orgId+"'");
        for(BranchTimeEntity branchTimeEntity:branchTimeEntities_sametime){
            Long hour00 = 0L;
            ItemData dataDay00 = dataMap.get(hour00);
            if(dataDay00!=null){
                dataDay00.setTotal_sale_sametime(dataDay00.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
            }else{
                dataDay00 = new ItemData();
                dataDay00.setTotal_sale_sametime(dataDay00.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataMap.put(hour00,dataDay00);
            }
            Long hour01 = 1L;
            ItemData dataDay01 = dataMap.get(hour01);
            if(dataDay01!=null){
                dataDay01.setTotal_sale_sametime(dataDay01.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
            }else{
                dataDay01 = new ItemData();
                dataDay01.setTotal_sale_sametime(dataDay01.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataMap.put(hour01,dataDay01);
            }
            Long hour02 = 2L;
            ItemData dataDay02 = dataMap.get(hour02);
            if(dataDay02!=null){
                dataDay02.setTotal_sale_sametime(dataDay02.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
            }else{
                dataDay02 = new ItemData();
                dataDay02.setTotal_sale_sametime(dataDay02.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataMap.put(hour02,dataDay02);
            }
            Long hour03 = 3L;
            ItemData dataDay03 = dataMap.get(hour03);
            if(dataDay03!=null){
                dataDay03.setTotal_sale_sametime(dataDay03.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
            }else{
                dataDay03 = new ItemData();
                dataDay03.setTotal_sale_sametime(dataDay03.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataMap.put(hour03,dataDay03);
            }
            Long hour04 = 4L;
            ItemData dataDay04 = dataMap.get(hour04);
            if(dataDay04!=null){
                dataDay04.setTotal_sale_sametime(dataDay04.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
            }else{
                dataDay04 = new ItemData();
                dataDay04.setTotal_sale_sametime(dataDay04.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataMap.put(hour04,dataDay04);
            }
            Long hour05 = 5L;
            ItemData dataDay05 = dataMap.get(hour05);
            if(dataDay05!=null){
                dataDay05.setTotal_sale_sametime(dataDay05.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
            }else{
                dataDay05 = new ItemData();
                dataDay05.setTotal_sale_sametime(dataDay05.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataMap.put(hour05,dataDay05);
            }
            Long hour06 = 6L;
            ItemData dataDay06 = dataMap.get(hour06);
            if(dataDay06!=null){
                dataDay06.setTotal_sale_sametime(dataDay06.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
            }else{
                dataDay06 = new ItemData();
                dataDay06.setTotal_sale_sametime(dataDay06.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataMap.put(hour06,dataDay06);
            }
            Long hour07 = 7L;
            ItemData dataDay07 = dataMap.get(hour07);
            if(dataDay07!=null){
                dataDay07.setTotal_sale_sametime(dataDay07.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
            }else{
                dataDay07 = new ItemData();
                dataDay07.setTotal_sale_sametime(dataDay07.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataMap.put(hour07,dataDay07);
            }
            Long hour08 = 8L;
            ItemData dataDay08 = dataMap.get(hour08);
            if(dataDay08!=null){
                dataDay08.setTotal_sale_sametime(dataDay08.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
            }else{
                dataDay08 = new ItemData();
                dataDay08.setTotal_sale_sametime(dataDay08.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataMap.put(hour08,dataDay08);
            }
            Long hour09 = 9L;
            ItemData dataDay09 = dataMap.get(hour09);
            if(dataDay09!=null){
                dataDay09.setTotal_sale_sametime(dataDay09.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
            }else{
                dataDay09 = new ItemData();
                dataDay09.setTotal_sale_sametime(dataDay09.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataMap.put(hour09,dataDay09);
            }
            Long hour10 = 10L;
            ItemData dataDay10 = dataMap.get(hour10);
            if(dataDay10!=null){
                dataDay10.setTotal_sale_sametime(dataDay10.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
            }else{
                dataDay10 = new ItemData();
                dataDay10.setTotal_sale_sametime(dataDay10.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataMap.put(hour10,dataDay10);
            }
            Long hour11 = 11L;
            ItemData dataDay11 = dataMap.get(hour11);
            if(dataDay11!=null){
                dataDay11.setTotal_sale_sametime(dataDay11.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
            }else{
                dataDay11 = new ItemData();
                dataDay11.setTotal_sale_sametime(dataDay11.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataMap.put(hour11,dataDay11);
            }
            Long hour12 = 12L;
            ItemData dataDay12 = dataMap.get(hour12);
            if(dataDay12!=null){
                dataDay12.setTotal_sale_sametime(dataDay12.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
            }else{
                dataDay12 = new ItemData();
                dataDay12.setTotal_sale_sametime(dataDay12.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataMap.put(hour12,dataDay12);
            }
            Long hour13 = 13L;
            ItemData dataDay13 = dataMap.get(hour13);
            if(dataDay13!=null){
                dataDay13.setTotal_sale_sametime(dataDay13.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
            }else{
                dataDay13 = new ItemData();
                dataDay13.setTotal_sale_sametime(dataDay13.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataMap.put(hour13,dataDay13);
            }
            Long hour14 = 14L;
            ItemData dataDay14 = dataMap.get(hour14);
            if(dataDay14!=null){
                dataDay14.setTotal_sale_sametime(dataDay14.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
            }else{
                dataDay14 = new ItemData();
                dataDay14.setTotal_sale_sametime(dataDay14.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataMap.put(hour14,dataDay14);
            }
            Long hour15 = 15L;
            ItemData dataDay15 = dataMap.get(hour15);
            if(dataDay15!=null){
                dataDay15.setTotal_sale_sametime(dataDay15.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
            }else{
                dataDay15 = new ItemData();
                dataDay15.setTotal_sale_sametime(dataDay15.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataMap.put(hour15,dataDay15);
            }
            Long hour16 = 16L;
            ItemData dataDay16 = dataMap.get(hour16);
            if(dataDay16!=null){
                dataDay16.setTotal_sale_sametime(dataDay16.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
            }else{
                dataDay16 = new ItemData();
                dataDay16.setTotal_sale_sametime(dataDay16.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataMap.put(hour16,dataDay16);
            }
            Long hour17 = 17L;
            ItemData dataDay17 = dataMap.get(hour17);
            if(dataDay17!=null){
                dataDay17.setTotal_sale_sametime(dataDay17.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
            }else{
                dataDay17 = new ItemData();
                dataDay17.setTotal_sale_sametime(dataDay17.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataMap.put(hour17,dataDay17);
            }
            Long hour18 =18L;
            ItemData dataDay18 = dataMap.get(hour18);
            if(dataDay18!=null){
                dataDay18.setTotal_sale_sametime(dataDay18.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
            }else{
                dataDay18 = new ItemData();
                dataDay18.setTotal_sale_sametime(dataDay18.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataMap.put(hour18,dataDay18);
            }
            Long hour19 = 19L;
            ItemData dataDay19 = dataMap.get(hour19);
            if(dataDay19!=null){
                dataDay19.setTotal_sale_sametime(dataDay19.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
            }else{
                dataDay19 = new ItemData();
                dataDay19.setTotal_sale_sametime(dataDay19.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataMap.put(hour19,dataDay19);
            }
            Long hour20 = 20L;
            ItemData dataDay20 = dataMap.get(hour20);
            if(dataDay20!=null){
                dataDay20.setTotal_sale_sametime(dataDay20.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
            }else{
                dataDay20 = new ItemData();
                dataDay20.setTotal_sale_sametime(dataDay20.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataMap.put(hour20,dataDay20);
            }
            Long hour21 = 21L;
            ItemData dataDay21 = dataMap.get(hour21);
            if(dataDay21!=null){
                dataDay21.setTotal_sale_sametime(dataDay21.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
            }else{
                dataDay21 = new ItemData();
                dataDay21.setTotal_sale_sametime(dataDay21.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataMap.put(hour21,dataDay21);
            }
            Long hour22 = 22L;
            ItemData dataDay22 = dataMap.get(hour22);
            if(dataDay22!=null){
                dataDay22.setTotal_sale_sametime(dataDay22.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
            }else{
                dataDay22 = new ItemData();
                dataDay22.setTotal_sale_sametime(dataDay22.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataMap.put(hour22,dataDay22);
            }
            Long hour23 = 23L;
            ItemData dataDay23 = dataMap.get(hour23);
            if(dataDay23!=null){
                dataDay23.setTotal_sale_sametime(dataDay23.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
            }else{
                dataDay23 = new ItemData();
                dataDay23.setTotal_sale_sametime(dataDay23.getTotal_sale_sametime() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataMap.put(hour23,dataDay23);
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<Long, ItemData> entry : dataMap.entrySet()) {
            Long key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("时间",key);
            resultJson.put("昨日时段销售额",itemData.getTotal_sale());
            resultJson.put("同期时段销售额",itemData.getTotal_sale_sametime());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray retDisDanjiaLiandai(String orgId) throws BaseException {


        Double sale_cnt =0d;
        Double bill_cnt =0d;
        Double sale = 0d;
        Double dis = 0d;
        Double ret_cnt = 0d;

        // 昨天 订单数、销售额
//        List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"'");
        List<Object> objects = unionSqlDao.query("select sum(sale_qty),sum(bill_cnt), sum(sale_amt),sum(ret_qty),sum(dis_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            Double sale_qty = (Double)itemEntityObj[0];
            Long bill_cnt_total = (Long)itemEntityObj[1];
            Double sale_amt = (Double)itemEntityObj[2];
            Double ret_qty = (Double)itemEntityObj[3];
            Double dis_amt = (Double)itemEntityObj[4];

            sale_cnt += sale_qty!=null?sale_qty:0;
            bill_cnt +=bill_cnt_total!=null?bill_cnt_total:0;
            sale +=sale_amt!=null?sale_amt:0;
            ret_cnt+= ret_qty!=null?ret_qty:0;
            dis += dis_amt!=null?dis_amt:0;
        }
        JSONArray results = new JSONArray();
        JSONObject resultJson = new JSONObject();
        resultJson.put("平均退货率",NumberUtil.getDouble2Format(ret_cnt/sale_cnt*100));
        resultJson.put("平均折扣率",NumberUtil.getDouble2Format(dis/sale*100));
        resultJson.put("客单价",NumberUtil.getDouble2Format(sale/bill_cnt*100));
        resultJson.put("连带率",NumberUtil.getDouble2Format(sale_cnt/bill_cnt*100));
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray periodSale(String orgId) throws BaseException {
        return null;
    }

    @Override
    public JSONArray yesMonthWancheng(String orgId) throws BaseException {


        Double sale = 0d;
        // 昨天 销售额
//        List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"'");
        List<Object> objects = unionSqlDao.query("select sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)");
        for(Object object : objects){
            Double itemEntityObj = (Double)object;
            sale +=itemEntityObj!=null?itemEntityObj:0;
        }
        // 昨整月 销售额
        Calendar cal_pre= Calendar.getInstance();//本月首日
        cal_pre.add(Calendar.MONTH, 0);
        cal_pre.set(Calendar.DAY_OF_MONTH,1);
        String dateNo1 = DateUtil.formatDate(cal_pre.getTime(),DateUtil.SIMPLE_DATE_FORMAT);
//        List<BranchDayEntity>branchDayEntities_month= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+dateNo1+"' and to_days(oper_date) <= (to_days(now()))");
        double monthTotalSale = 0d;
        List<Object> objects_month = unionSqlDao.query("select sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+dateNo1+"' and to_days(oper_date) <= (to_days(now()))");
        for(Object object : objects_month){
            Double itemEntityObj = (Double)object;
            monthTotalSale +=itemEntityObj!=null?itemEntityObj:0;
        }
        double monthAim = 0d;
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 0 and cycle_year= DATE_FORMAT(NOW(), '%Y')");
        for(AimEntity aimEntity: aimEntities){
            Double aim_value = aimEntity.getAim_value();
            monthAim +=(aim_value!=null?aim_value:0)/12;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("昨日销售",sale);
        jsonObject.put("月累计销售",monthTotalSale);
        jsonObject.put("完成率",NumberUtil.getDouble2Format(monthTotalSale/monthAim*100));

        JSONArray results = new JSONArray();
        results.add(jsonObject);
        return results;

    }

    @Override
    public JSONArray monthAimWancheng(String orgId) throws BaseException {

        // 昨整月 销售额
        Calendar cal_pre= Calendar.getInstance();//本月首日
        cal_pre.add(Calendar.MONTH, 0);
        cal_pre.set(Calendar.DAY_OF_MONTH,1);
        String dateNo1 = DateUtil.formatDate(cal_pre.getTime(),DateUtil.SIMPLE_DATE_FORMAT);
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+dateNo1+"' and to_days(oper_date) <= (to_days(now()))");
        double monthTotalSale = 0d;
        List<Object> objects = unionSqlDao.query("select sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+dateNo1+"' and to_days(oper_date) <= (to_days(now()))");
        for(Object object : objects){
            Double itemEntityObj = (Double)object;
            monthTotalSale +=itemEntityObj!=null?itemEntityObj:0;
        }
        double monthAim = 0d;
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 1 and cycle_year= DATE_FORMAT(NOW(), '%Y')" +
                " and cycle_month = cast(DATE_FORMAT(NOW(), '%m') as int)");
        for(AimEntity aimEntity: aimEntities){
            Double aim_value = aimEntity.getAim_value();
            monthAim +=(aim_value!=null?aim_value:0);
        }
        Calendar cal_now= Calendar.getInstance();
        int currentDay = cal_now.get(Calendar.DAY_OF_MONTH);//今天多少号
        double respectLv = monthTotalSale/currentDay*30/monthAim;
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name",String.format("本月任务：￥%s,目前已完成￥%s,完成率：%s,预测完成率：%s",
                NumberUtil.getDouble2Format(monthAim),
                NumberUtil.getDouble2Format(monthTotalSale),
                NumberUtil.getPercentFormat(monthTotalSale/monthAim),
                NumberUtil.getPercentFormat(monthTotalSale/monthAim),
                NumberUtil.getPercentFormat(respectLv)
        ));
        JSONArray results = new JSONArray();
        results.add(jsonObject);
        return results;
    }

    @Override
    public JSONArray title(String orgId) throws BaseException {

        // 昨 销售额
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                " and to_days(oper_date) = (to_days(now())-1)");
        double totalSale = 0d;
        List<Object> objects = unionSqlDao.query("select sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)");
        for(Object object : objects){
            Double itemEntityObj = (Double)object;
            totalSale +=itemEntityObj!=null?itemEntityObj:0;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("标题1",String.format("【%s】销售共计：￥%s",
                DateUtil.formatDate(new Date(),DateUtil.SIMPLE_DATE_FORMAT),
                totalSale
        ));
        JSONArray results = new JSONArray();
        results.add(jsonObject);
        return results;
    }

    @Override
    public JSONArray salerSale(String orgId) throws BaseException {

        class ItemData{
            String name;
            double tatal_sale;
            double tatal_cnt;
            double tatal_bill;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public double getTatal_sale() {
                return tatal_sale;
            }

            public void setTatal_sale(double tatal_sale) {
                this.tatal_sale = tatal_sale;
            }

            public double getTatal_cnt() {
                return tatal_cnt;
            }

            public void setTatal_cnt(double tatal_cnt) {
                this.tatal_cnt = tatal_cnt;
            }

            public double getTatal_bill() {
                return tatal_bill;
            }

            public void setTatal_bill(double tatal_bill) {
                this.tatal_bill = tatal_bill;
            }
        }
        Map<String,ItemData> dataMap = Maps.newLinkedHashMap();
        // 昨 销售额
//        List<SalerDayEntity>salerDayEntities= salerDayDao.list("where org_id = '"+orgId+"' " +
//                " and to_days(oper_date) = (to_days(now())-1) order by sale_amt desc");
        List<Object> objects = unionSqlDao.query("select sale_id, sum(sale_amt),sum(bill_cnt),sum(sale_qty) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1) group by sale_id order by sum(sale_amt) desc");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Long bill_cnt = (Long)itemEntityObj[2];
            Double sale_qty = (Double)itemEntityObj[3];
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
                itemData.setTatal_bill(itemData.getTatal_bill()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTatal_cnt(itemData.getTatal_cnt()+(sale_qty!=null?sale_qty:0));
            }else{
                itemData = new ItemData();
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
                itemData.setTatal_bill(itemData.getTatal_bill()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTatal_cnt(itemData.getTatal_cnt()+(sale_qty!=null?sale_qty:0));
                dataMap.put(sale_id,itemData);
            }
        }
        String saler_idsStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,saler_id
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(saler_idsStr)){
                saler_idsStr = saler_idsStr + ",'"+str+"'";
            }else{
                saler_idsStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(saler_idsStr)){
            //2员工名
            List<SalerEntity> salerEntities = salerDao.list("where sale_id in("+saler_idsStr+") and org_id = '"+orgId+"'");
            for(SalerEntity salerEntity : salerEntities){
                dataMap.get(salerEntity.getSale_id()).setName(salerEntity.getSale_name());
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("营业员编号",key);
            resultJson.put("姓名",itemData.getName());
            resultJson.put("销售金额",itemData.getTatal_sale());
            resultJson.put("客单价",NumberUtil.getDouble2Format(itemData.getTatal_sale()/itemData.getTatal_bill()));
            resultJson.put("连带率",NumberUtil.getDouble2Format(itemData.getTatal_cnt()/itemData.getTatal_bill()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchSaleTongqi(String orgId) throws BaseException {

        class ItemData{
            double total_sale;
            double total_sale_last_year;

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();
        // 昨 销售额
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                " and to_days(oper_date) = (to_days(now())-1)");
        List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1) group by branch_no");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(branch_no,itemData);
            }
        }

        // 去年昨 销售额
//        List<BranchDayEntity>branchDayEntities_last_year= branchDayDao.list("where org_id = '"+orgId+"' " +
//                " and to_days(oper_date) = (to_days(now())-1-365)");
        List<Object> objects_last_year = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1-365) group by branch_no");
        for(Object object : objects_last_year){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店",key);
            resultJson.put("实销",itemData.getTotal_sale());
            resultJson.put("同期实销",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchSaleRet(String orgId) throws BaseException {

        class DataDay{
            String branchName;
            double sale;
            double ret;

            public String getBranchName() {
                return branchName;
            }

            public void setBranchName(String branchName) {
                this.branchName = branchName;
            }

            public double getSale() {
                return sale;
            }

            public void setSale(double sale) {
                this.sale = sale;
            }

            public double getRet() {
                return ret;
            }

            public void setRet(double ret) {
                this.ret = ret;
            }
        }
        //昨日退货率详情
        JSONArray results = new JSONArray();
//        List<BranchDayEntity> thisYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1) and org_id = '"+orgId+"' order by oper_date");
        List<Object> objects_year = unionSqlDao.query("select branch_no, sum(sale_amt),sum(ret_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1) group by branch_no");
        Map<String,DataDay> thisYearDataMap = new HashMap<>();
        for(Object object : objects_year){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Double ret_amt = (Double)itemEntityObj[2];
            DataDay dataDay = thisYearDataMap.get(branch_no);
            if(dataDay!=null){
                dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
                dataDay.setRet(dataDay.getRet() + (ret_amt!=null?ret_amt:0));
            }else{
                dataDay = new DataDay();
                dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
                dataDay.setRet(dataDay.getRet() + (ret_amt!=null?ret_amt:0));
                thisYearDataMap.put(branch_no,dataDay);
            }
        }
        String branch_nosStr = "";
        Set<String> keys = thisYearDataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id ='"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                thisYearDataMap.get(branchEntity.getBranch_no()).setBranchName(branchEntity.getBranch_name());
            }
        }
        for (Map.Entry<String,DataDay> entry : thisYearDataMap.entrySet()) {
            JSONObject result = new JSONObject();
            String branchNo = entry.getKey();
            result.put("name",entry.getValue().getBranchName());
            result.put("销售",entry.getValue().getSale());
            result.put("退货",entry.getValue().getRet());
            result.put("退货率",NumberUtil.getPercentFormat(entry.getValue().getRet()/entry.getValue().getSale()));
            results.add(result);
        }
        return results;
    }

    @Override
    public JSONArray branchSaleDis(String orgId) throws BaseException {

        class DataDay{
            String branchName;
            double sale;
            double dis;

            public String getBranchName() {
                return branchName;
            }

            public void setBranchName(String branchName) {
                this.branchName = branchName;
            }

            public double getSale() {
                return sale;
            }

            public void setSale(double sale) {
                this.sale = sale;
            }

            public double getDis() {
                return dis;
            }

            public void setDis(double dis) {
                this.dis = dis;
            }
        }
        //昨日折扣率详情
        JSONArray results = new JSONArray();
//        List<BranchDayEntity> thisYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"' order by oper_date");
        List<Object> objects_year = unionSqlDao.query("select branch_no, sum(sale_amt),sum(dis_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1) group by branch_no");
        Map<String,DataDay> thisYearDataMap = new HashMap<>();
        for(Object object : objects_year){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Double dis_amt = (Double)itemEntityObj[2];
            DataDay dataDay = thisYearDataMap.get(branch_no);
            if(dataDay!=null){
                dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
                dataDay.setDis(dataDay.getDis() + (dis_amt!=null?dis_amt:0));
            }else{
                dataDay = new DataDay();
                dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
                dataDay.setDis(dataDay.getDis() + (dis_amt!=null?dis_amt:0));
                thisYearDataMap.put(branch_no,dataDay);
            }
        }
        String branch_nosStr = "";
        Set<String> keys = thisYearDataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id ='"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                thisYearDataMap.get(branchEntity.getBranch_no()).setBranchName(branchEntity.getBranch_name());
            }
        }
        for (Map.Entry<String,DataDay> entry : thisYearDataMap.entrySet()) {
            JSONObject result = new JSONObject();
            String branchNo = entry.getKey();
            result.put("name",entry.getValue().getBranchName());
            result.put("value",NumberUtil.getPercentFormat(entry.getValue().getDis()/entry.getValue().getSale()));
            results.add(result);
        }
        return results;
    }

    @Override
    public JSONArray branchSaleClient(String orgId) throws BaseException {

        //各门店客单价分布
        class DataDay{
            double sale;
            long bill_cnt;

            public double getSale() {
                return sale;
            }

            public void setSale(double sale) {
                this.sale = sale;
            }

            public long getBill_cnt() {
                return bill_cnt;
            }

            public void setBill_cnt(long bill_cnt) {
                this.bill_cnt = bill_cnt;
            }
        }
        JSONArray results = new JSONArray();
//        List<BranchDayEntity> yesterdayBranchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"' order by oper_date");
        Map<String,DataDay> yesterdayDataMap = new HashMap<>();
        List<Object> objects_yesterday = unionSqlDao.query("select branch_no, sum(sale_amt),sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1) group by branch_no");
        for(Object object : objects_yesterday){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Long bill_cnt = (Long)itemEntityObj[2];
            DataDay dataDay = yesterdayDataMap.get(branch_no);
            if(dataDay!=null){
                dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
                dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
            }else{
                dataDay = new DataDay();
                dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
                dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
                yesterdayDataMap.put(branch_no,dataDay);
            }
        }
        for (Map.Entry<String,DataDay> entry : yesterdayDataMap.entrySet()) {
            JSONObject result = new JSONObject();
            String branchNo = entry.getKey();
            result.put("name",branchNo);
            result.put("value",NumberUtil.getDouble2Format(entry.getValue().getSale()/entry.getValue().getBill_cnt()));
            results.add(result);
        }
        return results;
    }

    @Override
    public JSONArray branchSaleLiandailv(String orgId) throws BaseException {

        //各门店连带率分布
        class DataDay{
            double sale_qty;
            long bill_cnt;

            public double getSale_qty() {
                return sale_qty;
            }

            public void setSale_qty(double sale_qty) {
                this.sale_qty = sale_qty;
            }

            public long getBill_cnt() {
                return bill_cnt;
            }

            public void setBill_cnt(long bill_cnt) {
                this.bill_cnt = bill_cnt;
            }
        }
        JSONArray results = new JSONArray();
//        List<BranchDayEntity> yesterdayBranchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"' order by oper_date");
        List<Object> objects_yesterday = unionSqlDao.query("select branch_no, sum(sale_qty),sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1) group by branch_no");
        Map<String,DataDay> yesterdayDataMap = new HashMap<>();
        for(Object object : objects_yesterday){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double qty_sale = (Double)itemEntityObj[1];
            Long bill_cnt = (Long)itemEntityObj[2];
            DataDay dataDay = yesterdayDataMap.get(branch_no);
            if(dataDay!=null){
                dataDay.setSale_qty(dataDay.getSale_qty() + (qty_sale!=null?qty_sale:0));
                dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
            }else{
                dataDay = new DataDay();
                dataDay.setSale_qty(dataDay.getSale_qty() + (qty_sale!=null?qty_sale:0));
                dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
                yesterdayDataMap.put(branch_no,dataDay);
            }
        }
        for (Map.Entry<String,DataDay> entry : yesterdayDataMap.entrySet()) {
            JSONObject result = new JSONObject();
            String branchNo = entry.getKey();
            result.put("name",branchNo);
            result.put("value",NumberUtil.getDouble2Format(entry.getValue().getSale_qty()/entry.getValue().getBill_cnt()*100));
            results.add(result);
        }
        return results;
    }


    @Override
    public JSONArray branchMonthTitle(String orgId, String begin, String end) throws BaseException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name",String.format("%s至%s实时指标",
                begin,
                end
        ));
        JSONArray results = new JSONArray();
        results.add(jsonObject);
        return results;
    }

    @Override
    public JSONArray branchMonthWanchenglv(String orgId, String begin, String end, String branchNo) throws BaseException {

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        int year = 0;
        int month =0;
        try {
            year = sdf.parse(begin).getYear()+1900;
            month = sdf.parse(begin).getMonth()+1;
        } catch (ParseException e) {
        }
        double monthAim = 0d;
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 1 and cycle_year= " +year+
                " and cycle_month = "+month+ branchSql);
        for(AimEntity aimEntity: aimEntities){
            Double aim_value = aimEntity.getAim_value();
            monthAim +=(aim_value!=null?aim_value:0);
        }
        //累计销售销售
        double monthSale = 0d;
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+begin+"' and oper_date <= '"+ end+"'" + branchSql);
        List<Object> objects = unionSqlDao.query("select sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date <= '"+ end+"'"+
                branchSql);
        for(Object object : objects){
            Double itemEntityObj = (Double)object;
            monthSale += itemEntityObj!=null?itemEntityObj:0;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("value",NumberUtil.getDouble2Format(monthSale/monthAim*100));

        JSONArray results = new JSONArray();
        results.add(jsonObject);
        return results;
    }

    @Override
    public JSONArray branchMonthVip(String orgId, String dateBegin, String dateEnd, String branchNo) throws BaseException {


        double total_sale_cnt = 0d;
        double total_sale = 0d;

        double vip_active_cnt = 0d;
        double vip_total_sale = 0d;

        double vip_total_cnt = 0d;
        double vip_new_cnt = 0d;


        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')" + branchSql);
        List<Object> objects = unionSqlDao.query("select sum(sale_amt),sum(sale_qty) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')"+
                branchSql);
        for(Object object : objects){
            Object[] itemEntityObj = (Object[])object;
            total_sale += itemEntityObj[0]!=null?(Double)itemEntityObj[0]:0;
            total_sale_cnt +=itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0;
        }
//        List<VipDayEntity>vipDayEntities= vipDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')" + branchSql);
        List<Object> objects_vip_cnt = unionSqlDao.query("select count(id) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')"+
                branchSql);
        for(Object object : objects_vip_cnt){
            Long itemEntityObj = (Long)object;
            vip_active_cnt += itemEntityObj;
        }
        List<Object> objects_vip = unionSqlDao.query("select sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')"+
                branchSql);
        for(Object object : objects_vip){
            Double itemEntityObj = (Double)object;
            vip_total_sale += itemEntityObj!=null?itemEntityObj:0;
        }

        List<VipEntity> vipEntities_new = vipDao.list("where org_id = '"+orgId+"' " +
                "and to_days(create_date)>=to_days('"+dateBegin+"') and to_days(create_date) <= to_days('"+ dateEnd+"')" + branchSql);
        if(CollectionUtils.isNotEmpty(vipEntities_new)){
            vip_new_cnt = vipEntities_new.size();
        }

        List<VipEntity> vipEntities_all = vipDao.list("where org_id = '"+orgId+"' and to_days(create_date) <= to_days('"+ dateEnd+"')" + branchSql);
        if(CollectionUtils.isNotEmpty(vipEntities_all)){
            vip_total_cnt = vipEntities_all.size();
        }

        JSONArray results = new JSONArray();
        JSONObject resultJson1 = new JSONObject();
        resultJson1.put("name","在册人数");
        resultJson1.put("value1",vip_total_cnt);
        resultJson1.put("value2","365天内办卡或消费");
        results.add(resultJson1);

        JSONObject resultJson2 = new JSONObject();
        resultJson2.put("name","新增会员");
        resultJson2.put("value1",vip_new_cnt);
        resultJson2.put("value2","新增率"+NumberUtil.getPercentFormat(vip_new_cnt/vip_total_cnt));
        results.add(resultJson2);

        JSONObject resultJson3 = new JSONObject();
        resultJson3.put("name","活跃客户");
        resultJson3.put("value1",vip_active_cnt);
        resultJson3.put("value2","活跃度"+NumberUtil.getPercentFormat(vip_active_cnt/vip_total_cnt));
        results.add(resultJson3);

        JSONObject resultJson4 = new JSONObject();
        resultJson4.put("name","会员贡献");
        resultJson4.put("value1",NumberUtil.getDouble2Format(vip_total_sale));
        resultJson4.put("value2","贡献占比"+NumberUtil.getPercentFormat(vip_total_sale/total_sale));
        results.add(resultJson4);

        JSONObject resultJson5 = new JSONObject();
        resultJson5.put("name","人均贡献");
        resultJson5.put("value1",NumberUtil.getDouble2Format(vip_total_sale/vip_total_cnt));
        resultJson5.put("value2","会员人均消费金额");
        results.add(resultJson5);

        JSONObject resultJson6 = new JSONObject();
        resultJson6.put("name","人均数量");
        resultJson6.put("value1",NumberUtil.getDouble2Format(total_sale_cnt/vip_total_cnt));
        resultJson6.put("value2","会员人均购买数量");
        results.add(resultJson6);

        return results;
    }

    @Override
    public JSONArray branchMonthYunying(String orgId, String dateBegin, String dateEnd, String branchNo) throws BaseException {

        double monthAim =0d;
        double monthSale=0d;
        double monthCost =0d;
        double monthSaleCnt =0d;
        double monthSaleBillCnt = 0d;

        double lastYearMonthSale =0d;
        double lastYearMonthSaleCnt =0d;
        double lastYearMonthSaleBillCnt =0d;

        int year = 0;
        int month =0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            year = sdf.parse(dateBegin).getYear()+1900;
            month = sdf.parse(dateBegin).getMonth()+1;
        } catch (ParseException e) {
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 1 and cycle_year= "+year+" and cycle_month="+month+branchSql);
        if(CollectionUtils.isNotEmpty(aimEntities)){
            Double aim_value = aimEntities.get(0).getAim_value();
            monthAim +=(aim_value!=null?aim_value:0);
        }
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+dateBegin+"' and oper_date <= '"+ dateEnd+"'" + branchSql);
        List<Object> objects = unionSqlDao.query("select sum(sale_amt),sum(cost_amt),sum(sale_qty),sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+dateBegin+"' and oper_date <= '"+ dateEnd+"'"+
                branchSql);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            monthSale += itemEntityObj[0]!=null?(Double)itemEntityObj[0]:0;
            monthCost +=itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0;
            monthSaleCnt += itemEntityObj[2]!=null?(Double)itemEntityObj[2]:0;
            monthSaleBillCnt += itemEntityObj[3]!=null?(Long)itemEntityObj[3]:0;
        }

        List<BranchDayEntity>branchDayEntities_lastyear= branchDayDao.list("where org_id = '"+orgId+"' " +
                "and to_days(oper_date)>=(to_days('"+dateBegin+"')-365) and to_days(oper_date) <= (to_days('"+ dateEnd +"')-365)"+ branchSql);
        for(BranchDayEntity branchDayEntity_lastyear: branchDayEntities_lastyear){
            lastYearMonthSale += branchDayEntity_lastyear.getSale_amt()!=null?branchDayEntity_lastyear.getSale_amt():0;
            lastYearMonthSaleCnt += branchDayEntity_lastyear.getSale_qty()!=null?branchDayEntity_lastyear.getSale_qty():0;
            lastYearMonthSaleBillCnt += branchDayEntity_lastyear.getBill_cnt()!=null?branchDayEntity_lastyear.getBill_cnt():0;
        }
        List<Object> objects_lastyear = unionSqlDao.query("select sum(sale_amt),sum(sale_qty),sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+dateBegin+"')-365) and to_days(oper_date) <= (to_days('"+ dateEnd +"')-365)"+
                branchSql);
        for(Object object : objects_lastyear){
            Object[]itemEntityObj = (Object[])object;
            lastYearMonthSale += itemEntityObj[0]!=null?(Double)itemEntityObj[0]:0;
            lastYearMonthSaleCnt += itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0;
            lastYearMonthSaleBillCnt += itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0;
        }
        JSONArray results = new JSONArray();
        JSONObject resultJson1 = new JSONObject();
        resultJson1.put("name","销售完成");
        resultJson1.put("value1",NumberUtil.getDouble2Format(monthSale));
        resultJson1.put("value2","完成率"+NumberUtil.getPercentFormat(monthSale/monthAim));
        results.add(resultJson1);

        JSONObject resultJson2 = new JSONObject();
        resultJson2.put("name","毛利");
        resultJson2.put("value1",NumberUtil.getDouble2Format(monthSale-monthCost));
        resultJson2.put("value2","毛利率"+NumberUtil.getPercentFormat(monthCost/monthSale));
        results.add(resultJson2);

        JSONObject resultJson3 = new JSONObject();
        resultJson3.put("name","成交单数");
        resultJson3.put("value1",monthSaleCnt);
        resultJson3.put("value2","同比"+(lastYearMonthSaleCnt>monthSaleCnt?
                ("下降"+(lastYearMonthSaleCnt-monthSaleCnt))
                :("上涨"+(monthSaleCnt-lastYearMonthSaleCnt))));
        results.add(resultJson3);

        JSONObject resultJson4 = new JSONObject();
        resultJson4.put("name","客单价");
        double clientAmt = monthSale/monthSaleCnt;
        resultJson4.put("value1", NumberUtil.getDouble2Format(clientAmt));
        double lastClientAmt = lastYearMonthSale/lastYearMonthSaleCnt;
        resultJson4.put("value2","同比"+(lastClientAmt>clientAmt?
                ("下降"+NumberUtil.getDouble2Format((lastClientAmt-clientAmt)))
                :("上涨"+NumberUtil.getDouble2Format((clientAmt-lastClientAmt)))));
        results.add(resultJson4);

        JSONObject resultJson5 = new JSONObject();
        resultJson5.put("name","连带率");
        double monthLiandailv = monthSaleCnt/monthSaleBillCnt;
        resultJson5.put("value1",NumberUtil.getDouble2Format(monthLiandailv));
        double lastYearMonthLiandailv = lastYearMonthSaleCnt/lastYearMonthSaleBillCnt;
        resultJson5.put("value2","同比"+(lastYearMonthLiandailv>monthLiandailv?
                ("下降"+NumberUtil.getDouble2Format((lastYearMonthLiandailv-monthLiandailv)))
                :("上涨"+NumberUtil.getDouble2Format((monthLiandailv-lastYearMonthLiandailv)))));
        results.add(resultJson5);

        return results;
    }

    @Override
    public JSONArray branchMonthClsSaleFenbu(String orgId, String dateBegin, String dateEnd, String branchNo) throws BaseException {

        class ItemData{
            double tatal_sale;
            String cls_name;

            public double getTatal_sale() {
                return tatal_sale;
            }

            public void setTatal_sale(double tatal_sale) {
                this.tatal_sale = tatal_sale;
            }

            public String getCls_name() {
                return cls_name;
            }

            public void setCls_name(String cls_name) {
                this.cls_name = cls_name;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new HashMap<>();
        //1分类销售额
//        List<ClsDayEntity>  clsDayEntities = clsDayDao.list("where oper_date>='"+dateBegin+"' and oper_date <= '"+ dateEnd+"'" +
//                " and org_id = '"+orgId+"'"+branchSql);
        List<Object> objects = unionSqlDao.query("select item_clsno, sum(sale_amt) from ClsDayEntity where org_id ='"+orgId+"' and oper_date>='"+dateBegin+"' and oper_date <= '"+ dateEnd+"'"+
                branchSql+" group by item_clsno");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_clsno = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(item_clsno);
            if(itemData!=null){
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(item_clsno,itemData);
            }
        }
        //2对应分类名
        String cls_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(cls_nosStr)){
                cls_nosStr = cls_nosStr + ",'"+str+"'";
            }else{
                cls_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(cls_nosStr)){
            List<ItemClsEntity> itemClsEntities = itemClsDao.list("where item_clsno in("+cls_nosStr+") and org_id = '"+orgId+"'");
            for(ItemClsEntity itemClsEntity : itemClsEntities){
                dataMap.get(itemClsEntity.getItem_clsno()).setCls_name(itemClsEntity.getItem_clsname());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",itemData.getCls_name());
            resultJson.put("value",NumberUtil.getDouble2Format(itemData.getTatal_sale()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchMonthAimWanchengSaleClient(String orgId, String beginDate, String endDate, String branchNo) throws BaseException {

        class ItemData{
            String branch_name;
            double aim;
            double total_sale;
            double total_sale_cnt;
            double total_sale_bill_cnt;

            double total_sale_last_month;
            double total_sale_cnt_last_month;
            double total_sale_bill_cnt_last_month;

            double total_sale_last_year;
            double total_sale_cnt_last_year;
            double total_sale_bill_cnt_last_year;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public double getAim() {
                return aim;
            }

            public void setAim(double aim) {
                this.aim = aim;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_sale_bill_cnt() {
                return total_sale_bill_cnt;
            }

            public void setTotal_sale_bill_cnt(double total_sale_bill_cnt) {
                this.total_sale_bill_cnt = total_sale_bill_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_sale_cnt_last_month() {
                return total_sale_cnt_last_month;
            }

            public void setTotal_sale_cnt_last_month(double total_sale_cnt_last_month) {
                this.total_sale_cnt_last_month = total_sale_cnt_last_month;
            }

            public double getTotal_sale_bill_cnt_last_month() {
                return total_sale_bill_cnt_last_month;
            }

            public void setTotal_sale_bill_cnt_last_month(double total_sale_bill_cnt_last_month) {
                this.total_sale_bill_cnt_last_month = total_sale_bill_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getTotal_sale_cnt_last_year() {
                return total_sale_cnt_last_year;
            }

            public void setTotal_sale_cnt_last_year(double total_sale_cnt_last_year) {
                this.total_sale_cnt_last_year = total_sale_cnt_last_year;
            }

            public double getTotal_sale_bill_cnt_last_year() {
                return total_sale_bill_cnt_last_year;
            }

            public void setTotal_sale_bill_cnt_last_year(double total_sale_bill_cnt_last_year) {
                this.total_sale_bill_cnt_last_year = total_sale_bill_cnt_last_year;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new HashMap<>();
        //本期
//        List<BranchDayEntity> branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'"+branchSql);
        List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt),sum(bill_cnt),sum(sale_qty) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'"+
                branchSql+" group by branch_no");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setTotal_sale_bill_cnt(itemData.getTotal_sale_bill_cnt()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setTotal_sale_cnt(itemData.getTotal_sale_cnt()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setTotal_sale_bill_cnt(itemData.getTotal_sale_bill_cnt()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setTotal_sale_cnt(itemData.getTotal_sale_cnt()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
                dataMap.put(branch_no,itemData);
            }

        }
        //环比 上个月
//        List<BranchDayEntity> branchDayEntities_last_month= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+beginDate+"')-30) and to_days(oper_date)>=(to_days('"+endDate+"')-30)"+branchSql);
        List<Object> objects_last_month= unionSqlDao.query("select branch_no, sum(sale_amt),sum(bill_cnt),sum(sale_qty) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+beginDate+"')-30) and to_days(oper_date)>=(to_days('"+endDate+"')-30)"+
                branchSql+" group by branch_no");
        for(Object object : objects_last_month){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setTotal_sale_bill_cnt_last_month(itemData.getTotal_sale_bill_cnt_last_month()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setTotal_sale_cnt_last_month(itemData.getTotal_sale_cnt_last_month()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setTotal_sale_bill_cnt_last_month(itemData.getTotal_sale_bill_cnt_last_month()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setTotal_sale_cnt_last_month(itemData.getTotal_sale_cnt_last_month()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
                dataMap.put(branch_no,itemData);
            }
        }

        //同比 去年
        List<BranchDayEntity> branchDayEntities_last_year= branchDayDao.list("where org_id = '"+orgId+"' " +
                "and to_days(oper_date)>=(to_days('"+beginDate+"')-365) and to_days(oper_date)>=(to_days('"+endDate+"')-365)"+branchSql);
        List<Object> objects_last_year= unionSqlDao.query("select branch_no, sum(sale_amt),sum(bill_cnt),sum(sale_qty) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+beginDate+"')-365) and to_days(oper_date)>=(to_days('"+endDate+"')-365)"+
                branchSql+" group by branch_no");
        for(Object object : objects_last_year){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setTotal_sale_bill_cnt_last_year(itemData.getTotal_sale_bill_cnt_last_year()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setTotal_sale_cnt_last_year(itemData.getTotal_sale_cnt_last_year()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setTotal_sale_bill_cnt_last_year(itemData.getTotal_sale_bill_cnt_last_year()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setTotal_sale_cnt_last_year(itemData.getTotal_sale_cnt_last_year()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
                dataMap.put(branch_no,itemData);
            }
        }
        //今天最低任务
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 1 and cycle_year= DATE_FORMAT('"+beginDate+"', '%Y') and cycle_month= cast(DATE_FORMAT('"+beginDate+"', '%m') as int)"+branchSql);
        if(CollectionUtils.isNotEmpty(aimEntities)){
            Double aim_value = aimEntities.get(0).getAim_value();
            String branch_no = aimEntities.get(0).getBranch_no();
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setAim((aim_value!=null?aim_value:0)/30);
            }
        }

        //门店名
        String branch_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                dataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店",itemData.getBranch_name());
            resultJson.put("任务",NumberUtil.getDouble2Format(itemData.getAim()));
            resultJson.put("完成率",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getAim()));

            resultJson.put("本期销售",itemData.getTotal_sale());
            resultJson.put("本期销量",itemData.getTotal_sale_cnt());
            resultJson.put("本期客流",itemData.getTotal_sale_bill_cnt());
            resultJson.put("本期客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getTotal_sale_bill_cnt()));
            resultJson.put("本期连带率",NumberUtil.getDouble2Format(itemData.getTotal_sale_cnt()/itemData.getTotal_sale_bill_cnt()));

            resultJson.put("环比销售",itemData.getTotal_sale_last_month());
            resultJson.put("环比销量",itemData.getTotal_sale_cnt_last_month());
            resultJson.put("环比客流",itemData.getTotal_sale_bill_cnt_last_month());
            resultJson.put("环比客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/itemData.getTotal_sale_bill_cnt_last_month()));
            resultJson.put("环比连带率",NumberUtil.getDouble2Format(itemData.getTotal_sale_cnt_last_month()/itemData.getTotal_sale_bill_cnt_last_month()));

            resultJson.put("同比销售",itemData.getTotal_sale_last_year());
            resultJson.put("同比销量",itemData.getTotal_sale_cnt_last_year());
            resultJson.put("同比客流",itemData.getTotal_sale_bill_cnt_last_year());
            resultJson.put("同比客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/itemData.getTotal_sale_bill_cnt_last_year()));
            resultJson.put("同比连带率",NumberUtil.getDouble2Format(itemData.getTotal_sale_cnt_last_year()/itemData.getTotal_sale_bill_cnt_last_year()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchMonthClient(String orgId, String begin, String end, String branchNo) throws BaseException {


        class ItemData{
            String branch_name;
            double total_vip;
            double total_sale;
            double vip_active_cnt;
            double vip_new_cnt;
            double total_sale_last_month;
            double vip_active_cnt_last_month;
            double vip_new_cnt_last_month;
            double total_sale_last_year;
            double vip_active_cnt_last_year;
            double vip_new_cnt_last_year;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_vip() {
                return total_vip;
            }

            public void setTotal_vip(double total_vip) {
                this.total_vip = total_vip;
            }

            public double getVip_active_cnt() {
                return vip_active_cnt;
            }

            public void setVip_active_cnt(double vip_active_cnt) {
                this.vip_active_cnt = vip_active_cnt;
            }

            public double getVip_new_cnt() {
                return vip_new_cnt;
            }

            public void setVip_new_cnt(double vip_new_cnt) {
                this.vip_new_cnt = vip_new_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getVip_active_cnt_last_month() {
                return vip_active_cnt_last_month;
            }

            public void setVip_active_cnt_last_month(double vip_active_cnt_last_month) {
                this.vip_active_cnt_last_month = vip_active_cnt_last_month;
            }

            public double getVip_new_cnt_last_month() {
                return vip_new_cnt_last_month;
            }

            public void setVip_new_cnt_last_month(double vip_new_cnt_last_month) {
                this.vip_new_cnt_last_month = vip_new_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getVip_active_cnt_last_year() {
                return vip_active_cnt_last_year;
            }

            public void setVip_active_cnt_last_year(double vip_active_cnt_last_year) {
                this.vip_active_cnt_last_year = vip_active_cnt_last_year;
            }

            public double getVip_new_cnt_last_year() {
                return vip_new_cnt_last_year;
            }

            public void setVip_new_cnt_last_year(double vip_new_cnt_last_year) {
                this.vip_new_cnt_last_year = vip_new_cnt_last_year;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        //本期
//        List<VipDayEntity>vipDayEntities= vipDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')" + branchSql);
        Map<String,ItemData> dataMap = new HashMap<>();
        List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql+" group by branch_no");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(branch_no,itemData);
            }
        }
        List<Object> objects_active_cnt = unionSqlDao.query("select branch_no,card_id from VipDayEntity where to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')" + branchSql);
        for(Object object : objects_active_cnt){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setVip_active_cnt(itemData.getVip_active_cnt()+1);
            }
        }

        String branch_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        List<VipEntity> vipEntities_new = vipDao.list("where org_id = '"+orgId+"' " +
                "and to_days(create_date)>=to_days('"+begin+"') and to_days(create_date) <= to_days('"+ end+"')" + branchSql);
        for(VipEntity vipEntity : vipEntities_new){
            String branch_no = vipEntity.getBranch_no();
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setVip_new_cnt(itemData.getVip_new_cnt()+1);
            }else{
                itemData = new ItemData();
                itemData.setVip_new_cnt(itemData.getVip_new_cnt()+1);
                dataMap.put(branch_no,itemData);
            }
        }
        List<VipEntity> vipEntities_all = vipDao.list("where org_id = '"+orgId+"' " + branchSql);
        for(VipEntity vipEntity : vipEntities_all){
            String branch_no = vipEntity.getBranch_no();
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_vip(itemData.getTotal_vip()+1);
            }else{
                itemData = new ItemData();
                itemData.setTotal_vip(itemData.getTotal_vip()+1);
                dataMap.put(branch_no,itemData);
            }
        }
        //环比
//        List<VipDayEntity>vipDayEntities_last_month= vipDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)" + branchSql);
        List<Object> objects_last_month = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)"+
                branchSql+" group by branch_no");
        for(Object object : objects_last_month){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                dataMap.put(branch_no,itemData);
            }
        }
        List<Object> objects_active_cnt_last_month = unionSqlDao.query("select branch_no,card_id from VipDayEntity where to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)" + branchSql);
        for(Object object : objects_active_cnt_last_month){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setVip_active_cnt_last_month(itemData.getVip_active_cnt_last_month()+1);
            }
        }
        List<VipEntity> vipEntities_new_last_month = vipDao.list("where org_id = '"+orgId+"' " +
                "and to_days(create_date)>=(to_days('"+begin+"')-30) and to_days(create_date) <= (to_days('"+ end+"')-30)" + branchSql);
        for(VipEntity vipEntity : vipEntities_new_last_month){
            String branch_no = vipEntity.getBranch_no();
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setVip_new_cnt_last_month(itemData.getVip_new_cnt_last_month()+1);
            }else{
                itemData = new ItemData();
                itemData.setVip_new_cnt_last_month(itemData.getVip_new_cnt_last_month()+1);
                dataMap.put(branch_no,itemData);
            }
        }
        //同比
//        List<VipDayEntity>vipDayEntities_last_year= vipDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)" + branchSql);
        List<Object> objects_last_year = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)"+
                branchSql+" group by branch_no");
        for(Object object : objects_last_year){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                dataMap.put(branch_no,itemData);
            }
        }
        List<Object> objects_active_cnt_last_year = unionSqlDao.query("select branch_no,card_id from VipDayEntity where to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)" + branchSql);
        for(Object object : objects_active_cnt_last_year){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setVip_active_cnt_last_year(itemData.getVip_active_cnt_last_year()+1);
            }
        }
        List<VipEntity> vipEntities_new_last_year = vipDao.list("where org_id = '"+orgId+"' " +
                "and to_days(create_date)>=(to_days('"+begin+"')-365) and to_days(create_date) <= (to_days('"+ end+"')-365)" + branchSql);
        for(VipEntity vipEntity : vipEntities_new_last_year){
            String branch_no = vipEntity.getBranch_no();
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setVip_new_cnt_last_year(itemData.getVip_new_cnt()+1);
            }else{
                itemData = new ItemData();
                itemData.setVip_new_cnt_last_year(itemData.getVip_new_cnt()+1);
                dataMap.put(branch_no,itemData);
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                dataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店编号",key);
            resultJson.put("门店名称",itemData.getBranch_name());
            resultJson.put("在册人数",itemData.getTotal_vip());

            resultJson.put("新增人数_本期",itemData.getVip_new_cnt());
            resultJson.put("新增率_本期",NumberUtil.getDouble2Format(itemData.getVip_new_cnt()/itemData.getTotal_vip()));
            resultJson.put("活跃人数_本期",itemData.getVip_active_cnt());
            resultJson.put("活跃度_本期",NumberUtil.getDouble2Format(itemData.getVip_active_cnt()/itemData.getTotal_vip()));
            resultJson.put("人均贡献_本期",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getTotal_vip()));
            resultJson.put("消费总额_本期",itemData.getTotal_sale());

            resultJson.put("新增人数_环比",itemData.getVip_new_cnt_last_month());
            resultJson.put("新增率_环比",NumberUtil.getDouble2Format(itemData.getVip_new_cnt_last_month()/itemData.getTotal_vip()));
            resultJson.put("活跃人数_环比",itemData.getVip_active_cnt_last_month());
            resultJson.put("活跃度_环比",NumberUtil.getDouble2Format(itemData.getVip_active_cnt_last_month()/itemData.getTotal_vip()));
            resultJson.put("人均贡献_环比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/itemData.getTotal_vip()));
            resultJson.put("消费总额_环比",itemData.getTotal_sale_last_month());

            resultJson.put("新增人数_同比",itemData.getVip_new_cnt_last_year());
            resultJson.put("新增率_同比",NumberUtil.getDouble2Format(itemData.getVip_new_cnt_last_year()/itemData.getTotal_vip()));
            resultJson.put("活跃人数_同比",itemData.getVip_active_cnt_last_year());
            resultJson.put("活跃度_同比",NumberUtil.getDouble2Format(itemData.getVip_active_cnt_last_year()/itemData.getTotal_vip()));
            resultJson.put("人均贡献_同比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/itemData.getTotal_vip()));
            resultJson.put("消费总额_同比",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;

    }

    @Override
    public JSONArray branchMonthSaler(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String saler_name;
            String branch_no;
            Long join_time;
            double total_sale;
            double bill_cnt;
            double sale_qty;
            double valid_days;
            double total_sale_last_month;
            double bill_cnt_last_month;
            double sale_qty_last_month;
            double valid_days_last_month;
            double total_sale_last_year;
            double bill_cnt_last_year;
            double sale_qty_last_year;
            double valid_days_last_year;

            public String getSaler_name() {
                return saler_name;
            }

            public void setSaler_name(String saler_name) {
                this.saler_name = saler_name;
            }

            public String getBranch_no() {
                return branch_no;
            }

            public void setBranch_no(String branch_no) {
                this.branch_no = branch_no;
            }

            public Long getJoin_time() {
                return join_time;
            }

            public void setJoin_time(Long join_time) {
                this.join_time = join_time;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getValid_days() {
                return valid_days;
            }

            public void setValid_days(double valid_days) {
                this.valid_days = valid_days;
            }

            public double getBill_cnt() {
                return bill_cnt;
            }

            public void setBill_cnt(double bill_cnt) {
                this.bill_cnt = bill_cnt;
            }

            public double getSale_qty() {
                return sale_qty;
            }

            public void setSale_qty(double sale_qty) {
                this.sale_qty = sale_qty;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getValid_days_last_month() {
                return valid_days_last_month;
            }

            public void setValid_days_last_month(double valid_days_last_month) {
                this.valid_days_last_month = valid_days_last_month;
            }

            public double getBill_cnt_last_month() {
                return bill_cnt_last_month;
            }

            public void setBill_cnt_last_month(double bill_cnt_last_month) {
                this.bill_cnt_last_month = bill_cnt_last_month;
            }

            public double getSale_qty_last_month() {
                return sale_qty_last_month;
            }

            public void setSale_qty_last_month(double sale_qty_last_month) {
                this.sale_qty_last_month = sale_qty_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getValid_days_last_year() {
                return valid_days_last_year;
            }

            public void setValid_days_last_year(double valid_days_last_year) {
                this.valid_days_last_year = valid_days_last_year;
            }

            public double getBill_cnt_last_year() {
                return bill_cnt_last_year;
            }

            public void setBill_cnt_last_year(double bill_cnt_last_year) {
                this.bill_cnt_last_year = bill_cnt_last_year;
            }

            public double getSale_qty_last_year() {
                return sale_qty_last_year;
            }

            public void setSale_qty_last_year(double sale_qty_last_year) {
                this.sale_qty_last_year = sale_qty_last_year;
            }

        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        //本期
        Map<String,ItemData> dataMap = new HashMap<>();
//        List<SalerDayEntity> salerDayEntities= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')" + branchSql);
        List<Object> objects = unionSqlDao.query("select sale_id,sum(sale_amt),sum(bill_cnt),sum(sale_qty) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql+" group by sale_id");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setBill_cnt(itemData.getBill_cnt()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setSale_qty(itemData.getSale_qty()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setBill_cnt(itemData.getBill_cnt()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setSale_qty(itemData.getSale_qty()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
                dataMap.put(sale_id,itemData);
            }
        }
        List<Object> objects_cnt = unionSqlDao.query("select sale_id,oper_date from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql);
        for(Object object : objects_cnt){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setValid_days(itemData.getValid_days()+1);
            }
        }

        String sale_idsStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(sale_idsStr)){
                sale_idsStr = sale_idsStr + ",'"+str+"'";
            }else{
                sale_idsStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(sale_idsStr)){
            List<SalerEntity> salerEntities = salerDao.list("where org_id = '"+orgId+"' and sale_id in("+sale_idsStr+")" );
            for(SalerEntity salerEntity : salerEntities){
                String sale_id = salerEntity.getSale_id();
                ItemData itemData = dataMap.get(sale_id);
                if(itemData!=null){
                    itemData.setBranch_no(salerEntity.getBranch_no());
                    itemData.setSaler_name(salerEntity.getSale_name());
                    itemData.setJoin_time(salerEntity.getGmt_create().getTime());
                }
            }
        }

        //环比
//        List<SalerDayEntity> salerDayEntities_last_month= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)" + branchSql);
        List<Object> objects_last_month = unionSqlDao.query("select sale_id,sum(sale_amt),sum(bill_cnt),sum(sale_qty) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)"+
                branchSql+" group by sale_id");
        for(Object object : objects_last_month){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setBill_cnt_last_month(itemData.getBill_cnt_last_month()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setSale_qty_last_month(itemData.getSale_qty_last_month()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setBill_cnt_last_month(itemData.getBill_cnt_last_month()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setSale_qty_last_month(itemData.getSale_qty_last_month()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
                dataMap.put(sale_id,itemData);
            }
        }
        List<Object> objects_cnt_last_month  = unionSqlDao.query("select sale_id,oper_date from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)"+
                branchSql);
        for(Object object : objects_cnt_last_month ){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setValid_days_last_month(itemData.getValid_days()+1);
            }
        }
        //环比
//        List<SalerDayEntity> salerDayEntities_last_year= salerDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)" + branchSql);
        List<Object> objects_last_year= unionSqlDao.query("select sale_id,sum(sale_amt),sum(bill_cnt),sum(sale_qty) from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)"+
                branchSql+" group by sale_id");
        for(Object object : objects_last_year){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setBill_cnt_last_year(itemData.getBill_cnt_last_year()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setSale_qty_last_year(itemData.getSale_qty_last_year()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0));
                itemData.setBill_cnt_last_year(itemData.getBill_cnt_last_year()+(itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0));
                itemData.setSale_qty_last_year(itemData.getSale_qty_last_year()+(itemEntityObj[3]!=null?(Double)itemEntityObj[3]:0));
                dataMap.put(sale_id,itemData);
            }
        }
        List<Object> objects_cnt_last_year  = unionSqlDao.query("select sale_id,oper_date from SalerDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)"+
                branchSql);
        for(Object object : objects_cnt_last_year ){
            Object[]itemEntityObj = (Object[])object;
            String sale_id = ((String)itemEntityObj[0]);
            ItemData itemData = dataMap.get(sale_id);
            if(itemData!=null){
                itemData.setValid_days_last_year(itemData.getValid_days()+1);
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("员工编号",key);
            resultJson.put("门店名称",itemData.getBranch_no());
            resultJson.put("姓名",itemData.getSaler_name());
            resultJson.put("入职时间",itemData.getJoin_time()!=null?DateUtil.formatDate(new Date(itemData.getJoin_time()),DateUtil.SIMPLE_DATE_FORMAT):"-");
            resultJson.put("工龄",(System.currentTimeMillis()-(itemData.getJoin_time()!=null?itemData.getJoin_time():0))/1000/60/60/24);

            resultJson.put("销售_本期",itemData.getTotal_sale());
            resultJson.put("有效天数_本期",itemData.getValid_days());
            resultJson.put("成交单数_本期",itemData.getBill_cnt());
            resultJson.put("销量_本期",itemData.getSale_qty());
            resultJson.put("客单价_本期",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getBill_cnt()));
            resultJson.put("连带率_本期",NumberUtil.getDouble2Format(itemData.getSale_qty()/itemData.getBill_cnt()*100));

            resultJson.put("销售_环比",itemData.getTotal_sale_last_month());
            resultJson.put("有效天数_环比",itemData.getValid_days_last_month());
            resultJson.put("成交单数_环比",itemData.getBill_cnt_last_month());
            resultJson.put("销量_环比",itemData.getSale_qty_last_month());
            resultJson.put("客单价_环比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/itemData.getBill_cnt_last_month()));
            resultJson.put("连带率_环比",NumberUtil.getDouble2Format(itemData.getSale_qty_last_month()/itemData.getBill_cnt_last_month()*100));

            resultJson.put("销售_同比",itemData.getTotal_sale_last_year());
            resultJson.put("有效天数_同比",itemData.getValid_days_last_year());
            resultJson.put("成交单数_同比",itemData.getBill_cnt_last_year());
            resultJson.put("销量_同比",itemData.getSale_qty_last_year());
            resultJson.put("客单价_同比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/itemData.getBill_cnt_last_year()));
            resultJson.put("连带率_同比",NumberUtil.getDouble2Format(itemData.getSale_qty_last_year()/itemData.getBill_cnt_last_year()*100));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchMonthSaleFenbuDuibi(String orgId, String begin, String end, String branchNo) throws BaseException {
        class ItemData{
            double total_sale;
            double total_sale_last_month;
            double total_sale_last_year;

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";

        //本期
        Map<String,ItemData> dataMap = new HashMap<>();
//        List<BranchDayEntity> branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')" + branchSql);
        List<Object> objects = unionSqlDao.query("select DATE_FORMAT(oper_date, '%d'), sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+begin+"') and to_days(oper_date) <= to_days('"+ end+"')"+
                branchSql+ " GROUP BY DATE_FORMAT(oper_date, '%d')");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String day = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(day);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(day,itemData);
            }
        }
        //环比
//        List<BranchDayEntity> branchDayEntities_last_month= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)" + branchSql);
        List<Object> objects_last_month = unionSqlDao.query("select DATE_FORMAT(oper_date, '%d'), sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-30) and to_days(oper_date) <= (to_days('"+ end+"')-30)"+
                branchSql+ " GROUP BY DATE_FORMAT(oper_date, '%d')");
        for(Object object : objects_last_month){
            Object[]itemEntityObj = (Object[])object;
            String day = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(day);
            if(itemData!=null){
                itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
            }
        }
        //环比
//        List<BranchDayEntity> branchDayEntities_last_year= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)" + branchSql);
        List<Object> objects_last_year= unionSqlDao.query("select DATE_FORMAT(oper_date, '%d'), sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+begin+"')-365) and to_days(oper_date) <= (to_days('"+ end+"')-365)"+
                branchSql+ " GROUP BY DATE_FORMAT(oper_date, '%d')");
        for(Object object : objects_last_year){
            Object[]itemEntityObj = (Object[])object;
            String day = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(day);
            if(itemData!=null){
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("日期",key);
            resultJson.put("本期",itemData.getTotal_sale());
            resultJson.put("环比",itemData.getTotal_sale_last_month());
            resultJson.put("同比",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchMonthBranchSaleFenbu(String orgId, String begin, String end, String branchNo) throws BaseException {

        class ItemData{
            String branch_code;
            String branch_name;
            double total_sale;

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }
            public String getBranch_code() {
                return branch_code;
            }

            public void setBranch_code(String branch_code) {
                this.branch_code = branch_code;
            }
        }

        Map<String,ItemData> dataMap = new HashMap<>();
        //本期
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
//        List<BranchDayEntity> branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+begin+"' and oper_date <= '"+ end+"'" + branchSql);
        List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date <= '"+ end+"'"+
                branchSql+" group by branch_no");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(branch_no,itemData);
            }
        }
        //门店名
        String branch_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                dataMap.get(branchEntity.getBranch_no()).setBranch_code(branchEntity.getBranch_no());
                dataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("code", itemData.getBranch_code());
            resultJson.put("name",itemData.getBranch_name());
            resultJson.put("value",NumberUtil.getDouble2Format(itemData.getTotal_sale()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray branchMonthBranchAimWancheng(String orgId, String begin, String end) throws BaseException {

        //各门店实时任务进度
        class DataDay{
            double sale;
            double aim;
            public double getSale() {
                return sale;
            }

            public double getAim() {
                return aim;
            }

            public void setAim(double aim) {
                this.aim = aim;
            }

            public void setSale(double sale) {
                this.sale = sale;
            }
        }
        JSONArray results = new JSONArray();
//        List<BranchDayEntity> branchDayEntities = branchDayDao.list("where oper_date>='"+begin+"' and oper_date <= '"+ end+"'" +
//                " and org_id = '"+orgId+"'");
        Map<String,DataDay> dataMap = new HashMap<>();
        List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date <= '"+ end+"' group by branch_no");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            DataDay dataDay = dataMap.get(branch_no);
            if(dataDay!=null){
                dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
            }else{
                dataDay = new DataDay();
                dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
                dataMap.put(branch_no,dataDay);
            }
        }
        //门店名
        String branch_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' " +
                    "and aim_cycle = 1 " +
                    "and cycle_year= DATE_FORMAT(NOW(), '%Y') " +
                    "and cycle_month= cast(DATE_FORMAT(NOW(), '%m') as int) " +
                    "and aim_type = 1 and branch_no in("+branch_nosStr+")");
            for(AimEntity aimEntity : aimEntities){
                String branchNo = aimEntity.getBranch_no();
                Double aim = aimEntity.getAim_value();
                DataDay dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setAim(aim!=null?aim/12:0);
                }else{
                    dataDay = new DataDay();
                    dataDay.setAim(aim!=null?aim/12:0);
                    dataMap.put(branchNo,dataDay);
                }
            }
        }
        for (Map.Entry<String,DataDay> entry :dataMap.entrySet()) {
            String branchNo = entry.getKey();

            JSONObject result = new JSONObject();
            result.put("门店",branchNo);
            result.put("计划",NumberUtil.getDouble2Format(entry.getValue().getAim()));
            result.put("完成",entry.getValue().getSale());
            result.put("完成率",NumberUtil.getPercentFormat(entry.getValue().getSale()/entry.getValue().getAim()));
            results.add(result);
        }
        return results;
    }

    @Override
    public JSONArray branchMonthBranchCls(String orgId, String begin, String end,String branchNo) throws BaseException {

        class ItemData{
            double tatal_sale;
            String cls_name;

            public double getTatal_sale() {
                return tatal_sale;
            }

            public void setTatal_sale(double tatal_sale) {
                this.tatal_sale = tatal_sale;
            }

            public String getCls_name() {
                return cls_name;
            }

            public void setCls_name(String cls_name) {
                this.cls_name = cls_name;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new HashMap<>();
        //1分类销售额
//        List<ClsDayEntity>  clsDayEntities = clsDayDao.list("where oper_date>='"+begin+"' and oper_date <= '"+ end+"'" +
//                " and org_id = '"+orgId+"'"+branchSql);
        List<Object> objects = unionSqlDao.query("select item_clsno, sum(sale_amt) from ClsDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date <= '"+ end+"'"+
                branchSql+"group by item_clsno");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_clsno = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(item_clsno);
            if(itemData!=null){
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(item_clsno,itemData);
            }
        }
        //2对应分类名
        String cls_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(cls_nosStr)){
                cls_nosStr = cls_nosStr + ",'"+str+"'";
            }else{
                cls_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(cls_nosStr)){
            List<ItemClsEntity> itemClsEntities = itemClsDao.list("where item_clsno in("+cls_nosStr+") and org_id = '"+orgId+"'");
            for(ItemClsEntity itemClsEntity : itemClsEntities){
                dataMap.get(itemClsEntity.getItem_clsno()).setCls_name(itemClsEntity.getItem_clsname());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",itemData.getCls_name());
            resultJson.put("value",itemData.getTatal_sale());
            results.add(resultJson);
        }
        return results;
    }
}
