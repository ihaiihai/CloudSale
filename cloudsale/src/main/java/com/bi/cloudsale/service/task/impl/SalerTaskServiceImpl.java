package com.bi.cloudsale.service.task.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.utils.ArithUtil;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.dto.task.FollowUpTaskDto;
import com.bi.cloudsale.dto.task.SetUpTaskDto;
import com.bi.cloudsale.persistent.dao.saler.SaleSalerDayDao;
import com.bi.cloudsale.persistent.dao.task.TaskDao;
import com.bi.cloudsale.persistent.entity.saler.SaleSalerDayEntity;
import com.bi.cloudsale.persistent.entity.task.SaleAimEntity;
import com.bi.cloudsale.service.task.SalerTaskService;

@Service
@Transactional
public class SalerTaskServiceImpl implements SalerTaskService {
	
	@Resource
	private TaskDao taskDao;
	@Resource
	private SaleSalerDayDao saleSalerDayDao;
	
	@Override
	public List<FollowUpTaskDto> queryFollowUp(TaskQueryCondition condition) {
		List<FollowUpTaskDto> dtoList = new ArrayList<FollowUpTaskDto>();
		List<SaleAimEntity> entityList = taskDao.queryByCondition(condition);
		if(entityList != null && entityList.size() > 0) {
			Map<String,Map<String,FollowUpTaskDto>> map = new HashMap<String,Map<String,FollowUpTaskDto>>();
			Set<String> branchNo = new HashSet<String>();
			Set<String> salerNo = new HashSet<String>();
			for(SaleAimEntity entity : entityList) {
				FollowUpTaskDto dto = null;
				if(findMapItem_followup(map, entity.getBranch_no(), entity.getSale_id()) == null) {
					dto = new FollowUpTaskDto();
					dto.set门店(entity.getBranch_no());
					dto.set营业员(entity.getSale_id());
					if(!branchNo.contains(entity.getBranch_no())) {
						branchNo.add(entity.getBranch_no());
					}
					if(!salerNo.contains(entity.getSale_id())) {
						salerNo.add(entity.getSale_id());
					}
					dtoList.add(dto);
					addMapItem_followup(map, entity.getBranch_no(), entity.getSale_id(), dto);
				}else {
					dto = findMapItem_followup(map, entity.getBranch_no(), entity.getSale_id());
				}
				if(entity.getAim_cycle() == 0) {
					dto.set年度目标(entity.getAim_value());
				}
				if(entity.getAim_cycle() == 1 && entity.getCycle_month() == DateUtil.getCurrentMonth()) {
					dto.set本月任务(entity.getAim_value());
				}
			}
			//按年统计销售数据
			List<SaleSalerDayEntity> saleSumListYear = saleSalerDayDao.querySum(condition.getUserId(), condition.getOrgId(), salerNo, branchNo, DateUtil.getYearStart(condition.getYear() - 1), DateUtil.getYearEnd(condition.getYear() - 1));
			if(saleSumListYear != null && saleSumListYear.size() > 0) {
				for(SaleSalerDayEntity entity : saleSumListYear) {
					FollowUpTaskDto dto = findMapItem_followup(map, entity.getBranch_no(), entity.getSale_id());
					if(dto != null) {
						dto.set今年完成(entity.getSale_amt());
						dto.set年进度(ArithUtil.getPercent(dto.get今年完成(), dto.get年度目标()));
						try {
							dto.set年任务预测(ArithUtil.mul(ArithUtil.div(dto.get今年完成(), DateUtil.getDayInYear(condition.getYear()), 2), 365));
							dto.set年完成率预测(ArithUtil.div(dto.get年任务预测(), dto.get年度目标(), 2));
						} catch (IllegalAccessException e) {
						}
					}
				}
			}
			//按月统计销售数据
			List<SaleSalerDayEntity> saleSumListMonth = saleSalerDayDao.querySum(condition.getUserId(), condition.getOrgId(), salerNo, branchNo, DateUtil.getMonthStart(condition.getYear(), condition.getMonth()), DateUtil.getMonthEnd(condition.getYear(), condition.getMonth()));
			if(saleSumListMonth != null && saleSumListMonth.size() > 0) {
				for(SaleSalerDayEntity entity : saleSumListMonth) {
					FollowUpTaskDto dto = findMapItem_followup(map, entity.getBranch_no(), entity.getSale_id());
					if(dto != null) {
						dto.set本月完成(entity.getSale_amt());
						dto.set月进度(ArithUtil.getPercent(dto.get本月完成(), dto.get本月任务()));
						dto.set剩余任务(ArithUtil.sub(dto.get本月任务(), dto.get本月完成()));
						dto.set剩余天数(DateUtil.getMonthDays(condition.getYear(), condition.getMonth()) - DateUtil.getDayInMonth(condition.getYear(), condition.getMonth()));
						try {
							dto.set本月任务预测(ArithUtil.mul(ArithUtil.div(dto.get本月完成(), DateUtil.getDayInMonth(condition.getYear(), condition.getMonth()), 2), DateUtil.getMonthDays(condition.getYear(), condition.getMonth())));
							dto.set预测完成率(ArithUtil.div(dto.get本月任务预测(), dto.get本月完成(), 2));
						} catch (IllegalAccessException e) {
						}
					}
				}
			}
		}
		return dtoList;
	}

	@Override
	public List<SetUpTaskDto> querySetUp(TaskQueryCondition condition) {
		List<SetUpTaskDto> dtoList = new ArrayList<SetUpTaskDto>();
		List<SaleAimEntity> entityList = taskDao.queryByCondition(condition);
		if(entityList != null && entityList.size() > 0) {
			Map<String,Map<String,SetUpTaskDto>> map = new HashMap<String,Map<String,SetUpTaskDto>>();
			Set<String> branchNo = new HashSet<String>();
			Set<String> brandNo = new HashSet<String>();
			for(SaleAimEntity entity : entityList) {
				SetUpTaskDto dto = null;
				if(findMapItem(map, entity.getBranch_no(), entity.getSale_id()) == null) {
					dto = new SetUpTaskDto();
					dto.set门店编号(entity.getBranch_no());
					dto.set门店名称(entity.getBranch_no());
					dto.set营业员编号(entity.getSale_id());
					dto.set姓名(entity.getSale_id());
					if(!branchNo.contains(entity.getBranch_no())) {
						branchNo.add(entity.getBranch_no());
					}
					if(!brandNo.contains(entity.getItem_brandno())) {
						brandNo.add(entity.getItem_brandno());
					}
					dtoList.add(dto);
					addMapItem(map, entity.getBranch_no(), entity.getSale_id(), dto);
				}else {
					dto = findMapItem(map, entity.getBranch_no(), entity.getSale_id());
				}
				if(entity.getAim_cycle() == 0) {
					dto.set销售任务(entity.getAim_value());
				}
				if(entity.getAim_cycle() == 1) {
					setMonthAim(dto, entity.getCycle_month(), entity.getAim_value());
				}
			}
		}
		return dtoList;
	}
	
	private void addMapItem(Map<String,Map<String,SetUpTaskDto>> map, String branchNo, String saleNo, SetUpTaskDto dto) {
		Map<String,SetUpTaskDto> childMap = null;
		if(map.get(branchNo) != null) {
			childMap = map.get(branchNo);
		}else {
			childMap = new HashMap<String,SetUpTaskDto>();
		}
		childMap.put(saleNo, dto);
		map.put(branchNo, childMap);
	}
	
	private SetUpTaskDto findMapItem(Map<String,Map<String,SetUpTaskDto>> map, String branchNo, String saleNo) {
		if(map.get(branchNo) == null) {
			return null;
		}
		return map.get(branchNo).get(saleNo);
	}
	
	private void setMonthAim(SetUpTaskDto dto, Integer month, Double aimValue) {
        try {
        	Method method = dto.getClass().getMethod("set月度目标_" + String.format("%02d", month), Double.class);
			method.invoke(dto, aimValue);
		} catch (Exception e) {
			
		}
	}
	
	private void addMapItem_followup(Map<String,Map<String,FollowUpTaskDto>> map, String branchNo, String saleNo, FollowUpTaskDto dto) {
		Map<String,FollowUpTaskDto> childMap = null;
		if(map.get(branchNo) != null) {
			childMap = map.get(branchNo);
		}else {
			childMap = new HashMap<String,FollowUpTaskDto>();
		}
		childMap.put(saleNo, dto);
		map.put(branchNo, childMap);
	}
	
	private FollowUpTaskDto findMapItem_followup(Map<String,Map<String,FollowUpTaskDto>> map, String branchNo, String saleNo) {
		if(map.get(branchNo) == null) {
			return null;
		}
		return map.get(branchNo).get(saleNo);
	}

}
