package com.bi.cloudsale.service.sys;

import java.util.List;

import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.ModuleDto;

public interface ModuleService {

	List<ModuleDto> queryAll(String userId);
	
	List<OptionDto> queryAllOptions(String userId);
}
