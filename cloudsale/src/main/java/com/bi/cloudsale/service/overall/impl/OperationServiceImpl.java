package com.bi.cloudsale.service.overall.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.ArithUtil;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.dto.overall.BranchIndDto;
import com.bi.cloudsale.dto.overall.OperationDto;
import com.bi.cloudsale.persistent.dao.branch.SaleBranchDayDao;
import com.bi.cloudsale.persistent.dao.overall.OperationDao;
import com.bi.cloudsale.persistent.dao.saler.SaleSalerDayDao;
import com.bi.cloudsale.persistent.dao.saler.SalerInfoDao;
import com.bi.cloudsale.persistent.dao.vip.SaleVipDayDao;
import com.bi.cloudsale.persistent.entity.branch.SaleBranchDayEntity;
import com.bi.cloudsale.persistent.entity.overall.BranchIndEntity;
import com.bi.cloudsale.persistent.entity.saler.SaleSalerDayEntity;
import com.bi.cloudsale.persistent.entity.vip.SaleVipDayEntity;
import com.bi.cloudsale.service.overall.OperationService;

@Service
@Transactional
public class OperationServiceImpl implements OperationService {
	
	private static final Logger log = LoggerFactory.getLogger(OperationServiceImpl.class);
	
	@Resource
	private OperationDao operationDao;
	@Resource
	private SaleBranchDayDao saleBranchDayDao;
	@Resource
	private SaleSalerDayDao saleSalerDayDao;
	@Resource
	private SalerInfoDao salerInfoDao;
	@Resource
	private SaleVipDayDao saleVipDayDao;

	@Override
	public void createOperation(BranchIndDto operationDto) throws BaseException {
		BranchIndEntity entity = new BranchIndEntity();
		BeanUtil.copyProperties(operationDto, entity);
		try {
			operationDao.createBranchInd(entity);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public List<OperationDto> queryOperation(String orgId, String branchNos, String begin, String end) {
		List<OperationDto> list = new ArrayList<OperationDto>();
		List<BranchIndEntity> branchIndList = operationDao.queryByOrgId(orgId);
		if(branchIndList != null && branchIndList.size() > 0) {
			Map<String,OperationDto> map = new HashMap<String,OperationDto>();
			Map<String,Double> areaMap = new HashMap<String,Double>();
			Map<String,Double> totalSaleMap = new HashMap<String,Double>();
			Set<String> branchNo = new HashSet<String>();
			for(BranchIndEntity branchIndEntity : branchIndList) {
				OperationDto operationDto = new OperationDto();
				operationDto.set人均日销_目标(branchIndEntity.getConsume_saler());
				operationDto.set平米日均_目标(branchIndEntity.getConsume_area());
				operationDto.set日均销售_目标(branchIndEntity.getConsume_day());
				operationDto.set日均客流_目标(branchIndEntity.getBill_day());
				operationDto.set会员占比_目标(branchIndEntity.getVip_rate());
				operationDto.set会员日均贡献_目标(branchIndEntity.getConsume_vip());
				operationDto.set平均折扣_目标(branchIndEntity.getDis_rate());
				operationDto.set退货率_目标(branchIndEntity.getRet_rate());
				operationDto.set客单价_目标(branchIndEntity.getConsume_per());
				operationDto.set连带率_目标(branchIndEntity.getTrans_value());
				map.put(branchIndEntity.getBranch_no(), operationDto);
				areaMap.put(branchIndEntity.getBranch_no(), branchIndEntity.getArea());
				branchNo.add(branchIndEntity.getBranch_no());
				list.add(operationDto);
			}
			int dayDuration = DateUtil.getDuration(DateUtil.getDateStart(begin), DateUtil.getDateEnd(end));
			List<SaleBranchDayEntity> branchDayList = saleBranchDayDao.queryAllSumByBranch(null, orgId, branchNo, DateUtil.getDateStart(begin), DateUtil.getDateEnd(end));
			if(branchDayList != null && branchDayList.size() > 0) {
				for(SaleBranchDayEntity saleBranchDayEntity : branchDayList) {
					if(map.get(saleBranchDayEntity.getBranch_no()) != null) {
						try {
							totalSaleMap.put(saleBranchDayEntity.getBranch_no(), saleBranchDayEntity.getSale_amt());
							map.get(saleBranchDayEntity.getBranch_no()).set日均销售(ArithUtil.div(saleBranchDayEntity.getSale_amt(), dayDuration, 2));
							map.get(saleBranchDayEntity.getBranch_no()).set平米日均(ArithUtil.div(map.get(saleBranchDayEntity.getBranch_no()).get日均销售(), areaMap.get(saleBranchDayEntity.getBranch_no()), 2));
							map.get(saleBranchDayEntity.getBranch_no()).set日均客流(ArithUtil.div(saleBranchDayEntity.getBill_cnt(), dayDuration, 2));
							map.get(saleBranchDayEntity.getBranch_no()).set平均折扣(ArithUtil.getPercent(saleBranchDayEntity.getDis_amt(), saleBranchDayEntity.getSale_amt()));
							map.get(saleBranchDayEntity.getBranch_no()).set退货率(ArithUtil.getPercent(saleBranchDayEntity.getRet_qty(), saleBranchDayEntity.getSale_qty()));
							map.get(saleBranchDayEntity.getBranch_no()).set客单价(ArithUtil.getPercent(saleBranchDayEntity.getSale_amt(), saleBranchDayEntity.getBill_cnt()));
							map.get(saleBranchDayEntity.getBranch_no()).set连带率(ArithUtil.getPercent(saleBranchDayEntity.getSale_qty(), saleBranchDayEntity.getBill_cnt()));
						} catch (IllegalAccessException e) {
							log.error(e.getMessage(), e);
						}
					}
				}
			}
			List<SaleSalerDayEntity> salerDayList = saleSalerDayDao.querySalerCount(null, orgId, branchNo, DateUtil.getDateStart(begin), DateUtil.getDateEnd(end));
			if(salerDayList != null && salerDayList.size() > 0) {
				for(SaleSalerDayEntity saleSalerDayEntity : salerDayList) {
					if(map.get(saleSalerDayEntity.getBranch_no()) != null) {
						try {
							map.get(saleSalerDayEntity.getBranch_no()).set人均日销(ArithUtil.div(map.get(saleSalerDayEntity.getBranch_no()).get日均销售(), saleSalerDayEntity.getSalerCount(), 2));
						} catch (IllegalAccessException e) {
							log.error(e.getMessage(), e);
						}
					}
				}
			}
			List<SaleVipDayEntity> vipDayList = saleVipDayDao.querySum(null, orgId, branchNo, DateUtil.getDateStart(begin), DateUtil.getDateEnd(end));
			if(vipDayList != null && vipDayList.size() > 0) {
				for(SaleVipDayEntity saleVipDayEntity : vipDayList) {
					if(map.get(saleVipDayEntity.getBranch_no()) != null) {
						map.get(saleVipDayEntity.getBranch_no()).set会员占比(ArithUtil.getPercent(saleVipDayEntity.getSale_amt(), totalSaleMap.get(saleVipDayEntity.getBranch_no())));
						try {
							map.get(saleVipDayEntity.getBranch_no()).set会员日均贡献(ArithUtil.div(saleVipDayEntity.getSale_amt(), dayDuration, 2));
						} catch (IllegalAccessException e) {
							log.error(e.getMessage(), e);
						}
					}
				}
			}
			for(BranchIndEntity branchIndEntity : branchIndList) {
				try {
					map.get(branchIndEntity.getBranch_no()).set人均日销_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get人均日销(), map.get(branchIndEntity.getBranch_no()).get人均日销_目标()));
					map.get(branchIndEntity.getBranch_no()).set平米日均_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get平米日均(), map.get(branchIndEntity.getBranch_no()).get平米日均_目标()));
					map.get(branchIndEntity.getBranch_no()).set日均销售_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get日均销售(), map.get(branchIndEntity.getBranch_no()).get日均销售_目标()));
					map.get(branchIndEntity.getBranch_no()).set日均客流_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get日均客流(), map.get(branchIndEntity.getBranch_no()).get日均客流_目标()));
					map.get(branchIndEntity.getBranch_no()).set会员占比_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get会员占比(), map.get(branchIndEntity.getBranch_no()).get会员占比_目标()));
					map.get(branchIndEntity.getBranch_no()).set会员日均贡献_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get会员日均贡献(), map.get(branchIndEntity.getBranch_no()).get会员日均贡献_目标()));
					map.get(branchIndEntity.getBranch_no()).set平均折扣_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get平均折扣(), map.get(branchIndEntity.getBranch_no()).get平均折扣_目标()));
					map.get(branchIndEntity.getBranch_no()).set退货率_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get退货率(), map.get(branchIndEntity.getBranch_no()).get退货率_目标()));
					map.get(branchIndEntity.getBranch_no()).set客单价_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get客单价(), map.get(branchIndEntity.getBranch_no()).get客单价_目标()));
					map.get(branchIndEntity.getBranch_no()).set连带率_得分(ArithUtil.getScore(map.get(branchIndEntity.getBranch_no()).get连带率(), map.get(branchIndEntity.getBranch_no()).get连带率_目标()));
				} catch(Exception e) {
					log.error(e.getMessage(), e);
				}
			}
		}
		return list;
	}
	
}
