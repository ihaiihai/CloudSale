package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.service.BranchAimService;
import com.bi.cloudsale.service.ShouqingService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class ShouqingServiceImpl implements ShouqingService {

    private static final Logger log = LoggerFactory.getLogger(ShouqingServiceImpl.class);

    @Resource
    private BrandDao brandDao;
    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private ClsDayDao itemClsDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private SalerDayDao salerDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private BrandDayDao brandDayDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {

        List<BranchEntity> branchEntities = branchDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray brands(String orgId) throws BaseException {

        List<BrandEntity> brandEntities = brandDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BrandEntity brandEntity:brandEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("brand_no",brandEntity.getItem_brandno());
            jsonObject.put("brand_name",brandEntity.getItem_brandname());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray items(String orgId) throws BaseException {
        List<ItemEntity> itemEntities = itemDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(ItemEntity itemEntity:itemEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("item_no",itemEntity.getItem_no());
            jsonObject.put("item_name",itemEntity.getItem_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray clses(String orgId) throws BaseException {
        List<ItemClsEntity> itemClsEntities = itemClsDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(ItemClsEntity itemClsEntity:itemClsEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("item_clsno",itemClsEntity.getItem_clsno());
            jsonObject.put("item_clsname",itemClsEntity.getItem_clsname());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray query(String orgId, String type, String begin,String end, String branchNos, String itemClses, String brands,Integer pageNo,Integer pageSize) throws BaseException {
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        String itemClsSqlin = "";
        if(StringUtils.isNotEmpty(itemClses)){
            String[] item_clsnos = itemClses.split(",");
            for(String item_clsno:item_clsnos){
                if(!"".equals(itemClsSqlin)){
                    itemClsSqlin = itemClsSqlin+","+"'"+item_clsno+"'";
                }else{
                    itemClsSqlin = "'"+item_clsno+"'";
                }
            }
        }
        String itemClsSql = StringUtils.isNotEmpty(itemClsSqlin)?" and item_clsno in("+itemClsSqlin+")":"";
        String brandSqlin = "";
        if(StringUtils.isNotEmpty(brands)){
            String[] brand_nos = brands.split(",");
            for(String brand_no:brand_nos){
                if(!"".equals(brandSqlin)){
                    brandSqlin = brandSqlin+","+"'"+brand_no+"'";
                }else{
                    brandSqlin = "'"+brand_no+"'";
                }
            }
        }
        String brandSql = StringUtils.isNotEmpty(brandSqlin)?" and brand_no in("+brandSqlin+")":"";
        class ItemData{
            String item_no;
            String branch_no;
            String item_clsno;
            String item_brandno;
            double sale_cnt;
            double total_sale;
            double total_cost;
            double total_bill;
            double total_ret;
            double total_ret_cnt;
            double stock_cnt;
            double stock_sale;
            double stock_cost;

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public String getBranch_no() {
                return branch_no;
            }

            public void setBranch_no(String branch_no) {
                this.branch_no = branch_no;
            }

            public String getItem_clsno() {
                return item_clsno;
            }

            public void setItem_clsno(String item_clsno) {
                this.item_clsno = item_clsno;
            }

            public String getItem_brandno() {
                return item_brandno;
            }

            public void setItem_brandno(String item_brandno) {
                this.item_brandno = item_brandno;
            }

            public double getSale_cnt() {
                return sale_cnt;
            }

            public void setSale_cnt(double sale_cnt) {
                this.sale_cnt = sale_cnt;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_bill() {
                return total_bill;
            }

            public void setTotal_bill(double total_bill) {
                this.total_bill = total_bill;
            }

            public double getTotal_ret() {
                return total_ret;
            }

            public void setTotal_ret(double total_ret) {
                this.total_ret = total_ret;
            }

            public double getTotal_ret_cnt() {
                return total_ret_cnt;
            }

            public void setTotal_ret_cnt(double total_ret_cnt) {
                this.total_ret_cnt = total_ret_cnt;
            }

            public double getStock_sale() {
                return stock_sale;
            }

            public void setStock_sale(double stock_sale) {
                this.stock_sale = stock_sale;
            }

            public double getStock_cost() {
                return stock_cost;
            }

            public void setStock_cost(double stock_cost) {
                this.stock_cost = stock_cost;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();
        if(StringUtils.equals(type,"cls")){
            List<Object> objectList = unionSqlDao.query("SELECT sum(t1.sale_amt),sum(t1.sale_qty),sum(t1.bill_cnt),sum(t1.ret_amt),sum(t1.ret_qty),sum(t1.cost_amt),t1.item_clsno, " +
                    "t2.stock_qty,t2.branch_no,t2.cost_price,t2.cost_amt " +
                    " FROM ClsDayEntity t1,StockEntity t2,ItemEntity t3  " +
                    "where t1.branch_no=t2.branch_no and t3.item_clsno=t1.item_clsno and t3.item_no=t2.item_no " +
                    "and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' and t1.org_id='"+orgId+"' " +
                    "GROUP BY t1.item_clsno ORDER BY t2.branch_no",pageNo,pageSize);
            for(Object itemEntity: objectList){
                Object[]itemEntityObj = (Object[])itemEntity;
                Double total_sale = (Double)itemEntityObj[0];
                Double total_sale_cnt = (Double)itemEntityObj[1];
                Long bill_cnt = (Long)itemEntityObj[2];
                Double total_ret = (Double)itemEntityObj[3];
                Double ret_cnt = (Double)itemEntityObj[4];
                Double total_cost = (Double)itemEntityObj[5];
                String itemClsno = (String)itemEntityObj[6];
                Double stock_cnt = (Double)itemEntityObj[7];
                String branch_no = (String)itemEntityObj[8];
                Double cost_price = (Double)itemEntityObj[9];
                Double cost_amt = (Double)itemEntityObj[10];

                ItemData itemData = new ItemData();
                itemData.setBranch_no(branch_no);
                itemData.setTotal_sale((total_sale!=null?total_sale:0));
                itemData.setTotal_ret_cnt((ret_cnt!=null?ret_cnt:0));
                itemData.setTotal_ret((total_ret!=null?total_ret:0));
                itemData.setSale_cnt((total_sale_cnt!=null?total_sale_cnt:0));
                itemData.setTotal_cost((total_cost!=null?total_cost:0));
                itemData.setStock_cnt((stock_cnt!=null?stock_cnt:0));
                itemData.setItem_clsno(itemClsno);
                itemData.setStock_cost(cost_price!=null?cost_price:0);
                itemData.setStock_sale(cost_amt!=null?cost_amt:0);
                itemData.setTotal_bill(bill_cnt!=null?bill_cnt:0);

                dataMap.put(branch_no+"@@"+itemClsno,itemData);
            }
        }else if(StringUtils.equals(type,"item_brand")){
            List<Object> objectList = unionSqlDao.query("SELECT sum(t1.sale_amt),sum(t1.sale_qty),sum(t1.bill_cnt),sum(t1.ret_amt),sum(t1.ret_qty),sum(t1.cost_amt),t1.item_brandno,\n" +
                    "t2.stock_qty,t2.branch_no,t2.cost_price,t2.cost_amt\n" +
                    " FROM BrandDayEntity t1,StockEntity t2,ItemEntity t3 \n" +
                    "where t1.branch_no=t2.branch_no and t3.item_brandno=t1.item_brandno and t3.item_no=t2.item_no " +
                    "and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' and t1.org_id='"+orgId+"' " +
                    "GROUP BY t1.item_brandno ORDER BY t2.branch_no",pageNo,pageSize);
            for(Object itemEntity: objectList){
                Object[]itemEntityObj = (Object[])itemEntity;
                Double total_sale = (Double)itemEntityObj[0];
                Double total_sale_cnt = (Double)itemEntityObj[1];
                Long bill_cnt = (Long)itemEntityObj[2];
                Double total_ret = (Double)itemEntityObj[3];
                Double ret_cnt = (Double)itemEntityObj[4];
                Double total_cost = (Double)itemEntityObj[5];
                String item_brandno = (String)itemEntityObj[6];
                Double stock_cnt = (Double)itemEntityObj[7];
                String branch_no = (String)itemEntityObj[8];
                Double cost_price = (Double)itemEntityObj[9];
                Double cost_amt = (Double)itemEntityObj[10];

                ItemData itemData = new ItemData();
                itemData.setBranch_no(branch_no);
                itemData.setTotal_sale((total_sale!=null?total_sale:0));
                itemData.setTotal_ret_cnt((ret_cnt!=null?ret_cnt:0));
                itemData.setTotal_ret((total_ret!=null?total_ret:0));
                itemData.setSale_cnt((total_sale_cnt!=null?total_sale_cnt:0));
                itemData.setTotal_cost((total_cost!=null?total_cost:0));
                itemData.setStock_cnt((stock_cnt!=null?stock_cnt:0));
                itemData.setItem_brandno(item_brandno);
                itemData.setStock_cost(cost_price!=null?cost_price:0);
                itemData.setStock_sale(cost_amt!=null?cost_amt:0);
                itemData.setTotal_bill(bill_cnt!=null?bill_cnt:0);

                dataMap.put(branch_no+"@@"+item_brandno,itemData);
            }
        }else if(StringUtils.equals(type,"item")){
            List<Object> objectList = unionSqlDao.query("SELECT sum(t1.sale_amt),sum(t1.sale_qty),sum(t1.bill_cnt),sum(t1.ret_amt),sum(t1.ret_qty),sum(t1.cost_amt),t1.item_no,\n" +
                    "t2.stock_qty,t2.branch_no,t2.cost_price,t2.cost_amt\n" +
                    " FROM ItemDayEntity t1,StockEntity t2,ItemEntity t3 \n" +
                    "where t1.item_no=t2.item_no and t3.item_no=t2.item_no " +
                    "and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' and t1.org_id='"+orgId+"' " +
                    "GROUP BY t1.item_no",pageNo,pageSize);
            for(Object itemEntity: objectList){
                Object[]itemEntityObj = (Object[])itemEntity;
                Double total_sale = (Double)itemEntityObj[0];
                Double total_sale_cnt = (Double)itemEntityObj[1];
                Long bill_cnt = (Long)itemEntityObj[2];
                Double total_ret = (Double)itemEntityObj[3];
                Double ret_cnt = (Double)itemEntityObj[4];
                Double total_cost = (Double)itemEntityObj[5];
                String item_no = (String)itemEntityObj[6];
                Double stock_cnt = (Double)itemEntityObj[7];
                String branch_no = (String)itemEntityObj[8];
                Double cost_price = (Double)itemEntityObj[9];
                Double cost_amt = (Double)itemEntityObj[10];

                ItemData itemData = new ItemData();
                itemData.setBranch_no(branch_no);
                itemData.setTotal_sale((total_sale!=null?total_sale:0));
                itemData.setTotal_ret_cnt((ret_cnt!=null?ret_cnt:0));
                itemData.setTotal_ret((total_ret!=null?total_ret:0));
                itemData.setSale_cnt((total_sale_cnt!=null?total_sale_cnt:0));
                itemData.setTotal_cost((total_cost!=null?total_cost:0));
                itemData.setStock_cnt((stock_cnt!=null?stock_cnt:0));
                itemData.setItem_no(item_no);
                itemData.setStock_cost(cost_price!=null?cost_price:0);
                itemData.setStock_sale(cost_amt!=null?cost_amt:0);
                itemData.setTotal_bill(bill_cnt!=null?bill_cnt:0);

                dataMap.put(branch_no+"@@"+item_no,itemData);
            }
        }else if(StringUtils.equals(type,"main_supcust")){

        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String[] key = entry.getKey().split("@@");
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("门店名称", key[0]);
            resultJson.put("item_no",key[1]);
            resultJson.put("商品名称",key[1]);

            resultJson.put("分类",key[1]);
            resultJson.put("售罄_入库",itemData.getSale_cnt()+itemData.getSale_cnt());
            resultJson.put("售罄_销售",itemData.getSale_cnt());
            resultJson.put("售罄_库存",itemData.getSale_cnt());
            resultJson.put("售罄_售罄率",NumberUtil.getDouble2Format(itemData.getSale_cnt()/itemData.getSale_cnt()));
            resultJson.put("整体销售_金额",itemData.getTotal_sale());
            resultJson.put("整体销售_成本",itemData.getTotal_cost());
            resultJson.put("整体销售_数量",itemData.getSale_cnt());
            resultJson.put("整体销售_单数",itemData.getTotal_bill());
            resultJson.put("整体销售_客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getTotal_bill()));
            resultJson.put("整体销售_件均价",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getSale_cnt()));
            resultJson.put("整体销售_退货额",itemData.getTotal_ret());
            resultJson.put("整体销售_退货率",NumberUtil.getDouble2Format(itemData.getTotal_ret_cnt()/itemData.getSale_cnt()));
            resultJson.put("库存_库存数量",itemData.getStock_cnt());
            resultJson.put("库存_售价金额",itemData.getStock_sale());
            resultJson.put("库存_售价均价",NumberUtil.getDouble2Format(itemData.getStock_sale()/itemData.getStock_cnt()));
            resultJson.put("库存_成本金额",itemData.getStock_cost());
            resultJson.put("库存_成本均价",NumberUtil.getDouble2Format(itemData.getStock_cost()/itemData.getStock_cnt()));
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray itemSaleStock(String orgId,String itemNo, String begin, String end) throws BaseException {
        class ItemData{
            double total_sale;
            double stock_cnt;

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }
        }
        List<Object> objectList = unionSqlDao.query("SELECT sum(t1.sale_amt),sum(t1.sale_qty),sum(t1.bill_cnt),sum(t1.ret_amt),sum(t1.ret_qty),sum(t1.cost_amt),t1.item_no,\n" +
                "t2.stock_qty,t2.branch_no,t2.cost_price\n" +
                " FROM ItemDayEntity t1,StockEntity t2,ItemEntity t3 \n" +
                "where t1.item_no=t2.item_no and t3.item_no=t2.item_no and t1.item_no='"+itemNo+"' " +
                "and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' and t1.org_id='"+orgId+"' " +
                " ORDER BY t2.branch_no");
        Map<String,ItemData> dataMap = new HashMap<>();
        for(Object itemEntity: objectList){

            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[0];
            String item_no = (String)itemEntityObj[6];
            Double stock_cnt = (Double)itemEntityObj[7];
            String branch_no = (String)itemEntityObj[8];

            ItemData itemData = dataMap.get(branch_no+"@@"+item_no);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setStock_cnt(itemData.getStock_cnt()+(stock_cnt!=null?stock_cnt:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setStock_cnt(itemData.getStock_cnt()+(stock_cnt!=null?stock_cnt:0));
                dataMap.put(branch_no+"@@"+item_no,itemData);
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String[] key = entry.getKey().split("@@");
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店",key[0]);
            resultJson.put("月销量",itemData.getTotal_sale());
            resultJson.put("库存",itemData.getStock_cnt());
            results.add(resultJson);
        }
        return results;
    }
}
