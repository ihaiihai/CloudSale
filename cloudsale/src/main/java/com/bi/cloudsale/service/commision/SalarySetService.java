package com.bi.cloudsale.service.commision;

import java.util.List;

import com.bi.cloudsale.dto.commision.ItemInfoDto;

public interface SalarySetService {

	List<ItemInfoDto> queryByCondition(String orgId, String clsNo, String key, Integer pageIndex, Integer pageSize);
}
