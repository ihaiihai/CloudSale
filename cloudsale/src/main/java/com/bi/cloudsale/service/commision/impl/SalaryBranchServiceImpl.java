package com.bi.cloudsale.service.commision.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.dto.commision.SalaryBranchDto;
import com.bi.cloudsale.persistent.dao.commision.SalaryBranchDao;
import com.bi.cloudsale.persistent.entity.commision.SalaryBranchEntity;
import com.bi.cloudsale.service.commision.SalaryBranchService;
import com.bi.cloudsale.service.overall.impl.OperationServiceImpl;

@Service
@Transactional
public class SalaryBranchServiceImpl implements SalaryBranchService {
	
	private static final Logger log = LoggerFactory.getLogger(OperationServiceImpl.class);
	
	@Resource
	private SalaryBranchDao salaryBranchDao;

	@Override
	public List<SalaryBranchDto> queryByCondition(String orgId, Date startDate, Date endDate, String branchNos) {
		List<SalaryBranchDto> dtoList = new ArrayList<SalaryBranchDto>();
		List<SalaryBranchEntity> entityList = salaryBranchDao.queryByCondition(orgId, startDate, endDate, branchNos);
		try {
			BeanUtil.copyBeanPropertiesList(dtoList, entityList, SalaryBranchDto.class);
		} catch (BaseException e) {
			log.error(e.getMessage(), e);
		}
		return dtoList;
	}

	@Override
	public void updateConfirm(String orgId, String account, Date startDate, Date endDate, String branchNos,
			String salerIds) throws BaseException {
		try {
			salaryBranchDao.updateConfirm(orgId, account, startDate, endDate, branchNos, salerIds);
		}catch(HibernateException e) {
			throw new BaseException("更新失败", e);
		}
	}

}
