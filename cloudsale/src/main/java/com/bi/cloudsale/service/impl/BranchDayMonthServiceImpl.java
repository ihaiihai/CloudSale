package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.BranchDayMonthService;
import com.bi.cloudsale.service.HomePageService;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

@Service
@Transactional
public class BranchDayMonthServiceImpl implements BranchDayMonthService {

    private static final Logger log = LoggerFactory.getLogger(BranchDayMonthServiceImpl.class);

    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private ItemDayDao itemDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private VipDayDao vipDayDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {

        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray dayMonthBranchGainAbstract(String orgId,String branchNo) throws BaseException {


        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'"+branchSql);
        String branchName = "";
        if (CollectionUtils.isNotEmpty(branchEntities)) {
            branchName = branchEntities.get(0).getBranch_name();
        }

        Calendar cal_now= Calendar.getInstance();
        int currentDay = cal_now.get(Calendar.DAY_OF_MONTH);//今天多少号
        int leftDay = cal_now.getActualMaximum(Calendar.DAY_OF_MONTH)-currentDay;

        double todayAimSale = 0d;
        double monthAimSale = 0d;
        //今天最低任务
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 1 and cycle_month= cast(DATE_FORMAT(NOW(), '%m') as int)"+branchSql);
        if(CollectionUtils.isNotEmpty(aimEntities)){
            Double aim_value = aimEntities.get(0).getAim_value();
            todayAimSale +=(aim_value!=null?aim_value:0)/30;
            monthAimSale +=(aim_value!=null?aim_value:0);
        }

        double monthSale = 0d;
//        List<BranchDayEntity> thisMonthDayEntities = branchDayDao.list("where to_days(oper_date) <= (to_days(now()))" +
//                " and to_days(oper_date) >= (to_days(now())-day(now())+1) and org_id = '"+orgId+"'"+branchSql);
        List<Object> objects_year = unionSqlDao.query("select sum(sale_amt)as sale_amt from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) >= (to_days(now())-day(now())+1)"+
                branchSql);
        for(Object object : objects_year){
            Double total_sale = (Double)object;
            monthSale += (total_sale!=null?total_sale:0);
        }
        double respectLv = monthSale/currentDay*30/monthAimSale;
        JSONObject resultJson = new JSONObject();
        resultJson.put("value1",String.format("%s%s日销售报告",branchName,DateUtil.formatDate(new Date(),DateUtil.SIMPLE_DATE_FORMAT)));
        resultJson.put("value2",String.format("本月还剩：%s天 | 今日最低任务：%s元",leftDay,NumberUtil.getDouble2Format(todayAimSale)));
        resultJson.put("value3",String.format("本月任务：%s | 累计完成：%s | 完成率：%s | 预测完成率：%s",
                NumberUtil.getDouble2Format(monthAimSale),
                NumberUtil.getDouble2Format(monthSale),
                NumberUtil.getPercentFormat(monthSale/monthAimSale),
                NumberUtil.getPercentFormat(respectLv)));

        JSONArray results = new JSONArray();
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray yesterDayBranchGain(String orgId,String branchNo) throws BaseException {

        double yesTotalSale = 0d;
        double yesTotalCnt = 0L;
        long yesTotalBillCnt = 0L;
        long vipNew =0L;
        long vipBillCnt = 0L;
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
//        List<BranchDayEntity> thisMonthDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"'"+branchSql);
        List<Object> objects_year = unionSqlDao.query("select sum(sale_amt),sum(sale_qty),sum(bill_cnt),sum(vip_new),sum(bill_vip_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)"+
                branchSql);
        for(Object object : objects_year){
            Object[]itemEntityObj = (Object[])object;
            yesTotalSale += itemEntityObj[0]!=null?(Double)itemEntityObj[0]:0;
            yesTotalCnt += itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0;
            yesTotalBillCnt += itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0;
            vipNew += itemEntityObj[3]!=null?(Long)itemEntityObj[3]:0;
            vipBillCnt += itemEntityObj[4]!=null?(Long)itemEntityObj[4]:0;
        }
        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'"+branchSql);
        double area = 0d;
        if (CollectionUtils.isNotEmpty(branchEntities)) {
            area = branchEntities.get(0).getArea();
        }

        long salerCnt = 0L;
        List<SalerEntity> salerEntities = salerDao.list("where org_id = '"+orgId+"'"+branchSql);
        if(CollectionUtils.isNotEmpty(salerEntities)){
            salerCnt = salerEntities.size();
        }

        JSONObject resultJson = new JSONObject();
        resultJson.put("成交金额",yesTotalSale);
        resultJson.put("成交单数",yesTotalCnt);
        resultJson.put("客单价",NumberUtil.getDouble2Format(yesTotalSale/yesTotalBillCnt));
        resultJson.put("购物连带率",NumberUtil.getPercentFormat(yesTotalCnt/yesTotalBillCnt));
        resultJson.put("营业面积",area);
        resultJson.put("坪效",NumberUtil.getDouble2Format(yesTotalSale/area));
        resultJson.put("有效工号",salerCnt);
        resultJson.put("人均销售",NumberUtil.getDouble2Format(yesTotalSale/salerCnt));
        resultJson.put("新增会员",vipNew);
        resultJson.put("新增客户",yesTotalBillCnt-vipBillCnt);

        JSONArray results = new JSONArray();
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray yesterdayBranchBestItem(String orgId, String branchNo) throws BaseException {

        class ItemData{
            double total_sale;
            String item_name;
            String item_subno;
            double stock_cnt;
            double sale_cnt;

            public String getItem_subno() {
                return item_subno;
            }

            public void setItem_subno(String item_subno) {
                this.item_subno = item_subno;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }

            public double getSale_cnt() {
                return sale_cnt;
            }

            public void setSale_cnt(double sale_cnt) {
                this.sale_cnt = sale_cnt;
            }
        }

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = Maps.newLinkedHashMap();
        //1销量、销售额
//        List<ItemDayEntity> itemDayEntities = itemDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"'"+branchSql+" order by sale_qty desc",100);
        List<Object> objects = unionSqlDao.query("select item_no, sum(sale_amt),sum(sale_qty) from ItemDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)"+
                branchSql+" group by item_no order by sum(sale_qty)",1,100);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_no = (String)itemEntityObj[0];
            Double total_sale = (Double)itemEntityObj[1];
            Double total_sale_cnt = (Double)itemEntityObj[2];
            ItemData itemData = dataMap.get(item_no);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale() +  (total_sale!=null?total_sale:0));
                itemData.setSale_cnt(itemData.getSale_cnt()+ (total_sale_cnt!=null?total_sale_cnt:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale() +  (total_sale!=null?total_sale:0));
                itemData.setSale_cnt(itemData.getSale_cnt()+ (total_sale_cnt!=null?total_sale_cnt:0));
                dataMap.put(item_no,itemData);
            }
        }
        String item_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,branch_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_nosStr)){
                item_nosStr = item_nosStr + ",'"+str+"'";
            }else{
                item_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_nosStr)){
            //2商品名
            List<ItemEntity> itemEntities = itemDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'");
            for(ItemEntity itemEntity : itemEntities){
                dataMap.get(itemEntity.getItem_no()).setItem_name(itemEntity.getItem_name());
                dataMap.get(itemEntity.getItem_no()).setItem_subno(itemEntity.getItem_subno());
            }
            //3库存
            List<StockEntity> stockEntities = stockDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'"+branchSql);
            for(StockEntity stockEntity : stockEntities){
                dataMap.get(stockEntity.getItem_no()).setStock_cnt(stockEntity.getStock_qty());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("item_no",key);
            resultJson.put("条码",itemData.getItem_subno());
            resultJson.put("商品名称",itemData.getItem_name());
            resultJson.put("库存数量",itemData.getStock_cnt());
            resultJson.put("销售数量",itemData.getSale_cnt());
            resultJson.put("销售金额",itemData.getTotal_sale());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray todayLowestTarget(String orgId,String branchNo) throws BaseException {

        double todayAimSale = 0d;
        String dateTime = DateUtil.formatDate(new Date(),DateUtil.SIMPLE_DATE_FORMAT);
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        //今天最低任务
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_type=1 and aim_cycle = 1" +
                " and cycle_year=DATE_FORMAT(NOW(), '%Y') and cycle_month= cast(DATE_FORMAT(NOW(), '%m') as int)"+branchSql);
        if(CollectionUtils.isNotEmpty(aimEntities)){
            Double aim_value = aimEntities.get(0).getAim_value();
            todayAimSale +=(aim_value!=null?aim_value:0)/30;
        }
        //昨日销售
        double yesSale = 0d;
//        List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1) and org_id = '"+orgId+"'"+branchSql);
        List<Object> objects_yes = unionSqlDao.query("select sum(sale_amt)as sale_amt from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)"+
                branchSql);
        for(Object object : objects_yes){
            Double total_sale = (Double)object;
            yesSale += total_sale!=null?total_sale:0;
        }
        //1.昨日销售
        JSONObject yesSaleJson = new JSONObject();
        yesSaleJson.put("value",yesSale);

        JSONObject todayAimJson = new JSONObject();
        todayAimJson.put("今日最低任务",NumberUtil.getDouble2Format(todayAimSale));
        todayAimJson.put("昨日销售",NumberUtil.getDouble2Format(yesSale));

        JSONArray results = new JSONArray();
        results.add(todayAimJson);
        return results;
    }

    @Override
    public JSONArray monthGain(String orgId,String branchNo) throws BaseException {

        Calendar cal_pre= Calendar.getInstance();//本月首日
        cal_pre.add(Calendar.MONTH, 0);
        cal_pre.set(Calendar.DAY_OF_MONTH,1);
        String dateNo1 = DateUtil.formatDate(cal_pre.getTime(),DateUtil.SIMPLE_DATE_FORMAT);
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+dateNo1+"' and to_days(oper_date) <= (to_days(now()))"+branchSql);
        List<Object> objects_month = unionSqlDao.query("select sum(sale_amt)as sale_amt from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days(now())) and oper_date>='"+dateNo1+"'"+
                branchSql+"");
        double monthTotalSale = 0d;
        for(Object object : objects_month){
            monthTotalSale += (Double)object;
        }
        double monthAim = 0d;
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 0 and cycle_year= DATE_FORMAT(NOW(), '%Y')"+branchSql);
        if(CollectionUtils.isNotEmpty(aimEntities)){
            Double aim_value = aimEntities.get(0).getAim_value();
            monthAim +=(aim_value!=null?aim_value:0)/12;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("本月累计",monthTotalSale);
        jsonObject.put("本月任务",NumberUtil.getDouble2Format(monthAim));

        JSONArray results = new JSONArray();
        results.add(jsonObject);
        return results;
    }

    @Override
    public JSONArray yesterdayPeriod(String orgId, String branchNo) throws BaseException {

        class ItemData{
            double client_cnt;
            double total_sale;

            public double getClient_cnt() {
                return client_cnt;
            }

            public void setClient_cnt(double client_cnt) {
                this.client_cnt = client_cnt;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<Long,ItemData> dataMap = new HashMap<>();
        //1销量、销售额
        List<BranchTimeEntity> branchTimeEntities = branchTimeDao.list("where to_days(oper_date) = (to_days(now())-1)" +
                " and org_id = '"+orgId+"'"+branchSql);
        for(BranchTimeEntity branchTimeEntity:branchTimeEntities){
            Long hour00 = 0L;
            ItemData dataDay00 = dataMap.get(hour00);
            if(dataDay00!=null){
                dataDay00.setTotal_sale(dataDay00.getTotal_sale() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataDay00.setClient_cnt(dataDay00.getClient_cnt()+ (branchTimeEntity.getCnt_00()!=null?branchTimeEntity.getCnt_00():0));
            }else{
                dataDay00 = new ItemData();
                dataDay00.setTotal_sale(dataDay00.getTotal_sale() +  (branchTimeEntity.getAmt_00()!=null?branchTimeEntity.getAmt_00():0));
                dataDay00.setClient_cnt(dataDay00.getClient_cnt()+ (branchTimeEntity.getCnt_00()!=null?branchTimeEntity.getCnt_00():0));
                dataMap.put(hour00,dataDay00);
            }
            Long hour01 = 1L;
            ItemData dataDay01 = dataMap.get(hour01);
            if(dataDay01!=null){
                dataDay01.setTotal_sale(dataDay01.getTotal_sale() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataDay01.setClient_cnt(dataDay01.getClient_cnt()+ (branchTimeEntity.getCnt_01()!=null?branchTimeEntity.getCnt_01():0));
            }else{
                dataDay01 = new ItemData();
                dataDay01.setTotal_sale(dataDay01.getTotal_sale() +  (branchTimeEntity.getAmt_01()!=null?branchTimeEntity.getAmt_01():0));
                dataDay01.setClient_cnt(dataDay01.getClient_cnt()+ (branchTimeEntity.getCnt_01()!=null?branchTimeEntity.getCnt_01():0));
                dataMap.put(hour01,dataDay01);
            }
            Long hour02 = 2L;
            ItemData dataDay02 = dataMap.get(hour02);
            if(dataDay02!=null){
                dataDay02.setTotal_sale(dataDay02.getTotal_sale() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataDay02.setClient_cnt(dataDay02.getClient_cnt()+ (branchTimeEntity.getCnt_02()!=null?branchTimeEntity.getCnt_02():0));
            }else{
                dataDay02 = new ItemData();
                dataDay02.setTotal_sale(dataDay02.getTotal_sale() +  (branchTimeEntity.getAmt_02()!=null?branchTimeEntity.getAmt_02():0));
                dataDay02.setClient_cnt(dataDay02.getClient_cnt()+ (branchTimeEntity.getCnt_02()!=null?branchTimeEntity.getCnt_02():0));
                dataMap.put(hour02,dataDay02);
            }
            Long hour03 = 3L;
            ItemData dataDay03 = dataMap.get(hour03);
            if(dataDay03!=null){
                dataDay03.setTotal_sale(dataDay03.getTotal_sale() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataDay03.setClient_cnt(dataDay03.getClient_cnt()+ (branchTimeEntity.getCnt_03()!=null?branchTimeEntity.getCnt_03():0));
            }else{
                dataDay03 = new ItemData();
                dataDay03.setTotal_sale(dataDay03.getTotal_sale() +  (branchTimeEntity.getAmt_03()!=null?branchTimeEntity.getAmt_03():0));
                dataDay03.setClient_cnt(dataDay03.getClient_cnt()+ (branchTimeEntity.getCnt_03()!=null?branchTimeEntity.getCnt_03():0));
                dataMap.put(hour03,dataDay03);
            }
            Long hour04 = 4L;
            ItemData dataDay04 = dataMap.get(hour04);
            if(dataDay04!=null){
                dataDay04.setTotal_sale(dataDay04.getTotal_sale() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataDay04.setClient_cnt(dataDay04.getClient_cnt()+ (branchTimeEntity.getCnt_04()!=null?branchTimeEntity.getCnt_04():0));
            }else{
                dataDay04 = new ItemData();
                dataDay04.setTotal_sale(dataDay04.getTotal_sale() +  (branchTimeEntity.getAmt_04()!=null?branchTimeEntity.getAmt_04():0));
                dataDay04.setClient_cnt(dataDay04.getClient_cnt()+ (branchTimeEntity.getCnt_04()!=null?branchTimeEntity.getCnt_04():0));
                dataMap.put(hour04,dataDay04);
            }
            Long hour05 = 5L;
            ItemData dataDay05 = dataMap.get(hour05);
            if(dataDay05!=null){
                dataDay05.setTotal_sale(dataDay05.getTotal_sale() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataDay05.setClient_cnt(dataDay05.getClient_cnt()+ (branchTimeEntity.getCnt_05()!=null?branchTimeEntity.getCnt_05():0));
            }else{
                dataDay05 = new ItemData();
                dataDay05.setTotal_sale(dataDay05.getTotal_sale() +  (branchTimeEntity.getAmt_05()!=null?branchTimeEntity.getAmt_05():0));
                dataDay05.setClient_cnt(dataDay05.getClient_cnt()+ (branchTimeEntity.getCnt_05()!=null?branchTimeEntity.getCnt_05():0));
                dataMap.put(hour05,dataDay05);
            }
            Long hour06 = 6L;
            ItemData dataDay06 = dataMap.get(hour06);
            if(dataDay06!=null){
                dataDay06.setTotal_sale(dataDay06.getTotal_sale() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataDay06.setClient_cnt(dataDay06.getClient_cnt()+ (branchTimeEntity.getCnt_06()!=null?branchTimeEntity.getCnt_06():0));
            }else{
                dataDay06 = new ItemData();
                dataDay06.setTotal_sale(dataDay06.getTotal_sale() +  (branchTimeEntity.getAmt_06()!=null?branchTimeEntity.getAmt_06():0));
                dataDay06.setClient_cnt(dataDay06.getClient_cnt()+ (branchTimeEntity.getCnt_06()!=null?branchTimeEntity.getCnt_06():0));
                dataMap.put(hour06,dataDay06);
            }
            Long hour07 = 7L;
            ItemData dataDay07 = dataMap.get(hour07);
            if(dataDay07!=null){
                dataDay07.setTotal_sale(dataDay07.getTotal_sale() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataDay07.setClient_cnt(dataDay07.getClient_cnt()+ (branchTimeEntity.getCnt_07()!=null?branchTimeEntity.getCnt_07():0));
            }else{
                dataDay07 = new ItemData();
                dataDay07.setTotal_sale(dataDay07.getTotal_sale() +  (branchTimeEntity.getAmt_07()!=null?branchTimeEntity.getAmt_07():0));
                dataDay07.setClient_cnt(dataDay07.getClient_cnt()+ (branchTimeEntity.getCnt_07()!=null?branchTimeEntity.getCnt_07():0));
                dataMap.put(hour07,dataDay07);
            }
            Long hour08 = 8L;
            ItemData dataDay08 = dataMap.get(hour08);
            if(dataDay08!=null){
                dataDay08.setTotal_sale(dataDay08.getTotal_sale() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataDay08.setClient_cnt(dataDay08.getClient_cnt()+ (branchTimeEntity.getCnt_08()!=null?branchTimeEntity.getCnt_08():0));
            }else{
                dataDay08 = new ItemData();
                dataDay08.setTotal_sale(dataDay08.getTotal_sale() +  (branchTimeEntity.getAmt_08()!=null?branchTimeEntity.getAmt_08():0));
                dataDay08.setClient_cnt(dataDay08.getClient_cnt()+ (branchTimeEntity.getCnt_08()!=null?branchTimeEntity.getCnt_08():0));
                dataMap.put(hour08,dataDay08);
            }
            Long hour09 = 9L;
            ItemData dataDay09 = dataMap.get(hour09);
            if(dataDay09!=null){
                dataDay09.setTotal_sale(dataDay09.getTotal_sale() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataDay09.setClient_cnt(dataDay09.getClient_cnt()+ (branchTimeEntity.getCnt_09()!=null?branchTimeEntity.getCnt_09():0));
            }else{
                dataDay09 = new ItemData();
                dataDay09.setTotal_sale(dataDay09.getTotal_sale() +  (branchTimeEntity.getAmt_09()!=null?branchTimeEntity.getAmt_09():0));
                dataDay09.setClient_cnt(dataDay09.getClient_cnt()+ (branchTimeEntity.getCnt_09()!=null?branchTimeEntity.getCnt_09():0));
                dataMap.put(hour09,dataDay09);
            }
            Long hour10 = 10L;
            ItemData dataDay10 = dataMap.get(hour10);
            if(dataDay10!=null){
                dataDay10.setTotal_sale(dataDay10.getTotal_sale() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataDay10.setClient_cnt(dataDay10.getClient_cnt()+ (branchTimeEntity.getCnt_10()!=null?branchTimeEntity.getCnt_10():0));
            }else{
                dataDay10 = new ItemData();
                dataDay10.setTotal_sale(dataDay10.getTotal_sale() +  (branchTimeEntity.getAmt_10()!=null?branchTimeEntity.getAmt_10():0));
                dataDay10.setClient_cnt(dataDay10.getClient_cnt()+ (branchTimeEntity.getCnt_10()!=null?branchTimeEntity.getCnt_10():0));
                dataMap.put(hour10,dataDay10);
            }
            Long hour11 = 11L;
            ItemData dataDay11 = dataMap.get(hour11);
            if(dataDay11!=null){
                dataDay11.setTotal_sale(dataDay11.getTotal_sale() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataDay11.setClient_cnt(dataDay11.getClient_cnt()+ (branchTimeEntity.getCnt_11()!=null?branchTimeEntity.getCnt_11():0));
            }else{
                dataDay11 = new ItemData();
                dataDay11.setTotal_sale(dataDay11.getTotal_sale() +  (branchTimeEntity.getAmt_11()!=null?branchTimeEntity.getAmt_11():0));
                dataDay11.setClient_cnt(dataDay11.getClient_cnt()+ (branchTimeEntity.getCnt_11()!=null?branchTimeEntity.getCnt_11():0));
                dataMap.put(hour11,dataDay11);
            }
            Long hour12 = 12L;
            ItemData dataDay12 = dataMap.get(hour12);
            if(dataDay12!=null){
                dataDay12.setTotal_sale(dataDay12.getTotal_sale() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataDay12.setClient_cnt(dataDay12.getClient_cnt()+ (branchTimeEntity.getCnt_12()!=null?branchTimeEntity.getCnt_12():0));
            }else{
                dataDay12 = new ItemData();
                dataDay12.setTotal_sale(dataDay12.getTotal_sale() +  (branchTimeEntity.getAmt_12()!=null?branchTimeEntity.getAmt_12():0));
                dataDay12.setClient_cnt(dataDay12.getClient_cnt()+ (branchTimeEntity.getCnt_12()!=null?branchTimeEntity.getCnt_12():0));
                dataMap.put(hour12,dataDay12);
            }
            Long hour13 = 13L;
            ItemData dataDay13 = dataMap.get(hour13);
            if(dataDay13!=null){
                dataDay13.setTotal_sale(dataDay13.getTotal_sale() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataDay13.setClient_cnt(dataDay13.getClient_cnt()+ (branchTimeEntity.getCnt_13()!=null?branchTimeEntity.getCnt_13():0));
            }else{
                dataDay13 = new ItemData();
                dataDay13.setTotal_sale(dataDay13.getTotal_sale() +  (branchTimeEntity.getAmt_13()!=null?branchTimeEntity.getAmt_13():0));
                dataDay13.setClient_cnt(dataDay13.getClient_cnt()+ (branchTimeEntity.getCnt_13()!=null?branchTimeEntity.getCnt_13():0));
                dataMap.put(hour13,dataDay13);
            }
            Long hour14 = 14L;
            ItemData dataDay14 = dataMap.get(hour14);
            if(dataDay14!=null){
                dataDay14.setTotal_sale(dataDay14.getTotal_sale() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataDay14.setClient_cnt(dataDay14.getClient_cnt()+ (branchTimeEntity.getCnt_14()!=null?branchTimeEntity.getCnt_14():0));
            }else{
                dataDay14 = new ItemData();
                dataDay14.setTotal_sale(dataDay14.getTotal_sale() +  (branchTimeEntity.getAmt_14()!=null?branchTimeEntity.getAmt_14():0));
                dataDay14.setClient_cnt(dataDay14.getClient_cnt()+ (branchTimeEntity.getCnt_14()!=null?branchTimeEntity.getCnt_14():0));
                dataMap.put(hour14,dataDay14);
            }
            Long hour15 = 15L;
            ItemData dataDay15 = dataMap.get(hour15);
            if(dataDay15!=null){
                dataDay15.setTotal_sale(dataDay15.getTotal_sale() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataDay15.setClient_cnt(dataDay15.getClient_cnt()+ (branchTimeEntity.getCnt_15()!=null?branchTimeEntity.getCnt_15():0));
            }else{
                dataDay15 = new ItemData();
                dataDay15.setTotal_sale(dataDay15.getTotal_sale() +  (branchTimeEntity.getAmt_15()!=null?branchTimeEntity.getAmt_15():0));
                dataDay15.setClient_cnt(dataDay15.getClient_cnt()+ (branchTimeEntity.getCnt_15()!=null?branchTimeEntity.getCnt_15():0));
                dataMap.put(hour15,dataDay15);
            }
            Long hour16 = 16L;
            ItemData dataDay16 = dataMap.get(hour16);
            if(dataDay16!=null){
                dataDay16.setTotal_sale(dataDay16.getTotal_sale() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataDay16.setClient_cnt(dataDay16.getClient_cnt()+ (branchTimeEntity.getCnt_16()!=null?branchTimeEntity.getCnt_16():0));
            }else{
                dataDay16 = new ItemData();
                dataDay16.setTotal_sale(dataDay16.getTotal_sale() +  (branchTimeEntity.getAmt_16()!=null?branchTimeEntity.getAmt_16():0));
                dataDay16.setClient_cnt(dataDay16.getClient_cnt()+ (branchTimeEntity.getCnt_16()!=null?branchTimeEntity.getCnt_16():0));
                dataMap.put(hour16,dataDay16);
            }
            Long hour17 = 17L;
            ItemData dataDay17 = dataMap.get(hour17);
            if(dataDay17!=null){
                dataDay17.setTotal_sale(dataDay17.getTotal_sale() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataDay17.setClient_cnt(dataDay17.getClient_cnt()+ (branchTimeEntity.getCnt_17()!=null?branchTimeEntity.getCnt_17():0));
            }else{
                dataDay17 = new ItemData();
                dataDay17.setTotal_sale(dataDay17.getTotal_sale() +  (branchTimeEntity.getAmt_17()!=null?branchTimeEntity.getAmt_17():0));
                dataDay17.setClient_cnt(dataDay17.getClient_cnt()+ (branchTimeEntity.getCnt_17()!=null?branchTimeEntity.getCnt_17():0));
                dataMap.put(hour17,dataDay17);
            }
            Long hour18 =18L;
            ItemData dataDay18 = dataMap.get(hour18);
            if(dataDay18!=null){
                dataDay18.setTotal_sale(dataDay18.getTotal_sale() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataDay18.setClient_cnt(dataDay18.getClient_cnt()+ (branchTimeEntity.getCnt_18()!=null?branchTimeEntity.getCnt_18():0));
            }else{
                dataDay18 = new ItemData();
                dataDay18.setTotal_sale(dataDay18.getTotal_sale() +  (branchTimeEntity.getAmt_18()!=null?branchTimeEntity.getAmt_18():0));
                dataDay18.setClient_cnt(dataDay18.getClient_cnt()+ (branchTimeEntity.getCnt_18()!=null?branchTimeEntity.getCnt_18():0));
                dataMap.put(hour18,dataDay18);
            }
            Long hour19 = 19L;
            ItemData dataDay19 = dataMap.get(hour19);
            if(dataDay19!=null){
                dataDay19.setTotal_sale(dataDay19.getTotal_sale() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataDay19.setClient_cnt(dataDay19.getClient_cnt()+ (branchTimeEntity.getCnt_19()!=null?branchTimeEntity.getCnt_19():0));
            }else{
                dataDay19 = new ItemData();
                dataDay19.setTotal_sale(dataDay19.getTotal_sale() +  (branchTimeEntity.getAmt_19()!=null?branchTimeEntity.getAmt_19():0));
                dataDay19.setClient_cnt(dataDay19.getClient_cnt()+ (branchTimeEntity.getCnt_19()!=null?branchTimeEntity.getCnt_19():0));
                dataMap.put(hour19,dataDay19);
            }
            Long hour20 = 20L;
            ItemData dataDay20 = dataMap.get(hour20);
            if(dataDay20!=null){
                dataDay20.setTotal_sale(dataDay20.getTotal_sale() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataDay20.setClient_cnt(dataDay20.getClient_cnt()+ (branchTimeEntity.getCnt_20()!=null?branchTimeEntity.getCnt_20():0));
            }else{
                dataDay20 = new ItemData();
                dataDay20.setTotal_sale(dataDay20.getTotal_sale() +  (branchTimeEntity.getAmt_20()!=null?branchTimeEntity.getAmt_20():0));
                dataDay20.setClient_cnt(dataDay20.getClient_cnt()+ (branchTimeEntity.getCnt_20()!=null?branchTimeEntity.getCnt_20():0));
                dataMap.put(hour20,dataDay20);
            }
            Long hour21 = 21L;
            ItemData dataDay21 = dataMap.get(hour21);
            if(dataDay21!=null){
                dataDay21.setTotal_sale(dataDay21.getTotal_sale() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataDay21.setClient_cnt(dataDay21.getClient_cnt()+ (branchTimeEntity.getCnt_21()!=null?branchTimeEntity.getCnt_21():0));
            }else{
                dataDay21 = new ItemData();
                dataDay21.setTotal_sale(dataDay21.getTotal_sale() +  (branchTimeEntity.getAmt_21()!=null?branchTimeEntity.getAmt_21():0));
                dataDay21.setClient_cnt(dataDay21.getClient_cnt()+ (branchTimeEntity.getCnt_21()!=null?branchTimeEntity.getCnt_21():0));
                dataMap.put(hour21,dataDay21);
            }
            Long hour22 = 22L;
            ItemData dataDay22 = dataMap.get(hour22);
            if(dataDay22!=null){
                dataDay22.setTotal_sale(dataDay22.getTotal_sale() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataDay22.setClient_cnt(dataDay22.getClient_cnt()+ (branchTimeEntity.getCnt_22()!=null?branchTimeEntity.getCnt_22():0));
            }else{
                dataDay22 = new ItemData();
                dataDay22.setTotal_sale(dataDay22.getTotal_sale() +  (branchTimeEntity.getAmt_22()!=null?branchTimeEntity.getAmt_22():0));
                dataDay22.setClient_cnt(dataDay22.getClient_cnt()+ (branchTimeEntity.getCnt_22()!=null?branchTimeEntity.getCnt_22():0));
                dataMap.put(hour22,dataDay22);
            }
            Long hour23 = 23L;
            ItemData dataDay23 = dataMap.get(hour23);
            if(dataDay23!=null){
                dataDay23.setTotal_sale(dataDay23.getTotal_sale() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataDay23.setClient_cnt(dataDay23.getClient_cnt()+ (branchTimeEntity.getCnt_23()!=null?branchTimeEntity.getCnt_23():0));
            }else{
                dataDay23 = new ItemData();
                dataDay23.setTotal_sale(dataDay23.getTotal_sale() +  (branchTimeEntity.getAmt_23()!=null?branchTimeEntity.getAmt_23():0));
                dataDay23.setClient_cnt(dataDay23.getClient_cnt()+ (branchTimeEntity.getCnt_23()!=null?branchTimeEntity.getCnt_23():0));
                dataMap.put(hour23,dataDay23);
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<Long, ItemData> entry : dataMap.entrySet()) {
            Long key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("时间",key);
            resultJson.put("客流",itemData.getClient_cnt());
            resultJson.put("客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getClient_cnt()));
            resultJson.put("时段销售额",itemData.getTotal_sale());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray yesterdayCls(String orgId, String branchNo) throws BaseException {

        class ItemData{
            double tatal_sale;
            String cls_name;

            public double getTatal_sale() {
                return tatal_sale;
            }

            public void setTatal_sale(double tatal_sale) {
                this.tatal_sale = tatal_sale;
            }

            public String getCls_name() {
                return cls_name;
            }

            public void setCls_name(String cls_name) {
                this.cls_name = cls_name;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new HashMap<>();
        //1分类销售额
//        List<ClsDayEntity>  clsDayEntities = clsDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//                " and org_id = '"+orgId+"'"+branchSql);
        List<Object> objects_year = unionSqlDao.query("select item_clsno as item_clsno, sum(sale_amt)as sale_amt from ClsDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)"+
                branchSql+"  group by item_clsno");
        for(Object object : objects_year){
            Object[]itemEntityObj = (Object[])object;
            String clsno = (String)itemEntityObj[0];
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(clsno);
            if(itemData!=null){
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTatal_sale(itemData.getTatal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(clsno,itemData);
            }
        }
        //2对应分类名
        String cls_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(cls_nosStr)){
                cls_nosStr = cls_nosStr + ",'"+str+"'";
            }else{
                cls_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(cls_nosStr)){
            List<ItemClsEntity> itemClsEntities = itemClsDao.list("where item_clsno in("+cls_nosStr+") and org_id = '"+orgId+"'");
            for(ItemClsEntity itemClsEntity : itemClsEntities){
                dataMap.get(itemClsEntity.getItem_clsno()).setCls_name(itemClsEntity.getItem_clsname());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",itemData.getCls_name());
            resultJson.put("value",itemData.getTatal_sale());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray yesterdayPriceZone(String orgId, String branchNo) throws BaseException {

        class ItemData{
            double count;
            String zone_name;
            public double getCount() {
                return count;
            }

            public void setCount(double count) {
                this.count = count;
            }

            public String getZone_name() {
                return zone_name;
            }

            public void setZone_name(String zone_name) {
                this.zone_name = zone_name;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        //1价格区间
        List<PriceZoneEntity>  priceZoneEntities = priceZoneDao.list("where org_id = '"+orgId+"'"+branchSql);
        for(PriceZoneEntity priceZoneEntity:priceZoneEntities){
            String zoneNo = priceZoneEntity.getPrice_zone_no();
            ItemData itemData = dataMap.get(zoneNo);
            if(itemData!=null){
                itemData.setZone_name(priceZoneEntity.getPrice_zone_name());
            }else{
                itemData = new ItemData();
                itemData.setZone_name(priceZoneEntity.getPrice_zone_name());
                dataMap.put(zoneNo,itemData);
            }
        }
        //2客单价 暂时无法计算
        //todo 客单价 暂时无法计算

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",itemData.getZone_name());
            resultJson.put("value",itemData.getCount());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray yesterdayBranchBadItem(String orgId, String branchNo) throws BaseException {

        class ItemData{
            String item_subno;
            String item_name;
            double cost_amt;
            double stock_cnt;
            String last_sale_day;

            public String getItem_subno() {
                return item_subno;
            }

            public void setItem_subno(String item_subno) {
                this.item_subno = item_subno;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getCost_amt() {
                return cost_amt;
            }

            public void setCost_amt(double cost_amt) {
                this.cost_amt = cost_amt;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }

            public String getLast_sale_day() {
                return last_sale_day;
            }

            public void setLast_sale_day(String last_sale_day) {
                this.last_sale_day = last_sale_day;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new HashMap<>();
        //1库存
        List<StockEntity> stockEntities = stockDao.list("where org_id = '"+orgId+"'"+branchSql+" order by stock_qty desc",100);
        for(StockEntity stockEntity : stockEntities){
            String itemNo = stockEntity.getItem_no();
            ItemData dataDay = dataMap.get(itemNo);
            if(dataDay!=null){
                dataDay.setStock_cnt(stockEntity.getStock_qty());
                dataDay.setCost_amt(stockEntity.getCost_amt()!=null?stockEntity.getCost_amt():0);
            }else{
                dataDay = new ItemData();
                dataDay.setStock_cnt(stockEntity.getStock_qty());
                dataDay.setCost_amt(stockEntity.getCost_amt()!=null?stockEntity.getCost_amt():0);
                dataMap.put(itemNo,dataDay);
            }
        }
        String item_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,branch_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_nosStr)){
                item_nosStr = item_nosStr + ",'"+str+"'";
            }else{
                item_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_nosStr)){
            //2最近销售在哪一天
            List<ItemDayEntity> itemDayEntities = itemDayDao.list("where org_id = '"+orgId+"'"+branchSql+" and item_no in ("+item_nosStr+") order by oper_date desc",1);
            for(ItemDayEntity itemDayEntity:itemDayEntities){
                String itemNo = itemDayEntity.getItem_no();
                ItemData dataDay = dataMap.get(itemNo);
                if(dataDay!=null&&StringUtils.isEmpty(dataDay.getLast_sale_day())){
                    dataDay.setLast_sale_day(DateUtil.formatDate(new Date(itemDayEntity.getOper_date().getTime()),DateUtil.SIMPLE_DATE_FORMAT));
                }
            }

            //2商品名
            List<ItemEntity> itemEntities = itemDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'");
            for(ItemEntity itemEntity : itemEntities){
                ItemData dataDay = dataMap.get(itemEntity.getItem_no());
                if(dataDay!=null){
                    dataDay.setItem_name(itemEntity.getItem_name());
                    dataDay.setItem_subno(itemEntity.getItem_subno());
                }
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("item_no",key);
            resultJson.put("条码",itemData.getItem_subno());
            resultJson.put("商品名称",itemData.getItem_name());
            resultJson.put("库存数量",itemData.getStock_cnt());
            resultJson.put("库存金额",NumberUtil.getDouble2Format(itemData.getCost_amt()*itemData.getStock_cnt()));
            resultJson.put("最后销售日期",itemData.getLast_sale_day());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray yesterdayBranchExceptionStockItem(String orgId, String branchNo) throws BaseException {

        class ItemData{
            String item_subno;
            String item_name;
            double cost_amt;
            double stock_cnt;
            double sale_price;

            public String getItem_subno() {
                return item_subno;
            }

            public void setItem_subno(String item_subno) {
                this.item_subno = item_subno;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getCost_amt() {
                return cost_amt;
            }

            public void setCost_amt(double cost_amt) {
                this.cost_amt = cost_amt;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }

            public double getSale_price() {
                return sale_price;
            }

            public void setSale_price(double sale_price) {
                this.sale_price = sale_price;
            }
        }
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and t.branch_no = '"+branchNo+"'";
        Map<String,ItemData> dataMap = new HashMap<>();
        //1库存
        List<Object> stockEntities = stockDao.listBySql("select t.stock_qty*t.cost_amt as total_cost,t.id,t.user_id,t.branch_no,t.cost_price,t.max_qty,t.min_qty,t.item_no,t.stock_qty,t.cost_amt" +
                " from StockEntity t " +
                "where t.org_id = '"+orgId+"'"+branchSql+" order by total_cost desc",100);
        for(Object stockEntity : stockEntities){
            String itemNo = ((Object[])stockEntity)[7].toString();
            ItemData dataDay = dataMap.get(itemNo);
            if(dataDay!=null){
                dataDay.setStock_cnt(((Object[])stockEntity)[8]!=null?Double.valueOf(((Object[])stockEntity)[8].toString()):0);
                dataDay.setCost_amt(((Object[])stockEntity)[9]!=null?Double.valueOf(((Object[])stockEntity)[9].toString()):0);
            }else{
                dataDay = new ItemData();
                dataDay.setStock_cnt(((Object[])stockEntity)[8]!=null?Double.valueOf(((Object[])stockEntity)[8].toString()):0);
                dataDay.setCost_amt(((Object[])stockEntity)[9]!=null?Double.valueOf(((Object[])stockEntity)[9].toString()):0);
                dataMap.put(itemNo,dataDay);
            }
        }
        String item_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,branch_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_nosStr)){
                item_nosStr = item_nosStr + ",'"+str+"'";
            }else{
                item_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_nosStr)){
            //2商品名
            List<ItemEntity> itemEntities = itemDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'");
            for(ItemEntity itemEntity : itemEntities){
                ItemData dataDay = dataMap.get(itemEntity.getItem_no());
                if(dataDay!=null){
                    dataDay.setItem_name(itemEntity.getItem_name());
                    dataDay.setItem_subno(itemEntity.getItem_subno());
                    dataDay.setSale_price(itemEntity.getSale_price());
                }
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("item_no",key);
            resultJson.put("条码",itemData.getItem_subno());
            resultJson.put("商品名称",itemData.getItem_name());
            resultJson.put("库存数量",itemData.getStock_cnt());
            resultJson.put("库存金额",NumberUtil.getDouble2Format(itemData.getCost_amt()*itemData.getStock_cnt()));
            resultJson.put("零售单价",itemData.getSale_price());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray monthBranchTargetView(String orgId, String branchNo,String dateBegin,String dateEnd) throws BaseException {

        double monthAim =0d;
        double monthSale=0d;
        double monthCost =0d;
        double monthSaleCnt =0d;
        long monthSaleBillCnt = 0L;

        double lastYearMonthSale =0d;
        double lastYearMonthSaleCnt =0d;
        long lastYearMonthSaleBillCnt =0L;

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 0 and cycle_year= DATE_FORMAT(NOW(), '%Y')"+branchSql);
        if(CollectionUtils.isNotEmpty(aimEntities)){
            Double aim_value = aimEntities.get(0).getAim_value();
            monthAim +=(aim_value!=null?aim_value:0)/12;
        }
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+dateBegin+"' and oper_date <= '"+ dateEnd+"'" + branchSql);
        List<Object> objects = unionSqlDao.query("select sum(sale_amt),sum(cost_amt),sum(sale_qty),sum(bill_cnt) from BrandDayEntity where org_id ='"+orgId+"' and oper_date>='"+dateBegin+"' and oper_date <= '"+ dateEnd+"'"+
                branchSql);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            monthSale += itemEntityObj[0]!=null?(Double)itemEntityObj[0]:0;
            monthCost +=itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0;
            monthSaleCnt += itemEntityObj[2]!=null?(Double)itemEntityObj[2]:0;
            monthSaleBillCnt += itemEntityObj[3]!=null?(Long)itemEntityObj[3]:0;
        }

//        List<BranchDayEntity>branchDayEntities_lastyear= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+dateBegin+"')-365) and to_days(oper_date) <= (to_days('"+ dateEnd +"')-365)"+ branchSql);
        List<Object> objects_lastyear = unionSqlDao.query("select sum(sale_amt),sum(cost_amt),sum(bill_cnt) from BrandDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+dateBegin+"')-365) and to_days(oper_date) <= (to_days('"+ dateEnd +"')-365)"+
                branchSql);
        for(Object object : objects_lastyear){
            Object[]itemEntityObj = (Object[])object;
            lastYearMonthSale += itemEntityObj[0]!=null?(Double)itemEntityObj[0]:0;
            lastYearMonthSaleCnt += itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0;
            lastYearMonthSaleBillCnt += itemEntityObj[2]!=null?(Long)itemEntityObj[2]:0;
        }

        JSONArray results = new JSONArray();
        JSONObject resultJson1 = new JSONObject();
        resultJson1.put("name","销售完成");
        resultJson1.put("value1",NumberUtil.getDouble2Format(monthSale));
        resultJson1.put("value2","完成率"+NumberUtil.getPercentFormat(monthSale/monthAim));
        results.add(resultJson1);

        JSONObject resultJson2 = new JSONObject();
        resultJson2.put("name","毛利");
        resultJson2.put("value1",NumberUtil.getDouble2Format(monthSale-monthCost));
        resultJson2.put("value2","毛利率"+NumberUtil.getPercentFormat(monthCost/monthSale));
        results.add(resultJson2);

        JSONObject resultJson3 = new JSONObject();
        resultJson3.put("name","成交单数");
        resultJson3.put("value1",NumberUtil.getDouble2Format(monthSaleCnt));
        resultJson3.put("value2","同比"+(lastYearMonthSaleCnt>monthSaleCnt?
                ("下降"+(lastYearMonthSaleCnt-monthSaleCnt))
                :("上涨"+(monthSaleCnt-lastYearMonthSaleCnt))));
        results.add(resultJson3);

        JSONObject resultJson4 = new JSONObject();
        resultJson4.put("name","客单价");
        double clientAmt = monthSale/monthSaleCnt;
        resultJson4.put("value1", NumberUtil.getDouble2Format(clientAmt));
        double lastClientAmt = lastYearMonthSale/lastYearMonthSaleCnt;
        resultJson4.put("value2","同比"+(lastClientAmt>clientAmt?
                ("下降"+NumberUtil.getDouble2Format((lastClientAmt-clientAmt)))
                :("上涨"+NumberUtil.getDouble2Format((clientAmt-lastClientAmt)))));
        results.add(resultJson4);

        JSONObject resultJson5 = new JSONObject();
        resultJson5.put("name","连带率");
        double monthLiandailv = monthSaleCnt/monthSaleBillCnt;
        resultJson5.put("value1",NumberUtil.getDouble2Format(monthLiandailv));
        double lastYearMonthLiandailv = lastYearMonthSaleCnt/lastYearMonthSaleBillCnt;
        resultJson5.put("value2","同比"+(lastYearMonthLiandailv>monthLiandailv?
                ("下降"+NumberUtil.getDouble2Format((lastYearMonthLiandailv-monthLiandailv)))
                :("上涨"+NumberUtil.getDouble2Format((monthLiandailv-lastYearMonthLiandailv)))));
        results.add(resultJson5);

        return results;
    }

    @Override
    public JSONArray monthBranchVipView(String orgId, String branchNo, String dateBegin, String dateEnd) throws BaseException {


        double total_sale_cnt = 0d;
        double total_sale = 0d;

        long vip_active_cnt = 0L;
        double vip_total_sale = 0d;

        long vip_total_cnt = 0L;
        long vip_new_cnt = 0L;


        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')" + branchSql);
        List<Object> objects = unionSqlDao.query("select sum(sale_amt),sum(sale_qty) from BrandDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')"+
                branchSql);
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            total_sale += itemEntityObj[0]!=null?(Double)itemEntityObj[0]:0;
            total_sale_cnt +=itemEntityObj[1]!=null?(Double)itemEntityObj[1]:0;
        }
//        List<VipDayEntity>vipDayEntities= vipDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')" + branchSql);
        List<Object> objects_vip_cnt = unionSqlDao.query("select count(id) from VipDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')"+
                branchSql);
        for(Object object : objects_vip_cnt){
            vip_active_cnt = (Long)object;
        }
        List<Object> objects_vip = unionSqlDao.query("select sum(sale_amt) from VipDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=to_days('"+dateBegin+"') and to_days(oper_date) <= to_days('"+ dateEnd+"')"+
                branchSql);
        for(Object object : objects_vip){
            vip_total_sale += object!=null?(Double)object:0;
        }

        List<VipEntity> vipEntities_new = vipDao.list("where org_id = '"+orgId+"' " +
                "and to_days(create_date)>=to_days('"+dateBegin+"') and to_days(create_date) <= to_days('"+ dateEnd+"')" + branchSql);
        if(CollectionUtils.isNotEmpty(vipEntities_new)){
            vip_new_cnt = vipEntities_new.size();
        }

        List<VipEntity> vipEntities_all = vipDao.list("where org_id = '"+orgId+"' and to_days(create_date) <= to_days('"+ dateEnd+"')" + branchSql);
        if(CollectionUtils.isNotEmpty(vipEntities_all)){
            vip_total_cnt = vipEntities_all.size();
        }

        JSONArray results = new JSONArray();
        JSONObject resultJson1 = new JSONObject();
        resultJson1.put("name","在册人数");
        resultJson1.put("value1",vip_total_cnt);
        resultJson1.put("value2","365天内办卡或消费");
        results.add(resultJson1);

        JSONObject resultJson2 = new JSONObject();
        resultJson2.put("name","新增会员");
        resultJson2.put("value1",vip_new_cnt);
        resultJson2.put("value2","新增率"+NumberUtil.getPercentFormat(vip_new_cnt/vip_total_cnt));
        results.add(resultJson2);

        JSONObject resultJson3 = new JSONObject();
        resultJson3.put("name","活跃客户");
        resultJson3.put("value1",vip_active_cnt);
        resultJson3.put("value2","活跃度"+NumberUtil.getPercentFormat(vip_active_cnt/vip_total_cnt));
        results.add(resultJson3);

        JSONObject resultJson4 = new JSONObject();
        resultJson4.put("name","会员贡献");
        resultJson4.put("value1",vip_total_sale);
        resultJson4.put("value2","贡献占比"+NumberUtil.getPercentFormat(vip_total_sale/total_sale));
        results.add(resultJson4);

        JSONObject resultJson5 = new JSONObject();
        resultJson5.put("name","人均贡献");
        resultJson5.put("value1",NumberUtil.getDouble2Format(vip_total_sale/vip_total_cnt));
        resultJson5.put("value2","会员人均消费金额");
        results.add(resultJson5);

        JSONObject resultJson6 = new JSONObject();
        resultJson6.put("name","人均数量");
        resultJson6.put("value1",NumberUtil.getDouble2Format(total_sale_cnt/vip_total_cnt));
        resultJson6.put("value2","会员人均购买数量");
        results.add(resultJson6);

        return results;
    }

    @Override
    public JSONArray monthBranchSaleWanchenglv(String orgId, String branchNo, String beginDate, String endDate) throws BaseException {

        double monthAimSale = 0d;
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        //月任务
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 0 and cycle_year= DATE_FORMAT('"+beginDate+"', '%Y')"+branchSql);
        if(CollectionUtils.isNotEmpty(aimEntities)){
            Double aim_value = aimEntities.get(0).getAim_value();
            monthAimSale +=(aim_value!=null?aim_value:0)/12;
        }
        //月完成
        double monthSale = 0d;
//        List<BranchDayEntity> branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'" + branchSql);
        List<Object> objects = unionSqlDao.query("select sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'"+
                branchSql);
        for(Object object : objects){
            monthSale +=object!=null?(Double)object:0;
        }
        JSONArray results = new JSONArray();
        JSONObject resultJson = new JSONObject();
        resultJson.put("value",NumberUtil.getPercentFormat(monthSale/monthAimSale));
        results.add(resultJson);
        return results;
    }

    @Override
    public JSONArray monthBranchSaleBodong(String orgId, String branchNo, String beginDate, String endDate) throws BaseException {

        class ItemData{
            double total_sale;
            double total_sale_last_month;
            double total_sale_last_year;

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }
        }

        Map<Integer,ItemData> dataMap = Maps.newLinkedHashMap();
        //本期
        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
//        List<BranchDayEntity> branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'" + branchSql);
        List<Object> objects = unionSqlDao.query("select oper_date, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'"+
                branchSql+" group by oper_date order by oper_date");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            int date = ((Timestamp)itemEntityObj[0]).getDate();
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(date);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataMap.put(date,itemData);
            }
        }

        //环比 上个月
//        List<BranchDayEntity> branchDayEntities_last_month= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+beginDate+"')-30) and to_days(oper_date) <= (to_days('"+endDate+"')-30)" + branchSql);
        List<Object> objects_last_month = unionSqlDao.query("select oper_date, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+beginDate+"')-30) and to_days(oper_date) <= (to_days('"+endDate+"')-30)"+
                branchSql+" group by oper_date order by oper_date");
        for(Object object : objects_last_month){
            Object[]itemEntityObj = (Object[])object;
            int date = ((Timestamp)itemEntityObj[0]).getDate();
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(date);
            if(itemData!=null){
                itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
            }
        }
        //同比 去年
        List<Object> objects_last_year = unionSqlDao.query("select oper_date, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+beginDate+"')-365) and to_days(oper_date) <= (to_days('"+endDate+"')-365)"+
                branchSql+" group by oper_date order by oper_date");
        for(Object object : objects_last_year){
            Object[]itemEntityObj = (Object[])object;
            int date = ((Timestamp)itemEntityObj[0]).getDate();
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(date);
            if(itemData!=null){
                itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
            }
        }
//        List<BranchDayEntity> branchDayEntities_last_year= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and to_days(oper_date)>=(to_days('"+beginDate+"')-365) and to_days(oper_date) <= (to_days('"+endDate+"')-365)" + branchSql);
        JSONArray results = new JSONArray();
        for (Map.Entry<Integer, ItemData> entry : dataMap.entrySet()) {
            Integer key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("日期",key);
            resultJson.put("本期",itemData.getTotal_sale());
            resultJson.put("同比",itemData.getTotal_sale_last_year());
            resultJson.put("环比",itemData.getTotal_sale_last_month());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray monthBranchSaleFenbu(String orgId, String beginDate, String endDate) throws BaseException {

        Map<String, Double> dataMap = new HashMap<>();

//        List<BranchDayEntity> branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'");
        List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+beginDate+"')-365) and to_days(oper_date) <= (to_days('"+endDate+"')-365)"+
                " group by branch_no");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Double itemData = dataMap.get(branch_no);
            if(itemData!=null){
                dataMap.put(branch_no,itemData+(total_sale!=null?total_sale:0));
            }else{
                dataMap.put(branch_no,(total_sale!=null?total_sale:0));
            }
        }

        List<Map.Entry<String, Double>> list = new ArrayList<Map.Entry<String, Double>>(dataMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            // 降序排序
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                return -o1.getValue().compareTo(o2.getValue());
            }
        });
        JSONArray results = new JSONArray();
        for (int i=0;i<list.size()&&i<25;i++) {
            Map.Entry<String, Double> mapping = list.get(i);
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",mapping.getKey());
            resultJson.put("value",mapping.getValue());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray monthBranchClsFenbu(String orgId, String branchNo, String beginDate, String endDate) throws BaseException {

        Map<String, Double> dataMap = new HashMap<>();

        String branchSql = StringUtils.isEmpty(branchNo)?"":" and branch_no = '"+branchNo+"'";
        List<ClsDayEntity> clsDayEntities= clsDayDao.list("where org_id = '"+orgId+"' " +
                "and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'"+branchSql);
        List<Object> objects = unionSqlDao.query("select item_clsno, sum(sale_amt) from ClsDayEntity where org_id ='"+orgId+"' and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'"+
                branchSql+" group by item_clsno");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String item_clsno = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Double itemData = dataMap.get(item_clsno);
            if(itemData!=null){
                dataMap.put(item_clsno,itemData+(total_sale!=null?total_sale:0));
            }else{
                dataMap.put(item_clsno,(total_sale!=null?total_sale:0));
            }
        }

        List<Map.Entry<String, Double>> list = new ArrayList<>(dataMap.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {
            // 降序排序
            @Override
            public int compare(Map.Entry<String, Double> o1, Map.Entry<String, Double> o2) {
                return -o1.getValue().compareTo(o2.getValue());
            }
        });
        JSONArray results = new JSONArray();
        for (int i=0;i<list.size();i++) {
            Map.Entry<String, Double> mapping = list.get(i);
            JSONObject resultJson = new JSONObject();
            resultJson.put("name",mapping.getKey());
            resultJson.put("value",mapping.getValue());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray monthBranchSaleTongbiHuanbiZhanbi(String orgId, String beginDate, String endDate) throws BaseException {

        class ItemData{
            String branch_name;
            double aim;
            double total_sale;
            double total_sale_cnt;
            double total_sale_bill_cnt;

            double total_sale_last_month;
            double total_sale_cnt_last_month;
            double total_sale_bill_cnt_last_month;

            double total_sale_last_year;
            double total_sale_cnt_last_year;
            double total_sale_bill_cnt_last_year;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public double getAim() {
                return aim;
            }

            public void setAim(double aim) {
                this.aim = aim;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_sale_bill_cnt() {
                return total_sale_bill_cnt;
            }

            public void setTotal_sale_bill_cnt(double total_sale_bill_cnt) {
                this.total_sale_bill_cnt = total_sale_bill_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_sale_cnt_last_month() {
                return total_sale_cnt_last_month;
            }

            public void setTotal_sale_cnt_last_month(double total_sale_cnt_last_month) {
                this.total_sale_cnt_last_month = total_sale_cnt_last_month;
            }

            public double getTotal_sale_bill_cnt_last_month() {
                return total_sale_bill_cnt_last_month;
            }

            public void setTotal_sale_bill_cnt_last_month(double total_sale_bill_cnt_last_month) {
                this.total_sale_bill_cnt_last_month = total_sale_bill_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getTotal_sale_cnt_last_year() {
                return total_sale_cnt_last_year;
            }

            public void setTotal_sale_cnt_last_year(double total_sale_cnt_last_year) {
                this.total_sale_cnt_last_year = total_sale_cnt_last_year;
            }

            public double getTotal_sale_bill_cnt_last_year() {
                return total_sale_bill_cnt_last_year;
            }

            public void setTotal_sale_bill_cnt_last_year(double total_sale_bill_cnt_last_year) {
                this.total_sale_bill_cnt_last_year = total_sale_bill_cnt_last_year;
            }
        }

        Map<String,ItemData> dataMap = new HashMap<>();
        //本期
//        List<BranchDayEntity> branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"'");
        List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt),sum(bill_cnt),sum(sale_qty) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+beginDate+"' and oper_date <= '"+ endDate+"' group by branch_no");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Long total_bill_cnt = (Long)itemEntityObj[2];
            Double total_sale_qty = (Double)itemEntityObj[3];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setTotal_sale_bill_cnt(itemData.getTotal_sale_bill_cnt()+(total_bill_cnt!=null?total_bill_cnt:0));
                itemData.setTotal_sale_cnt(itemData.getTotal_sale_cnt()+(total_sale_qty!=null?total_sale_qty:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setTotal_sale_bill_cnt(itemData.getTotal_sale_bill_cnt()+(total_bill_cnt!=null?total_bill_cnt:0));
                itemData.setTotal_sale_cnt(itemData.getTotal_sale_cnt()+(total_sale_qty!=null?total_sale_qty:0));
                dataMap.put(branch_no,itemData);
            }
        }
        //门店名
        String branch_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            //环比 上个月
//            List<BranchDayEntity> branchDayEntities_last_month= branchDayDao.list("where org_id = '"+orgId+"' " +
//                    "and to_days(oper_date)>=(to_days('"+beginDate+"')-30) and to_days(oper_date)>=(to_days('"+endDate+"')-30) and branch_no in("+branch_nosStr+")");
            List<Object> objects_last_month = unionSqlDao.query("select branch_no, sum(sale_amt),sum(bill_cnt),sum(sale_qty) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+beginDate+"')-30) and to_days(oper_date)>=(to_days('"+endDate+"')-30) and branch_no in("+branch_nosStr+") group by branch_no");
            for(Object object : objects_last_month){
                Object[]itemEntityObj = (Object[])object;
                String branch_no = ((String)itemEntityObj[0]);
                Double total_sale = (Double)itemEntityObj[1];
                Long total_bill_cnt = (Long)itemEntityObj[2];
                Double total_sale_qty = (Double)itemEntityObj[3];
                ItemData itemData = dataMap.get(branch_no);
                if(itemData!=null){
                    itemData.setTotal_sale_last_month(itemData.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                    itemData.setTotal_sale_bill_cnt_last_month(itemData.getTotal_sale_bill_cnt_last_month()+(total_bill_cnt!=null?total_bill_cnt:0));
                    itemData.setTotal_sale_cnt_last_month(itemData.getTotal_sale_cnt_last_month()+(total_sale_qty!=null?total_sale_qty:0));
                }
            }
            //同比 去年
//            List<BranchDayEntity> branchDayEntities_last_year= branchDayDao.list("where org_id = '"+orgId+"' " +
//                    "and to_days(oper_date)>=(to_days('"+beginDate+"')-365) and to_days(oper_date)>=(to_days('"+endDate+"')-365) and branch_no in("+branch_nosStr+")");
            List<Object> objects_last_year = unionSqlDao.query("select branch_no, sum(sale_amt),sum(bill_cnt),sum(sale_qty) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+beginDate+"')-365) and to_days(oper_date)>=(to_days('"+endDate+"')-365) and branch_no in("+branch_nosStr+") group by branch_no");
            for(Object object : objects_last_year){
                Object[]itemEntityObj = (Object[])object;
                String branch_no = ((String)itemEntityObj[0]);
                Double total_sale = (Double)itemEntityObj[1];
                Long total_bill_cnt = (Long)itemEntityObj[2];
                Double total_sale_qty = (Double)itemEntityObj[3];
                ItemData itemData = dataMap.get(branch_no);
                if(itemData!=null){
                    itemData.setTotal_sale_last_year(itemData.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                    itemData.setTotal_sale_bill_cnt_last_year(itemData.getTotal_sale_bill_cnt_last_year()+(total_bill_cnt!=null?total_bill_cnt:0));
                    itemData.setTotal_sale_cnt_last_year(itemData.getTotal_sale_cnt_last_year()+(total_sale_qty!=null?total_sale_qty:0));
                }
            }
            //今天最低任务
            List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 1 and cycle_year= DATE_FORMAT('"+beginDate+"', '%Y')" +
                    " and cycle_month = cast(DATE_FORMAT(NOW(), '%m') as int) and branch_no in("+branch_nosStr+")");
            if(CollectionUtils.isNotEmpty(aimEntities)){
                Double aim_value = aimEntities.get(0).getAim_value();
                String branch_no = aimEntities.get(0).getBranch_no();
                ItemData itemData = dataMap.get(branch_no);
                if(itemData!=null){
                    itemData.setAim((aim_value!=null?aim_value:0)/30);
                }
            }



            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                dataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店",itemData.getBranch_name());
            resultJson.put("任务",NumberUtil.getDouble2Format(itemData.getAim()));
            resultJson.put("完成率",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getAim()));

            resultJson.put("本期销售",itemData.getTotal_sale());
            resultJson.put("本期销量",itemData.getTotal_sale_cnt());
            resultJson.put("本期客流",itemData.getTotal_sale_bill_cnt());
            resultJson.put("本期客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getTotal_sale_bill_cnt()));
            resultJson.put("本期连带率",NumberUtil.getDouble2Format(itemData.getTotal_sale_cnt()/itemData.getTotal_sale_bill_cnt()));

            resultJson.put("环比销售",itemData.getTotal_sale_last_month());
            resultJson.put("环比销量",itemData.getTotal_sale_cnt_last_month());
            resultJson.put("环比客流",itemData.getTotal_sale_bill_cnt_last_month());
            resultJson.put("环比客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/itemData.getTotal_sale_bill_cnt_last_month()));
            resultJson.put("环比连带率",NumberUtil.getDouble2Format(itemData.getTotal_sale_cnt_last_month()/itemData.getTotal_sale_bill_cnt_last_month()));

            resultJson.put("同比销售",itemData.getTotal_sale_last_year());
            resultJson.put("同比销量",itemData.getTotal_sale_cnt_last_year());
            resultJson.put("同比客流",itemData.getTotal_sale_bill_cnt_last_year());
            resultJson.put("同比客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/itemData.getTotal_sale_bill_cnt_last_year()));
            resultJson.put("同比连带率",NumberUtil.getDouble2Format(itemData.getTotal_sale_cnt_last_year()/itemData.getTotal_sale_bill_cnt_last_year()));
            results.add(resultJson);
        }
        return results;
    }
}
