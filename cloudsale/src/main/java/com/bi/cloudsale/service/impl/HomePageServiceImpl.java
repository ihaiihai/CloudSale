package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.dto.ClientSaveDto;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.cls.SaleClsDayDao;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.cls.SaleClsDayEntity;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.ClientService;
import com.bi.cloudsale.service.HomePageService;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

@Service
@Transactional
public class HomePageServiceImpl implements HomePageService {

	private static final Logger log = LoggerFactory.getLogger(HomePageServiceImpl.class);

	@Resource
	private BranchDao branchDao;
	@Resource
	private BranchDayDao branchDayDao;
	@Resource
	private AimDao aimDao;
	@Resource
	private UserInfoDao userInfoDao;
	@Resource
	private UnionSqlDao unionSqlDao;
	@Resource
	private SaleClsDayDao saleClsDayDao;

	@Override
	public JSONArray yesterdayGain(String orgId) throws BaseException {

		double yesSale = 0d;
		double yesSaleCount = 0d;
		double yesVipSaleCount = 0d;
		double yesNewVip = 0d;
		
		double beforeYesSale = 0d;
		double beforeYesSaleCount = 0d;
		double beforeYesVipSaleCount = 0d;
		double beforeYesNewVip = 0d;

//		List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1) and org_id = '"+orgId+"'");
		List<Object> objects = unionSqlDao.query("select sum(sale_amt),sum(bill_cnt),sum(bill_vip_cnt),sum(vip_new) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			Double total_sale = (Double)itemEntityObj[0];
			Long total_bill = (Long)itemEntityObj[1];
			Long total_bill_vip = (Long)itemEntityObj[2];
			Long total_vip_new = (Long)itemEntityObj[3];
			yesSale += total_sale!=null?total_sale:0;
			yesSaleCount +=total_bill!=null?total_bill:0;
			yesVipSaleCount +=total_bill_vip!=null?total_bill_vip:0;
			yesNewVip += total_vip_new!=null?total_vip_new:0;
		}
//		List<BranchDayEntity> branchDayEntities1 = branchDayDao.list("where to_days(oper_date) = (to_days(now())-2) and org_id = '"+orgId+"'");
		List<Object> objects1 = unionSqlDao.query("select sum(sale_amt),sum(bill_cnt),sum(bill_vip_cnt),sum(vip_new) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-2)");
		for(Object object : objects1){
			Object[]itemEntityObj = (Object[])object;
			Double total_sale = (Double)itemEntityObj[0];
			Long total_bill = (Long)itemEntityObj[1];
			Long total_bill_vip = (Long)itemEntityObj[2];
			Long total_vip_new = (Long)itemEntityObj[3];
			beforeYesSale += total_sale!=null?total_sale:0;
			beforeYesSaleCount +=total_bill!=null?total_bill:0;
			beforeYesVipSaleCount +=total_bill_vip!=null?total_bill_vip:0;
			beforeYesNewVip += total_vip_new!=null?total_vip_new:0;
		}
		//1.昨日销售
		JSONObject yesSaleJson = new JSONObject();
		yesSaleJson.put("value",yesSale);
		//2.昨日成交单数
		JSONObject yesSaleCountJson = new JSONObject();
		yesSaleCountJson.put("value",yesSaleCount);
		//3.昨日新增用户
		JSONObject yesNewUserJson = new JSONObject();
		yesNewUserJson.put("value",yesSaleCount - yesVipSaleCount);
		//4.昨日新增会员
		JSONObject yesNewVipJson = new JSONObject();
		yesNewVipJson.put("value",yesNewVip);
		//5.昨日销售
		JSONObject beforeYesSaleJson = new JSONObject();
		beforeYesSaleJson.put("value",beforeYesSale);
		//6.昨日成交单数
		JSONObject beforeYesSaleCountJson = new JSONObject();
		beforeYesSaleCountJson.put("value",beforeYesSaleCount);
		//7.昨日新增用户
		JSONObject beforeYesNewUserJson = new JSONObject();
		beforeYesNewUserJson.put("value",beforeYesSaleCount - beforeYesVipSaleCount);
		//8.昨日新增会员
		JSONObject beforeYesNewVipJson = new JSONObject();
		beforeYesNewVipJson.put("value",beforeYesNewVip);
		
		JSONArray results = new JSONArray();
		results.add(yesSaleJson);
		results.add(yesSaleCountJson);
		results.add(yesNewUserJson);
		results.add(yesNewVipJson);
		results.add(beforeYesSaleJson);
		results.add(beforeYesSaleCountJson);
		results.add(beforeYesNewUserJson);
		results.add(beforeYesNewVipJson);
		return results;
	}

	@Override
	public JSONArray todayRealtimeIndex(String orgId) throws BaseException {
		Date nowDate = new Date();
		double todaySale = 0d;
		double todaySaleCnt = 0d;//累计销售数量
		double todayProfit = 0d;
		double todayCompletionRate = 0d;
		double todayLeftSale = 0d;
		double todayLeftTime =DateUtil.getRemainMinutesOneDay(nowDate)/60+(DateUtil.getRemainMinutesOneDay(nowDate) -DateUtil.getRemainMinutesOneDay(nowDate)/60*60)/60.0;
		double clientCount = 0d;

//		List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) = to_days(now()) and org_id = '"+orgId+"'");
		List<Object> objects = unionSqlDao.query("select sum(sale_amt),sum(sale_qty),sum(bill_cnt),sum(cost_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = to_days(now())");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			Double total_sale = (Double)itemEntityObj[0];
			Double total_qty = (Double)itemEntityObj[1];
			Long total_bill = (Long)itemEntityObj[2];
			Double total_cost = (Double)itemEntityObj[3];
			todaySale += total_sale!=null?total_sale:0;
			todaySaleCnt +=total_qty!=null?total_qty:0;
			todayProfit +=(total_sale!=null?total_sale:0)
					-(total_cost!=null?total_cost:0);
			clientCount +=total_bill!=null?total_bill:0;
		}

		//今天剩余、今天完成率
		double todayAim = 0d;
		List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 1 and cycle_year= DATE_FORMAT(NOW(), '%Y') and cycle_month= cast(DATE_FORMAT(NOW(), '%m') as int)");
		if(CollectionUtils.isNotEmpty(aimEntities)){
			Double aim_value = aimEntities.get(0).getAim_value();
			todayAim +=(aim_value!=null?aim_value:0)/30;
		}
		todayLeftSale = todayAim - todaySale;
		todayCompletionRate = todaySale/todayAim;

		JSONObject todaySaleJson = new JSONObject();
		todaySaleJson.put("实时销售金额",todaySale);
		JSONObject todayProfitJson = new JSONObject();
		todayProfitJson.put("实时毛利",NumberUtil.getDouble2Format(todayProfit));
		JSONObject todayCompletionRateJson = new JSONObject();
		todayCompletionRateJson.put("今日完成率", NumberUtil.getPercentFormat(todayCompletionRate));
		JSONObject todayLeftSaleJson = new JSONObject();
		todayLeftSaleJson.put("剩余金额",NumberUtil.getDouble2Format(todayLeftSale));

		JSONObject todayLeftTimeJson = new JSONObject();
		todayLeftTimeJson.put("剩余时间",NumberUtil.getDouble2Format(todayLeftTime));
		JSONObject clientCountJson = new JSONObject();
		clientCountJson.put("客流量",clientCount);
		JSONObject clientAmountJson = new JSONObject();
		clientAmountJson.put("客单价",NumberUtil.getDouble2Format(todaySale/clientCount));
		JSONObject clientRelatedJson = new JSONObject();
		clientRelatedJson.put("购物连带率",NumberUtil.getPercentFormat(todaySaleCnt/clientCount));

		JSONArray results = new JSONArray();
		results.add(todaySaleJson);
		results.add(todayProfitJson);
		results.add(todayCompletionRateJson);
		results.add(todayLeftSaleJson);

		results.add(todayLeftTimeJson);
		results.add(clientCountJson);
		results.add(clientAmountJson);
		results.add(clientRelatedJson);
		return results;
	}

	@Override
	public JSONArray todayLowestTarget(String orgId) throws BaseException {

		double todayAimSale = 0d;
		String dateTime = DateUtil.formatDate(new Date(),DateUtil.SIMPLE_DATE_FORMAT);
		//今天最低任务
		List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 1 and cycle_year= DATE_FORMAT(NOW(), '%Y') and cycle_month= cast(DATE_FORMAT(NOW(), '%m') as int)");
		if(CollectionUtils.isNotEmpty(aimEntities)){
			Double aim_value = aimEntities.get(0).getAim_value();
			todayAimSale +=(aim_value!=null?aim_value:0)/365;
		}
		JSONObject todayAimJson = new JSONObject();
		todayAimJson.put("今日最低任务",NumberUtil.getDouble2Format(todayAimSale));
		JSONObject dateJson = new JSONObject();
		dateJson.put("日期",dateTime);

		JSONArray results = new JSONArray();
		results.add(todayAimJson);
		results.add(dateJson);
		return results;
	}

	@Override
	public JSONArray monthGain(String orgId) throws BaseException {
		class ItemData{
			String day;
			double day_sale;
			double total_sale;
			double day_sale_last_year;

			public String getDay() {
				return day;
			}

			public void setDay(String day) {
				this.day = day;
			}

			public double getDay_sale() {
				return day_sale;
			}

			public void setDay_sale(double day_sale) {
				this.day_sale = day_sale;
			}

			public double getTotal_sale() {
				return total_sale;
			}

			public void setTotal_sale(double total_sale) {
				this.total_sale = total_sale;
			}

			public double getDay_sale_last_year() {
				return day_sale_last_year;
			}

			public void setDay_sale_last_year(double day_sale_last_year) {
				this.day_sale_last_year = day_sale_last_year;
			}
		}
		Map<String,ItemData> dataMap = new TreeMap<>();

		Calendar cal_now= Calendar.getInstance();
		int now_day = cal_now.get(Calendar.DAY_OF_MONTH);//今天多少号
		Calendar cal_pre= Calendar.getInstance();//本月首日
		cal_pre.add(Calendar.MONTH, 0);
		cal_pre.set(Calendar.DAY_OF_MONTH,1);
		String firstDay = DateUtil.formatDate(cal_pre.getTime(),DateUtil.SIMPLE_DATE_FORMAT);
		String lastDay = DateUtil.formatDate(cal_now.getTime(),DateUtil.SIMPLE_DATE_FORMAT);
		while(cal_pre.get(Calendar.DAY_OF_MONTH)<=now_day){
			String dateShow = DateUtil.formatDate(cal_pre.getTime(),DateUtil.SIMPLE_DATE_FORMAT);
			dataMap.put(dateShow,new ItemData());
			cal_pre.set(Calendar.DAY_OF_MONTH,cal_pre.get(Calendar.DAY_OF_MONTH)+1);
		}
//		List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//				"and oper_date>='"+firstDay+"' and oper_date<='"+lastDay+"' order by oper_date");
		double total_dale = 0;
		List<Object> objects = unionSqlDao.query("select oper_date, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+firstDay+"' and oper_date<='"+lastDay+"'"+" group by oper_date");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String oper_date = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
			Double total_sale = (Double)itemEntityObj[1];
			total_dale += (total_sale!=null?total_sale:0);
			ItemData itemData = dataMap.get(oper_date);
			if(itemData!=null){
				itemData.setDay_sale(itemData.getDay_sale()+(total_sale!=null?total_sale:0));
				itemData.setTotal_sale(itemData.getTotal_sale()+total_dale);
			}
		}
		List<BranchDayEntity>branchDayEntities_last_year= branchDayDao.list("where org_id = '"+orgId+"' " +
				"and to_days(oper_date)>=(to_days('"+firstDay+"')-365) and to_days(oper_date)<=(to_days('"+lastDay+"')-365) order by oper_date");
		List<Object> objects_sametime = unionSqlDao.query("select oper_date, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date)>=(to_days('"+firstDay+"')-365) and to_days(oper_date)<=(to_days('"+lastDay+"')-365)"+
				" group by oper_date");
		for(Object object : objects_sametime){
			Object[]itemEntityObj = (Object[])object;
			String oper_date = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
			Double total_sale = (Double)itemEntityObj[1];
			total_dale += (total_sale!=null?total_sale:0);
			ItemData itemData = dataMap.get(oper_date);
			if(itemData!=null){
				itemData.setDay_sale_last_year(itemData.getDay_sale_last_year()+total_dale);
			}

		}
		JSONArray results = new JSONArray();
		for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
			String key = entry.getKey();
			ItemData itemData =  entry.getValue();
			JSONObject resultJson = new JSONObject();
			resultJson.put("日期",key);
			resultJson.put("本期销售",itemData.getDay_sale());
			resultJson.put("同期销售",itemData.getDay_sale_last_year());
			resultJson.put("累计",itemData.getTotal_sale());
			results.add(resultJson);
		}
		return results;
	}

	@Override
	public JSONArray monthGainPercent(String orgId) throws BaseException {

		//本月计划、累计完成、完成率、共计成交、平均客单价、连带率、
		JSONArray results = new JSONArray();
		Calendar cal_now= Calendar.getInstance();
		Calendar cal_pre= Calendar.getInstance();//本月首日
		cal_pre.add(Calendar.MONTH, 0);
		cal_pre.set(Calendar.DAY_OF_MONTH,1);
		double thisMonthAim = 0d;//月目标
		double totalSale = 0d;//累计完成
		long totalCount = 0L;//总交易笔数
		double aveClientAmt = 0d;//平均客单价
		double totalCnt = 0d;//总销售数量
		String begin =  DateUtil.formatDate(cal_pre.getTime(),DateUtil.SIMPLE_DATE_FORMAT);
		String end =  DateUtil.formatDate(cal_now.getTime(),DateUtil.SIMPLE_DATE_FORMAT);
//		List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//				"and oper_date>='"+begin+"' and oper_date<='"+end+"'");
		List<Object> objects = unionSqlDao.query("select sum(sale_amt),sum(bill_cnt),sum(sale_qty) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<='"+end+"'");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			Double total_sale = (Double)itemEntityObj[0];
			Long total_bill= (Long)itemEntityObj[1];
			Double total_sale_qty = (Double)itemEntityObj[2];
			//计算本月销售累计
			totalSale += total_sale!=null?total_sale:0;
			//计算本月单数累计
			totalCount +=total_bill!=null?total_bill:0;
			//计算本月销量累计
			totalCnt +=total_sale_qty!=null?total_sale_qty:0;
		}

		//今天最低任务
		List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 1 and cycle_year= DATE_FORMAT(NOW(), '%Y') and cycle_month= cast(DATE_FORMAT(NOW(), '%m') as int)");
		if(CollectionUtils.isNotEmpty(aimEntities)){
			Double aim_value = aimEntities.get(0).getAim_value();
			thisMonthAim = (aim_value!=null?aim_value:0);
		}
		aveClientAmt = totalSale/totalCount;
		double liandailv = totalCnt/totalCount;
		double wanchenglv = totalSale/thisMonthAim;

		JSONObject result = new JSONObject();
		result.put("name","本月计划："+NumberUtil.getDouble2Format(thisMonthAim)+"元 | 累计完成："+NumberUtil.getDouble2Format(totalSale)+"元 | 完成率："+NumberUtil.getPercentFormat(wanchenglv)+
				" | 共计成交："+totalCount+" | 平均客单价："+NumberUtil.getDouble2Format(aveClientAmt)+"元/单 | 连带率："+NumberUtil.getDouble2Format(liandailv)+"件/单");
		results.add(result);

		return results;
	}

	@Override
	public JSONArray yesterdayTZDL(String orgId) throws BaseException {

		// 昨日退货率、昨日折扣率、昨日客单价、昨日连带率
		JSONArray results = new JSONArray();
		double totalsale_qty = 0d;//总销量
		double totalRet_qty = 0d;//总退货数量
		double totalSale = 0d;//总销售金额
		double totalDis_amt = 0d;//总折扣金额
		long totalBill_cnt = 0L;//总销售笔数
//		List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1) and org_id = '"+orgId+"'");
		List<Object> objects = unionSqlDao.query("select sum(sale_amt),sum(bill_cnt),sum(sale_qty),sum(ret_qty),sum(dis_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			Double sale = (Double)itemEntityObj[0];
			Long bill= (Long)itemEntityObj[1];
			Double sale_qty = (Double)itemEntityObj[2];
			Double ret_qty = (Double)itemEntityObj[3];
			Double dis = (Double)itemEntityObj[4];

			totalSale += sale!=null?sale:0;
			totalBill_cnt +=bill!=null?bill:0;
			totalsale_qty +=sale_qty!=null?sale_qty:0;
			totalRet_qty += ret_qty!=null?ret_qty:0;
			totalDis_amt += dis!=null?dis:0;
		}
		double tuihuolv = totalRet_qty/totalsale_qty;
		double zhekoulv = totalDis_amt/totalSale;
		double kedanjia = totalSale/totalBill_cnt;
		double liandailv = totalsale_qty/totalBill_cnt;

		JSONObject resultJson = new JSONObject();
		resultJson.put("昨日退货率",NumberUtil.getDouble2Format(tuihuolv));
		resultJson.put("昨日折扣率",NumberUtil.getDouble2Format(zhekoulv));
		resultJson.put("昨日客单价",NumberUtil.getDouble2Format(kedanjia));
		resultJson.put("昨日连带率",NumberUtil.getDouble2Format(liandailv));
		results.add(resultJson);

		return results;
	}

	@Override
	public JSONArray todayBranchSale(String orgId) throws BaseException {

		//各门店今天实时销售分布
		class BranchData{
			double sale;
			String branch_name;

			public String getBranch_name() {
				return branch_name;
			}

			public void setBranch_name(String branch_name) {
				this.branch_name = branch_name;
			}

			public double getSale() {
				return sale;
			}

			public void setSale(double sale) {
				this.sale = sale;
			}
		}
		JSONArray results = new JSONArray();
		Map<String,BranchData> todayData = new TreeMap<>();
//		List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())) and org_id = '"+orgId+"'");
		List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now()))"+
				" group by branch_no");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String branchNo = ((String)itemEntityObj[0]);
			Double total_sale = (Double)itemEntityObj[1];
			BranchData dataDay = todayData.get(branchNo);
			if(dataDay!=null){
				dataDay.setSale(dataDay.getSale() +(total_sale!=null?total_sale:0));
			}else{
				dataDay = new BranchData();
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				todayData.put(branchNo,dataDay);
			}
		}
		String branch_nosStr = "";
		Set<String> keys = todayData.keySet() ;// 得到全部的key,cls_no
		Iterator<String> iter = keys.iterator() ;
		while(iter.hasNext()){
			String str = iter.next();
			if(StringUtils.isNotEmpty(branch_nosStr)){
				branch_nosStr = branch_nosStr + ",'"+str+"'";
			}else{
				branch_nosStr =  "'"+str+"'";
			}
		}
		if(StringUtils.isNotEmpty(branch_nosStr)){
			List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
			for(BranchEntity branchEntity : branchEntities){
				todayData.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
			}
		}
		for (Map.Entry<String,BranchData> entry : todayData.entrySet()) {
			String branchNo = entry.getKey();
			JSONObject result = new JSONObject();
			result.put("branchNo", branchNo);
			result.put("branchName", entry.getValue().getBranch_name());
			result.put("sale", entry.getValue().getSale());
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray someDayBranchSale(String orgId,String oper_date) throws BaseException {

		//某天各店铺销售分布
		class BranchData{
			double sale;
			String branch_name;

			public String getBranch_name() {
				return branch_name;
			}

			public void setBranch_name(String branch_name) {
				this.branch_name = branch_name;
			}

			public double getSale() {
				return sale;
			}

			public void setSale(double sale) {
				this.sale = sale;
			}
		}
		JSONArray results = new JSONArray();
		Map<String,BranchData> somedayData = new TreeMap<>();
//		List<BranchDayEntity> branchDayEntities = branchDayDao.list("where oper_date = '"+oper_date+"' and org_id = '"+orgId+"'");
		List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date = '"+oper_date+"'"+
				"  group by branch_no");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String branchNo = ((String)itemEntityObj[0]);
			Double total_sale = (Double)itemEntityObj[1];
			BranchData dataDay = somedayData.get(branchNo);
			if(dataDay!=null){
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
			}else{
				dataDay = new BranchData();
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				somedayData.put(branchNo,dataDay);
			}
		}
		String branch_nosStr = "";
		Set<String> keys = somedayData.keySet() ;// 得到全部的key,cls_no
		Iterator<String> iter = keys.iterator() ;
		while(iter.hasNext()){
			String str = iter.next() ;
			if(StringUtils.isNotEmpty(branch_nosStr)){
				branch_nosStr = branch_nosStr + ",'"+str+"'";
			}else{
				branch_nosStr =  "'"+str+"'";
			}
		}
		if(StringUtils.isNotEmpty(branch_nosStr)){
			List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
			for(BranchEntity branchEntity : branchEntities){
				somedayData.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
			}
		}
		for (Map.Entry<String,BranchData> entry : somedayData.entrySet()) {
			String branchNo = entry.getKey();
			JSONObject result = new JSONObject();
			if(entry.getValue().getBranch_name() != null) {
				result.put(entry.getValue().getBranch_name(),entry.getValue().getSale());
				results.add(result);
			}
		}
		return results;
	}

	@Override
	public JSONArray yestdayBranchSale(String orgId) throws BaseException {

		class BranchData{
			double aim;
			double sale;

			public double getAim() {
				return aim;
			}

			public void setAim(double aim) {
				this.aim = aim;
			}

			public double getSale() {
				return sale;
			}

			public void setSale(double sale) {
				this.sale = sale;
			}
		}
		//昨日各店销售完成情况
		JSONArray results = new JSONArray();
		Map<String,BranchData> yestdayData = new TreeMap<>();
//		List<BranchDayEntity> branchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1) and org_id = '"+orgId+"'");
		List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)"+
				" group by branch_no");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String branchNo = ((String)itemEntityObj[0]);
			Double total_sale = (Double)itemEntityObj[1];
			BranchData dataDay = yestdayData.get(branchNo);
			if(dataDay!=null){
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
			}else{
				dataDay = new BranchData();
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				yestdayData.put(branchNo,dataDay);
			}
		}
		String branch_nosStr = "";
		Set<String> keys = yestdayData.keySet() ;// 得到全部的key,cls_no
		Iterator<String> iter = keys.iterator() ;
		while(iter.hasNext()){
			String str = iter.next() ;
			if(StringUtils.isNotEmpty(branch_nosStr)){
				branch_nosStr = branch_nosStr + ",'"+str+"'";
			}else{
				branch_nosStr =  "'"+str+"'";
			}
		}
		if(StringUtils.isNotEmpty(branch_nosStr)){
			List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' " +
					"and aim_cycle = 1 " +
					"and cycle_year= DATE_FORMAT(NOW(), '%Y') " +
					"and cycle_month= cast(DATE_FORMAT(NOW(), '%m') as int) " +
					"and aim_type = 1 and branch_no in ("+branch_nosStr+")");
			for(AimEntity aimEntity : aimEntities){
				String branch_no = aimEntity.getBranch_no();
				if(yestdayData.get(branch_no)!=null){
					Double aim = aimEntity.getAim_value();
					yestdayData.get(aimEntity.getBranch_no()).setAim((aim!=null?aim:0)/30);
				}
			}
		}
		for (Map.Entry<String,BranchData> entry : yestdayData.entrySet()) {
			String branchNo = entry.getKey();
			JSONObject result = new JSONObject();
			result.put("门店",branchNo);
			result.put("任务",NumberUtil.getDouble2Format(entry.getValue().getAim()));
			result.put("完成",entry.getValue().getSale());
			result.put("完成率",NumberUtil.getPercentFormat(entry.getValue().getSale()/entry.getValue().getAim()));
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray last30BranchBillCnt(String orgId) throws BaseException {

		//最近30天内成交单数波动详情
		class DataDay{
			double bill_cnt;

			double bill_cnt_lastYear;
			public double getBill_cnt() {
				return bill_cnt;
			}

			public void setBill_cnt(double bill_cnt) {
				this.bill_cnt = bill_cnt;
			}

			public double getBill_cnt_lastYear() {
				return bill_cnt_lastYear;
			}

			public void setBill_cnt_lastYear(double bill_cnt_lastYear) {
				this.bill_cnt_lastYear = bill_cnt_lastYear;
			}
		}
		JSONArray results = new JSONArray();
//		List<BranchDayEntity> thisYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) <= (to_days(now()))" +
//				" and to_days(oper_date) >= (to_days(now())-30) and org_id = '"+orgId+"' order by oper_date");
		Map<String,DataDay> thisYearDataMap = Maps.newLinkedHashMap();
		List<Object> objects = unionSqlDao.query("select oper_date, sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days(now())) and to_days(oper_date) >= (to_days(now())-30)"+
				" group by oper_date order by oper_date");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String operateDate = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
			Long bill_cnt = (Long)itemEntityObj[1];
			DataDay dataDay = thisYearDataMap.get(operateDate);
			if(dataDay!=null){
				dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
			}else{
				dataDay = new DataDay();
				dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
				thisYearDataMap.put(operateDate,dataDay);
			}
		}
//		List<BranchDayEntity> lastYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) <= (to_days(now())-365)" +
//				" and (to_days(oper_date)) >= (to_days(now())-30-364) and org_id = '"+orgId+"'");
		List<Object> objects_last_year = unionSqlDao.query("select oper_date, sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days(now())-365) and (to_days(oper_date)) >= (to_days(now())-30-364)"+
				" group by oper_date order by oper_date");
		for(Object object : objects_last_year){
			Object[]itemEntityObj = (Object[])object;
			String operateDate = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
			Long bill_cnt = (Long)itemEntityObj[1];
			DataDay dataDay = thisYearDataMap.get(operateDate);
			if(dataDay!=null){
				dataDay.setBill_cnt_lastYear(dataDay.getBill_cnt_lastYear() + (bill_cnt!=null?bill_cnt:0));
			}
		}
		for (Map.Entry<String,DataDay> entry : thisYearDataMap.entrySet()) {
			JSONObject result = new JSONObject();
			result.put("name",entry.getKey());
			result.put("本期",entry.getValue().getBill_cnt());
			result.put("同比",entry.getValue().getBill_cnt_lastYear());
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray last30BranchNewClient(String orgId) throws BaseException {

		class DataDay{
			double bill_cnt;
			double bill_vip_cnt;

			double bill_cnt_lastYear;
			double bill_vip_cnt_lastYear;
			public double getBill_cnt() {
				return bill_cnt;
			}

			public void setBill_cnt(double bill_cnt) {
				this.bill_cnt = bill_cnt;
			}

			public double getBill_vip_cnt() {
				return bill_vip_cnt;
			}

			public void setBill_vip_cnt(double bill_vip_cnt) {
				this.bill_vip_cnt = bill_vip_cnt;
			}

			public double getBill_cnt_lastYear() {
				return bill_cnt_lastYear;
			}

			public void setBill_cnt_lastYear(double bill_cnt_lastYear) {
				this.bill_cnt_lastYear = bill_cnt_lastYear;
			}

			public double getBill_vip_cnt_lastYear() {
				return bill_vip_cnt_lastYear;
			}

			public void setBill_vip_cnt_lastYear(double bill_vip_cnt_lastYear) {
				this.bill_vip_cnt_lastYear = bill_vip_cnt_lastYear;
			}
		}
		//最近30日内新增客户波动详情
		JSONArray results = new JSONArray();
//		List<BranchDayEntity> thisYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) <= (to_days(now()))" +
//				" and to_days(oper_date) >= (to_days(now())-30) and org_id = '"+orgId+"' order by oper_date");
		Map<String,DataDay> thisYearDataMap = Maps.newLinkedHashMap();
		List<Object> objects = unionSqlDao.query("select oper_date,sum(bill_cnt),sum(bill_vip_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days(now())) and to_days(oper_date) >= (to_days(now())-30)"+
				" group by oper_date order by oper_date");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String operateDate = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
			Long bill_cnt = (Long)itemEntityObj[1];
			Long bill_vip_cnt = (Long)itemEntityObj[2];
			DataDay dataDay = thisYearDataMap.get(operateDate);
			if(dataDay!=null){
				dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
				dataDay.setBill_vip_cnt(dataDay.getBill_vip_cnt() + (bill_vip_cnt!=null?bill_vip_cnt:0));
			}else{
				dataDay = new DataDay();
				dataDay.setBill_cnt(bill_cnt!=null?bill_cnt:0);
				dataDay.setBill_vip_cnt(dataDay.getBill_vip_cnt() + (bill_vip_cnt!=null?bill_vip_cnt:0));
				thisYearDataMap.put(operateDate,dataDay);
			}
		}
//		List<BranchDayEntity> lastYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) <= (to_days(now())-365)" +
//				" and (to_days(oper_date)) >= (to_days(now())-30-364) and org_id = '"+orgId+"'");
		List<Object> objects_last_year = unionSqlDao.query("select oper_date,sum(bill_cnt),sum(bill_vip_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days(now())-365) and (to_days(oper_date)) >= (to_days(now())-30-364)"+
				" group by oper_date order by oper_date");
		for(Object object : objects_last_year){
			Object[]itemEntityObj = (Object[])object;
			String operateDate = DateUtil.formatDate(DateUtils.addDays(new Date(((Timestamp)itemEntityObj[0]).getTime()),365),DateUtil.SIMPLE_DATE_FORMAT);
			Long bill_cnt = (Long)itemEntityObj[1];
			Long bill_vip_cnt = (Long)itemEntityObj[2];
			DataDay dataDay = thisYearDataMap.get(operateDate);
			if(dataDay!=null){
				dataDay.setBill_cnt_lastYear(dataDay.getBill_cnt_lastYear() + (bill_cnt!=null?bill_cnt:0));
				dataDay.setBill_vip_cnt_lastYear(dataDay.getBill_vip_cnt_lastYear() + (bill_vip_cnt!=null?bill_vip_cnt:0));
			}
		}
		for (Map.Entry<String,DataDay> entry : thisYearDataMap.entrySet()) {
			JSONObject result = new JSONObject();
			result.put("name",entry.getKey());
			result.put("本期",entry.getValue().getBill_cnt()-entry.getValue().getBill_vip_cnt());
			result.put("同比",entry.getValue().getBill_cnt_lastYear()-entry.getValue().getBill_vip_cnt_lastYear());
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray last30BranchNewVip(String orgId) throws BaseException {

		class DataDay{
			long vip_new;
			long vip_new_lastYear;

			public long getVip_new() {
				return vip_new;
			}

			public void setVip_new(long vip_new) {
				this.vip_new = vip_new;
			}

			public long getVip_new_lastYear() {
				return vip_new_lastYear;
			}

			public void setVip_new_lastYear(long vip_new_lastYear) {
				this.vip_new_lastYear = vip_new_lastYear;
			}
		}
		//近30日内新增会员波动详情
		JSONArray results = new JSONArray();
//		List<BranchDayEntity> thisYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) <= (to_days(now()))" +
//				" and to_days(oper_date) >= (to_days(now())-30) and org_id = '"+orgId+"' order by oper_date");
		Map<String,DataDay> thisYearDataMap = Maps.newLinkedHashMap();
		List<Object> objects = unionSqlDao.query("select oper_date, sum(vip_new) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days(now())) and to_days(oper_date) >= (to_days(now())-30)"+
				" group by oper_date order by oper_date");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String operateDate = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
			Long vip_new = (Long)itemEntityObj[1];
			DataDay dataDay = thisYearDataMap.get(operateDate);
			if(dataDay!=null){
				dataDay.setVip_new(dataDay.getVip_new() + (vip_new!=null?vip_new:0));
			}else{
				dataDay = new DataDay();
				dataDay.setVip_new(dataDay.getVip_new() + (vip_new!=null?vip_new:0));
				thisYearDataMap.put(operateDate,dataDay);
			}
		}

//		List<BranchDayEntity> lastYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) <= (to_days(now())-365)" +
//				" and (to_days(oper_date)) >= (to_days(now())-30-364) and org_id = '"+orgId+"'");
		List<Object> objects_lastYear = unionSqlDao.query("select oper_date, sum(vip_new) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) <= (to_days(now())) and to_days(oper_date) >= (to_days(now())-30)"+
				" group by oper_date order by oper_date");
		for(Object object : objects_lastYear){
			Object[]itemEntityObj = (Object[])object;
			String operateDate = DateUtil.formatDate(DateUtils.addDays(new Date(((Timestamp)itemEntityObj[0]).getTime()),365),DateUtil.SIMPLE_DATE_FORMAT);
			Long vip_new = (Long)itemEntityObj[1];
			DataDay dataDay = thisYearDataMap.get(operateDate);
			if(dataDay!=null){
				dataDay.setVip_new_lastYear(dataDay.getVip_new_lastYear() + (vip_new!=null?vip_new:0));
			}
		}
		for (Map.Entry<String,DataDay> entry : thisYearDataMap.entrySet()) {
			JSONObject result = new JSONObject();
			result.put("name",entry.getKey());
			result.put("本期",entry.getValue().getVip_new());
			result.put("同比",entry.getValue().getVip_new_lastYear());
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray yesterdayRetDetail(String orgId) throws BaseException {

		class DataDay{
			String branch_name;
			double sale;
			double ret;

			public String getBranch_name() {
				return branch_name;
			}

			public void setBranch_name(String branch_name) {
				this.branch_name = branch_name;
			}

			public double getSale() {
				return sale;
			}

			public void setSale(double sale) {
				this.sale = sale;
			}

			public double getRet() {
				return ret;
			}

			public void setRet(double ret) {
				this.ret = ret;
			}
		}
		//昨日退货率详情
		JSONArray results = new JSONArray();
//		List<BranchDayEntity> thisYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//				" and org_id = '"+orgId+"' order by oper_date");
		Map<String,DataDay> thisYearDataMap = new HashMap<>();
		List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt), sum(ret_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)"+
				" group by branch_no");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String branchNo = ((String)itemEntityObj[0]);
			Double total_sale = (Double)itemEntityObj[1];
			Double total_ret = (Double)itemEntityObj[2];
			DataDay dataDay = thisYearDataMap.get(branchNo);
			if(dataDay!=null){
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				dataDay.setRet(dataDay.getRet() + (total_ret!=null?total_ret:0));
			}else{
				dataDay = new DataDay();
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				dataDay.setRet(dataDay.getRet() + (total_ret!=null?total_ret:0));
				thisYearDataMap.put(branchNo,dataDay);
			}
		}
		String branch_nosStr = "";
		Set<String> keys = thisYearDataMap.keySet() ;// 得到全部的key,cls_no
		Iterator<String> iter = keys.iterator() ;
		while(iter.hasNext()){
			String str = iter.next() ;
			if(StringUtils.isNotEmpty(branch_nosStr)){
				branch_nosStr = branch_nosStr + ",'"+str+"'";
			}else{
				branch_nosStr =  "'"+str+"'";
			}
		}
		if(StringUtils.isNotEmpty(branch_nosStr)){
			List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
			for(BranchEntity branchEntity : branchEntities){
				thisYearDataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
			}
		}
		for (Map.Entry<String,DataDay> entry : thisYearDataMap.entrySet()) {
			JSONObject result = new JSONObject();
			String branchNo = entry.getKey();
			result.put("name",entry.getValue().getBranch_name());
			result.put("销售",entry.getValue().getSale());
			result.put("退货",entry.getValue().getRet());
			result.put("退货率",NumberUtil.getPercentFormat(entry.getValue().getRet()/entry.getValue().getSale()));
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray yesterdayDisDetail(String orgId) throws BaseException {

		class DataDay{
			double sale;
			double dis;
			String branch_name;

			public String getBranch_name() {
				return branch_name;
			}

			public void setBranch_name(String branch_name) {
				this.branch_name = branch_name;
			}

			public double getSale() {
				return sale;
			}

			public void setSale(double sale) {
				this.sale = sale;
			}

			public double getDis() {
				return dis;
			}

			public void setDis(double dis) {
				this.dis = dis;
			}
		}
		//昨日折扣率详情
		JSONArray results = new JSONArray();
//		List<BranchDayEntity> thisYearBranchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//				" and org_id = '"+orgId+"' order by oper_date");
		Map<String,DataDay> thisYearDataMap = new HashMap<>();
		List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt),sum(dis_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)"+
				" group by branch_no");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String branchNo = ((String)itemEntityObj[0]);
			Double total_sale = (Double)itemEntityObj[1];
			Double total_dis = (Double)itemEntityObj[2];
			DataDay dataDay = thisYearDataMap.get(branchNo);
			if(dataDay!=null){
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				dataDay.setDis(dataDay.getDis() + (total_dis!=null?total_dis:0));
			}else{
				dataDay = new DataDay();
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				dataDay.setDis(dataDay.getDis() + (total_dis!=null?total_dis:0));
				thisYearDataMap.put(branchNo,dataDay);
			}
		}
		String branch_nosStr = "";
		Set<String> keys = thisYearDataMap.keySet() ;// 得到全部的key,cls_no
		Iterator<String> iter = keys.iterator() ;
		while(iter.hasNext()){
			String str = iter.next() ;
			if(StringUtils.isNotEmpty(branch_nosStr)){
				branch_nosStr = branch_nosStr + ",'"+str+"'";
			}else{
				branch_nosStr =  "'"+str+"'";
			}
		}
		if(StringUtils.isNotEmpty(branch_nosStr)){
			List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
			for(BranchEntity branchEntity : branchEntities){
				thisYearDataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
			}
		}
		for (Map.Entry<String,DataDay> entry : thisYearDataMap.entrySet()) {
			JSONObject result = new JSONObject();
			String branchNo = entry.getKey();
			result.put("name",entry.getValue().getBranch_name());
			result.put("value",NumberUtil.getPercentFormat(entry.getValue().getDis()/entry.getValue().getSale()));
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray yesterdayClientAmtDetail(String orgId) throws BaseException {

		//各门店客单价分布
		class DataDay{
			double sale;
			long bill_cnt;

			public double getSale() {
				return sale;
			}

			public void setSale(double sale) {
				this.sale = sale;
			}

			public long getBill_cnt() {
				return bill_cnt;
			}

			public void setBill_cnt(long bill_cnt) {
				this.bill_cnt = bill_cnt;
			}
		}
		JSONArray results = new JSONArray();
//		List<BranchDayEntity> yesterdayBranchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//				" and org_id = '"+orgId+"' order by oper_date");
		Map<String,DataDay> yesterdayDataMap = new HashMap<>();
		List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt),sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)"+
				" group by branch_no");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String branchNo = ((String)itemEntityObj[0]);
			Double total_sale = (Double)itemEntityObj[1];
			Long bill_cnt = (Long)itemEntityObj[2];
			DataDay dataDay = yesterdayDataMap.get(branchNo);
			if(dataDay!=null){
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
			}else{
				dataDay = new DataDay();
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
				yesterdayDataMap.put(branchNo,dataDay);
			}
		}
		for (Map.Entry<String,DataDay> entry : yesterdayDataMap.entrySet()) {
			JSONObject result = new JSONObject();
			String branchNo = entry.getKey();
			result.put("name",branchNo);
			result.put("value",NumberUtil.getDouble2Format(entry.getValue().getSale()/entry.getValue().getBill_cnt()));
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray yesterdayLiandailvDetail(String orgId) throws BaseException {

		//各门店连带率分布
		class DataDay{
			double sale_qty;
			long bill_cnt;

			public double getSale_qty() {
				return sale_qty;
			}

			public void setSale_qty(double sale_qty) {
				this.sale_qty = sale_qty;
			}

			public long getBill_cnt() {
				return bill_cnt;
			}

			public void setBill_cnt(long bill_cnt) {
				this.bill_cnt = bill_cnt;
			}
		}
		JSONArray results = new JSONArray();
//		List<BranchDayEntity> yesterdayBranchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now())-1)" +
//				" and org_id = '"+orgId+"' order by oper_date");
		Map<String,DataDay> yesterdayDataMap = new HashMap<>();
		List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_qty),sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now())-1)"+
				" group by branch_no");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String branchNo = ((String)itemEntityObj[0]);
			Double sale_qty = (Double)itemEntityObj[1];
			Long bill_cnt = (Long)itemEntityObj[2];
			DataDay dataDay = yesterdayDataMap.get(branchNo);
			if(dataDay!=null){
				dataDay.setSale_qty(dataDay.getSale_qty() + (sale_qty!=null?sale_qty:0));
				dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
			}else{
				dataDay = new DataDay();
				dataDay.setSale_qty(dataDay.getSale_qty() + (sale_qty!=null?sale_qty:0));
				dataDay.setBill_cnt(dataDay.getBill_cnt() + (bill_cnt!=null?bill_cnt:0));
				yesterdayDataMap.put(branchNo,dataDay);
			}
		}
		for (Map.Entry<String,DataDay> entry : yesterdayDataMap.entrySet()) {
			JSONObject result = new JSONObject();
			String branchNo = entry.getKey();
			result.put("name",branchNo);
			result.put("value",NumberUtil.getDouble2Format(entry.getValue().getSale_qty()/entry.getValue().getBill_cnt()));
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray todayRealtimeAim(String orgId) throws BaseException {

		//各门店实时任务进度
		class DataDay{
			double sale;
			double branchAim;

			public double getBranchAim() {
				return branchAim;
			}

			public void setBranchAim(double branchAim) {
				this.branchAim = branchAim;
			}

			public double getSale() {
				return sale;
			}

			public void setSale(double sale) {
				this.sale = sale;
			}
		}
		JSONArray results = new JSONArray();
//		List<BranchDayEntity> yesterdayBranchDayEntities = branchDayDao.list("where to_days(oper_date) = (to_days(now()))" +
//				" and org_id = '"+orgId+"' order by oper_date");
		Map<String,DataDay> yesterdayDataMap = new HashMap<>();
		List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt) from BranchDayEntity where org_id ='"+orgId+"' and to_days(oper_date) = (to_days(now()))"+
				" group by branch_no");
		for(Object object : objects){
			Object[]itemEntityObj = (Object[])object;
			String branchNo = ((String)itemEntityObj[0]);
			Double total_sale = (Double)itemEntityObj[1];
			DataDay dataDay = yesterdayDataMap.get(branchNo);
			if(dataDay!=null){
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
			}else{
				dataDay = new DataDay();
				dataDay.setSale(dataDay.getSale() + (total_sale!=null?total_sale:0));
				yesterdayDataMap.put(branchNo,dataDay);
			}
		}
		String branch_nosStr = "";
		Set<String> keys = yesterdayDataMap.keySet() ;// 得到全部的key,cls_no
		Iterator<String> iter = keys.iterator() ;
		while(iter.hasNext()){
			String str = iter.next() ;
			if(StringUtils.isNotEmpty(branch_nosStr)){
				branch_nosStr = branch_nosStr + ",'"+str+"'";
			}else{
				branch_nosStr =  "'"+str+"'";
			}
		}
		if(StringUtils.isNotEmpty(branch_nosStr)){
			List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' " +
					"and aim_cycle = 1 " +
					"and cycle_year= DATE_FORMAT(NOW(), '%Y') " +
					"and cycle_month=cast(DATE_FORMAT(NOW(), '%m') as int) " +
					"and aim_type = 1 and branch_no in ("+branch_nosStr+")");
			for(AimEntity aimEntity : aimEntities){
				double aim =  aimEntity.getAim_value()!=null? aimEntity.getAim_value():0;
				yesterdayDataMap.get(aimEntity.getBranch_no()).setBranchAim(aim/30);
			}
		}
		for (Map.Entry<String,DataDay> entry : yesterdayDataMap.entrySet()) {
			String branchNo = entry.getKey();
			JSONObject result = new JSONObject();
			result.put("门店",branchNo);
			result.put("任务",NumberUtil.getDouble2Format(entry.getValue().getBranchAim()));
			result.put("今日完成",entry.getValue().getSale());
			result.put("完成率",NumberUtil.getPercentFormat(entry.getValue().getSale()/entry.getValue().getBranchAim()));
			results.add(result);
		}
		return results;
	}

	@Override
	public JSONArray someDayBranchClsSale(String orgId, String branchNo, Date oper_date) throws BaseException {
		JSONArray results = new JSONArray();
		List<SaleClsDayEntity> list = saleClsDayDao.querySumByBranch(null, orgId, branchNo, DateUtil.getDayStart(oper_date), DateUtil.getDayEnd(oper_date));
		if(list != null && list.size() > 0) {
			for(SaleClsDayEntity entity : list) {
				JSONObject result = new JSONObject();
				result.put(entity.getItem_clsno(), entity.getSale_amt());
				results.add(result);
			}
		}
		return results;
	}

}
