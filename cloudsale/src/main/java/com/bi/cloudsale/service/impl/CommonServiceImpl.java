package com.bi.cloudsale.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.entity.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.service.CommonService;

@Service
@Transactional
public class CommonServiceImpl implements CommonService {
	
	@Resource
    private BrandDao brandDao;
    @Resource
    private BranchDao branchDao;
    @Resource
    private BranchClsDao branchClsDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private SalerDao salerDao;
	@Resource
	private ItemDao itemDao;

	@Override
	public Map<String,Object> queryAllBranch(String orgId) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<BranchEntity> branchEntities = branchDao.list("where org_id ='"+orgId+"'");
		for(BranchEntity entity : branchEntities) {
			map.put(entity.getBranch_no(), entity);
		}
		return map;	
	}

	@Override
	public Map<String, Object> queryAllBrand(String orgId) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<BrandEntity> brandEntities = brandDao.list("where org_id ='"+orgId+"'");
		for(BrandEntity entity : brandEntities) {
			map.put(entity.getItem_brandno(), entity);
		}
		return map;
	}

	@Override
	public Map<String, Object> queryAllCls(String orgId) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<ItemClsEntity> clsEntities = itemClsDao.list("where org_id ='"+orgId+"'");
		for(ItemClsEntity entity : clsEntities) {
			map.put(entity.getItem_clsno(), entity);
		}
		return map;
	}

	@Override
	public List<OptionDto> queryClsOptions(String orgId) {
		List<OptionDto> list = new ArrayList<OptionDto>();
		Map<String,OptionDto> map = new HashMap<String,OptionDto>();
		List<ItemClsEntity> clsEntities = itemClsDao.list("where org_id ='"+orgId+"'");
		for(ItemClsEntity entity : clsEntities) {
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(entity.getItem_clsno());
			optionDto.setLabel(entity.getItem_clsname().trim());
			if(StringUtils.isBlank(entity.getCls_parent())) {
				list.add(optionDto);
			}else {
				OptionDto parentOptionDto = map.get(entity.getCls_parent().trim());
				if(parentOptionDto.getChildren() == null) {
					parentOptionDto.setChildren(new ArrayList<OptionDto>());
				}
				parentOptionDto.getChildren().add(optionDto);
			}
			map.put(entity.getItem_clsno().trim(), optionDto);
		}
		return list;
	}

	@Override
	public List<OptionDto> queryBrandOptions(String orgId) {
		List<OptionDto> list = new ArrayList<OptionDto>();
		Map<String,OptionDto> map = new HashMap<String,OptionDto>();
		List<BrandEntity> brandEntities = brandDao.list("where org_id ='"+orgId+"'");
		for(BrandEntity entity : brandEntities) {
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(entity.getItem_brandno());
			optionDto.setLabel(entity.getItem_brandname().trim());
			if(StringUtils.isBlank(entity.getBrand_parent()) || entity.getBrand_parent().indexOf("*") != -1) {
				list.add(optionDto);
			}else {
				OptionDto parentOptionDto = map.get(entity.getBrand_parent().trim());
				if(parentOptionDto.getChildren() == null) {
					parentOptionDto.setChildren(new ArrayList<OptionDto>());
				}
				parentOptionDto.getChildren().add(optionDto);
			}
			map.put(entity.getItem_brandno().trim(), optionDto);
		}
		return list;
	}

	@Override
	public List<OptionDto> queryBranchOptions(String orgId) {
		List<OptionDto> list = new ArrayList<OptionDto>();
		Map<String,OptionDto> map = new HashMap<String,OptionDto>();
		List<BranchClsEntity> branchClsEntities = branchClsDao.list("where org_id ='"+orgId+"' order by branch_clsno");
		List<BranchEntity> branchEntities = branchDao.list("where org_id ='"+orgId+"' order by branch_no");
		for(BranchClsEntity entity : branchClsEntities) {
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(entity.getBranch_clsno());
			optionDto.setLabel(entity.getBranch_clsname().trim());
			if(StringUtils.isBlank(entity.getParent_clsno())) {
				list.add(optionDto);
			}else {
				OptionDto parentOptionDto = map.get(entity.getParent_clsno().trim());
				if(parentOptionDto != null) {
					if(parentOptionDto.getChildren() == null) {
						parentOptionDto.setChildren(new ArrayList<OptionDto>());
					}
					parentOptionDto.getChildren().add(optionDto);
				}
			}
			map.put(entity.getBranch_clsno().trim(), optionDto);
		}
		for(BranchEntity entity : branchEntities) {
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(entity.getBranch_no().trim());
			optionDto.setLabel(entity.getBranch_name().trim());
			if(StringUtils.isBlank(entity.getBranch_clsno()) || entity.getBranch_clsno().indexOf("*") != -1) {
				list.add(optionDto);
			}else {
				OptionDto parentOptionDto = map.get(entity.getBranch_clsno().trim());
				if(parentOptionDto.getChildren() == null) {
					parentOptionDto.setChildren(new ArrayList<OptionDto>());
				}
				parentOptionDto.getChildren().add(optionDto);
			}
			map.put(entity.getBranch_no().trim(), optionDto);
		}
		return list;
	}

	@Override
	public Map<String, Object> queryAllSaler(String orgId) {
		Map<String, Object> map = new HashMap<String, Object>();
		List<SalerEntity> salerEntitys = salerDao.list("where org_id ='"+orgId+"'");
		for(SalerEntity entity : salerEntitys) {
			map.put(entity.getSale_id(), entity);
		}
		return map;
	}

	@Override
	public List<OptionDto> querySalerOptions(String orgId) {
		List<OptionDto> list = new ArrayList<OptionDto>();
		List<SalerEntity> salerEntities = salerDao.list("where org_id ='"+orgId+"' order by sale_id");
		for(SalerEntity entity : salerEntities) {
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(entity.getSale_id());
			optionDto.setLabel(entity.getSale_name());
			list.add(optionDto);
		}
		return list;
	}

	@Override
	public JSONObject itemDetail(String orgId,String itemNo) {
		List<ItemEntity> itemEntities = itemDao.list("where item_no='"+itemNo+"' and org_id = '"+orgId+"'");
		if(CollectionUtils.isNotEmpty(itemEntities)){
			ItemEntity itemEntity = itemEntities.get(0);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("itemNo",itemEntity.getItem_no());
			jsonObject.put("itemName",itemEntity.getItem_name());
			return jsonObject;
		}
		return new JSONObject();
	}

	@Override
	public JSONObject itemClsDetail(String orgId,String itemClsNo) {
		List<ItemClsEntity> itemEntities = itemClsDao.list("where item_clsno='"+itemClsNo+"' and org_id = '"+orgId+"'");
		if(CollectionUtils.isNotEmpty(itemEntities)){
			ItemClsEntity itemEntity = itemEntities.get(0);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("itemClsNo",itemEntity.getItem_clsno());
			jsonObject.put("itemClsName",itemEntity.getItem_clsname());
			return jsonObject;
		}
		return new JSONObject();
	}

	@Override
	public JSONObject itemBrandDetail(String orgId, String brandNo) {
		List<BrandEntity> itemEntities = brandDao.list("where item_brandno='"+brandNo+"' and org_id = '"+orgId+"'");
		if(CollectionUtils.isNotEmpty(itemEntities)){
			BrandEntity itemEntity = itemEntities.get(0);
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("itemBrandNo",itemEntity.getItem_brandno());
			jsonObject.put("itemBrandName",itemEntity.getItem_brandname());
			return jsonObject;
		}
		return new JSONObject();
	}
}
