package com.bi.cloudsale.service.overall;

import java.util.List;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.dto.overall.BranchIndDto;
import com.bi.cloudsale.dto.overall.OperationDto;

public interface OperationService {

	void createOperation(BranchIndDto operationDto) throws BaseException;
	
	List<OperationDto> queryOperation(String orgId, String branchNos, String begin, String end);
}
