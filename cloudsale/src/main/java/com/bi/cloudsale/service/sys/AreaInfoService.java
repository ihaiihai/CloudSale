package com.bi.cloudsale.service.sys;

import java.util.List;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.AreaInfoDto;

public interface AreaInfoService {

	void createAreaInfo(AreaInfoDto areaInfoDto) throws BaseException;
	
	void deleteAreaInfo(String areaCode) throws BaseException;
	
	void updateAreaInfo(AreaInfoDto areaInfoDto) throws BaseException;
	
	List<AreaInfoDto> queryByCondition(BaseQueryCondition baseQueryCondition);
	
	List<OptionDto> queryAllOptions(String orgId);
	
	AreaInfoDto queryByCode(String areaCode);
	
}
