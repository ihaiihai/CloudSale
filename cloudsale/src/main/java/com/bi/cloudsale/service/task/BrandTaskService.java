package com.bi.cloudsale.service.task;

import java.util.List;

import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.dto.task.SetUpTaskDto;

public interface BrandTaskService {

	List<SetUpTaskDto> querySetUp(TaskQueryCondition condition);
}
