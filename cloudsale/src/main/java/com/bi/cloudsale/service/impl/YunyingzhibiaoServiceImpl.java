package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.BranchDayMonthService;
import com.bi.cloudsale.service.YunyingzhibiaoService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
@Transactional
public class YunyingzhibiaoServiceImpl implements YunyingzhibiaoService {

    private static final Logger log = LoggerFactory.getLogger(YunyingzhibiaoServiceImpl.class);

    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private ItemDayDao itemDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private VipDayDao vipDayDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {

        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray queryBranchYunying(String orgId, String begin, String end, String branchNos) throws BaseException {

        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
       String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        class ItemData{
            String branch_name;
            double area;
            double saler_cnt;
            double total_sale;
            double total_bill_cnt;
            double total_billvip_cnt;
            double total_vip_sale;
            double total_dis;
            double total_sale_cnt;
            double total_ret_cnt;
            double aim;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public double getArea() {
                return area;
            }

            public void setArea(double area) {
                this.area = area;
            }

            public double getSaler_cnt() {
                return saler_cnt;
            }

            public void setSaler_cnt(double saler_cnt) {
                this.saler_cnt = saler_cnt;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_bill_cnt() {
                return total_bill_cnt;
            }

            public void setTotal_bill_cnt(double total_bill_cnt) {
                this.total_bill_cnt = total_bill_cnt;
            }

            public double getTotal_billvip_cnt() {
                return total_billvip_cnt;
            }

            public void setTotal_billvip_cnt(double total_billvip_cnt) {
                this.total_billvip_cnt = total_billvip_cnt;
            }

            public double getTotal_vip_sale() {
                return total_vip_sale;
            }

            public void setTotal_vip_sale(double total_vip_sale) {
                this.total_vip_sale = total_vip_sale;
            }

            public double getTotal_dis() {
                return total_dis;
            }

            public void setTotal_dis(double total_dis) {
                this.total_dis = total_dis;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_ret_cnt() {
                return total_ret_cnt;
            }

            public void setTotal_ret_cnt(double total_ret_cnt) {
                this.total_ret_cnt = total_ret_cnt;
            }

            public double getAim() {
                return aim;
            }

            public void setAim(double aim) {
                this.aim = aim;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
//        List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+begin+"' and oper_date <= '"+ end+"'" + branchSql);
        List<Object> objects = unionSqlDao.query("select branch_no, sum(sale_amt),sum(ret_qty),sum(sale_qty),sum(dis_amt),sum(bill_cnt),sum(bill_vip_cnt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date <= '"+ end+"'"+
                branchSql +" group by branch_no");
        for(Object object : objects){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            Double ret_qty = (Double)itemEntityObj[2];
            Double sale_qty = (Double)itemEntityObj[3];
            Double dis_amt = (Double)itemEntityObj[4];
            Long bill_cnt = (Long)itemEntityObj[5];
            Long bill_vip_cnt = (Long)itemEntityObj[6];

            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_qty!=null?ret_qty:0));
                itemData.setTotal_sale_cnt(itemData.getTotal_sale_cnt()+(sale_qty!=null?sale_qty:0));
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setTotal_dis(itemData.getTotal_dis()+(dis_amt!=null?dis_amt:0));
                itemData.setTotal_bill_cnt(itemData.getTotal_bill_cnt()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTotal_billvip_cnt(itemData.getTotal_billvip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
            }else{
                itemData = new ItemData();
                itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_qty!=null?ret_qty:0));
                itemData.setTotal_sale_cnt(itemData.getTotal_sale_cnt()+(sale_qty!=null?sale_qty:0));
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setTotal_dis(itemData.getTotal_dis()+(dis_amt!=null?dis_amt:0));
                itemData.setTotal_bill_cnt(itemData.getTotal_bill_cnt()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTotal_billvip_cnt(itemData.getTotal_billvip_cnt()+(bill_vip_cnt!=null?bill_vip_cnt:0));
                dataMap.put(branch_no,itemData);
            }
        }

        Date beginDate = java.sql.Date.valueOf(begin);
        Date endDate = java.sql.Date.valueOf(end);
        long day = (endDate.getTime()-beginDate.getTime())/(24*60*60*1000);
        List<AimEntity> aimEntities = aimDao.list("where org_id = '"+orgId+"' and aim_cycle = 0 and cycle_year= DATE_FORMAT('"+begin+"', '%Y')"+branchSql);
        for(AimEntity aimEntity: aimEntities){
            String branch_no = aimEntity.getBranch_no();
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                Double aim_value = aimEntities.get(0).getAim_value();
                itemData.setAim((aim_value!=null?aim_value:0)/365*day);
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSqlin)?" and branch_no in("+branchSqlin+")":"";
        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'"+branchSql);
        for(BranchEntity branchEntity : branchEntities){
            String branch_no = branchEntity.getBranch_no();
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setBranch_name(branchEntity.getBranch_name());
                itemData.setArea(branchEntity.getArea());
            }
        }

        List<SalerEntity> salerEntities = salerDao.list("where org_id = '"+orgId+"'"+branchSql);
        for(SalerEntity salerEntity : salerEntities){
            String branch_no = salerEntity.getBranch_no();
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setSaler_cnt(dataMap.get(salerEntity.getBranch_no()).getSaler_cnt()+1);
            }
        }
//        List<VipDayEntity>vipDayEntities= vipDayDao.list("where org_id = '"+orgId+"' " +
//                "and oper_date>='"+begin+"' and oper_date <= '"+ end+"'" + branchSql);
        List<Object> objects_vip = unionSqlDao.query("select branch_no, sum(sale_amt) from VipDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date <= '"+ end+"'"+
                branchSql +" group by branch_no");
        for(Object object : objects_vip){
            Object[]itemEntityObj = (Object[])object;
            String branch_no = ((String)itemEntityObj[0]);
            Double total_sale = (Double)itemEntityObj[1];
            ItemData itemData = dataMap.get(branch_no);
            if(itemData!=null){
                itemData.setTotal_vip_sale(itemData.getTotal_vip_sale()+(total_sale!=null?total_sale:0));
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店编号",key);
            resultJson.put("门店名称",itemData.getBranch_name());
            resultJson.put("标题","销售总额: ￥"+itemData.getTotal_sale()+" | 面积: "+itemData.getArea()+"㎡ | 员工数量: "+itemData.getSaler_cnt()+"人");
            resultJson.put("人均日销",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getSaler_cnt()));
            resultJson.put("平米日均",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getArea()));
            resultJson.put("日均销售",NumberUtil.getDouble2Format(itemData.getTotal_sale()/day));
            resultJson.put("日均客流",NumberUtil.getDouble2Format(itemData.getTotal_bill_cnt()/day));
            resultJson.put("会员占比",NumberUtil.getDouble2Format(itemData.getTotal_vip_sale()/itemData.getTotal_sale()));
            resultJson.put("会员日均贡献",NumberUtil.getDouble2Format(itemData.getTotal_vip_sale()/day));
            resultJson.put("平均折扣",NumberUtil.getDouble2Format(itemData.getTotal_dis()/itemData.getTotal_sale()));
            resultJson.put("退货率",NumberUtil.getDouble2Format(itemData.getTotal_ret_cnt()/itemData.getTotal_sale_cnt()*100));
            resultJson.put("客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getTotal_bill_cnt()));
            resultJson.put("连带率",NumberUtil.getDouble2Format(itemData.getTotal_sale_cnt()/itemData.getTotal_bill_cnt()*100));
            resultJson.put("评估得分",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getAim()*100));
            results.add(resultJson);
        }
        return results;
    }

}
