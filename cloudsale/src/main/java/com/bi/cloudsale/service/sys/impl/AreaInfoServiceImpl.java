package com.bi.cloudsale.service.sys.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.AreaInfoDto;
import com.bi.cloudsale.persistent.dao.BranchClsDao;
import com.bi.cloudsale.persistent.dao.BranchDao;
import com.bi.cloudsale.persistent.dao.sys.AreaInfoDao;
import com.bi.cloudsale.persistent.entity.BranchClsEntity;
import com.bi.cloudsale.persistent.entity.BranchEntity;
import com.bi.cloudsale.persistent.entity.sys.AreaInfoEntity;
import com.bi.cloudsale.service.sys.AreaInfoService;

@Service
@Transactional
public class AreaInfoServiceImpl implements AreaInfoService {
	
	private static final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);
	
	@Resource
	private AreaInfoDao areaInfoDao;
	@Resource
	private BranchClsDao branchClsDao;
	@Resource
	private BranchDao branchDao;

	@Override
	public void createAreaInfo(AreaInfoDto areaInfoDto) throws BaseException {
		AreaInfoEntity entity = new AreaInfoEntity();
		BeanUtil.copyProperties(areaInfoDto, entity);
		if(StringUtils.isEmpty(entity.getAreaCode())) {
			entity.setAreaCode(UUID.randomUUID().toString().replaceAll("-", ""));
		}
		try {
			areaInfoDao.createAreaInfo(entity);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public void deleteAreaInfo(String areaCode) throws BaseException {
		try {
			areaInfoDao.deleteAreaInfo(areaCode);
			areaInfoDao.deleteAreaByParentCode(areaCode);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public void updateAreaInfo(AreaInfoDto areaInfoDto) throws BaseException {
		try {
			AreaInfoEntity entity = areaInfoDao.queryByCode(areaInfoDto.getAreaCode());
			entity.setAreaName(areaInfoDto.getAreaName());
			entity.setBranchCode(areaInfoDto.getBranchCode());
			entity.setDescription(areaInfoDto.getDescription());
			areaInfoDao.updateAreaInfo(entity);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public List<AreaInfoDto> queryByCondition(BaseQueryCondition baseQueryCondition) {
		List<AreaInfoDto> dtos = new ArrayList<AreaInfoDto>();
		StringBuilder hql = new StringBuilder();
		hql.append("where org_id = '" + baseQueryCondition.getOrgId() + "'");
		hql.append(" order by branch_clsno");
		List<BranchClsEntity> branchClsEntities = branchClsDao.list(hql.toString());
		hql = new StringBuilder();
		hql.append("where org_id = '" + baseQueryCondition.getOrgId() + "'");
		if(StringUtils.isNotBlank(baseQueryCondition.getKey())) {
			hql.append(" and branch_name like '%" + baseQueryCondition.getKey() + "%'");
		}
		hql.append(" order by branch_no");
		List<BranchEntity> branchEntities = branchDao.list(hql.toString());
		
		Map<String,Object> map = new HashMap<String,Object>();
		for(BranchClsEntity branchClsEntity : branchClsEntities) {
			AreaInfoDto areaInfoDto = new AreaInfoDto();
			areaInfoDto.setExpanded(true);
			areaInfoDto.setLoaded(true);
			areaInfoDto.setLeaf(true);
			areaInfoDto.setAreaCode(branchClsEntity.getBranch_clsno());
			areaInfoDto.setAreaName(branchClsEntity.getBranch_clsname());
			if(StringUtils.isBlank(branchClsEntity.getParent_clsno().trim())) {
				areaInfoDto.setLevel(0);
				areaInfoDto.setChildCount(0);
				dtos.add(areaInfoDto);
			}else {
				AreaInfoDto parentDto = (AreaInfoDto) map.get(branchClsEntity.getParent_clsno().trim());
				areaInfoDto.setLevel(parentDto.getLevel() + 1);
				dtos.add(dtos.indexOf(parentDto) + parentDto.getChildCount() + 1, areaInfoDto);
				parentDto.setChildCount(parentDto.getChildCount() + 1);
			}
			map.put(branchClsEntity.getBranch_clsno().trim(), areaInfoDto);
		}
		for(BranchEntity branchEntity : branchEntities) {
			AreaInfoDto areaInfoDto = new AreaInfoDto();
			areaInfoDto.setExpanded(true);
			areaInfoDto.setLoaded(true);
			areaInfoDto.setLeaf(true);
			areaInfoDto.setAreaName(branchEntity.getBranch_name());
			areaInfoDto.setBranchCode(branchEntity.getBranch_no());
			AreaInfoDto parentDto = (AreaInfoDto) map.get(branchEntity.getBranch_clsno().trim());
			if(parentDto != null) {
				areaInfoDto.setLevel(parentDto.getLevel() + 1);
				dtos.add(dtos.indexOf(parentDto) + parentDto.getChildCount() + 1, areaInfoDto);
				parentDto.setChildCount(parentDto.getChildCount() + 1);
				map.put(branchEntity.getBranch_no(), areaInfoDto);
			}
		}
		return dtos;
	}

	@Override
	public AreaInfoDto queryByCode(String areaCode) {
		AreaInfoDto dto = new AreaInfoDto();
		AreaInfoEntity entity  = areaInfoDao.queryByCode(areaCode);
		if(entity != null) {
			BeanUtil.copyProperties(entity, dto);
			return dto;
		}else {
			return null;
		}
	}

	@Override
	public List<OptionDto> queryAllOptions(String orgId) {
		List<OptionDto> list = new ArrayList<OptionDto>();
		
		BaseQueryCondition baseQueryCondition = new BaseQueryCondition();
		baseQueryCondition.setOrgId(orgId);
		
		StringBuilder hql = new StringBuilder();
		hql.append("where org_id = '" + baseQueryCondition.getOrgId() + "'");
		hql.append(" order by branch_clsno");
		List<BranchClsEntity> branchClsEntities = branchClsDao.list(hql.toString());
		hql = new StringBuilder();
		hql.append("where org_id = '" + baseQueryCondition.getOrgId() + "'");
		if(StringUtils.isNotBlank(baseQueryCondition.getKey())) {
			hql.append(" and branch_name like '%" + baseQueryCondition.getKey() + "%'");
		}
		hql.append(" order by branch_no");
		List<BranchEntity> branchEntities = branchDao.list(hql.toString());
		
		Map<String,OptionDto> map = new HashMap<String,OptionDto>();
		for(BranchClsEntity branchClsEntity : branchClsEntities) {
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(branchClsEntity.getBranch_clsno().trim());
			optionDto.setLabel(branchClsEntity.getBranch_clsname().trim());
			if(StringUtils.isBlank(branchClsEntity.getParent_clsno().trim())) {
				list.add(optionDto);
			}else {
				OptionDto parentOptionDto = map.get(branchClsEntity.getParent_clsno().trim());
				if(parentOptionDto.getChildren() == null) {
					parentOptionDto.setChildren(new ArrayList<OptionDto>());
				}
				parentOptionDto.getChildren().add(optionDto);
			}
			map.put(branchClsEntity.getBranch_clsno().trim(), optionDto);
		}
		for(BranchEntity branchEntity : branchEntities) {
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(branchEntity.getBranch_no().trim());
			optionDto.setLabel(branchEntity.getBranch_name().trim());
			if(StringUtils.isBlank(branchEntity.getBranch_clsno().trim())) {
				list.add(optionDto);
			}else {
				OptionDto parentOptionDto = map.get(branchEntity.getBranch_clsno().trim());
				if(parentOptionDto != null) {
					if(parentOptionDto.getChildren() == null) {
						parentOptionDto.setChildren(new ArrayList<OptionDto>());
					}
					parentOptionDto.getChildren().add(optionDto);
				}
			}
			map.put(branchEntity.getBranch_no().trim(), optionDto);
		}
		return list;
	}
	
}
