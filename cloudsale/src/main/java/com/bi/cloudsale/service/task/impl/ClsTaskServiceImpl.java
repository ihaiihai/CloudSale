package com.bi.cloudsale.service.task.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.utils.ArithUtil;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.dto.task.SetUpTaskDto;
import com.bi.cloudsale.persistent.dao.cls.SaleClsDayDao;
import com.bi.cloudsale.persistent.dao.task.TaskDao;
import com.bi.cloudsale.persistent.entity.cls.SaleClsDayEntity;
import com.bi.cloudsale.persistent.entity.task.SaleAimEntity;
import com.bi.cloudsale.service.task.ClsTaskService;

@Service
@Transactional
public class ClsTaskServiceImpl implements ClsTaskService {
	
	@Resource
	private TaskDao taskDao;
	@Resource
	private SaleClsDayDao saleClsDayDao;

	@Override
	public List<SetUpTaskDto> querySetUp(TaskQueryCondition condition) {
		List<SetUpTaskDto> dtoList = new ArrayList<SetUpTaskDto>();
		List<SaleAimEntity> entityList = taskDao.queryByCondition(condition);
		if(entityList != null && entityList.size() > 0) {
			Map<String,Map<String,SetUpTaskDto>> map = new HashMap<String,Map<String,SetUpTaskDto>>();
			Set<String> branchNo = new HashSet<String>();
			Set<String> clsNo = new HashSet<String>();
			for(SaleAimEntity entity : entityList) {
				SetUpTaskDto dto = null;
				if(findMapItem(map, entity.getBranch_no(), entity.getItem_clsno()) == null) {
					dto = new SetUpTaskDto();
					dto.set门店编号(entity.getBranch_no());
					dto.set门店名称(entity.getBranch_no());
					dto.set类别编号(entity.getItem_clsno());
					dto.set类别(entity.getItem_clsno());
					if(!branchNo.contains(entity.getBranch_no())) {
						branchNo.add(entity.getBranch_no());
					}
					if(!clsNo.contains(entity.getItem_clsno())) {
						clsNo.add(entity.getItem_clsno());
					}
					dtoList.add(dto);
					addMapItem(map, entity.getBranch_no(), entity.getItem_clsno(), dto);
				}else {
					dto = findMapItem(map, entity.getBranch_no(), entity.getItem_clsno());
				}
				if(entity.getAim_cycle() == 0) {
					dto.set销售任务(entity.getAim_value());
				}
				if(entity.getAim_cycle() == 1) {
					setMonthAim(dto, entity.getCycle_month(), entity.getAim_value());
				}
			}
			//去年按门店统计销售数据
			List<SaleClsDayEntity> saleSumBranchList = saleClsDayDao.querySumByBranch(condition.getUserId(), condition.getOrgId(), branchNo, DateUtil.getYearStart(condition.getYear() - 1), DateUtil.getYearEnd(condition.getYear() - 1));
			Map<String,Double> sumBranchMap = new HashMap<String,Double>();
			if(saleSumBranchList != null && saleSumBranchList.size() > 0) {
				for(SaleClsDayEntity entity : saleSumBranchList) {
					sumBranchMap.put(entity.getBranch_no(), entity.getSale_amt());
				}
			}
			//去年按门店与品牌统计销售数据
			List<SaleClsDayEntity> saleSumList = saleClsDayDao.querySum(condition.getUserId(), condition.getOrgId(), clsNo, branchNo, DateUtil.getYearStart(condition.getYear() - 1), DateUtil.getYearEnd(condition.getYear() - 1));
			if(saleSumList != null && saleSumList.size() > 0) {
				for(SaleClsDayEntity entity : saleSumList) {
					SetUpTaskDto dto = findMapItem(map, entity.getBranch_no(), entity.getItem_clsno());
					if(dto != null) {
						dto.set前一年销售(entity.getSale_amt());
						dto.set业绩提升(ArithUtil.getIncrease(entity.getSale_amt(), dto.get销售任务()));
						if(sumBranchMap.get(entity.getBranch_no()) != null) {
							dto.set前一年占比(ArithUtil.getPercent(entity.getSale_amt(), sumBranchMap.get(entity.getBranch_no())));
						}
					}
				}
			}
		}
		return dtoList;
	}
	
	private void addMapItem(Map<String,Map<String,SetUpTaskDto>> map, String branchNo, String clsNo, SetUpTaskDto dto) {
		Map<String,SetUpTaskDto> childMap = null;
		if(map.get(branchNo) != null) {
			childMap = map.get(branchNo);
		}else {
			childMap = new HashMap<String,SetUpTaskDto>();
		}
		childMap.put(clsNo, dto);
		map.put(branchNo, childMap);
	}
	
	private SetUpTaskDto findMapItem(Map<String,Map<String,SetUpTaskDto>> map, String branchNo, String clsNo) {
		if(map.get(branchNo) == null) {
			return null;
		}
		return map.get(branchNo).get(clsNo);
	}
	
	private void setMonthAim(SetUpTaskDto dto, Integer month, Double aimValue) {
        try {
        	Method method = dto.getClass().getMethod("set月度目标_" + String.format("%02d", month), Double.class);
			method.invoke(dto, aimValue);
		} catch (Exception e) {
			
		}
	}

}
