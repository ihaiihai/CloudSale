package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/22 21:51
 * @Version: 1.0
 **/
public interface SalerService {
    JSONObject salerInfo(String orgId, String begin, String end, String branchNo) throws BaseException;
    JSONArray salerRank(String orgId, String begin, String end, String branchNo) throws BaseException;
    JSONArray salerDetail(String orgId, String begin, String end, String branchNo) throws BaseException;
    JSONArray salerRadio(String orgId, String begin, String end, String branchNo) throws BaseException;
    JSONArray salerOther(String orgId, String begin, String end, String branchNo) throws BaseException;
}
