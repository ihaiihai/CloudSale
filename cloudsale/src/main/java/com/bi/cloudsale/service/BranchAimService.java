package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;

import java.text.ParseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/15 17:45
 * @Version: 1.0
 **/
public interface BranchAimService {
    JSONArray branches(String orgId) throws BaseException;
    JSONArray brands(String orgId) throws BaseException;
    String setAim(String orgId,String userId, JSONObject params) throws BaseException, ParseException;

    JSONArray queryWholeAimGain(String orgId, String year,String month, String branchNos) throws BaseException;
    JSONArray queryWholeAimDetail(String orgId, String year, String branchNos) throws BaseException;

    JSONArray queryClsAimGain(String orgId, String year,String month, String branchNos) throws BaseException;
    JSONArray queryClsAimDetail(String orgId, String year, String branchNos) throws BaseException;

    JSONArray queryBrandAimGain(String orgId, String year,String month, String branchNos) throws BaseException;
    JSONArray queryBrandAimDetail(String orgId, String year, String branchNos) throws BaseException;

    JSONArray querySalerAimGain(String orgId, String year,String month, String branchNos) throws BaseException;
    JSONArray querySalerAimDetail(String orgId, String year, String branchNos) throws BaseException;
}
