package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.entity.BranchEntity;
import com.bi.cloudsale.persistent.entity.BrandEntity;
import com.bi.cloudsale.persistent.entity.ItemClsEntity;
import com.bi.cloudsale.persistent.entity.ItemEntity;
import com.bi.cloudsale.service.ShouqingService;
import com.bi.cloudsale.service.XinpindongxiaoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class XinpindongxiaoServiceImpl implements XinpindongxiaoService {

    private static final Logger log = LoggerFactory.getLogger(XinpindongxiaoServiceImpl.class);

    @Resource
    private BrandDao brandDao;
    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private ClsDayDao itemClsDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private SalerDayDao salerDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private PriceZoneDao priceZoneDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private BrandDayDao brandDayDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {

        List<BranchEntity> branchEntities = branchDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray brands(String orgId) throws BaseException {

        List<BrandEntity> brandEntities = brandDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BrandEntity brandEntity:brandEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("brand_no",brandEntity.getItem_brandno());
            jsonObject.put("brand_name",brandEntity.getItem_brandname());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray items(String orgId) throws BaseException {
        List<ItemEntity> itemEntities = itemDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(ItemEntity itemEntity:itemEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("item_no",itemEntity.getItem_no());
            jsonObject.put("item_name",itemEntity.getItem_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray clses(String orgId) throws BaseException {
        List<ItemClsEntity> itemClsEntities = itemClsDao.list("where org_id ='"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(ItemClsEntity itemClsEntity:itemClsEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("item_clsno",itemClsEntity.getItem_clsno());
            jsonObject.put("item_clsname",itemClsEntity.getItem_clsname());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray query(String orgId, String begin,String end, String branchNos, String itemClses, String brands,String itemNos) throws BaseException {
        String branchSqlin = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSqlin)){
                    branchSqlin = branchSqlin+","+"'"+branch_no+"'";
                }else{
                    branchSqlin = "'"+branch_no+"'";
                }
            }
        }
        String branchSql = StringUtils.isNotEmpty(branchSqlin)?" and t1.branch_no in("+branchSqlin+")":"";
        String itemClsSqlin = "";
        if(StringUtils.isNotEmpty(itemClses)){
            String[] item_clsnos = itemClses.split(",");
            for(String item_clsno:item_clsnos){
                if(!"".equals(itemClsSqlin)){
                    itemClsSqlin = itemClsSqlin+","+"'"+item_clsno+"'";
                }else{
                    itemClsSqlin = "'"+item_clsno+"'";
                }
            }
        }
        String itemClsSql = StringUtils.isNotEmpty(itemClsSqlin)?" and t3.item_clsno in("+itemClsSqlin+")":"";
        String brandSqlin = "";
        if(StringUtils.isNotEmpty(brands)){
            String[] brand_nos = brands.split(",");
            for(String brand_no:brand_nos){
                if(!"".equals(brandSqlin)){
                    brandSqlin = brandSqlin+","+"'"+brand_no+"'";
                }else{
                    brandSqlin = "'"+brand_no+"'";
                }
            }
        }
        String brandSql = StringUtils.isNotEmpty(brandSqlin)?" and t5.brand_no in("+brandSqlin+")":"";

        String itemSqlin = "";
        if(StringUtils.isNotEmpty(itemNos)){
            String[] item_nos = itemNos.split(",");
            for(String item_no:item_nos){
                if(!"".equals(itemSqlin)){
                    itemSqlin = itemSqlin+","+"'"+item_no+"'";
                }else{
                    itemSqlin = "'"+item_no+"'";
                }
            }
        }
        String itemSql = StringUtils.isNotEmpty(itemSqlin)?" and t1.item_no in("+itemSqlin+")":"";
        class ItemData{
            String item_no;
            String item_name;
            String branch_no;
            String item_clsno;
            String item_brandno;
            double sale_cnt;
            double price;
            double total_sale;
            double total_cost;
            double total_bill;
            double total_ret;
            double total_ret_cnt;
            double stock_cnt;
            Timestamp oper_date;

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public String getBranch_no() {
                return branch_no;
            }

            public void setBranch_no(String branch_no) {
                this.branch_no = branch_no;
            }

            public String getItem_clsno() {
                return item_clsno;
            }

            public void setItem_clsno(String item_clsno) {
                this.item_clsno = item_clsno;
            }

            public String getItem_brandno() {
                return item_brandno;
            }

            public void setItem_brandno(String item_brandno) {
                this.item_brandno = item_brandno;
            }

            public double getSale_cnt() {
                return sale_cnt;
            }

            public void setSale_cnt(double sale_cnt) {
                this.sale_cnt = sale_cnt;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_bill() {
                return total_bill;
            }

            public void setTotal_bill(double total_bill) {
                this.total_bill = total_bill;
            }

            public double getTotal_ret() {
                return total_ret;
            }

            public void setTotal_ret(double total_ret) {
                this.total_ret = total_ret;
            }

            public double getTotal_ret_cnt() {
                return total_ret_cnt;
            }

            public void setTotal_ret_cnt(double total_ret_cnt) {
                this.total_ret_cnt = total_ret_cnt;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }

            public Timestamp getOper_date() {
                return oper_date;
            }

            public void setOper_date(Timestamp oper_date) {
                this.oper_date = oper_date;
            }
        }
        Map<String,ItemData> dataMap = new HashMap<>();
        List<Object> objectList = unionSqlDao.query("SELECT t1.item_no,t1.branch_no,t1.sale_amt,t1.sale_qty,t1.bill_cnt,t1.ret_qty,t1.ret_amt,t1.cost_amt,t1.oper_date,\n" +
                " t2.item_name,t2.price,\n" +
                " t4.stock_qty\n" +
                " FROM ItemDayEntity t1,ItemEntity t2,ItemClsEntity t3,StockEntity t4  \n" +
                "WHERE t1.oper_date<='"+end+"' and t1.oper_date >='"+begin+"' \n" +
                " and t1.item_no=t2.item_no and t2.item_clsno=t3.item_clsno and t1.item_no=t4.item_no  \n" +
                " and t1.item_no NOT in (SELECT DISTINCT(item_no) from ItemDayEntity where oper_date<'"+begin+"') and t1.org_id='"+orgId+"'"+branchSql+itemClsSql+brandSql+itemSql+" order by t1.oper_date");
        for(Object itemEntity: objectList){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_no = (String)itemEntityObj[0];
            String branch_no = (String)itemEntityObj[1];
            Double total_sale = (Double)itemEntityObj[2];
            Double total_sale_cnt = (Double)itemEntityObj[3];
            Long bill_cnt = (Long)itemEntityObj[4];
            Double ret_cnt = (Double)itemEntityObj[5];
            Double ret_amt = (Double)itemEntityObj[6];
            Double total_cost = (Double)itemEntityObj[7];
            Timestamp oper_date = (Timestamp)itemEntityObj[8];
            String item_name = (String)itemEntityObj[9];
            Double price = (Double)itemEntityObj[10];
            Double stock_cnt = (Double)itemEntityObj[11];

            ItemData itemData = dataMap.get(item_no);
            if(itemData!=null){
                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                itemData.setTotal_ret(itemData.getTotal_ret()+(ret_amt!=null?ret_amt:0));
                itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                itemData.setOper_date(oper_date);
            }else{
                itemData = new ItemData();
                itemData.setItem_name(item_name);
                itemData.setItem_no(item_no);
                itemData.setBranch_no(branch_no);
                itemData.setPrice(price);
                itemData.setStock_cnt(stock_cnt);

                itemData.setTotal_sale(itemData.getTotal_sale()+(total_sale!=null?total_sale:0));
                itemData.setSale_cnt(itemData.getSale_cnt()+(total_sale_cnt!=null?total_sale_cnt:0));
                itemData.setTotal_bill(itemData.getTotal_bill()+(bill_cnt!=null?bill_cnt:0));
                itemData.setTotal_ret_cnt(itemData.getTotal_ret_cnt()+(ret_cnt!=null?ret_cnt:0));
                itemData.setTotal_ret(itemData.getTotal_ret()+(ret_amt!=null?ret_amt:0));
                itemData.setTotal_cost(itemData.getTotal_cost()+(total_cost!=null?total_cost:0));
                itemData.setOper_date(oper_date);
                dataMap.put(item_no,itemData);
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("item_no",key);
            resultJson.put("条码",key);
            resultJson.put("商品名称",itemData.getItem_name());
            resultJson.put("门店编号",itemData.getBranch_no());
            resultJson.put("门店名称",itemData.getBranch_no());
            resultJson.put("上货数量",0);
            resultJson.put("成本均价",itemData.getPrice());
            resultJson.put("上货成本",itemData.getPrice());
            resultJson.put("销售金额",itemData.getTotal_sale());
            resultJson.put("销售数量",itemData.getSale_cnt());
            resultJson.put("成交单数",itemData.getTotal_bill());
            resultJson.put("客单价",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getTotal_bill()));
            resultJson.put("退货金额",itemData.getTotal_ret());
            resultJson.put("退货率",NumberUtil.getDouble2Format(itemData.getTotal_ret_cnt()/itemData.getSale_cnt()));
            resultJson.put("毛利贡献",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("毛利率",NumberUtil.getDouble2Format((itemData.getTotal_sale()-itemData.getTotal_cost())/itemData.getTotal_cost()));
            resultJson.put("实时库存",itemData.getStock_cnt());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
           long unSaleDays = 0L;
            try {
                unSaleDays = (sdf.parse(end).getTime()-itemData.getOper_date().getTime())/1000/60/60/24;
            } catch (ParseException e) {
                e.printStackTrace();
            }
            resultJson.put("未动天数",unSaleDays);
            results.add(resultJson);
        }
        return results;
    }

}
