package com.bi.cloudsale.service.sys;

import java.util.List;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.RemoteInfoDto;
import com.bi.cloudsale.dto.sys.UserInfoDto;

public interface UserInfoService {

	UserInfoDto queryByAccount(String account) throws BaseException;

	UserInfoDto queryByMobile(String mobile) throws BaseException;
	
	void createUser(UserInfoDto userInfoDto) throws BaseException;
	
	void deleteUser(String userId) throws BaseException;
	
	void updateUser(UserInfoDto userInfoDto) throws BaseException;
	
	List<UserInfoDto> queryByCondition(BaseQueryCondition baseQueryCondition);
	
	UserInfoDto queryByUserId(String userId);

	List<UserInfoDto> queryByOrgId(String orgId);
	
	void updateRemoteInfo(RemoteInfoDto dto) throws BaseException;
}
