package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.ClientFenbuService;
import com.bi.cloudsale.service.XiaoshoufenxiService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.sql.Timestamp;
import java.util.*;

@Service
@Transactional
public class XiaoshoufenxiServiceImpl implements XiaoshoufenxiService {

    private static final Logger log = LoggerFactory.getLogger(XiaoshoufenxiServiceImpl.class);

    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private ItemDayDao itemDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private BrandDayDao brandDayDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private VipDayDao vipDayDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private SalerDayDao salerDayDao;
    @Resource
    private BrandDao brandDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {


        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray byItem(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException {

        String branchSql = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSql)){
                    branchSql = branchSql+","+"'"+branch_no+"'";
                }else{
                    branchSql = "'"+branch_no+"'";
                }
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSql)?" and t1.branch_no in("+branchSql+")":"";
        class ItemData{
            String item_no;
            double total_sale;
            double total_cost;
            double total_sale_cnt;

            double total_sale_last_month;
            double total_cost_last_month;
            double total_sale_cnt_last_month;

            double total_sale_last_year;
            double total_cost_last_year;
            double total_sale_cnt_last_year;

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_cost_last_month() {
                return total_cost_last_month;
            }

            public void setTotal_cost_last_month(double total_cost_last_month) {
                this.total_cost_last_month = total_cost_last_month;
            }

            public double getTotal_sale_cnt_last_month() {
                return total_sale_cnt_last_month;
            }

            public void setTotal_sale_cnt_last_month(double total_sale_cnt_last_month) {
                this.total_sale_cnt_last_month = total_sale_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getTotal_cost_last_year() {
                return total_cost_last_year;
            }

            public void setTotal_cost_last_year(double total_cost_last_year) {
                this.total_cost_last_year = total_cost_last_year;
            }

            public double getTotal_sale_cnt_last_year() {
                return total_sale_cnt_last_year;
            }

            public void setTotal_sale_cnt_last_year(double total_sale_cnt_last_year) {
                this.total_sale_cnt_last_year = total_sale_cnt_last_year;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
        //本期
        double all_sale =0;
        List<Object> all_sales = unionSqlDao.query("select t1.item_no, sum(t1.sale_amt) as sale_amt from ItemDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[1];
            all_sale += total_sale!=null?total_sale:0;
        }
        List<Object> objectThis = unionSqlDao.query("select t1.item_no,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from ItemDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' group by t1.item_no",pageNo,pageSize);
        for(Object itemEntity:objectThis){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_no);
            if(dataDay!=null){
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
            }else{
                dataDay = new ItemData();
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
                dataMap.put(item_no,dataDay);
            }
        }

        //环比
        double all_sale_last_month =0;
        List<Object> objectThis_last_month = unionSqlDao.query("select t1.item_no,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from ItemDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
               + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-30) and to_days(t1.oper_date)<=(to_days('"+end+"')-30) group by t1.item_no");
        for(Object itemEntity:objectThis_last_month){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_no);
            all_sale_last_month += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_month(dataDay.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_month(dataDay.getTotal_cost_last_month()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_month(dataDay.getTotal_sale_cnt_last_month()+(total_qty!=null?total_qty:0));
            }
        }
        //同比
        double all_sale_last_year =0;
        List<Object> objectThis_last_year = unionSqlDao.query("select t1.item_no,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from ItemDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-365) and to_days(t1.oper_date)<=(to_days('"+end+"')-365) group by t1.item_no");
        for(Object itemEntity:objectThis_last_year){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_no);
            all_sale_last_year += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_year(dataDay.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_year(dataDay.getTotal_cost_last_year()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_year(dataDay.getTotal_sale_cnt_last_year()+(total_qty!=null?total_qty:0));
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("分类编号",key);
            resultJson.put("分类名称",key);
            resultJson.put("本期_销售",itemData.getTotal_sale());
            resultJson.put("本期_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale()/all_sale));
            resultJson.put("本期_销量",itemData.getTotal_sale_cnt());
            resultJson.put("本期_利润",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("本期_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale()-itemData.getTotal_cost())/itemData.getTotal_sale())));
            resultJson.put("环比_销售",itemData.getTotal_sale_last_month());
            resultJson.put("环比_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/all_sale_last_month));
            resultJson.put("环比_销量",itemData.getTotal_sale_cnt_last_month());
            resultJson.put("环比_利润",itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month());
            resultJson.put("环比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month())/itemData.getTotal_sale_last_month())));
            resultJson.put("同比_销售",itemData.getTotal_sale_last_year());
            resultJson.put("同比_占比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/all_sale_last_year));
            resultJson.put("同比_销量",itemData.getTotal_sale_cnt_last_year());
            resultJson.put("同比_利润",itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year());
            resultJson.put("同比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year())/itemData.getTotal_sale_last_year())));
            resultJson.put("同比_增长率",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray byBranch(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException {

        String branchSql = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSql)){
                    branchSql = branchSql+","+"'"+branch_no+"'";
                }else{
                    branchSql = "'"+branch_no+"'";
                }
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSql)?" and t1.branch_no in("+branchSql+")":"";
        class ItemData{
            String item_no;
            double total_sale;
            double total_cost;
            double total_sale_cnt;

            double total_sale_last_month;
            double total_cost_last_month;
            double total_sale_cnt_last_month;

            double total_sale_last_year;
            double total_cost_last_year;
            double total_sale_cnt_last_year;

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_cost_last_month() {
                return total_cost_last_month;
            }

            public void setTotal_cost_last_month(double total_cost_last_month) {
                this.total_cost_last_month = total_cost_last_month;
            }

            public double getTotal_sale_cnt_last_month() {
                return total_sale_cnt_last_month;
            }

            public void setTotal_sale_cnt_last_month(double total_sale_cnt_last_month) {
                this.total_sale_cnt_last_month = total_sale_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getTotal_cost_last_year() {
                return total_cost_last_year;
            }

            public void setTotal_cost_last_year(double total_cost_last_year) {
                this.total_cost_last_year = total_cost_last_year;
            }

            public double getTotal_sale_cnt_last_year() {
                return total_sale_cnt_last_year;
            }

            public void setTotal_sale_cnt_last_year(double total_sale_cnt_last_year) {
                this.total_sale_cnt_last_year = total_sale_cnt_last_year;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
        //本期
        double all_sale =0;
        List<Object> all_sales = unionSqlDao.query("select t1.branch_no,sum(t1.sale_amt) as sale_amt from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[1];
            all_sale += total_sale!=null?total_sale:0;
        }
        List<Object> objectThis = unionSqlDao.query("select t1.branch_no,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' group by t1.branch_no",pageNo,pageSize);
        for(Object itemEntity:objectThis){
            Object[]itemEntityObj = (Object[])itemEntity;
            String branch_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(branch_no);
            if(dataDay!=null){
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
            }else{
                dataDay = new ItemData();
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
                dataMap.put(branch_no,dataDay);
            }
        }

        //环比
        double all_sale_last_month =0;
        List<Object> objectThis_last_month = unionSqlDao.query("select t1.branch_no,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-30) and to_days(t1.oper_date)<=(to_days('"+end+"')-30) group by t1.branch_no");
        for(Object itemEntity:objectThis_last_month){
            Object[]itemEntityObj = (Object[])itemEntity;
            String branch_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(branch_no);
            all_sale_last_month += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_month(dataDay.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_month(dataDay.getTotal_cost_last_month()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_month(dataDay.getTotal_sale_cnt_last_month()+(total_qty!=null?total_qty:0));
            }
        }
        //同比
        double all_sale_last_year =0;
        List<Object> objectThis_last_year = unionSqlDao.query("select t1.branch_no,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-365) and to_days(t1.oper_date)<=(to_days('"+end+"')-365) group by t1.branch_no");
        for(Object itemEntity:objectThis_last_year){
            Object[]itemEntityObj = (Object[])itemEntity;
            String branch_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(branch_no);
            all_sale_last_year += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_year(dataDay.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_year(dataDay.getTotal_cost_last_year()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_year(dataDay.getTotal_sale_cnt_last_year()+(total_qty!=null?total_qty:0));
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("门店编号",key);
            resultJson.put("门店名称",key);
            resultJson.put("本期_销售",itemData.getTotal_sale());
            resultJson.put("本期_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale()/all_sale));
            resultJson.put("本期_销量",itemData.getTotal_sale_cnt());
            resultJson.put("本期_利润",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("本期_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale()-itemData.getTotal_cost())/itemData.getTotal_sale())));
            resultJson.put("环比_销售",itemData.getTotal_sale_last_month());
            resultJson.put("环比_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/all_sale_last_month));
            resultJson.put("环比_销量",itemData.getTotal_sale_cnt_last_month());
            resultJson.put("环比_利润",itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month());
            resultJson.put("环比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month())/itemData.getTotal_sale_last_month())));
            resultJson.put("同比_销售",itemData.getTotal_sale_last_year());
            resultJson.put("同比_占比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/all_sale_last_year));
            resultJson.put("同比_销量",itemData.getTotal_sale_cnt_last_year());
            resultJson.put("同比_利润",itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year());
            resultJson.put("同比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year())/itemData.getTotal_sale_last_year())));
            resultJson.put("同比_增长率",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray byMonth(String orgId, String yyyy_MM, String branchNos,Integer pageSize,Integer pageNo) throws BaseException {
        String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
        String begin = yyyy_MM+"-01";
        String end = yyyy_MM+"-"+lastDayInMonth;

        String branchSql = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSql)){
                    branchSql = branchSql+","+"'"+branch_no+"'";
                }else{
                    branchSql = "'"+branch_no+"'";
                }
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSql)?" and branch_no in("+branchSql+")":"";
        class ItemData{
            String item_no;
            double total_sale;
            double total_cost;
            double total_sale_cnt;

            double total_sale_last_month;
            double total_cost_last_month;
            double total_sale_cnt_last_month;

            double total_sale_last_year;
            double total_cost_last_year;
            double total_sale_cnt_last_year;

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_cost_last_month() {
                return total_cost_last_month;
            }

            public void setTotal_cost_last_month(double total_cost_last_month) {
                this.total_cost_last_month = total_cost_last_month;
            }

            public double getTotal_sale_cnt_last_month() {
                return total_sale_cnt_last_month;
            }

            public void setTotal_sale_cnt_last_month(double total_sale_cnt_last_month) {
                this.total_sale_cnt_last_month = total_sale_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getTotal_cost_last_year() {
                return total_cost_last_year;
            }

            public void setTotal_cost_last_year(double total_cost_last_year) {
                this.total_cost_last_year = total_cost_last_year;
            }

            public double getTotal_sale_cnt_last_year() {
                return total_sale_cnt_last_year;
            }

            public void setTotal_sale_cnt_last_year(double total_sale_cnt_last_year) {
                this.total_sale_cnt_last_year = total_sale_cnt_last_year;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
        //本期
        List<Object> objectThis = unionSqlDao.query("select t1.branch_no,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' group by t1.branch_no");
        for(Object itemEntity:objectThis){
            Object[]itemEntityObj = (Object[])itemEntity;
            String branch_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(yyyy_MM);
            if(dataDay!=null){
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
            }else{
                dataDay = new ItemData();
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
                dataMap.put(yyyy_MM,dataDay);
            }
        }

        //环比
        List<Object> objectThis_last_month = unionSqlDao.query("select t1.branch_no,t1.sale_amt,t1.cost_amt,t1.sale_qty from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-30) and to_days(t1.oper_date)<=(to_days('"+end+"')-30)");
        for(Object itemEntity:objectThis_last_month){
            Object[]itemEntityObj = (Object[])itemEntity;
            String branch_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(yyyy_MM);
            if(dataDay!=null){
                dataDay.setTotal_sale_last_month(dataDay.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_month(dataDay.getTotal_cost_last_month()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_month(dataDay.getTotal_sale_cnt_last_month()+(total_qty!=null?total_qty:0));
            }
        }
        //同比
        List<Object> objectThis_last_year = unionSqlDao.query("select t1.branch_no,t1.sale_amt,t1.cost_amt,t1.sale_qty from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-365) and to_days(t1.oper_date)<=(to_days('"+end+"')-365)");
        for(Object itemEntity:objectThis_last_year){
            Object[]itemEntityObj = (Object[])itemEntity;
            String branch_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(yyyy_MM);
            if(dataDay!=null){
                dataDay.setTotal_sale_last_year(dataDay.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_year(dataDay.getTotal_cost_last_year()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_year(dataDay.getTotal_sale_cnt_last_year()+(total_qty!=null?total_qty:0));
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("月份",key);
            resultJson.put("本期_销售",itemData.getTotal_sale());
            resultJson.put("本期_占比", 1);
            resultJson.put("本期_销量",itemData.getTotal_sale_cnt());
            resultJson.put("本期_利润",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("本期_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale()-itemData.getTotal_cost())/itemData.getTotal_sale())));
            resultJson.put("环比_销售",itemData.getTotal_sale_last_month());
            resultJson.put("环比_占比", 1);
            resultJson.put("环比_销量",itemData.getTotal_sale_cnt_last_month());
            resultJson.put("环比_利润",itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month());
            resultJson.put("环比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month())/itemData.getTotal_sale_last_month())));
            resultJson.put("同比_销售",itemData.getTotal_sale_last_year());
            resultJson.put("同比_占比",1);
            resultJson.put("同比_销量",itemData.getTotal_sale_cnt_last_year());
            resultJson.put("同比_利润",itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year());
            resultJson.put("同比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year())/itemData.getTotal_sale_last_year())));
            resultJson.put("同比_增长率",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray byDay(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException {

        String branchSql = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSql)){
                    branchSql = branchSql+","+"'"+branch_no+"'";
                }else{
                    branchSql = "'"+branch_no+"'";
                }
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSql)?" and t1.branch_no in("+branchSql+")":"";
        class ItemData{
            String item_no;
            double total_sale;
            double total_cost;
            double total_sale_cnt;

            double total_sale_last_month;
            double total_cost_last_month;
            double total_sale_cnt_last_month;

            double total_sale_last_year;
            double total_cost_last_year;
            double total_sale_cnt_last_year;

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_cost_last_month() {
                return total_cost_last_month;
            }

            public void setTotal_cost_last_month(double total_cost_last_month) {
                this.total_cost_last_month = total_cost_last_month;
            }

            public double getTotal_sale_cnt_last_month() {
                return total_sale_cnt_last_month;
            }

            public void setTotal_sale_cnt_last_month(double total_sale_cnt_last_month) {
                this.total_sale_cnt_last_month = total_sale_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getTotal_cost_last_year() {
                return total_cost_last_year;
            }

            public void setTotal_cost_last_year(double total_cost_last_year) {
                this.total_cost_last_year = total_cost_last_year;
            }

            public double getTotal_sale_cnt_last_year() {
                return total_sale_cnt_last_year;
            }

            public void setTotal_sale_cnt_last_year(double total_sale_cnt_last_year) {
                this.total_sale_cnt_last_year = total_sale_cnt_last_year;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
        //本期
        double all_sale =0;
        List<Object> all_sales = unionSqlDao.query("select t1.oper_date, sum(t1.sale_amt) as sale_amt from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[1];
            all_sale += total_sale!=null?total_sale:0;
        }
        List<Object> objectThis = unionSqlDao.query("select t1.oper_date,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' group by t1.oper_date order by t1.oper_date",pageNo,pageSize);
        for(Object itemEntity:objectThis){
            Object[]itemEntityObj = (Object[])itemEntity;
            String oper_date = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(oper_date);
            if(dataDay!=null){
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
            }else{
                dataDay = new ItemData();
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
                dataMap.put(oper_date,dataDay);
            }
        }

        //环比
        double all_sale_last_month =0;
        List<Object> objectThis_last_month = unionSqlDao.query("select t1.oper_date,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-30) and to_days(t1.oper_date)<=(to_days('"+end+"')-30) group by t1.oper_date");
        for(Object itemEntity:objectThis_last_month){
            Object[]itemEntityObj = (Object[])itemEntity;
            String oper_date = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(oper_date);
            all_sale_last_month += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_month(dataDay.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_month(dataDay.getTotal_cost_last_month()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_month(dataDay.getTotal_sale_cnt_last_month()+(total_qty!=null?total_qty:0));
            }
        }
        //同比
        double all_sale_last_year =0;
        List<Object> objectThis_last_year = unionSqlDao.query("select t1.oper_date,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BranchDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-365) and to_days(t1.oper_date)<=(to_days('"+end+"')-365) group by t1.oper_date");
        for(Object itemEntity:objectThis_last_year){
            Object[]itemEntityObj = (Object[])itemEntity;
            String oper_date = DateUtil.formatDate(new Date(((Timestamp)itemEntityObj[0]).getTime()),DateUtil.SIMPLE_DATE_FORMAT);
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(oper_date);
            all_sale_last_year += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_year(dataDay.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_year(dataDay.getTotal_cost_last_year()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_year(dataDay.getTotal_sale_cnt_last_year()+(total_qty!=null?total_qty:0));
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("日期",key);
            resultJson.put("本期_销售",itemData.getTotal_sale());
            resultJson.put("本期_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale()/all_sale));
            resultJson.put("本期_销量",itemData.getTotal_sale_cnt());
            resultJson.put("本期_利润",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("本期_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale()-itemData.getTotal_cost())/itemData.getTotal_sale())));
            resultJson.put("环比_销售",itemData.getTotal_sale_last_month());
            resultJson.put("环比_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/all_sale_last_month));
            resultJson.put("环比_销量",itemData.getTotal_sale_cnt_last_month());
            resultJson.put("环比_利润",itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month());
            resultJson.put("环比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month())/itemData.getTotal_sale_last_month())));
            resultJson.put("同比_销售",itemData.getTotal_sale_last_year());
            resultJson.put("同比_占比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/all_sale_last_year));
            resultJson.put("同比_销量",itemData.getTotal_sale_cnt_last_year());
            resultJson.put("同比_利润",itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year());
            resultJson.put("同比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year())/itemData.getTotal_sale_last_year())));
            resultJson.put("同比_增长率",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray byCls(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException {

        String branchSql = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSql)){
                    branchSql = branchSql+","+"'"+branch_no+"'";
                }else{
                    branchSql = "'"+branch_no+"'";
                }
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSql)?" and t1.branch_no in("+branchSql+")":"";
        class ItemData{
            String item_no;
            double total_sale;
            double total_cost;
            double total_sale_cnt;

            double total_sale_last_month;
            double total_cost_last_month;
            double total_sale_cnt_last_month;

            double total_sale_last_year;
            double total_cost_last_year;
            double total_sale_cnt_last_year;

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_cost_last_month() {
                return total_cost_last_month;
            }

            public void setTotal_cost_last_month(double total_cost_last_month) {
                this.total_cost_last_month = total_cost_last_month;
            }

            public double getTotal_sale_cnt_last_month() {
                return total_sale_cnt_last_month;
            }

            public void setTotal_sale_cnt_last_month(double total_sale_cnt_last_month) {
                this.total_sale_cnt_last_month = total_sale_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getTotal_cost_last_year() {
                return total_cost_last_year;
            }

            public void setTotal_cost_last_year(double total_cost_last_year) {
                this.total_cost_last_year = total_cost_last_year;
            }

            public double getTotal_sale_cnt_last_year() {
                return total_sale_cnt_last_year;
            }

            public void setTotal_sale_cnt_last_year(double total_sale_cnt_last_year) {
                this.total_sale_cnt_last_year = total_sale_cnt_last_year;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
        //本期
        double all_sale =0;
        List<Object> all_sales = unionSqlDao.query("select t1.item_clsno, sum(t1.sale_amt) as sale_amt from ClsDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[1];
            all_sale += total_sale!=null?total_sale:0;
        }
        List<Object> objectThis = unionSqlDao.query("select t1.item_clsno,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from ClsDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' group by t1.item_clsno",pageNo,pageSize);
        for(Object itemEntity:objectThis){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_clsno = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_clsno);
            if(dataDay!=null){
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
            }else{
                dataDay = new ItemData();
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
                dataMap.put(item_clsno,dataDay);
            }
        }

        //环比
        double all_sale_last_month =0;
        List<Object> objectThis_last_month = unionSqlDao.query("select t1.item_clsno,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from ClsDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-30) and to_days(t1.oper_date)<=(to_days('"+end+"')-30) group by t1.item_clsno");
        for(Object itemEntity:objectThis_last_month){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_clsno = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_clsno);
            all_sale_last_month += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_month(dataDay.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_month(dataDay.getTotal_cost_last_month()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_month(dataDay.getTotal_sale_cnt_last_month()+(total_qty!=null?total_qty:0));
            }
        }
        //同比
        double all_sale_last_year =0;
        List<Object> objectThis_last_year = unionSqlDao.query("select t1.item_clsno,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from ClsDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-365) and to_days(t1.oper_date)<=(to_days('"+end+"')-365) group by t1.item_clsno");
        for(Object itemEntity:objectThis_last_year){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_clsno = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_clsno);
            all_sale_last_year += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_year(dataDay.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_year(dataDay.getTotal_cost_last_year()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_year(dataDay.getTotal_sale_cnt_last_year()+(total_qty!=null?total_qty:0));
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("类别编号",key);
            resultJson.put("类别名称",key);
            resultJson.put("本期_销售",itemData.getTotal_sale());
            resultJson.put("本期_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale()/all_sale));
            resultJson.put("本期_销量",itemData.getTotal_sale_cnt());
            resultJson.put("本期_利润",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("本期_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale()-itemData.getTotal_cost())/itemData.getTotal_sale())));
            resultJson.put("环比_销售",itemData.getTotal_sale_last_month());
            resultJson.put("环比_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/all_sale_last_month));
            resultJson.put("环比_销量",itemData.getTotal_sale_cnt_last_month());
            resultJson.put("环比_利润",itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month());
            resultJson.put("环比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month())/itemData.getTotal_sale_last_month())));
            resultJson.put("同比_销售",itemData.getTotal_sale_last_year());
            resultJson.put("同比_占比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/all_sale_last_year));
            resultJson.put("同比_销量",itemData.getTotal_sale_cnt_last_year());
            resultJson.put("同比_利润",itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year());
            resultJson.put("同比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year())/itemData.getTotal_sale_last_year())));
            resultJson.put("同比_增长率",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray byBrand(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException {

        String branchSql = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSql)){
                    branchSql = branchSql+","+"'"+branch_no+"'";
                }else{
                    branchSql = "'"+branch_no+"'";
                }
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSql)?" and t1.branch_no in("+branchSql+")":"";
        class ItemData{
            String item_no;
            double total_sale;
            double total_cost;
            double total_sale_cnt;

            double total_sale_last_month;
            double total_cost_last_month;
            double total_sale_cnt_last_month;

            double total_sale_last_year;
            double total_cost_last_year;
            double total_sale_cnt_last_year;

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_cost_last_month() {
                return total_cost_last_month;
            }

            public void setTotal_cost_last_month(double total_cost_last_month) {
                this.total_cost_last_month = total_cost_last_month;
            }

            public double getTotal_sale_cnt_last_month() {
                return total_sale_cnt_last_month;
            }

            public void setTotal_sale_cnt_last_month(double total_sale_cnt_last_month) {
                this.total_sale_cnt_last_month = total_sale_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getTotal_cost_last_year() {
                return total_cost_last_year;
            }

            public void setTotal_cost_last_year(double total_cost_last_year) {
                this.total_cost_last_year = total_cost_last_year;
            }

            public double getTotal_sale_cnt_last_year() {
                return total_sale_cnt_last_year;
            }

            public void setTotal_sale_cnt_last_year(double total_sale_cnt_last_year) {
                this.total_sale_cnt_last_year = total_sale_cnt_last_year;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
        //本期
        double all_sale =0;
        List<Object> all_sales = unionSqlDao.query("select t1.item_brandno, sum(t1.sale_amt) as sale_amt from BrandDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[1];
            all_sale += total_sale!=null?total_sale:0;
        }
        List<Object> objectThis = unionSqlDao.query("select t1.item_brandno,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BrandDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' group by t1.item_brandno",pageNo,pageSize);
        for(Object itemEntity:objectThis){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_brandno = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_brandno);
            if(dataDay!=null){
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
            }else{
                dataDay = new ItemData();
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
                dataMap.put(item_brandno,dataDay);
            }
        }

        //环比
        double all_sale_last_month =0;
        List<Object> objectThis_last_month = unionSqlDao.query("select t1.item_brandno,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BrandDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-30) and to_days(t1.oper_date)<=(to_days('"+end+"')-30) group by t1.item_brandno");
        for(Object itemEntity:objectThis_last_month){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_brandno = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_brandno);
            all_sale_last_month += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_month(dataDay.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_month(dataDay.getTotal_cost_last_month()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_month(dataDay.getTotal_sale_cnt_last_month()+(total_qty!=null?total_qty:0));
            }
        }
        //同比
        double all_sale_last_year =0;
        List<Object> objectThis_last_year = unionSqlDao.query("select t1.item_brandno,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from BrandDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-365) and to_days(t1.oper_date)<=(to_days('"+end+"')-365) group by t1.item_brandno");
        for(Object itemEntity:objectThis_last_year){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_brandno = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_brandno);
            all_sale_last_year += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_year(dataDay.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_year(dataDay.getTotal_cost_last_year()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_year(dataDay.getTotal_sale_cnt_last_year()+(total_qty!=null?total_qty:0));
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("品牌编号",key);
            resultJson.put("品牌名称",key);
            resultJson.put("本期_销售",itemData.getTotal_sale());
            resultJson.put("本期_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale()/all_sale));
            resultJson.put("本期_销量",itemData.getTotal_sale_cnt());
            resultJson.put("本期_利润",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("本期_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale()-itemData.getTotal_cost())/itemData.getTotal_sale())));
            resultJson.put("环比_销售",itemData.getTotal_sale_last_month());
            resultJson.put("环比_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/all_sale_last_month));
            resultJson.put("环比_销量",itemData.getTotal_sale_cnt_last_month());
            resultJson.put("环比_利润",itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month());
            resultJson.put("环比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month())/itemData.getTotal_sale_last_month())));
            resultJson.put("同比_销售",itemData.getTotal_sale_last_year());
            resultJson.put("同比_占比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/all_sale_last_year));
            resultJson.put("同比_销量",itemData.getTotal_sale_cnt_last_year());
            resultJson.put("同比_利润",itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year());
            resultJson.put("同比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year())/itemData.getTotal_sale_last_year())));
            resultJson.put("同比_增长率",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray bySaler(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException {

        String branchSql = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSql)){
                    branchSql = branchSql+","+"'"+branch_no+"'";
                }else{
                    branchSql = "'"+branch_no+"'";
                }
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSql)?" and t1.branch_no in("+branchSql+")":"";
        class ItemData{
            String item_no;
            double total_sale;
            double total_cost;
            double total_sale_cnt;

            double total_sale_last_month;
            double total_cost_last_month;
            double total_sale_cnt_last_month;

            double total_sale_last_year;
            double total_cost_last_year;
            double total_sale_cnt_last_year;

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public double getTotal_sale_cnt() {
                return total_sale_cnt;
            }

            public void setTotal_sale_cnt(double total_sale_cnt) {
                this.total_sale_cnt = total_sale_cnt;
            }

            public double getTotal_sale_last_month() {
                return total_sale_last_month;
            }

            public void setTotal_sale_last_month(double total_sale_last_month) {
                this.total_sale_last_month = total_sale_last_month;
            }

            public double getTotal_cost_last_month() {
                return total_cost_last_month;
            }

            public void setTotal_cost_last_month(double total_cost_last_month) {
                this.total_cost_last_month = total_cost_last_month;
            }

            public double getTotal_sale_cnt_last_month() {
                return total_sale_cnt_last_month;
            }

            public void setTotal_sale_cnt_last_month(double total_sale_cnt_last_month) {
                this.total_sale_cnt_last_month = total_sale_cnt_last_month;
            }

            public double getTotal_sale_last_year() {
                return total_sale_last_year;
            }

            public void setTotal_sale_last_year(double total_sale_last_year) {
                this.total_sale_last_year = total_sale_last_year;
            }

            public double getTotal_cost_last_year() {
                return total_cost_last_year;
            }

            public void setTotal_cost_last_year(double total_cost_last_year) {
                this.total_cost_last_year = total_cost_last_year;
            }

            public double getTotal_sale_cnt_last_year() {
                return total_sale_cnt_last_year;
            }

            public void setTotal_sale_cnt_last_year(double total_sale_cnt_last_year) {
                this.total_sale_cnt_last_year = total_sale_cnt_last_year;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
        //本期
        double all_sale =0;
        List<Object> all_sales = unionSqlDao.query("select t1.sale_id, sum(t1.sale_amt) as sale_amt from SalerDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"'");
        for(Object itemEntity:all_sales){
            Object[]itemEntityObj = (Object[])itemEntity;
            Double total_sale = (Double)itemEntityObj[1];
            all_sale += total_sale!=null?total_sale:0;
        }
        List<Object> objectThis = unionSqlDao.query("select t1.sale_id,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from SalerDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' group by t1.sale_id",pageNo,pageSize);
        for(Object itemEntity:objectThis){
            Object[]itemEntityObj = (Object[])itemEntity;
            String sale_id = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(sale_id);
            if(dataDay!=null){
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
            }else{
                dataDay = new ItemData();
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt(dataDay.getTotal_sale_cnt()+(total_qty!=null?total_qty:0));
                dataMap.put(sale_id,dataDay);
            }
        }

        //环比
        double all_sale_last_month =0;
        List<Object> objectThis_last_month = unionSqlDao.query("select t1.sale_id,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from SalerDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-30) and to_days(t1.oper_date)<=(to_days('"+end+"')-30) group by t1.sale_id");
        for(Object itemEntity:objectThis_last_month){
            Object[]itemEntityObj = (Object[])itemEntity;
            String sale_id = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(sale_id);
            all_sale_last_month += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_month(dataDay.getTotal_sale_last_month()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_month(dataDay.getTotal_cost_last_month()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_month(dataDay.getTotal_sale_cnt_last_month()+(total_qty!=null?total_qty:0));
            }
        }
        //同比
        double all_sale_last_year =0;
        List<Object> objectThis_last_year = unionSqlDao.query("select t1.sale_id,sum(t1.sale_amt) as sale_amt,sum(t1.cost_amt) as cost_amt,sum(t1.sale_qty) as sale_qty from SalerDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                + " and to_days(t1.oper_date)>=(to_days('"+begin+"')-365) and to_days(t1.oper_date)<=(to_days('"+end+"')-365) group by t1.sale_id");
        for(Object itemEntity:objectThis_last_year){
            Object[]itemEntityObj = (Object[])itemEntity;
            String sale_id = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_cost = (Double)itemEntityObj[2];
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(sale_id);
            all_sale_last_year += total_sale!=null?total_sale:0;
            if(dataDay!=null){
                dataDay.setTotal_sale_last_year(dataDay.getTotal_sale_last_year()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cost_last_year(dataDay.getTotal_cost_last_year()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_sale_cnt_last_year(dataDay.getTotal_sale_cnt_last_year()+(total_qty!=null?total_qty:0));
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("营业员编号",key);
            resultJson.put("营业员名称",key);
            resultJson.put("本期_销售",itemData.getTotal_sale());
            resultJson.put("本期_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale()/all_sale));
            resultJson.put("本期_销量",itemData.getTotal_sale_cnt());
            resultJson.put("本期_利润",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("本期_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale()-itemData.getTotal_cost())/itemData.getTotal_sale())));
            resultJson.put("环比_销售",itemData.getTotal_sale_last_month());
            resultJson.put("环比_占比", NumberUtil.getDouble2Format(itemData.getTotal_sale_last_month()/all_sale_last_month));
            resultJson.put("环比_销量",itemData.getTotal_sale_cnt_last_month());
            resultJson.put("环比_利润",itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month());
            resultJson.put("环比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_month()-itemData.getTotal_cost_last_month())/itemData.getTotal_sale_last_month())));
            resultJson.put("同比_销售",itemData.getTotal_sale_last_year());
            resultJson.put("同比_占比",NumberUtil.getDouble2Format(itemData.getTotal_sale_last_year()/all_sale_last_year));
            resultJson.put("同比_销量",itemData.getTotal_sale_cnt_last_year());
            resultJson.put("同比_利润",itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year());
            resultJson.put("同比_毛利率",NumberUtil.getDouble2Format(((itemData.getTotal_sale_last_year()-itemData.getTotal_cost_last_year())/itemData.getTotal_sale_last_year())));
            resultJson.put("同比_增长率",itemData.getTotal_sale_last_year());
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray itemDetail(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException {

        String branchSql = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSql)){
                    branchSql = branchSql+","+"'"+branch_no+"'";
                }else{
                    branchSql = "'"+branch_no+"'";
                }
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSql)?" and t1.branch_no in("+branchSql+")":"";
        class ItemData{
            String item_no;
            String item_subno;
            String item_name;
            double cost_price;
            double sale_price;
            double stock_cnt;

            double total_sale;
            double total_cost;
            double total_cnt;
            double total_ret;
            double total_ret_cnt;
            double total_dis;

            public double getTotal_ret_cnt() {
                return total_ret_cnt;
            }

            public void setTotal_ret_cnt(double total_ret_cnt) {
                this.total_ret_cnt = total_ret_cnt;
            }

            public double getTotal_cost() {
                return total_cost;
            }

            public void setTotal_cost(double total_cost) {
                this.total_cost = total_cost;
            }

            public String getItem_no() {
                return item_no;
            }

            public void setItem_no(String item_no) {
                this.item_no = item_no;
            }

            public String getItem_subno() {
                return item_subno;
            }

            public void setItem_subno(String item_subno) {
                this.item_subno = item_subno;
            }

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getCost_price() {
                return cost_price;
            }

            public void setCost_price(double cost_price) {
                this.cost_price = cost_price;
            }

            public double getSale_price() {
                return sale_price;
            }

            public void setSale_price(double sale_price) {
                this.sale_price = sale_price;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }

            public double getTotal_sale() {
                return total_sale;
            }

            public void setTotal_sale(double total_sale) {
                this.total_sale = total_sale;
            }

            public double getTotal_cnt() {
                return total_cnt;
            }

            public void setTotal_cnt(double total_cnt) {
                this.total_cnt = total_cnt;
            }

            public double getTotal_ret() {
                return total_ret;
            }

            public void setTotal_ret(double total_ret) {
                this.total_ret = total_ret;
            }

            public double getTotal_dis() {
                return total_dis;
            }

            public void setTotal_dis(double total_dis) {
                this.total_dis = total_dis;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
        List<Object> objectThis = unionSqlDao.query("select t1.item_no,sum(t1.sale_amt) as sale_amt,t1.oper_date,sum(t1.sale_qty) as sale_qty,sum(t1.ret_amt) as ret_amt,sum(t1.dis_amt) as dis_amt,sum(t1.cost_amt) as cost_amt,sum(t1.ret_qty) as ret_qty from ItemDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' group by t1.item_no",pageNo,pageSize);
        for(Object itemEntity:objectThis){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_no = itemEntityObj[0].toString();
            Double total_sale = (Double)itemEntityObj[1];
            Double total_qty = (Double)itemEntityObj[3];
            Double total_ret = (Double)itemEntityObj[4];
            Double total_dis = (Double)itemEntityObj[5];
            Double total_cost = (Double)itemEntityObj[6];
            Double total_ret_cnt = (Double)itemEntityObj[7];
            ItemData dataDay = dataMap.get(item_no);
            if(dataDay!=null){
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cnt(dataDay.getTotal_cnt()+(total_qty!=null?total_qty:0));
                dataDay.setTotal_ret(dataDay.getTotal_ret()+(total_ret!=null?total_ret:0));
                dataDay.setTotal_dis(dataDay.getTotal_dis()+(total_dis!=null?total_dis:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_ret_cnt(dataDay.getTotal_ret_cnt()+(total_ret_cnt!=null?total_ret_cnt:0));
            }else{
                dataDay = new ItemData();
                dataDay.setTotal_sale(dataDay.getTotal_sale()+(total_sale!=null?total_sale:0));
                dataDay.setTotal_cnt(dataDay.getTotal_cnt()+(total_qty!=null?total_qty:0));
                dataDay.setTotal_ret(dataDay.getTotal_ret()+(total_ret!=null?total_ret:0));
                dataDay.setTotal_dis(dataDay.getTotal_dis()+(total_dis!=null?total_dis:0));
                dataDay.setTotal_cost(dataDay.getTotal_cost()+(total_cost!=null?total_cost:0));
                dataDay.setTotal_ret_cnt(dataDay.getTotal_ret_cnt()+(total_ret_cnt!=null?total_ret_cnt:0));
                dataMap.put(item_no,dataDay);
            }
        }

        String item_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,branch_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_nosStr)){
                item_nosStr = item_nosStr + ",'"+str+"'";
            }else{
                item_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_nosStr)){
            //2商品名
            List<ItemEntity> itemEntities = itemDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'");
            for(ItemEntity itemEntity : itemEntities){
                if( dataMap.get(itemEntity.getItem_no())!=null){
                    dataMap.get(itemEntity.getItem_no()).setItem_name(itemEntity.getItem_name());
                    dataMap.get(itemEntity.getItem_no()).setItem_subno(itemEntity.getItem_subno());
                    dataMap.get(itemEntity.getItem_no()).setSale_price(itemEntity.getSale_price());
                    dataMap.get(itemEntity.getItem_no()).setCost_price(itemEntity.getPrice());
                }
            }
            //3库存
            List<StockEntity> stockEntities = stockDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'"+branchSql);
            for(StockEntity stockEntity : stockEntities){
                if(dataMap.get(stockEntity.getItem_no())!=null){
                    dataMap.get(stockEntity.getItem_no()).setStock_cnt(stockEntity.getStock_qty());
                }
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("item_no",itemData.getItem_subno());
            resultJson.put("条码",itemData.getItem_subno());
            resultJson.put("货号",key);
            resultJson.put("商品名称", itemData.getItem_name());
            resultJson.put("零售价",itemData.getSale_price());
            resultJson.put("成交均价",NumberUtil.getDouble2Format(itemData.getTotal_sale()/itemData.getTotal_cnt()));
            resultJson.put("平均折扣",NumberUtil.getDouble2Format((itemData.getTotal_dis()/itemData.getTotal_sale())));
            resultJson.put("销售金额",itemData.getTotal_sale());
            resultJson.put("销售数量", itemData.getTotal_cnt());
            resultJson.put("有效数量",0);
            resultJson.put("利润",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("退货金额",itemData.getTotal_ret());
            resultJson.put("退货率",NumberUtil.getDouble2Format(itemData.getTotal_ret_cnt()/itemData.getTotal_cnt()*100));
            resultJson.put("毛利",itemData.getTotal_sale()-itemData.getTotal_cost());
            resultJson.put("库存",itemData.getStock_cnt());
            resultJson.put("原价金额",0);
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray itemStock(String orgId, String begin, String end, String branchNos,String itemNo,Integer pageSize,Integer pageNo) throws BaseException {

        String branchSql = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(!"".equals(branchSql)){
                    branchSql = branchSql+","+"'"+branch_no+"'";
                }else{
                    branchSql = "'"+branch_no+"'";
                }
            }
        }
        branchSql = StringUtils.isNotEmpty(branchSql)?" and t1.branch_no in("+branchSql+")":"";
        class ItemData{
            String item_name;
            double sale_cnt;
            double stock_cnt;

            public String getItem_name() {
                return item_name;
            }

            public void setItem_name(String item_name) {
                this.item_name = item_name;
            }

            public double getSale_cnt() {
                return sale_cnt;
            }

            public void setSale_cnt(double sale_cnt) {
                this.sale_cnt = sale_cnt;
            }

            public double getStock_cnt() {
                return stock_cnt;
            }

            public void setStock_cnt(double stock_cnt) {
                this.stock_cnt = stock_cnt;
            }
        }
        Map<String ,ItemData> dataMap = new HashMap<>();
        List<Object> objectThis = unionSqlDao.query("select t1.item_no,sum(t1.sale_amt) as sale_amt,oper_date,sum(t1.sale_qty) as sale_qty,sum(t1.ret_amt) as ret_amt,sum(t1.dis_amt) as dis_amt,sum(t1.cost_amt) as cost_amt,sum(t1.ret_qty) as ret_qty from ItemDayEntity t1 where t1.org_id = '"+orgId+"'"+branchSql
                +" and t1.oper_date>='"+begin+"' and t1.oper_date<='"+end+"' and t1.item_no='"+itemNo+"' group by t1.item_no",pageNo,pageSize);
        for(Object itemEntity:objectThis){
            Object[]itemEntityObj = (Object[])itemEntity;
            String item_no = itemEntityObj[0].toString();
            Double total_qty = (Double)itemEntityObj[3];
            ItemData dataDay = dataMap.get(item_no);
            if(dataDay!=null){
                dataDay.setSale_cnt(dataDay.getSale_cnt()+(total_qty!=null?total_qty:0));
            }else{
                dataDay = new ItemData();
                dataDay.setSale_cnt(dataDay.getSale_cnt()+(total_qty!=null?total_qty:0));
                dataMap.put(item_no,dataDay);
            }
        }

        String item_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,branch_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(item_nosStr)){
                item_nosStr = item_nosStr + ",'"+str+"'";
            }else{
                item_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(item_nosStr)){
            //2商品名
            List<ItemEntity> itemEntities = itemDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'");
            for(ItemEntity itemEntity : itemEntities){
                if( dataMap.get(itemEntity.getItem_no())!=null){
                    dataMap.get(itemEntity.getItem_no()).setItem_name(itemEntity.getItem_name());
                }
            }
            //3库存
            List<StockEntity> stockEntities = stockDao.list("where item_no in("+item_nosStr+") and org_id = '"+orgId+"'"+branchSql);
            for(StockEntity stockEntity : stockEntities){
                if(dataMap.get(stockEntity.getItem_no())!=null){
                    dataMap.get(stockEntity.getItem_no()).setStock_cnt(stockEntity.getStock_qty());
                }
            }
        }

        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店",itemData.getItem_name());
            resultJson.put("月销量",itemData.getSale_cnt());
            resultJson.put("库存",itemData.getStock_cnt());
            results.add(resultJson);
        }
        return results;
    }

}
