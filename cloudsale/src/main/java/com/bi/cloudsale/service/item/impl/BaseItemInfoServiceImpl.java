package com.bi.cloudsale.service.item.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.dto.item.BaseItemInfoDto;
import com.bi.cloudsale.persistent.dao.item.BaseItemInfoDao;
import com.bi.cloudsale.persistent.entity.item.BaseItemInfoEntity;
import com.bi.cloudsale.service.item.BaseItemInfoService;
import com.bi.cloudsale.service.overall.impl.OperationServiceImpl;

@Service
@Transactional
public class BaseItemInfoServiceImpl implements BaseItemInfoService {
	
	private static final Logger log = LoggerFactory.getLogger(BaseItemInfoServiceImpl.class);
	
	@Resource
	private BaseItemInfoDao baseItemInfoDao;

	@Override
	public List<BaseItemInfoDto> queryByCondition(String orgId, String clsNo, String key, Integer pageIndex, Integer pageSize) {
		List<BaseItemInfoDto> dtoList = new ArrayList<BaseItemInfoDto>();
		List<BaseItemInfoEntity> entityList = baseItemInfoDao.queryByCondition(orgId, clsNo, key, pageIndex, pageSize);
		try {
			BeanUtil.copyBeanPropertiesList(dtoList, entityList, BaseItemInfoDto.class);
		} catch (BaseException e) {
			log.error(e.getMessage(), e);
		}
		return dtoList;
	}

}
