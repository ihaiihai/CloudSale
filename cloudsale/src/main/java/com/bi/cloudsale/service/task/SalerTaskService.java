package com.bi.cloudsale.service.task;

import java.util.List;

import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.dto.task.FollowUpTaskDto;
import com.bi.cloudsale.dto.task.SetUpTaskDto;

public interface SalerTaskService {
	
	List<FollowUpTaskDto> queryFollowUp(TaskQueryCondition condition);

	List<SetUpTaskDto> querySetUp(TaskQueryCondition condition);
}
