package com.bi.cloudsale.service.task.impl;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.utils.ArithUtil;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.dto.task.SetUpTaskDto;
import com.bi.cloudsale.persistent.dao.branch.SaleBranchDayDao;
import com.bi.cloudsale.persistent.dao.task.TaskDao;
import com.bi.cloudsale.persistent.entity.branch.SaleBranchDayEntity;
import com.bi.cloudsale.persistent.entity.task.SaleAimEntity;
import com.bi.cloudsale.service.task.OverallTaskService;

@Service
@Transactional
public class OverallTaskServiceImpl implements OverallTaskService {
	
	@Resource
	private TaskDao taskDao;
	@Resource
	private SaleBranchDayDao saleBranchDayDao;

	@Override
	public List<SetUpTaskDto> querySetUp(TaskQueryCondition condition) {
		List<SetUpTaskDto> dtoList = new ArrayList<SetUpTaskDto>();
		List<SaleAimEntity> entityList = taskDao.queryByCondition(condition);
		if(entityList != null && entityList.size() > 0) {
			Map<String,SetUpTaskDto> map = new HashMap<String,SetUpTaskDto>();
			Set<String> branchNo = new HashSet<String>();
			for(SaleAimEntity entity : entityList) {
				SetUpTaskDto dto = null;
				if(map.get(entity.getBranch_no()) == null) {
					dto = new SetUpTaskDto();
					dto.set门店编号(entity.getBranch_no());
					dto.set门店名称(entity.getBranch_no());
					if(!branchNo.contains(entity.getBranch_no())) {
						branchNo.add(entity.getBranch_no());
					}
					dtoList.add(dto);
					map.put(entity.getBranch_no(), dto);
				}else {
					dto = map.get(entity.getBranch_no());
				}
				if(entity.getAim_cycle() == 0) {
					dto.set销售任务(entity.getAim_value());
				}
				if(entity.getAim_cycle() == 1) {
					setMonthAim(dto, entity.getCycle_month(), entity.getAim_value());
				}
			}
			//去年门店总共销售数据
			double total = saleBranchDayDao.querySum(condition.getUserId(), condition.getOrgId(), branchNo, DateUtil.getYearStart(condition.getYear() - 1), DateUtil.getYearEnd(condition.getYear() - 1));
			//去年按门店统计销售数据
			List<SaleBranchDayEntity> saleSumList = saleBranchDayDao.querySumByBranch(condition.getUserId(), condition.getOrgId(), branchNo, DateUtil.getYearStart(condition.getYear() - 1), DateUtil.getYearEnd(condition.getYear() - 1));
			if(saleSumList != null && saleSumList.size() > 0) {
				for(SaleBranchDayEntity entity : saleSumList) {
					SetUpTaskDto dto = map.get(entity.getBranch_no());
					if(dto != null) {
						dto.set前一年销售(entity.getSale_amt());
						dto.set业绩提升(ArithUtil.getIncrease(entity.getSale_amt(), dto.get销售任务()));
						dto.set前一年占比(ArithUtil.getPercent(entity.getSale_amt(), total));
					}
				}
			}
		}
		return dtoList;
	}
	
	private void setMonthAim(SetUpTaskDto dto, Integer month, Double aimValue) {
        try {
        	Method method = dto.getClass().getMethod("set月度目标_" + String.format("%02d", month), Double.class);
			method.invoke(dto, aimValue);
		} catch (Exception e) {
			
		}
	}

}
