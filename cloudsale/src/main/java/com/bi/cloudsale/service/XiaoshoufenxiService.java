package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;

import java.util.List;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/26 14:57
 * @Version: 1.0
 **/
public interface XiaoshoufenxiService {

    JSONArray branches(String orgId) throws BaseException;
    JSONArray byItem(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException;
    JSONArray byBranch(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException;
    JSONArray byMonth(String orgId, String yyyy_MM, String branchNos,Integer pageSize,Integer pageNo) throws BaseException;
    JSONArray byDay(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException;
    JSONArray byCls(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException;
    JSONArray byBrand(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException;
    JSONArray bySaler(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException;
    JSONArray itemDetail(String orgId, String begin, String end, String branchNos,Integer pageSize,Integer pageNo) throws BaseException;
    JSONArray itemStock(String orgId, String begin, String end, String branchNos,String itemNo,Integer pageSize,Integer pageNo) throws BaseException;
}
