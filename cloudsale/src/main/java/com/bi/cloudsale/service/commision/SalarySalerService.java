package com.bi.cloudsale.service.commision;

import java.util.Date;
import java.util.List;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.dto.commision.SalarySalerDto;

public interface SalarySalerService {

	List<SalarySalerDto> queryByCondition(String orgId, Date startDate, Date endDate, String branchNos, String salerIds);
	
	void updateConfirm(String orgId, String account, Date startDate, Date endDate, String branchNos, String salerIds) throws BaseException;
}
