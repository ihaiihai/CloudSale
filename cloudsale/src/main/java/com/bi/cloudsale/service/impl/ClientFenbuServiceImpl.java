package com.bi.cloudsale.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.NumberUtil;
import com.bi.cloudsale.persistent.dao.*;
import com.bi.cloudsale.persistent.dao.sys.UserInfoDao;
import com.bi.cloudsale.persistent.entity.*;
import com.bi.cloudsale.persistent.entity.sys.UserInfoEntity;
import com.bi.cloudsale.service.ClientFenbuService;
import com.bi.cloudsale.service.SalerService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
@Transactional
public class ClientFenbuServiceImpl implements ClientFenbuService {

    private static final Logger log = LoggerFactory.getLogger(ClientFenbuServiceImpl.class);

    @Resource
    private BranchDao branchDao;
    @Resource
    private ItemClsDao itemClsDao;
    @Resource
    private BranchDayDao branchDayDao;
    @Resource
    private ItemDao itemDao;
    @Resource
    private ItemDayDao itemDayDao;
    @Resource
    private AimDao aimDao;
    @Resource
    private SalerDao salerDao;
    @Resource
    private StockDao stockDao;
    @Resource
    private BranchTimeDao branchTimeDao;
    @Resource
    private ClsDayDao clsDayDao;
    @Resource
    private BrandDayDao brandDayDao;
    @Resource
    private VipDao vipDao;
    @Resource
    private VipDayDao vipDayDao;
    @Resource
    private UserInfoDao userInfoDao;
    @Resource
    private SalerDayDao salerDayDao;
    @Resource
    private BrandDao brandDao;
    @Resource
    private UnionSqlDao unionSqlDao;

    @Override
    public JSONArray branches(String orgId) throws BaseException {


        List<BranchEntity> branchEntities = branchDao.list("where org_id = '"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BranchEntity branchEntity:branchEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("branch_no",branchEntity.getBranch_no());
            jsonObject.put("branch_name",branchEntity.getBranch_name());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray clses(String orgId) throws BaseException {


        List<ItemClsEntity> itemClsEntities = itemClsDao.list("where org_id = '"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(ItemClsEntity itemClsEntity:itemClsEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("item_clsno",itemClsEntity.getItem_clsno());
            jsonObject.put("item_clsname",itemClsEntity.getItem_clsname());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray brands(String orgId) throws BaseException {


        List<BrandEntity> brandEntities = brandDao.list("where org_id = '"+orgId+"'");
        JSONArray resuls = new JSONArray();
        for(BrandEntity brandEntity:brandEntities){
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("item_brandno",brandEntity.getItem_brandno());
            jsonObject.put("item_brandname",brandEntity.getItem_brandname());
            resuls.add(jsonObject);
        }
        return resuls;
    }

    @Override
    public JSONArray queryByAmt(String orgId, String begin, String end, String branchNos,String clses,String brands) throws BaseException {
        class ItemData{
            String branch_name;
            double total_bill_cnt;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public double getTotal_bill_cnt() {
                return total_bill_cnt;
            }

            public void setTotal_bill_cnt(double total_bill_cnt) {
                this.total_bill_cnt = total_bill_cnt;
            }
        }
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        String clses_str = "";
        if(StringUtils.isNotEmpty(clses)){
            String[] cls_nos = clses.split(",");
            for(String cls_no:cls_nos){
                if(StringUtils.isNotEmpty(clses_str)){
                    clses_str = clses_str + ",'"+cls_no+"'";
                }else{
                    clses_str =  "'"+cls_no+"'";
                }
            }
        }
        String brands_str = "";
        if(StringUtils.isNotEmpty(brands)){
            String[] brand_nos = brands.split(",");
            for(String brand_no:brand_nos){
                if(StringUtils.isNotEmpty(brands_str)){
                    brands_str = brands_str + ",'"+brand_no+"'";
                }else{
                    brands_str =  "'"+brand_no+"'";
                }
            }
        }

        Map<String,ItemData> dataMap = new HashMap<>();
        if(StringUtils.isNotEmpty(clses_str)){
            String cls_noSql = " and item_clsno in ("+clses_str+")";
//            List<ClsDayEntity>clsDayEntities= clsDayDao.list("where org_id = '"+orgId+"' " +
//                    "and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+cls_noSql);
            List<Object> objects = unionSqlDao.query("select branch_no, sum(bill_cnt) from ClsDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+cls_noSql +" group by branch_no");
            for(Object object : objects){
                Object[]itemEntityObj = (Object[])object;
                String branchNo = ((String)itemEntityObj[0]);
                Double bill_cnt = (Double)itemEntityObj[1];
                ItemData dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                }else{
                    dataDay = new ItemData();
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                    dataMap.put(branchNo,dataDay);
                }
            }
        }else if(StringUtils.isNotEmpty(brands_str)){
            String brand_noSql = " and item_brandno in ("+brands_str+")";
//            List<BrandDayEntity>brandDayEntities= brandDayDao.list("where org_id = '"+orgId+"' " +
//                    "and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+brand_noSql);
            List<Object> objects = unionSqlDao.query("select branch_no, sum(bill_cnt) from BrandDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+brand_noSql +" group by branch_no");
            for(Object object : objects){
                Object[]itemEntityObj = (Object[])object;
                String branchNo = ((String)itemEntityObj[0]);
                Double bill_cnt = (Double)itemEntityObj[1];
                ItemData dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                }else{
                    dataDay = new ItemData();
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                    dataMap.put(branchNo,dataDay);
                }
            }
        }else{
            String branchSql = StringUtils.isEmpty(branch_nos_str)?"":" and branch_no in ("+branch_nos_str+")";
//            List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                    "and oper_date>='"+begin+"' and oper_date <= '"+end+"'"+branchSql);
            List<Object> objects = unionSqlDao.query("select branch_no, sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+branchSql +" group by branch_no");
            for(Object object : objects){
                Object[]itemEntityObj = (Object[])object;
                String branchNo = ((String)itemEntityObj[0]);
                Long bill_cnt = (Long)itemEntityObj[1];
                ItemData dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                }else{
                    dataDay = new ItemData();
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                    dataMap.put(branchNo,dataDay);
                }
            }
        }

        String branch_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                dataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject(true);
            resultJson.put("门店编号",key);
            resultJson.put("门店名称",itemData.getBranch_name());
            resultJson.put("订单总数",itemData.getTotal_bill_cnt());

            resultJson.put("五十以下_数量",0);
            resultJson.put("五十以下_占比",0);
            resultJson.put("五十到一百_数量",0);
            resultJson.put("五十到一百_占比",0);
            resultJson.put("一百到两百_数量",0);

            resultJson.put("一百到两百_占比",0);
            resultJson.put("二百到三百_数量",0);
            resultJson.put("二百到三百_占比",0);
            resultJson.put("三百到四百_数量",0);
            resultJson.put("三百到四百_占比",0);

            resultJson.put("四百到五百_数量",0);
            resultJson.put("四百到五百_占比",0);
            resultJson.put("五百到一千_数量",0);
            resultJson.put("五百到一千_占比",0);
            resultJson.put("一千以上_数量",0);
            resultJson.put("一千以上_占比",0);
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray queryByCnt(String orgId, String begin, String end, String branchNos,String clses,String brands) throws BaseException {
        class ItemData{
            String branch_name;
            double total_bill_cnt;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public double getTotal_bill_cnt() {
                return total_bill_cnt;
            }

            public void setTotal_bill_cnt(double total_bill_cnt) {
                this.total_bill_cnt = total_bill_cnt;
            }
        }
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        String clses_str = "";
        if(StringUtils.isNotEmpty(clses)){
            String[] cls_nos = clses.split(",");
            for(String cls_no:cls_nos){
                if(StringUtils.isNotEmpty(clses_str)){
                    clses_str = clses_str + ",'"+cls_no+"'";
                }else{
                    clses_str =  "'"+cls_no+"'";
                }
            }
        }
        String brands_str = "";
        if(StringUtils.isNotEmpty(brands)){
            String[] brand_nos = brands.split(",");
            for(String brand_no:brand_nos){
                if(StringUtils.isNotEmpty(brands_str)){
                    brands_str = brands_str + ",'"+brand_no+"'";
                }else{
                    brands_str =  "'"+brand_no+"'";
                }
            }
        }

        Map<String,ItemData> dataMap = new HashMap<>();
        if(StringUtils.isNotEmpty(clses_str)){
            String cls_noSql = " and item_clsno in ("+clses_str+")";
//            List<ClsDayEntity>clsDayEntities= clsDayDao.list("where org_id = '"+orgId+"' " +
//                    "and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+cls_noSql);
            List<Object> objects = unionSqlDao.query("select branch_no, sum(bill_cnt) from ClsDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+cls_noSql +" group by branch_no");
            for(Object object : objects){
                Object[]itemEntityObj = (Object[])object;
                String branchNo = ((String)itemEntityObj[0]);
                Double bill_cnt = (Double)itemEntityObj[1];
                ItemData dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                }else{
                    dataDay = new ItemData();
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                    dataMap.put(branchNo,dataDay);
                }
            }
        }else if(StringUtils.isNotEmpty(brands_str)){
            String brand_noSql = " and item_brandno in ("+brands_str+")";
//            List<BrandDayEntity>brandDayEntities= brandDayDao.list("where org_id = '"+orgId+"' " +
//                    "and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+brand_noSql);
            List<Object> objects = unionSqlDao.query("select branch_no, sum(bill_cnt) from BrandDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+brand_noSql +" group by branch_no");
            for(Object object : objects){
                Object[]itemEntityObj = (Object[])object;
                String branchNo = ((String)itemEntityObj[0]);
                Double bill_cnt = (Double)itemEntityObj[1];
                ItemData dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                }else{
                    dataDay = new ItemData();
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                    dataMap.put(branchNo,dataDay);
                }
            }
        }else{
            String branchSql = StringUtils.isEmpty(branch_nos_str)?"":" and branch_no in ("+branch_nos_str+")";
//            List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                    "and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+branchSql);
            List<Object> objects = unionSqlDao.query("select branch_no, sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+branchSql +" group by branch_no");
            for(Object object : objects){
                Object[]itemEntityObj = (Object[])object;
                String branchNo = ((String)itemEntityObj[0]);
                Long bill_cnt = (Long)itemEntityObj[1];
                ItemData dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                }else{
                    dataDay = new ItemData();
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                    dataMap.put(branchNo,dataDay);
                }
            }
        }
        String branch_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                dataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店编号",key);
            resultJson.put("门店名称",itemData.getBranch_name());
            resultJson.put("订单总数",itemData.getTotal_bill_cnt());

            resultJson.put("数量1个_数量",0);
            resultJson.put("数量1个_占比",0);
            resultJson.put("数量2个_数量",0);
            resultJson.put("数量2个_占比",0);
            resultJson.put("数量3个_数量",0);
            resultJson.put("数量3个_占比",0);
            resultJson.put("数量4个_数量",0);
            resultJson.put("数量4个_占比",0);
            resultJson.put("数量5个_数量",0);
            resultJson.put("数量5个_占比",0);
            resultJson.put("数量5个以上_数量",0);
            resultJson.put("数量5个以上_占比",0);
            results.add(resultJson);
        }
        return results;
    }

    @Override
    public JSONArray queryBySku(String orgId, String begin, String end, String branchNos,String clses,String brands) throws BaseException {
        class ItemData{
            String branch_name;
            double total_bill_cnt;

            public String getBranch_name() {
                return branch_name;
            }

            public void setBranch_name(String branch_name) {
                this.branch_name = branch_name;
            }

            public double getTotal_bill_cnt() {
                return total_bill_cnt;
            }

            public void setTotal_bill_cnt(double total_bill_cnt) {
                this.total_bill_cnt = total_bill_cnt;
            }
        }
        String branch_nos_str = "";
        if(StringUtils.isNotEmpty(branchNos)){
            String[] branch_nos = branchNos.split(",");
            for(String branch_no:branch_nos){
                if(StringUtils.isNotEmpty(branch_nos_str)){
                    branch_nos_str = branch_nos_str + ",'"+branch_no+"'";
                }else{
                    branch_nos_str =  "'"+branch_no+"'";
                }
            }
        }
        String clses_str = "";
        if(StringUtils.isNotEmpty(clses)){
            String[] cls_nos = clses.split(",");
            for(String cls_no:cls_nos){
                if(StringUtils.isNotEmpty(clses_str)){
                    clses_str = clses_str + ",'"+cls_no+"'";
                }else{
                    clses_str =  "'"+cls_no+"'";
                }
            }
        }
        String brands_str = "";
        if(StringUtils.isNotEmpty(brands)){
            String[] brand_nos = brands.split(",");
            for(String brand_no:brand_nos){
                if(StringUtils.isNotEmpty(brands_str)){
                    brands_str = brands_str + ",'"+brand_no+"'";
                }else{
                    brands_str =  "'"+brand_no+"'";
                }
            }
        }

        Map<String,ItemData> dataMap = new HashMap<>();
        if(StringUtils.isNotEmpty(clses_str)){
            String cls_noSql = " and item_clsno in ("+clses_str+")";
//            List<ClsDayEntity>clsDayEntities= clsDayDao.list("where org_id = '"+orgId+"' " +
//                    "and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+cls_noSql);
            List<Object> objects = unionSqlDao.query("select branch_no, sum(bill_cnt) from ClsDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+cls_noSql +" group by branch_no");
            for(Object object : objects){
                Object[]itemEntityObj = (Object[])object;
                String branchNo = ((String)itemEntityObj[0]);
                Double bill_cnt = (Double)itemEntityObj[1];
                ItemData dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                }else{
                    dataDay = new ItemData();
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                    dataMap.put(branchNo,dataDay);
                }
            }
        }else if(StringUtils.isNotEmpty(brands_str)){
            String brand_noSql = " and item_brandno in ("+brands_str+")";
//            List<BrandDayEntity>brandDayEntities= brandDayDao.list("where org_id = '"+orgId+"' " +
//                    "and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+brand_noSql);
            List<Object> objects = unionSqlDao.query("select branch_no, sum(bill_cnt) from BrandDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+brand_noSql +" group by branch_no");
            for(Object object : objects){
                Object[]itemEntityObj = (Object[])object;
                String branchNo = ((String)itemEntityObj[0]);
                Double bill_cnt = (Double)itemEntityObj[1];
                ItemData dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                }else{
                    dataDay = new ItemData();
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                    dataMap.put(branchNo,dataDay);
                }
            }
        }else{
            String branchSql = StringUtils.isEmpty(branch_nos_str)?"":" and branch_no in ("+branch_nos_str+")";
//            List<BranchDayEntity>branchDayEntities= branchDayDao.list("where org_id = '"+orgId+"' " +
//                    "and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+branchSql);
            List<Object> objects = unionSqlDao.query("select branch_no, sum(bill_cnt) from BranchDayEntity where org_id ='"+orgId+"' and oper_date>='"+begin+"' and oper_date<= '"+end+"'"+branchSql +" group by branch_no");
            for(Object object : objects){
                Object[]itemEntityObj = (Object[])object;
                String branchNo = ((String)itemEntityObj[0]);
                Long bill_cnt = (Long)itemEntityObj[1];
                ItemData dataDay = dataMap.get(branchNo);
                if(dataDay!=null){
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                }else{
                    dataDay = new ItemData();
                    dataDay.setTotal_bill_cnt(dataDay.getTotal_bill_cnt() +  (bill_cnt!=null?bill_cnt:0));
                    dataMap.put(branchNo,dataDay);
                }
            }
        }
        String branch_nosStr = "";
        Set<String> keys = dataMap.keySet() ;// 得到全部的key,cls_no
        Iterator<String> iter = keys.iterator() ;
        while(iter.hasNext()){
            String str = iter.next() ;
            if(StringUtils.isNotEmpty(branch_nosStr)){
                branch_nosStr = branch_nosStr + ",'"+str+"'";
            }else{
                branch_nosStr =  "'"+str+"'";
            }
        }
        if(StringUtils.isNotEmpty(branch_nosStr)){
            List<BranchEntity> branchEntities = branchDao.list("where branch_no in("+branch_nosStr+") and org_id = '"+orgId+"'");
            for(BranchEntity branchEntity : branchEntities){
                dataMap.get(branchEntity.getBranch_no()).setBranch_name(branchEntity.getBranch_name());
            }
        }
        JSONArray results = new JSONArray();
        for (Map.Entry<String, ItemData> entry : dataMap.entrySet()) {
            String key = entry.getKey();
            ItemData itemData =  entry.getValue();
            JSONObject resultJson = new JSONObject();
            resultJson.put("门店编号",key);
            resultJson.put("门店名称",itemData.getBranch_name());
            resultJson.put("订单总数",itemData.getTotal_bill_cnt());

            resultJson.put("交叉1_数量",0);
            resultJson.put("交叉1_占比",0);
            resultJson.put("交叉2_数量",0);
            resultJson.put("交叉2_占比",0);
            resultJson.put("交叉3_数量",0);
            resultJson.put("交叉3_占比",0);
            resultJson.put("交叉4_数量",0);
            resultJson.put("交叉4_占比",0);
            resultJson.put("交叉5_数量",0);
            resultJson.put("交叉5_占比",0);
            resultJson.put("交叉5个以上_数量",0);
            resultJson.put("交叉5个以上_占比",0);
            results.add(resultJson);
        }
        return results;
    }

}
