package com.bi.cloudsale.service.sys.impl;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.AreaInfoDto;
import com.bi.cloudsale.persistent.dao.BranchClsDao;
import com.bi.cloudsale.persistent.dao.BranchDao;
import com.bi.cloudsale.persistent.dao.SalerDao;
import com.bi.cloudsale.persistent.dao.sys.AreaInfoDao;
import com.bi.cloudsale.persistent.entity.BranchClsEntity;
import com.bi.cloudsale.persistent.entity.BranchEntity;
import com.bi.cloudsale.persistent.entity.SalerEntity;
import com.bi.cloudsale.persistent.entity.sys.AreaInfoEntity;
import com.bi.cloudsale.service.sys.AreaInfoService;
import com.bi.cloudsale.service.sys.SalerInfoService;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;

@Service
@Transactional
public class SalerInfoServiceImpl implements SalerInfoService {

	private static final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);

	@Resource
	private SalerDao salerDao;
	@Resource
	private BranchClsDao branchClsDao;
	@Resource
	private BranchDao branchDao;

	@Override
	public List<OptionDto> queryAllOptions(String orgId) {
		List<OptionDto> list = new ArrayList<OptionDto>();

		BaseQueryCondition baseQueryCondition = new BaseQueryCondition();
		baseQueryCondition.setOrgId(orgId);

		StringBuilder hql = new StringBuilder();
		hql.append("where org_id = '" + baseQueryCondition.getOrgId() + "'");
		hql.append(" order by branch_clsno");
		List<BranchClsEntity> branchClsEntities = branchClsDao.list(hql.toString());
		hql = new StringBuilder();
		hql.append("where org_id = '" + baseQueryCondition.getOrgId() + "'");
		if(StringUtils.isNotBlank(baseQueryCondition.getKey())) {
			hql.append(" and branch_name like '%" + baseQueryCondition.getKey() + "%'");
		}
		hql.append(" order by branch_no");
		List<BranchEntity> branchEntities = branchDao.list(hql.toString());
		hql = new StringBuilder();
		hql.append("where org_id = '" + baseQueryCondition.getOrgId() + "'");
		List<SalerEntity> salerEntities = salerDao.list(hql.toString());

		Map<String,OptionDto> map = new HashMap<String,OptionDto>();
		for(BranchClsEntity branchClsEntity : branchClsEntities) {
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(branchClsEntity.getBranch_clsno().trim());
			optionDto.setLabel(branchClsEntity.getBranch_clsname().trim());
			if(StringUtils.isBlank(branchClsEntity.getParent_clsno().trim())) {
				list.add(optionDto);
			}else {
				OptionDto parentOptionDto = map.get(branchClsEntity.getParent_clsno().trim());
				if(parentOptionDto.getChildren() == null) {
					parentOptionDto.setChildren(new ArrayList<OptionDto>());
				}
				parentOptionDto.getChildren().add(optionDto);
			}
			map.put(branchClsEntity.getBranch_clsno().trim(), optionDto);
		}
		for(BranchEntity branchEntity : branchEntities) {
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(branchEntity.getBranch_no().trim());
			optionDto.setLabel(branchEntity.getBranch_name().trim());
			if(StringUtils.isBlank(branchEntity.getBranch_clsno().trim())) {
				list.add(optionDto);
			}else {
				OptionDto parentOptionDto = map.get(branchEntity.getBranch_clsno().trim());
				if(parentOptionDto != null) {
					if(parentOptionDto.getChildren() == null) {
						parentOptionDto.setChildren(new ArrayList<OptionDto>());
					}
					parentOptionDto.getChildren().add(optionDto);
				}
			}
			map.put(branchEntity.getBranch_no().trim(), optionDto);
		}
		for(SalerEntity salerEntity : salerEntities){
			OptionDto optionDto =  new OptionDto();
			optionDto.setId(salerEntity.getSale_id().trim());
			optionDto.setLabel(salerEntity.getSale_name());

			OptionDto parentOptionDto = map.get(salerEntity.getBranch_no().trim());
			if(parentOptionDto != null) {
				if(parentOptionDto.getChildren() == null) {
					parentOptionDto.setChildren(new ArrayList<OptionDto>());
				}
				parentOptionDto.getChildren().add(optionDto);
			}
		}
		return list;
	}
}
