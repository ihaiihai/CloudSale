package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/22 21:51
 * @Version: 1.0
 **/
public interface CompanyDayMonthService {
    JSONArray clsFenbu(String orgId) throws BaseException;
    JSONArray clsFenbuDetail(String orgId,String itemClsName) throws BaseException;
    JSONArray smallTitle(String orgId) throws BaseException;
    JSONArray shijianduan(String orgId) throws BaseException;
    JSONArray retDisDanjiaLiandai(String orgId) throws BaseException;
    JSONArray periodSale(String orgId) throws BaseException;
    JSONArray yesMonthWancheng(String orgId) throws BaseException;
    JSONArray monthAimWancheng(String orgId) throws BaseException;
    JSONArray title(String orgId) throws BaseException;
    JSONArray salerSale(String orgId) throws BaseException;
    JSONArray branchSaleTongqi(String orgId) throws BaseException;

    JSONArray branchSaleRet(String orgId) throws BaseException;
    JSONArray branchSaleDis(String orgId) throws BaseException;
    JSONArray  branchSaleClient(String orgId) throws BaseException;
    JSONArray  branchSaleLiandailv(String orgId) throws BaseException;

    JSONArray  branchMonthTitle(String orgId,String begin,String end) throws BaseException;
    JSONArray  branchMonthWanchenglv(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray  branchMonthVip(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray  branchMonthYunying(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray  branchMonthClsSaleFenbu(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray  branchMonthAimWanchengSaleClient(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray  branchMonthClient(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray  branchMonthSaler(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray  branchMonthSaleFenbuDuibi(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray  branchMonthBranchSaleFenbu(String orgId,String begin,String end,String branchNo) throws BaseException;

    JSONArray branchMonthBranchAimWancheng(String orgId,String begin,String end) throws BaseException;
    JSONArray branchMonthBranchCls(String orgId,String begin,String end,String branchNo) throws BaseException;
}
