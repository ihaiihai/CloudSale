package com.bi.cloudsale.service.sys.impl;

import javax.annotation.Resource;

import org.hibernate.HibernateException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.BeanUtil;
import com.bi.cloudsale.dto.sys.RoleAuthDto;
import com.bi.cloudsale.persistent.dao.sys.RoleAuthDao;
import com.bi.cloudsale.persistent.entity.sys.RoleAuthEntity;
import com.bi.cloudsale.service.sys.RoleAuthService;

@Service
@Transactional
public class RoleAuthServiceImpl implements RoleAuthService {
	
	private static final Logger log = LoggerFactory.getLogger(UserInfoServiceImpl.class);
	
	@Resource
	private RoleAuthDao roleAuthDao;

	@Override
	public RoleAuthDto queryByRoleCode(String roleCode, Integer itemtype) {
		RoleAuthDto dto = new RoleAuthDto();
		RoleAuthEntity entity = roleAuthDao.queryByRoleCode(roleCode, itemtype);
		BeanUtil.copyProperties(entity, dto);
		return dto;
	}

	@Override
	public void createRoleAuth(RoleAuthDto roleAuthDto) throws BaseException {
		RoleAuthEntity entity = new RoleAuthEntity();
		BeanUtil.copyProperties(roleAuthDto, entity);
		try {
			roleAuthDao.createRoleAuth(entity);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public void updateRoleAuth(RoleAuthDto roleAuthDto) throws BaseException {
		try {
			RoleAuthEntity entity = roleAuthDao.queryByRoleCode(roleAuthDto.getRoleCode(), roleAuthDto.getItemType());
			entity.setItemId(roleAuthDto.getItemId());
			roleAuthDao.updateRoleAuth(entity);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

	@Override
	public void deleteRoleAuth(String roleCode, Integer itemType) throws BaseException {
		try {
			roleAuthDao.deleteRoleAuth(roleCode, itemType);
		}catch(HibernateException e) {
			log.error(e.getMessage(), e);
			throw new BaseException("数据库操作失败", e);
		}
	}

}
