package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/22 21:51
 * @Version: 1.0
 **/
public interface ClientFenbuService {
    JSONArray branches(String orgId) throws BaseException;
    JSONArray clses(String orgId) throws BaseException;
    JSONArray brands(String orgId) throws BaseException;
    JSONArray queryByAmt(String orgId, String begin, String end, String branchNos,String clses,String brands) throws BaseException;
    JSONArray queryByCnt(String orgId, String begin, String end, String branchNos,String clses,String brands) throws BaseException;
    JSONArray queryBySku(String orgId, String begin, String end, String branchNos,String clses,String brands) throws BaseException;
}
