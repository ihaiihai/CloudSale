package com.bi.cloudsale.service;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.service
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/22 10:42
 * @Version: 1.0
 **/
public interface DisErrorService {
    JSONArray branches(String orgId) throws BaseException;
    JSONArray branchDislv(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray branchClsDisFenbu(String orgId,String begin,String end,String branchNo) throws BaseException;
    JSONArray branchClsBranchDisFenbu(String orgId,String begin,String end,String branchNo,String clsNo) throws BaseException;
    JSONArray branchBestSaleItemDis(String orgId,String begin,String end,String branchNo) throws BaseException;

    JSONArray branchErrorlv(String orgId,String branchNo) throws BaseException;
    JSONArray branchErrorFenbu(String orgId,String branchNo) throws BaseException;
    JSONArray branchErrorDetail(String orgId,String branchNo,Integer pageSize,Integer pageNo,String clsNo) throws BaseException;
}
