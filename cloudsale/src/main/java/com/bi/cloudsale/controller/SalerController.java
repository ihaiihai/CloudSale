package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.CompanyDayMonthService;
import com.bi.cloudsale.service.SalerService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.Calendar;

@Controller
@RequestMapping("/Report_PC/Saler_Report")
public class SalerController {

	private static final Logger log = LoggerFactory.getLogger(SalerController.class);

	@Autowired
	private SalerService salerService;

	/**
	 * 入职、离职、有销售、、
	 * begin=2018-12-01&end=2019-01-01&branchNo=000013
	 */
	@RequestMapping(value = "/salerInfo", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody
	JSONObject salerInfo(HttpServletRequest request,
						 @QueryParam("begin") String begin,
						 @QueryParam("end") String end,
						 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONObject results = new JSONObject();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:salerInfo ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = salerService.salerInfo(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 销售排名
	 */
	@RequestMapping(value = "/salerRank", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray salerRank(HttpServletRequest request,
											 @QueryParam("begin") String begin,
											 @QueryParam("end") String end,
											 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:salerRank ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = salerService.salerRank(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 销售详情 点状图
	 */
	@RequestMapping(value = "/salerDetail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray salerDetail(HttpServletRequest request,
											 @QueryParam("begin") String begin,
											 @QueryParam("end") String end,
											 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:salerDetail ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = salerService.salerDetail(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 雷达图
	 */
	@RequestMapping(value = "/salerRadio", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray salerRadio(HttpServletRequest request,
											   @QueryParam("begin") String begin,
											   @QueryParam("end") String end,
											   @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:salerRadio ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = salerService.salerRadio(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 其他各项
	 */
	@RequestMapping(value = "/salerOther", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray salerOther(HttpServletRequest request,
											  @QueryParam("begin") String begin,
											  @QueryParam("end") String end,
											  @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:salerOther ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = salerService.salerOther(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
