package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.JinxiaocunService;
import com.bi.cloudsale.service.ShouqingService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;

@Controller
@RequestMapping("/Report_PC/Jinxiaocun")
public class JinxiaocunController {

	private static final Logger log = LoggerFactory.getLogger(JinxiaocunController.class);

	@Autowired
	private JinxiaocunService jinxiaocunService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jinxiaocunService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 品牌列表
	 */
	@RequestMapping(value = "/brand_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray brandList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("brand_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jinxiaocunService.brands(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 分类列表
	 */
	@RequestMapping(value = "/cls_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray clsList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("cls_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jinxiaocunService.clses(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 首页或点击第一栏（让branchNos、itemClses、brands某一项是点击那项）
	 */
	@RequestMapping(value = "/jinxiaocun_query", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray queryJinxiaocun(HttpServletRequest request,
												 @QueryParam("type") String type,
												 @QueryParam("begin") String begin,
												 @QueryParam("end") String end,
												 @QueryParam("branchNos") String branchNos,
												 @QueryParam("itemClses") String itemClses,
												 @QueryParam("brands") String brands) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("jinxiaocun_query:orgId ="+orgId+","+begin+","+end+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jinxiaocunService.query(orgId,type,begin,end,branchNos,itemClses,brands);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 点击第二栏 单品明细
	 */
	@RequestMapping(value = "/item_sale_stock", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray itemSaleStock(HttpServletRequest request,
												 @QueryParam("begin") String begin,
												 @QueryParam("end") String end,
												 @QueryParam("branchNos") String branchNos,
												 @QueryParam("itemClses") String itemClses,
												 @QueryParam("brands") String brands,
												 @QueryParam("pageSize") Integer pageSize,
												 @QueryParam("pageNo") Integer pageNo,
												 @QueryParam("onlyHasStock") boolean onlyHasStock) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("item_sale_stock:orgId ="+orgId+","+begin+","+end+","+brands);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jinxiaocunService.itemDetail(orgId,begin,end,branchNos,itemClses,brands,pageSize,pageNo,onlyHasStock);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 点击第三栏 单品明细Sku
	 */
	@RequestMapping(value = "/item_sale_stock_sku", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray itemSaleStockSku(HttpServletRequest request,
												 @QueryParam("begin") String begin,
												 @QueryParam("end") String end,
												 @QueryParam("branchNos") String branchNos,
												 @QueryParam("itemClses") String itemClses,
												 @QueryParam("brands") String brands,
												 @QueryParam("pageSize") Integer pageSize,
												 @QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("item_sale_stock_sku:orgId ="+orgId+","+begin+","+end+","+brands);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jinxiaocunService.itemDetailSku(orgId,begin,end,branchNos,itemClses,brands,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
