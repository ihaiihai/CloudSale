package com.bi.cloudsale.controller.commision;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.model.BasicResult;
import com.bi.cloudsale.common.model.ResponseConstant;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.dto.commision.SalarySalerDto;
import com.bi.cloudsale.service.commision.SalarySalerService;

@Controller("SalarySalerController")
@RequestMapping("/salary/saler")
public class SalarySalerController {
	
	@Resource
	private SalarySalerService salarySalerService;
	
	@RequestMapping(value = "/queryByCondition", method = RequestMethod.GET)
	@ResponseBody
	public List<SalarySalerDto> queryByCondition(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("branchNos") String branchNos, @QueryParam("salerIds") String salerIds, 
			@QueryParam("begin") String begin, @QueryParam("end") String end) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return salarySalerService.queryByCondition(orgId, DateUtil.getDateStart(begin), DateUtil.getDateEnd(end), branchNos, salerIds);
	}
	
	@RequestMapping(value = "/updateConfirm", method = RequestMethod.GET)
	@ResponseBody
	public BasicResult updateConfirm(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("branchNos") String branchNos, @QueryParam("salerIds") String salerIds, 
			@QueryParam("begin") String begin, @QueryParam("end") String end) throws IOException {
		BasicResult basicResult = new BasicResult();
		try {
			HttpSession session = request.getSession();
			String orgId = (String) session.getAttribute("orgId");
			String account = (String) session.getAttribute("account");
			salarySalerService.updateConfirm(orgId, account, DateUtil.getDateStart(begin), DateUtil.getDateEnd(end), branchNos, salerIds);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
}
