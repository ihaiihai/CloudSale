package com.bi.cloudsale.controller.sys;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.model.BasicResult;
import com.bi.cloudsale.common.model.ResponseConstant;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.RoleInfoDto;
import com.bi.cloudsale.service.sys.RoleInfoService;

@Controller("SysRoleController")
@RequestMapping("/sys/role")
public class RoleController {

	@Resource
	private RoleInfoService roleInfoService;
	
	@RequestMapping(value = "/queryOptions", method = RequestMethod.GET)
	@ResponseBody
	public List<OptionDto> queryRoleOptions(HttpServletRequest request) throws IOException {
		
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		List<OptionDto> list = roleInfoService.queryOptions(userId);
		return list;
	}
	
	@RequestMapping(value = "/queryByCondition", method = RequestMethod.GET)
	@ResponseBody
	public List<RoleInfoDto> areaQueryByCondition(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("key") String key) throws IOException {
		
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		BaseQueryCondition baseQueryCondition = new BaseQueryCondition();
		baseQueryCondition.setUserId(userId);
		baseQueryCondition.setKey(key);
		List<RoleInfoDto> list = roleInfoService.queryByCondition(baseQueryCondition);
		return list;
	}
	
	@RequestMapping(value = "/queryByCode", method = RequestMethod.GET)
	@ResponseBody
	public RoleInfoDto queryRoleByCode(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("roleCode") String roleCode) throws IOException {
		
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		RoleInfoDto dto = roleInfoService.queryByCode(roleCode);
		return dto;
	}
	
	@RequestMapping(value = "/createRole", method = RequestMethod.POST)
	@ResponseBody
	public BasicResult createRole(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody RoleInfoDto roleInfoDto) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		try {
			HttpSession session = request.getSession();
			String userId = (String) session.getAttribute("userId");
			String orgId = (String) session.getAttribute("orgId");
			roleInfoDto.setUserId(userId);
			roleInfoDto.setOrgId(orgId);
			roleInfoService.createRoleInfo(roleInfoDto);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
	
	@RequestMapping(value = "/deleteRole", method = RequestMethod.GET)
	@ResponseBody
	public BasicResult deleteRole(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("roleCode") String roleCode) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		try {
			roleInfoService.deleteRoleInfo(roleCode);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
	
	@RequestMapping(value = "/updateRole", method = RequestMethod.POST)
	@ResponseBody
	public BasicResult updateRole(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody RoleInfoDto roleInfoDto) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		try {
			roleInfoService.updateRoleInfo(roleInfoDto);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
}
