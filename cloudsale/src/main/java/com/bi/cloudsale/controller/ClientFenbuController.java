package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.ClientFenbuService;
import com.bi.cloudsale.service.SalerService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import java.io.IOException;

@Controller
@RequestMapping("/Report_PC/Client_Fenbu")
public class ClientFenbuController {

	private static final Logger log = LoggerFactory.getLogger(ClientFenbuController.class);

	@Autowired
	private ClientFenbuService clientFenbuService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branches", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branches(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branches ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = clientFenbuService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 分类列表
	 */
	@RequestMapping(value = "/clses", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray clses(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:clses ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = clientFenbuService.clses(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 品牌列表
	 */
	@RequestMapping(value = "/brands", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray brands(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:brands ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = clientFenbuService.brands(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}


	/**
	 * query
	 * begin=2018-01-01&end=2019-01-01&type=1
	 */
	@RequestMapping(value = "/query", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray query(HttpServletRequest request,
											  @QueryParam("type") Integer type,
											   @QueryParam("begin") String begin,
											   @QueryParam("end") String end,
											   @QueryParam("branchNos") String branchNos,
											  @QueryParam("clses") String clses,
											  @QueryParam("brands") String brands) throws IOException {
		//type:1按金额，2按数量，3按sku交叉
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:query ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				if(type==1){
					results = clientFenbuService.queryByAmt(orgId,begin,end,branchNos,clses,brands);
				}else if(type==2){
					results = clientFenbuService.queryByCnt(orgId,begin,end,branchNos,clses,brands);
				}else{
					results = clientFenbuService.queryBySku(orgId,begin,end,branchNos,clses,brands);
				}
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
