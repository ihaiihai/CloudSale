package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.XiaoshoufenxiService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;

@Controller
@RequestMapping("/Report_PC/Xiaoshou_Fenxi")
public class XiaoshoufenxiController {

	private static final Logger log = LoggerFactory.getLogger(XiaoshoufenxiController.class);

	@Autowired
	private XiaoshoufenxiService xiaoshoufenxiService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 按商品 门店列表 日期区间 分页
	 * begin=2017-12-01&end=2019-01-01&itemNo=2&pageSize=10&pageNo=1
	 */
	@RequestMapping(value = "/byItem", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray byItem(HttpServletRequest request,
														@QueryParam("begin") String begin,
															  @QueryParam("end") String end,
															  @QueryParam("branchNos") String branchNos,
															  @QueryParam("pageSize") Integer pageSize,
															  @QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("byItem:orgId ="+orgId+","+begin+","+end+","+branchNos+","+pageSize+","+pageNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.byItem(orgId,begin,end,branchNos,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 按门店
	 */
	@RequestMapping(value = "/byBranch", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray byBranch(HttpServletRequest request,
															  @QueryParam("begin") String begin,
															  @QueryParam("end") String end,
															  @QueryParam("branchNos") String branchNos,
															  @QueryParam("pageSize") Integer pageSize,
															  @QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("byBranch:orgId ="+orgId+","+begin+","+end+","+branchNos+","+pageSize+","+pageNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.byBranch(orgId,begin,end,branchNos,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 按月份
	 * yyyy_MM=2018-12&pageSize=10&pageNo=1
	 */
	@RequestMapping(value = "/byMonth", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray byMonth(HttpServletRequest request,
											@QueryParam("yyyy_MM") String yyyy_MM,
											@QueryParam("branchNos") String branchNos,
											@QueryParam("pageSize") Integer pageSize,
											@QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("byMonth:orgId ="+orgId+","+yyyy_MM+","+branchNos+","+pageSize+","+pageNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.byMonth(orgId,yyyy_MM,branchNos,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 按天
	 * begin=2018-12-01&pageSize=10&pageNo=1&end=2019-01-01
	 */
	@RequestMapping(value = "/byDay", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray byDay(HttpServletRequest request,
											@QueryParam("begin") String begin,
											@QueryParam("end") String end,
											@QueryParam("branchNos") String branchNos,
											@QueryParam("pageSize") Integer pageSize,
											@QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("byDay:orgId ="+orgId+","+begin+","+end+","+branchNos+","+pageSize+","+pageNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.byDay(orgId,begin,end,branchNos,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 按分类
	 * begin=2018-12-01&pageSize=10&pageNo=1&end=2019-01-01
	 */
	@RequestMapping(value = "/byCls", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray byCls(HttpServletRequest request,
										 @QueryParam("begin") String begin,
										 @QueryParam("end") String end,
										 @QueryParam("branchNos") String branchNos,
										 @QueryParam("pageSize") Integer pageSize,
										 @QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("byCls:orgId ="+orgId+","+begin+","+end+","+branchNos+","+pageSize+","+pageNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.byCls(orgId,begin,end,branchNos,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 按品牌
	 * begin=2018-12-01&pageSize=10&pageNo=1&end=2019-01-01
	 */
	@RequestMapping(value = "/byBrand", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray byBrand(HttpServletRequest request,
										 @QueryParam("begin") String begin,
										 @QueryParam("end") String end,
										 @QueryParam("branchNos") String branchNos,
										 @QueryParam("pageSize") Integer pageSize,
										 @QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("byBrand:orgId ="+orgId+","+begin+","+end+","+branchNos+","+pageSize+","+pageNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.byBrand(orgId,begin,end,branchNos,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 按销售员
	 * begin=2018-12-01&pageSize=10&pageNo=1&end=2019-01-01
	 */
	@RequestMapping(value = "/bySaler", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray bySaler(HttpServletRequest request,
										   @QueryParam("begin") String begin,
										   @QueryParam("end") String end,
										   @QueryParam("branchNos") String branchNos,
										   @QueryParam("pageSize") Integer pageSize,
										   @QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("bySaler:orgId ="+orgId+","+begin+","+end+","+branchNos+","+pageSize+","+pageNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.bySaler(orgId,begin,end,branchNos,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 单品销售汇总
	 * begin=2018-12-01&pageSize=10&pageNo=1&end=2019-01-01
	 */
	@RequestMapping(value = "/itemDetail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray itemDetail(HttpServletRequest request,
										   @QueryParam("begin") String begin,
										   @QueryParam("end") String end,
										   @QueryParam("branchNos") String branchNos,
										   @QueryParam("pageSize") Integer pageSize,
										   @QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("itemDetail:orgId ="+orgId+","+begin+","+end+","+branchNos+","+pageSize+","+pageNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.itemDetail(orgId,begin,end,branchNos,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 单品销量库存
	 * begin=2018-12-01&pageSize=10&pageNo=1&end=2019-01-01&itemNo=6923244761268
	 */
	@RequestMapping(value = "/itemStock", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray itemStock(HttpServletRequest request,
											  @QueryParam("begin") String begin,
											  @QueryParam("end") String end,
											  @QueryParam("branchNos") String branchNos,
											 @QueryParam("itemNo") String itemNo,
											  @QueryParam("pageSize") Integer pageSize,
											  @QueryParam("pageNo") Integer pageNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("itemStock:orgId ="+orgId+","+begin+","+end+","+branchNos+","+pageSize+","+pageNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xiaoshoufenxiService.itemStock(orgId,begin,end,branchNos,itemNo,pageSize,pageNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
