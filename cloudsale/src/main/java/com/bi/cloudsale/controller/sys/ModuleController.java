package com.bi.cloudsale.controller.sys;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.ModuleDto;
import com.bi.cloudsale.service.sys.ModuleService;

@Controller("SysModuleController")
@RequestMapping("/sys/module")
public class ModuleController {

	@Resource
	private ModuleService moduleService;
	
	@RequestMapping(value = "/queryOptions", method = RequestMethod.GET)
	@ResponseBody
	public List<OptionDto> queryAreaOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		List<OptionDto> list = moduleService.queryAllOptions(userId);
		return list;
	}
	
	@RequestMapping(value = "/queryAll", method = RequestMethod.GET)
	@ResponseBody
	public List<ModuleDto> queryAll(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		List<ModuleDto> list = moduleService.queryAll(userId);
		return list;
	}
}
