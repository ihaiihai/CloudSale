package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.dto.deductrule.DeductRuleDto;
import com.bi.cloudsale.dto.deductrule.DeductRuleMasterDto;
import com.bi.cloudsale.service.DeductRuleService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/Report_PC/Deduct_Rule")
public class DeductRuleController {

	private static final Logger log = LoggerFactory.getLogger(DeductRuleController.class);

	@Autowired
	private DeductRuleService deductRuleService;

	/**
	 * 提成规则列表
	 */
	@RequestMapping(value = "/queryRules", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody
	List<DeductRuleMasterDto> queryRules(HttpServletRequest request,
										 @QueryParam("branchNos") String branchNos,
										 @QueryParam("begin") String begin,
										 @QueryParam("end") String end,
										 @QueryParam("ruleName") String ruleName,
										 @QueryParam("isEnable") Long isEnable) throws IOException {
		List<DeductRuleMasterDto> results = new ArrayList<>();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("queryRules:orgId ="+orgId+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = deductRuleService.queryRules(begin,end,branchNos, URLDecoder.decode(ruleName,"UTF-8"),isEnable,orgId, null);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 提成规则详情
	 */
	@RequestMapping(value = "/rule_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody
	DeductRuleDto ruleDetail(HttpServletRequest request,
							 @QueryParam("deductNo") String deductNo) throws IOException {
		DeductRuleDto results = null;
		try {
			results = deductRuleService.ruleDetail(deductNo);
		} catch (BaseException e) {
			log.error(e.getMessage(),e);
		}
		return results;
	}
	/**
	 * 设置终止/开启
	 */
	@RequestMapping(value = "/setEnable", method = { RequestMethod.GET})
	public @ResponseBody String setEnable(HttpServletRequest request,
										  @QueryParam("deductNo") String deductNo,
										  @QueryParam("isEnable") Long isEnable) throws IOException {
		String results = "";
		String userId = (String) request.getSession().getAttribute("userId");
		if(userId==null){
			userId ="2093519f6ddf456d936c646a49da9c5a";
		}
		try {
			results = deductRuleService.setEnable(deductNo,isEnable,userId);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return results;
	}
	
	/**
	 * 设置终止/开启
	 */
	@RequestMapping(value = "/deleteRule", method = { RequestMethod.GET})
	public @ResponseBody String deleteRule(HttpServletRequest request,
										  @QueryParam("deductNo") String deductNo) throws IOException {
		String userId = (String) request.getSession().getAttribute("userId");
		if(userId==null){
			userId ="2093519f6ddf456d936c646a49da9c5a";
		}
		try {
			deductRuleService.deleteRule(deductNo,userId);
			return deductNo;
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		return null;
	}

	@RequestMapping(value = "/submit_rule", method = { RequestMethod.POST})
	public @ResponseBody String submitRule(HttpServletRequest request,
										   @RequestBody DeductRuleDto deductRuleDto) throws IOException {
		String results = "ok";
		String orgId = (String) request.getSession().getAttribute("orgId");
		String userId = (String) request.getSession().getAttribute("userId");
		if(orgId==null||userId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
			userId ="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("submit_aim:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				deductRuleService.saveOrUpdateRule(orgId,userId,deductRuleDto);
			} catch (Exception e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
