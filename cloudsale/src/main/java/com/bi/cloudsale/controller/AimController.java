package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.BranchAimService;
import com.bi.cloudsale.service.BranchDayMonthService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.Calendar;

@Controller
@RequestMapping("/Report_PC/Branch_Aim")
public class AimController {

	private static final Logger log = LoggerFactory.getLogger(AimController.class);

	@Autowired
	private BranchAimService branchAimService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 品牌列表
	 */
	@RequestMapping(value = "/brand_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray brandList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("brand_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.brands(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 整体任务进度
	 * year=2019&month=1&branchNos=000001
	 */
	@RequestMapping(value = "/whole_aim_gain", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray wholeAimGain(HttpServletRequest request,
														@QueryParam("branchNos") String branchNos,
												@QueryParam("year") String year,
												@QueryParam("month") String month) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("whole_aim_gain:orgId ="+orgId+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.queryWholeAimGain(orgId,year,month,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 整体任务详情
	 * year=2019&branchNos=000001
	 */
	@RequestMapping(value = "/whole_aim_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray wholeAimDetail(HttpServletRequest request,
												@QueryParam("branchNos") String branchNos,
												@QueryParam("year") String year) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("whole_aim_detail:orgId ="+orgId+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.queryWholeAimDetail(orgId,year,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 分类任务进度
	 * year=2019&month=1
	 */
	@RequestMapping(value = "/cls_aim_gain", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray clsAimGain(HttpServletRequest request,
											  @QueryParam("branchNos") String branchNos,
											  @QueryParam("year") String year,
											  @QueryParam("month") String month) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("cls_aim_gain:orgId ="+orgId+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.queryClsAimGain(orgId,year,month,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 分类任务详情
	 * year=2019
	 */
	@RequestMapping(value = "/cls_aim_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray clsAimDetail(HttpServletRequest request,
												@QueryParam("branchNos") String branchNos,
												@QueryParam("year") String year) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("cls_aim_detail:orgId ="+orgId+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.queryClsAimDetail(orgId,year,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 品牌任务进度
	 * year=2019&month=1
	 */
	@RequestMapping(value = "/brand_aim_gain", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray brandAimGain(HttpServletRequest request,
											  @QueryParam("branchNos") String branchNos,
											  @QueryParam("year") String year,
											  @QueryParam("month") String month) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("brand_aim_gain:orgId ="+orgId+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.queryBrandAimGain(orgId,year,month,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 品牌任务详情
	 * year=2019
	 */
	@RequestMapping(value = "/brand_aim_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray brandAimDetail(HttpServletRequest request,
												@QueryParam("branchNos") String branchNos,
												@QueryParam("year") String year) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("brand_aim_detail:orgId ="+orgId+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.queryBrandAimDetail(orgId,year,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 个人任务进度
	 * year=2019&month=1
	 */
	@RequestMapping(value = "/saler_aim_gain", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray salerAimGain(HttpServletRequest request,
												@QueryParam("branchNos") String branchNos,
												@QueryParam("year") String year,
												@QueryParam("month") String month) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("saler_aim_gain:orgId ="+orgId+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.querySalerAimGain(orgId,year,month,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 个人任务详情
	 * year=2019
	 */
	@RequestMapping(value = "/saler_aim_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray salerAimDetail(HttpServletRequest request,
												  @QueryParam("branchNos") String branchNos,
												  @QueryParam("year") String year) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("saler_aim_detail:orgId ="+orgId+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.querySalerAimDetail(orgId,year,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 提交任务
	 * [
	 {
	 "flag":"MDZTYD",
	 "key_words":"1",
	 "key_value":2093519f6ddf456d936c646a49da9c5a456,
	 "oper_date":"2018-12-01"
	 }
	 ]
	 */
	@RequestMapping(value = "/submit_aim", method = { RequestMethod.POST})
	public @ResponseBody String submitAim(HttpServletRequest request,
												  @RequestBody JSONObject aims) throws IOException {
		String results = "";
		String orgId = (String) request.getSession().getAttribute("orgId");
		String userId = (String) request.getSession().getAttribute("userId");
		if(orgId==null||userId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
			userId ="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("submit_aim:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchAimService.setAim(orgId,userId,aims);
			} catch (Exception e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
