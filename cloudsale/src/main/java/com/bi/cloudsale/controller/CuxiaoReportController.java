package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.ClientFenbuService;
import com.bi.cloudsale.service.CuxiaoReportService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import java.io.IOException;

@Controller
@RequestMapping("/Report_PC/Cuxiao_Report")
public class CuxiaoReportController {

	private static final Logger log = LoggerFactory.getLogger(CuxiaoReportController.class);

	@Autowired
	private CuxiaoReportService cuxiaoReportService;

	/**
	 * xiaoshou_lirun
	 * begin=2018-01-01&end=2019-01-01
	 */
	@RequestMapping(value = "/xiaoshou_lirun_maoli", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray xiaoshou_lirun_maoli(HttpServletRequest request,
										 @QueryParam("begin") String begin,
										 @QueryParam("end") String end,
										 @QueryParam("branchNos") String branchNos) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:query ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = cuxiaoReportService.xiaoshou_lirun_maoli(orgId,begin,end,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * kehu_huiyuan
	 * begin=2018-01-01&end=2019-01-01
	 */
	@RequestMapping(value = "/kehu_huiyuan", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray kehu_huiyuan(HttpServletRequest request,
												  @QueryParam("begin") String begin,
												  @QueryParam("end") String end,
												  @QueryParam("branchNos") String branchNos) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:query ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = cuxiaoReportService.kehu_huiyuan(orgId,begin,end,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * tongqi_huanbi
	 * begin=2018-01-01&end=2019-01-01
	 */
	@RequestMapping(value = "/tongqi_huanbi", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray tongqi_huanbi(HttpServletRequest request,
												@QueryParam("begin") String begin,
												@QueryParam("end") String end,
												@QueryParam("branchNos") String branchNos) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:query ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				JSONArray results1 = cuxiaoReportService.tongqi_huanbi(orgId,begin,end,branchNos);
				JSONArray results2 = cuxiaoReportService.keliu_kedan(orgId,begin,end,branchNos);
				results.add(results1);
				results.add(results2);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * huangshi
	 * begin=2018-01-01&end=2019-01-01
	 */
	@RequestMapping(value = "/huangshi", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray huangshi(HttpServletRequest request,
												 @QueryParam("begin") String begin,
												 @QueryParam("end") String end,
												 @QueryParam("branchNos") String branchNos) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:query ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				JSONArray results1 = cuxiaoReportService.huangshi(orgId,begin,end,branchNos);
				JSONArray results2 = cuxiaoReportService.baofadu(orgId,begin,end,branchNos);
				results.add(results1);
				results.add(results2);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * zhekou
	 * begin=2018-01-01&end=2019-01-01
	 */
	@RequestMapping(value = "/zhekou", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray zhekou(HttpServletRequest request,
										   @QueryParam("begin") String begin,
										   @QueryParam("end") String end,
										   @QueryParam("branchNos") String branchNos) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:query ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = cuxiaoReportService.zhekou(orgId,begin,end,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * item_rank
	 * begin=2018-01-01&end=2019-01-01
	 */
	@RequestMapping(value = "/item_rank", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray item_rank(HttpServletRequest request,
										  @QueryParam("begin") String begin,
										  @QueryParam("end") String end,
										  @QueryParam("branchNos") String branchNos) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:query ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = cuxiaoReportService.item_rank(orgId,begin,end,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
