package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.ShouqingService;
import com.bi.cloudsale.service.XinpindongxiaoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;

@Controller
@RequestMapping("/Report_PC/Xinpin_dongxiao")
public class XinpindongxiaoController {

	private static final Logger log = LoggerFactory.getLogger(XinpindongxiaoController.class);

	@Autowired
	private XinpindongxiaoService xinpindongxiaoService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xinpindongxiaoService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 品牌列表
	 */
	@RequestMapping(value = "/brand_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray brandList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("brand_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xinpindongxiaoService.brands(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 分类列表
	 */
	@RequestMapping(value = "/cls_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray clsList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("cls_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xinpindongxiaoService.clses(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 商品列表
	 */
	@RequestMapping(value = "/item_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray itemList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("item_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xinpindongxiaoService.items(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 售罄详情
	 */
	@RequestMapping(value = "/dongxiao_query", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray queryDongxiao(HttpServletRequest request,
												 @QueryParam("begin") String begin,
												 @QueryParam("end") String end,
												 @QueryParam("branchNos") String branchNos,
												 @QueryParam("itemClses") String itemClses,
												 @QueryParam("itemNos") String itemNos,
												 @QueryParam("brands") String brands) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("dongxiao_query:orgId ="+orgId+","+begin+","+end+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = xinpindongxiaoService.query(orgId,begin,end,branchNos,itemClses,brands,itemNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
