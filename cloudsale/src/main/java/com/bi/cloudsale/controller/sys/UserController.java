package com.bi.cloudsale.controller.sys;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.model.BasicResult;
import com.bi.cloudsale.common.model.ResponseConstant;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.sys.UserInfoDto;
import com.bi.cloudsale.service.sys.UserInfoService;

@Controller("SysUserController")
@RequestMapping("/sys/user")
public class UserController {

	@Resource
	private UserInfoService userInfoService;
	
	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	@ResponseBody
	public BasicResult createUser(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody UserInfoDto userInfoDto) throws IOException {
		
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		BasicResult basicResult = new BasicResult();
		try {
			userInfoDto.setParentUserId(userId);
			userInfoService.createUser(userInfoDto);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(StringUtils.isNotEmpty(e.getCode()) ? e.getCode() : ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
	
	@RequestMapping(value = "/deleteUser", method = RequestMethod.GET)
	@ResponseBody
	public BasicResult deleteUser(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("userId") String userId) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		try {
			userInfoService.deleteUser(userId);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
	
	@RequestMapping(value = "/updateUser", method = RequestMethod.POST)
	@ResponseBody
	public BasicResult updateUser(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody UserInfoDto userInfoDto) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		try {
			userInfoService.updateUser(userInfoDto);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
	
	@RequestMapping(value = "/queryByCondition", method = RequestMethod.GET)
	@ResponseBody
	public List<UserInfoDto> queryByCondition(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("key") String key) throws IOException {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		String parentUserId = (String) session.getAttribute("parentUserId");
		String orgId = (String) session.getAttribute("orgId");
		BaseQueryCondition baseQueryCondition = new BaseQueryCondition();
		if("100".equals(parentUserId)) {
			baseQueryCondition.setOrgId(orgId);
		}else {
			baseQueryCondition.setUserId(userId);
		}
		baseQueryCondition.setKey(key);
		List<UserInfoDto> list = userInfoService.queryByCondition(baseQueryCondition);
		return list;
	}
	
	@RequestMapping(value = "/queryByUserId", method = RequestMethod.GET)
	@ResponseBody
	public UserInfoDto queryByUserId(@QueryParam("userId") String userId, HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		UserInfoDto dto = userInfoService.queryByUserId(userId);
		if(dto != null) {
			BaseQueryCondition baseQueryCondition = new BaseQueryCondition();
			baseQueryCondition.setUserId(userId);
			List<UserInfoDto> list = userInfoService.queryByCondition(baseQueryCondition);
			dto.setChildUserCount(list.size());
		}
		return dto;
	}
}
