package com.bi.cloudsale.controller.sys;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.service.sys.ButtonService;

@Controller("SysButtonController")
@RequestMapping("/sys/button")
public class ButtonController {

	@Resource
	private ButtonService buttonService;
	
	@RequestMapping(value = "/queryOptions", method = RequestMethod.GET)
	@ResponseBody
	public List<OptionDto> queryButtonOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		List<OptionDto> list = buttonService.queryAllOptions();
		return list;
	}
}
