
package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.annotation.FieldName;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.common.utils.ExcelUtil;
import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.dto.commision.SalaryBranchDto;
import com.bi.cloudsale.dto.commision.SalaryDetailDto;
import com.bi.cloudsale.dto.commision.SalarySalerDto;
import com.bi.cloudsale.dto.deductrule.DeductRuleMasterDto;
import com.bi.cloudsale.dto.task.FollowUpTaskDto;
import com.bi.cloudsale.dto.task.SetUpTaskDto;
import com.bi.cloudsale.service.*;
import com.bi.cloudsale.service.commision.SalaryBranchService;
import com.bi.cloudsale.service.commision.SalaryDetailService;
import com.bi.cloudsale.service.commision.SalarySalerService;
import com.bi.cloudsale.service.task.BrandTaskService;
import com.bi.cloudsale.service.task.ClsTaskService;
import com.bi.cloudsale.service.task.OverallTaskService;
import com.bi.cloudsale.service.task.SalerTaskService;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.File;
import java.io.FileInputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.*;

@Controller
@RequestMapping("/Report_PC/dataExport")
public class DataExportController
{
	private static final Log LOG = LogFactory.getLog(DataExportController.class);

	@Resource
	ClientFenbuService clientFenbuService;
	@Autowired
	private ShouqingService shouqingService;
	@Autowired
	private JiegoubaogaoService jiegoubaogaoService;
	@Autowired
	private JinxiaocunService jinxiaocunService;
	@Autowired
	private XinpindongxiaoService xinpindongxiaoService;
	@Autowired
	private XiaoshoufenxiService xiaoshoufenxiService;
	@Autowired
	private DeductRuleService deductRuleService;
	@Autowired
	private BranchAimService branchAimService;
	@Resource
	private BrandTaskService brandTaskService;
	@Resource
	private ClsTaskService clsTaskService;
	@Resource
	private OverallTaskService overallTaskService;
	@Resource
	private SalerTaskService salerTaskService;
	@Resource
	private SalaryDetailService salaryDetailService;
	@Resource
	private SalaryBranchService salaryBranchService;
	@Resource
	private SalarySalerService salarySalerService;

	//http://localhost:8080/cloudsale/Report_PC/dataExport/excel?begin=2018-01-01&end=2019-01-01&busi=clientFenbu
	@RequestMapping(value = "/excel", method = RequestMethod.GET)
	public void createBoxListExcel(HttpServletResponse response, HttpServletRequest request,
								   @QueryParam("begin") String begin,
								   @QueryParam("end") String end,
								   @QueryParam("branchNos") String branchNos,
								   @QueryParam("clsNos") String clsNos,
								   @QueryParam("brandNos") String brandNos,
								   @QueryParam("type") String type,
								   @QueryParam("pageNo") Integer pageNo,
								   @QueryParam("priceZones") String priceZones,
								   @QueryParam("pageSize") Integer pageSize,
								   @QueryParam("likeName") String likeName,
								   @QueryParam("busi") String busi) throws Exception {
		String orgId = (String) request.getSession().getAttribute("orgId");
		String userId = (String) request.getSession().getAttribute("userId");
		
		List<?> dtoList = new ArrayList<>();
		LinkedHashMap<String, String> fieldMap = new LinkedHashMap<String, String>();
		if(StringUtils.equals("commisionDetail",busi)) {
			dtoList = salaryDetailService.queryByCondition(orgId, DateUtil.getDateStart(begin), DateUtil.getDateEnd(end), branchNos, null);
			fieldMap = getFields(dtoList);
		}else if(StringUtils.equals("commisionBranch",busi)) {
			dtoList = salaryBranchService.queryByCondition(orgId, DateUtil.getDateStart(begin), DateUtil.getDateEnd(end), branchNos);
			fieldMap = getFields(dtoList);
		}else if(StringUtils.equals("commisionSaler",busi)) {
			dtoList = salarySalerService.queryByCondition(orgId, DateUtil.getDateStart(begin), DateUtil.getDateEnd(end), branchNos, null);
			fieldMap = getFields(dtoList);
		}else if(StringUtils.equals("commisionSet",busi)){
			dtoList = deductRuleService.queryRules(begin,end,branchNos, URLDecoder.decode(likeName,"UTF-8"),Long.valueOf(type),orgId, null);
			fieldMap = getFields(dtoList);
		}else if(StringUtils.equals("brandTaskSetUp",busi)){
			TaskQueryCondition condition = new TaskQueryCondition();
			condition.setUserId(userId);
			condition.setOrgId(orgId);
			condition.setBranchNos(branchNos);
			condition.setYear(Integer.valueOf(begin));
			condition.setAimType(3);
			dtoList = brandTaskService.querySetUp(condition);
			fieldMap = getFields(dtoList);
		}else if(StringUtils.equals("overallTaskSetUp",busi)){
			TaskQueryCondition condition = new TaskQueryCondition();
			condition.setUserId(userId);
			condition.setOrgId(orgId);
			condition.setBranchNos(branchNos);
			condition.setYear(Integer.valueOf(begin));
			condition.setAimType(1);
			dtoList = overallTaskService.querySetUp(condition);
			fieldMap = getFields(dtoList);
		}else if(StringUtils.equals("clsTaskSetUp",busi)){
			TaskQueryCondition condition = new TaskQueryCondition();
			condition.setUserId(userId);
			condition.setOrgId(orgId);
			condition.setBranchNos(branchNos);
			condition.setYear(Integer.valueOf(begin));
			condition.setAimType(2);
			dtoList = clsTaskService.querySetUp(condition);
			fieldMap = getFields(dtoList);
		}else if(StringUtils.equals("salerTaskSetUp",busi)){
			TaskQueryCondition condition = new TaskQueryCondition();
			condition.setUserId(userId);
			condition.setOrgId(orgId);
			condition.setBranchNos(branchNos);
			condition.setYear(Integer.valueOf(begin));
			condition.setAimType(4);
			dtoList = salerTaskService.querySetUp(condition);
			fieldMap = getFields(dtoList);
		}else if(StringUtils.equals("salerTaskFollowUp",busi)){
			TaskQueryCondition condition = new TaskQueryCondition();
			condition.setUserId(userId);
			condition.setOrgId(orgId);
			condition.setBranchNos(branchNos);
			condition.setYear(Integer.valueOf(begin));
			condition.setMonth(Integer.valueOf(end));
			condition.setAimType(4);
			dtoList = salerTaskService.queryFollowUp(condition);
			fieldMap = getFields(dtoList);
		}
		JSONArray list = new JSONArray();
		//根据busi来查询不同的接口
		if(StringUtils.equals("clientFenbu",busi)){
			list = clientFenbuService.queryByAmt(orgId,begin,end,branchNos,clsNos,brandNos);//getPersonList();
			fieldMap = getFields(list);
		}else if(StringUtils.equals("soldOutManage",busi)){
			list = shouqingService.query(orgId,type,begin,end,branchNos,clsNos,brandNos,pageNo,pageSize);
			fieldMap = getFields(list);
		}else if(StringUtils.equals("goodStruct",busi)){
			list = jiegoubaogaoService.query(orgId,type,begin,end,branchNos,clsNos,brandNos, JSON.parseArray(priceZones));
			fieldMap = getFields(list);
		}else if(StringUtils.equals("jinxiaocun",busi)){
			list = jinxiaocunService.query(orgId,type,begin,end,branchNos,clsNos,brandNos);
			fieldMap = getFields(list);
		}else if(StringUtils.equals("xinpinDongxiao",busi)){
			list = xinpindongxiaoService.query(orgId,begin,end,branchNos,clsNos,brandNos,"");
			fieldMap = getFields(list);
		}else if(StringUtils.equals("xiaoshouFenxi",busi)){
			if(StringUtils.equals("byItem",type)){
				list = xiaoshoufenxiService.byItem(orgId,begin,end,branchNos,pageSize,pageNo);
				fieldMap = getFields(list);
			}else if(StringUtils.equals("byBranch",type)){
				list = xiaoshoufenxiService.byBranch(orgId,begin,end,branchNos,pageSize,pageNo);
				fieldMap = getFields(list);
			}else if(StringUtils.equals("byMonth",type)){
			}else if(StringUtils.equals("byDay",type)){
			}else if(StringUtils.equals("byCls",type)){
				list = xiaoshoufenxiService.byCls(orgId,begin,end,branchNos,pageSize,pageNo);
				fieldMap = getFields(list);
			}else if(StringUtils.equals("byBrand",type)){
				list = xiaoshoufenxiService.byBrand(orgId,begin,end,branchNos,pageSize,pageNo);
				fieldMap = getFields(list);
			}else if(StringUtils.equals("bySaler",type)){
				list = xiaoshoufenxiService.bySaler(orgId,begin,end,branchNos,pageSize,pageNo);
				fieldMap = getFields(list);
			}
		}else if(StringUtils.equals("brandTaskFollowUp",busi)){
			list =branchAimService.queryBrandAimGain(orgId,begin,end,branchNos);
			fieldMap = getFields(list);
		}else if(StringUtils.equals("clsTaskFollowUp",busi)){
			list =branchAimService.queryClsAimGain(orgId,begin,end,branchNos);
			fieldMap = getFields(list);
		}else if(StringUtils.equals("overallTaskFollowUp",busi)){
			list = branchAimService.queryWholeAimGain(orgId,begin,end,branchNos);
			fieldMap = getFields(list);
		}
		if(fieldMap.size() > 0) {
			try {
				OutputStream out = response.getOutputStream();
				String filename = DateUtil.formatDate(new Date(), DateUtil.SIMPLE_DATE_FORMAT) + ".xls";
	            filename = URLEncoder.encode(filename, "UTF-8");
	            response.addHeader("Content-Disposition", "attachment;filename=" + filename);
	            response.setContentType("application/x-download; charset=UTF-8");
				if(dtoList != null && dtoList.size() > 0) {
		            ExcelUtil.listToExcel(dtoList, fieldMap, "Sheet1", out);
		        }else if(list != null && list.size() > 0) {
		            ExcelUtil.listToExcel(list, fieldMap, "Sheet1", out);
		        }else {
		        	LOG.error("没有查询到数据");
		        }
			} catch (Exception e) {
				LOG.error("导出失败"+e.getMessage(), e);
			}
			return;
		}
	}
	
	private LinkedHashMap<String, String> getFields(JSONArray data) {
		LinkedHashMap<String, String> fieldMap = new LinkedHashMap<String, String>(); 
		JSONObject obj = (JSONObject) data.get(0);
		for (Map.Entry<String,Object> entry : obj.entrySet()) {
			fieldMap.put(entry.getKey(), entry.getKey());
		}
		return fieldMap;
	}
	
	private LinkedHashMap<String, String> getFields(List<?> data) {
		LinkedHashMap<String, String> fieldMap = new LinkedHashMap<String, String>(); 
		Field fields[] = data.get(0).getClass().getDeclaredFields();
		Field.setAccessible(fields, true);
		for (Field field : fields) {
			FieldName fieldName = field.getAnnotation(FieldName.class);
			if(fieldName != null) {
				fieldMap.put(field.getName(), fieldName.value());
			}
		}
		return fieldMap;
	}
}

