package com.bi.cloudsale.controller.sys;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bi.cloudsale.service.sys.RoleAuthService;

@Controller("SysRoleAuthController")
@RequestMapping("/sys/roleAuth")
public class RoleAuthController {

	@Resource
	private RoleAuthService roleAuthService;
}
