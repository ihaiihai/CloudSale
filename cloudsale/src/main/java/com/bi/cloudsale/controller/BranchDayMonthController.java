package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.service.BranchDayMonthService;
import com.bi.cloudsale.service.HomePageService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

@Controller
@RequestMapping("/Report_PC/Branch_DayMonth")
public class BranchDayMonthController {

	private static final Logger log = LoggerFactory.getLogger(BranchDayMonthController.class);

	@Autowired
	private BranchDayMonthService branchDayMonthService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 门店本月、日销售概要
	 * branchNo=000016
	 */
	@RequestMapping(value = "/day_month_branch_gain_abstract", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray dayMonthBranchGainAbstract(HttpServletRequest request,
														@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("day_month_gain_abstract:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.dayMonthBranchGainAbstract(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}


	/**
	 * 门店昨日销售详情
	 */
	@RequestMapping(value = "/yesterday_branch_gain", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterDayBranchGain(HttpServletRequest request,
											 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("today_gain:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.yesterDayBranchGain(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
    /**
     * 畅销商品排行100
     */
    @RequestMapping(value = "/yesterday_branch_best_item", method = { RequestMethod.POST,RequestMethod.GET })
    public @ResponseBody JSONArray yesterdayBranchBestItem(HttpServletRequest request,
                                                   @QueryParam("branchNo") String branchNo) throws IOException {
        JSONArray results = new JSONArray();
        String orgId = (String) request.getSession().getAttribute("orgId");
        if(orgId==null){
            orgId="2093519f6ddf456d936c646a49da9c5a";
        }
        log.info("today_gain:orgId ="+orgId+","+branchNo);
        if(StringUtils.isNotEmpty(orgId)){
            try {
                results = branchDayMonthService.yesterdayBranchBestItem(orgId,branchNo);
            } catch (BaseException e) {
                log.error(e.getMessage(),e);
            }
        }
        return results;
    }
	/**
	 * 今日最低任务
	 * branchNo=000001
	 */
	@RequestMapping(value = "/today_lowest_target", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray todayLowestTarget(HttpServletRequest request,
													 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("today_lowest_target:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.todayLowestTarget(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 本月销售
	 */
	@RequestMapping(value = "/month_gain", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthGain(HttpServletRequest request,
											 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_gain:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.monthGain(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 昨日各时段客流、单价
	 */
	@RequestMapping(value = "/yesterday_period", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayPeriod(HttpServletRequest request,
												   @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yesterday_period:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.yesterdayPeriod(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 昨日销售类别分布8
	 */
	@RequestMapping(value = "/yesterday_cls", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayCls(HttpServletRequest request,
												   @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yesterday_cls:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.yesterdayCls(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 昨日销售客单分布9
	 */
	@RequestMapping(value = "/yesterday_price_zone", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayPriceZone(HttpServletRequest request,
												@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yesterday_price_zone:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.yesterdayPriceZone(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 滞销商品排行100 10
	 */
	@RequestMapping(value = "/yesterday_branch_bad_item", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayBranchBadItem(HttpServletRequest request,
														   @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("today_gain:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.yesterdayBranchBadItem(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 异常库存排行100 11
	 */
	@RequestMapping(value = "/yesterday_branch_exception_stock_item", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayBranchExceptionStockItem(HttpServletRequest request,
														  @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("today_gain:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = branchDayMonthService.yesterdayBranchExceptionStockItem(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * xxxx-xx-xx至xxxx-xx-xx实时指标
	 * operMonth = 2018-11
	 */
	@RequestMapping(value = "/month_realtime_target_text", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthRealTimeTargetText(HttpServletRequest request,
														   @QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		JSONObject resultJson = new JSONObject();
		String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
		resultJson.put("name",operMonth+"-01至"+operMonth+"-"+lastDayInMonth+"实时指标");
		results.add(resultJson);
		return results;
	}

	/**
	 * 关键运营指标监控 13
	 * operMonth = 2018-11
	 */
	@RequestMapping(value = "/month_branch_target_view", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthBranchTargetView(HttpServletRequest request,
																	 @QueryParam("branchNo") String branchNo,
														 @QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_branch_target_view:orgId ="+orgId+","+branchNo+","+operMonth);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = branchDayMonthService.monthBranchTargetView(orgId,branchNo,operMonth+"-01",operMonth+"-"+lastDayInMonth);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 会员各项指标监控 14
	 * operMonth = 2018-11
	 */
	@RequestMapping(value = "/month_branch_vip_view", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthBranchVipView(HttpServletRequest request,
														 @QueryParam("branchNo") String branchNo,
														 @QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_branch_vip_view:orgId ="+orgId+","+branchNo+","+operMonth);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = branchDayMonthService.monthBranchVipView(orgId,branchNo,operMonth+"-01",operMonth+"-"+lastDayInMonth);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 本月销售完成率
	 * operMonth = 2018-11
	 */
	@RequestMapping(value = "/month_branch_sale_wanchenglv", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthBranchSaleWanchenglv(HttpServletRequest request,
													  @QueryParam("branchNo") String branchNo,
													  @QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_branch_sale_wanchenglv:orgId ="+orgId+","+branchNo+","+operMonth);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = branchDayMonthService.monthBranchSaleWanchenglv(orgId,branchNo,operMonth+"-01",operMonth+"-"+lastDayInMonth);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 本月销售波动，日、本期、同比、环比
	 * operMonth = 2018-11
	 */
	@RequestMapping(value = "/month_branch_sale_bodong", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthBranchSaleBodong(HttpServletRequest request,
															 @QueryParam("branchNo") String branchNo,
															 @QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_branch_sale_bodong:orgId ="+orgId+","+branchNo+","+operMonth);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = branchDayMonthService.monthBranchSaleBodong(orgId,branchNo,operMonth+"-01",operMonth+"-"+lastDayInMonth);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 本月门店销售分布
	 * operMonth = 2018-11
	 */
	@RequestMapping(value = "/month_branch_sale_fenbu", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthBranchSaleFenbu(HttpServletRequest request,
														 @QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_branch_sale_fenbu:orgId ="+orgId+","+operMonth);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = branchDayMonthService.monthBranchSaleFenbu(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 本月类别销售分布
	 * operMonth = 2018-11
	 */
	@RequestMapping(value = "/month_branch_cls_fenbu", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthBranchClsFenbu(HttpServletRequest request,
													   @QueryParam("branchNo") String branchNo,
														@QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_branch_cls_fenbu:orgId ="+orgId+","+branchNo+","+operMonth);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = branchDayMonthService.monthBranchClsFenbu(orgId,branchNo,operMonth+"-01",operMonth+"-"+lastDayInMonth);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各门店销售同比、环比、结构占比
	 */
	@RequestMapping(value = "/month_branch_sale_tongbi_huangbi_zhanbi", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthBranchSaleTongbiHuanbiZhanbi(HttpServletRequest request,
													   @QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_branch_sale_tongbi_huangbi_zhanbi:orgId ="+orgId+","+operMonth);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = branchDayMonthService.monthBranchSaleTongbiHuanbiZhanbi(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
