package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.JiegoubaogaoService;
import com.bi.cloudsale.service.ShouqingService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;

@Controller
@RequestMapping("/Report_PC/Jiegou_Baogao")
public class JiegoubaogaoController {

	private static final Logger log = LoggerFactory.getLogger(JiegoubaogaoController.class);

	@Autowired
	private JiegoubaogaoService jiegoubaogaoService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jiegoubaogaoService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 品牌列表
	 */
	@RequestMapping(value = "/brand_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray brandList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("brand_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jiegoubaogaoService.brands(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 分类列表
	 */
	@RequestMapping(value = "/cls_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray clsList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("cls_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jiegoubaogaoService.clses(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 商品列表
	 */
	@RequestMapping(value = "/item_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray itemList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("item_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jiegoubaogaoService.items(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 *priceZones = [{
	 					"low": 0,
	 					"high": 100
					 }, {
	 					"low": 0,
						 "high": 100
					}]
	 */
	@RequestMapping(value = "/jiegou_query", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray queryJiegou(HttpServletRequest request,
											   @QueryParam("type") String type,
											   @QueryParam("begin") String begin,
											   @QueryParam("end") String end,
											   @QueryParam("branchNos") String branchNos,
											   @QueryParam("itemClses") String itemClses,
											   @QueryParam("brands") String brands, @RequestBody JSONArray priceZones) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("jiegou_query:orgId ="+orgId+","+begin+","+end+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jiegoubaogaoService.query(orgId,type,begin,end,branchNos,itemClses,brands,priceZones);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 详情
	 * begin=2017-12-01&end=2029-01-01&clsNo=1&branchNo=2
	 * [{
	 "low": 0,
	 "high": 500
	 }, {
	 "low": 0,
	 "high": 100
	 }]
	 */
	@RequestMapping(value = "/item_sale", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray itemSale(HttpServletRequest request,
												 @QueryParam("begin") String begin,
												 @QueryParam("end") String end,
											@QueryParam("branchNo") String branchNo,
												 @QueryParam("clsNo") String clsNo,
											@QueryParam("onlyHasStock") boolean onlyHasStock,
											@RequestBody JSONArray priceZones) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("item_sale:orgId ="+orgId+","+begin+","+end+","+clsNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = jiegoubaogaoService.itemSaleByClsno(orgId,branchNo,clsNo,priceZones,begin,end,onlyHasStock);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
