package com.bi.cloudsale.controller.sys;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.model.BasicResult;
import com.bi.cloudsale.common.model.ResponseConstant;
import com.bi.cloudsale.condition.BaseQueryCondition;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.AreaInfoDto;
import com.bi.cloudsale.service.sys.AreaInfoService;

@Controller("SysAreaController")
@RequestMapping("/sys/area")
public class AreaController {

	@Resource
	private AreaInfoService areaInfoService;
	
	@RequestMapping(value = "/queryByCondition", method = RequestMethod.GET)
	@ResponseBody
	public Map<String,Object> roleQueryByCondition(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("key") String key) throws IOException {
		
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		BaseQueryCondition baseQueryCondition = new BaseQueryCondition();
		baseQueryCondition.setOrgId(orgId);
		baseQueryCondition.setKey(key);
		Map<String,Object> map = new HashMap<String,Object>();
		List<AreaInfoDto> list = areaInfoService.queryByCondition(baseQueryCondition);
		map.put("rows", list);
		return map;
	}
	
	@RequestMapping(value = "/queryOptions", method = RequestMethod.GET)
	@ResponseBody
	public List<OptionDto> queryAreaOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		List<OptionDto> list = areaInfoService.queryAllOptions(orgId);
		return list;
	}
	
	@RequestMapping(value = "/queryByCode", method = RequestMethod.GET)
	@ResponseBody
	public AreaInfoDto queryByCode(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("areaCode") String areaCode) throws IOException {
		
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		AreaInfoDto dto = areaInfoService.queryByCode(areaCode);
		return dto;
	}
	
	@RequestMapping(value = "/createArea", method = RequestMethod.POST)
	@ResponseBody
	public BasicResult createArea(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody AreaInfoDto areaInfoDto) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		try {
			HttpSession session = request.getSession();
			String userId = (String) session.getAttribute("userId");
			areaInfoDto.setUserId(userId);
			areaInfoService.createAreaInfo(areaInfoDto);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
	
	@RequestMapping(value = "/deleteArea", method = RequestMethod.GET)
	@ResponseBody
	public BasicResult deleteArea(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("areaCode") String areaCode) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		try {
			areaInfoService.deleteAreaInfo(areaCode);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
	
	@RequestMapping(value = "/updateArea", method = RequestMethod.POST)
	@ResponseBody
	public BasicResult updateArea(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody AreaInfoDto areaInfoDto) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		try {
			areaInfoService.updateAreaInfo(areaInfoDto);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
}
