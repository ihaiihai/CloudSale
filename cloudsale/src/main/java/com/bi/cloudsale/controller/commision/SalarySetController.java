package com.bi.cloudsale.controller.commision;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bi.cloudsale.condition.ItemQueryCondition;
import com.bi.cloudsale.dto.commision.ItemInfoDto;
import com.bi.cloudsale.service.commision.SalarySetService;

@Controller("SalarySetController")
@RequestMapping("/salary/set")
public class SalarySetController {

	@Resource
	private SalarySetService salarySetService;
	
	@RequestMapping(value = "/queryByCondition", method = RequestMethod.POST)
	@ResponseBody
	public List<ItemInfoDto> queryByCondition(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody ItemQueryCondition condition) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return salarySetService.queryByCondition(orgId, condition.getClsNo(), condition.getKey(), null, null);
	}
}
