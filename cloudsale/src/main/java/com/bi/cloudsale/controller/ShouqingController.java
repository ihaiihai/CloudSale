package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.ShouqingService;
import com.bi.cloudsale.service.YunyingzhibiaoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;

@Controller
@RequestMapping("/Report_PC/Shouqing")
public class ShouqingController {

	private static final Logger log = LoggerFactory.getLogger(ShouqingController.class);

	@Autowired
	private ShouqingService shouqingService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = shouqingService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 品牌列表
	 */
	@RequestMapping(value = "/brand_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray brandList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("brand_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = shouqingService.brands(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 分类列表
	 */
	@RequestMapping(value = "/cls_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray clsList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("cls_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = shouqingService.clses(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 商品列表
	 */
	@RequestMapping(value = "/item_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray itemList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("item_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = shouqingService.items(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 售罄详情
	 * begin=2017-12-01&end=2019-01-01&type=cls&pageNo=1&pageSize=10
	 */
	@RequestMapping(value = "/shouqing_query", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray queryShouqing(HttpServletRequest request,
												 @QueryParam("type") String type,
												 @QueryParam("begin") String begin,
												 @QueryParam("end") String end,
												 @QueryParam("branchNos") String branchNos,
												 @QueryParam("itemClses") String itemClses,
												 @QueryParam("brands") String brands,
												 @QueryParam("pageNo") Integer pageNo,
												 @QueryParam("pageSize") Integer pageSize) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("shouqing_query:orgId ="+orgId+","+begin+","+end+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = shouqingService.query(orgId,type,begin,end,branchNos,itemClses,brands,pageNo,pageSize);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 售罄详情
	 */
	@RequestMapping(value = "/item_sale_stock", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray itemSaleStock(HttpServletRequest request,
												 @QueryParam("begin") String begin,
												 @QueryParam("end") String end,
												 @QueryParam("itemNo") String itemNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("item_sale_stock:orgId ="+orgId+","+begin+","+end+","+itemNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = shouqingService.itemSaleStock(orgId,itemNo,begin,end);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
