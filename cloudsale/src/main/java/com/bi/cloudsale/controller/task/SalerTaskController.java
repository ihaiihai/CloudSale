package com.bi.cloudsale.controller.task;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.dto.task.FollowUpTaskDto;
import com.bi.cloudsale.dto.task.SetUpTaskDto;
import com.bi.cloudsale.service.task.SalerTaskService;

@Controller("SalerTaskController")
@RequestMapping("/task/saler")
public class SalerTaskController {

	@Resource
	private SalerTaskService salerTaskService;
	
	@RequestMapping(value = "/queryFollowUp", method = RequestMethod.GET)
	@ResponseBody
	public List<FollowUpTaskDto> queryFollowUp(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("branchNos") String branchNos, @QueryParam("year") Integer year, @QueryParam("month") Integer month) throws IOException {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		String orgId = (String) session.getAttribute("orgId");
		TaskQueryCondition condition = new TaskQueryCondition();
		condition.setUserId(userId);
		condition.setOrgId(orgId);
		condition.setBranchNos(branchNos);
		condition.setYear(year);
		condition.setMonth(month);
		condition.setAimType(4);
		return salerTaskService.queryFollowUp(condition);
	}
	
	@RequestMapping(value = "/querySetUp", method = RequestMethod.GET)
	@ResponseBody
	public List<SetUpTaskDto> querySetUp(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("branchNos") String branchNos, @QueryParam("year") Integer year) throws IOException {
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		String orgId = (String) session.getAttribute("orgId");
		TaskQueryCondition condition = new TaskQueryCondition();
		condition.setUserId(userId);
		condition.setOrgId(orgId);
		condition.setBranchNos(branchNos);
		condition.setYear(year);
		condition.setAimType(4);
		return salerTaskService.querySetUp(condition);
	}
}
