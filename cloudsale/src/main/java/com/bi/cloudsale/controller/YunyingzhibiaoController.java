package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.XiaoshoufenxiService;
import com.bi.cloudsale.service.YunyingzhibiaoService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;

@Controller
@RequestMapping("/Report_PC/Yunying_Zhibiao")
public class YunyingzhibiaoController {

	private static final Logger log = LoggerFactory.getLogger(YunyingzhibiaoController.class);

	@Autowired
	private YunyingzhibiaoService yunyingzhibiaoService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = yunyingzhibiaoService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 按商品 门店列表 日期区间 分页
	 * begin=2018-12-01&end=2019-01-01
	 */
	@RequestMapping(value = "/branch_yunying", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray queryBranchYunying(HttpServletRequest request,
														@QueryParam("begin") String begin,
															  @QueryParam("end") String end,
															  @QueryParam("branchNos") String branchNos) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_yunying:orgId ="+orgId+","+begin+","+end+","+branchNos);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = yunyingzhibiaoService.queryBranchYunying(orgId,begin,end,branchNos);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
