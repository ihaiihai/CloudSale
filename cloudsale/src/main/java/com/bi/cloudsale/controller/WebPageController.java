package com.bi.cloudsale.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/CloudSale/Report_PC/web_page")
public class WebPageController {
	
	@RequestMapping(value = "/home_page.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String home(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/home_page";
    }

	@RequestMapping(value = "Aspx/headquarters_daily.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String headquarters_daily(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/headquarters_daily";
    }
	
	@RequestMapping(value = "Aspx/headquarters_monthly.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String headquarters_monthly(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/headquarters_monthly";
    }
	
	@RequestMapping(value = "Aspx/purchase_report.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String purchase_report(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/purchase_report";
    }
	
	@RequestMapping(value = "Aspx/Sales_details_inquiry.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String Sales_details_inquiry(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/Sales_details_inquiry";
    }
	
	@RequestMapping(value = "Aspx/DailyCheckReport.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String DailyCheckReport(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/DailyCheckReport";
    }
	
	@RequestMapping(value = "Aspx/sales_summary_main.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String sales_summary_main(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/sales_summary_main";
    }
	
	@RequestMapping(value = "Aspx/store_evaluation.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String store_evaluation(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/store_evaluation";
    }
	
	@RequestMapping(value = "Aspx/PurchaseAnalysis.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String PurchaseAnalysis(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/PurchaseAnalysis";
    }
	
	@RequestMapping(value = "Aspx/PurchaseTask.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String PurchaseTask(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/PurchaseTask";
    }
	
	@RequestMapping(value = "Aspx/PurchaseAnalysisFlow.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String PurchaseAnalysisFlow(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/PurchaseAnalysisFlow";
    }
	
	@RequestMapping(value = "Aspx/PurchaseAnalysisNewSku.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String PurchaseAnalysisNewSku(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/PurchaseAnalysisNewSku";
    }
	
	@RequestMapping(value = "Aspx/PurchaseOrder.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String PurchaseOrder(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/PurchaseOrder";
    }
	
	@RequestMapping(value = "Aspx/PurchaseDistribution.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String PurchaseDistribution(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/PurchaseDistribution";
    }
	
	@RequestMapping(value = "Aspx/store_daily.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String store_daily(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/store_daily";
    }
	
	@RequestMapping(value = "Aspx/return_analysis.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String return_analysis(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/return_analysis";
    }
	
	@RequestMapping(value = "Aspx/discount_monitoring.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String discount_monitoring(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/discount_monitoring";
    }
	
	@RequestMapping(value = "Aspx/personnel_report.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String personnel_report(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/personnel_report";
    }
	
	@RequestMapping(value = "Aspx/guest_distribution.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String guest_distribution(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/guest_distribution";
    }
	
	@RequestMapping(value = "Aspx/distribution_report.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String distribution_report(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/distribution_report";
    }
	
	@RequestMapping(value = "Aspx/sales_promotion_report.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String sales_promotion_report(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/sales_promotion_report";
    }
	
	@RequestMapping(value = "Aspx/best_seller.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String best_seller(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/best_seller";
    }
	
	@RequestMapping(value = "Aspx/StockTargetList.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String StockTargetList(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/StockTargetList";
    }
	
	@RequestMapping(value = "Aspx/members_single_portrait.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String members_single_portrait(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/members_single_portrait";
    }
	
	@RequestMapping(value = "Aspx/DailyCheckList.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String DailyCheckList(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/DailyCheckList";
    }
	
	@RequestMapping(value = "Aspx/NewVipReport.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String NewVipReport(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/NewVipReport";
    }
	
	@RequestMapping(value = "Aspx/VipserviceReport.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String VipserviceReport(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/VipserviceReport";
    }
	
	@RequestMapping(value = "Aspx/PurchaseAnalysisList.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String PurchaseAnalysisList(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/PurchaseAnalysisList";
    }
	
	@RequestMapping(value = "Aspx/Sales_target_main.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String Sales_target_main(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/Sales_target_main";
    }
	
	@RequestMapping(value = "Aspx/Sales_tasks_ALL.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String Sales_tasks_ALL(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/Sales_tasks_ALL";
    }
	
	@RequestMapping(value = "Aspx/Sales_target_clsEx.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String Sales_target_clsEx(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/Sales_target_clsEx";
    }
	
	@RequestMapping(value = "Aspx/Sales_tasks_clsEx.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String Sales_tasks_clsEx(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/Sales_tasks_clsEx";
    }
	
	@RequestMapping(value = "Aspx/Sales_target_brand.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String Sales_target_brand(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/Sales_target_brand";
    }
	
	@RequestMapping(value = "Aspx/Sales_tasks_brand.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String Sales_tasks_brand(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/Sales_tasks_brand";
    }
	
	@RequestMapping(value = "Aspx/Sales_target_saleMan.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String Sales_target_saleMan(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/Sales_target_saleMan";
    }
	
	@RequestMapping(value = "Aspx/Sales_tasks_saleMan.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String Sales_tasks_saleMan(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/Sales_tasks_saleMan";
    }
	
	@RequestMapping(value = "Aspx/DynamicSalary.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String DynamicSalary(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/DynamicSalary";
    }
	
	@RequestMapping(value = "Aspx/DynamicSalaryList.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String DynamicSalaryList(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/DynamicSalaryList";
    }
	
	@RequestMapping(value = "Aspx/DynamicSalarySetting.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String DynamicSalarySetting(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/DynamicSalarySetting";
    }
	
	@RequestMapping(value = "Aspx/SalaryReport.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String SalaryReport(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/SalaryReport";
    }
	
	@RequestMapping(value = "Aspx/StockTarget.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String StockTarget(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/StockTarget";
    }
	
	@RequestMapping(value = "Aspx/sellThroughRate.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String sellThroughRate(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/sellThroughRate";
    }
	
	@RequestMapping(value = "Aspx/inventory_report.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String inventory_report(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/inventory_report";
    }
	
	@RequestMapping(value = "Aspx/deposit_report.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String deposit_report(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/deposit_report";
    }
	
	@RequestMapping(value = "Aspx/SKUStructure.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String SKUStructure(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/SKUStructure";
    }
	
	@RequestMapping(value = "Aspx/abnormal_stock.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String abnormal_stock(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Aspx/abnormal_stock";
    }
	
}
