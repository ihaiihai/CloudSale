package com.bi.cloudsale.controller.base;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bi.cloudsale.condition.ItemQueryCondition;
import com.bi.cloudsale.dto.item.BaseItemInfoDto;
import com.bi.cloudsale.service.item.BaseItemInfoService;

@Controller("BaseItemController")
@RequestMapping("/base/item")
public class BaseItemInfoController {

	@Resource
	private BaseItemInfoService baseItemInfoService;
	
	@RequestMapping(value = "/queryByCondition", method = RequestMethod.POST)
	@ResponseBody
	public List<BaseItemInfoDto> queryByCondition(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody ItemQueryCondition condition) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return baseItemInfoService.queryByCondition(orgId, condition.getClsNo(), condition.getKey(), null, null);
	}
}
