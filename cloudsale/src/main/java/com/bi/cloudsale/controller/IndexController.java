package com.bi.cloudsale.controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.model.BasicResult;
import com.bi.cloudsale.common.model.ResponseConstant;
import com.bi.cloudsale.common.utils.DateUtil;
import com.bi.cloudsale.dto.LoginRequestDto;
import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.dto.sys.RoleInfoDto;
import com.bi.cloudsale.dto.sys.UserInfoDto;
import com.bi.cloudsale.service.CommonService;
import com.bi.cloudsale.service.sys.RoleInfoService;
import com.bi.cloudsale.service.sys.UserInfoService;

@Controller
@RequestMapping("/")
public class IndexController {
	
	private static final Logger log = LoggerFactory.getLogger(IndexController.class);
	
	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private RoleInfoService roleInfoService;
	@Autowired
	private CommonService commonService;

	@RequestMapping(value = "home", method = { RequestMethod.POST,RequestMethod.GET })
    public String home(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		HttpSession session = request.getSession();
		String account = (String) session.getAttribute("account");
		if(StringUtils.isEmpty(account)) {
            return "login1";
		}
		String orgId = (String) session.getAttribute("orgId");
		String userId = (String) session.getAttribute("userId");
		String roleName = (String) session.getAttribute("roleName");
		model.addAttribute("userId", userId);
		model.addAttribute("account", account);
		model.addAttribute("roleName", roleName);
		return "index1";
    }
	
	@RequestMapping(value = "login", method = { RequestMethod.POST,RequestMethod.GET })
    public String login(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		HttpSession session = request.getSession();
		session.setAttribute("account", null);
		return "login1";
    }
	
	@RequestMapping(value = "recoverpw", method = { RequestMethod.POST,RequestMethod.GET })
    public String recoverpw(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "recoverpw";
    }
	
	@RequestMapping(value = "register", method = { RequestMethod.POST,RequestMethod.GET })
    public String register(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "register";
    }
	
	@RequestMapping(value = "test", method = { RequestMethod.POST,RequestMethod.GET })
    public String test(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "test";
    }
	
	@RequestMapping(value = "login/submit", method = RequestMethod.POST)
	@ResponseBody
	public BasicResult loginSubmit(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody LoginRequestDto loginRequestDto) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		
		UserInfoDto dto = null;
		try {
			dto = userInfoService.queryByAccount(loginRequestDto.getUserAccount());
		} catch (BaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(dto != null && loginRequestDto != null && StringUtils.equals(loginRequestDto.getUserPwd(), dto.getPassword())) {
			if(dto.getAccountType() == null || dto.getAccountType() != 2) {
				Date createDate = dto.getGmtCreate();
				Integer authTime = dto.getAuthTime();
				long roleDate = DateUtil.addDay(createDate, authTime != null ? authTime : 0).getTime();
				long now = System.currentTimeMillis();
				if(roleDate < now) {
					basicResult.setCode(ResponseConstant.ERROR_CODE);
					basicResult.setMsg("账号已超过授权时间");
					return basicResult;
				}
			}
			HttpSession session = request.getSession();
			session.setAttribute("userId", dto.getUserId());
			session.setAttribute("parentUserId", dto.getParentUserId());
			session.setAttribute("orgId", dto.getOrganizeId());
			session.setAttribute("account", dto.getAccount());
			RoleInfoDto roleInfoDto = roleInfoService.queryByCode(dto.getRoleCode());
			session.setAttribute("roleName", roleInfoDto != null ? roleInfoDto.getRoleName() : "");
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}else {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg("用户名或密码不正确");
		}
		return basicResult;
	}
	
	@RequestMapping(value = "/queryAllBranch", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> queryAllBranch(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.queryAllBranch(orgId);
	}
	
	@RequestMapping(value = "/queryAllBrand", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> queryAllBrand(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.queryAllBrand(orgId);
	}
	
	@RequestMapping(value = "/queryAllCls", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> queryAllCls(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.queryAllCls(orgId);
	}
	
	@RequestMapping(value = "/queryAllSaler", method = RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> queryAllSaler(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.queryAllSaler(orgId);
	}
	
	@RequestMapping(value = "/queryClsOptions", method = RequestMethod.GET)
	@ResponseBody
	public List<OptionDto> queryClsOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.queryClsOptions(orgId);
	}
	
	@RequestMapping(value = "/queryBrandOptions", method = RequestMethod.GET)
	@ResponseBody
	public List<OptionDto> queryBrandOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.queryBrandOptions(orgId);
	}
	
	@RequestMapping(value = "/queryBranchOptions", method = RequestMethod.GET)
	@ResponseBody
	public List<OptionDto> queryBranchOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.queryBranchOptions(orgId);
	}
	
	@RequestMapping(value = "/querySalerOptions", method = RequestMethod.GET)
	@ResponseBody
	public List<OptionDto> querySalerOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.querySalerOptions(orgId);
	}

	@RequestMapping(value = "/itemDetail", method = RequestMethod.GET)
	@ResponseBody
	public JSONObject itemDetail(HttpServletRequest request, HttpServletResponse response,
								 @QueryParam("itemNo") String itemNo) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.itemDetail(orgId,itemNo);
	}

	@RequestMapping(value = "/itemClsDetail", method = RequestMethod.GET)
	@ResponseBody
	public JSONObject itemClsDetail(HttpServletRequest request, HttpServletResponse response,
									@QueryParam("itemNo") String itemNo) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.itemClsDetail(orgId,itemNo);
	}

	@RequestMapping(value = "/itemBrandDetail", method = RequestMethod.GET)
	@ResponseBody
	public JSONObject itemBrandDetail(HttpServletRequest request, HttpServletResponse response,
									@QueryParam("itemNo") String itemNo) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return commonService.itemBrandDetail(orgId,itemNo);
	}
}
