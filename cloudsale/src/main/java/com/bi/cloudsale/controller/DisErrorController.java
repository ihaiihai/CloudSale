package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.BranchDayMonthService;
import com.bi.cloudsale.service.DisErrorService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.Calendar;

/**
 * 折让、异常报告
 */
@Controller
@RequestMapping("/Report_PC/Branch_DisError")
public class DisErrorController {

	private static final Logger log = LoggerFactory.getLogger(DisErrorController.class);

	@Autowired
	private DisErrorService disErrorService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = disErrorService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 门店 折扣率
	 */
	@RequestMapping(value = "/branch_dislv", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchDislv(HttpServletRequest request,
														@QueryParam("begin") String begin,
														@QueryParam("end") String end,
														@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_dislv:orgId ="+orgId+","+branchNo+","+begin+","+end);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = disErrorService.branchDislv(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各品类折扣情况
	 */
	@RequestMapping(value = "/branch_cls_dis_fenbu", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchClsDisFenbu(HttpServletRequest request,
											   @QueryParam("begin") String begin,
											   @QueryParam("end") String end,
											   @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_cls_dis_fenbu:orgId ="+orgId+","+branchNo+","+begin+","+end);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = disErrorService.branchClsDisFenbu(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各品牌同单折让情况
	 */
	@RequestMapping(value = "/branch_cls_branch_dis_fenbu", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchClsBranchDisFenbu(HttpServletRequest request,
													 @QueryParam("begin") String begin,
													 @QueryParam("end") String end,
													 @QueryParam("branchNo") String branchNo,
														   @QueryParam("clsNo") String clsNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_cls_branch_dis_fenbu:orgId ="+orgId+","+branchNo+","+begin+","+end+","+clsNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = disErrorService.branchClsBranchDisFenbu(orgId,begin,end,branchNo,clsNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 畅销单品折扣监控（按销售金额排序）
	 */
	@RequestMapping(value = "/branch_best_sale_item_dis", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchBestSaleItemDis(HttpServletRequest request,
														   @QueryParam("begin") String begin,
														   @QueryParam("end") String end,
														   @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_best_sale_item_dis:orgId ="+orgId+","+branchNo+","+begin+","+end);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = disErrorService.branchBestSaleItemDis(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 门店 异常率
	 */
	@RequestMapping(value = "/branch_errorlv", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchErrorlv(HttpServletRequest request,
											   @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_errorlv:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = disErrorService.branchErrorlv(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 门店 分类异常分布
	 */
	@RequestMapping(value = "/branch_error_fenbu", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchErrorFenbu(HttpServletRequest request,
												 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_error_fenbu:orgId ="+orgId+","+branchNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = disErrorService.branchErrorFenbu(orgId,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 门店 异常详情
	 */
	@RequestMapping(value = "/branch_error_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchErrorDetail(HttpServletRequest request,
													@QueryParam("clsNo") String clsNo,
													@QueryParam("pageSize") Integer pageSize,
													@QueryParam("pageNo") Integer pageNo,
													@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_error_detail:orgId ="+orgId+","+branchNo+","+pageSize+","+pageNo+","+clsNo);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = disErrorService.branchErrorDetail(orgId,branchNo,pageSize,pageNo,clsNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
