package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSON;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.model.BasicResult;
import com.bi.cloudsale.common.model.ResponseConstant;
import com.bi.cloudsale.dto.ClientSaveDto;
import com.bi.cloudsale.dto.CommisionRuleCondDto;
import com.bi.cloudsale.dto.LoginRequestDto;
import com.bi.cloudsale.dto.RemoteInfoDto;
import com.bi.cloudsale.dto.deductrule.DeductRuleDto;
import com.bi.cloudsale.dto.deductrule.DeductRuleMasterDto;
import com.bi.cloudsale.dto.sys.ModuleDto;
import com.bi.cloudsale.dto.sys.UserInfoDto;
import com.bi.cloudsale.service.ClientService;
import com.bi.cloudsale.service.DeductRuleService;
import com.bi.cloudsale.service.sys.ModuleService;
import com.bi.cloudsale.service.sys.UserInfoService;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/client")
public class ClientController {
	
	private static final Logger log = LoggerFactory.getLogger(ClientController.class);
	@Autowired
	private UserInfoService userInfoService;
	@Autowired
	private ClientService clientService;

	@RequestMapping(value = "login", method = RequestMethod.POST)
	public @ResponseBody BasicResult login(HttpServletRequest request,
							   @RequestBody LoginRequestDto loginRequestDto) throws IOException {
		UserInfoDto dto = null;
		try {
			dto = userInfoService.queryByAccount(loginRequestDto.getUserAccount());
			if(dto==null){
				dto = userInfoService.queryByMobile(loginRequestDto.getUserAccount());
			}
		} catch (BaseException e) {
			e.printStackTrace();
		}
		if(dto != null && loginRequestDto != null && StringUtils.equals(loginRequestDto.getUserPwd(), dto.getPassword())) {
			Map<String,String> map  = new HashMap<String,String>();
			map.put("userId", dto.getUserId());
			map.put("orgId", dto.getOrganizeId());
			return BasicResult.success(map);
		}else {
			return BasicResult.error("用户名或密码不正确");
		}
	}
	
	@RequestMapping(value = "bindRemoteInfo", method = RequestMethod.POST)
	public @ResponseBody BasicResult bindRemoteInfo(HttpServletRequest request,
							   @RequestBody RemoteInfoDto remoteInfoDto) throws IOException {
		try {
			userInfoService.updateRemoteInfo(remoteInfoDto);
			return BasicResult.success();
		} catch (BaseException e) {
			return BasicResult.error("绑定失败");
		}
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public @ResponseBody BasicResult save(@RequestBody ClientSaveDto clientSaveDto){

		log.info("client save:"+JSON.toJSONString(clientSaveDto));
		try {
			Long id = clientService.saveOrUpdate(clientSaveDto);
			return BasicResult.success(id);
		} catch (BaseException e) {
			log.error("Client.save,"+e.getMessage(), e);
			return BasicResult.error(e.getMessage());
		}
	}

	@Autowired
	private DeductRuleService deductRuleService;

	@RequestMapping(value = "/queryValidCommisionRules", method = RequestMethod.POST)
	public @ResponseBody BasicResult queryValidCommisionRules(@RequestBody CommisionRuleCondDto commisionRuleCondDto){

		log.info("validCommisionRule:"+JSON.toJSONString(commisionRuleCondDto));
		if(StringUtils.isEmpty(commisionRuleCondDto.getOrgId())){
			return BasicResult.error("请求参数不全，请检查");
		}
		try {
			List<DeductRuleMasterDto> results = deductRuleService.queryRules(commisionRuleCondDto.getBegin(),commisionRuleCondDto.getEnd(),"", "",1L,commisionRuleCondDto.getOrgId(),commisionRuleCondDto.getGmt_modify());
			return BasicResult.success(results);
		} catch (BaseException e) {
			log.error("Client.save,"+e.getMessage(), e);
			return BasicResult.error(e.getMessage());
		}
	}

	/**
	 * 提成规则详情
	 */
	@RequestMapping(value = "/rule_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody
	BasicResult ruleDetail(HttpServletRequest request,
							 @QueryParam("deductNo") String deductNo) throws IOException {
		DeductRuleDto result = null;
		try {
			result = deductRuleService.ruleDetail(deductNo);
			return BasicResult.success(result);
		} catch (BaseException e) {
			return BasicResult.error(e.getMessage());
		}
	}
}
