package com.bi.cloudsale.controller;

import java.io.IOException;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.HomePageService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/Report_PC/Home_page")
public class HomePageController {

	private static final Logger log = LoggerFactory.getLogger(HomePageController.class);

	@Autowired
	private HomePageService homePageService;

	@RequestMapping(value = "/home_page.aspx", method = { RequestMethod.POST,RequestMethod.GET })
    public String home(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		return "Home_page/home_page";
    }

	/**
	 * 昨日销售完成、昨日成交单数、昨日新增用户、昨日新增会员
	 */
	@RequestMapping(value = "/yesterday_gain", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayGain(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yesterday_gain:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.yesterdayGain(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 今日门店实时运营监控指标
	 */
	@RequestMapping(value = "/today_realtime_index", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray  todayRealtimeIndex(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("today_realtime_index:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.todayRealtimeIndex(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 今日最低任务
	 */
	@RequestMapping(value = "/today_lowest_target", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray todayLowestTarget(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("today_lowest_target:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.todayLowestTarget(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 月完成
	 */
	@RequestMapping(value = "/month_gain", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthGain(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_gain:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.monthGain(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 月完成度百分比
	 */
	@RequestMapping(value = "/month_gain_percent", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthGainPercent(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_gain_percent:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.monthGainPercent(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 昨日退货率、昨日折扣率、昨日客单价、昨日连带率
	 */
	@RequestMapping(value = "/yesterday_t_z_d_l", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayTZDL(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yesterday_t_z_d_l:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.yesterdayTZDL(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各门店今天实时销售分布
	 */
	@RequestMapping(value = "/today_branch_sale", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray todayBranchSale(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("today_branch_sale:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.todayBranchSale(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

    /**
     * 某天各店铺销售分布（首页柱状图点击后调用）
     */
    @RequestMapping(value = "/someday_branch_sale", method = { RequestMethod.POST,RequestMethod.GET })
    public @ResponseBody JSONArray someDayBranchSale(HttpServletRequest request,@QueryParam("operDate") String operDate) throws IOException {
        //operDate = "2018-12-06"
        JSONArray results = new JSONArray();
        String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
        log.info("someday_branch_sale:orgId ="+orgId+",operDate="+operDate);
        if(StringUtils.isNotEmpty(orgId)&&StringUtils.isNotEmpty(operDate)){
            try {
                results = homePageService.someDayBranchSale(orgId,operDate);
            } catch (BaseException e) {
                log.error(e.getMessage(),e);
            }
        }
        return results;
    }
    
    /**
     * 某天各店铺销售分布（首页柱状图点击后调用）
     */
    @RequestMapping(value = "/someday_branch_cls_sale", method = { RequestMethod.POST,RequestMethod.GET })
    public @ResponseBody JSONArray someDayBranchClsSale(HttpServletRequest request,@QueryParam("branchNo") String branchNo) throws IOException {
        //operDate = "2018-12-06"
        JSONArray results = new JSONArray();
        String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
        if(StringUtils.isNotEmpty(orgId)&&StringUtils.isNotEmpty(branchNo)){
            try {
                results = homePageService.someDayBranchClsSale(orgId,branchNo,new Date());
            } catch (BaseException e) {
                log.error(e.getMessage(),e);
            }
        }
        return results;
    }

    /**
     * 昨日各店销售完成情况（首页点击昨日销售完成）
     */
    @RequestMapping(value = "/yestday_branch_sale", method = { RequestMethod.POST,RequestMethod.GET })
    public @ResponseBody JSONArray yestdayBranchSale(HttpServletRequest request) throws IOException {
        JSONArray results = new JSONArray();
        String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
        log.info("someday_branch_sale:orgId ="+orgId);
        if(StringUtils.isNotEmpty(orgId)){
            try {
                results = homePageService.yestdayBranchSale(orgId);
            } catch (BaseException e) {
                log.error(e.getMessage(),e);
            }
        }
        return results;
    }

    /**
     * 最近30天内成交单数波动详情（昨日成交单数点击后）
     */
    @RequestMapping(value = "/last30_branch_bill_cnt", method = { RequestMethod.POST,RequestMethod.GET })
    public @ResponseBody JSONArray last30BranchBillCnt(HttpServletRequest request) throws IOException {
        JSONArray results = new JSONArray();
        String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
        log.info("last30_branch_bill_cnt:orgId ="+orgId);
        if(StringUtils.isNotEmpty(orgId)){
            try {
                results = homePageService.last30BranchBillCnt(orgId);
            } catch (BaseException e) {
                log.error(e.getMessage(),e);
            }
        }
        return results;
    }

	/**
	 * 最近30日内新增客户波动详情（昨日新增客户点击后）
	 */
	@RequestMapping(value = "/last30_branch_new_client", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray last30BranchNewClient(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("last30_branch_new_client:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.last30BranchNewClient(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 近30日内新增会员波动详情（昨日新增会员点击后）
	 */
	@RequestMapping(value = "/last30_branch_new_vip", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray last30BranchNewVip(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("last30_branch_new_vip:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.last30BranchNewVip(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 昨日退货率详情（昨日退货率大圈点击后）
	 */
	@RequestMapping(value = "/yesterday_ret_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayRetDetail(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yesterday_ret_detail:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.yesterdayRetDetail(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 昨日折扣率详情（昨日折扣率大圈点击后）
	 */
	@RequestMapping(value = "/yesterday_dis_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayDisDetail(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yesterday_dis_detail:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.yesterdayDisDetail(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各门店客单价分布（昨日客单价大圈点击后）
	 */
	@RequestMapping(value = "/yesterday_client_amt_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayClientAmtDetail(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yesterday_client_amt_detail:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.yesterdayClientAmtDetail(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各门店连带率分布（昨日连带率大圈点击后）
	 */
	@RequestMapping(value = "/yesterday_liandailv_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesterdayLiandailvDetail(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yesterday_client_amt_detail:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.yesterdayLiandailvDetail(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各门店实时任务进度（今日最低任务点击后）
	 */
	@RequestMapping(value = "/today_realtime_aim", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray todayRealtimeAim(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("today_realtime_aim:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = homePageService.todayRealtimeAim(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
