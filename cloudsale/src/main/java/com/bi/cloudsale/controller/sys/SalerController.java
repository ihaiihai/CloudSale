package com.bi.cloudsale.controller.sys;

import com.bi.cloudsale.dto.OptionDto;
import com.bi.cloudsale.service.sys.SalerInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller("SysSalerController")
@RequestMapping("/sys/saler")
public class SalerController {
	@Resource
	private SalerInfoService salerInfoService;

	@RequestMapping(value = "/queryOptions", method = RequestMethod.GET)
	@ResponseBody
	public List<OptionDto> querySalerOptions(HttpServletRequest request, HttpServletResponse response) throws IOException {
		
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		List<OptionDto> list = salerInfoService.queryAllOptions(orgId);
		return list;
	}
}
