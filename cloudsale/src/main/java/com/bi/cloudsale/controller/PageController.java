package com.bi.cloudsale.controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bi.cloudsale.dto.sys.RoleAuthDto;
import com.bi.cloudsale.dto.sys.UserInfoDto;
import com.bi.cloudsale.persistent.enums.sys.RoleAuthTypeEnum;
import com.bi.cloudsale.service.sys.RoleAuthService;
import com.bi.cloudsale.service.sys.UserInfoService;

@Controller
@RequestMapping("/page")
public class PageController {
	
	@Resource
	private UserInfoService userInfoService;
	@Resource
	private RoleAuthService roleAuthService;
	
	@RequestMapping(value = "/home", method = { RequestMethod.POST,RequestMethod.GET })
    public String home_page(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "首页");
		return "Html/home";
    }
	
	@RequestMapping(value = "/headquarters_day", method = { RequestMethod.POST,RequestMethod.GET })
    public String headquarters_day(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "总部日报");
		return "Html/overall/report/headquarters_day";
    }
	
	@RequestMapping(value = "/headquarters_month", method = { RequestMethod.POST,RequestMethod.GET })
    public String headquarters_month(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "总部月报");
		return "Html/overall/report/headquarters_month";
    }
	
	@RequestMapping(value = "/branch_day", method = { RequestMethod.POST,RequestMethod.GET })
    public String branch_day(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "门店日报");
		return "Html/overall/report/branch_day";
    }
	
	@RequestMapping(value = "/rejected_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String rejected_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "退货报告");
		return "Html/overall/report/rejected";
    }
	
	@RequestMapping(value = "/allowance_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String allowance_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "折让报告");
		return "Html/overall/report/allowance";
    }
	
	@RequestMapping(value = "/personnel_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String personnel_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "人员报告");
		return "Html/overall/report/personnel";
    }
	
	@RequestMapping(value = "/customer_order", method = { RequestMethod.POST,RequestMethod.GET })
    public String customer_order(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "客单分布");
		return "Html/overall/business/customer";
    }
	
	@RequestMapping(value = "/promotion_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String promotion_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "促销报告");
		return "Html/overall/business/promotion";
    }
	
	@RequestMapping(value = "/sales_analysis", method = { RequestMethod.POST,RequestMethod.GET })
    public String sales_analysis(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "销售分析");
		return "Html/overall/sales";
    }
	
	@RequestMapping(value = "/operation_monitor", method = { RequestMethod.POST,RequestMethod.GET })
    public String operation_monitor(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "运营指标监控");
		return "Html/overall/operation";
    }
	
	@RequestMapping(value = "/overall_task_set", method = { RequestMethod.POST,RequestMethod.GET })
    public String overall_task_set(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "整体任务设定");
		return "Html/task/overall/set_up";
    }
	
	@RequestMapping(value = "/overall_task_follow", method = { RequestMethod.POST,RequestMethod.GET })
    public String overall_task_follow(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "整体任务跟进");
		return "Html/task/overall/follow_up";
    }
	
	@RequestMapping(value = "/category_task_set", method = { RequestMethod.POST,RequestMethod.GET })
    public String category_task_set(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "品类任务设定");
		return "Html/task/category/set_up";
    }
	
	@RequestMapping(value = "/category_task_follow", method = { RequestMethod.POST,RequestMethod.GET })
    public String category_task_follow(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "品类任务进度");
		return "Html/task/category/follow_up";
    }
	
	@RequestMapping(value = "/brand_task_set", method = { RequestMethod.POST,RequestMethod.GET })
    public String brand_task_set(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "品牌任务设定");
		return "Html/task/brand/set_up";
    }
	
	@RequestMapping(value = "/brand_task_follow", method = { RequestMethod.POST,RequestMethod.GET })
    public String brand_task_follow(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "品牌任务进度");
		return "Html/task/brand/follow_up";
    }
	
	@RequestMapping(value = "/personal_task_set", method = { RequestMethod.POST,RequestMethod.GET })
    public String personal_task_set(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "个人任务设定");
		return "Html/task/personal/set_up";
    }
	
	@RequestMapping(value = "/personal_task_follow", method = { RequestMethod.POST,RequestMethod.GET })
    public String personal_task_follow(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "个人任务进度");
		return "Html/task/personal/follow_up";
    }
	
	@RequestMapping(value = "/commision_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String commision_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "实时提成报告");
		return "Html/performance/commision_report";
    }
	
	@RequestMapping(value = "/commision_detail", method = { RequestMethod.POST,RequestMethod.GET })
    public String commision_detail(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "提成详情明细");
		return "Html/performance/commision_detail";
    }
	
	@RequestMapping(value = "/commision_branch", method = { RequestMethod.POST,RequestMethod.GET })
    public String commision_branch(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "门店提成明细");
		return "Html/performance/commision_branch";
    }
	
	@RequestMapping(value = "/commision_saler", method = { RequestMethod.POST,RequestMethod.GET })
    public String commision_saler(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "营业员提成明细");
		return "Html/performance/commision_saler";
    }
	
	@RequestMapping(value = "/commision_set", method = { RequestMethod.POST,RequestMethod.GET })
    public String commision_set(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "动态提成设置");
		return "Html/performance/commision_set";
    }
	
	@RequestMapping(value = "/salary_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String salary_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "工资详情报告");
		return "Html/performance/salary_report";
    }
	
	@RequestMapping(value = "/grid_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String grid_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "网络指标报告");
		return "Html/goods/grid_manage/report";
    }
	
	@RequestMapping(value = "/grid_set", method = { RequestMethod.POST,RequestMethod.GET })
    public String grid_set(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "网络指标设定");
		return "Html/goods/grid_manage/set_up";
    }
	
	@RequestMapping(value = "/sold_out_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String sold_out_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "货品售罄报告");
		return "Html/goods/sold_out_manage/report";
    }
	
	@RequestMapping(value = "/turnover_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String turnover_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "库存周转报告");
		return "Html/goods/turnover_analysis/stock_report";
    }
	
	@RequestMapping(value = "/deposit_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String deposit_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "寄存报告");
		return "Html/goods/deposit_manage/report";
    }
	
	@RequestMapping(value = "/goods_structure_report", method = { RequestMethod.POST,RequestMethod.GET })
    public String goods_structure_report(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "商品结构报告");
		return "Html/goods/goods_structure";
    }
	
	@RequestMapping(value = "/new_product_analysis", method = { RequestMethod.POST,RequestMethod.GET })
    public String new_product_analysis(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "新品动销分析");
		return "Html/goods/new_product_analysis";
    }
	
	@RequestMapping(value = "/inventory_analysis", method = { RequestMethod.POST,RequestMethod.GET })
    public String inventory_analysis(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "进销存分析");
		return "Html/goods/inventory_analysis";
    }
	
	@RequestMapping(value = "/branch_month", method = { RequestMethod.POST,RequestMethod.GET })
    public String branch_month(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "门店月报");
		return "Html/branch/month_report";
    }
	
	@RequestMapping(value = "/branch_abnormal", method = { RequestMethod.POST,RequestMethod.GET })
    public String branch_abnormal(HttpServletRequest request, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "异常报告");
		return "Html/branch/abnormal_report";
    }

	@RequestMapping(value = "/sys/user", method = { RequestMethod.POST,RequestMethod.GET })
    public String user(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "用户管理");
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		UserInfoDto userDto = userInfoService.queryByUserId(userId);
		if(userDto != null) {
			model.addAttribute("accountType", userDto.getAccountType());
			RoleAuthDto roleAuthDto = roleAuthService.queryByRoleCode(userDto.getRoleCode(), RoleAuthTypeEnum.BUTTON.getValue());
			if(roleAuthDto != null) {
				String buttonIds = roleAuthDto.getItemId();
				if(buttonIds.indexOf("201") != -1) {
					model.addAttribute("showAddBtn", "1");
				}
				if(buttonIds.indexOf("202") != -1) {
					model.addAttribute("showEditBtn", "1");
				}
				if(buttonIds.indexOf("203") != -1) {
					model.addAttribute("showDeleteBtn", "1");
				}
			}
		}
		return "Html/sys/user";
    }
	
	@RequestMapping(value = "/sys/role", method = { RequestMethod.POST,RequestMethod.GET })
    public String post(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "角色管理");
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		UserInfoDto userDto = userInfoService.queryByUserId(userId);
		if(userDto != null) {
			RoleAuthDto roleAuthDto = roleAuthService.queryByRoleCode(userDto.getRoleCode(), RoleAuthTypeEnum.BUTTON.getValue());
			if(roleAuthDto != null) {
				String buttonIds = roleAuthDto.getItemId();
				if(buttonIds.indexOf("101") != -1) {
					model.addAttribute("showAddBtn", "1");
				}
				if(buttonIds.indexOf("102") != -1) {
					model.addAttribute("showEditBtn", "1");
				}
				if(buttonIds.indexOf("103") != -1) {
					model.addAttribute("showDeleteBtn", "1");
				}
			}
		}
		return "Html/sys/role";
    }
	
	@RequestMapping(value = "/sys/area", method = { RequestMethod.POST,RequestMethod.GET })
    public String area(HttpServletRequest request, HttpServletResponse response, Model model) throws IOException {
		model.addAttribute("baseurl", request.getContextPath());
		model.addAttribute("title", "区域管理");
		HttpSession session = request.getSession();
		String userId = (String) session.getAttribute("userId");
		UserInfoDto userDto = userInfoService.queryByUserId(userId);
		if(userDto != null) {
			RoleAuthDto roleAuthDto = roleAuthService.queryByRoleCode(userDto.getRoleCode(), RoleAuthTypeEnum.BUTTON.getValue());
			if(roleAuthDto != null) {
				String buttonIds = roleAuthDto.getItemId();
				if(buttonIds.indexOf("301") != -1) {
					model.addAttribute("showAddBtn", "1");
				}
				if(buttonIds.indexOf("302") != -1) {
					model.addAttribute("showEditBtn", "1");
				}
				if(buttonIds.indexOf("303") != -1) {
					model.addAttribute("showDeleteBtn", "1");
				}
			}
		}
		return "Html/sys/area";
    }
	
}
