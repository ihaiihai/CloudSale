package com.bi.cloudsale.controller.overall;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.QueryParam;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.common.model.BasicResult;
import com.bi.cloudsale.common.model.ResponseConstant;
import com.bi.cloudsale.condition.TaskQueryCondition;
import com.bi.cloudsale.dto.overall.BranchIndDto;
import com.bi.cloudsale.dto.overall.OperationDto;
import com.bi.cloudsale.dto.task.SetUpTaskDto;
import com.bi.cloudsale.service.overall.OperationService;

@Controller("OperationController")
@RequestMapping("/overall/operation")
public class OperationController {

	@Resource
	private OperationService operationService;
	
	@RequestMapping(value = "/createOperation", method = RequestMethod.POST)
	@ResponseBody
	public BasicResult createOperation(HttpServletRequest request, HttpServletResponse response, 
			@RequestBody BranchIndDto operationDto) throws IOException {
		
		BasicResult basicResult = new BasicResult();
		try {
			HttpSession session = request.getSession();
			String userId = (String) session.getAttribute("userId");
			String orgId = (String) session.getAttribute("orgId");
			operationDto.setUser_id(userId);
			operationDto.setOrg_id(orgId);
			operationService.createOperation(operationDto);
			basicResult.setCode(ResponseConstant.SUCCESS_CODE);
		}catch(BaseException e) {
			basicResult.setCode(ResponseConstant.ERROR_CODE);
			basicResult.setMsg(e.getMessage());
		}
		return basicResult;
	}
	
	@RequestMapping(value = "/queryFollowUp", method = RequestMethod.GET)
	@ResponseBody
	public List<OperationDto> queryFollowUp(HttpServletRequest request, HttpServletResponse response, 
			@QueryParam("branchNos") String branchNos, @QueryParam("begin") String begin, 
			@QueryParam("end") String end) throws IOException {
		HttpSession session = request.getSession();
		String orgId = (String) session.getAttribute("orgId");
		return operationService.queryOperation(orgId, branchNos, begin, end);
	}
}
