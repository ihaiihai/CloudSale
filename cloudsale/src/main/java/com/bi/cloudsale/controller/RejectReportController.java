package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.BranchDayMonthService;
import com.bi.cloudsale.service.RejectReportService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.util.Calendar;

@Controller
@RequestMapping("/Report_PC/Branch_RejectReport")
public class RejectReportController {

	private static final Logger log = LoggerFactory.getLogger(RejectReportController.class);

	@Autowired
	private RejectReportService rejectReportService;

	/**
	 * 门店列表
	 */
	@RequestMapping(value = "/branch_list", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchList(HttpServletRequest request, HttpServletResponse response){
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_list:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = rejectReportService.branches(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 门店 单品退货top 10
	 */
	@RequestMapping(value = "/branch_reject_top10", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchRejectTop10(HttpServletRequest request,
													 @QueryParam("begin") String begin,
													 @QueryParam("end") String end,
														@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_reject_top10:orgId ="+orgId+","+branchNo+","+begin+","+end);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = rejectReportService.branchRejectTop10(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}


	/**
	 * 门店 各品类实销 退货 退货率分布
	 */
	@RequestMapping(value = "/branch_sale_reject_retlv", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchSaleRejectRetlv(HttpServletRequest request,
													 @QueryParam("begin") String begin,
													 @QueryParam("end") String end,
													 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_sale_reject_retlv:orgId ="+orgId+","+branchNo+","+begin+","+end);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = rejectReportService.branchSaleRejectRetlv(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 门店 各品类退货分布
	 * begin=2018-12-01&end=2019-01-01&branchNo=000013
	 */
	@RequestMapping(value = "/branch_cls_reject_fenbu", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchClsRejectFenbu(HttpServletRequest request,
														 @QueryParam("begin") String begin,
														 @QueryParam("end") String end,
														 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_cls_reject_fenbu:orgId ="+orgId+","+branchNo+","+begin+","+end);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = rejectReportService.branchClsRejectFenbu(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 门店 各商品退货
	 * begin=2018-12-01&end=2019-01-01&branchNo=000013
	 */
	@RequestMapping(value = "/branch_item_reject_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchItemRejectDetail(HttpServletRequest request,
														@QueryParam("begin") String begin,
														@QueryParam("end") String end,
														@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_item_reject_detail:orgId ="+orgId+","+branchNo+","+begin+","+end);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = rejectReportService.branchItemRejectDetail(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 门店 退货金额、退货率
	 * begin=2018-12-01&end=2019-01-01&branchNo=000013
	 */
	@RequestMapping(value = "/branch_ret_retlv", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchRetRetlv(HttpServletRequest request,
														  @QueryParam("begin") String begin,
														  @QueryParam("end") String end,
														  @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("branch_ret_retlv:orgId ="+orgId+","+branchNo+","+begin+","+end);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = rejectReportService.branchRetRetlv(orgId,begin,end,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
