package com.bi.cloudsale.controller;

import com.alibaba.fastjson.JSONArray;
import com.bi.cloudsale.common.exception.BaseException;
import com.bi.cloudsale.service.CompanyDayMonthService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.QueryParam;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.Calendar;

@Controller
@RequestMapping("/Report_PC/Company_DayMonth")
public class CompanyDayMonthController {

	private static final Logger log = LoggerFactory.getLogger(CompanyDayMonthController.class);

	@Autowired
	private CompanyDayMonthService companyDayMonthService;

	/**
	 * 公司分类销售分布
	 */
	@RequestMapping(value = "/cls_fenbu", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray clsFenbu(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("cls_fenbu:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.clsFenbu(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	/**
	 * 公司分类销售分布 点击后
	 */
	@RequestMapping(value = "/cls_fenbu_detail", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray clsFenbuDetail(HttpServletRequest request,
												  @QueryParam("itemClsName") String itemClsName) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("cls_fenbu:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				itemClsName = URLDecoder.decode(itemClsName,"utf-8");
				results = companyDayMonthService.clsFenbuDetail(orgId,itemClsName);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 小标题
	 */
	@RequestMapping(value = "/small_title", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray smallTitle(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("small_title:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.smallTitle(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	@RequestMapping(value = "/shijianduan", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray shijianduan(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("small_title:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.shijianduan(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 平均退货折扣客单价连带率
	 */
	@RequestMapping(value = "/ret_dis_danjia_liandai", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray retDisDanjiaLiandai(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("ret_dis_danjia_liandai:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.retDisDanjiaLiandai(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 时间段销售,%%%依赖明细，暂时整不了
	 */
	@RequestMapping(value = "/period_sale", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray periodSale(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("period_sale:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.periodSale(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 昨日销售、月完成
	 */
	@RequestMapping(value = "/yes_month_wancheng", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray yesMonthWancheng(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("yes_month_wancheng:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.yesMonthWancheng(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 月任务、预测完成率
	 */
	@RequestMapping(value = "/month_aim_wancheng", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray monthAimWancheng(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("month_aim_wancheng:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.monthAimWancheng(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 标题
	 */
	@RequestMapping(value = "/title", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray title(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:orgId ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.title(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 营业员销售
	 */
	@RequestMapping(value = "/saler_sale", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray salerSale(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:saler_sale ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.salerSale(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 门店实销、同期实销
	 */
	@RequestMapping(value = "/branch_sale_tongqi", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchSaleTongqi(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_sale_tongqi ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.branchSaleTongqi(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
	
	/**
	 * 各门店销售及退货情况 点击平均退货率后
	 */
	@RequestMapping(value = "/branch_sale_ret", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchSaleRet(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_sale_ret ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.branchSaleRet(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各门店折扣分布 点击平均折扣率后
	 */
	@RequestMapping(value = "/branch_sale_dis", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchSaleDis(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_sale_dis ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.branchSaleDis(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各门店客单价分布 点击客单价后
	 */
	@RequestMapping(value = "/branch_sale_client", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchSaleClient(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_sale_client ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.branchSaleClient(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 各门店客连带率分布 点击连带率后
	 */
	@RequestMapping(value = "/branch_sale_liandailv", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchSaleLiandailv(HttpServletRequest request) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_sale_liandailv ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				results = companyDayMonthService.branchSaleLiandailv(orgId);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 标题
	 * operMonth=2019-01
	 */
	@RequestMapping(value = "/branch_month_title", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthTitle(HttpServletRequest request,
														 @QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_title ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthTitle(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 任务五完成率
	 */
	@RequestMapping(value = "/branch_month_wanchenglv", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthWanchenglv(HttpServletRequest request,
														 @QueryParam("operMonth") String operMonth,
													@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_wanchenglv ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthWanchenglv(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 会员各项指标监控
	 */
	@RequestMapping(value = "/branch_month_vip", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthVip(HttpServletRequest request,
												  @QueryParam("operMonth") String operMonth,
														 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_vip ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthVip(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 关键运营指标监控
	 */
	@RequestMapping(value = "/branch_month_yunying", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthYunying(HttpServletRequest request,
													  @QueryParam("operMonth") String operMonth,
												  @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_yunying ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthYunying(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 分类销售分布
	 */
	@RequestMapping(value = "/branch_month_cls_sale_fenbu", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthClsSaleFenbu(HttpServletRequest request,
														@QueryParam("operMonth") String operMonth,
													  @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_sale_fenbu ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthClsSaleFenbu(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 各门店任务、完成率、本期销售、销量客流等
	 */
	@RequestMapping(value = "/branch_month_aim_wancheng_sale_client", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthAimWanchengSaleClient(HttpServletRequest request,
																	@QueryParam("operMonth") String operMonth,
														@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_aim_wancheng_sale_client ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthAimWanchengSaleClient(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 各门店客流情况
	 */
	@RequestMapping(value = "/branch_month_client", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthClient(HttpServletRequest request,
													 @QueryParam("operMonth") String operMonth,
																	@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_client ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthClient(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 各门店销售员销售分布
	 */
	@RequestMapping(value = "/branch_month_saler", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthSaler(HttpServletRequest request,
													@QueryParam("operMonth") String operMonth,
													 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_saler ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthSaler(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 销售分布对比分析
	 */
	@RequestMapping(value = "/branch_month_sale_fenbu_duibi", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthSaleFenbuDuibi(HttpServletRequest request,
															 @QueryParam("operMonth") String operMonth,
													@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_sale_fenbu_duibi ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthSaleFenbuDuibi(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 门店销售分布
	 */
	@RequestMapping(value = "/branch_month_branch_sale_fenbu", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthBranchSaleFenbu(HttpServletRequest request,
															  @QueryParam("operMonth") String operMonth,
															 @QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_branch_sale_fenbu ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthBranchSaleFenbu(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 点击本月销售完成率后
	 */
	@RequestMapping(value = "/branch_month_branch_aim_wancheng", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthBranchAimWancheng(HttpServletRequest request,
															  @QueryParam("operMonth") String operMonth) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_branch_aim_wancheng ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthBranchAimWancheng(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}

	/**
	 * 总部月报 点击各门店销售分布后
	 */
	@RequestMapping(value = "/branch_month_branch_cls", method = { RequestMethod.POST,RequestMethod.GET })
	public @ResponseBody JSONArray branchMonthBranchCls(HttpServletRequest request,
																@QueryParam("operMonth") String operMonth,
																@QueryParam("branchNo") String branchNo) throws IOException {
		JSONArray results = new JSONArray();
		String orgId = (String) request.getSession().getAttribute("orgId");
		if(orgId==null){
			orgId="2093519f6ddf456d936c646a49da9c5a";
		}
		log.info("title:branch_month_branch_cls ="+orgId);
		if(StringUtils.isNotEmpty(orgId)){
			try {
				String lastDayInMonth =String.valueOf(Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH));
				results = companyDayMonthService.branchMonthBranchCls(orgId,operMonth+"-01",operMonth+"-"+lastDayInMonth,branchNo);
			} catch (BaseException e) {
				log.error(e.getMessage(),e);
			}
		}
		return results;
	}
}
