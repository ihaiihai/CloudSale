package com.bi.cloudsale.system.interceptor;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;

public class SecurityInterceptor implements HandlerInterceptor {
	
	 private final static Logger logger = LoggerFactory
	            .getLogger(SecurityInterceptor.class);
	
	/** 
	* 免检查地址 
	*/ 
	private List<String> uncheckUrls; 
	
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		return true;
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
		if (modelAndView != null) {
			String viewName = modelAndView.getViewName();
			if(uncheckUrls.size() > 0 && uncheckUrls.contains(viewName)) {
				return;
			}
			HttpSession session = request.getSession();
			String account = (String) session.getAttribute("account");
			if(StringUtils.isEmpty(account)) {
				modelAndView.addObject("timeout", "1");
			}
		}else {
			HttpSession session = request.getSession();
			String account = (String) session.getAttribute("account");
			if(StringUtils.isEmpty(account)) {
//				response.setContentType("text/json; charset=UTF-8");
//				response.getWriter().print("timeout");
				return;
			}
		}
	}
	
	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
	}

	public List<String> getUncheckUrls() {
		return uncheckUrls;
	}

	public void setUncheckUrls(List<String> uncheckUrls) {
		this.uncheckUrls = uncheckUrls;
	}
	
}
