package com.bi.cloudsale.common.utils;


import java.math.BigDecimal;

/**
 * 加减乘除计算
 * 
 * @author baoy
 * 
 */
public class ArithUtil {

	/**
	 * 提供精确加法计算的add方法
	 * 
	 * @param value1
	 *            被加数
	 * @param value2
	 *            加数
	 * @return 两个参数的和
	 */
	public static double add(double value1, double value2) {
		BigDecimal b1 = new BigDecimal(Double.toString(value1));
		BigDecimal b2 = new BigDecimal(Double.toString(value2));
		return b1.add(b2).doubleValue();
	}

	/**
	 * 提供精确减法运算的sub方法
	 * 
	 * @param value1
	 *            被减数
	 * @param value2
	 *            减数
	 * @return 两个参数的差
	 */
	public static double sub(double value1, double value2) {
		BigDecimal b1 = new BigDecimal(Double.toString(value1));
		BigDecimal b2 = new BigDecimal(Double.toString(value2));
		return b1.subtract(b2).doubleValue();
	}

	/**
	 * 提供精确乘法运算的mul方法
	 * 
	 * @param value1
	 *            被乘数
	 * @param value2
	 *            乘数
	 * @return 两个参数的积
	 */
	public static double mul(double value1, double value2) {
		BigDecimal b1 = new BigDecimal(Double.toString(value1));
		BigDecimal b2 = new BigDecimal(Double.toString(value2));
		return b1.multiply(b2).doubleValue();
	}

	/**
	 * 提供精确的除法运算方法div
	 * 
	 * @param value1
	 *            被除数
	 * @param value2
	 *            除数
	 * @param scale
	 *            精确范围
	 * @return 两个参数的商
	 * @throws IllegalAccessException
	 */
	public static double div(double value1, double value2, int scale)
			throws IllegalAccessException {
		// 如果精确范围小于0，抛出异常信息
//		if (scale < 0) {
//			throw new IllegalAccessException("精确度不能小于0");
//		}
		if (value2==0){
			return 0;
		}
		BigDecimal b1 = new BigDecimal(Double.toString(value1));
		BigDecimal b2 = new BigDecimal(Double.toString(value2));
		//四舍五入返回
		return b1.divide(b2, scale, 4).doubleValue();
	}
	
	/**
	 * <p>Title: div</p>
	 * <p>Description: 两个double类型相除，进位模式可值参</p>
	 * @param value1
	 * @param value2
	 * @param scale
	 * @param roundingMode
	 * @return
	 */
	public static double div(double value1, double value2, int scale, int roundingMode) throws Exception{
		if (value2==0){
			return 0;
		}
		BigDecimal b1 = new BigDecimal(Double.toString(value1));
		BigDecimal b2 = new BigDecimal(Double.toString(value2));
		//四舍五入返回
		return b1.divide(b2, scale, roundingMode).doubleValue();
	}
	
	public static double setScale(double value, int scale, int roundingMode) throws Exception {
		BigDecimal b = new BigDecimal(Double.toString(value));
		return b.setScale(scale, roundingMode).doubleValue();
	}
	
	public static Double getIncrease(double a, double b) {
    	if(a == 0 || b == 0)return null;
    	try {
			return div(sub(b,a), a, 2);
		} catch (IllegalAccessException e) {
			return null;
		}
    }
	
	public static Double getPercent(double a, double b) {
    	if(a == 0 || b == 0)return null;
    	try {
			return div(a, b, 2);
		} catch (IllegalAccessException e) {
			return null;
		}
    }
	
	public static Double getScore(double a, double b) {
		if(a == 0 || b == 0)return null;
		try {
			return div(mul(a, 100), b, 2);
		} catch (IllegalAccessException e) {
			return null;
		}
	}
	
}
