package com.bi.cloudsale.common.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.view.AbstractTemplateViewResolver;
import org.springframework.web.servlet.view.AbstractUrlBasedView;

public class RichFreeMarkerViewResolver extends AbstractTemplateViewResolver {

	/**
	 * Set default viewClass
	 */
	public RichFreeMarkerViewResolver() {
		setViewClass(RichFreeMarkerView.class);
	}

	/*
	 * (non-Javadoc)
	 * @see org.springframework.web.servlet.view.AbstractTemplateViewResolver#buildView(java.lang.String)
	 */
	@Override
	protected AbstractUrlBasedView buildView(String viewName) throws Exception {
		AbstractUrlBasedView view = super.buildView(viewName);
		if(StringUtils.equals(viewName, "template")) {
			view.setUrl(viewName + ".jsp");
		}else if (viewName.startsWith("/")) {
			view.setUrl(viewName + getSuffix());
		}
		return view;
	}
	
	
}
