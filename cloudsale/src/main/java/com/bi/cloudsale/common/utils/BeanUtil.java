package com.bi.cloudsale.common.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.converters.DateTimeConverter;
import org.apache.commons.beanutils.converters.IntegerConverter;
import org.apache.commons.beanutils.converters.LongConverter;
import org.apache.commons.beanutils.converters.ShortConverter;

import com.bi.cloudsale.common.exception.BaseException;

import net.sf.cglib.beans.BeanCopier;

public class BeanUtil {
	
    private static ThreadLocal<Map<String, BeanCopier> > beanCopierMap = new ThreadLocal<Map<String, BeanCopier> >(){
        
        public Map<String, BeanCopier> initialValue() {  
            Map<String, BeanCopier> beanCopierMap = new HashMap<String, BeanCopier>();  
            return beanCopierMap;
        }  
    };
    
    static {
        ConvertUtils.register(new DateTimeConverter() {

            @SuppressWarnings("rawtypes")
            @Override
            protected Class getDefaultType() {
                return java.util.Date.class;
            }

            @SuppressWarnings("rawtypes")
            public Object convert(Class type, Object value) {
                if (value == null) {
                    return null;
                } else {
                    return super.convert(type, value);
                }
            }

        }, Date.class);
        
    	ConvertUtils.register(new LongConverter(null), Long.class);
    	ConvertUtils.register(new ShortConverter(null), Short.class);
    	ConvertUtils.register(new IntegerConverter(null), Integer.class);
//		ConvertUtils.register(new DoubleConverter(null), Double.class);
//		ConvertUtils.register(new BigDecimalConverter(null), BigDecimal.class);
    }

    /**
     * 将一个bean的属性复制到另一个bean中。
     * @param expectedBean
     * @param sourceBean
     * @return
     * @throws BaseException
     */
    public static final <T, E> T copyBeanProperties(T expectedBean, E sourceBean) throws BaseException {
        if (sourceBean != null) {
            try {
                BeanUtils.copyProperties(expectedBean, sourceBean);
            } catch (IllegalAccessException e) {
                throw new BaseException("BeanUtil错误", e);
            } catch (InvocationTargetException e) {
                throw new BaseException("BeanUtil错误", e);
            }
        }

        return expectedBean;
    }

    public static final <T, E> List<T> copyBeanPropertiesList(
            List<T> expectedBeanList, List<E> sourceBeanList,
            Class<T> expectedClazz) throws BaseException {
        if (sourceBeanList != null) {
            if (expectedBeanList == null) {
                expectedBeanList = new ArrayList<T>();
            }

            try {
                for (E e : sourceBeanList) {
                    T t = expectedClazz.newInstance();
                    BeanUtils.copyProperties(t, e);
                    expectedBeanList.add(t);
                }
            } catch (InstantiationException e) {
                throw new BaseException("BeanUtil错误", e);
            } catch (IllegalAccessException e) {
                throw new BaseException("BeanUtil错误", e);
            } catch (InvocationTargetException e) {
                throw new BaseException("BeanUtil错误", e);
            }
        }

        return expectedBeanList;
    }
    
    /**
     * 用序列化与反序列化实现深克隆
     * @param src
     * @return
     */
    public static Object deepClone(Object src) {
        Object o = null;
        try {
            if (src != null) {
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ObjectOutputStream oos = new ObjectOutputStream(baos);
                oos.writeObject(src);
                oos.close();
                ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
                ObjectInputStream ois = new ObjectInputStream(bais);
                o = ois.readObject();
                ois.close();
            }
        } catch (IOException e)  {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return o;
    }
    
     /**
     * @param source
     * @param target
     */
    public static void copyProperties(Object source,Object target){  
         String beanKey = generateKey(source.getClass(),target.getClass());  
         BeanCopier copier = null;  
         if (!beanCopierMap.get().containsKey(beanKey)) {  
             copier = BeanCopier.create(source.getClass(), target.getClass(), false);  
             beanCopierMap.get().put(beanKey, copier);  
         }else {  
             copier = beanCopierMap.get().get(beanKey);  
         }  
         copier.copy(source, target, null);  
     }  
    
     private static String generateKey(Class<?>class1,Class<?>class2){  
         return class1.toString() + class2.toString();  
     }  
}
