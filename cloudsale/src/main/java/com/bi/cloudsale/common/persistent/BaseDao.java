package com.bi.cloudsale.common.persistent;

import java.io.Serializable;
import java.util.*;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.google.common.base.Preconditions;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.AbstractEntityPersister;


public class BaseDao<T extends Serializable> {
    private Class<T> clazz;

    private final static String COUNTHQL = "select count(*) from";

    private final static String FROM = "from";
    @Resource(name = "sessionFactoryForHSPlatform")
    private SessionFactory sessionFactory;

    public void create(final T entity) throws HibernateException {
        try {
            Preconditions.checkNotNull(entity);
            getCurrentSession().saveOrUpdate(entity);
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }

    public T createEntity(final T entity) throws HibernateException {
        try {
            Preconditions.checkNotNull(entity);
            getCurrentSession().saveOrUpdate(entity);
            return entity;
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }
    public Serializable createReturnId(final T entity) throws HibernateException {
        try {
            Preconditions.checkNotNull(entity);
            Serializable io=   getCurrentSession().save(entity);
            return io;
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }

    public void delete(final T entity) throws HibernateException {
        try {
            Preconditions.checkNotNull(entity);
            getCurrentSession().delete(entity);
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }

    public void deleteById(final long entityId) throws HibernateException {
        final T entity = findById(entityId);
        Preconditions.checkState(entity != null);
        delete(entity);
    }

    @SuppressWarnings("unchecked")
    public List<T> findAll() throws HibernateException {
        try {
            return getCurrentSession().createQuery("from " + clazz.getName()).list();
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public T findById(final long id) throws HibernateException {
        try {
            return (T) getCurrentSession().get(clazz, id);
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public List<T> findByHql(final String hql) throws HibernateException {
        try {
            return getCurrentSession().createQuery("from " + clazz.getName() + " " + hql).list();
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }

    @SuppressWarnings("unchecked")
    public List<T> findBySql(final String sql) throws HibernateException {
        try {
            return getCurrentSession().createQuery(sql).list();
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }
    
    @SuppressWarnings("unchecked")
	public List<T> find(String sql, List<Object> param) {
		Query q = this.getCurrentSession().createQuery(sql);
		if (param != null && param.size() > 0) {
			for (int i = 0; i < param.size(); i++) {
				q.setParameter(i, param.get(i));
			}
		}
		return q.list();
	}
    
    @SuppressWarnings("unchecked")
	public List<T> find(String sql, Map<String,Object> mParam) {
		Query q = this.getCurrentSession().createQuery(sql);
		if(!mParam.isEmpty()) {
			Set<String> keys = mParam.keySet();
			for(String key : keys) {
				Object obj = mParam.get(key);  
                if(obj instanceof Collection<?>){  
                    q.setParameterList(key, (Collection<?>)obj);  
                }else if(obj instanceof Object[]){  
                    q.setParameterList(key, (Object[])obj);  
                }else{  
                    q.setParameter(key, obj);  
                }  
			}
		}
		return q.list();
	}
    
    @SuppressWarnings("unchecked")
	public List<T> find(String sql, List<Object> param, Map<String,String> mParam) {
		Query q = this.getCurrentSession().createQuery(sql);
		if (param != null && param.size() > 0) {
			for (int i = 0; i < param.size(); i++) {
				q.setParameter(i, param.get(i));
			}
		}
		if(!mParam.isEmpty()) {
			Set<String> keys = mParam.keySet();
			for(String key : keys) {
				q.setString(key, mParam.get(key));
			}
		}
		return q.list();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> find(String sql, List<Object> param, Integer page, Integer rows) {
		if(page == null || page < 1){
			page = 0;
		}
		if(rows == null || rows < 1){
			rows = 10;
		}
		Query q = this.getCurrentSession().createQuery(sql);
		if (param != null && param.size() > 0) {
			for (int i = 0; i < param.size(); i++) {
				q.setParameter(i, param.get(i));
			}
		}
		return q.setFirstResult(page * rows).setMaxResults(rows).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> find(String sql, Map<String,Object> mParam, Integer page, Integer rows) {
		if(page == null || page < 1){
			page = 0;
		}
		if(rows == null || rows < 1){
			rows = 10;
		}
		Query q = this.getCurrentSession().createQuery(sql);
		if(!mParam.isEmpty()) {
			Set<String> keys = mParam.keySet();
			for(String key : keys) {
				Object obj = mParam.get(key);  
                if(obj instanceof Collection<?>){  
                    q.setParameterList(key, (Collection<?>)obj);  
                }else if(obj instanceof Object[]){  
                    q.setParameterList(key, (Object[])obj);  
                }else{  
                    q.setParameter(key, obj);  
                }  
			}
		}
		return q.setFirstResult(page * rows).setMaxResults(rows).list();
	}
	
	@SuppressWarnings("unchecked")
	public List<T> find(String sql, List<Object> param, Map<String,String> mParam, Integer page, Integer rows) {
		if(page == null || page < 1){
			page = 0;
		}
		if(rows == null || rows < 1){
			rows = 10;
		}
		Query q = this.getCurrentSession().createQuery(sql);
		if (param != null && param.size() > 0) {
			for (int i = 0; i < param.size(); i++) {
				q.setParameter(i, param.get(i));
			}
		}
		if(!mParam.isEmpty()) {
			Set<String> keys = mParam.keySet();
			for(String key : keys) {
				q.setString(key, mParam.get(key));
			}
		}
		return q.setFirstResult(page * rows).setMaxResults(rows).list();
	}
	
	public long findCount(String sql, List<Object> param) {
		Query q = this.getCurrentSession().createQuery(sql);
		if (param != null && param.size() > 0) {
			for (int i = 0; i < param.size(); i++) {
				q.setParameter(i, param.get(i));
			}
		}
		Number number=(Number) q.uniqueResult();
        return  number.longValue();
	}
	
	public long findCount(String sql, List<Object> param, Map<String,String> mParam) {
		Query q = this.getCurrentSession().createQuery(sql);
		if (param != null && param.size() > 0) {
			for (int i = 0; i < param.size(); i++) {
				q.setParameter(i, param.get(i));
			}
		}
		if(!mParam.isEmpty()) {
			Set<String> keys = mParam.keySet();
			for(String key : keys) {
				q.setString(key, mParam.get(key));
			}
		}
		Number number=(Number) q.uniqueResult();
        return  number.longValue();
	}
	
	public double findDouble(String sql, List<Object> param) {
		Query q = this.getCurrentSession().createQuery(sql);
		if (param != null && param.size() > 0) {
			for (int i = 0; i < param.size(); i++) {
				q.setParameter(i, param.get(i));
			}
		}
		Number number=(Number) q.uniqueResult();
		if(number == null) {
			return 0;
		}else {
			return  number.doubleValue();
		}
	}
	
	public double findDouble(String sql, Map<String,Object> mParam) {
		Query q = this.getCurrentSession().createQuery(sql);
		if(!mParam.isEmpty()) {
			Set<String> keys = mParam.keySet();
			for(String key : keys) {
				Object obj = mParam.get(key);  
                if(obj instanceof Collection<?>){  
                    q.setParameterList(key, (Collection<?>)obj);  
                }else if(obj instanceof Object[]){  
                    q.setParameterList(key, (Object[])obj);  
                }else{  
                    q.setParameter(key, obj);  
                }  
			}
		}
		Number number=(Number) q.uniqueResult();
		if(number == null) {
			return 0;
		}else {
			return  number.doubleValue();
		}
	}
	
	public T findOne(String sql, List<Object> param){
		List<T> l = this.find(sql, param);
		if(l != null && l.size() > 0){
			return l.get(0);
		} else{
			return null;
		}
	}
	
	public int excuteByHql(String hql, List<Object> param) throws HibernateException {
		try {
			int result;
			Query q = this.getCurrentSession().createQuery(hql);
			if (param != null && param.size() > 0) {
				for (int i = 0; i < param.size(); i++) {
					q.setParameter(i, param.get(i));
				}
			}
			result = q.executeUpdate();
			return result;
		} catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
	}
	
	public int excuteByHql(String sql, Map<String,Object> mParam) throws HibernateException {
		Query q = this.getCurrentSession().createQuery(sql);
		if(!mParam.isEmpty()) {
			Set<String> keys = mParam.keySet();
			for(String key : keys) {
				Object obj = mParam.get(key);  
                if(obj instanceof Collection<?>){  
                    q.setParameterList(key, (Collection<?>)obj);  
                }else if(obj instanceof Object[]){  
                    q.setParameterList(key, (Object[])obj);  
                }else{  
                    q.setParameter(key, obj);  
                }  
			}
		}
		try {
			int result;
			result = q.executeUpdate();
			return result;
		} catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
	}

    public int excuteBySql(String sql) throws HibernateException {
        try {
            int result;
            SQLQuery query = getCurrentSession().createSQLQuery(sql);
            result = query.executeUpdate();
            return result;
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }

    }

    public T update(final T entity) throws HibernateException {
        try {
            Preconditions.checkNotNull(entity);
            getCurrentSession().update(entity);
            return entity;
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }
    /**
     * 
     * <p> Hibernate4  新占位符下分页查询方法
     * hql语句-按照查询条件进行分页查询，
     * </p>
     * hql-必须是hql语句
     * values-查询条件
     * pageSize-分页量
     * @Since 2017年06月22日 上午9:21:54
     * @author <a href=jiang.dn@neusoft.com>jiangdn</a>
     * 入参：FROM PatientAttentionEntity WHERE 1=1   AND  createTime >=UNIX_TIMESTAMP(:timesTamp) AND  trim(relationType) =:relationType ORDER BY  createTime DESC
     * Map<String, Object> params 形式 ： parameters.put("timesTamp",timesTamp);
     */
    
    public long countByHqlToLong(final String hql,final Map<String, Object> params) {
        Query query = getCurrentSession().createQuery(hql);
        if ((params != null) && !params.isEmpty()) {  
            for (String key : params.keySet()) {  
                Object obj = params.get(key); 
              //这里考虑传入的参数是什么类型，不同类型使用的方法不同  
                if(obj instanceof Collection<?>){  
                    query.setParameterList(key, (Collection<?>)obj);  
                }else if(obj instanceof Object[]){  
                    query.setParameterList(key, (Object[])obj);  
                }else{  
                    query.setParameter(key, obj);  
                }  
               
            }  
        }  
        Number number=(Number) query.uniqueResult();
        return  number.longValue();
    }
    /**
     * 
     * <p> Hibernate4  新占位符下HQL查询根据条件查找符合条件实体类信息。 
     * 
     * </p>
     * hql-必须是hql语句 （必须完整写出 from Class） 
     * values-查询条件 Map<String, Object> params
     * @Since 2017年06月22日 上午9:21:54
     * @author <a href=jiang.dn@neusoft.com>jiangdn</a>
     * 入参：FROM PatientAttentionEntity WHERE 1=1   AND  createTime >=UNIX_TIMESTAMP(:timesTamp) AND  trim(relationType) =:relationType ORDER BY  createTime DESC
     * Map<String, Object> params 形式 ： parameters.put("timesTamp",timesTamp);
     */
    public  List<T> findListByHQL4( final String hql, final Map<String, Object> params) {  
        Query query = this.getCurrentSession().createQuery(hql);  
        if ((params != null) && !params.isEmpty()) {  
            for (String key : params.keySet()) {  
                Object obj = params.get(key); 
              //这里考虑传入的参数是什么类型，不同类型使用的方法不同  
                if(obj instanceof Collection<?>){  
                    query.setParameterList(key, (Collection<?>)obj);  
                }else if(obj instanceof Object[]){  
                    query.setParameterList(key, (Object[])obj);  
                }else{  
                    query.setParameter(key, obj);  
                }  
               
            }  
        }  
        return query.list();  
    }  
    
    
    /**
     * 
     * <p> Hibernate4  新占位符下分页查询方法-pageNum方式
     * hql语句-按照查询条件进行分页查询，
     * </p>
     * hql-必须是hql语句
     * values-查询条件
     * offset-页偏移
     * pageSize-分页量
     * @Since 2017年06月22日 上午9:21:54
     * @author <a href=jiang.dn@neusoft.com>jiangdn</a>
     * 入参：FROM PatientAttentionEntity WHERE 1=1   AND  createTime >=UNIX_TIMESTAMP(:timesTamp) AND  trim(relationType) =:relationType ORDER BY  createTime DESC
     * Map<String, Object> params 形式 ： parameters.put("timesTamp",timesTamp);
     */
    
    
   
    public List<T> findByPageToPageNum(String hql, Map<String, Object> params, int pageNum, int pageSize) {
        // TODO Auto-generated method stub
        int offset = pageNum * pageSize;
        Query query = getCurrentSession().createQuery(hql);
        if ((params != null) && !params.isEmpty()) {  
            for (String key : params.keySet()) {  
                query.setParameter(key, params.get(key));  
            }  
        }  
        List<T> result =
                query.setFirstResult(offset).setMaxResults(pageSize)
                .list();
        return result;
    } 
    
    /**Hibernate4  新占位符下将普通查询hql语句，转换为count 查询语句
     * 
     * <p>
     * 将普通查询hql语句，转换为count 查询语句， 此方法不支持count(id) 转换
     * </p>
     * 必须是hql语句
     * @Since 2017年06月22日 上午9:21:54
     * @author <a href=jiang.dn@neusoft.com>jiangdn</a>
     * @return select COUNT(*) FROM ENTITY WHERE 1=1 AND param=? ORDER BY ID 入参如：FROM PatientAttentionEntity WHERE 1=1
     *         AND createTime >=UNIX_TIMESTAMP(?) 出参如： select COUNT(*) FROM PatientAttentionEntity WHERE 1=1 AND
     *         createTime >=UNIX_TIMESTAMP(?) 实现自动转换，PatientAttentionEntity为与表的hibernate映射文件
     *         因为countHql和countSql类型为private方法，所以不去修正，
     */

    public  final String formatCountByHql(String hql) {
        String countHql = null;
        if (StringUtils.isNotBlank(hql) && StringUtils.containsIgnoreCase(hql, FROM)) {
            int index = StringUtils.indexOfIgnoreCase(hql, FROM);
            countHql = COUNTHQL + StringUtils.substring(hql, index + 4, hql.length());
        }
        return countHql;
    }
    /**
     * 
     * <p> Hibernate4  新占位符-执行update语句
     * hql语句-按照查询条件执行update语句，
     * </p>
     * hql-必须是hql语句
     * values-查询条件
     * @Since 2017年06月22日 上午9:21:54
     * @author <a href=jiang.dn@neusoft.com>jiangdn</a>
     * 入参：update PatientAttentionEntity t set t.userName = :userName ,remarkContent=:remarkContent  where UUID=:uuid
     * Map<String, Object> params 形式 ： parameters.put("timesTamp",timesTamp);
     */
    public int updateWithoutExp(final String hql,final Map<String, Object> params) throws HibernateException {
        try {
            Query query = getCurrentSession().createQuery(hql);
            if ((params != null) && !params.isEmpty()) {  
                for (String key : params.keySet()) {  
                    query.setParameter(key, params.get(key));  
                }  
            }  
            Number number=(Number)  query.executeUpdate();
            return number.intValue();
        } catch (Exception e) {
            throw new HibernateException(e.getMessage(), e);
        }
    }
    
    protected Session getCurrentSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * 获取所有实体类
     */
    public Map<String,Class> getTables(){
        Map<String, ClassMetadata> metaMap = sessionFactory.getAllClassMetadata();
        Map<String, Class> map = new HashMap<>();
        for (String key : metaMap.keySet()) {
            AbstractEntityPersister classMetadata = (AbstractEntityPersister) metaMap
                    .get(key);
            String tableName = classMetadata.getTableName().toLowerCase();
            int index = tableName.indexOf(".");
            if (index >= 0) {
                tableName = tableName.substring(index + 1);
            }
            Class tableClass = null;
            try {
                tableClass = Class.forName(key);
                map.put(tableName, tableClass);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    protected void setClazz(final Class<T> clazzToSet) {
        this.clazz = Preconditions.checkNotNull(clazzToSet);
    }
}
