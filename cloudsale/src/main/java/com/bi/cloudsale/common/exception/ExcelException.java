package com.bi.cloudsale.common.exception;

public class ExcelException extends Exception {

	private static final long serialVersionUID = -8165882207827154661L;

	public ExcelException(){
		// TODO Auto-generated constructor stub
	}
	
	public ExcelException(String message) {
        super(message);
        // TODO Auto-generated constructor stub
    }

    public ExcelException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

    public ExcelException(String message, Throwable cause) {
        super(message, cause);
        // TODO Auto-generated constructor stub
    }
}
