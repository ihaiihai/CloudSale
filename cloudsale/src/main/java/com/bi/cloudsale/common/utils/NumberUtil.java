package com.bi.cloudsale.common.utils;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.common.utils
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/17 21:08
 * @Version: 1.0
 **/
public class NumberUtil {
    /**
     * double 转百分数
     */
    public static String getPercentFormat(double d){
        if(Double.isNaN(d)){
            return "-";
        }
        if(Double.isInfinite(d)){
            return "∞";
        }
        if(d==0){
            return "0";
        }
        NumberFormat nf = java.text.NumberFormat.getPercentInstance();
        nf.setMaximumIntegerDigits(10);//小数点前保留2位
        nf.setMinimumFractionDigits(2);// 小数点后保留2位
        String str = nf.format(d);
        return str;
    }
    /**
     * double 保留两位
     */
    public static String getDouble2Format(double d){
        if(Double.isNaN(d)){
            return "-";
        }
        if(Double.isInfinite(d)){
            return "∞";
        }
        if(d==0){
            return "0";
        }
        return String.format("%.2f", d);
    }
}
