package com.bi.cloudsale.common.exception;

public class BaseException extends Exception {
	
    private static final long serialVersionUID = 2804759567965575214L;
    private String code;
    private String message;

    public BaseException(String code, String message, Exception e) {
        super(e);
        this.code = code;
        this.message = message;
    }

    public BaseException(String code, String message) {
        this.code = code;
        this.message = message;
    }
    
    public BaseException(String message, Exception e) {
    	super(e);
    	this.message = message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

}
