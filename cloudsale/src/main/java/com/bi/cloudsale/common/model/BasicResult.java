package com.bi.cloudsale.common.model;

public class BasicResult {

    public BasicResult() {

    }

    public BasicResult(String code) {
        this.code = code;
    }

    public BasicResult(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public BasicResult(String code, Object data) {
        this.code = code;
        this.data = data;
    }

    public BasicResult(String code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public static BasicResult success(){
        BasicResult basicResult = new BasicResult(ResponseConstant.SUCCESS_CODE,null);
        return basicResult;
    }

    public static BasicResult success(Object data){
        BasicResult basicResult = new BasicResult(ResponseConstant.SUCCESS_CODE,data);
        return basicResult;
    }

    public static BasicResult error(String msg){
        BasicResult basicResult = new BasicResult(ResponseConstant.ERROR_CODE,msg);
        return basicResult;
    }

    /**
     * 代码 0为成功 -1为错误
     */
    private String code;
    /**
     * 成功或错误信息
     */
    private String msg;
    /**
     * 数据
     */
    private Object data;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
