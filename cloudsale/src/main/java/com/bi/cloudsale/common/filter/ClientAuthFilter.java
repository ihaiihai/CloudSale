package com.bi.cloudsale.common.filter;

import org.apache.commons.codec.digest.DigestUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * 验证上传数据接口签名
 *
 * @ProjectName: xkdaapi
 * @Package: com.xikang.daapi.config.filter
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2018/12/10 10:48
 * @Version: 1.0
 **/
public class ClientAuthFilter implements Filter {

    private static final Logger logger = LoggerFactory.getLogger(ClientAuthFilter.class);
    private static final long TIME_DIFFERENCE = 5 * 60 * 1000L;
    /**
     * 客户端认证白名单
     */
    private static String[] IGNORE_URLS = new String[]{
            "/login",
            "/bindRemoteInfo"
    };

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {

        if (isIgnored((HttpServletRequest) servletRequest)||(auth(servletRequest))) {
            filterChain.doFilter(servletRequest, servletResponse);
        } else {
            // 未授权
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            resp.flushBuffer();
        }
    }

    @Override
    public void destroy() {

    }

    /**
    * 校验
    */
    private boolean auth(ServletRequest request) {

        String user_id = request.getParameter("user_id");
        String timestamp = request.getParameter("timestamp");

        // 如果时间差大于1分钟，验证失败
        long now = System.currentTimeMillis();
        long time = Long.parseLong(timestamp);
        if ((now - time) >= TIME_DIFFERENCE) {
            return false;
        }

        // 客户端签名
        String clientSign = request.getParameter("sign");
        Map<String, String> map = new TreeMap<>();
        map.put("user_id",user_id);
        map.put("timestamp",timestamp);
        return sign(map).equalsIgnoreCase(clientSign);
    }

    /**
     *  签名
     */
    private String sign(Map<String, String> params) {
        StringBuilder baseString = new StringBuilder();
        // 自然序
        for (String key : params.keySet()) {
            baseString.append(key).append("=").append(params.get(key));
        }
        return DigestUtils.md5Hex(baseString.toString());
    }

    private boolean isIgnored(HttpServletRequest request){
        String uri = request.getRequestURI().trim().toLowerCase();
        for (String partURI : IGNORE_URLS) {
            if (uri.contains(partURI.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
    
    public static void main(String[] args) {
    	long now = System.currentTimeMillis();
    	System.out.println(now);
    	String user_id = "f5f0da5a3f5b43d49c41e79efb2b6e3d";
    	String sign = DigestUtils.md5Hex("timestamp="+now+"user_id=" + user_id);
    	System.out.println(sign);
	}
}
