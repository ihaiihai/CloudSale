package com.bi.cloudsale.common.utils;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 与日期相关的工具类
 * 
 * @author user
 *
 */
public class DateUtil {

	private static int year;

	public static final String SIMPLE_DATE_FORMAT = "yyyy-MM-dd";

	public static final String MD_DATE_FORMAT = "MM-dd";

	public static final String SHORT_DATE_FORMAT = "MM-dd HH:mm";

	public static final String FULL_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    public static final String SHORT_TIME_FORMAT = "HH:mm";

	public static final int DELAYMILISECOND = 1000;
	
	public static final long ONE_DAY_TIMES = 24*60*60*1000;
	public static final long ONE_HOUR_TIMES = 60*60*1000;

	private static Log logger = LogFactory.getLog(DateUtil.class);
	
	/**
     * 变更日期
     * @param date
     * @param num
     * @return Date
     */
    public static Date addDay(Date date, int num){
    	Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, num);
        return calendar.getTime();
    }

	/**
	 * "yyyy-MM-dd hh:mm:ss"，
	 * 
	 * @param date
	 * @param formatstr
	 * @return
	 */
	public static String formatDate(Date date, String formatstr) {

		SimpleDateFormat df = new SimpleDateFormat(formatstr);
		String result = "";
		if (date == null) {
			return result;
		}
		try {
			result = df.format(date);
		} catch (Exception e) {
			logger.error(e);
		}

		return result;
	}
	/**  
	 * Created on 2016年9月18日 by tianmh
	 * <p>
	 * Description:Date类型转换为固定格式Date类型
	 * </p>
	 * @param date
	 * @param regex
	 * @return Date
	 * @update [日期YYYY-MM-DD] [更改人姓名]
	 */ 
	public static Date format(Date date, String regex) {
		DateFormat df = new SimpleDateFormat(regex);
		String dateStr = df.format(date);
		try {
			return df.parse(dateStr);
		} catch (ParseException e) {
			throw new RuntimeException("Failed to parse date", e);
		}
	}
	/**
	 * 
	 * Created on 2010-8-24
	 * <p>
	 * Description:[把string转换date]
	 * </p>
	 * 
	 * @param dateStr
	 * @param formatstr
	 * @return
	 * @author:<a href=wang_l_neu@neusoft.com>王亮</a>
	 * @update:[日期YYYY-MM-DD] [更改人姓名]
	 */
	public static Date formatStringToDate(String dateStr, String formatstr) {

		SimpleDateFormat df = new SimpleDateFormat(formatstr);
		Date date = null;
		if (StringUtils.isBlank(dateStr)) {
			return date;
		}
		try {
			date = df.parse(dateStr);
		} catch (ParseException e) {
			logger.error(e);
		}
		return date;
	}

	/**
	 * 获得一天的最早日期时间,如传进来2008-09-09 20:15:30 返回 2008-09-09 00:00:00
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp getBeginINDay(Timestamp date) {
		Calendar ca = Calendar.getInstance();
		ca.setTime(date);
		ca.set(Calendar.HOUR_OF_DAY, 0);
		ca.set(Calendar.MINUTE, 0);
		ca.set(Calendar.SECOND, 0);
		return new Timestamp(ca.getTimeInMillis());
	}

	/**
	 * 获得一天的最早日期时间,如传进来2008-09-09 20:15:30 返回 2008-09-09 00:00:00
	 * 
	 * @param date
	 * @return
	 */
	public static Date getBeginINDay(Date date) {
		Calendar ca = Calendar.getInstance();
		ca.setTime(date);
		ca.set(Calendar.HOUR_OF_DAY, 0);
		ca.set(Calendar.MINUTE, 0);
		ca.set(Calendar.SECOND, 0);
		return new Date(ca.getTimeInMillis());
	}

	/**
	 * 获得一天的最晚日期时间,如传进来2008-09-09 10:15:30 返回 2008-09-09 23:59:59
	 * 
	 * @param date
	 * @return
	 */
	public static Timestamp getEndINDay(Timestamp date) {
		Calendar ca = Calendar.getInstance();
		ca.setTime(date);
		ca.set(Calendar.HOUR_OF_DAY, 23);
		ca.set(Calendar.MINUTE, 59);
		ca.set(Calendar.SECOND, 59);
		return new Timestamp(ca.getTimeInMillis());
	}

	/**
	 * 获得一天的最晚日期时间,如传进来2008-09-09 10:15:30 返回 2008-09-09 23:59:59
	 * 
	 * @param date
	 * @return
	 */
	public static Date getEndINDay(Date date) {
		Calendar ca = Calendar.getInstance();
		ca.setTime(date);
		ca.set(Calendar.HOUR_OF_DAY, 23);
		ca.set(Calendar.MINUTE, 59);
		ca.set(Calendar.SECOND, 59);
		return new Date(ca.getTimeInMillis());
	}

	/**
	 * 到今天截止还有多少分
	 */
	public static Integer getRemainMinutesOneDay(Date currentDate) {
		Calendar midnight=Calendar.getInstance();
		midnight.setTime(currentDate);
		midnight.add(Calendar.DAY_OF_MONTH,1);
		midnight.set(Calendar.HOUR_OF_DAY,0);
		midnight.set(Calendar.MINUTE,0);
		midnight.set(Calendar.SECOND,0);
		midnight.set(Calendar.MILLISECOND,0);
		Integer minutes=(int)((midnight.getTime().getTime()-currentDate.getTime())/1000/60);
		return minutes;
	}

	/**
	 * 
	 * Created on 2010-10-25
	 * <p>
	 * Description:[获得n天后的日期]
	 * </p>
	 * 
	 * @param n
	 * @return
	 */
	public static Date getNextDay(int n) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.DAY_OF_MONTH, n);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	/**
	 * Created on 2010-10-26
	 * <p>
	 * Description:[获得n月之后日期]
	 * </p>
	 * 
	 * @param i
	 * @return
	 * @author:<a href=wang_l_neu@neusoft.com>王亮</a>
	 * @update:[日期YYYY-MM-DD] [更改人姓名]
	 */
	public static Date getNextMonth(int n) {

		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(System.currentTimeMillis());
		calendar.add(Calendar.MONTH, n);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	/**
	 * 根据出生日期计算年龄
	 * 
	 * @param birthday
	 *            出生日期
	 * @return 虚岁
	 */
	public static int convertBirthdayToAge(Date birthday) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(birthday);
		Calendar today = new GregorianCalendar();
		int age = today.get(Calendar.YEAR) - cal.get(Calendar.YEAR);
		today.add(Calendar.YEAR, age);
		if (cal.before(today)) {
			age++;
		}
		return age;
	}

	public static String toFriendlyString(Date date, String format) {
		// 如果format为空，则直接使用yyyy-MM-dd HH:mm:ss的全格式形式
		format = format == null ? FULL_DATE_FORMAT : format;

		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		Calendar today = new GregorianCalendar();

		// 超过12个月
		// 返回几年前
		int source_year = cal.get(Calendar.YEAR);
		int today_year = today.get(Calendar.YEAR);
		Format f = new SimpleDateFormat(format);
		// if (source_year<=(today_year-1)){
		// //return f.format(date);
		//// return (today_year-source_year)+"年前";
		// }

		// 超过5周
		// 返回多少月前
		today.add(Calendar.DAY_OF_YEAR, -35);
		boolean bool = cal.before(today);
		today.add(Calendar.DAY_OF_YEAR, 35);
		if (bool) {
			int month = cal.get(Calendar.MONTH);
			int today_month = today.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);
			int today_day = today.get(Calendar.DAY_OF_MONTH);
			int monthsub = (today_year - source_year) * 12 + (today_month - month) - (day > today_day ? 1 : 0);
			return monthsub + "月前";
		}
		// 1~5周
		// 返回几周前
		today.add(Calendar.DATE, -7);
		bool = cal.before(today);
		today.add(Calendar.DATE, 7);
		if (bool) {
			long ld1 = today.getTimeInMillis();
			long ld2 = cal.getTimeInMillis();
			long days = (long) ((ld1 - ld2) / 86400000);
			return days / 7 + "周前";
		}
		// 1天~1周(7天)
		// 返回几天前
		today.add(Calendar.DATE, -1);
		bool = cal.before(today);
		today.add(Calendar.DATE, 1);
		if (bool) {
			long ld1 = today.getTimeInMillis();
			long ld2 = cal.getTimeInMillis();
			long days = (long) ((ld1 - ld2) / 86400000);
			return days + "天前";
		}
		// 1小时~24小时
		// 返回几小时前
		today.add(Calendar.HOUR, -1);
		bool = cal.before(today);
		today.add(Calendar.HOUR, 1);
		if (bool) {
			long ld1 = today.getTimeInMillis();
			long ld2 = cal.getTimeInMillis();
			long hour = (long) ((ld1 - ld2) / 3600000);
			return hour + "小时前";
		}
		// 30~60分钟
		// 返回半小时前
		today.add(Calendar.MINUTE, -29);
		bool = cal.before(today);
		today.add(Calendar.MINUTE, 29);
		if (bool) {
			return "半小时前";
		}

		// 5~30分钟
		// 返回几分钟前
		today.add(Calendar.MINUTE, -4);
		bool = cal.before(today);
		today.add(Calendar.MINUTE, 4);
		if (bool) {
			long ld1 = today.getTimeInMillis();
			long ld2 = cal.getTimeInMillis();
			long minute = (long) ((ld1 - ld2) / 60000);
			return minute + "分钟前";
		}

		// 0~5分钟
		// 返回刚刚
		bool = cal.before(today);
		if (bool) {
			return "刚刚";
		}
		// 当前时间之后，按实际format显示
		return f.format(date);

	}

	/**
	 * 根据 距离 1970-01-01 08：00 的秒数 ，获取 Date 类型的时间
	 * 
	 * @param lTime
	 *            - 距离 1970-01-01 08：00 的毫秒数, 如果 lTime < 0, 说明 该时间要早 于
	 *            1970-01-01 08：00，相反则时间 晚于 1970-01-01 08：00。
	 * @return Date
	 */
	public static Date formatLongToDate(final long lTime) {
		Date dTmpDate = new Date(lTime);
		return dTmpDate;
	}

	/**
	 * 根据Date类型的时间，计算出 距离 1970-01-01 08：00 的毫秒数
	 * 
	 * @param dDate
	 *            - 时间
	 * @return 距离 1970-01-01 08：00 的毫秒数
	 */
	public static long formatDateToLong(final Date dDate) {
		if (null == dDate) {
			logger.error("The date is null, so that can't format the date.");
			return 0;
		}
		long lTime = dDate.getTime();
		return lTime;
	}

	/**
	 * 根据 String 类型的时间，计算出 距离 1970-01-01 08：00 的毫秒数
	 * 
	 * @param strTime
	 *            - 时间; strFormat - 时间格式; 例如：(yyyy-MM-dd HH:mm:ss)
	 * @return 距离 1970-01-01 08：00 的毫秒数
	 */
	public static long formatDateStringToLong(final String strTime, final String strFormat) {
		// the param can't null.
		if ((StringUtils.isBlank(strTime)) || (StringUtils.isBlank(strFormat))) {
			logger.error("The strTime or strFormat is null, so that can't format the date.");
			return 0;
		} else {
		}

		// 1. Format the date from String to Date.
		Date dTmpDate = formatStringToDate(strTime, strFormat);
		if (null == dTmpDate) {
			logger.error("The dTmpDate is null, so that can't format the date.");
			return 0;
		} else {
		}

		// 2. Format the date from Date to long.
		long lTime = formatDateToLong(dTmpDate);

		return lTime;
	}

	/**
	 * 根据 String 类型的时间，计算出 距离 1970-01-01 08：00 的毫秒数
	 * 
	 * @param strTime
	 *            - 时间; strFormat - 时间格式; 例如：(yyyy-MM-dd HH:mm:ss)
	 * @return 距离 1970-01-01 08：00 的毫秒数
	 */
	public static String formatDateLongToString(final long lTime, final String strFormat) {
		// the param can't null.
		if (StringUtils.isBlank(strFormat)) {
			logger.error("The strFormat is null, so that can't format the date.");
			return null;
		} else {
		}

		// 1. Format the date from long to Date.
		Date dTmpDate = formatLongToDate(lTime);
		if (null == dTmpDate) {
			logger.error("The dTmpDate is null, so that can't format the date.");
			return null;
		} else {
		}

		// 2. Format the date from Date to String.
		String strTime = formatDate(dTmpDate, strFormat);

		return strTime;
	}

	/**
	 * 根据 String 类型的时间，计算出 距离 1970-01-01 08：00 的毫秒数
	 * 
	 * @param strTime
	 *            - 时间;
	 * @return 距离 1970-01-01 08：00 的毫秒数
	 */
	public static String formatDateLongToString(final long lTime) {
		// 1. Format the date from long to Date.
		Date dTmpDate = formatLongToDate(lTime);
		if (null == dTmpDate) {
			logger.error("The dTmpDate is null, so that can't format the date.");
			return null;
		} else {
		}

		// 2. Format the date from Date to String.
		String strTime = formatDate(dTmpDate, FULL_DATE_FORMAT);

		return strTime;
	}

	/**
	 * 根据 String 类型的时间，计算出 距离 1970-01-01 08：00 的毫秒数
	 * 
	 * @param strTime
	 *            - 时间;
	 * @return 距离 1970-01-01 08：00 的毫秒数
	 */
	public static long formatDateStringToLong(final String strTime) {
		// the param can't null.
		if (StringUtils.isBlank(strTime)) {
			logger.error("The strTime is null, so that can't format the date.");
			return 0;
		} else {
		}

		// 1. Format the date from String to Date.
		Date dTmpDate = formatStringToDate(strTime, FULL_DATE_FORMAT);
		if (null == dTmpDate) {
			logger.error("The dTmpDate is null, so that can't format the date.");
			return 0;
		} else {
		}

		// 2. Format the date from Date to long.
		long lTime = formatDateToLong(dTmpDate);

		return lTime;
	}
	/**
	 * 计算日期为模糊表达，“我的预约”item的右上角显示
	 * @param date 所要计算的时间
	 * @return 模糊表达的时间
	 */
	public static String getFuzzyTimeFromDate(Date date){
		if(date==null){
			return "";
		}
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date mdate, ndate;
		try {
			mdate = sdf.parse(sdf.format(date));
			ndate =sdf.parse(sdf.format(new Date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return "";
		}  
        Calendar cal = Calendar.getInstance();
        cal.setTime(mdate);    
        long mtime = cal.getTimeInMillis();                 
        cal.setTime(ndate);    
        long ntime = cal.getTimeInMillis();
        long dayDiff=(mtime-ntime)/(1000*3600*24);  //化为天
		if(dayDiff>1){
			//两天后
			stringBuffer.append(dayDiff + "天后");
		}else if(dayDiff<=-1){
			//两天前
			stringBuffer.append("1天前");
		}else{
			int hours = date.getHours();
			if(dayDiff==0){
				//今天
				if(hours<12){
					stringBuffer.append("今天上午");
				}else if(hours >= 12 && hours <= 18){
					stringBuffer.append("今天下午");
				}else{
					stringBuffer.append("今天晚间");
				}
			}else if(dayDiff == 1){
				//明天
				if(hours<12){
					stringBuffer.append("明天上午");
				}else if(hours >= 12 && hours <= 18){
					stringBuffer.append("明天下午");
				}else{
					stringBuffer.append("明天晚间");
				}
			}
		}
		return stringBuffer.toString();
	}

	/**
	 * 计算日期为模糊表达，“我的预约”item的中间显示
	 * @param date 所要计算的时间
	 * @return 模糊表达的时间
	 */
	public static String getFuzzyTime2FromDate(Date matchDate){
		if(matchDate==null){
			return "";
		}
		StringBuffer stringBuffer = new StringBuffer();
		stringBuffer.append("");
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
        Date mdate, ndate;
		try {
			mdate = sdf.parse(sdf.format(matchDate));
			ndate =sdf.parse(sdf.format(new Date()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			return "";
		}  
        Calendar cal = Calendar.getInstance();
        cal.setTime(mdate);    
        long mtime = cal.getTimeInMillis();                 
        cal.setTime(ndate);    
        long ntime = cal.getTimeInMillis();
        long dayDiff=(mtime-ntime)/(1000*3600*24);  //化为天
		if(dayDiff==0||dayDiff==1){
			SimpleDateFormat myFormat = new SimpleDateFormat("HH:mm");
		    String time = myFormat.format(matchDate);
			//今天或明天
			if(dayDiff==0){
				//今天
				stringBuffer.append("今天"+time);
			}else if(dayDiff == 1){
				//明天
				stringBuffer.append("明天"+time);
			}
			return stringBuffer.toString();
		}
		SimpleDateFormat myFormat = new SimpleDateFormat("MM月dd日 HH:mm");
	    String time = myFormat.format(matchDate);
		return time;
	}
	

    /**
     * 
     * Created on 2010-10-25
     * <p>
     * Description:[获得n天后的日期]
     * </p>
     * 
     * @param n
     * @return
     */
    public static Date getNextDay(Date date, int n) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(date.getTime());
        calendar.add(Calendar.DAY_OF_MONTH, n);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        return calendar.getTime();
    }
    
    /**
     * 获取本月第一天
     * @return
     */
    public static Date getFirstDayInMonth() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.set(Calendar.DAY_OF_MONTH,1);
    	calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
    	return calendar.getTime();
    }
    
    /**
     * 获取本月最后一天
     * @return
     */
    public static Date getEndDayInMonth() {
    	Calendar calendar = Calendar.getInstance();
    	calendar.add(Calendar.DAY_OF_MONTH,1);
    	calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
    	return calendar.getTime();
    }
	
	public static Date getDayStart(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
	
	public static Date getDayEnd(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	public static Date getBeforeDayStart(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)-1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
	
	public static Date getBeforeDayEnd(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DATE, calendar.get(Calendar.DATE)-1);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}

	
	/**
	 * <p>Title: getMonthStart</p>
	 * <p>Description: 获取月份开始时间</p>
	 * @param date
	 * @return
	 */
	public static Date getMonthStart(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month-1);
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
	
	/**
	 * <p>Title: getMonthEnd</p>
	 * <p>Description: 获取月份结束时间</p>
	 * @param date
	 * @return
	 */
	public static Date getMonthEnd(int year, int month) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month-1);
		calendar.set(Calendar.DATE, 31);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	public static Date getYearStart(int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, Calendar.JANUARY);
		calendar.set(Calendar.DATE, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		return calendar.getTime();
	}
	
	public static Date getYearEnd(int year) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, Calendar.DECEMBER);
		calendar.set(Calendar.DATE, 31);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		return calendar.getTime();
	}
	
	public static int getCurrentMonth() {
		Calendar cale = Calendar.getInstance();
		cale.setTime(new Date());
		return cale.get(Calendar.MONTH) + 1;
	}
	
	public static int getDayInYear(int year) {
		Calendar cale = Calendar.getInstance();
		cale.setTime(new Date());
		cale.set(Calendar.YEAR, year);
		return cale.get(Calendar.DAY_OF_YEAR);
	}
	
	public static int getDayInMonth(int year, int month) {
		Calendar cale = Calendar.getInstance();
		cale.setTime(new Date());
		cale.set(Calendar.YEAR, year);
		cale.set(Calendar.MONTH, month-1);
		return cale.get(Calendar.DAY_OF_MONTH);
	}
	
	public static int getMonthDays(int year, int month) {
		Calendar cale = Calendar.getInstance();
		cale.setTime(new Date());
		cale.set(Calendar.YEAR, year);
		cale.set(Calendar.MONTH, month-1);
		return cale.getActualMaximum(Calendar.DAY_OF_MONTH);
	}
	
	public static Date getDateStart(String dateStr) {
		if(StringUtils.isEmpty(dateStr)) {
			return null;
		}
		return getDayStart(formatStringToDate(dateStr, SIMPLE_DATE_FORMAT));
	}
	
	public static Date getDateEnd(String dateStr) {
		if(StringUtils.isEmpty(dateStr)) {
			return null;
		}
		return getDayEnd(formatStringToDate(dateStr, SIMPLE_DATE_FORMAT));
	}
	
	public static int getDuration(Date startDate, Date endDate) {
        int newL = (int) ((startDate.getTime() - endDate.getTime()) / (1000 * 3600 * 24));
        return newL;

    }
	
	public static void main(String[] args) {
		System.out.println(getDayInYear(2019));
		System.out.println(getDayInMonth(2019,2));
		System.out.println(getMonthDays(2019,2));
	}
	
}
