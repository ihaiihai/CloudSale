package com.bi.cloudsale.common.filter;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * java类简单作用描述
 *
 * @ProjectName: cloudsale
 * @Package: com.bi.cloudsale.common.filter
 * @ClassName: ${TYPE_NAME}
 * @Description: java类作用描述
 * @Author: sunzhimin
 * @CreateDate: 2019/2/13 10:21
 * @Version: 1.0
 **/
@Aspect
public class AspectAPIMoinitor {

    private static final Logger log = LoggerFactory.getLogger(AspectAPIMoinitor.class);
    /**
     * 统计API接口调用耗时（方法调用的时间）
     */
    @Around("execution(* com.bi.cloudsale.controller.*.*(..))")
    public Object logServiceMethodAccess(ProceedingJoinPoint joinPoint) throws Throwable {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        long start = System.currentTimeMillis();
        Object object = joinPoint.proceed();
        long end = System.currentTimeMillis();
        long takeTime = end - start;
        String className = joinPoint.getSignature().toString();
        log.info("接口耗时："+className+":"+request.getRequestURL().toString()+":"+Arrays.toString(joinPoint.getArgs())+":"+takeTime);
        return object;
    }
}
